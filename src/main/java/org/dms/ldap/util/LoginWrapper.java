/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ldap.util;

import java.util.ArrayList;

/**
 *
 * @author Dasun Chathuranga
 */
public class LoginWrapper {
    private String flag;
    private ArrayList<String> successMessages;
    private ArrayList<String> exceptionMessages;
    private ArrayList<?> data;
    private int totalRecords;
    
    private String distributorName;
    private String distributorAddress;
    private String distributorCode;
    private String distributorTelephone;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public ArrayList<String> getSuccessMessages() {
        return successMessages;
    }

    public void setSuccessMessages(ArrayList<String> successMessages) {
        this.successMessages = successMessages;
    }

    public ArrayList<String> getExceptionMessages() {
        return exceptionMessages;
    }

    public void setExceptionMessages(ArrayList<String> exceptionMessages) {
        this.exceptionMessages = exceptionMessages;
    }

    public ArrayList<?> getData() {
        return data;
    }

    public void setData(ArrayList<?> data) {
        this.data = data;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDistributorTelephone() {
        return distributorTelephone;
    }

    public void setDistributorTelephone(String distributorTelephone) {
        this.distributorTelephone = distributorTelephone;
    }
    
    
}
