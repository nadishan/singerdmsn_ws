/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ldap.util;

import java.util.ArrayList;

/**
 *
 * @author hashan
 */
public class ResponseWrapper {
    private String flag;
    private ArrayList<String> successMessages;
    private ArrayList<String> exceptionMessages;
    private ArrayList<?> data;
    private int totalRecords;

    /**
     * @return the flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * @return the successMessages
     */
    public ArrayList<String> getSuccessMessages() {
        return successMessages;
    }

    /**
     * @param successMessages the successMessages to set
     */
    public void setSuccessMessages(ArrayList<String> successMessages) {
        this.successMessages = successMessages;
    }

    /**
     * @return the exceptionMessages
     */
    public ArrayList<String> getExceptionMessages() {
        return exceptionMessages;
    }

    /**
     * @param exceptionMessages the exceptionMessages to set
     */
    public void setExceptionMessages(ArrayList<String> exceptionMessages) {
        this.exceptionMessages = exceptionMessages;
    }

    /**
     * @return the data
     */
    public ArrayList getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(ArrayList data) {
        this.data = data;
    }

    /**
     * @return the totalRecords
     */
    public int getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

}
