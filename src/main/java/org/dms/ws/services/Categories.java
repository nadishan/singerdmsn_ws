/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.CategoryContraller;
import org.dms.ws.singer.entities.Category;

/**
 *
 * @author SDU
 */
@Path("/Categories")
public class Categories {
    
    @GET
    @Path("/getCategories")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getCategories(@QueryParam("cateId") @DefaultValue("0") int cateId,
            @QueryParam("cateName") @DefaultValue("all") String cateName,
            @QueryParam("cateDesc") @DefaultValue("all") String cateDesc,
            @QueryParam("cateStatus") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("9999") int limit) {
        return CategoryContraller.getCategories(cateId, cateName, cateDesc, status,order,type, start, limit);
    }
    
    
    @PUT
    @Path("/addCaterory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addCaterory(Category cg,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){
        return Response.status(201).entity(CategoryContraller.addCaterory(cg,userId,room,department,branch,countryCode,division,organization)).build();
    }
    
    @POST
    @Path("/editCategory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editCategory(Category cg,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){
        return Response.status(201).entity(CategoryContraller.editCategory(cg,userId,room,department,branch,countryCode,division,organization)).build();
    }
}
