/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WarrentyRegistrationController;
import org.dms.ws.singer.entities.WarrentyRegistration;

/**
 *
 * @author SDU
 */
@Path("/WarrentyRegistrations")
public class WarrentyRegistrations {

    @GET
    @Path("/getWarrentyRegistration")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWarrentyRegistration(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("warrntyRegRefNo") @DefaultValue("0") int warrntyRegRefNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("customerNIC") @DefaultValue("all") String customerNIC,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("contactNo") @DefaultValue("all") String contactNo,
            @QueryParam("dateOfBirth") @DefaultValue("all") String dateOfBirth,
            @QueryParam("sellingPrice") @DefaultValue("0") double sellingPrice,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("modleDescription") @DefaultValue("all") String modleDescription,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WarrentyRegistrationController.getWarrentyRegistration(bisId, warrntyRegRefNo,imeiNo, customerNIC, customerName, contactNo, dateOfBirth, sellingPrice, modleNo, modleDescription, product, fromDate, toDate, order, type, start, limt);

    }
    
    
    @GET
    @Path("/getDsrWarrentyRegistration")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDsrWarrentyRegistration(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("warrntyRegRefNo") @DefaultValue("0") int warrntyRegRefNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("customerNIC") @DefaultValue("all") String customerNIC,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("contactNo") @DefaultValue("all") String contactNo,
            @QueryParam("dateOfBirth") @DefaultValue("all") String dateOfBirth,
            @QueryParam("sellingPrice") @DefaultValue("0") double sellingPrice,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("modleDescription") @DefaultValue("all") String modleDescription,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WarrentyRegistrationController.getDsrWarrentyRegistration(bisId, warrntyRegRefNo,imeiNo, customerNIC, customerName, contactNo, dateOfBirth, sellingPrice, modleNo, modleDescription, product, fromDate, toDate, order, type, start, limt);

    }

    @PUT
    @Path("/addWarrentyRegistration")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWarrentyRegistration(WarrentyRegistration wr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {

        return Response.status(201).entity(WarrentyRegistrationController.addWarrentyRegistration(wr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @PUT
    @Path("/addWorkOrderWarrentyRegistration")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrderWarrentyRegistration(WarrentyRegistration wr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {

        return Response.status(201).entity(WarrentyRegistrationController.addWorkOrderWarrentyRegistration(wr, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
