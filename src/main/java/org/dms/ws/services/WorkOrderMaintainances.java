/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WorkOrderController;
import org.dms.ws.singer.controllers.WorkOrderFeedBackController;
import org.dms.ws.singer.controllers.WorkOrderMaintainaceController;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderEstimate;
import org.dms.ws.singer.entities.WorkOrderMaintain;

/**
 *
 * @author SDU
 */
@Path("/WorkOrderMaintainances")
public class WorkOrderMaintainances {

    @GET
    @Path("/getWorkOrderList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderList(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("mjrDefectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("minDefecDesc") @DefaultValue("all") String minDefecDesc,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelName") @DefaultValue("all") String levelName,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("repairPayment") @DefaultValue("0") int repairPayment,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderMaintainaceController.getWorkOrderList(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleDesc, assessoriesRetained, mjrDefectDesc, minDefecDesc, rccReference, dateOfSale, proofOfPurches, bisId, binName, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelName, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, repairPayment, warrantyStatusMsg, order, type, start, limit);
    }

    @GET
    @Path("/getWorkOrderMaintainaceDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderMaintainaceDetails(@QueryParam("workorderMaintainId") @DefaultValue("0") int workorderMaintainId) {
        return WorkOrderMaintainaceController.getWorkOrderMaintainaceDetails(workorderMaintainId);
    }

    @PUT
    @Path("/addWorkOrderMaintainces")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrderMaintainces(WorkOrderMaintain wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderMaintainaceController.addWorkOrderMaintainces(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editWorkOrderMaintainces")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWorkOrderMaintainces(WorkOrderMaintain wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization) {
        return Response.status(201).entity(WorkOrderMaintainaceController.editWorkOrderMaintainces(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/deleteWorkOrderMaintainces")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteWorkOrderMaintainces(WorkOrderMaintain wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization) {
        return Response.status(201).entity(WorkOrderMaintainaceController.deleteWorkOrderMaintainces(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @PUT
    @Path("/addWorkOrderEstimate")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrderEstimate(WorkOrderEstimate woe,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization) {
        return Response.status(201).entity(WorkOrderMaintainaceController.addWorkOrderEstimate(woe, userId, room, department, branch, countryCode, division, organization)).build();

    }

    @GET
    @Path("/getWorkOrderMaintainList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderMaintainList(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("warrantyTypedesc") @DefaultValue("all") String warrantyTypedesc,
            @QueryParam("technician") @DefaultValue("all") String technician,
            @QueryParam("iemiNo") @DefaultValue("all") String iemiNo,
            @QueryParam("modelDesc") @DefaultValue("all") String modelDesc,
            @QueryParam("levelDesc") @DefaultValue("all") String levelDesc,
            @QueryParam("estimateStatusMsg") @DefaultValue("all") String estimateStatusMsg,
            @QueryParam("repairStatus") @DefaultValue("all") String repairStatus,
            @QueryParam("invoiceNo") @DefaultValue("all") String invoiceNo,
            @QueryParam("statusMsg") @DefaultValue("all") String statusMsg,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderMaintainaceController.getWorkOrderMaintainList(bisId, workOrderNo, warrantyTypedesc, technician, iemiNo, modelDesc, levelDesc, estimateStatusMsg, repairStatus, invoiceNo, statusMsg, order, type, start, limit);
    }

    @GET
    @Path("/getWorkOrderListToClose")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderListToClose(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("warrantyTypedesc") @DefaultValue("all") String warrantyTypedesc,
            @QueryParam("technician") @DefaultValue("all") String technician,
            @QueryParam("iemiNo") @DefaultValue("all") String iemiNo,
            @QueryParam("modelDesc") @DefaultValue("all") String modelDesc,
            @QueryParam("levelDesc") @DefaultValue("all") String levelDesc,
            @QueryParam("estimateStatusMsg") @DefaultValue("all") String estimateStatusMsg,
            @QueryParam("repairStatus") @DefaultValue("all") String repairStatus,
            @QueryParam("invoiceNo") @DefaultValue("all") String invoiceNo,
            @QueryParam("statusMsg") @DefaultValue("all") String statusMsg,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderMaintainaceController.getWorkOrderListToClose(workOrderNo, warrantyTypedesc, technician, iemiNo, modelDesc, levelDesc, estimateStatusMsg, repairStatus, invoiceNo, statusMsg, bisId, order, type, start, limit);
    }

    @GET
    @Path("/GenarateEstimateNo")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper GenarateEstimateNo() {

        return WorkOrderMaintainaceController.GenarateEstimateNo();
    }

    @GET
    @Path("/getRepairType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getRepairType() {

        return WorkOrderMaintainaceController.getRepairType();
    }

    @GET
    @Path("/getEstimateStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEstimateStatus() {

        return WorkOrderMaintainaceController.getEstimateStatus();
    }

    @GET
    @Path("/getPaymentOption")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getPaymentOption() {

        return WorkOrderMaintainaceController.getPaymentOption();
    }

    @GET
    @Path("/getMaintainWarranty")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getMaintainWarranty() {
        return WorkOrderMaintainaceController.getMaintainWarranty();
    }

    @GET
    @Path("/sendEstimateCustomerMail")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper sendEstimateCustomerMail(@QueryParam("workOrderNo") String workOrderNo,
            @QueryParam("woReply") String woReply,
            @QueryParam("esRefNo") String esRefNo,
            @QueryParam("customerRef") String customerRef,
            @QueryParam("workorderMaintainId") @DefaultValue("0") int workorderMaintainId) {

        return WorkOrderMaintainaceController.sendEstimateCustomerMail(workOrderNo, woReply, esRefNo, customerRef, workorderMaintainId);

    }

    @GET
    @Path("/getWorkOrderListToAddMaintain")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderList(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("mjrDefectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderMaintainaceController.getWorkOrderList_To_Add_Maintain(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, order, type, start, limit);

    }
    
    
    
    @GET
    @Path("/getWorkOrderAllList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderAllList(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("mjrDefectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("paymentType") @DefaultValue("0") int paymentType,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderMaintainaceController.getWorkOrderFullList(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, paymentType, order, type, start, limit);

    }

    
    

}
