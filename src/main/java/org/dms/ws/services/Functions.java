/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.FunctionController;

/**
 *
 * @author SDU
 */
@Path("/Functions")
public class Functions {

    @GET
    @Path("/getFunctions")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getFunctions(@QueryParam("funtId") @DefaultValue("0") int functId,
            @QueryParam("funtDesc") @DefaultValue("all") String functDesc,
            @QueryParam("functtype") @DefaultValue("0") int functType,
            @QueryParam("functstatus") @DefaultValue("0") int functStatus,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return FunctionController.getFunctions(functId, functDesc, functType, functStatus, start, limt);

    }

    @GET
    @Produces
    @Path("/getServerDate")
    public String getServerDate() {

        Date d1 = new Date();
        SimpleDateFormat df = new SimpleDateFormat("ddMMYYYY");
        String formattedDate = df.format(d1);
        
        System.out.println("\nSystem date >>>>>>"+formattedDate);
        return formattedDate;
    }

}
