/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.EventPromotionController;

/**
 *
 * @author SDU
 */
@Path("/EventPromotions")
public class EventPromotions {
    
    
    @GET
    @Path("/getEventPromotionList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventPromotionList(@QueryParam("userId") String userId) {
        return EventPromotionController.getEventPromotionList(userId);

    }
    
    
    @GET
    @Path("/getPromotionPointList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getPromotionPointList(@QueryParam("userId") String userId) {
        return EventPromotionController.getPromotionPointList(userId);

    }
}
