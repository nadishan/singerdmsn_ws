/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.enterprise.inject.Any;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.IssueInventoryController;
import org.dms.ws.singer.entities.DeviceIssue;
import org.dms.ws.singer.entities.DeviceIssueOffline;

/**
 *
 * @author SDU
 */
@Path("/IssueInventories")
public class IssueInventories {

    @GET
    @Path("/getInventoryIssueList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getInventoryIssueList(@QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("distributerID") @DefaultValue("0") int distributerID,
            @QueryParam("dSRId") @DefaultValue("0") int dSRId,
            @QueryParam("issueDate") @DefaultValue("all") String issueDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return IssueInventoryController.getInventoryIssueList(issueNo, distributerID, dSRId, issueDate, status, fromDate, toDate, order, type, start, limt);

    }

    @GET
    @Path("/getInventoryIssueDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getInventoryIssueDetail(@QueryParam("issueNo") int issueNo,
            @QueryParam("dSRId") int dSRId,
            @QueryParam("status") @DefaultValue("0") int status) {

        return IssueInventoryController.getInventoryIssueDetail(issueNo, dSRId, status);

    }

    @GET
    @Path("/getInventoryIssueDetailMobile")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getInventoryIssueDetailMobile(@QueryParam("issueNo") int issueNo,
            @QueryParam("dSRId") int dSRId) {

        return IssueInventoryController.getInventoryIssueDetail_To_Mobile(issueNo, dSRId);

    }

    @GET
    @Path("/getDSRInventoryIssueDetailMobile")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDSRInventoryIssueDetailMobile(@QueryParam("dSRId") int dSRId) {

        return IssueInventoryController.getDSRInventoryIssueDetail_To_Mobile(dSRId);

    }

    @GET
    @Path("/getAllDSRModelList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getAllDSRModelList(@QueryParam("dSRId") int dSRId,
            @QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("model") @DefaultValue("all") String model) {

        return IssueInventoryController.getAllDSRModelList(dSRId, issueNo, model);

    }

    @PUT
    @Path("/addInventoryIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addInventoryIssue(DeviceIssue di,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(IssueInventoryController.addInventoryIssue(di, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/addOfflineInventoryIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addOfflineInventoryIssue(DeviceIssueOffline di,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(IssueInventoryController.addOfflineInventoryIssue(di, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/closeDayEnd")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response closeDayEnd(@QueryParam("bisId") int bisId,
            @QueryParam("totalTrans") int totalTrans,
            @QueryParam("itemCnt") int itemCnt) {
        
        System.out.println(">>>>> closeDayEnd");
        
        return Response.status(201).entity(IssueInventoryController.closeDayEnd(bisId,totalTrans,itemCnt)).build();
    }

    @POST
    @Path("/editInventoryIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editInventoryIssue(DeviceIssue di,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(IssueInventoryController.editInventoryIssue(di, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/cancleInventoryIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response cancleInventoryIssue(DeviceIssue di,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(IssueInventoryController.cancleInventoryIssue(di, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/inqUserModelList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper inqUserModelList(@QueryParam("dSRId") int dSRId) {

        return IssueInventoryController.getUserModels(dSRId);

    }

    @GET
    @Path("/inqUserModelImei")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper inqUserModelImei(@QueryParam("dSRId") int dSRId,
            @QueryParam("modelNo") int modelNo) {

        return IssueInventoryController.getModelImeis(dSRId, modelNo);

    }

    @GET
    @Path("/inqUserModelImeiForOffline")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper inqUserModelImeiForOffline(@QueryParam("dSRId") int dSRId,
            @QueryParam("modelNo") @DefaultValue("0") int modelNo) {

        return IssueInventoryController.getModelImeisForOffline(dSRId);

    }

    @GET
    @Path("/checkIMEI")
    @Produces({MediaType.APPLICATION_JSON})
    public int checkIMEI(@QueryParam("bisId") int bisId,
            @QueryParam("imeiNo") String imeiNo) {
        IssueInventoryController inv = new IssueInventoryController();
        return inv.checkImei(bisId, imeiNo);

    }
}
