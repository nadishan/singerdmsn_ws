/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ImportDebitNoteController;
import org.dms.ws.singer.entities.ImportDebitNote;

/**
 *
 * @author SDU
 */
@Path("/ImportDebitNotes")
public class ImportDebitNotes {

    @GET
    @Path("/getImportDebitNote")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImportDebitNote(
            @QueryParam("bisId") int bisId,
            @QueryParam("debitNoteNo") @DefaultValue("all") String debitNoteNo,
            @QueryParam("orderNo") @DefaultValue("all") String orderNo,
            @QueryParam("invoiceNo") @DefaultValue("all") String invoiceNo,
            @QueryParam("contactNo") @DefaultValue("all") String contactNo,
            @QueryParam("partNo") @DefaultValue("all") String partNo,
            @QueryParam("orderQty") @DefaultValue("0") int orderQty,
            @QueryParam("deleveryQty") @DefaultValue("0") int deleveryQty,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("headStatus") @DefaultValue("0") int headStatus,
            @QueryParam("customerNo") @DefaultValue("all") String customerNo,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ImportDebitNoteController.getImportDebitNote(bisId, debitNoteNo, orderNo, invoiceNo, contactNo, partNo, orderQty, deleveryQty, deleveryDate, headStatus, customerNo,order,type, start, limt);

    }

    @GET
    @Path("/getImportDebitNoteDetial")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImportDebitNoteDetial(@QueryParam("debitNoteNo") @DefaultValue("all") String debitNoteNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("customerCode") @DefaultValue("all") String customerCode,
            @QueryParam("shopCode") @DefaultValue("all") String shopCode,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("orderNo") @DefaultValue("all") String orderNo,
            @QueryParam("salerPrice") @DefaultValue("0") double salerPrice,
            @QueryParam("dateAccepted") @DefaultValue("all") String dateAccepted,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("site") @DefaultValue("all") String site,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ImportDebitNoteController.getImportDebitNoteDetial(debitNoteNo, imeiNo, customerCode, shopCode, modleNo, bisId, orderNo, salerPrice, dateAccepted, status, remarks, site, order,type,start, limt);

    }

    @PUT
    @Path("/addImportDebitNote")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addImportDebitNote(ImportDebitNote im,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(ImportDebitNoteController.addImportDebitNote(im, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
