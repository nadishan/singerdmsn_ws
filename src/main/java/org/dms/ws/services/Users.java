/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.singer.controllers.UserController;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserGroup;

/**
 *
 * @author SDU
 */
@Path("/Users")
public class Users {

    @GET
    @Path("/getUsers")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getUsersDetails(
            @QueryParam("key") @DefaultValue("all") String key,
            @QueryParam("value") @DefaultValue("all") String value,
            @QueryParam("status") @DefaultValue("all") String status,
            @QueryParam("start") @DefaultValue("all") String start,
            @QueryParam("limit") @DefaultValue("all") String limit,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
 
        return UserController.getUsers(key,value,status,start,limit,userId,room,department,branch,countryCode,division,organization, system);
    }

    @PUT
    @Path("/addUser")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addUser(User us,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch") String branch, 
            @HeaderParam("countryCode")String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization")String organization,
            @HeaderParam("system")String system) {

        
        return Response.status(201).entity(UserController.addUser(us,userId,room,department,branch,countryCode,division,organization,system)).build();
    }
    
    
    @POST
    @Path("/editUser")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editUser(User us,
            @HeaderParam("userId")  String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode")  String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system")String system){   
        return Response.status(201).entity(UserController.editUser(us,userId,room,department,branch,countryCode,division,organization,system)).build();
    }
    
    @GET
    @Path("/resetUserPassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response resetUserPassword(@QueryParam("userId")  String userid,
            @QueryParam("email")  String email,
            @HeaderParam("userId")  String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode")  String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system")String system){   
        return Response.status(201).entity(UserController.resetUserPassword(userid,email,userId,room,department,branch,countryCode,division,organization,system)).build();
    }
    
    
    @POST
    @Path("/editUserWithoutGroup")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editUserWithoutGroup(User us,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode")  String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization")  String organization,
            @HeaderParam("system")String system){   
        return Response.status(201).entity(UserController.editUserWithoutGroup(us,userId,room,department,branch,countryCode,division,organization,system)).build();
    }
    
    
    @POST
    @Path("/changePassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response changePassword(User us,
            @HeaderParam("userId")  String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode")  String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization")  String organization,
            @HeaderParam("system")String system){   
        return Response.status(201).entity(UserController.changePassword(us,userId,room,department,branch,countryCode,division,organization,system)).build();
    }

    
    
    
    @GET
    @Path("/getUserGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getUserGroups(
            @QueryParam("userId") @DefaultValue("all") String userId,
            @HeaderParam("userId")  String user_id, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode")  String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return UserController.getUserGroups(userId,userId,room,department,branch,countryCode,division,organization,system);
    }
    
    
    @GET
    @Path("/getAllUsers")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getAllUsers(
            @QueryParam("userId") @DefaultValue("all") String usId,
            @QueryParam("firstName") @DefaultValue("all") String firstName,
            @QueryParam("lastName") @DefaultValue("all") String lastName,
            @QueryParam("commonName") @DefaultValue("all") String commonName,
            @QueryParam("designation") @DefaultValue("all") String designation,
            @QueryParam("telephoneNumber") @DefaultValue("all") String telephoneNumber,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("createdOn") @DefaultValue("all") String createdOn,
            @QueryParam("inactivedOn") @DefaultValue("all") String inactivedOn,
            @QueryParam("status") @DefaultValue("all") String status,
            @QueryParam("extraParams") @DefaultValue("all") String extraParams,
            @QueryParam("start") @DefaultValue("all") String start,
            @QueryParam("limit") @DefaultValue("all") String limit,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
 
        return UserController.getUsers123(usId,firstName,lastName,commonName,designation,telephoneNumber,email,createdOn,inactivedOn,status,extraParams,start,limit,userId,room,department,branch,countryCode,division,organization, system);
    }
    
    
}
