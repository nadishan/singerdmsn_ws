/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.DeviceReturnController;
import org.dms.ws.singer.entities.DeviceReturn;

/**
 *
 * @author SDU
 */
@Path("/DeviceReturns")
public class DeviceReturns {

    @GET
    @Path("/getDeviceReturnList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceReturnList(@QueryParam("returnNo") @DefaultValue("0") int returnNo,
            @QueryParam("distributorId") @DefaultValue("0") int distributorId,
            @QueryParam("dsrId") @DefaultValue("0") int dsrId,
            @QueryParam("distributorName") @DefaultValue("all") String distributorName,
            @QueryParam("dsrName") @DefaultValue("all") String dsrName,
            @QueryParam("returnDate") @DefaultValue("all") String returnDate,
            @QueryParam("price") @DefaultValue("0") double price,
            @QueryParam("distributorMargin") @DefaultValue("0") double distributorMargin,
            @QueryParam("dealerMargin") @DefaultValue("0") double dealerMargin,
            @QueryParam("discount") @DefaultValue("0") double discount,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return DeviceReturnController.getDeviceReturnList(returnNo, distributorId, dsrId, distributorName, dsrName, returnDate, price, distributorMargin, dealerMargin, discount, status, order, type, start, limt);

    }

    @GET
    @Path("/getDeviceReturnListHeadOffice")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceReturnListHeadOffice(@QueryParam("returnNo") @DefaultValue("0") int returnNo,
            @QueryParam("distributorId") @DefaultValue("0") int distributorId,
            @QueryParam("dsrId") @DefaultValue("0") int dsrId,
            @QueryParam("distributorName") @DefaultValue("all") String distributorName,
            @QueryParam("dsrName") @DefaultValue("all") String dsrName,
            @QueryParam("returnDate") @DefaultValue("all") String returnDate,
            @QueryParam("price") @DefaultValue("0") double price,
            @QueryParam("distributorMargin") @DefaultValue("0") double distributorMargin,
            @QueryParam("dealerMargin") @DefaultValue("0") double dealerMargin,
            @QueryParam("discount") @DefaultValue("0") double discount,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("reason") @DefaultValue("all") String reason,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return DeviceReturnController.getDeviceReturnListHeadOffice(returnNo, distributorId, dsrId, distributorName, dsrName, returnDate, price, distributorMargin, dealerMargin, discount, status, reason, order, type, start, limt);

    }

    @PUT
    @Path("/addDeviceReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addDeviceReturn(DeviceReturn dr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceReturnController.addDeviceReturn(dr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @PUT
    @Path("/addDeviceReturnHeadOffice")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addDeviceReturnHeadOffice(DeviceReturn dr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceReturnController.addDeviceReturnHeadOffice(dr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDeviceReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceReturn(DeviceReturn dr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceReturnController.editDeviceReturn(dr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDeviceReturnHeadOffice")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceReturnHeadOffice(DeviceReturn dr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceReturnController.editDeviceReturnHeadOffice(dr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDeviceReturnDtl")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceReturnDtl(DeviceReturn dr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        System.out.println("editDeviceReturnDtl method");
        return Response.status(201).entity(DeviceReturnController.editDeviceReturnDtl(dr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDeviceReturnDtlHo")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceReturnDtlHo(DeviceReturn dr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        System.out.println("editDeviceReturnDtlHo method");
        return Response.status(201).entity(DeviceReturnController.editDeviceReturnDtlHo(dr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getDeviceReturnDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceReturnDetail(@QueryParam("returnNo") @DefaultValue("0") int returnNo) {
        return DeviceReturnController.getDeviceReturnDetail(returnNo);

    }

    @POST
    @Path("/editDeviceReturnDtlHoRej")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceReturnDtlHoRej(@QueryParam("returnNo") @DefaultValue("0") int returnNo)
             {
        return Response.status(201).entity(DeviceReturnController.editDeviceReturnDtlHoRej(returnNo)).build();
    }

    
    @GET
    @Path("/checkDayEnd")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper checkDayEnd(@QueryParam("bisId") @DefaultValue("0") int bisId) {
        return DeviceReturnController.checkDayEnd(bisId);

    }

    @GET
    @Path("/getDeviceReturnDetailHeadOffice")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceReturnDetailHeadOffice(@QueryParam("returnNo") @DefaultValue("0") int returnNo) {
        return DeviceReturnController.getDeviceReturnDetailHeadOffice(returnNo);

    }

    @GET
    @Path("/getDeviceReturnDetailHeadOfficeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceReturnDetailHeadOfficeList(@QueryParam("returnNo") @DefaultValue("0") int returnNo,
    @QueryParam("modleDesc") @DefaultValue("ALL") String modleDesc,
    @QueryParam("imeiNo") @DefaultValue("ALL") String imeiNo,
    @QueryParam("salesPrice") @DefaultValue("0") int salesPrice) {
        return DeviceReturnController.getDeviceReturnDetailHeadOfficeList(returnNo,modleDesc,imeiNo,salesPrice);

    }

}
