/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.EventMasterController;
import org.dms.ws.singer.entities.EventBisType;
import org.dms.ws.singer.entities.EventMaster;

/**
 *
 * @author SDU
 */
@Path("/EventMasters")
public class EventMasters {

    @GET
    @Path("/getEventMasterList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventMasterList(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("eventName") @DefaultValue("all") String eventName,
            @QueryParam("eventDesc") @DefaultValue("all") String eventDesc,
            @QueryParam("startDate") @DefaultValue("all") String startDate,
            @QueryParam("endDate") @DefaultValue("all") String endDate,
            @QueryParam("threshold") @DefaultValue("all") String threshold,
            @QueryParam("statusMsg") @DefaultValue("all") String statusMsg,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("statusString") @DefaultValue("all") String statusString,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return EventMasterController.getEventMasterList(eventId, eventName, eventDesc, startDate, endDate, threshold,statusMsg,status,statusString, order, type, start, limt);

    }

    @GET
    @Path("/getBussinessStructTypeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessStructTypeList() {
        return EventMasterController.getBussinessStructTypeList();

    }

    @GET
    @Path("/getBussinessList")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessList(@QueryParam("bisTypeList") String bisTypeList,
            @QueryParam("bisName") @DefaultValue("all") String bisName,
            @QueryParam("bisTypeDesc") @DefaultValue("all") String bisTypeDesc,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {
        return EventMasterController.getBussinessList(bisTypeList, bisName, bisTypeDesc, order, type, start, limt);

    }

    @PUT
    @Path("/addEventMaster")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addEventMaster(EventMaster em,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(EventMasterController.addEventMaster(em, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editEventMaster")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editEventMaster(EventMaster em,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(EventMasterController.editEventMaster(em, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getEventMasterDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventMasterDetails(@QueryParam("eventId") int eventId) {
        return EventMasterController.getEventMasterDetails(eventId);

    }

    @GET
    @Path("/getSalesType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getSalesType() {
        return EventMasterController.getSalesType();

    }

    @GET
    @Path("/getEventPeriod")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventPeriod() {
        return EventMasterController.getEventPeriod();

    }

    @GET
    @Path("/getProductFamily")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getProductFamily() {
        return EventMasterController.getProductFamily();

    }

    @GET
    @Path("/getBrandList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBrandList() {
        return EventMasterController.getBrandList();

    }

    
    @POST
    @Path("/startEvent")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response startEvent(EventMaster em,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, 
            @HeaderParam("system") String system) {
        return Response.status(201).entity(EventMasterController.startEvent(em, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
}
