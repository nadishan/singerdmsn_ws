/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ClearTransitItemController;
import org.dms.ws.singer.entities.DeviceIssue;

/**
 *
 * @author SDU
 */
@Path("/ClearTransit")
public class ClearTransitItems {

    @GET
    @Path("/getClearTransitList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getClearTransitList(@QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("fromName") @DefaultValue("all") String fromName,
            @QueryParam("distributerID") @DefaultValue("0") int distributerID,
            @QueryParam("toName") @DefaultValue("all") String toName,
            @QueryParam("dSRId") @DefaultValue("0") int dSRId,
            @QueryParam("issueDate") @DefaultValue("all") String issueDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ClearTransitItemController.getClearTransitList(issueNo, fromName, distributerID, toName, dSRId, issueDate, order, type, start, limt);

    }

    @GET
    @Path("/getClearTransitDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getClearTransitDetails(@QueryParam("issueNo") @DefaultValue("0") int issueNo) {

        return ClearTransitItemController.getClearTransitDetails(issueNo);

    }

    @PUT
    @Path("/addClearTransit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addClearTransit(DeviceIssue TransitItem,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(ClearTransitItemController.addClearTransit(TransitItem, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
