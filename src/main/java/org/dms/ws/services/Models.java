/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ModelListController;
import org.dms.ws.singer.entities.ModelList;

/**
 *
 * @author SDU
 */
@Path("/Models")
public class Models {

    @GET
    @Path("/getModelDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getModelDetails(@QueryParam("model_No") @DefaultValue("0") int modelno,
            @QueryParam("model_Description") @DefaultValue("all") String modeldesc,
            @QueryParam("part_Prd_Code") @DefaultValue("all") String partprdcode,
            @QueryParam("art_Prd_Code_Desc") @DefaultValue("all") String partprddesc,
            @QueryParam("part_Prd_Family") @DefaultValue("all") String partfamily,
            @QueryParam("part_Prd_Family_Desc") @DefaultValue("all") String partfamilydesc,
            @QueryParam("commodity_Group_1") @DefaultValue("all") String commoditygroup1,
            @QueryParam("commodity_Group_2") @DefaultValue("all") String commoditygroup2,
            @QueryParam("wrn_Scheme") @DefaultValue("all") String wrnscheme,
            @QueryParam("rep_Category") @DefaultValue("all") String repcat,
            @QueryParam("selling_Price") @DefaultValue("0") double sellprice,
            @QueryParam("cost_Price") @DefaultValue("0") double costprice,
            @QueryParam("dealer_Margin") @DefaultValue("0") double dealermargin,
            @QueryParam("distributor_Margin") @DefaultValue("0") double distributormargin,
            @QueryParam("status") @DefaultValue("0") int status,
            //@QueryParam("altPartsExist") @DefaultValue("0") int altPartsExist,
            @QueryParam("altPartsExistDesc") @DefaultValue("all") String altPartsExist,
            //@QueryParam("erpPartCode") @DefaultValue("all") String erpPart,
            @QueryParam("erp_part") @DefaultValue("all") String erpPart,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ModelListController.getModelDetails(modelno, modeldesc, partprdcode, partprddesc, partfamily, partfamilydesc, commoditygroup1, commoditygroup2, wrnscheme, repcat, sellprice, costprice, dealermargin, distributormargin, status,altPartsExist,erpPart,order,type, start, limt);

    }

    @POST
    @Path("/editModel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editModel(ModelList modell,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(ModelListController.editModel(modell, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
