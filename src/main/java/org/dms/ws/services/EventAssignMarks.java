/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.EventAssignMarkController;
import org.dms.ws.singer.entities.EventTransaction;

/**
 *
 * @author SDU
 */
@Path("/EventAssignMarks")
public class EventAssignMarks {

    @GET
    @Path("/getEventAssignMarksList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventAssignMarksList(@QueryParam("nonSalePoint") @DefaultValue("0") int nonSalePoint,
            @QueryParam("userId") @DefaultValue("all") String userId,
            @QueryParam("assignBy") @DefaultValue("all") String assignBy,
            @QueryParam("pointAddDate") @DefaultValue("all") String pointAddDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("statusMsg") @DefaultValue("all") String statusMsg,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return EventAssignMarkController.getEventAssignMarksList(nonSalePoint, userId, assignBy, pointAddDate, status,eventId,statusMsg, order, type, start, limt);
    }

    @GET
    @Path("/getEventNonSalesRuleList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventNonSalesRuleList(@QueryParam("eventId") int eventId) {
        return EventAssignMarkController.getEventNonSalesRuleList(eventId);

    }

    @PUT
    @Path("/addEventMarks")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addEventMarks(ArrayList<EventTransaction> EventTransList,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(EventAssignMarkController.addEventMarks(EventTransList, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editEventMarks")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editEventMarks(ArrayList<EventTransaction> EventTransList,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(EventAssignMarkController.editEventMarks(EventTransList, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    
    @GET
    @Path("/getEventParticipant")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventParticipant(@QueryParam("eventId") int eventId) {
        return EventAssignMarkController.getEventParticipant(eventId);

    }
    
    
    @GET
    @Path("/getEventAssignMarksDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventAssignMarksDetails(@QueryParam("userId") String userId,
            @QueryParam("eventId") int eventId) {
        return EventAssignMarkController.getEventAssignMarksDetails(userId,eventId);

    }
    
    
    
    @GET
    @Path("/getEventAssignNonSalesRule")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventAssignNonSalesRule(@QueryParam("userId") String userId,
            @QueryParam("eventId") int eventId) {
        return EventAssignMarkController.getEventAssignNonSalesRule(userId,eventId);

    }

}
