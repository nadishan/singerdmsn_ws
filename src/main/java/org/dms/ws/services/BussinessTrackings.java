/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.DSRTrakingController;
import org.dms.ws.singer.entities.DSRTrack;

/**
 *
 * @author SDU
 */
@Path("/BussinessTrackings")
public class BussinessTrackings {

    @GET
    @Path("/getBussinessTrackingList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessTrackingList(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("shops") @DefaultValue("0") int shops,
            @QueryParam("subdeler") @DefaultValue("0") int subdeler,
            @QueryParam("distributor") @DefaultValue("0") int distributor,
            @QueryParam("service") @DefaultValue("0") int service) {
        return DSRTrakingController.getBussinessTrackingList(bisId,shops, subdeler, distributor, service);

    }
    
    
    @GET
    @Path("/getTrackingEnableList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getTrackingEnableList(@QueryParam("bisId") int bisId,
            @QueryParam("bisTypeId") int bisTypeId) {
        return DSRTrakingController.getTrackingEnableList(bisId,bisTypeId);

    }

    
    
    @POST
    @Path("/addEnableTracking")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addEnableTracking(ArrayList<DSRTrack> trackingList,            
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){
      return Response.status(201).entity(DSRTrakingController.addEnableTracking(trackingList,userId,room,department,branch,countryCode,division,organization)).build();
    }
    
    
    
    @POST
    @Path("/addLocationTracking")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addLocationTracking(DSRTrack trackingList,            
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){
      return Response.status(201).entity(DSRTrakingController.addLocationTracking(trackingList,userId,room,department,branch,countryCode,division,organization)).build();
    }
    
    
    
    @GET
    @Path("/getTrackingHistoryList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getTrackingHistoryList(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("userId") @DefaultValue("all") String userId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limit) {
        return DSRTrakingController.getTrackingHistoryList(bisId, fromDate, toDate,userId, start, limit);

    }
    
    
    @GET
    @Path("/getLastTrackingDSRList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getLastTrackingDSRList(@QueryParam("bisId") int bisId,
            @QueryParam("bisTypeId") int bisTypeId) {
        return DSRTrakingController.getLastTrackingDSRList(bisId, bisTypeId);

    }
    
    
    
}
