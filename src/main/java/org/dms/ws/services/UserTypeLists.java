/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.UserTypeListController;

/**
 *
 * @author SDU
 */
@Path("/UserTypeLists")
public class UserTypeLists {

    @GET
    @Path("/getUserTypeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getUserTypeList(@QueryParam("userId") String userId,
            @QueryParam("bisType")  int bisType) {
        return UserTypeListController.getUserTypeList(userId, bisType);
    }
    
    
    @GET
    @POST
    @Path("/getUserTypeListLowUpper")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getUserTypeListLowUpper(@QueryParam("userId") String userId, @QueryParam("bisName") @DefaultValue("all") String bisName) {
        return UserTypeListController.getUserTypeListLowUpper(userId, bisName);
    }
    
    
    @GET
    @Path("/getUpperLevelList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getUpperLevelList(@QueryParam("bisId") int bisId,
            @QueryParam("bisType")  int bisType) {
        return UserTypeListController.getUpperLevelList(bisId, bisType);
    }
    
    
    
    @GET
    @Path("/getBussinessTypeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessTypeList(@QueryParam("bisType")  int bisType) {
        return UserTypeListController.getBussinessTypeList(bisType);
    }

    @GET
    @Path("/getLowLevelBussinessList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getLowLevelBussinessList(@QueryParam("bisType")  int bisType,
            @QueryParam("bisId")  int bisId) {
        return UserTypeListController.getLowLevelBussinessList(bisType,bisId);
    }
    
    
    @GET
    @Path("/getBussinessAvilableTypeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessAvilableTypeList(@QueryParam("bisId")  int bisId,
            @QueryParam("bisType")  int bisType) {
        return UserTypeListController.getBussinessAvilableTypeList(bisId,bisType);
    }
    
    
    @GET
    @Path("/getBusinessDownList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBusinessDownList(@QueryParam("bisId")  int bisId,
            @QueryParam("bisType")  int bisType) {
        return UserTypeListController.getBusinessDownList(bisId,bisType);
    }
    
    
    @GET
    @Path("/getBusinessDownListWithoutBussCategory")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBusinessDownListWithoutBussCategory(@QueryParam("bisId")  int bisId) {
        return UserTypeListController.getBusinessDownListWithoutBussCategory(bisId);
    }
    
    
    @GET
    @Path("/getBusinessDownTypeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBusinessDownTypeList(@QueryParam("bisId")  int bisId,
            @QueryParam("bisType")  int bisType) {
        return UserTypeListController.getBusinessDownTypeList(bisId,bisType);
    }
    
    
    @GET
    @Path("/getUpperBusinessLevel")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getUpperBusinessLevel(@QueryParam("bisId") int bisId,
            @QueryParam("bisType")  int bisType) {
        return UserTypeListController.getUpperBusinessLevel(bisId, bisType);
    }
    
     @GET
    @Path("/getBisIdUserList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBisIdUserList(@QueryParam("bisId")  int bisId) {
        return UserTypeListController.getBisIdUserList(bisId);
    }
    
}
