/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.BussStrucController;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.User;

/**
 *
 * @author SDU
 */
@Path("/BussinessStructures")
public class BussinessStructures {

    @GET
    @Path("/getBussinessStructure")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessStructureDetails(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("userCnt") @DefaultValue("0") int userCnt) {
        return BussStrucController.getBussinessStructure(bisId, userCnt);
    }
    
    @GET
    @Path("/getBussinessStructureServiceCenters")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessStructureServiceCenters(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("userCnt") @DefaultValue("0") int userCnt) {
        return BussStrucController.getBussinessStructureServiceCenters(bisId, userCnt);
    }
    
    
    @GET
    @Path("/getBussinessStructureParticulerBisId")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessStructureParticulerBisId(@QueryParam("bisId") @DefaultValue("0") int bisId) {
        return BussStrucController.getBussinessStructureParticulerBisId(bisId);
    }

    @PUT
    @Path("/addBussinesStructure")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addBussinesStructure(BussinessStructure bsList,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(BussStrucController.addBussinessStructure(bsList, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getBussinessStructureDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessStructureDetail(@QueryParam("bisId") int bisId) {
        return BussStrucController.getBussinessStructureDetail(bisId);
    }

    @GET
    @Path("/getBussinessUsers")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessUsers(@QueryParam("bisId") @DefaultValue("0") int bisId) {
        return BussStrucController.getBussinessUsers(bisId);
    }

    @POST
    @Path("/addBussinessUsers")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addBussinessUsers(ArrayList<User> usList,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(BussStrucController.addBussinessUsers(usList, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editBussinessStructure")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editBussinessStructure(BussinessStructure bsList,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(BussStrucController.editBussinessStructure(bsList, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getBussinessAsignList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessAsignList(@QueryParam("structType") @DefaultValue("0") String structType) {
        return BussStrucController.getBussinessAsignList(structType);
    }

    @GET
    @Path("/getBussinessName")
    @Produces({MediaType.APPLICATION_JSON})
    public String getBussinessName(@QueryParam("bisId") String bisId) {
        return BussStrucController.getBussinessName(bisId);
    }

    
    @GET
    @Path("/getUserList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getUserList(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("userCnt") @DefaultValue("0") int userCnt) {
        return BussStrucController.getUserList(bisId, userCnt);
    }
    
}
