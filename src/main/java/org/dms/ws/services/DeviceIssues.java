/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.DeviceIssueController;
import org.dms.ws.singer.entities.DSRReportEntity;
import org.dms.ws.singer.entities.DeviceIssue;

/**
 *
 * @author SDU
 */
@Path("/DeviceIssues")
public class DeviceIssues {

    @GET
    @Path("/getDeviceIssueList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceIssueList(@QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("distributerID") @DefaultValue("0") int distributerID,
            @QueryParam("dSRId") @DefaultValue("0") int dSRId,
            @QueryParam("issueDate") @DefaultValue("all") String issueDate,
            @QueryParam("price") @DefaultValue("0") double price,
            @QueryParam("distributorMargin") @DefaultValue("0") double distributorMargin,
            @QueryParam("delerMargin") @DefaultValue("0") double delerMargin,
            @QueryParam("discount") @DefaultValue("0") double discount,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return DeviceIssueController.getDeviceIssueList(issueNo, distributerID, dSRId, issueDate, price, distributorMargin, delerMargin, discount, status, order, type, start, limt);

    }

    @PUT
    @Path("/addDeviceIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addDeviceIssue(DeviceIssue di,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceIssueController.addDeviceIssue(di, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDeviceIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceIssue(DeviceIssue di,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceIssueController.editDeviceIssue(di, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getDeviceIssueDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceIssueDetail(@QueryParam("issueNo") @DefaultValue("0") int issueNo) {

        return DeviceIssueController.getDeviceIssueDetail(issueNo);

    }

    @GET
    @Path("/getModelWiseDSRDeviceIssue")
    @Produces({MediaType.APPLICATION_JSON})
    public ArrayList<DSRReportEntity> getModelWiseDSRDeviceIssue(@QueryParam("issueNo") int issueNo) {
        return DeviceIssueController.getModelWiseDSRDeviceIssue(issueNo);
    }

}
