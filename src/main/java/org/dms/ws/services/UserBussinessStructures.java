/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.UserBusinessStructuresController;
import org.dms.ws.singer.entities.BSUserMapper;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserBusinessStructure;

/**
 *
 * @author SDU
 */
@Path("/UserBussinessStructures")
public class UserBussinessStructures {
    
    @GET
    @Path("/getUserBusinessStructures")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getUserBusinessStructures(@QueryParam("bisId") @DefaultValue("0") int bisId){
        return UserBusinessStructuresController.getUserBusinessStructures(bisId);
    }
    
//    @PUT
//    @Path("/addUserBusinessStructure")
//    @Consumes({MediaType.APPLICATION_JSON})
//    @Produces({MediaType.APPLICATION_JSON})
//    public Response addUserBusinessStructure(UserBusinessStructure bsList){
//        return Response.status(201).entity(UserBusinessStructuresController.addUserBusinessStructure(bsList)).build();
//    }
    
    //////////////////////////////////////////Add Users to LDAP and Front DB/////////////////////////////////////
    
//    @PUT
//    @Path("/addUserBusinessStructure")
//    @Consumes({MediaType.APPLICATION_JSON})
//    @Produces({MediaType.APPLICATION_JSON})
//    public Response addUserBusinessStructure(ArrayList<User> bsList){
//        return Response.status(201).entity(UserBusinessStructuresController.addBusinessStructureUser_LDAP_DB(bsList)).build();
//    }
    
    
    
    
    @PUT
    @Path("/editBusinessStructureUser_LDAP_DB")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editBusinessStructureUser_LDAP_DB(BSUserMapper bsum,
            @HeaderParam("userId")  String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode")  String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){   
        return Response.status(201).entity(UserBusinessStructuresController.editBusinessStructureUser_LDAP_DB(bsum,userId,room,department,branch,countryCode,division,organization,system)).build();
        
        
    }
    
    
    
}
