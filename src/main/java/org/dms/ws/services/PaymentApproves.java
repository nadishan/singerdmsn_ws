/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WorkOrderApproveController;
import org.dms.ws.singer.entities.PaymentApprove;
import org.dms.ws.singer.entities.WorkOrderPaymentDetail;

/**
 *
 * @author SDU
 */
@Path("/PaymentApproves")
public class PaymentApproves {

   @GET
    @Path("/getApprovePaymentList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getApprovePaymentList(@QueryParam("setviceName") @DefaultValue("all") String setviceName,
            @QueryParam("paymentPrice") @DefaultValue("0") double paymentPrice,
            @QueryParam("paymentId") @DefaultValue("0") int paymentId,
            @QueryParam("refDate") @DefaultValue("all") String refDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("tax") @DefaultValue("0") int tax,
            @QueryParam("essdAccountNo") @DefaultValue("all") String essdAccountNo,
            @QueryParam("poNumber") @DefaultValue("all") String poNumber,
            @QueryParam("poDate") @DefaultValue("all") String poDate,
            @QueryParam("settleAmount") @DefaultValue("0") int settleAmount,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("chequeNo") @DefaultValue("all") String chequeNo,
            @QueryParam("chequeDetails") @DefaultValue("all") String chequeDetails,
            @QueryParam("chequeDate") @DefaultValue("all") String chequeDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WorkOrderApproveController.getApprovePaymentList(setviceName, paymentPrice, paymentId, refDate, status,tax,essdAccountNo,poNumber,poDate,settleAmount,remarks,chequeNo,chequeDetails,chequeDate, order, type, start, limt);

    }


    
//    //new
//    @PUT
//    @Path("/addWorkOrderGridUpdate")
//    @Produces({MediaType.APPLICATION_JSON})
//    @Consumes({MediaType.APPLICATION_JSON})
//    public MessageWrapper  addWorkOrderGridUpdate(String workOrderNo,int paymentId,int deductionAmount)
//            
//    
//    {
//
//        return WorkOrderApproveController.addWorkOrderGridUpdate(workOrderNo, paymentId, deductionAmount);
//
//    }
    
    //2
    @GET
    @Path("/addWorkOrderGridUpdate")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public MessageWrapper  addWorkOrderGridUpdate(@QueryParam("workOrderNo") @DefaultValue("all")String workOrderNo,
            @QueryParam("paymentId") @DefaultValue("0") int paymentId,
            @QueryParam("deductionAmount") @DefaultValue("0") int deductionAmount,
            @QueryParam("nbt") @DefaultValue("0") double nbt,
            @QueryParam("vat") @DefaultValue("0") double vat,
            @QueryParam("currentDeduction") @DefaultValue("0") double currentDeduction,
            @QueryParam("status") @DefaultValue("0") int status)
            
    
    {

        return WorkOrderApproveController.addWorkOrderGridUpdate(workOrderNo, paymentId, deductionAmount,nbt,vat,currentDeduction,status);

    }
    

    
    @GET
    @Path("/getApprovePaymentDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getApprovePaymentDetails(@QueryParam("paymentId") @DefaultValue("0") int paymentId,
             @QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
             @QueryParam("warrentyTypeDesc") @DefaultValue("all") String warrentyVrifType,
             @QueryParam("imeiNo") @DefaultValue("all") String imeiNo)
    
    {
        return WorkOrderApproveController.getApprovePaymentDetails(paymentId,workOrderNo,warrentyVrifType,imeiNo);

    }

    @POST
    @Path("/editApprovePayment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editApprovePayment(WorkOrderPaymentDetail wop,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderApproveController.editApprovePayment(wop, userId, room, department, branch, countryCode, division, organization)).build();
    }

    //new3
    @POST
    @Path("/addApprovePayment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addApprovePayment(PaymentApprove wop,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("delayDays") int delayDays,
            @HeaderParam("deductionAmount") int deductionAmount,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderApproveController.addApprovePayment(wop, userId, room, department, branch, countryCode, division,delayDays,deductionAmount, organization)).build();
    }

    @POST
    @Path("/addSettleAmount")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addSettleAmount(PaymentApprove wop,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderApproveController.addSettleAmount(wop, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/addChequeDetails")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addChequeDetails(PaymentApprove wop,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderApproveController.addChequeDetails(wop, userId, room, department, branch, countryCode, division, organization)).build();
    }

    
    
    @GET
    @Path("/getApprovePaymentHeader")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getApprovePaymentHeader(@QueryParam("paymentId") @DefaultValue("0") int paymentId) {
        return WorkOrderApproveController.getApprovePaymentHeader(paymentId);

    }
    
    
}
