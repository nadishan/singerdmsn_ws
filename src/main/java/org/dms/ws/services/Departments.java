/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.singer.controllers.DepartmentController;

/**
 *
 * @author SDU
 */
@Path("/Departments")
public class Departments {
    
    @GET
    @Path("/getDepartmentList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDepartmentList(){
        return DepartmentController.getDepartmentList();
    }
    
}
