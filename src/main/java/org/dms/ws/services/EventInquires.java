/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.EventInquireController;

/**
 *
 * @author SDU
 */
@Path("/EventInquires")
public class EventInquires {

    @GET
    @Path("/getEventInqurieList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventInqurieList(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("eventName") @DefaultValue("all") String eventName,
            @QueryParam("eventDesc") @DefaultValue("all") String eventDesc,
            @QueryParam("startDate") @DefaultValue("all") String startDate,
            @QueryParam("endDate") @DefaultValue("all") String endDate,
            @QueryParam("threshold") @DefaultValue("all") String threshold,
            @QueryParam("eventAchievement") @DefaultValue("all") String eventAchievement,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return EventInquireController.getEventInqurieList(eventId, eventName, eventDesc, startDate, endDate, threshold, eventAchievement, order, type, start, limt);

    }

    
    @GET
    @Path("/getEventMasterDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getEventMasterDetails(@QueryParam("eventId") int eventId) {
        return EventInquireController.getEventMasterDetails(eventId);

    } 
    
    
    
    @GET
    @Path("/getParticipentList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getParticipentList(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("paticipantName") @DefaultValue("all") String paticipantName,
            @QueryParam("location") @DefaultValue("all") String location,
            @QueryParam("paticipantType") @DefaultValue("all") String paticipantType,
            @QueryParam("achivePoint") @DefaultValue("all") String achivePoint,
            @QueryParam("posotion") @DefaultValue("all") String posotion,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return EventInquireController.getParticipentList(eventId, paticipantName, location, paticipantType, achivePoint, posotion, order, type, start, limt);

    }
    
}
