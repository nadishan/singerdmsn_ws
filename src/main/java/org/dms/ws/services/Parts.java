/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.PartContraoller;
import org.dms.ws.singer.entities.Part;

/**
 *
 * @author SDU
 */
@Path("/Parts")
public class Parts {

    @GET
    @Path("/getPartsDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getPartsDetails(@QueryParam("partNo") @DefaultValue("0") int partno,
            @QueryParam("partDesc") @DefaultValue("all") String partdesc,
            @QueryParam("partExist") @DefaultValue("0") int partexist,
            @QueryParam("partPrdCode") @DefaultValue("all") String partprdcode,
            @QueryParam("partPrdDesc") @DefaultValue("all") String partprddesc,
            @QueryParam("partPrdFamily") @DefaultValue("all") String partfamily,
            @QueryParam("partPrdFamilyDesc") @DefaultValue("all") String partfamilydesc,
            @QueryParam("partGroup1") @DefaultValue("all") String partgroup1,
            @QueryParam("partGroup2") @DefaultValue("all") String partgroup2,
            @QueryParam("partSellPrice") @DefaultValue("0") double partsellprice,
            @QueryParam("partCost") @DefaultValue("0") double partcost,
            @QueryParam("erpPartNo") @DefaultValue("all") String erppartno,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return PartContraoller.getPartsDetails(partno, partdesc, partexist,
                partprdcode, partprddesc, partfamily,
                partfamilydesc, partgroup1, partgroup2,
                partsellprice, partcost,erppartno,order,type ,start, limt);

    }
    
    
    
    @POST
    @Path("/editParts")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editParts(Part parts,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(PartContraoller.editParts(parts, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
