/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Stack;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.websocket.Session;
import org.codehaus.jackson.map.ObjectMapper;
import org.dms.ws.singer.controllers.DbCon;
import static org.dms.ws.singer.controllers.DbCon.logger;

/**
 *
 * @author SDU
 */
//@Singleton
//@Startup
public class CronJob {

    //  public static Session socketSession;
    //   private static ObjectMapper objMapper;
    //   private static Stack<AccessRecord> tagEvtQueue;
    //   public static long lastSearchedIndex = 0;
//    public void TagEventSchedular() {
//        objMapper = new ObjectMapper();
//        tagEvtQueue = new Stack<>();
//        socketSession = null;
//        //System.out.println("Tag Event Watcher Dispatched.............");
//    }
//    public static void initWebSocketSessions(Session sessions) {
//        socketSession = sessions;
//    }
    int i = 0;

    @Schedule(second = "10/30", minute = "*", hour = "*", dayOfWeek = "*", persistent = false, timezone = "UTC")

    public void EventMarkAssignSheduler() {

        System.out.println("  " + (i + 1));

//        logger.info("EventMarkAssignSheduler Mehtod Call.......");
//        DbCon dbCon = new DbCon();
//        Connection con = dbCon.getCon();
//        CallableStatement pros;
//        try {
//           pros = con.prepareCall("{call EVENT_MARKS_ASIGN}");
//           pros.executeQuery();
//
//        } catch (Exception ex) {
//            logger.info("Error in addWorkOrder Mehtod  :" + ex.toString());
//            
//        }
//     //   ValidationWrapper vw = new ValidationWrapper();
//        
//        logger.info("Ready to Return Values..........");
//        dbCon.ConectionClose(con);
//        logger.info("-----------------------------------------------------------");
//        
    }

    public void ERPDebitNoteLoad() {

        logger.info("ERPDebitNoteLoad method Call..........");
        int max_debit_header = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_debit_Header = dbCon.search(con, "SELECT DEBIT_NOTE_NO,"
                    + "CUSTOMER_CODE,"
                    + "SHOP_CODE,"
                    + "MODEL_NO,"
                    + "ORDER_NO,"
                    + "DATE_ACCEPTED,"
                    + "USER_INSERTED "
                    + "FROM DEBIT_NOTE_HEADER "
                    + "WHERE DATE_ACCEPTED > (SELECT MAX(DATE_INSERTED) "
                    + "FROM SD_ACCEPT_DEBIT_NOTE_HEADER)");

            
            ResultSet rs_max_debitNo = dbCon.search(con, "SELECT NVL(MAX(ACCEPT_NOTE_NO),0) MAX "
                    + "FROM SD_ACCEPT_DEBIT_NOTE_HEADER");

            while (rs_max_debitNo.next()) {
                max_debit_header = rs_max_debitNo.getInt("MAX");
            }

            PreparedStatement ps_in_debit_hrd = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_DEBIT_NOTE_HEADER(ACCEPT_NOTE_NO,"
                    + "DEBIT_NOTE_NO,"
                    + "CUSTOMER_NO,"
                    + "ORDER_NO,"
                    + "DELIVERY_DATE,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED) "
                    + "VALUES(?,?,?,?,TO_DATE(?,'YYYY/MM/DD')?,USER,SYSDATE)");

            while (rs_debit_Header.next()) {
                max_debit_header = max_debit_header + 1;
                ps_in_debit_hrd.setInt(1, max_debit_header);
                ps_in_debit_hrd.setString(2, rs_debit_Header.getString("DEBIT_NOTE_NO"));
                ps_in_debit_hrd.setString(3, rs_debit_Header.getString("CUSTOMER_CODE"));
                ps_in_debit_hrd.setString(4, rs_debit_Header.getString("ORDER_NO"));
                ps_in_debit_hrd.setString(5, rs_debit_Header.getString("DATE_ACCEPTED"));
                ps_in_debit_hrd.addBatch();
            }
            ps_in_debit_hrd.executeBatch();

            
            ResultSet rs_debit_Details = dbCon.search(con, "SELECT DEBIT_NOTE_NO,"
                    + "IMEI_NO,"
                    + "CUSTOMER_CODE,"
                    + "SHOP_CODE,"
                    + "MODEL_NO,"
                    + "ORDER_NO,"
                    + "SALES_PRICE,"
                    + "DATE_ACCEPTED,"
                    + "USER_INSERTED "
                    + "FROM DEBIT_NOTES_DETAIL "
                    + "WHERE DEBIT_NOTE_NO IN (SELECT DEBIT_NOTE_NO "
                    + "FROM DEBIT_NOTE_HEADER "
                    + "WHERE DATE_ACCEPTED > (SELECT MAX(DATE_INSERTED) "
                    + "ROM SD_ACCEPT_DEBIT_NOTE_HEADER))");

            PreparedStatement ps_in_debit_dtl = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_DEBIT_NOTE_DETAIL(ACCEPT_NOTE_DETAILS_NO,"
                    + "ACCEPT_NOTE_NO,"
                    + "DEBIT_NOTE_NO,"
                    + "IMEI_NO,"
                    + "CUSTOMER_CODE,"
                    + "SHOP_CODE,"
                    + "MODEL_NO,"
                    + "ORDER_NO,"
                    + "SALES_PRICE,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,USER,SYSDATE)");

            int debit_dtl_seq = 0;

            while (rs_debit_Details.next()) {
                debit_dtl_seq = debit_dtl_seq + 1;
                ps_in_debit_dtl.setInt(1, debit_dtl_seq);
                ps_in_debit_dtl.setInt(2, 0);
                ps_in_debit_dtl.setString(3, rs_debit_Details.getString("DEBIT_NOTE_NO"));
                ps_in_debit_dtl.setString(4, rs_debit_Details.getString("IMEI_NO"));
                ps_in_debit_dtl.setString(5, rs_debit_Details.getString("CUSTOMER_CODE"));
                ps_in_debit_dtl.setString(6, rs_debit_Details.getString("SHOP_CODE"));
                ps_in_debit_dtl.setString(7, rs_debit_Details.getString("MODEL_NO"));
                ps_in_debit_dtl.setString(8, rs_debit_Details.getString("ORDER_NO"));
                ps_in_debit_dtl.setString(9, rs_debit_Details.getString("SALES_PRICE"));
                ps_in_debit_dtl.addBatch();
            }
            ps_in_debit_dtl.executeBatch();

        } catch (Exception ex) {
            logger.info("Error ERPDebitNoteLoad......:" + ex.toString());
        }
        logger.info("Data Load Done...........");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
    }

}
