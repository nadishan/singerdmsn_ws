/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WarrentyReplacementController;
import org.dms.ws.singer.entities.WarrenyReplacement;

/**
 *
 * @author SDU
 */
@Path("/WarrentyReplacements")
public class WarrentyReplacements {
    
    
    @GET
    @Path("/getWarrentyReplacement")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWarrentyReplacement(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("oldImei") @DefaultValue("all") String oldImei,
            @QueryParam("newImei") @DefaultValue("all") String newImei,
            @QueryParam("exRefNo") @DefaultValue("all") String exRefNo,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WarrentyReplacementController.getWarrentyReplacement(workOrderNo, oldImei, newImei, exRefNo, fromDate, toDate, order, type, start, limt);

    }

    

    @PUT
    @Path("/addWarrentyReplacement")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWarrentyReplacement(WarrenyReplacement wr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(201).entity(WarrentyReplacementController.addWarrentyReplacement(wr, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    
    
    @POST
    @Path("/cancleWarrentyReplacement")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response cancleWarrentyReplacement(WarrenyReplacement wr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {

        return Response.status(201).entity(WarrentyReplacementController.cancleWarrentyReplacement(wr, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
}
