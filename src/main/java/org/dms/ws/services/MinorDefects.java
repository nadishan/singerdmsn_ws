/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.MinorDefectController;
import org.dms.ws.singer.entities.MinorDefect;

/**
 *
 * @author SDU
 */
@Path("/MinorDefects")
public class MinorDefects {

    @GET
    @Path("/getMinorDefects")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getMinorDefects(@QueryParam("minDefectCode") @DefaultValue("0") int mindefectcode,
            @QueryParam("minDefectDesc") @DefaultValue("all") String mindefectdesc,
            @QueryParam("mrjDefectCode") @DefaultValue("0") int mjrdefectcode,
            @QueryParam("minDefectStatus") @DefaultValue("0") int mindefectstatus,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return MinorDefectController.getMinorDefects(mindefectcode, mindefectdesc, mjrdefectcode, mindefectstatus,order,type ,start, limit);

    }

    @PUT
    @Path("/addMinorDefect")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addMinorDefect(MinorDefect md,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(MinorDefectController.addMinorDefect(md, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editMinorDefect")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editMinorDefect(MinorDefect md,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(MinorDefectController.editMinorDefect(md, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
