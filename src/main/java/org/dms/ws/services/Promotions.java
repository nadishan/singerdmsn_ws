/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.PromotionController;

/**
 *
 * @author SDU
 */
@Path("/Promotions")
public class Promotions {
    
    
    @GET
    @Path("/getPromotionList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getPromotionList(@QueryParam("promotionId") @DefaultValue("0") int promotionId,
            @QueryParam("promotionName") @DefaultValue("all") String promotionName,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return PromotionController.getPromotionList(promotionId, promotionName, start, limit);
    }
    
}
