/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.ShopVisitController;
import org.dms.ws.singer.controllers.SwapIMEIController;
import org.dms.ws.singer.entities.SwapIMEI;

/**
 *
 * @author Dasun Chathuranga
 */

@Path("/SwapIMEIService")
public class SwapIMEIService {
    @GET
    @Path("/validateIMEI")
    @Produces({MediaType.APPLICATION_JSON})
    public SwapIMEI getShopVisitList(@QueryParam("imei") String imei) {
        SwapIMEIController swap = new SwapIMEIController();
        return swap.getCurrentIMEiLocation(imei);
    }
    
    @GET
    @Path("/swapIMEI")
    @Produces({MediaType.APPLICATION_JSON})
    public int swapIMEI(@QueryParam("type") int type,
            @QueryParam("oldimei") String oldimei,
            @QueryParam("newimei") String newimei,
            @QueryParam("bisid") int bisid) {
        SwapIMEIController swap = new SwapIMEIController();
        return swap.changeIMEI(type, oldimei, newimei, bisid);
    }
}
