/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.CustomerController;

/**
 *
 * @author SDU
 */
@Path("/Customers")
public class Customers {

    @GET
    @Path("/getCustomerDetals")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getCustomerDetals(@QueryParam("customerNIC") String customerNIC) {

        return CustomerController.getCustomerDetals(customerNIC);
    }

    
    
    @GET
    @Path("/checkCustomerDetal")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper checkCustomerDetal(@QueryParam("customerNIC") String customerNIC) {

        return CustomerController.checkCustomerDetal(customerNIC);
    }
}
