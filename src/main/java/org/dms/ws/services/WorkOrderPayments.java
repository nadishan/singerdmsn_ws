/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WorkPaymentController;
import org.dms.ws.singer.entities.WorkOrderPayment;

/**
 *
 * @author SDU
 */
@Path("/WorkOrderPayments")
public class WorkOrderPayments {

    @GET
    @Path("/getWorkOrderPaymentList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderPaymentList(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("warrantyVerifType") @DefaultValue("all") String warrantyVerifType,
            @QueryParam("defectDesc") @DefaultValue("all") String defectDesc,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WorkPaymentController.getWorkOrderPaymentList(workOrderNo, imeiNo, warrantyVerifType, defectDesc, status, order, type, start, limt);

    }

    @GET
    @Path("/getWorkOrderPaymentListNew")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderPaymentList_New(@QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("paymentId") @DefaultValue("0") int paymentId,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("amount") @DefaultValue("0") double amount,
            @QueryParam("paymentDate") @DefaultValue("all") String paymentDate,
            @QueryParam("poNo") @DefaultValue("all") String poNo,
            @QueryParam("poDate") @DefaultValue("all") String poDate,
            @QueryParam("chequeNo") @DefaultValue("all") String chequeNo,
            @QueryParam("chequeDate") @DefaultValue("all") String chequeDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WorkPaymentController.getWorkOrderPaymentList_New(bisId, paymentId, amount, paymentDate, poNo, poDate, chequeNo, chequeDate, status, order, type, start, limt);

    }

    @GET
    @Path("/getWorkOrderPaymentLisToGrid")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderPaymentList_for_Grid(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("paymentId") @DefaultValue("0") int paymentId,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("amount") @DefaultValue("0") double amount,
            @QueryParam("paymentDate") @DefaultValue("all") String paymentDate,
            @QueryParam("poNo") @DefaultValue("all") String poNo,
            @QueryParam("poDate") @DefaultValue("all") String poDate,
            @QueryParam("chequeNo") @DefaultValue("all") String chequeNo,
            @QueryParam("chequeDate") @DefaultValue("all") String chequeDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt){

        return WorkPaymentController.getWorkOrderPaymentList_for_Grid(bisId, paymentId, amount, paymentDate, poNo, poDate, chequeNo, chequeDate, status, order, type, start, limt);

    }

    @GET
    @Path("/getWorkOrderPaymentListwithPrice")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderPaymentListwithPrice(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("warrentyTypeDesc") @DefaultValue("all") String warrentyTypeDesc,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("defectDesc") @DefaultValue("all") String defectDesc,
            @QueryParam("laborCost") @DefaultValue("0") double laborCost,
            @QueryParam("spareCost") @DefaultValue("0") double spareCost,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type) {

        return WorkPaymentController.getWorkOrderPaymentListwithPrice(workOrderNo, warrentyTypeDesc, imeiNo, defectDesc, laborCost, spareCost, bisId, order, type);

    }

    @GET
    @Path("/getWorkOrderPaymentGrid")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderPaymentGrid(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("paymentId") @DefaultValue("0") int paymentId,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt
    ) {

        return WorkPaymentController.getWorkOrderPaymentList_grid(bisId, paymentId, status, start, limt);

    }

    @PUT
    @Path("/addWorkOrderPayment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrderPayment(WorkOrderPayment wop,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkPaymentController.addWorkOrderPayment(wop, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getWorkOrderPaymentDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderPaymentDetails(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo) {
        return WorkPaymentController.getWorkOrderPaymentDetails(workOrderNo);

    }

}
