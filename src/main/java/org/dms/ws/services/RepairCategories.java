/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.RepairCategoryController;
import org.dms.ws.singer.entities.RepairCategory;

/**
 *
 * @author SDU
 */
@Path("/RepairCategories")
public class RepairCategories {
    
    @GET
    @Path("/getRapairCategories")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getRapairCategories(@QueryParam("reCateId") @DefaultValue("0") int reCateId,
            @QueryParam("reCateDesc") @DefaultValue("all") String reCatedesc,
            @QueryParam("reCateStatus") @DefaultValue("0") int reCateStatus,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("9999") int limit){
        
        return RepairCategoryController.getRapairCategories(reCateId, reCatedesc, reCateStatus,order,type,start, limit);
        
    }
    
    
    @PUT
    @Path("/addRepairCategory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addRepairCategory(RepairCategory rc,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,@HeaderParam("system") String system){
         return Response.status(201).entity(RepairCategoryController.addRepairCategory(rc,userId,room,department,branch,countryCode,division,organization)).build();
    }
    
    
    @POST
    @Path("/editRepairCategory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editCategory(RepairCategory rc,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,@HeaderParam("system") String system){
        return Response.status(201).entity(RepairCategoryController.editRepairCategory(rc,userId,room,department,branch,countryCode,division,organization)).build();
    }
    
    
}
