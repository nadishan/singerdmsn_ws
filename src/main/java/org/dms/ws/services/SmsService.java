/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.SmsController;

/**
 *
 * @author Shanka
 */
@Path("/SmsService")
public class SmsService {

    @GET
    @Path("/send")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public MessageWrapper sendSms(@QueryParam("number") String number, @QueryParam("msg") String msg) throws IOException {


        System.out.println("Message parse to controller as "+ number+" and message "+msg);
        SmsController sms = new SmsController();
        return sms.send(number,msg);
//return 1;
    }

}
