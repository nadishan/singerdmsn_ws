/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WarrantySchemeController;
import org.dms.ws.singer.entities.WarrantyScheme;

/**
 *
 * @author SDU
 */
@Path("/WarrantyScheme")
public class WarrantySchemes {

    @GET
    @Path("/getWarrantySchemeList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWarrantySchemeList(
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("schemeName") @DefaultValue("all") String schemeName,
            @QueryParam("period") @DefaultValue("0") int period,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WarrantySchemeController.getWarrantySchemeList(schemeId, schemeName, period,status,order,type ,start, limt);

    }

    
    
    @GET
    @Path("/getWarrantyDays")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper getWarrantyDays(@QueryParam("imeiNo")  String imeiNo,
            @QueryParam("dateOfSale") String dateOfSale) {

        return WarrantySchemeController.getWarrantyDays(imeiNo, dateOfSale);

    }
    
    
    @GET
    @Path("/getImeiMasterDetailswithWarranty")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiMasterDetailswithWarranty(@QueryParam("imeiNo")  String imeiNo) {

        return WarrantySchemeController.getImeiMasterDetailswithWarranty(imeiNo);

    }
    
    
    @GET
    @Path("/getImeiMasterDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiMasterDetails(@QueryParam("imeiNo")  String imeiNo,
            @QueryParam("warrentyType") int warrentyType) {

        return WarrantySchemeController.getImeiMasterDetails(imeiNo, warrentyType);

    }
    
    
    @PUT
    @Path("/addWarrantySchema")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWarrantySchema(WarrantyScheme ws,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(WarrantySchemeController.addWarrantySchema(ws, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    
    @POST
    @Path("/editWarrantySchema")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWarrantySchema(WarrantyScheme ws,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(WarrantySchemeController.editWarrantySchema(ws, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
}
