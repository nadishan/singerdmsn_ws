/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.CreditNoteIssueController;
import org.dms.ws.singer.entities.CreditNoteIssue;

/**
 *
 * @author SDU
 */
@Path("/CreditNoteIssues")
public class CreditNoteIssues {

    @GET
    @Path("/getCreditNoteIssueDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getCreditNoteIssueDetails(@QueryParam("creditNoteIssueNo") @DefaultValue("0") int creditNoteIssueNo,
            @QueryParam("debitNoteNo") @DefaultValue("all") String debitNoteNo,
            @QueryParam("distributerId") @DefaultValue("0") int distributerId,
            @QueryParam("issueDate") @DefaultValue("all") String issueDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return CreditNoteIssueController.getCreditNoteIssueDetails(creditNoteIssueNo, debitNoteNo, distributerId, issueDate, status,order,type ,start, limt);

    }

    @PUT
    @Path("/addCreditNoteIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addCreditNoteIssue(CreditNoteIssue cni,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(CreditNoteIssueController.addCreditNoteIssue(cni, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editCreditNoteIssue")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editCreditNoteIssue(CreditNoteIssue cni,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(CreditNoteIssueController.editCreditNoteIssue(cni, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getCreditNoteDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getCreditNoteIssueDetail(@QueryParam("creditNoteIssueNo") @DefaultValue("0") int creditNoteIssueNo) {
        return CreditNoteIssueController.getCreditNoteIssueDetail(creditNoteIssueNo);

    }

    @GET
    @Path("/checkImeiReplacementReturn")
    @Produces({MediaType.APPLICATION_JSON})
    public int checkImeiReplacementReturn(
            @QueryParam("imei") String imei) {
        return CreditNoteIssueController.checkImeiReplacementReturn(imei);
    }
}
