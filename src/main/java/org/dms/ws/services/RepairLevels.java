/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.LevelContraller;
import org.dms.ws.singer.entities.RepairLevel;

/**
 *
 * @author SDU
 */
//@Path("addLevel(RepairLevel rl)")
@Path("/addLevel")
public class RepairLevels {

    @GET
    @Path("/getLevels")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getLevels(@QueryParam("levelId") @DefaultValue("0") int levelid,
            @QueryParam("levelCode") @DefaultValue("all") String levelcode,
            @QueryParam("levelName") @DefaultValue("all") String levelname,
            @QueryParam("levelDesc") @DefaultValue("all") String levledesc,
            @QueryParam("levelCatId") @DefaultValue("0") int levelcateid,
            @QueryParam("levelPrice") @DefaultValue("0") double levelprice,
            @QueryParam("levelStatus") @DefaultValue("0") int levelstatus,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return LevelContraller.getLevels(levelid, levelcode, levelname, levledesc, levelcateid, levelprice, levelstatus,order,type,start, limit);

    }

    @PUT
    @Path("/addLevel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addLevel(RepairLevel rl,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(LevelContraller.addLevel(rl, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editLevel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editLevel(RepairLevel rl,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(LevelContraller.editLevel(rl, userId, room, department, branch, countryCode, division, organization)).build();
    }

    
    @GET
    @Path("/getRepairLevelsForImei")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getRepairLevelsForImei(@QueryParam("imie_no") @DefaultValue("all") String imie_no) {

        return LevelContraller.getRepairLevelsForImei(imie_no);

    }
    
}
