/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WorkOrderTransferController;
import org.dms.ws.singer.entities.WorkOrderTransfer;

/**
 *
 * @author SDU
 */
@Path("/WorkOrderTransfers")
public class WorkOrderTransfers {

    @GET
    @Path("/getWorkOrderTransfer")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderTransfer(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("bisName") @DefaultValue("all") String bisName,
            @QueryParam("bis_id") @DefaultValue("0") int bis_id,
            @QueryParam("transferDate") @DefaultValue("all") String transferDate,
            @QueryParam("transferReason") @DefaultValue("all") String transferReason,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("courierEmail") @DefaultValue("all") String courierEmail,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("transferLocationDesc") @DefaultValue("all") String transferLocationDesc,
            @QueryParam("tansferLocationId") @DefaultValue("0") int tansferLocationId,
            @QueryParam("returnDate") @DefaultValue("all") String returnDate,
            @QueryParam("returnReason") @DefaultValue("all") String returnReason,
            @QueryParam("returnDeleveryDetails") @DefaultValue("all") String returnDeleveryDetails,
            @QueryParam("deleveryTypeDesc") @DefaultValue("all") String deleveryTypeDesc,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WorkOrderTransferController.getWorkOrderTransfer(workOrderNo, bisName,bis_id, transferDate, transferReason, deleveryType, courierEmail, status,transferLocationDesc,tansferLocationId,returnDate,returnReason,returnDeleveryDetails, deleveryTypeDesc, order, type, start, limt);

    }

    @GET
    @Path("/verifyWorkOrder")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper verifyWorkOrder(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo) {
        return WorkOrderTransferController.verifyWorkOrder(workOrderNo);

    }

    @GET
    @Path("/getWorkOrderToTransfer")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderToTransfer(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId) {
        return WorkOrderTransferController.getWorkOrderToTransfer(workOrderNo,bisId);

    }
    
    
    @GET
    @Path("/getWorkOrderTransferStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderTransferStatus() {
        return WorkOrderTransferController.getWorkOrderTransferStatus();

    }
    

    @PUT
    @Path("/addWorkOrderTransfer")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrderTransfer(WorkOrderTransfer wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderTransferController.addWorkOrderTransfer(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editWorkOrderTransfer")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWorkOrderTransfer(WorkOrderTransfer wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderTransferController.editWorkOrderTransfer(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }

    
    @POST
    @Path("/returnWorkOrderTransfer")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response returnWorkOrderTransfer(WorkOrderTransfer wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderTransferController.returnWorkOrderTransfer(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    
    @POST
    @Path("/deleteWorkOrderTransfer")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteWorkOrderTransfer(WorkOrderTransfer wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderTransferController.deleteWorkOrderTransfer(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
