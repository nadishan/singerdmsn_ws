/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.BussinessTypeController;

/**
 *
 * @author SDU
 */
@Path("/BussinessTypes")
public class BussinessTypes {
    
    
    @GET
    @Path("/getBussinessType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessType(@QueryParam("bisTypeId") @DefaultValue("0") int bisTypeId,
            @QueryParam("bisTypeDesc") @DefaultValue("all") String bisTypeDesc,
            @QueryParam("entyMode") @DefaultValue("0") int entyMode,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return BussinessTypeController.getBussinessType(bisTypeId, bisTypeDesc, entyMode, status, order, type, start, limt);

    }
    
}
