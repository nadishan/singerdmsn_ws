/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WarrantyAdjustmentController;
import org.dms.ws.singer.entities.WarrantyAdjustment;

/**
 *
 * @author SDU
 */
@Path("/WarrantyAdjustment")
public class WarrantyAdjustments {

    @GET
    @Path("/getWarrantyAdjustmentDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getCleartransitlDetails(
            @QueryParam("iMEI") @DefaultValue("all") String IMEI,
            @QueryParam("extendedDays") @DefaultValue("0") int extendedDays,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WarrantyAdjustmentController.getWarrantyAdjustmentDetails(IMEI, extendedDays,order,type ,start, limt);

    }

    @PUT
    @Path("/addWarrantyAdjustment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWarrantyAdjustment(WarrantyAdjustment warrantyAdjustment,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(WarrantyAdjustmentController.addWarrantyAdjustment(warrantyAdjustment, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/EditWarrantyAdjustment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWarrantyAdjustment(WarrantyAdjustment warrantyAdjustment,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(WarrantyAdjustmentController.editWarrantyAdjustment(warrantyAdjustment, userId, room, department, branch, countryCode, division, organization)).build();
    }

    
    
    
    @POST
    @Path("/deleteWarrantyAdjustment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteWarrantyAdjustment(WarrantyAdjustment warrantyAdjustment,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization) {
        return Response.status(201).entity(WarrantyAdjustmentController.deleteWarrantyAdjustment(warrantyAdjustment, userId, room, department, branch, countryCode, division, organization)).build();
    }
}
