/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.AcceptDebitNoteController;
import org.dms.ws.singer.entities.AcceptInventory;
import org.dms.ws.singer.entities.ImportDebitNote;

/**
 *
 * @author SDU
 */
@Path("/AcceptDebitNotes")
public class AcceptDebitNotes {

    @GET
    @Path("/getAcceptDebitNote")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getAcceptDebitNote(@QueryParam("debitNoteNo") @DefaultValue("all") String debitNoteNo,
            @QueryParam("orderNo") @DefaultValue("all") String orderNo,
            @QueryParam("invoiceNo") @DefaultValue("all") String invoiceNo,
            @QueryParam("contactNo") @DefaultValue("all") String contactNo,
            @QueryParam("partNo") @DefaultValue("all") String partNo,
            @QueryParam("orderQty") @DefaultValue("0") int orderQty,
            @QueryParam("deleveryQty") @DefaultValue("0") int deleveryQty,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("headStatus") @DefaultValue("0") int headStatus,
            @QueryParam("customerNo") @DefaultValue("all") String customerNo,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return AcceptDebitNoteController.getAcceptDebitNote(debitNoteNo, orderNo, invoiceNo, contactNo, partNo, orderQty, deleveryQty, deleveryDate, headStatus, customerNo, status, bisId, order, type, start, limt);

    }

    @GET
    @Path("/getAcceptDebitNoteDetial")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getAcceptDebitNoteDetial(@QueryParam("debitNoteNo") String debitNoteNo) {
        return AcceptDebitNoteController.getAcceptDebitNoteDetial(debitNoteNo);

    }

    @GET
    @Path("/getAcceptDebitNoteDetialOnly")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getAcceptDebitNoteDetialOnly(@QueryParam("debitNoteNo") String debitNoteNo,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {
        return AcceptDebitNoteController.getAcceptDebitNoteDetialOnly(debitNoteNo, start, limt);

    }

    @PUT
    @Path("/addacceptDebitDetials")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addacceptDebitDetials(ImportDebitNote im,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
       

        return Response.status(201).entity(AcceptDebitNoteController.addacceptDebitDetials(im, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @PUT
    @Path("/addacceptDebitDetialsAll")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addacceptDebitDetialsAll(ImportDebitNote im,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(201).entity(AcceptDebitNoteController.addacceptDebitDetialsAll(im, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/deleteImeiMaster")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteImeiMaster(String imeiNo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(201).entity(AcceptDebitNoteController.deleteImeiMaster(imeiNo, userId, room, department, branch, countryCode, division, organization)).build();
    }
}
