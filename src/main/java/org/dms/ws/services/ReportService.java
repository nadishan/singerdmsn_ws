/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.singer.controllers.ReportController;
import org.dms.ws.singer.entities.ReadExcel;

/**
 *
 * @author Dasun Chathuranga
 */
@Path("/reportService")
public class ReportService {
    
    public static String excelPath() {
        String pdf = "";
        try {
            Properties properties = new Properties();
            String jbossPath = System.getenv("WS_HOME");

            String pathSep = jbossPath.replace("\\", "/");
            String ipPath = pathSep + "/SINDMA_Conn.properties";
            properties.load(new FileInputStream(ipPath));

            pdf = properties.getProperty("EXCEL");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println("EXCEL Path: " + pdf);

        return pdf;
    }

    private static final String FILE_PATH_USERTASKS_EXCEL = excelPath() + "/report.xls";
    private ReadExcel ReadExcel;

    @GET
    @Path("/readReport")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper readReport() {
        ResponseWrapper response = new ResponseWrapper();
        ReportController readRepo = new ReportController();
        return readRepo.getReportDetailsFromDB(response);
    }

    @GET
    @Path("/usertasksexcel")
    @Produces({"application/xls"})
    public Response getUserTasks_Excel(
                        @QueryParam("query") String query, 
                        @QueryParam("reportName") String reportName, 
                        @QueryParam("condition1") @DefaultValue("0") String condition1,
                        @QueryParam("condition2") @DefaultValue("0") String condition2,
                        @QueryParam("condition3") @DefaultValue("0") String condition3,
                        @QueryParam("condition4") @DefaultValue("0") String condition4,
                        @QueryParam("condition5") @DefaultValue("0") String condition5,
                        @QueryParam("condition6") @DefaultValue("0") String condition6) {

       
     //   System.out.println("QUERY>>>>"+ query);
        
        ReportController repoToExcel = new ReportController();
        repoToExcel.getData(query,condition1, condition2, condition3, condition4, condition5, condition6);
        
        File file = new File(FILE_PATH_USERTASKS_EXCEL);

        Response.ResponseBuilder response = Response.ok(file);
        response.header("Content-Disposition",
                "attachment; filename=" + reportName + ".xls");
        
        
        
        return response.build();
    }

    @PUT
    @Path("/addexcelreport")
    @Produces({MediaType.APPLICATION_JSON})
    public int putExcelreport(ReadExcel readExcel) {
        ResponseWrapper response = new ResponseWrapper();
        return ReportController.insertReportDetails(readExcel);
    }

    @POST
    @Path("/updateexcelreport")
    @Produces({MediaType.APPLICATION_JSON})
    public int updateExcelReport(ReadExcel readExcel) {
        ResponseWrapper response = new ResponseWrapper();
        return ReportController.updateReportDetails(readExcel);
    }
    
    
    
}
