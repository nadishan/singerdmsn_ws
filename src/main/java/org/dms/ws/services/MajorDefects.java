/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.MajorDefectController;
import org.dms.ws.singer.entities.MajorDefect;

/**
 *
 * @author SDU
 */
@Path("/MajorDefects")
public class MajorDefects {

    @GET
    @Path("/getMajorDefects")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getMajorDefects(@QueryParam("mjrDefectCode") @DefaultValue("0") int mjrdefectcode,
            @QueryParam("mjrDefectDesc") @DefaultValue("all") String mjrdefectdesc,
            @QueryParam("mjrDefectStatus") @DefaultValue("0") int mjrdefectstatus,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return MajorDefectController.getMajorDefects(mjrdefectcode, mjrdefectdesc, mjrdefectstatus,order,type,start, limit);

    }

    @PUT
    @Path("/addMajorDefect")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addMajorDefect(MajorDefect md,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(MajorDefectController.addMajorDefect(md, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editMajorDefect")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editMajorDefect(MajorDefect md,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(MajorDefectController.editMajorDefect(md, userId, room, department, branch, countryCode, division, organization)).build();
    }

}
