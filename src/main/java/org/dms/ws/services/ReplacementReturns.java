/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ReturnReplacementController;
import org.dms.ws.singer.entities.ReplasementReturn;

/**
 *
 * @author SDU
 */
@Path("/ReplacementReturns")
public class ReplacementReturns {

    @GET
    @Path("/getReturnReplacement")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getReturnReplacement(@QueryParam("replRetutnNo") @DefaultValue("0") int replRetutnNo,
            @QueryParam("exchangeReferenceNo") @DefaultValue("all") String exgRefNo,
            @QueryParam("distributerId") @DefaultValue("0") int distributerId,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("returnDate") @DefaultValue("all") String returnDate,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ReturnReplacementController.getReturnReplacement(replRetutnNo, exgRefNo, distributerId, imeiNo,modleDesc, returnDate, status,order,type ,start, limt);

    }

    @PUT
    @Path("/addReplasementReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addReplasementReturn(ReplasementReturn rr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(ReturnReplacementController.addReplasementReturn(rr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editReplasementReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editReplasementReturn(ReplasementReturn rr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(ReturnReplacementController.editReplasementReturn(rr, userId, room, department, branch, countryCode, division, organization)).build();
    }

    
    @POST
    @Path("/approveReplasementReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response approveReplasementReturn(ReplasementReturn rr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(ReturnReplacementController.approveReplasementReturn(rr, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    
    
    @POST
    @Path("/deleteReplasementReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteReplasementReturn(ReplasementReturn rr,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {
        return Response.status(201).entity(ReturnReplacementController.deleteReplasementReturn(rr, userId, room, department, branch, countryCode, division, organization)).build();
    }
}
