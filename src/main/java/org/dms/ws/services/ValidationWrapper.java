/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

/**
 *
 * @author SDU
 */
public class ValidationWrapper {
    private String ReturnFlag;
    private String ReturnMsg;
    private String ReturnRefNo;

    /**
     * @return the ReturnFlag
     */
    public String getReturnFlag() {
        return ReturnFlag;
    }

    /**
     * @param ReturnFlag the ReturnFlag to set
     */
    public void setReturnFlag(String ReturnFlag) {
        this.ReturnFlag = ReturnFlag;
    }

    /**
     * @return the ReturnMsg
     */
    public String getReturnMsg() {
        return ReturnMsg;
    }

    /**
     * @param ReturnMsg the ReturnMsg to set
     */
    public void setReturnMsg(String ReturnMsg) {
        this.ReturnMsg = ReturnMsg;
    }

    /**
     * @return the ReturnRefNo
     */
    public String getReturnRefNo() {
        return ReturnRefNo;
    }

    /**
     * @param ReturnRefNo the ReturnRefNo to set
     */
    public void setReturnRefNo(String ReturnRefNo) {
        this.ReturnRefNo = ReturnRefNo;
    }

 
    
    
}
