/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.print.attribute.standard.Media;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ldap.util.LdapUser;
import org.dms.ldap.util.LoginWrapper;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.singer.controllers.UserController;
import org.dms.ws.singer.entities.User;

/**
 *
 * @author SDU
 */
@Path("/Login")
public class Login {

    @POST
    @Path("/UserValidation")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper Uservalidation(LdapUser user,
            @HeaderParam("system") String system) {
        return UserController.validateUser(user,system);
    }
    
    @POST
    @Path("/UserValidationAndroid")
    @Produces({MediaType.APPLICATION_JSON})
    public LoginWrapper UserValidationAndroid(LdapUser user,
            @HeaderParam("system") String system) {
        return UserController.validateUserAndroid(user,system);
    }
    
    @POST
    @Path("/UserValidationAndroidP3")
    @Produces({MediaType.APPLICATION_JSON})
    public LoginWrapper UserValidationAndroidP3(LdapUser user,
            @HeaderParam("system") String system,
            @HeaderParam("imei") String imei) {
        return UserController.validateUserAndroidP3(user,system,imei);
    }

    @GET
    @Path("/logoutUser")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper logoutUser(@QueryParam("username") String username,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room") String room, 
            @HeaderParam("department") String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division") String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return UserController.logoutUser(username,userId,room,department,branch,countryCode,division,organization,system);
    }
    
    @GET
    @Path("/jbossCheck")
    @Produces({MediaType.APPLICATION_JSON})
    public int jbossCheck(){
        return 1;
    }
    
    @GET
    @Path("/dbCheck")
    @Produces({MediaType.APPLICATION_JSON})
    public int dbCheck(){
        UserController usCtrl = new UserController();
        return usCtrl.DBCheck();
    }

}
