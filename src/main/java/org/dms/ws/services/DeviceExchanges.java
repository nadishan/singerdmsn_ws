/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.DeviceExchangeController;
import org.dms.ws.singer.entities.DeviceExchange;

/**
 *
 * @author SDU
 */
@Path("/DeviceExchanges")
public class DeviceExchanges {

    @GET
    @Path("/getDeviceExchangeDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceExchangeDetails(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("exchangeReferenceNo") @DefaultValue("all") String exchangeReferenceNo,
            @QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerNIC") @DefaultValue("all") String customerNIC,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modelDesc") @DefaultValue("all") String modelDesc,
            @QueryParam("warrantyRegDate") @DefaultValue("all") String warrantyRegDate,
            @QueryParam("reasonDesc") @DefaultValue("all") String reasonDesc,
            @QueryParam("exchangeDesc") @DefaultValue("all") String exchangeDesc,
            @QueryParam("warrantyDesc") @DefaultValue("all") String warrantyDesc,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("erpCode") @DefaultValue("all") String erpCode,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return DeviceExchangeController.getDeviceExchangeDetails(bisId, exchangeReferenceNo, workOrderNo, imeiNo, customerName, customerNIC, product, brand, modelDesc, warrantyRegDate, reasonDesc, exchangeDesc, warrantyDesc, order, erpCode, type, start, limt);

    }

    @PUT
    @Path("/addDeviceExchange")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addDeviceExchange(DeviceExchange de,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceExchangeController.addDeviceExchange(de, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDeviceReturn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editDeviceReturn(DeviceExchange de,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceExchangeController.editDeviceExchange(de, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/deleteDeviceExchange")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteDeviceExchange(DeviceExchange de,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(DeviceExchangeController.deleteDeviceExchange(de, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getDeviceExchangeReason")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceExchangeReason() {
        return DeviceExchangeController.getDeviceExchangeReason();

    }

    @GET
    @Path("/getDeviceExchangeType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeviceExchangeType() {
        return DeviceExchangeController.getDeviceExchangeType();

    }

    @GET
    @Path("/getWorkOrdersToExchange")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrdersToExchange(@QueryParam("bisId") int bisId,
            @QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("0") int imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("defectDesc") @DefaultValue("all") String defectDesc,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("delayDate") @DefaultValue("0") int delayDate
            ) {
        return DeviceExchangeController.getWorkOrdersToExchange(bisId,
                workOrderNo,
                customerName,
                customerAddress,
                workTelephoneNo,
                email,
                customerNic,
                imeiNo,
                product,
                brand,
                modleDesc,
                defectDesc,
                rccReference,
                dateOfSale,
                proofOfPurches,
                binName,
                warrantyStatusMsg,
                delayDate
                );

    }
    
    @GET
    @Path("/approveRejectExchange")
    @Produces({MediaType.APPLICATION_JSON})
    public int approveRejectExchange(
    @QueryParam("refNo") String refNo,
            @QueryParam("status") int status) {
        return DeviceExchangeController.approveRejectExchange(refNo, status);

    }
}
