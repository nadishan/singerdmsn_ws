/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.dms.ldap.util.LdapUser;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.singer.controllers.AcceptDebitNoteController;
import org.dms.ws.singer.controllers.AcceptInventoryController;
import org.dms.ws.singer.controllers.BussStrucController;
import org.dms.ws.singer.controllers.BussinessTypeController;
import org.dms.ws.singer.controllers.CategoryContraller;
import org.dms.ws.singer.controllers.ClearTransitItemController;
import org.dms.ws.singer.controllers.CreditNoteIssueController;
import org.dms.ws.singer.controllers.CustomerController;
import org.dms.ws.singer.controllers.DSRTrakingController;
import org.dms.ws.singer.controllers.DbProcedureController;
import org.dms.ws.singer.controllers.DeviceExchangeController;
import org.dms.ws.singer.controllers.DeviceIssueController;
import org.dms.ws.singer.controllers.DeviceReturnController;
import org.dms.ws.singer.controllers.EventAssignMarkController;
import org.dms.ws.singer.controllers.EventInquireController;
import org.dms.ws.singer.controllers.EventMasterController;
import org.dms.ws.singer.controllers.EventPromotionController;
import org.dms.ws.singer.controllers.GroupController;
import org.dms.ws.singer.controllers.ImeiAdjusmentController;
import org.dms.ws.singer.controllers.ImeiMasterController;
import org.dms.ws.singer.controllers.ImportDebitNoteController;
import org.dms.ws.singer.controllers.IssueInventoryController;
import org.dms.ws.singer.controllers.LevelContraller;
import org.dms.ws.singer.controllers.MajorDefectController;
import org.dms.ws.singer.controllers.MinorDefectController;
import org.dms.ws.singer.controllers.ModelListController;
import org.dms.ws.singer.controllers.PartContraoller;
import org.dms.ws.singer.controllers.RepairCategoryController;
import org.dms.ws.singer.controllers.ReturnReplacementController;
import org.dms.ws.singer.controllers.ShopVisitController;
import org.dms.ws.singer.controllers.TransferRepairController;
import org.dms.ws.singer.controllers.UserBusinessMappingController;
import org.dms.ws.singer.controllers.UserBusinessStructuresController;
import org.dms.ws.singer.controllers.UserController;
import org.dms.ws.singer.controllers.UserTypeListController;
import org.dms.ws.singer.controllers.WarrantyAdjustmentController;
import org.dms.ws.singer.controllers.WarrantyMaintainanceController;
import org.dms.ws.singer.controllers.WarrantySchemeController;
import org.dms.ws.singer.controllers.WarrentyRegistrationController;
import org.dms.ws.singer.controllers.WarrentyReplacementController;
import org.dms.ws.singer.controllers.WorkOrderApproveController;
import org.dms.ws.singer.controllers.WorkOrderCloseController;
import org.dms.ws.singer.controllers.WorkOrderController;
import org.dms.ws.singer.controllers.WorkOrderFeedBackController;
import org.dms.ws.singer.controllers.WorkOrderInquireController;
import org.dms.ws.singer.controllers.WorkOrderMaintainaceController;
import org.dms.ws.singer.controllers.WorkOrderTransferController;
import org.dms.ws.singer.controllers.WorkPaymentController;
import org.dms.ws.singer.entities.AcceptDebitNote;
import org.dms.ws.singer.entities.AcceptInvenroryImei;
import org.dms.ws.singer.entities.AcceptInventory;
import org.dms.ws.singer.entities.BSUserMapper;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.BussinessType;
import org.dms.ws.singer.entities.Category;
import org.dms.ws.singer.entities.ClearTransitItem;
import org.dms.ws.singer.entities.CreditNoteIssue;
import org.dms.ws.singer.entities.CreditNoteIssueDetail;
import org.dms.ws.singer.entities.Customer;
import org.dms.ws.singer.entities.DSRTrack;
import org.dms.ws.singer.entities.DeviceExchange;
import org.dms.ws.singer.entities.DeviceIssue;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.entities.DeviceReturn;
import org.dms.ws.singer.entities.DeviceReturnDetail;
import org.dms.ws.singer.entities.EstimatedParts;
import org.dms.ws.singer.entities.EventAward;
import org.dms.ws.singer.entities.EventBisType;
import org.dms.ws.singer.entities.EventBussinessList;
import org.dms.ws.singer.entities.EventMaster;
import org.dms.ws.singer.entities.EventNonSalesRule;
import org.dms.ws.singer.entities.EventParticipant;
import org.dms.ws.singer.entities.EventRulePointScheme;
import org.dms.ws.singer.entities.EventSalesRule;
import org.dms.ws.singer.entities.EventTransaction;
import org.dms.ws.singer.entities.ImeiAdjusment;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.ImportDebitNote;
import org.dms.ws.singer.entities.ImportDebitNoteDetails;
import org.dms.ws.singer.entities.LdapUserEnty;
import org.dms.ws.singer.entities.MajorDefect;
import org.dms.ws.singer.entities.MdeCode;
import org.dms.ws.singer.entities.MinorDefect;
import org.dms.ws.singer.entities.ModelList;
import org.dms.ws.singer.entities.Part;
import org.dms.ws.singer.entities.PaymentApprove;
import org.dms.ws.singer.entities.QuickImeiMaster;
import org.dms.ws.singer.entities.QuickWorkOrder;
import org.dms.ws.singer.entities.RepairCategory;
import org.dms.ws.singer.entities.RepairLevel;
import org.dms.ws.singer.entities.ReplasementReturn;
import org.dms.ws.singer.entities.ShopVisitItemList;
import org.dms.ws.singer.entities.ShopVistCheck;
import org.dms.ws.singer.entities.TransferRepair;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserBusinessMapping;
import org.dms.ws.singer.entities.UserBusinessStructure;
import org.dms.ws.singer.entities.UserTypeList;
import org.dms.ws.singer.entities.WarrantyAdjustment;
import org.dms.ws.singer.entities.WarrantyScheme;
import org.dms.ws.singer.entities.WarrentyRegistration;
import org.dms.ws.singer.entities.WarrenyReplacement;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderClose;
import org.dms.ws.singer.entities.WorkOrderDefect;
import org.dms.ws.singer.entities.WorkOrderEstimate;
import org.dms.ws.singer.entities.WorkOrderFeedBack;
import org.dms.ws.singer.entities.WorkOrderInquire;
import org.dms.ws.singer.entities.WorkOrderMaintain;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import org.dms.ws.singer.entities.WorkOrderPayment;
import org.dms.ws.singer.entities.WorkOrderPaymentDetail;
import org.dms.ws.singer.entities.WorkOrderStatusInquiry;
import org.dms.ws.singer.entities.WorkOrderTransfer;
import org.dms.ws.singer.utils.CustomerFeedBack;
import org.dms.ws.singer.utils.EmaiSendConfirmCode;
import org.dms.ws.singer.utils.EmailSendFeedBack;
import org.dms.ws.singer.utils.EmailSender;

/**
 *
 * @author SDU
 */
public class ComonTest {

    //"admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer"
    public static void replyWorkOrder() {

        WorkOrderFeedBack dt = new WorkOrderFeedBack();
        dt.setEmail("ruwana@dmssw.com");
        dt.setCustomerName("Ruwan");
        dt.setWoReply("Your Device can't Repair");
        dt.setFeedBackId(1);

        ValidationWrapper vw = WorkOrderFeedBackController.replyWorkOrderFeedBack(dt, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

//    public static void getWorkOrdersToExchange() {
//
////        MessageWrapper mw = DeviceExchangeController.getWorkOrdersToExchange(229);
//
//        System.out.println("Total Recode" + mw.getTotalRecords());
//        //System.out.println("Data     "+mw.getData());
//        ArrayList<WorkOrder> imList = mw.getData();
//
//        for (WorkOrder im : imList) {
//            System.out.println("     " + im.getWorkOrderNo());
//        }
//    }

    public static void checkImeiWarranty() {

        ArrayList<ImeiMaster> checkImeiWarrantyList = DeviceExchangeController.checkImeiUnderWarranty();

        //System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        // ArrayList<WorkOrder> imList = mw.getData();
        for (ImeiMaster im : checkImeiWarrantyList) {
            System.out.println("     " + im.getImeiNo());
        }
    }

    public static void getWorkOrderToClose() {

        MessageWrapper mw = WorkOrderCloseController.getWorkOrderToClose(0);

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<WorkOrder> imList = mw.getData();

        for (WorkOrder im : imList) {
            System.out.println("     " + im.getWorkOrderNo());
        }
    }

    public static void getWorkOrderToTransfer() {

        MessageWrapper mw = WorkOrderTransferController.getWorkOrderToTransfer("all", 0);

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<WorkOrder> imList = mw.getData();

        for (WorkOrder im : imList) {
            System.out.println("     " + im.getWorkOrderNo());
        }
    }

    public static void getWorkOrderFeedBack() {

        MessageWrapper mw = WorkOrderFeedBackController.getWorkOrderFeedbackList(0, "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "ALL", 0, "warrenty", "DESC", 0, 100);

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<WorkOrderFeedBack> imList = mw.getData();

        for (WorkOrderFeedBack im : imList) {
//            System.out.print("     " + im.getImeiNo());
//            System.out.print("     " + im.getWorkOrderNo());
//            System.out.print("     " + im.getFeedBackId());
//            System.out.print("     " + im.getDeleveryDate());
//            System.out.print("     " + im.getCustomerName());
//            System.out.print("     " + im.getModelName());
//            System.out.print("     " + im.getPurchaseDate());
//            System.out.print("     " + im.getPurchaseShop());
//            System.out.print("     " + im.getServiceCenter());
//            System.out.print("     " + im.getReplyStatus());
//            System.out.println("     " + im.getTelephoneNo());
            System.out.println("     " + im.getWarrenty());
        }
    }

//    public static void getImeiValidateAcceptInventory() {
//
//        MessageWrapper mw = ImeiMasterController.getImeiValidationAcceptInventory("7101", 245, "ALL");
//
//        System.out.println("Total Recode" + mw.getTotalRecords());
//        //System.out.println("Data     "+mw.getData());
//        ArrayList<ImeiMaster> imList = mw.getData();
//
//        for (ImeiMaster im : imList) {
//            System.out.print("     " + im.getImeiNo());
//            System.out.print("     " + im.getModleNo());
//            System.out.print("     " + im.getBisId());
//            System.out.print("     " + im.getBisName());
//            System.out.print("     " + im.getDebitnoteNo());
//            System.out.print("     " + im.getProduct());
//            System.out.print("     " + im.getBrand());
//            System.out.print("     " + im.getModleDesc());
//            System.out.print("     " + im.getPurchaseDate());
//            System.out.println("     " + im.getLocation());
//        }
//    }

    public static void getImeiValidateIssueInventory() {

        MessageWrapper mw = ImeiMasterController.getImeiValidationIssueInventory("789819293834545", 94);

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<ImeiMaster> imList = mw.getData();

        for (ImeiMaster im : imList) {
            System.out.print("     " + im.getImeiNo());
            System.out.print("     " + im.getModleNo());
            System.out.print("     " + im.getBisId());
            System.out.print("     " + im.getBisName());
            System.out.print("     " + im.getDebitnoteNo());
            System.out.print("     " + im.getProduct());
            System.out.print("     " + im.getBrand());
            System.out.print("     " + im.getModleDesc());
            System.out.print("     " + im.getPurchaseDate());
            System.out.println("     " + im.getLocation());
        }
    }

    public static void getImeiDetailsAccept() {

        MessageWrapper mw = ImeiMasterController.getImeiDetailsAccept("689646464646440");

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<ImeiMaster> imList = mw.getData();

        for (ImeiMaster im : imList) {
            System.out.print("     " + im.getImeiNo());
            System.out.print("     " + im.getModleNo());
            System.out.print("     " + im.getDebitnoteNo());
            System.out.print("     " + im.getProduct());
            System.out.print("     " + im.getModleDesc());
            System.out.println("     " + im.getLocation());
        }
    }

    public static void getImeiDetails() {

        MessageWrapper mw = ImeiMasterController.getImeiDetails("689646464646440","256");

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<ImeiMaster> imList = mw.getData();

        for (ImeiMaster im : imList) {
            System.out.print("     " + im.getImeiNo());
            System.out.print("     " + im.getModleNo());
            System.out.print("     " + im.getBisId());
            System.out.print("     " + im.getBisName());
            System.out.print("     " + im.getDebitnoteNo());
            System.out.print("     " + im.getProduct());
            System.out.print("     " + im.getBrand());
            System.out.print("     " + im.getModleDesc());
            System.out.print("     " + im.getPurchaseDate());
            System.out.println("     " + im.getLocation());
        }
    }

//    public static void getModleList() {
//
//        MessageWrapper mw = ModelListController.getModelDetails(0, "all", "all", "all", "all", "all", "all", "all", "all", "all", 0, 0, 0, 0, 0, 0, "all", "all", 0, 200);
//        ArrayList<ModelList> ml = mw.getData();
//        System.out.println("Total Records: " + mw.getTotalRecords());
//
//        for (ModelList m : ml) {
//            System.out.print(m.getModel_No());
//            System.out.print(" -> " + m.getArt_Prd_Code_Desc());
//            System.out.print(" -> " + m.getCommodity_Group_1());
//            System.out.print(" -> " + m.getCommodity_Group_2());
//            System.out.print(" -> " + m.getModel_Description());
//            System.out.print(" -> " + m.getPart_Prd_Code());
//            System.out.print(" -> " + m.getPart_Prd_Family());
//            System.out.print(" -> " + m.getPart_Prd_Family_Desc());
//            System.out.print(" -> " + m.getCost_Price());
//            System.out.print(" -> " + m.getDealer_Margin());
//            System.out.print(" -> " + m.getDistributor_Margin());
//            System.out.print(" -> " + m.getRep_Category());
//            System.out.print(" -> " + m.getSelling_Price());
//            System.out.print(" -> " + m.getStatus());
//            System.out.println("-> " + m.getWrn_Scheme());
//        }
//    }

    public static void getEventPeriod() {

        MessageWrapper mw = EventMasterController.getEventPeriod();

        ArrayList<MdeCode> utlList = mw.getData();

        for (MdeCode ut : utlList) {
            System.out.print("   " + ut.getDescription());
            System.out.println("   " + ut.getCode());

        }

    }

    public static void getBrandList() {

        MessageWrapper mw = EventMasterController.getBrandList();

        ArrayList<MdeCode> utlList = mw.getData();

        for (MdeCode ut : utlList) {
            System.out.print("   " + ut.getDescription());
            System.out.println("   " + ut.getCode());

        }
    }

    public static void getProductFamily() {

        MessageWrapper mw = EventMasterController.getProductFamily();

        ArrayList<MdeCode> utlList = mw.getData();

        for (MdeCode ut : utlList) {
            System.out.print("   " + ut.getDescription());
            System.out.println("   " + ut.getCode());

        }

    }

    public static void getSalesType() {

        MessageWrapper mw = EventMasterController.getSalesType();

        ArrayList<MdeCode> utlList = mw.getData();

        for (MdeCode ut : utlList) {
            System.out.print("   " + ut.getDescription());
            System.out.println("   " + ut.getCode());

        }

    }

    public static void editWarrantySchema() {

        WarrantyScheme dt = new WarrantyScheme();
        dt.setSchemeId(5);
        dt.setSchemeName("test");
        dt.setStatus(1);
        dt.setPeriod(50);

        ValidationWrapper vw = WarrantySchemeController.editWarrantySchema(dt, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addWarrantySchema() {

        WarrantyScheme dt = new WarrantyScheme();
        dt.setSchemeName("new");
        dt.setStatus(1);
        dt.setPeriod(50);

        ValidationWrapper vw = WarrantySchemeController.addWarrantySchema(dt, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getWaarantySchema() {

        MessageWrapper mw = WarrantySchemeController.getWarrantySchemeList(0, "all", 180, 0, "all", "all", 0, 200);

        ArrayList<WarrantyScheme> utlList = mw.getData();

        for (WarrantyScheme ut : utlList) {
            System.out.print("   " + ut.getSchemeId());
            System.out.print("   " + ut.getSchemeName());
            System.out.print("   " + ut.getStatusMsg());
            System.out.print("   " + ut.getPeriod());
            System.out.println("   " + ut.getStatus());

        }

    }

    public static void getDayMovement() {

        MessageWrapper mw = ShopVisitController.getDayMovement(108, "2014/12/11", 0, 200);

        ArrayList<ShopVistCheck> utlList = mw.getData();

        for (ShopVistCheck ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getVisitDate());
            System.out.print("   " + ut.getLongtitude());
            System.out.print("   " + ut.getLatitude());
            System.out.print("   " + ut.getSubDelerName());
            System.out.print("   " + ut.getParentName());
            System.out.println("   " + ut.getShopVistId());

            ArrayList<ShopVisitItemList> itemlist = ut.getShopVisitItemList();
            for (ShopVisitItemList si : itemlist) {
                System.out.print("   " + si.getSeqNo());
                System.out.print("   " + si.getModleNo());
                System.out.print("   " + si.getItemCount());
                System.out.print("   " + si.getModleDesc());
                System.out.println("   " + si.getShopVisitId());
            }

        }

    }

    public static void getBussineAvilableList() {

        MessageWrapper mw = UserTypeListController.getBussinessAvilableTypeList(15, 1);
        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getFirstName());
            System.out.print("   " + ut.getLastName());
            System.out.print("   " + ut.getUserId());
            System.out.println("   " + ut.getBisId());

        }

    }

    public static void getWorkOrderListToClose() {

        MessageWrapper mw = WorkOrderMaintainaceController.getWorkOrderListToClose("all", "all", "all", "all", "all", "all", "all", "all", "all", "all", 219, "all", "all", 0, 999);

        ArrayList<WorkOrderMaintain> utlList = mw.getData();

        for (WorkOrderMaintain ut : utlList) {
            System.out.print("   " + ut.getWorkorderMaintainId());
            System.out.print("   " + ut.getWorkOrderNo());
            System.out.print("   " + ut.getWarantyStatus());
            System.out.print("   " + ut.getStatusMsg());
            System.out.print("   " + ut.getModelDesc());
            System.out.print("   " + ut.getStatusMsg());
            System.out.print("   " + ut.getWarrantyTypedesc());
            System.out.print("   " + ut.getInvoiceNo());
            System.out.print("   " + ut.getWarrantyVrifType());
            System.out.println("   " + ut.getEstimateStatusMsg());

        }

    }

    public static void getWorkOrderMaitainList() {

        MessageWrapper mw = WorkOrderMaintainaceController.getWorkOrderMaintainList(220, "420", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", 0, 200);

        ArrayList<WorkOrderMaintain> utlList = mw.getData();

        for (WorkOrderMaintain ut : utlList) {
            System.out.print("   " + ut.getWorkorderMaintainId());
            System.out.print("   " + ut.getWorkOrderNo());
            System.out.print("   " + ut.getWarantyStatus());
            System.out.print("   " + ut.getStatusMsg());
            System.out.print("   " + ut.getModelDesc());
            System.out.print("   " + ut.getStatusMsg());
            System.out.print("   " + ut.getWarrantyTypedesc());
            System.out.print("   " + ut.getInvoiceNo());
            System.out.print("   " + ut.getWarrantyVrifType());
            System.out.println("   " + ut.getEstimateStatusMsg());

        }

    }

    public static void sentMail() {
        try {
            // EmaiSendConfirmCode.sendHTMLMailWorkOrder("sajithd@dmssw.com", "Sajith", "123", "WO000022");
            // EmailSender.sendHTMLMail("sajithd@dmssw.com", "123", "sajith", "Dulanga");
            //EmaiSendConfirmCode.sendHTMLMailWorkOrderClose("dasunm@dmssw.com", "Sajith", "WO000022", "Huwai", "reffRcc", "model", "imei", "deliveryType", "deliveryDate", "CourierNo", "gatePass");

            ArrayList<EstimatedParts> partDetailsList = new ArrayList<>();
            EstimatedParts e = new EstimatedParts();
            e.setPartNo(1);
            e.setPartDescription("Part A");
            e.setQty(1);
            e.setUnitPrice(200.00);
            e.setAmount(200.00);
            partDetailsList.add(e);
            CustomerFeedBack.sendHTMLMail("dasunm@dmssw.com", "Sajith", "15500.00", "ES000010", "Change Singnal Chip", "WO000022", partDetailsList, 200.00, "Level B", 200.00, "Model", "Imei");
            //EmailSendFeedBack.sendHTMLMail("dasunm@dmssw.com", "Dasun", "Damn!!!");
        } catch (Exception we) {
            System.out.println("Error   " + we.getMessage());
        }

    }

    public static void DateFormat() {

        String original = "2014-11-24T09:23Z";
        String date = original.substring(0, 10);
        String time = original.substring(11, 16);

        System.out.println(original.substring(0, 10) + " " + original.substring(11, 16));
    }

    public static void editEventMarks() {

        ArrayList<EventTransaction> etList = new ArrayList<>();

        EventTransaction et = new EventTransaction();
        et.setAssignId(94);
        et.setEventId(1);
        et.setUserId("shk91");
        et.setPointAddDate("2014/12/30");
        et.setNonSalePoint(20);
        et.setNonSaleId(1);
        et.setRemarks("Sample");

        etList.add(et);

        ValidationWrapper vw = EventAssignMarkController.editEventMarks(etList, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addEventMarks() {

        ArrayList<EventTransaction> etList = new ArrayList<>();

        EventTransaction et = new EventTransaction();
        et.setAssignId(94);
        et.setUserId("shk91");
        et.setEventId(1);
        et.setPointAddDate("2014/11/30");
        et.setNonSalePoint(20);
        et.setNonSaleId(1);
        et.setRemarks("Sample");

        EventTransaction et1 = new EventTransaction();
        et1.setAssignId(94);
        et1.setUserId("shk91");
        et1.setEventId(1);
        et1.setPointAddDate("2014/11/30");
        et1.setNonSalePoint(50);
        et1.setNonSaleId(2);
        et1.setRemarks("Sample");

        etList.add(et);
        etList.add(et1);

        ValidationWrapper vw = EventAssignMarkController.addEventMarks(etList, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getMarksList() {

        MessageWrapper mw = EventAssignMarkController.getEventAssignMarksList(0, "all", "all", "all", 0, 0, "all", "all", "all", 0, 200);

        ArrayList<EventTransaction> utlList = mw.getData();

        for (EventTransaction ut : utlList) {
            System.out.print("   " + ut.getNonSalePoint());
            System.out.print("   " + ut.getUserId());
            System.out.print("   " + ut.getAssignBy());
            System.out.print("   " + ut.getPointAddDate());
            System.out.print("   " + ut.getRemarks());
            System.out.println("   " + ut.getStatusMsg());

        }

    }

    public static void DeleteWarantyAdjusment() {

        WarrantyAdjustment ct = new WarrantyAdjustment();
        ct.setWarrantyId(12);

        ValidationWrapper vw = WarrantyAdjustmentController.deleteWarrantyAdjustment(ct, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Msg  " + vw.getReturnMsg());
    }

    public static void getPartsList() {

        MessageWrapper mw = PartContraoller.getPartsDetails(0, "IC", 0, "all", "all", "all", "all", "all", "all", 0, 0, "all", "all", "all", 0, 200);

        ArrayList<Part> utlList = mw.getData();

        for (Part ut : utlList) {
            System.out.print("   " + ut.getPartNo());
            System.out.print("   " + ut.getErpPartNo());
            System.out.print("   " + ut.getPartDesc());
            System.out.print("   " + ut.getPartPrdCode());
            System.out.print("   " + ut.getPartPrdDesc());
            System.out.print("   " + ut.getPartPrdFamilyDesc());
            System.out.print("   " + ut.getPartCost());
            System.out.print("   " + ut.getPartExist());
            System.out.println("   " + ut.getPartSellPrice());

        }

    }

    public static void getDeviceReturnDetials() {

        MessageWrapper mw = DeviceReturnController.getDeviceReturnDetail(11);

        ArrayList<DeviceReturn> utlList = mw.getData();

        for (DeviceReturn ut : utlList) {
            System.out.print("   " + ut.getReturnNo());
            System.out.print("   " + ut.getDistributorName());
            System.out.print("   " + ut.getDsrName());
            System.out.print("   " + ut.getReturnDate());
            System.out.println("   " + ut.getStatus());
            ArrayList<DeviceReturnDetail> deviceRetunrDetailList = ut.getDeviceReturnList();

            for (DeviceReturnDetail rd : deviceRetunrDetailList) {
                System.out.print("  " + rd.getImeiNo());
                System.out.print("  " + rd.getModleDesc());
                System.out.print("  " + rd.getModleNo());
                System.out.println("  " + rd.getSalesPrice());
            }
        }

    }

    public static void getDeviceReturnList() {

        MessageWrapper mw = DeviceReturnController.getDeviceReturnList(0, 0,226, "all", "all", "all", 0, 0, 0, 0, 0, "all", "all", 0, 25);

        ArrayList<DeviceReturn> utlList = mw.getData();

        for (DeviceReturn ut : utlList) {
            System.out.print("   " + ut.getReturnNo());
            System.out.print("   " + ut.getDistributorName());
            System.out.print("   " + ut.getDsrName());
            System.out.print("   " + ut.getReturnDate());
            System.out.println("   " + ut.getStatus());
        }
    }

    public static void getBussinessTypeList() {

        MessageWrapper mw = BussStrucController.getBussinessAsignList("7,8");

        ArrayList<BussinessStructure> utlList = mw.getData();

        for (BussinessStructure ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getBisDesc());
            System.out.print("   " + ut.getBisPrtnId());
            System.out.println("   " + ut.getBisUserCount());
        }
    }

    public static void getAssignNonSalesRule() {

        MessageWrapper mw = EventAssignMarkController.getEventAssignNonSalesRule("subcna1", 1);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventNonSalesRule> woLost = mw.getData();

        for (EventNonSalesRule wo : woLost) {
            System.out.print("    " + wo.getNonSalesRule());
            System.out.print("    " + wo.getPoint());
            System.out.print("    " + wo.getDescription());
            System.out.print("    " + wo.getTaget());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getNonSalesRuleList() {

        MessageWrapper mw = EventAssignMarkController.getEventNonSalesRuleList(10);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventNonSalesRule> woLost = mw.getData();

        for (EventNonSalesRule wo : woLost) {
            System.out.print("    " + wo.getNonSalesRule());
            System.out.print("    " + wo.getPoint());
            System.out.print("    " + wo.getDescription());
            System.out.print("    " + wo.getTaget());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getParticipant() {

        MessageWrapper mw = EventInquireController.getParticipentList(10, "all", "all", "all", "all", "all", "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventParticipant> woLost = mw.getData();

        for (EventParticipant wo : woLost) {
            System.out.print("    " + wo.getPaticipantName());
            System.out.print("    " + wo.getPaticipantType());
            System.out.print("    " + wo.getLocation());
            System.out.print("    " + wo.getAchivePoint());
            System.out.println("    " + wo.getPosotion());

        }

    }

    public static void getEventParticipant() {

        MessageWrapper mw = EventAssignMarkController.getEventParticipant(2);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventBussinessList> woLost = mw.getData();

        for (EventBussinessList wo : woLost) {
            System.out.print("    " + wo.getSeqNo());
            System.out.print("    " + wo.getBisId());
            System.out.print("    " + wo.getUserId());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getEventInquire() {

        MessageWrapper mw = EventInquireController.getEventInqurieList(0, "all", "all", "all", "all", "all", "all", "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventMaster> woLost = mw.getData();

        for (EventMaster wo : woLost) {
            System.out.print("    " + wo.getEventId());
            System.out.print("    " + wo.getEventName());
            System.out.print("    " + wo.getEventDesc());
            System.out.print("    " + wo.getStartDate());
            System.out.print("    " + wo.getEndDate());
            System.out.print("    " + wo.getEventAchievement() + "%");
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getEventMasterDetails() {

        MessageWrapper mw = EventMasterController.getEventMasterDetails(10);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventMaster> woLost = mw.getData();

        for (EventMaster wo : woLost) {
            System.out.print("    " + wo.getEventId());
            System.out.print("    " + wo.getEventName());
            System.out.print("    " + wo.getEventDesc());
            System.out.print("    " + wo.getStartDate());
            System.out.print("    " + wo.getEndDate());
            System.out.println("    " + wo.getStatus());

            ArrayList<EventAward> EvetAward = wo.getEvenAwardList();
            System.out.println("Award.............");
            for (EventAward ea : EvetAward) {
                System.out.print("   " + ea.getAwardId());
                System.out.print("   " + ea.getPalce());
                System.out.print("Award---   " + ea.getAward());
                System.out.print("   " + ea.getEventid());
                System.out.println("   " + ea.getStatus());
            }

            ArrayList<EventNonSalesRule> nonsales = wo.getEventNonSalesRuleList();
            System.out.println("Non Sales............");
            for (EventNonSalesRule ea : nonsales) {
                System.out.print("   " + ea.getNonSalesRule());
                System.out.print("   " + ea.getPoint());
                System.out.print("   " + ea.getTaget());
                System.out.println("   " + ea.getStatus());
            }

            ArrayList<EventSalesRule> salesRule = wo.getEventSalesRuleList();
            System.out.println("sales Rules.............");
            for (EventSalesRule ea : salesRule) {
                System.out.print("   " + ea.getRuleId());
                System.out.print("Modle Desc   " + ea.getModleDesc());
                System.out.print("   " + ea.getIntervals());
                System.out.print("   " + ea.getStatus());
                System.out.println("   " + ea.getStatus());

            }

            System.out.println("Bussiness Type............");
            ArrayList<EventBisType> bistype = wo.getEventBisTypeList();

            for (EventBisType ea : bistype) {
                System.out.print("   " + ea.getEventId());
                System.out.print("   " + ea.getEventPartId());
                System.out.print("   " + ea.getBisStruTypeCount());
                System.out.print("   " + ea.getBisStruTypeName());
                System.out.println("   " + ea.getStatus());

                ArrayList<EventBussinessList> eventbussiList = ea.getEventBussinessList();
                System.out.println("Bussiness Asign List............");
                for (EventBussinessList es : eventbussiList) {
                    System.out.print("   " + es.getBisId());
                    System.out.print("   " + es.getEventPartId());
                    System.out.print("User--   " + es.getUserId());
                    System.out.print("   " + es.getBisName());
                    System.out.println("   " + es.getStatus());
                }

            }

        }

    }

    public static void editCreateEvent() {

        EventMaster em = new EventMaster();
        em.setEventId(1);
        em.setEventName("Mega Point Bonus 123");
        // em.setEventName("Sample Event");
        em.setEventDesc("More than 100 point");
        em.setEndDate("2015/05/30");
        em.setMktMrgComment("Test Comment");
        em.setEventMrgComment("Sajith Sample");
        em.setStartDate("2014/12/01");
        em.setThreshold("Test Thre");

        //----------------------------------------------------------------------------
        ArrayList<EventBisType> ebtList = new ArrayList<>();

        EventBisType ebt1 = new EventBisType();
        ebt1.setBisStruTypeId(1);
        ebt1.setBisStruTypeCount(9);

        ArrayList<EventBussinessList> prt_dtlList = new ArrayList<>();

        EventBussinessList prt_dtl1 = new EventBussinessList();
        prt_dtl1.setBisId(129);
        prt_dtl1.setBisName("LG");

        EventBussinessList prt_dtl2 = new EventBussinessList();
        prt_dtl2.setBisId(131);
        prt_dtl2.setBisName("Lexus");

        prt_dtlList.add(prt_dtl1);
        prt_dtlList.add(prt_dtl2);

        ebt1.setEventBussinessList(prt_dtlList);
//------------------------------------------------------------------------------
        EventBisType ebt12 = new EventBisType();
        ebt12.setBisStruTypeId(3);
        ebt12.setBisStruTypeCount(5);

        ArrayList<EventBussinessList> prt_dtlList1 = new ArrayList<>();

        EventBussinessList prt_dtl12 = new EventBussinessList();
        prt_dtl12.setBisId(114);
        prt_dtl12.setBisName("sunny");

        EventBussinessList prt_dtl22 = new EventBussinessList();
        prt_dtl22.setBisId(115);
        prt_dtl22.setBisName("np1");

        prt_dtlList1.add(prt_dtl12);
        prt_dtlList1.add(prt_dtl22);

        ebt12.setEventBussinessList(prt_dtlList1);

//------------------------------------------------------------------------------ 
        ebtList.add(ebt1);
        ebtList.add(ebt12);
        em.setEventBisTypeList(ebtList);
//------------------------------------------------------------------------------        
        ArrayList<EventSalesRule> eventRuleList = new ArrayList<>();

        EventSalesRule esr = new EventSalesRule();
        esr.setProductFamily("Test");
        esr.setBarnd("Test_Brand");
        esr.setModleNo(4);
        esr.setPeriod(2);
        esr.setIntervals(5);
        esr.setAcceptedInteval(3);
        esr.setSalesType(1);
        esr.setPoint(12.5);
        esr.setMinReq(10.3);
        esr.setPointSchemeFlag(1);
        esr.setTotalAchivement(150);

        ArrayList<EventRulePointScheme> pointSchemaList = new ArrayList<>();
        EventRulePointScheme rps = new EventRulePointScheme();
        rps.setSchemeSaleType(1);
        rps.setSchmeNoOfUnit(150);
        rps.setScheamfixedPoint(50);
        rps.setSchmefloatFlag(1);
        rps.setSchmeAddnSalesType(2);
        rps.setSchmeAssignPoint(20);
        pointSchemaList.add(rps);

        esr.setEventRulePointSchemaList(pointSchemaList);
        eventRuleList.add(esr);

        em.setEventSalesRuleList(eventRuleList);

        ArrayList<EventNonSalesRule> eventNonSaleRules = new ArrayList<>();

        EventNonSalesRule nonSale1 = new EventNonSalesRule();
        nonSale1.setDescription("Sample123");
        nonSale1.setTaget(50);
        nonSale1.setPoint(20);
        nonSale1.setSalesType(1);

        EventNonSalesRule nonSale2 = new EventNonSalesRule();
        nonSale2.setDescription("Sample321");
        nonSale2.setTaget(50);
        nonSale2.setPoint(20);
        nonSale2.setSalesType(1);

        eventNonSaleRules.add(nonSale1);
        eventNonSaleRules.add(nonSale2);

        em.setEventNonSalesRuleList(eventNonSaleRules);

        ArrayList<EventAward> eventAwards = new ArrayList<>();

        EventAward ea1 = new EventAward();
        ea1.setPalce("Sigapooer");

        EventAward ea2 = new EventAward();
        ea2.setPalce("Bankok");

        eventAwards.add(ea1);
        eventAwards.add(ea2);

        em.setEvenAwardList(eventAwards);

        ValidationWrapper vw = EventMasterController.editEventMaster(em, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void startEvent() {

        EventMaster em = new EventMaster();

        em.setEventName("Hello");
        em.setEventDesc("Hi hi");
        em.setEndDate("2014/12/31");
        em.setMktMrgComment("What the Hell");
        em.setEventMrgComment("Wath th dfdsfdsf");
        em.setStartDate("2014/12/08");
        em.setThreshold("1");
        em.setEventId(8);

        ValidationWrapper vw = EventMasterController.startEvent(em, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addCreateEvent() {

        EventMaster em = new EventMaster();

        em.setEventName("newcdfdreatrq1");
        em.setEventDesc("uituerj");
        em.setEndDate("2015-01-09");
        em.setMktMrgComment("345345345");
        //em.setEventMrgComment("Wath th dfdsfdsf");
        em.setStartDate("2014-12-17");
        em.setThreshold("4534535");

        //----------------------------------------------------------------------------
        ArrayList<EventBisType> ebtList = new ArrayList<>();

        EventBisType ebt1 = new EventBisType();
        ebt1.setBisStruTypeId(4);
        ebt1.setBisStruTypeName("Territory & Brand Manager");
        ebt1.setBisStruTypeCount(5);

        ArrayList<EventBussinessList> prt_dtlList = new ArrayList<>();

        EventBussinessList prt_dtl1 = new EventBussinessList();
        prt_dtl1.setBisId(102);
        prt_dtl1.setBisTypeId(4);
        prt_dtl1.setBisName("Territory Manager Colombo North");
        prt_dtl1.setBisTypeDesc("Territory & Brand Manager");
        prt_dtl1.setUserId("tmcn1");

        EventBussinessList prt_dtl2 = new EventBussinessList();
        prt_dtl2.setBisId(173);
        prt_dtl2.setBisName("Territory Negombo");
        prt_dtl2.setBisDesc("Territory & Brand Manager");
        prt_dtl2.setBisName("tmnego");

        prt_dtlList.add(prt_dtl1);
        prt_dtlList.add(prt_dtl2);

        ebt1.setEventBussinessList(prt_dtlList);
//------------------------------------------------------------------------------
        EventBisType ebt12 = new EventBisType();
        ebt12.setBisStruTypeId(3);
        ebt12.setBisStruTypeCount(3);
        ebt12.setBisStruTypeName("Business Development & Operation Manager");

        ArrayList<EventBussinessList> prt_dtlList1 = new ArrayList<>();

        EventBussinessList prt_dtl12 = new EventBussinessList();
        prt_dtl12.setBisId(141);
        prt_dtl12.setBisName("QA Test");
        prt_dtl12.setBisDesc("Business Development & Operation Manager");
        prt_dtl12.setUserId("fds");

//        EventBussinessList prt_dtl22 = new EventBussinessList();
//        prt_dtl22.setBisId(115);
//        prt_dtl22.setBisName("np1");
        prt_dtlList1.add(prt_dtl12);
//        prt_dtlList1.add(prt_dtl22);

        ebt12.setEventBussinessList(prt_dtlList1);

//------------------------------------------------------------------------------ 
        ebtList.add(ebt1);
        ebtList.add(ebt12);
        em.setEventBisTypeList(ebtList);
//------------------------------------------------------------------------------        
        ArrayList<EventSalesRule> eventRuleList = new ArrayList<>();

        EventSalesRule esr = new EventSalesRule();
        esr.setProductFamily("SAMSUNG");
        esr.setBarnd("PHONE - SMART");
        esr.setModleNo(35);
        esr.setPeriod(2);
        esr.setIntervals(45);
        esr.setAcceptedInteval(5);
        esr.setSalesType(1);
        esr.setPoint(5);
        esr.setMinReq(5);
        esr.setPointSchemeFlag(0);
        esr.setTotalAchivement(0);

        ArrayList<EventRulePointScheme> rulePointSchme = new ArrayList<>();

//        EventRulePointScheme ps = new EventRulePointScheme();
//        ps.setSalesType(1);
//        ps.setNoOfUnit(150);
//        ps.setFixedPoint(50);
//        ps.setFloatFlag(1);
//        ps.setAddnSalesType(2);
//        ps.setAssignPoint(20);
//        rulePointSchme.add(ps);
        esr.setEventRulePointSchemaList(rulePointSchme);

        eventRuleList.add(esr);

        em.setEventSalesRuleList(eventRuleList);

        ArrayList<EventNonSalesRule> eventNonSaleRules = new ArrayList<>();

        EventNonSalesRule nonSale1 = new EventNonSalesRule();
        nonSale1.setDescription("5453535");
        nonSale1.setTaget(435);
        nonSale1.setPoint(535);
        nonSale1.setSalesType(2);

//        EventNonSalesRule nonSale2 = new EventNonSalesRule();
//        nonSale2.setDescription("Sample321");
//        nonSale2.setTaget(50);
//        nonSale2.setPoint(20);
//        nonSale2.setSalesType(1);
        eventNonSaleRules.add(nonSale1);
        //       eventNonSaleRules.add(nonSale2);

        em.setEventNonSalesRuleList(eventNonSaleRules);

        ArrayList<EventAward> eventAwards = new ArrayList<>();

        EventAward ea1 = new EventAward();
        ea1.setPalce("534535");
        ea1.setAward("533545");

//        EventAward ea2 = new EventAward();
//        ea2.setPalce("Bankok");
        eventAwards.add(ea1);
        //   eventAwards.add(ea2);

        em.setEvenAwardList(eventAwards);

        ValidationWrapper vw = EventMasterController.addEventMaster(em, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getBussinessName() {

        //MessageWrapper mw = WarrantyAdjustmentController.getWarrantyAdjustmentDetails("all", 0, "extendedDays", "DESC", 0, 1000);
        System.out.println("Total   " + BussStrucController.getBussinessName("3"));

    }

    public static void getWarrantyAdjusment() {

        MessageWrapper mw = WarrantyAdjustmentController.getWarrantyAdjustmentDetails("all", 0, "extendedDays", "DESC", 0, 1000);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<WarrantyAdjustment> woLost = mw.getData();

        for (WarrantyAdjustment wo : woLost) {
            System.out.print("    " + wo.getiMEI());
            System.out.print("    " + wo.getExtendedDays());
            System.out.print("    " + wo.getWarrantyId());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getBussinessList() {

        // MessageWrapper mw = EventMasterController.getBussinessStructTypeList();
        MessageWrapper mw = EventMasterController.getBussinessList("1,2,3", "all", "all", "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventBussinessList> woList = mw.getData();

        for (EventBussinessList wo : woList) {
            System.out.print("    " + wo.getBisName());
            System.out.print("    " + wo.getUserId());
            System.out.println("    " + wo.getBisTypeDesc());

        }

    }

    public static void getBisTypeList() {

        MessageWrapper mw = EventMasterController.getBussinessStructTypeList();

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventBisType> woLost = mw.getData();

        for (EventBisType wo : woLost) {
            System.out.print("    " + wo.getBisStruTypeId());
            System.out.print("    " + wo.getBisStruTypeName());
            System.out.println("    " + wo.getBisStruTypeCount());

        }

    }

    public static void getEventPointDetails() {

        MessageWrapper mw = EventAssignMarkController.getEventAssignMarksDetails("subdc", 1);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventTransaction> woLost = mw.getData();

        for (EventTransaction wo : woLost) {
            System.out.print("    " + wo.getEventId());
            System.out.print("    " + wo.getNonSaleId());
            System.out.print("    " + wo.getRemarks());
            System.out.print("    " + wo.getUserId());
            System.out.print("    " + wo.getPointAddDate());
            System.out.println("    " + wo.getStatus());

            ArrayList<EventNonSalesRule> nsList = wo.getEventNonSalesRuleList();

            for (EventNonSalesRule ensr : nsList) {
                System.out.print("   " + ensr.getEventId());
                System.out.print("   " + ensr.getDescription());
                System.out.print("   " + ensr.getNonSalesRule());
                System.out.print("   " + ensr.getPoint());
                System.out.println("   " + ensr.getStatus());
            }

        }

    }

    public static void getEventPointList() {

        MessageWrapper mw = EventPromotionController.getPromotionPointList("shk91");

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventMaster> woLost = mw.getData();

        for (EventMaster wo : woLost) {
            System.out.print("    " + wo.getEventId());
            System.out.print("    " + wo.getEventName());
            System.out.print("    " + wo.getEventDesc());
            System.out.print("    " + wo.getStartDate());
            System.out.print("    " + wo.getEndDate());
            System.out.print("    " + wo.getThreshold());
            System.out.print("    " + wo.getStatusMsg());
            System.out.println("    " + wo.getEventPoint());

        }

    }

    public static void getEventPromotionList() {

        MessageWrapper mw = EventPromotionController.getEventPromotionList("shk91");

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventMaster> woLost = mw.getData();

        for (EventMaster wo : woLost) {
            System.out.print("    " + wo.getEventId());
            System.out.print("    " + wo.getEventName());
            System.out.print("    " + wo.getEventDesc());
            System.out.print("    " + wo.getStartDate());
            System.out.print("    " + wo.getEndDate());
            System.out.print("    " + wo.getThreshold());
            System.out.print("    " + wo.getStatusMsg());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getEventMaster() {

        MessageWrapper mw = EventMasterController.getEventMasterList(0, "all", "all", "all", "all", "all", "all", 1, "all", "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<EventMaster> woLost = mw.getData();

        for (EventMaster wo : woLost) {
            System.out.print("    " + wo.getEventId());
            System.out.print("    " + wo.getEventName());
            System.out.print("    " + wo.getEventDesc());
            System.out.print("    " + wo.getStartDate());
            System.out.print("    " + wo.getEndDate());
            System.out.print("    " + wo.getThreshold());
            System.out.print("    " + wo.getStatusMsg());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void resetUserPassword() {

        ResponseWrapper vw = UserController.resetUserPassword("subcna3", "sajithd@dmssw.com", "hasha41", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");

        System.out.println("Flage   " + vw.getFlag());
        System.out.println("Exception   " + vw.getExceptionMessages());
        System.out.println("Sucess   " + vw.getSuccessMessages());
        System.out.println("Mesg   " + vw.getTotalRecords());

    }

    public static void addClearTransit() {

        DeviceIssue dt = new DeviceIssue();
        dt.setIssueNo(2);
        dt.setStatus(5);
        dt.setRemarks("Sample Clear");

        ArrayList<DeviceIssueIme> dlit = new ArrayList<>();

        DeviceIssueIme dti1 = new DeviceIssueIme();
        dti1.setIssueNo(2);
        dti1.setImeiNo("22");

        DeviceIssueIme dti2 = new DeviceIssueIme();
        dti2.setIssueNo(2);
        dti2.setImeiNo("15");

        dlit.add(dti1);
        dlit.add(dti2);

        dt.setDeviceImei(dlit);

        ValidationWrapper vw = ClearTransitItemController.addClearTransit(dt, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addBussnessStrut() {

        BussinessStructure bs = new BussinessStructure();
        // bs.setBisId(93);
        bs.setBisName("Sample");
        bs.setBisDesc("Sample 2");
        bs.setBisPrtnId(2);
        bs.setCategory1(5);
        bs.setCategory2(5);
        bs.setLatitude("1");
        bs.setLogitude("1");
        bs.setMapFlag(1);
        bs.setStatus(1);
        bs.setStructType(7);
        bs.setTeleNumber("1231231231");
        bs.setAddress("asd");
        bs.setBisUserCount(0);
        bs.setErpCode("E2001");
        bs.setPoTitle("1");
        bs.setCheqTitle("E2");
        bs.setEssdAcntNo("01");
        bs.setEddsPct("001");
        bs.setInvoiceNo("E21");

        ValidationWrapper vw = BussStrucController.addBussinessStructure(bs, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }

    public static void getClertTransitDetails() {

        MessageWrapper mw = ClearTransitItemController.getClearTransitDetails(1);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<ClearTransitItem> woLost = mw.getData();

        for (ClearTransitItem wo : woLost) {
            System.out.print("    " + wo.getIssueNo());
            System.out.print("    " + wo.getSeqNo());;
            System.out.println("    " + wo.getImeiNo());

        }

    }

    public static void getClertTransit() {

        MessageWrapper mw = ClearTransitItemController.getClearTransitList(0, "all", 0, "all", 0, "all", "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<DeviceIssue> woLost = mw.getData();

        for (DeviceIssue wo : woLost) {
            System.out.print("    " + wo.getIssueNo());
            System.out.print("    " + wo.getDistributerID());
            System.out.print("    " + wo.getFromName());
            System.out.print("    " + wo.getToName());
            System.out.print("    " + wo.getdSRId());
            System.out.print("    " + wo.getIssueDate());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void genarateEstimate() {

        ValidationWrapper vw = WorkOrderMaintainaceController.GenarateEstimateNo();

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnRefNo());
        System.out.println(vw.getReturnMsg());
    }

    public static void addCategory() {

        Category ct = new Category();
        ct.setCateName("My Name 123");
        ct.setCateDesc("Category Desc 1234");
        ct.setCateStatus(1);

        ValidationWrapper vw = CategoryContraller.addCaterory(ct, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void ValidateIMEI() {

        ValidationWrapper mw = ImeiMasterController.validateImeiInLocation("4543", 1);

        System.out.println("Flag   " + mw.getReturnFlag());
        System.out.println("Mesage   " + mw.getReturnMsg());

    }

    public static void getLowlevelBisList() {

        MessageWrapper mw = UserTypeListController.getLowLevelBussinessList(1, 3);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<BussinessStructure> woLost = mw.getData();

        for (BussinessStructure wo : woLost) {
            System.out.print("    " + wo.getBisId());
            System.out.print("    " + wo.getBisName());
            System.out.println("    " + wo.getBisDesc());

        }

    }

    public static void getDELeveryType() {

        MessageWrapper mw = WorkOrderCloseController.getDeleveryType();

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<MdeCode> woLost = mw.getData();

        for (MdeCode wo : woLost) {
            System.out.print("    " + wo.getDescription());
            System.out.print("    " + wo.getCode());
            System.out.println("    " + wo.getDomainName());

        }

    }

    public static void addWorkOrderClose() {

        WorkOrderClose wp = new WorkOrderClose();
        wp.setGatePass("rrr");
        wp.setCourierNo("");
        wp.setWorkOrderNo("WO000170");
        wp.setDeleverDate("2015-02-09");
        wp.setRepairid(2);
        wp.setDeleveryId(3);
        wp.setLocationId(94);
        wp.setStatus(3);

        ValidationWrapper vw = WorkOrderCloseController.addWorkOrderClose(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void verifyWorkOrderClose() {

        ValidationWrapper mw = WorkOrderCloseController.verifyWorkOrder("WO000114");

        System.out.println("Falg   " + mw.getReturnFlag());
        System.out.println("Msg  " + mw.getReturnMsg());

    }

    public static void getWorkOrderClose() {

        MessageWrapper mw = WorkOrderCloseController.getWorkOrderClossing("all", "all", "all", "all", "all", "all", "all", 229, "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<WorkOrderClose> woLost = mw.getData();

        for (WorkOrderClose wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getDeleverDate());
            System.out.print("    " + wo.getDeleveryType());
            System.out.print("    " + wo.getGatePass());
            System.out.print("    " + wo.getRepairSatus());
            System.out.print("    " + wo.getTransferLocation());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getTrackingHistoryList() {

        //MessageWrapper mw = DSRTrakingController.getTrackingHistoryList(64, "2014/12/10 19:23:11", "2014/12/12 19:23:11", 0, 100);
        MessageWrapper mw = DSRTrakingController.getTrackingHistoryList(0,"all", "all", "all", 0, 100);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<DSRTrack> woLost = mw.getData();

        for (DSRTrack wo : woLost) {
            System.out.print("    " + wo.getBisId());
            System.out.print("    " + wo.getLogtitute());
            System.out.print("    " + wo.getLatitite());
            System.out.println("    " + wo.getVistiorBisId());

        }

    }

    public static void addLocationTracking() {

        DSRTrack dt = new DSRTrack();
        dt.setBisId(108);
        //dt.setVistiorBisId(5);
        dt.setLogtitute("3.25");
        dt.setUserId("shk91 ");
        dt.setLatitite("4.23");
        dt.setVisitDate("2014-11-24 09:23");

        ValidationWrapper vw = DSRTrakingController.addLocationTracking(dt, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getLevels() {

        MessageWrapper mw = LevelContraller.getLevels(0, "all", "all", "all", 0, 0, 0, "all", "all", 0, 200);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<RepairLevel> woLost = mw.getData();

        for (RepairLevel wo : woLost) {
            System.out.print("    " + wo.getLevelId());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getLevelDesc());
            System.out.println("    " + wo.getLevelPrice());

        }

    }

    public static void getRepairLevelsForImei() {

        MessageWrapper mw = LevelContraller.getRepairLevelsForImei("911371452107470");

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<RepairLevel> woLost = mw.getData();

        for (RepairLevel wo : woLost) {
            System.out.print("    " + wo.getLevelId());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getLevelDesc());
            System.out.println("    " + wo.getLevelPrice());

        }

    }

    public static void addEnabletracking() {

        ArrayList<DSRTrack> trList = new ArrayList<>();

        DSRTrack dt1 = new DSRTrack();
        dt1.setUserId("shk91");
        dt1.setEnableFlag(1);

        DSRTrack dt2 = new DSRTrack();
        dt2.setUserId("Z01");
        dt2.setEnableFlag(1);

        trList.add(dt1);
        trList.add(dt2);

        ValidationWrapper vw = DSRTrakingController.addEnableTracking(trList, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getTrackingEnableList() {

        MessageWrapper mw = DSRTrakingController.getTrackingEnableList(94, 3);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<DSRTrack> woLost = mw.getData();

        for (DSRTrack wo : woLost) {
            System.out.print("    " + wo.getBisId());
            System.out.print("    " + wo.getUserId());
            System.out.print("    " + wo.getFirstName());
            System.out.print("    " + wo.getLastName());
            System.out.print("   Paraent   " + wo.getParantName());
            System.out.println("    " + wo.getEnableFlag());

        }

    }

    public static void getDSRMovement() {

        MessageWrapper mw = DSRTrakingController.getLastTrackingDSRList(94, 1);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<DSRTrack> woLost = mw.getData();

        for (DSRTrack wo : woLost) {
            System.out.print("    " + wo.getBisId());
            System.out.print("    " + wo.getBisName());
            System.out.print("    " + wo.getTelePhoneNum());
            System.out.print("    " + wo.getVisitDate());
            System.out.print("    " + wo.getBisTypeDesc());
            System.out.print("    " + wo.getAddress());
            System.out.println("    " + wo.getParantName());

        }

    }

    public static void getTrackingList() {

        MessageWrapper mw = DSRTrakingController.getBussinessTrackingList(105, 0, 2, 0, 0);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<DSRTrack> woLost = mw.getData();

        for (DSRTrack wo : woLost) {
            System.out.print("    " + wo.getBisId());
            System.out.print("    " + wo.getBisName());
            System.out.print("    " + wo.getBisDesc());
            System.out.print("    " + wo.getLogtitute());
            System.out.print("    " + wo.getLatitite());
            System.out.print("    " + wo.getBisTypeDesc());
            System.out.println("    " + wo.getBisTypeId());

        }

    }

    public static void deleteWorkOrderTransfer() {

        WorkOrderTransfer wp = new WorkOrderTransfer();
        wp.setWoTransNo(2);

        ValidationWrapper vw = WorkOrderTransferController.deleteWorkOrderTransfer(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void editWorkOrderTransfer() {

        WorkOrderTransfer wp = new WorkOrderTransfer();
        wp.setWoTransNo(24);
        wp.setBisId(94);
        wp.setWorkOrderNo("WO000011");
        wp.setTransferDate("2015-01-07");
        wp.setCourierEmail("");
        wp.setTransferReason("can not repair display here");
        wp.setDeleveryType(1);
        wp.setDeleverDetils("by van we can deliver");
        wp.setTansferLocationId(108);
        wp.setStatus(2);

        ValidationWrapper vw = WorkOrderTransferController.editWorkOrderTransfer(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void verifyWorkOrderTransfer() {

        ValidationWrapper mw = WorkOrderTransferController.verifyWorkOrder("WO000111");

        System.out.println("Falg   " + mw.getReturnFlag());
        System.out.println("Msg  " + mw.getReturnMsg());

    }

    public static void getWorkOrderTransfer() {

        MessageWrapper mw = WorkOrderTransferController.getWorkOrderTransfer("all", "all", 0, "all", "all", 0, "all", 0, "all", 0, "all", "all", "all", "all", "all", "all", 0, 1000);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<WorkOrderTransfer> woLost = mw.getData();

        for (WorkOrderTransfer wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getBisName());
            System.out.print("    " + wo.getDeleverDetils());
            System.out.print("    " + wo.getDeleveryTypeDesc());
            System.out.print("    " + wo.getTransferDate());
            System.out.print("    " + wo.getTransferReason());
            System.out.print("   " + wo.getDeleveryType());
            System.out.print("  " + wo.getBisId());
            System.out.print("    " + wo.getWoTransNo());
            System.out.print("    " + wo.getStatusMsg());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void addWorkOrderTransfer() {

        WorkOrderTransfer wp = new WorkOrderTransfer();
        wp.setBisId(94);
        wp.setWorkOrderNo("WO000009");
        wp.setTransferDate("2014/12/31");
        wp.setDeleverDetils("sg");
        wp.setDeleveryType(1);
        wp.setCourierEmail("test@mail.com");
        wp.setTransferReason("Sample");
        wp.setStatus(1);

        ValidationWrapper vw = WorkOrderTransferController.addWorkOrderTransfer(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addWorkOrderEstimate() {

        WorkOrderEstimate wp = new WorkOrderEstimate();
        wp.setCostLabor(100);
        wp.setWorkOrderNo("WO000011");
        wp.setCostSparePart(500);
        wp.setImeiNo("235");
        wp.setBisId(1);
        wp.setStatus(1);

        ValidationWrapper vw = WorkOrderMaintainaceController.addWorkOrderEstimate(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void deleteWorkOrderMaintainance() {

        WorkOrderMaintain wp = new WorkOrderMaintain();
        wp.setWorkOrderNo("WO000011");
        wp.setWorkorderMaintainId(2);

        ValidationWrapper vw = WorkOrderMaintainaceController.deleteWorkOrderMaintainces(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void editWorkOrderMaintainance() {

        WorkOrderMaintain wp = new WorkOrderMaintain();
        wp.setWorkOrderNo("WO000020");
        wp.setWarrantyVrifType(1);
        wp.setNonWarrantyType(1);
        wp.setActionTaken("Sample Test");
        wp.setTechnician("Tec 001");
        wp.setRepairLevelId(1);
        wp.setRepairStatus(2);
        wp.setEstimateClosingDate("2014/12/31");
        wp.setWorkorderMaintainId(2);

        ArrayList<WorkOrderMaintainDetail> mainTainDetails = new ArrayList<>();

        WorkOrderMaintainDetail wodetails = new WorkOrderMaintainDetail();
        wodetails.setWrokOrderNo("WO000020");
        wodetails.setPartNo(1);
        wodetails.setPartDesc("Display");
        wodetails.setPartSellPrice(152.25);
        wodetails.setThirdPartyFlag(1);
        wodetails.setInvoiceNo("1112");
        mainTainDetails.add(wodetails);

        wp.setWoDetials(mainTainDetails);

        ArrayList<WorkOrderDefect> mainTainDefect = new ArrayList<>();

        WorkOrderDefect wod = new WorkOrderDefect();
        wod.setMajorCode(1);
        wod.setMinorCode(1);
        wod.setRemarks("Hello");

        WorkOrderDefect wod1 = new WorkOrderDefect();
        wod1.setMajorCode(1);
        wod1.setMinorCode(1);
        wod1.setRemarks("Hi");

        mainTainDefect.add(wod);
        mainTainDefect.add(wod1);

        wp.setWoDefect(mainTainDefect);

        ArrayList<WorkOrderEstimate> mainTainEstimate = new ArrayList<>();

        WorkOrderEstimate woe = new WorkOrderEstimate();
        woe.setWorkOrderNo("WO000020");
        woe.setImeiNo("300000000000008");
        woe.setBisId(94);
        woe.setPaymentOption(1);
        woe.setStatus(1);
        woe.setCustomerRef("Estimated");
        mainTainEstimate.add(woe);

        wp.setWoEstimate(mainTainEstimate);

        ValidationWrapper vw = WorkOrderMaintainaceController.editWorkOrderMaintainces(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addWorkOrderMaintainance() {

        WorkOrderMaintain wp = new WorkOrderMaintain();
        wp.setWorkOrderNo("WO000003");
        wp.setWarrantyVrifType(1);
        wp.setNonWarrantyType(1);
        wp.setActionTaken("asd");
        wp.setTechnician("asd");
        wp.setRepairLevelId(1);
        wp.setRepairStatus(1);
        wp.setEstimateClosingDate("2014-12-02");
        wp.setRemarks("asd");
        wp.setIemiNo("300000000000009");

        ArrayList<WorkOrderMaintainDetail> mainTainDetails = new ArrayList<>();

        WorkOrderMaintainDetail wodetails = new WorkOrderMaintainDetail();
        wodetails.setWrokOrderNo("WO000003");
        wodetails.setPartNo(1);
        wodetails.setPartDesc("Display");
        wodetails.setPartSellPrice(85.35);
        wodetails.setThirdPartyFlag(1);
        wodetails.setInvoiceNo("1");
        mainTainDetails.add(wodetails);

        wp.setWoDetials(mainTainDetails);

        ArrayList<WorkOrderDefect> mainTainDefect = new ArrayList<>();

        WorkOrderDefect wod = new WorkOrderDefect();
        wod.setMajorCode(1);
        wod.setMinorCode(1);
        wod.setRemarks("0");
        mainTainDefect.add(wod);

        wp.setWoDefect(mainTainDefect);

        ValidationWrapper vw = WorkOrderMaintainaceController.addWorkOrderMaintainces(wp, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getWorkOrderMaintainDealsOne() {

        MessageWrapper mw = WorkOrderMaintainaceController.getWorkOrderMaintainaceDetails(2);

        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<WorkOrderMaintain> woLost = mw.getData();

        for (WorkOrderMaintain wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getWorkorderMaintainId());
            System.out.print("    " + wo.getIemiNo());
            System.out.print("    " + wo.getRemarks());
            System.out.print("    " + wo.getModelDesc());
            System.out.print("    " + wo.getWarrantyTypedesc());
            System.out.print("    " + wo.getNonWarrantyTypeDesc());
            System.out.print("    " + wo.getTechnician());
            System.out.println("    " + wo.getLevelDesc());
            ArrayList<WorkOrderMaintainDetail> list = wo.getWoDetials();

            for (WorkOrderMaintainDetail wd : list) {
                System.out.print("  " + wd.getPartDesc());
                System.out.println("  " + wd.getPartNo());
            }

            ArrayList<WorkOrderDefect> defctlist = wo.getWoDefect();

            for (WorkOrderDefect wdf : defctlist) {
                System.out.print(wdf.getMajDesc());
                System.out.println(wdf.getMinDesc());
            }

            ArrayList<WorkOrderEstimate> estimatelist = wo.getWoEstimate();

            for (WorkOrderEstimate wdf : estimatelist) {
                System.out.print(wdf.getEsRefNo());
                System.out.print("  " + wdf.getWorkOrderNo());
                System.out.print("  " + wdf.getImeiNo());
                System.out.print("  " + wdf.getPaymentOption());
                System.out.print("  " + wdf.getCustomerRef());
                System.out.print("  " + wdf.getCostSparePart());
                System.out.println("  " + wdf.getStatus());
            }

        }

    }

    public static void getWorkOrderMaintainList() {
//                                                                            1      2      3     4       5      6      7       8     9     10       11      12    13      14    15     16    17  18    19 20 21 22  23     24     25     26    27 28 29  30     31      32   33   34    35    36  37
        //MessageWrapper mw = WorkOrderMaintainaceController.getWorkOrderList("all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", 0, "all", 0, 0, 0, 0, "all", "all", "all", "all", 0, 0, 0, "all", "all", "all", 0, 0, "all", "all", "all", 0, 200);
        //                      1             2            3             4               5      6          7       8      9     10               11              12              13            14           15           16           17    18       19          20             21             22                 23          24          25       26         27            28              29               30         31          32      33     34     35   36      37    
        //    getWorkOrderList(workorderno,customername,customeraddress,worktelephoneNo,email,customernic,imeino,product,brand,modle_desc,assessoriesretained,mjr_defect_desc,min_defect_desc,rccreference,dateofsale,proofofpurches,bisid,bis_name,schemeid,workorderstatus,warrentyvriftype,nonwarrentyvriftype,tecnician,actiontaken,level_desc,remarks, repairstatus, deleverytype, transferlocation, courierno, deleverydate, gatepass,status,order,type, start, limit)

        //MessageWrapper mw = WorkOrderController.getWorkOrderList("all", "all", "all", "all", "all", "all", "all", "all", "all", 0, "all", "all", "all", "all", "all", 0, 0, 0, 0, 0, "all", "all", 0, "all", 0, 0, 0, "all", "all", "all", 0, "all", "all", 94, "all", "all", "all", "all", "all", "all", 0, 1000);
        MessageWrapper mw = WorkOrderMaintainaceController.getWorkOrderList_To_Add_Maintain("all", "all", "all", "all", "all", "all", "all", "all", "all", 0, "all", "all", "all", "all", "all", 94, 0, 0, 0, 0, "all", "all", 0, "all", 0, 0, 0, "all", "all", "all", 0, "all", "all", 94, "all", "all", "all", "all", "all", 0, 1000);
        System.out.println("Total   " + mw.getTotalRecords());

        ArrayList<WorkOrder> woLost = mw.getData();

        for (WorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getRepairStatusMsg());
            System.out.print("    " + wo.getRepairDate());
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getWorkTelephoneNo());
            System.out.print("    " + wo.getCustomerAddress());
            System.out.print("    " + wo.getCustomerName());
            System.out.print("    " + wo.getCustomerNic());
            System.out.print("    " + wo.getEmail());
            System.out.print("    " + wo.getModleDesc());
            System.out.print("    " + wo.getMjrDefectDesc());
            System.out.print("    " + wo.getMinDefecDesc());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getBinName());
            System.out.print("    " + wo.getEmail());
            System.out.print("    " + wo.getEstimateStatusMsg());
            System.out.print("    " + wo.getStatusMsg());
            System.out.print("    " + wo.getRepairPaymentStatusMsg());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getCreateDate());
            System.out.print("    " + wo.getWarrantyStatusMsg());
            System.out.print("    " + wo.getEstimateStatus());
            System.out.println("    " + wo.getImeiNo());

        }

    }

    public static void getApprovePaymentHeaderDtails() {

        MessageWrapper mw = WorkOrderApproveController.getApprovePaymentHeader(1);

        System.out.println("Total  " + mw.getTotalRecords());

        ArrayList<PaymentApprove> woLost = mw.getData();

        for (PaymentApprove wo : woLost) {
            System.out.print("    " + wo.getPaymentId());
            System.out.print("    " + wo.getPaymentPrice());
            System.out.print("    " + wo.getStatus());
            System.out.print("    " + wo.getTax());
            System.out.print("    " + wo.getEssdAccountNo());
            System.out.print("    " + wo.getChequeDate());
            System.out.print("    " + wo.getChequeDetails());
            System.out.print("    " + wo.getChequeNo());
            System.out.print("    " + wo.getPoNumber());
            System.out.print("    " + wo.getPoDate());
            System.out.print("    " + wo.getSettleAmount());
            System.out.println("    " + wo.getPoNumber());

        }
    }

    public static void addcheequAmount() {

        PaymentApprove wp1 = new PaymentApprove();
        wp1.setPaymentId(1);
        wp1.setChequeNo("5645");
        wp1.setChequeDetails("to account");
        wp1.setChequeDate("2014/12/31");

        ValidationWrapper vw = WorkOrderApproveController.addChequeDetails(wp1, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addSettleAmount() {

        PaymentApprove wp1 = new PaymentApprove();
        wp1.setPaymentId(1);
        wp1.setPoNumber("11");
        wp1.setPoDate("2014/12/10");
        wp1.setSettleAmount(500);
        wp1.setRemarks("test");

        ValidationWrapper vw = WorkOrderApproveController.addSettleAmount(wp1, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addApprovePayment() {

        PaymentApprove wp1 = new PaymentApprove();
        wp1.setPaymentId(34);
        wp1.setStatus(2);

//        ValidationWrapper vw = WorkOrderApproveController.addApprovePayment(wp1, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

  //      System.out.println("Flage   " + vw.getReturnFlag());
    //    System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void editApprovePayment() {

        WorkOrderPaymentDetail wp1 = new WorkOrderPaymentDetail();
        wp1.setWorkOrderNo("WO000005");
        wp1.setStatus(2);

        ValidationWrapper vw = WorkOrderApproveController.editApprovePayment(wp1, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getApprovePaymentDetails() {

//        MessageWrapper mw = WorkOrderApproveController.getApprovePaymentDetails(40);
//
//        System.out.println("Total  " + mw.getTotalRecords());
//
//        ArrayList<WorkOrderPaymentDetail> woLost = mw.getData();
//
//        for (WorkOrderPaymentDetail wo : woLost) {
//            System.out.print("    " + wo.getPaymentId());
//            System.out.print("    " + wo.getImeiNo());
//            System.out.print("    " + wo.getStatus());
//            System.out.print("    " + wo.getAmount());
//            System.out.print("    " + wo.getWarrentyTypeDesc());
//            System.out.print("    " + wo.getDefectDesc());
//            System.out.print("    " + wo.getWorkOrderNo());
//            System.out.println("    " + wo.getWarrantyVerifType());
//
//        }

    }

    public static void getApprovePaymentList() {


        MessageWrapper mw = WorkOrderApproveController.getApprovePaymentList("all", 0, 10, "all", 0, 0, "all", "all", "all", 0, "all", "all", "all", "all", "all", "all", 1,2);

        System.out.println("Total  " + mw.getTotalRecords());

        ArrayList<PaymentApprove> woLost = mw.getData();

        for (PaymentApprove wo : woLost) {
            System.out.print("    " + wo.getPaymentId());
            System.out.print("    " + wo.getSetviceName());
            System.out.print("    " + wo.getPaymentPrice());
            System.out.print("    " + wo.getStatus());
            System.out.print("    " + wo.getTax());
            System.out.print("    " + wo.getEssdAccountNo());
            System.out.println("    " + wo.getPoNumber());
            System.out.println("DELAY DATE "+wo.getDelayDate());
            
        }

    }
    
    public static void addWorkOrderGridUpdate() {


        MessageWrapper mw = WorkOrderApproveController.addWorkOrderGridUpdate("WO000548",63,980,5,3,2,1);

        System.out.println("Total  " + mw.getTotalRecords());

        ArrayList<PaymentApprove> woLost = mw.getData();

        for (PaymentApprove wo : woLost) {
           System.out.print("    " + wo.getPaymentId());
           System.out.print("    " + wo.getSetviceName()); 
            
        }

    }

    public static void addWorkOrderPayment() {

        WorkOrderPayment idn = new WorkOrderPayment();

        idn.setAmount(230);
        idn.setEssdAccountNo("1");
        idn.setPaymentDate("");
        idn.setStatus(0);
        idn.setNbtValue(10.00);
        idn.setVatValue(12.00);

        ArrayList<WorkOrderPaymentDetail> woplist = new ArrayList<>();

        WorkOrderPaymentDetail wp1 = new WorkOrderPaymentDetail();
        wp1.setWorkOrderNo("WO000011");
        wp1.setImeiNo("564652131231231");
        wp1.setWarrantyVerifType(2);
        wp1.setDefectType(1257);
        wp1.setAmount(230);
        wp1.setStatus(3);

//        WorkOrderPaymentDetail wp2 = new WorkOrderPaymentDetail();
//        wp2.setWorkOrderNo("WO000011");
//        wp2.setImeiNo("564652131231231");
//        wp2.setWarrantyVerifType(1);
//        wp2.setDefectType(1);
//        wp2.setAmount(750);
        woplist.add(wp1);
//        woplist.add(wp2);

        idn.setWoPaymentDetials(woplist);

        ValidationWrapper vw = WorkPaymentController.addWorkOrderPayment(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Reference    " + vw.getReturnRefNo());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getWorkOrderPaymentPrice() {

        MessageWrapper mw = WorkPaymentController.getWorkOrderPaymentListwithPrice("all", "all", "all", "all", 0, 0.0, 0, "all", "all");

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrderPaymentDetail> woLost = mw.getData();

        for (WorkOrderPaymentDetail wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getImeiNo());
            System.out.print("    " + wo.getWarrentyTypeDesc());
            System.out.print("    " + wo.getDefectDesc());
            System.out.print("    " + wo.getLaborCost());
            System.out.print("    " + wo.getSpareCost());
            System.out.print("    " + wo.getInvoice());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getWorkOrderPayment() {

        MessageWrapper mw = WorkPaymentController.getWorkOrderPaymentList("all", "all", "all", "all", 0, "all", "all", 0, 100);

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrderPaymentDetail> woLost = mw.getData();

        for (WorkOrderPaymentDetail wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getPaymentId());
            System.out.print("    " + wo.getImeiNo());
            System.out.print("    " + wo.getWarrentyTypeDesc());
            System.out.print("    " + wo.getDefectDesc());
            System.out.print("    " + wo.getReferenceNo());
            System.out.print("    " + wo.getStatusMsg());
            System.out.print("    " + wo.getTecnician());
            System.out.print("    " + wo.getActionTake());
            System.out.print("    " + wo.getRemarks());
            System.out.print("    " + wo.getAmount());
            System.out.println("    " + wo.getStatus());

        }

    }

    public static void getWorkOrderPayment_New() {

        MessageWrapper mw = WorkPaymentController.getWorkOrderPaymentList_New(94, 0, 0, "all", "all", "all", "all", "all", 0, "all", "all", 0, 100);

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrderPayment> woLost = mw.getData();

        for (WorkOrderPayment wo : woLost) {
            System.out.print("    " + wo.getPaymentId());
            System.out.print("    " + wo.getAmount());
            System.out.println("    " + wo.getStatus());
            System.out.println("-----------------------------------------");
            for (WorkOrderPaymentDetail wo1 : wo.getWoPaymentDetials()) {
                System.out.print("    " + wo1.getWorkOrderNo());
                System.out.println("    " + wo1.getImeiNo());
            }

        }

    }

    public static void getWorkOrderPaymentList_for_Grid() {

        MessageWrapper mw = WorkPaymentController.getWorkOrderPaymentList_for_Grid(94, 0, 0, "all", "all", "all", "all", "all", 0, "all", "all", 0, 100);

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrderPayment> woLost = mw.getData();

        for (WorkOrderPayment wo : woLost) {
            System.out.print("    " + wo.getPaymentId());
            System.out.print("    " + wo.getAmount());
            System.out.println("    " + wo.getStatus());
            System.out.println("-----------------------------------------");
            for (WorkOrderPaymentDetail wo1 : wo.getWoPaymentDetials()) {
                System.out.print("    " + wo1.getWorkOrderNo());
                System.out.println("    " + wo1.getImeiNo());
            }

        }

    }

    public static void getDeviceExchangeType() {

        MessageWrapper mw = DeviceExchangeController.getDeviceExchangeType();

        System.out.println(mw.getTotalRecords());

        ArrayList<MdeCode> woLost = mw.getData();

        for (MdeCode wo : woLost) {
            System.out.print("    " + wo.getCode());
            System.out.println("    " + wo.getDescription());

        }

    }

    public static void getDeviceExchangeReason() {

        MessageWrapper mw = DeviceExchangeController.getDeviceExchangeReason();

        System.out.println(mw.getTotalRecords());

        ArrayList<MdeCode> woLost = mw.getData();

        for (MdeCode wo : woLost) {
            System.out.print("    " + wo.getCode());
            System.out.println("    " + wo.getDescription());

        }

    }

    public static void getWorkOrderMaintaince() {

        MessageWrapper mw = WarrantyMaintainanceController.getWorkOrderMaintainDetail("WO000002");

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrder> woLost = mw.getData();

        for (WorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getWorkTelephoneNo());
            System.out.print("    " + wo.getCustomerAddress());
            System.out.print("    " + wo.getCustomerName());
            System.out.print("    " + wo.getCustomerNic());
            System.out.print("    " + wo.getEmail());
            System.out.println("    " + wo.getImeiNo());

        }

    }

    public static void cancleransferRepair() {

        TransferRepair idn = new TransferRepair();

        idn.setTransRepairNo(2);

        ValidationWrapper vw = TransferRepairController.cancleTransferRepair(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addTransferRepair() {

        TransferRepair idn = new TransferRepair();

        idn.setImeiNo("123");
        idn.setBisId(13);
        idn.setTransferDate("2014/10/25");

        ValidationWrapper vw = TransferRepairController.addTransferRepair(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void gettranferRepair() {

        MessageWrapper mw = TransferRepairController.getTransferRepair(0, "all", 0, 0, "all", "all", "all", "all", 0, 100);

        System.out.println("Total Recode " + mw.getTotalRecords());

        ArrayList<TransferRepair> mdList = mw.getData();

        for (TransferRepair md : mdList) {
            System.out.print("   " + md.getTransRepairNo());
            System.out.print("   " + md.getImeiNo());
            System.out.print("   " + md.getModleDesc());
            System.out.print("   " + md.getBisId());
            System.out.print("   " + md.getStatus());
            System.out.print("   " + md.getProduct());
            System.out.println("   " + md.getTransferDate());

        }

    }

    public static void getWorkOrderInquire() {

        MessageWrapper mw = WorkOrderInquireController.getWorkOrderInquire("all","all","all", 0, 5);

        System.out.println("Total Recode  " + mw.getTotalRecords());

        ArrayList<WorkOrderInquire> imList = mw.getData();

        for (WorkOrderInquire im : imList) {
            System.out.print("     " + im.getWorkOrderNo());
            System.out.print("     " + im.getDeleveryDate());
            System.out.print("     " + im.getOpenDate());
            System.out.print("     " + im.getProduct());
            System.out.print("     " + im.getWoStatus());
            System.out.print("     " + im.getEstimateNo());
            System.out.println("     " + im.getEstimateStatus());

        }
    }

    public static void getbisType() {

        MessageWrapper mw = BussinessTypeController.getBussinessType(0, "all", 0, 0, "all", "all", 0, 20);

        System.out.println("Total Recode  " + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<BussinessType> imList = mw.getData();

        for (BussinessType im : imList) {
            System.out.print("     " + im.getBisTypeId());
            System.out.print("     " + im.getBisTypeDesc());
            System.out.print("     " + im.getEntyMode());
            System.out.println("     " + im.getStatus());
        }
    }

    public static void addShopVisit() {

        ShopVistCheck idn = new ShopVistCheck();

        idn.setLatitude("96.235");
        idn.setLongtitude("98.654");
        idn.setdSR(108);
        idn.setSubDealer(120);

        ValidationWrapper vw = ShopVisitController.addShopVisitCheck(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getShopVisitCheck() {

        MessageWrapper mw = ShopVisitController.getShopVisitList("6.19", "79.87", 10);

        System.out.println("Total Recode  " + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<ShopVistCheck> imList = mw.getData();

        for (ShopVistCheck im : imList) {
            System.out.print("     " + im.getBisId());
            System.out.print("     " + im.getBisName());
            System.out.print("     " + im.getFirstName());
            System.out.print("     " + im.getLastName());
            System.out.print("     " + im.getLatitude());
            System.out.print("     " + im.getLongtitude());
            System.out.println("     " + im.getStatus());
        }
    }

    public static void getCustomerDetails() {

        MessageWrapper mw = CustomerController.getCustomerDetals("891232536V");

        System.out.println("Total Recode  " + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<Customer> imList = mw.getData();

        for (Customer im : imList) {
            System.out.print("     " + im.getAddress());
            System.out.print("     " + im.getContactNo());
            System.out.print("     " + im.getCustomerNIC());
            System.out.print("     " + im.getCustomerName());
            System.out.print("     " + im.getDateOfBirth());
            System.out.print("     " + im.getEmail());
            System.out.println("     " + im.getStatus());
        }
    }

    public static void getQuickImeiMaster() {

        MessageWrapper mw = ImeiMasterController.getQuickImeiMasterDetails("235", 0, 0, "all", "all", "all", "all", "all", 0, 0, "all", "all", 0, 100);

        System.out.println("Total Recode" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<QuickImeiMaster> imList = mw.getData();

        for (QuickImeiMaster im : imList) {
            System.out.print("     " + im.getImeiNo());
            System.out.print("     " + im.getModleNo());
            System.out.print("     " + im.getModleDesc());
            System.out.println("     " + im.getProduct());
        }
    }

    public static void addWorkOderWarrentyRegistration() {

        WarrentyRegistration idn = new WarrentyRegistration();

        idn.setContactNo("1234567890");
        idn.setCustomerNIC("963258741V");
        idn.setCustomerName("DMS");
        idn.setDateOfBirth("1880/01/22");
        idn.setImeiNo("2321312");
        idn.setEmail("dasunm@dmssw.com");
        idn.setSellingPrice(18);
        idn.setBisId(1);
        idn.setCustomerCode("654");
        idn.setShopCode("456");
        idn.setSiteDesc("Hello");
        idn.setProofPurchas("Hello2");
        idn.setWarantyType(1);
        idn.setModleNo(2);
        idn.setRegisterDate("2014/12/11");
        idn.setOrderNo("D05");

        ValidationWrapper vw = WarrentyRegistrationController.addWorkOrderWarrentyRegistration(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addWarrentyRegistration() {

        WarrentyRegistration idn = new WarrentyRegistration();

        idn.setContactNo("1231231231");
        idn.setCustomerNIC("000000000V");
        idn.setCustomerName("SajithDumesh");
        idn.setDateOfBirth("2015-05-25");
        idn.setImeiNo("911406755159032");
        idn.setEmail("sajithd@dmssw.com");
        idn.setSellingPrice(0);
        idn.setBisId(219);
        idn.setModleDescription("");
        idn.setRegisterDate("2015-02-01");
        idn.setCustomerAddress("123");
        idn.setCustomerCode("33535");
        idn.setModleNo(39);
        idn.setOrderNo("5435");
        idn.setProofPurchas("45435");
        idn.setShopCode("54354");
        idn.setSiteDesc("45345");
        idn.setWarantyType(1);
        idn.setWarrntyRegRefNo(0);
        idn.setProduct("");

        ValidationWrapper vw = WarrentyRegistrationController.addWarrentyRegistration(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getWarrentyRegistation() {

        MessageWrapper mw = WarrentyRegistrationController.getWarrentyRegistration(226, 0, "all", "all", "all", "all", "all", 0, 0, "all", "all", "all", "all", "all", "all", 0, 100);
        System.out.println("Total recode   " + mw.getTotalRecords());

        ArrayList<WarrentyRegistration> crList = mw.getData();

        for (WarrentyRegistration ci : crList) {
            System.out.print("    " + ci.getContactNo());
            System.out.print("    " + ci.getRemainsDays());
            System.out.print("    " + ci.getCustomerNIC());
            System.out.print("    " + ci.getCustomerName());
            System.out.print("    " + ci.getDateOfBirth());
            System.out.print("    " + ci.getImeiNo());
            System.out.print("    " + ci.getModleDescription());
            System.out.print("    " + ci.getProduct());
            System.out.print("    " + ci.getModleNo());
            System.out.print("    " + ci.getSellingPrice());
            System.out.print("    " + ci.getRemainsDays());
            System.out.println("    " + ci.getWarrntyRegRefNo());
        }

    }

    public static void cancleWarrentReplacement() {

        WarrenyReplacement idn = new WarrenyReplacement();

        idn.setSeqNo(6);
        idn.setOldImei("87");
        idn.setNewImei("234");

        ValidationWrapper vw = WarrentyReplacementController.cancleWarrentyReplacement(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addWarrentReplacement() {

        WarrenyReplacement idn = new WarrenyReplacement();

        idn.setWorkOrderNo("WO000015");
        idn.setOldImei("564652131231231");
        idn.setNewImei("202015052050547");
        idn.setBisId(120);
        idn.setExchangeReferenceNo("DE000011");

        ValidationWrapper vw = WarrentyReplacementController.addWarrentyReplacement(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getWarrentyReplacement() {

        MessageWrapper mw = WarrentyReplacementController.getWarrentyReplacement("all", "all", "all", "all", "all", "all", "all", "all", 0, 100);
        System.out.println("Total recode   " + mw.getTotalRecords());

        ArrayList<WarrenyReplacement> crList = mw.getData();

        for (WarrenyReplacement ci : crList) {
            System.out.print("    " + ci.getWorkOrderNo());
            System.out.print("    " + ci.getOldImei());
            System.out.print("    " + ci.getNewImei());
            System.out.print("    " + ci.getCusName());
            System.out.print("    " + ci.getReaminDate());
            System.out.print("    " + ci.getReplaceDate());
            System.out.println("    " + ci.getExchangeReferenceNo());
        }

    }

    public static void addAcceptInventory() {

        AcceptInventory idn = new AcceptInventory();

        idn.setIssueNo(1);
        idn.setStatus(2);
        idn.setDsrId(108);

        ArrayList<AcceptInvenroryImei> idnllist = new ArrayList<>();

        AcceptInvenroryImei idnl = new AcceptInvenroryImei();
        idnl.setIssueNo(1);
        idnl.setImeiNo("789819293834545");
        idnl.setModleNo(2);
        idnl.setBisId(94);
        idnl.setStatus(1);

        idnllist.add(idnl);

        idn.setAcceptInvImei(idnllist);

        ValidationWrapper vw = AcceptInventoryController.addAcceptInventory(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void addQuickWorkOrder() {

        QuickWorkOrder wo = new QuickWorkOrder();
        wo.setCustomerName("chandima nanayakakra");
        wo.setCustomerNic("880801250V");
        wo.setWorkTelephoneNo("2141241244");
        wo.setBisId(94);
        wo.setAssessoriesRetained("Charger,");
        wo.setDefectNo("34");
        //  wo.setDeleveryDate("2014/02/01");
        wo.setEmail("ruwana@dmssw.com");
        wo.setImeiNo("100000000000042");
        wo.setModleNo(28);
        wo.setProduct("SAF");
        wo.setRepairStatus(1);
        // wo.setTransferLocation();

        ValidationWrapper vw = WorkOrderController.addQuickWorkOrder(wo, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void removeQuickWorkOrder() {

        QuickWorkOrder wo = new QuickWorkOrder();

        wo.setWorkOrderNo("WO000046");

        ValidationWrapper vw = WorkOrderController.removeQuickWorkOrder(wo, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void editQuickWorkOrder() {

        QuickWorkOrder wo = new QuickWorkOrder();
        wo.setCustomerName("asd");
        wo.setCustomerNic("131231231V");
        wo.setWorkTelephoneNo("2312312312");
        wo.setBisId(94);
        wo.setAssessoriesRetained("Charger,");
        wo.setDefectNo("34");
        //  wo.setDeleveryDate("2014/02/01");
        wo.setEmail("ruwana@dmssw.com");
        wo.setImeiNo("701346401054644");
        wo.setModleNo(28);
        wo.setProduct("SAF");
        wo.setRepairStatus(1);
        wo.setTransferLocation(94);
        wo.setWorkOrderNo("WO000046");

        ValidationWrapper vw = WorkOrderController.editQuickWorkOrder(wo, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void editImportDebitNote() {

        ImportDebitNote idn = new ImportDebitNote();

        idn.setDebitNoteNo("10");
        idn.setStatus(2);

        ArrayList<ImportDebitNoteDetails> idnllist = new ArrayList<>();

        ImportDebitNoteDetails idnl = new ImportDebitNoteDetails();
        idnl.setDebitNoteNo("10");
        idnl.setImeiNo("9999");
        idnl.setCustomerCode("1");
        idnl.setShopCode("1");
        idnl.setModleNo(1);
        idnl.setBisId(1);
        idnl.setOrderNo("1");
        idnl.setSalerPrice(0);
        idnl.setDateAccepted("2014/04/16");
        idnl.setStatus(2);
        idnl.setRemarks("IMEI is Cleared");

        idnllist.add(idnl);

        idn.setImportDebitList(idnllist);

        ValidationWrapper vw = ImportDebitNoteController.editImportDebitNote(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getAcceptInventoryImei() {

        MessageWrapper mw = AcceptInventoryController.getAcceptInventoryImei(379, 2, 245, 0, 0, 100);
        System.out.println("Total recode   " + mw.getTotalRecords());

        ArrayList<AcceptInvenroryImei> crList = mw.getData();

        for (AcceptInvenroryImei ci : crList) {
            System.out.print("    " + ci.getSeqNo());
            System.out.print("    " + ci.getIssueNo());
            System.out.print("    " + ci.getImeiNo());
            System.out.print("    " + ci.getModleDescription());
            System.out.println("    " + ci.getStatus());
        }

    }

    public static void getAcceptInventory() {

        MessageWrapper mw = AcceptInventoryController.getAcceptInventory(0, 2, "all", 0, 283, "2014/12/16", "2014/12/16", "all", "all", 0, 0, 100);

        System.out.println("Total recode" + mw.getTotalRecords());

        ArrayList<AcceptInventory> crList = mw.getData();

        for (AcceptInventory ci : crList) {
            System.out.print("    " + ci.getIssueNo());
            System.out.print("    " + ci.getIssueDate());
            System.out.print("    " + ci.getStatusMsg());
            System.out.println("    " + ci.getStatus());
        }

    }

    public static void getImeiAdjusmentDetail() {
        try {
            MessageWrapper mw = ImeiAdjusmentController.getImeiAdjusmentDetail("234");

            System.out.println("Total recode : " + mw.getTotalRecords());

            ArrayList<ImeiAdjusment> rr = mw.getData();
            for (ImeiAdjusment rer : rr) {

                System.out.print("    " + rer.getDebitNoteNo());
                System.out.print("    " + rer.getImeiNo());
                System.out.print("    " + rer.getCustomerCode());
                System.out.print("    " + rer.getShopCode());
                System.out.print("    " + rer.getModleNo());
                System.out.print("    " + rer.getBisId());
                System.out.print("    " + rer.getOrderNo());
                System.out.print("    " + rer.getSalerPrice());
                System.out.print("    " + rer.getStatus());

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void getInventoryIssue() {

        MessageWrapper mw = IssueInventoryController.getInventoryIssueList(0, 108, 120, "all", 2, "all", "all", "all", "all", 0, 200);

        System.out.println(mw.getTotalRecords());

        ArrayList<DeviceIssue> woLost = mw.getData();

        for (DeviceIssue wo : woLost) {
            System.out.print("       " + wo.getIssueNo());
            System.out.print("      " + wo.getDistributerID());
            System.out.print("      " + wo.getdSRId());
            System.out.print("      " + wo.getToName());
            System.out.print("    " + wo.getStatusMsg());
            System.out.println("      " + wo.getStatus());

        }
    }

    public static void getMajorDefectCode() {

        MessageWrapper mw = MajorDefectController.getMajorDefects(0, "all", 2, "all", "all", 0, 200);

        System.out.println(mw.getTotalRecords());

        ArrayList<MajorDefect> woLost = mw.getData();

        for (MajorDefect wo : woLost) {
            System.out.print("    " + wo.getMjrDefectCode());
            System.out.print("    " + wo.getMjrDefectDesc());
            System.out.println("    " + wo.getMjrDefectStatus());
        }

    }

    public static void addMajorDefectCode() {

        MajorDefect md = new MajorDefect();
        md.setMjrDefectDesc("System");
        md.setMjrDefectStatus(1);

        ValidationWrapper vw = MajorDefectController.addMajorDefect(md, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Msg    " + vw.getReturnMsg());
    }

    public static void getQuickWorkOrder() {

        MessageWrapper mw = WorkOrderController.getQuickWorkOrder("all", "all", "all", "all", "all","922020370V", "all", "all",219, 0, 100);

        System.out.println(mw.getTotalRecords());

        ArrayList<QuickWorkOrder> woLost = mw.getData();

        for (QuickWorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getImeiNo());
            System.out.print("    " + wo.getServiceCenter());
            System.out.print("    " + wo.getModleDesc());
            System.out.print("    " + wo.getDeleveryDate());
            System.out.print("    " + wo.getStatus());
            System.out.print("    " + wo.getRepairStatusMsq());
            System.out.println("    " + wo.getCreateDate());

        }

    }

    public static void editMajorDefec() {
        MajorDefect md = new MajorDefect();

        md.setMjrDefectCode(8);
        md.setMjrDefectDesc("fgj");
        md.setMjrDefectStatus(2);

        ValidationWrapper vw = MajorDefectController.editMajorDefect(md, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void editMinorCode() {

        MinorDefect md = new MinorDefect();
        md.setMinDefectCode(65);
        md.setMinDefectDesc("System123");
        md.setMrjDefectCode(2);
        md.setMinDefectStatus(1);

        ValidationWrapper vw = MinorDefectController.editMinorDefect(md, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void editCategory() {

        Category ct = new Category();
        ct.setCateId(20);
        ct.setCateName("vc");
        ct.setCateDesc("Managemett");
        ct.setCateStatus(2);

        ValidationWrapper vw = CategoryContraller.editCategory(ct, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void editRepairCategory() {

        RepairCategory rc = new RepairCategory();
        rc.setReCateId(3);
        rc.setReCateDesc("abc");
        rc.setReCateStatus(1);
        rc.setUser("D01");

        ValidationWrapper vw = RepairCategoryController.editRepairCategory(rc, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void deleteDeviceExchange() {

        DeviceExchange de = new DeviceExchange();
        de.setExchangeReferenceNo("de");
        de.setRemarks("Damage");

        ValidationWrapper vw = DeviceExchangeController.editDeviceExchange(de, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());

    }

    public static void addAcceptdebitNote() {

        ImportDebitNote idn = new ImportDebitNote();

        idn.setDebitNoteNo("D11");
        idn.setInvoiceNo("43");
        idn.setContactNo("0123456780");
        idn.setOrderNo("43");
        idn.setPartNo("85");
        idn.setOrderQty(10);
        idn.setDeleveryQty(0);
        idn.setDeleveryDate("");
        idn.setHeadStatus(1);
        idn.setCustomerNo("C004");
        idn.setSiteDescription("Site4");
        idn.setStatus(1);

        ArrayList<ImportDebitNoteDetails> idnllist = new ArrayList<>();

        ImportDebitNoteDetails idnl = new ImportDebitNoteDetails();
        idnl.setDebitNoteNo("D11");
        idnl.setImeiNo("505050064446443");
        idnl.setCustomerCode("C004");
        idnl.setShopCode("");
        idnl.setDateAccepted("2014/11/11");
        idnl.setModleNo(43);
        idnl.setBisId(94);
        idnl.setSalerPrice(0);
        idnl.setOrderNo("43");
        idnl.setRemarks("");
        idnl.setSite("");
        idnl.setStatus(1);
        idnllist.add(idnl);

//        ImportDebitNoteDetails idn2 = new ImportDebitNoteDetails();
//        idn2.setDebitNoteNo("G01");
//        idn2.setImeiNo("100000000000041");
//        idn2.setCustomerCode("C002");
//        idn2.setShopCode("");
//        idn2.setModleNo(0);
//        idn2.setBisId(94);
//        idn2.setSalerPrice(0);
//        idn2.setOrderNo("22");
//        idn2.setRemarks("");
//        idn2.setSite("");
//        idn2.setDateAccepted("");
//        idn2.setStatus(1);
//        idnllist.add(idn2);
        idn.setImportDebitList(idnllist);

        ValidationWrapper vw = AcceptDebitNoteController.addacceptDebitDetials(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flage   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());

    }

    public static void getAcceptDebitnoteDetails() {

        MessageWrapper mw = AcceptDebitNoteController.getAcceptDebitNoteDetial("A11");

        System.out.println(mw.getTotalRecords());

        ArrayList<ImportDebitNote> woLost = mw.getData();

        ArrayList<ImportDebitNoteDetails> impdetails = new ArrayList<>();

        for (ImportDebitNote wo : woLost) {
            System.out.print("    " + wo.getDebitNoteNo());
            System.out.print("    " + wo.getInvoiceNo());
            System.out.print("    " + wo.getContactNo());
            System.out.print("    " + wo.getDeleveryDate());
            System.out.print("    " + wo.getOrderNo());
            System.out.print("    " + wo.getPartNo());
            System.out.println("    " + wo.getSiteDescription());
            impdetails = wo.getImportDebitList();

        }

        System.out.println("---------------------------------------------");

        for (ImportDebitNoteDetails im : impdetails) {
            System.out.print("    " + im.getDebitNoteNo());
            System.out.print("    " + im.getImeiNo());
            System.out.print("    " + im.getOrderNo());
            System.out.print("    " + im.getShopCode());
            System.out.print("    " + im.getSalerPrice());
            System.out.print("    " + im.getModleNo());
            System.out.print("    " + im.getModleDesc());
            System.out.println("");
        }

    }
    
    
     public static void getAcceptDebitNoteDetialOnly() {

        MessageWrapper mw = AcceptDebitNoteController.getAcceptDebitNoteDetialOnly("4786",10,15);

        System.out.println(mw.getTotalRecords());

        ArrayList<ImportDebitNote> woLost = mw.getData();

        ArrayList<ImportDebitNoteDetails> impdetails = new ArrayList<>();

        for (ImportDebitNote wo : woLost) {
            System.out.print("    " + wo.getDebitNoteNo());
            System.out.print("    " + wo.getInvoiceNo());
            System.out.print("    " + wo.getContactNo());
            System.out.print("    " + wo.getDeleveryDate());
            System.out.print("    " + wo.getOrderNo());
            System.out.print("    " + wo.getPartNo());
            System.out.println("    " + wo.getSiteDescription());
            impdetails = wo.getImportDebitList();

        }

        System.out.println("---------------------------------------------");

        for (ImportDebitNoteDetails im : impdetails) {
            System.out.print("    " + im.getDebitNoteNo());
            System.out.print("    " + im.getImeiNo());
            System.out.print("    " + im.getOrderNo());
            System.out.print("    " + im.getShopCode());
            System.out.print("    " + im.getSalerPrice());
            System.out.print("    " + im.getModleNo());
            System.out.print("    " + im.getModleDesc());
            System.out.println("");
        }

    }

    public static void editDeviceExchange() {

        DeviceExchange de = new DeviceExchange();
        de.setExchangeReferenceNo("DE000001");
        de.setWorkOrderNo("WO000001");
        de.setImeiNo("234");
        de.setTypeOfExchange(1);
        de.setBisId(2);
        de.setReason(5);

        ValidationWrapper vw = DeviceExchangeController.editDeviceExchange(de, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void addDeviceExchange() {

        DeviceExchange de = new DeviceExchange();
        de.setWorkOrderNo("WO000001");
        de.setImeiNo("52562626");
        de.setTypeOfExchange(1);
        de.setBrand("z1");
        de.setCustomerNIC("236234634V");
        de.setCustomerName("kushani anurada");
        de.setBisId(94);
        de.setReason(1);
        de.setModelDesc("Ascend P5");
        de.setProduct("sony");
        de.setRemarks("ssss");
        de.setWarrantyDesc("With in Warrenty Period");
        de.setWrtPeriodFlag(1);
        de.setWarrantyRegDate("2014/12/15");
        de.setWorkOrderNo("WO000027");

        ValidationWrapper vw = DeviceExchangeController.addDeviceExchange(de, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void editInventoryIssue() {

        DeviceIssue di = new DeviceIssue();

        di.setIssueNo(29);
        di.setStatus(2);

        ArrayList<DeviceIssueIme> dIme = new ArrayList<DeviceIssueIme>();
        DeviceIssueIme de = new DeviceIssueIme();
        de.setImeiNo("234");
        de.setModleNo(1);
        dIme.add(de);

        di.setDeviceImei(dIme);

        ValidationWrapper vw = IssueInventoryController.editInventoryIssue(di, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void addInventoryIssue() {

        DeviceIssue di = new DeviceIssue();

        // di.setIssueNo(18);
        di.setDistributerID(51);
        di.setdSRId(43);
        di.setIssueDate("2014/10/22");
        di.setStatus(2);

        ArrayList<DeviceIssueIme> dIme = new ArrayList<DeviceIssueIme>();
        DeviceIssueIme de = new DeviceIssueIme();
        de.setImeiNo("456");
        de.setModleNo(1);
        de.setIssueNo(18);
        dIme.add(de);

        di.setDeviceImei(dIme);

        ValidationWrapper vw = IssueInventoryController.addInventoryIssue(di, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

//    public static void getDeviceExchange() {
//
//        MessageWrapper mw = DeviceExchangeController.getDeviceExchangeDetails("all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "all", 0, 200);
//
//        System.out.println("Total recode  :" + mw.getTotalRecords());
//        ArrayList<DeviceExchange> deo = mw.getData();
//
//        for (DeviceExchange de : deo) {
//            System.out.print("   " + de.getExchangeReferenceNo());
//            System.out.print("   " + de.getImeiNo());
//            System.out.print("   " + de.getWorkOrderNo());
//            System.out.print("   " + de.getCustomerName());
//            System.out.print("   " + de.getCustomerNIC());
//            System.out.print("   " + de.getExchangeDesc());
//            System.out.print("   " + de.getWarrantyDesc());
//            System.out.print("   " + de.getReasonDesc());
//            System.out.print("   " + de.getWarrantyRegDate());
//            System.out.print("   " + de.getProduct());
//            System.out.print("   " + de.getBrand());
//            System.out.println("   " + de.getModelDesc());
//        }
//
//    }

    public static void getCustomer() {

        MessageWrapper mw = DeviceIssueController.getDeviceIssueDetail(5);

        System.out.println("Total Recode " + mw.getTotalRecords());

        ArrayList<DeviceIssue> mdList = mw.getData();

        for (DeviceIssue md : mdList) {
            System.out.print("   " + md.getIssueNo());
            System.out.print("   " + md.getdSRId());
            System.out.print("   " + md.getIssueDate());
            System.out.print("   " + md.getDelerMargin());
            System.out.print("   " + md.getDiscount());
            System.out.print("   " + md.getDistributerID());
            System.out.print("   " + md.getDistributorMargin());
            System.out.print("   " + md.getPrice());
            System.out.println("   " + md.getStatus());
            ArrayList<DeviceIssueIme> dime = md.getDeviceImei();
            System.out.println("------------------------------------------");
            for (DeviceIssueIme di : dime) {
                System.out.print("   " + di.getSeqNo());
                System.out.print("   " + di.getImeiNo());
                System.out.print("   " + di.getIssueNo());
                System.out.print("   " + di.getModleNo());
                System.out.println("   " + di.getStatus());
            }

        }

    }

    public static void getDeviceIssueLIst() {

        MessageWrapper mw = DeviceIssueController.getDeviceIssueList(0, 0, 0, "all", 0, 0, 0, 0, 0, "all", "all", 0, 200);

        System.out.println("Total Recode " + mw.getTotalRecords());

        ArrayList<DeviceIssue> mdList = mw.getData();

        for (DeviceIssue md : mdList) {
            System.out.print("   " + md.getIssueNo());
            System.out.print("   " + md.getDtlCnt());
            System.out.print("   " + md.getdSRId());
            System.out.print("   " + md.getIssueDate());
            System.out.print("   " + md.getDelerMargin());
            System.out.print("   " + md.getDiscount());
            System.out.print("   " + md.getDistributerID());
            System.out.print("   " + md.getDistributorMargin());
            System.out.print("   " + md.getPrice());
            System.out.print("   " + md.getFromName());
            System.out.print("   " + md.getToName());
            System.out.println("   " + md.getStatus());

        }

    }

    public static void getInventoryIssuedetail() {

        MessageWrapper mw = IssueInventoryController.getInventoryIssueDetail_To_Mobile(245, 78);

        System.out.println("Total Recode " + mw.getTotalRecords());

        ArrayList<DeviceIssueIme> dime = mw.getData();

        for (DeviceIssueIme di : dime) {
            System.out.print("   " + di.getSeqNo());
            System.out.print("   " + di.getImeiNo());
            System.out.print("   " + di.getIssueNo());
            System.out.print("   " + di.getModleNo());
            System.out.print("   " + di.getModleDesc());
            
           // System.out.print("   " + di.getDistributorCode());
          //  System.out.print("   " + di.getDistributorName());
            
            System.out.println(" sta    " + di.getStatus());
        }

    }

    public static void getDeviceIssuedetail() {

        MessageWrapper mw = DeviceIssueController.getDeviceIssueDetail(1);

        System.out.println("Total Recode " + mw.getTotalRecords());

        ArrayList<DeviceIssueIme> mdList = mw.getData();

//        for (DeviceIssue md : mdList) {
//            System.out.print("   " + md.getIssueNo());
//            System.out.print("   " + md.getdSRId());
//            System.out.print("   " + md.getIssueDate());
//            System.out.print("   " + md.getDelerMargin());
//            System.out.print("   " + md.getDiscount());
//            System.out.print("   " + md.getDistributerID());
//            System.out.print("   " + md.getDistributorMargin());
//            System.out.print("   " + md.getPrice());
//            System.out.println("   " + md.getStatus());
//            ArrayList<DeviceIssueIme> dime = md.getDeviceImei();
//            System.out.println("------------------------------------------");
        for (DeviceIssueIme di : mdList) {
            System.out.print("   " + di.getSeqNo());
            System.out.print("   " + di.getImeiNo());
            System.out.print("   " + di.getIssueNo());
            System.out.print("   " + di.getModleNo());
            System.out.print("   " + di.getModleDesc());
            System.out.print("   " + di.getSalesPrice());
            System.out.print("   " + di.getMargin());
            System.out.println("   " + di.getStatus());
        }

        //   }
    }

    public static void editWorkOrder() {

        WorkOrder wo = new WorkOrder();
        wo.setWorkOrderNo("WO000039");
        wo.setCustomerName("Sajith Dulanga");
        wo.setCustomerAddress("Kurunegala");
        wo.setBisId(4);
        wo.setAssessoriesRetained("aaa");
        wo.setActionTaken("");
        wo.setCustomerNic("890222536V");
        wo.setDateOfSale("2014/01/22");
        wo.setDefectNo("1");
        wo.setDeleveryDate("2014-11-19");
        wo.setDeleveryType(0);
        wo.setEmail("sajaith@dmsss.com");
        wo.setGatePass("");
        wo.setImeiNo("8888888888");
        wo.setModleNo(0);
        wo.setLevelId(1);
        wo.setNonWarrentyVrifType(1);
        wo.setProduct("");
        wo.setProofOfPurches("");
        wo.setRccReference("a");
        wo.setRemarks("w");
        wo.setRepairStatus(0);
        wo.setSchemeId(2);
        wo.setStatus(0);
        wo.setTecnician("");
        wo.setTransferLocation(1);
        wo.setWarrentyVrifType(1);
        wo.setWorkOrderStatus(0);
        wo.setWorkTelephoneNo("0722241067");

        ValidationWrapper vw = WorkOrderController.editWorkOrder(wo, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void addWorkOrder() {

        WorkOrder wo = new WorkOrder();
//        wo.setCustomerName("Dumesh Chanaka");
//        wo.setCustomerAddress("Panadura");
//        wo.setBisId(94);
//        wo.setAssessoriesRetained("retain");
//        wo.setCustomerNic("333333333V");
//        wo.setWorkTelephoneNo("0717893206");
//        //wo.setBrand("123");
//        // wo.setProduct("Sample Pro");
////        wo.setActionTaken("test");
//        wo.setDateOfSale("2014/05/06");
//        wo.setDefectNo("vcx");
//        wo.setDeleveryDate("2014/05/06");
////        wo.setDeleveryType(1);
//        wo.setEmail("sajithd@dmssw.com");
////        wo.setGatePass("test");
//        wo.setImeiNo("2222222222228");
////        wo.setModleNo(1);
////        wo.setLevelId(1);
//        wo.setNonWarrentyVrifType(1);
////        wo.setProduct("test");
////        wo.setProofOfPurches("test");
//        wo.setRccReference("refference");
//        wo.setRemarks("remark");
////        wo.setRepairStatus(1);
////        wo.setSchemeId(2);
////        wo.setStatus(1);
////        wo.setTecnician("tets");
////        wo.setTransferLocation(2);
//        wo.setWarrentyVrifType(1);
        
        wo.setWorkOrderNo("");
        wo.setCustomerName("Mr.Dinakara Samaranayake");
        wo.setCustomerAddress("25,postallane,colombo10");
        wo.setWorkTelephoneNo("0773033330");
        wo.setEmail("dinakara.samaranayake@ag3.lk");
        wo.setCustomerNic("892473536V");
        wo.setImeiNo("911403605545627");
        wo.setProduct("PHONE - MEDIA PAD");
        wo.setBrand("HUAWEI");
        wo.setModleNo(173);
        wo.setAssessoriesRetained("unitonly");
        wo.setDefectNo("22");
        wo.setRccReference("2055903");
        wo.setDateOfSale("2013/01/01");
        wo.setProofOfPurches("");
        wo.setBisId(224);
        wo.setSchemeId(0);
        wo.setWorkOrderStatus(0);
        wo.setWarrantyStatus(0);
        wo.setWarrentyVrifType(0);
        wo.setNonWarrentyVrifType(1);
        wo.setTecnician("");
        wo.setActionTaken("");
        wo.setLevelId(0);
        wo.setRemarks("directcustomer");
        wo.setRepairStatus(0);
        wo.setDeleveryType(0);
        wo.setTransferLocation(0);
        wo.setCourierNo("");
        wo.setDeleveryDate("");
        wo.setGatePass("");
        wo.setStatus(0);
        wo.setStatusMsg("");
        wo.setCreateDate("");
        wo.setDelayDate("");
        wo.setRepairDate("");
        wo.setDefectDesc("");
        wo.setMinorDefectNo(0);
        wo.setMinorDefectDesc("");
        wo.setRepairLevelDesc("");
        wo.setSaleDate("");
        wo.setWarrantyStatus(0);
        wo.setWarrantyStatusMsg("");
        wo.setPaymentStatus(0);
        wo.setMjrDefectDesc("");
        wo.setMinDefecDesc("");
        wo.setModleDesc("");
        wo.setLevelName("");
        wo.setEstimateRefNo("");
        wo.setEstimateStatus(0);
        wo.setEstimateStatusMsg("");
        wo.setRepairPayment(0);
        wo.setRepairStatusMsg("");
        wo.setNonWarrantyRemarks("touch damaged");
        wo.setCustomerComplain("touch only : customer approved");
        wo.setServiceBisId(226);
        wo.setServiceBisDesc("");
        wo.setRegisterDate("");
        wo.setRepairStatusMsg("");
        wo.setExpireStatus(1);

        
        
        
        
        

        ValidationWrapper vw = WorkOrderController.addWorkOrder(wo, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
        System.out.println("Wo Number   " + vw.getReturnRefNo());
        
    }

    public static void deleteWorkOrder() {

        WorkOrder wo = new WorkOrder();
        wo.setWorkOrderNo("WO000041");

        ValidationWrapper vw = WorkOrderController.deleteWorkOrder(wo, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());

    }

    public static void getWorkOrderDetails() {

        MessageWrapper mw = WorkOrderController.getWorkOrderDetails("WO000040");

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrder> woLost = mw.getData();

        for (WorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getWorkTelephoneNo());
            System.out.print("    " + wo.getCustomerAddress());
            System.out.print("    " + wo.getCustomerName());
            System.out.print("    " + wo.getCustomerNic());
            System.out.print("    " + wo.getEmail());
            System.out.println("    " + wo.getImeiNo());

        }

    }

    public static void getWorkOrderPaymentDetails() {

        MessageWrapper mw = WorkPaymentController.getWorkOrderPaymentDetails("WO000005");

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrderPaymentDetail> woLost = mw.getData();

        for (WorkOrderPaymentDetail wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getWarrantyVerifType());
            System.out.print("    " + wo.getStatus());
            System.out.print("    " + wo.getTecnician());
            System.out.print("    " + wo.getActionTake());
            System.out.println("    " + wo.getImeiNo());

            ArrayList<WorkOrderMaintainDetail> dime = wo.getWoDetials();
            System.out.println("------------------------------------------");
            for (WorkOrderMaintainDetail di : dime) {
                System.out.print("   " + di.getPartNo());
                System.out.print("   " + di.getPartDesc());
                System.out.print("   " + di.getPartSellPrice());
                System.out.print("   " + di.getThirdPartyFlag());
                System.out.println("   " + di.getStatus());
            }

        }

    }

    public static void getWorkOrderDetail() {

        MessageWrapper mw = WorkOrderController.getWorkOrderDetail("WO000012");

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrder> woLost = mw.getData();

        for (WorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getWarrantyStatusMsg());
            System.out.print("    " + wo.getRepairStatusMsg());
            System.out.print("    " + wo.getRepairDate());
            System.out.print("    " + wo.getModleDesc());
            System.out.print("    " + wo.getWorkTelephoneNo());
            System.out.print("    " + wo.getCustomerAddress());
            System.out.print("    " + wo.getCustomerName());
            System.out.print("    " + wo.getCustomerNic());
            System.out.print("    " + wo.getEmail());
            System.out.print("    " + wo.getCreateDate());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getWarrantyStatusMsg());
            System.out.print("    " + wo.getBinName());
            System.out.print("    " + wo.getDefectDesc());
            System.out.println("    " + wo.getImeiNo());

            ArrayList<WorkOrderMaintain> woDetials = wo.getWoDetials();
            ArrayList<WorkOrderMaintainDetail> woMaintainDetials = wo.getWoMaintainDetials();
            ArrayList<WorkOrderDefect> woDefect = wo.getWoDefect();

            for (WorkOrderMaintain wom : woDetials) {
                System.out.print("  " + wom.getWorkorderMaintainId());
                System.out.print("  " + wom.getWorkOrderNo());
                System.out.print("  " + wom.getTechnician());
                System.out.println("  " + wom.getStatusMsg());
            }

            for (WorkOrderMaintainDetail womd : woMaintainDetials) {
                System.out.print("  " + womd.getPartDesc());
                System.out.print("  " + womd.getPartNo());
                System.out.print("  " + womd.getSeqNo());
                System.out.println("  " + womd.getWorkOrderMainId());
            }

            for (WorkOrderDefect wod : woDefect) {
                System.out.print("  " + wod.getWoDefectId());
                System.out.print("  " + wod.getMajDesc());
                System.out.print("  " + wod.getMinDesc());
                System.out.println("  " + wod.getStatus());
            }

        }
    }

    public static void getWorkOrderList() {

        MessageWrapper mw = WorkOrderController.getWorkOrderList("all", "all", "all", "all", "all", "all", "all", "all", "all", 0, "all", "all", "all", "all", "all", 229, 0, 0, 0, 0, "all", "all", 0, "all", 0, 0, 0, "all", "all", "all", 0, "all", "all", 0, "all", "all", "all", "all", 0, "all", "all", 1, 0, 200);

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrder> woLost = mw.getData();

        for (WorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getRepairStatusMsg());
            System.out.print("    " + wo.getRepairDate());
            System.out.print("    " + wo.getModleDesc());
            System.out.print("    " + wo.getWorkTelephoneNo());
            System.out.print("    " + wo.getCustomerAddress());
            System.out.print("    " + wo.getCustomerName());
            System.out.print("    " + wo.getCustomerNic());
            System.out.print("    " + wo.getEmail());
            System.out.print("    " + wo.getCreateDate());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getWarrantyStatusMsg());
            System.out.print("    " + wo.getBinName());
            System.out.print("    " + wo.getDefectDesc());
            System.out.println("    " + wo.getImeiNo());

        }

    }

    public static void getWorkOrderList123() {

        MessageWrapper mw = WorkOrderController.getWorkOrderList123("all", "all", "all", "all", "all", "all", "all", "all", "all", 0, "all", "all", "all", "all", "all", 94, 0, 0, 0, 0, "all", "all", 0, "all", 0, 0, 0, "all", "all", "all", 0, "all", "all", 0, "all", "all", "all", "all", "all", 0, 200);

        System.out.println(mw.getTotalRecords());

        ArrayList<WorkOrder> woLost = mw.getData();

        for (WorkOrder wo : woLost) {
            System.out.print("    " + wo.getWorkOrderNo());
            System.out.print("    " + wo.getRepairStatusMsg());
            System.out.print("    " + wo.getRepairDate());
            System.out.print("    " + wo.getModleDesc());
            System.out.print("    " + wo.getWorkTelephoneNo());
            System.out.print("    " + wo.getCustomerAddress());
            System.out.print("    " + wo.getCustomerName());
            System.out.print("    " + wo.getCustomerNic());
            System.out.print("    " + wo.getEmail());
            System.out.print("    " + wo.getCreateDate());
            System.out.print("    " + wo.getLevelName());
            System.out.print("    " + wo.getWarrantyStatusMsg());
            System.out.print("    " + wo.getBinName());
            System.out.print("    " + wo.getDefectDesc());
            System.out.println("    " + wo.getImeiNo());

        }

    }

    public static void checkcustomer() {

        ValidationWrapper vw = CustomerController.checkCustomer("890222564V");

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());
    }

    public static void senrEstimateMAil() {

        ValidationWrapper vw = WorkOrderMaintainaceController.sendEstimateCustomerMail("WO000019", "111,000.00", "est_No", "reff_No", 19);

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Mesg   " + vw.getReturnMsg());
    }

    public static void addImportDebitNote() {

        // AcceptDebitNoteController ac = new AcceptDebitNoteController();
        ImportDebitNote idn = new ImportDebitNote();
        idn.setDebitNoteNo("10");
        idn.setContactNo("123456780");
        idn.setCustomerNo("5");
        idn.setDeleveryDate("2014/11/11");
        idn.setHeadStatus(1);
        idn.setInvoiceNo("15");
        idn.setOrderNo("20");
        idn.setOrderQty(115);
        idn.setPartNo("111");
        idn.setSiteDescription("Location");
        idn.setStatus(1);
        idn.setUser("S08");

        ArrayList<ImportDebitNoteDetails> impList = new ArrayList<>();

        ImportDebitNoteDetails imlist = new ImportDebitNoteDetails();

//        imlist.setDebitNoteNo("10");
//        imlist.setCustomerCode("5");
//        imlist.setDateAccepted("2014/11/16");
//        imlist.setImeiNo("234");
//        imlist.setModleNo(1);
//        imlist.setOrderNo("20");
//        imlist.setRemarks("aaaaaa");
//        imlist.setSalerPrice(185.20);
//        imlist.setShopCode("10");
//        imlist.setStatus(1);
//        imlist.setUser("S08");
//        
//        impList.add(imlist);
//        
        idn.setImportDebitList(impList);

        ValidationWrapper vw = ImportDebitNoteController.addImportDebitNote(idn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());

    }

    public static void editDeviceIssue() {

        DeviceIssueController dic = new DeviceIssueController();

        DeviceIssueIme dii1 = new DeviceIssueIme();

        dii1.setImeiNo("300000000000010");
        dii1.setModleNo(12);
        dii1.setSalesPrice(70689.9);
        dii1.setMargin(1);

//
        ArrayList<DeviceIssueIme> di = new ArrayList<DeviceIssueIme>();
        di.add(dii1);
//        di.add(dii2);

        DeviceIssue dis = new DeviceIssue();
        dis.setIssueNo(7);
        dis.setdSRId(108);
        dis.setDelerMargin(0);
        dis.setDiscount(1);
        dis.setDistributerID(94);
        dis.setDistributorMargin(0);
        dis.setPrice(70689.9);
        dis.setStatus(1);
        dis.setIssueDate("2014-12-12");

        dis.setDeviceImei(di);

        ValidationWrapper vw = new ValidationWrapper();
        vw = dic.editDeviceIssue(dis, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }
    
    
     public static void editParts() {

        Part pt = new Part();

        pt.setPartNo(66);
        pt.setPartDesc("FRONT COVER - CRYSTAL701");
        pt.setPartExist(0);
        pt.setPartPrdCode("BR067");
        pt.setPartPrdDesc("PARTS");
        pt.setPartPrdFamily("PF689");
        pt.setPartPrdFamilyDesc("INTEXT MOBILE PARTS");
        pt.setPartGroup1("2P014");
        pt.setPartGroup2("2P014");
        pt.setPartSellPrice(200);
        pt.setPartCost(0);
        pt.setErpPartNo("INTS-CRI-701-12");
        pt.setStatus(1);

        ValidationWrapper vw = PartContraoller.editParts(pt, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }

    public static void debitNoteTransferProcedureCall() {

        
        ValidationWrapper vw = DbProcedureController.debitNoteTransferProcedureCall("admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");


        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }

    public static void editBussnessStrut() {

        BussinessStructure bs = new BussinessStructure();
        bs.setBisId(173);
        bs.setBisName("GTR");
        bs.setBisDesc("adasda");
        bs.setBisPrtnId(0);
        bs.setCategory1(5);
        bs.setCategory2(5);
        bs.setLatitude("1");
        bs.setLogitude("1");
        bs.setMapFlag(0);
        bs.setStatus(2);
        bs.setStructType(7);
        bs.setTeleNumber("1231231231");
        bs.setAddress("asd");
        bs.setBisUserCount(0);
        bs.setErpCode("G001");
        bs.setPoTitle("123");
        bs.setCheqTitle("Efdf2");
        bs.setEssdAcntNo("d01");
        bs.setEddsPct("0f01");
        bs.setInvoiceNo("Eff21");

        ValidationWrapper vw = BussStrucController.editBussinessStructure(bs, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }

   /* public static void getImportDebitNoteList() {

     //   MessageWrapper mw = ImportDebitNoteController.getImportDebitNote("all", "all", "all", "all", "all", 0, 0, "all", 0, "all", "all", "all", 0, 200);

        System.out.println("Total recode" + mw.getTotalRecords());
//
      //  ArrayList<ImportDebitNote> crList = mw.getData();
        for (ImportDebitNote ci : crList) {
            System.out.print("    " + ci.getDebitNoteNo());
            System.out.print("    " + ci.getContactNo());
            System.out.print("    " + ci.getCustomerNo());
            System.out.print("    " + ci.getOrderNo());
            System.out.print("    " + ci.getDeleveryDate());
            System.out.print("    " + ci.getInvoiceNo());
            System.out.print("    " + ci.getPartNo());
            System.out.print("    " + ci.getSiteDescription());
            System.out.print("    " + ci.getDeleveryQty());
            System.out.print("    " + ci.getHeadStatus());
            System.out.println("    " + ci.getStatus());
        }

    }*/

    public static void getImportDebitNoteDetails() {

        MessageWrapper mw = ImportDebitNoteController.getImportDebitNoteDetial("G02", "all", "all", "all", 0, 0, "all", 0, "all", 0, "all", "all", "all", "all", 0, 100);

        System.out.println("Total recode" + mw.getTotalRecords());

        ArrayList<ImportDebitNoteDetails> crList = mw.getData();

        for (ImportDebitNoteDetails ci : crList) {
            System.out.print("    " + ci.getDebitNoteNo());
            System.out.print("    " + ci.getModleDesc());
            System.out.print("    " + ci.getCustomerCode());
            System.out.print("    " + ci.getImeiNo());
            System.out.print("    " + ci.getOrderNo());
            System.out.print("    " + ci.getShopCode());
            System.out.print("    " + ci.getBisId());
            System.out.print("    " + ci.getModleNo());
            System.out.print("    " + ci.getSalerPrice());
            System.out.print("    " + ci.getSite());
            System.out.println("    " + ci.getStatus());
        }

    }

    public static void editCrditNote() {

        CreditNoteIssue cn = new CreditNoteIssue();
        cn.setCreditNoteIssueNo(10);
        cn.setDistributerId(94);
        cn.setIssueDate("2014-12-18");
        cn.setStatus(1);

        ArrayList<CreditNoteIssueDetail> drList = new ArrayList<>();

        CreditNoteIssueDetail dr1 = new CreditNoteIssueDetail();
        dr1.setImeiNo("606050605000006");
        dr1.setModleNo(22);
        dr1.setStatus(1);

//        CreditNoteIssueDetail dr2 = new CreditNoteIssueDetail();
//        dr2.setImeiNo("4444444");
//
//        CreditNoteIssueDetail dr3 = new CreditNoteIssueDetail();
//        dr3.setImeiNo("4545454");
        drList.add(dr1);
//        drList.add(dr2);
//        drList.add(dr3);

        cn.setCerditNoteList(drList);

        ValidationWrapper vw = CreditNoteIssueController.editCreditNoteIssue(cn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());

    }

    public static void addCreditNote() {

        CreditNoteIssue cn = new CreditNoteIssue();

        cn.setDebitNoteNo("2");
        cn.setDistributerId(1);
        cn.setIssueDate("2014-12-02");
        cn.setStatus(1);

        ArrayList<CreditNoteIssueDetail> drList = new ArrayList<>();

        CreditNoteIssueDetail dr1 = new CreditNoteIssueDetail();
        dr1.setImeiNo("444456654646648");
        //  dr1.setModleNo(1);

//
//        CreditNoteIssueDetail dr2 = new CreditNoteIssueDetail();
//        dr2.setImeiNo("456");
//        dr2.setModleNo(1);
//        dr2.setStatus(1);
//        dr2.setUser("D03");
//
        drList.add(dr1);
//        drList.add(dr2);
//
        cn.setCerditNoteList(drList);
        ValidationWrapper vw = CreditNoteIssueController.addCreditNoteIssue(cn, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag    " + vw.getReturnFlag());
        System.out.println("Mesage    " + vw.getReturnMsg());

    }

    public static void getcebitNoteIssue() {

        MessageWrapper mw = CreditNoteIssueController.getCreditNoteIssueDetail(7);

        System.out.println("Total recode" + mw.getTotalRecords());

        ArrayList<CreditNoteIssueDetail> crList = mw.getData();

        for (CreditNoteIssueDetail ci : crList) {
            System.out.print("    " + ci.getImeiNo());
            System.out.print("    " + ci.getCrNoteissueNo());
            System.out.print("    " + ci.getModleNo());
            System.out.print("    " + ci.getModleDesc());
            System.out.print("    " + ci.getSalesPrice());
            System.out.print("    " + ci.getSeqNo());
            System.out.println("    " + ci.getStatus());
        }
    }

    public static void getcebitNoteIssueList() {

        MessageWrapper mw = CreditNoteIssueController.getCreditNoteIssueDetails(0, "all", 0, "all", 0, "all", "all", 0, 10);

        System.out.println("Total recode" + mw.getTotalRecords());

        ArrayList<CreditNoteIssue> crList = mw.getData();

        for (CreditNoteIssue ci : crList) {
            System.out.print("    " + ci.getCreditNoteIssueNo());
            System.out.print("    " + ci.getDebitNoteNo());
            System.out.print("    " + ci.getDistributerId());
            System.out.print("    " + ci.getIssueDate());
            System.out.println("    " + ci.getStatus());
        }
    }

    public static void editDeviceReturn() {

        DeviceReturn dr = new DeviceReturn();
        dr.setReturnNo(12);
        dr.setDistributorId(105);
        dr.setDsrId(108);
        dr.setReturnDate("2014/09/30");
        dr.setStatus(1);

        ArrayList<DeviceReturnDetail> drList = new ArrayList<>();

        DeviceReturnDetail dr1 = new DeviceReturnDetail();
        dr1.setImeiNo("505050064446443");
        dr1.setModleNo(43);
        dr1.setSalesPrice(125.25);

//        DeviceReturnDetail dr2 = new DeviceReturnDetail();
//        dr2.setImeiNo("00001");
//        dr2.setModleNo(1);
        drList.add(dr1);
        //  drList.add(dr2);

        dr.setDeviceReturnList(drList);

        DeviceReturnController.editDeviceReturn(dr, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

    }

    public static void addDeviceReturn() {

        DeviceReturn dr = new DeviceReturn();

        dr.setDistributorId(94);
        dr.setReturnDate("2014/09/27");
        dr.setStatus(1);
        dr.setDsrId(108);

        ArrayList<DeviceReturnDetail> drList = new ArrayList<>();

        DeviceReturnDetail dr1 = new DeviceReturnDetail();
        dr1.setImeiNo("606050605000005");
        dr1.setModleNo(12);

//        DeviceReturnDetail dr2 = new DeviceReturnDetail();
//        dr2.setImeiNo("456");
//        dr2.setModleNo(1);
//        dr2.setStatus(1);
//        dr2.setUser("D03");
        drList.add(dr1);
        //    drList.add(dr2);

        dr.setDeviceReturnList(drList);
        ValidationWrapper vw = DeviceReturnController.addDeviceReturn(dr, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag    " + vw.getReturnFlag());
        System.out.println("Message    " + vw.getReturnMsg());

    }

    public static void addMinorDefectCode() {
        MinorDefect md = new MinorDefect();
        //  md.setMinDefectCode(1);
        md.setMinDefectDesc("System2");
        md.setMrjDefectCode(2);
        md.setMinDefectStatus(1);
        //  md.setUser("D06");

        ValidationWrapper vw = MinorDefectController.addMinorDefect(md, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag   " + vw.getReturnFlag());
        System.out.println("Msg    " + vw.getReturnMsg());
    }

    public static void getExpireDaywithIMEI() {

        MessageWrapper mw = WarrantySchemeController.getImeiMasterDetailswithWarranty("365263252632000");

        System.out.println("Redord  " + mw.getTotalRecords());
        ArrayList<ImeiMaster> imlist = mw.getData();

        for (ImeiMaster im : imlist) {
            System.out.print("   " + im.getImeiNo());
            System.out.print("   " + im.getWarrantyScheme());
            System.out.print("   " + im.getWarrantyExpireDate());
            System.out.print("   " + im.getBrand());
            System.out.print("   " + im.getModleDesc());
            System.out.print("   " + im.getProduct());
            System.out.print("   " + im.getPurchaseDate());
            System.out.print("   " + im.getSalesPrice());
            System.out.print("   " + im.getBisName());
            System.out.print("   Expire   " + im.getExpireStatus());
            System.out.println("   " + im.getBisId());
        }

    }

    public static void getWarrantyDays() {

        ValidationWrapper vw = WarrantySchemeController.getWarrantyDays("23", "2014/11/10");

        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
        System.out.println("Ref   " + vw.getReturnRefNo());
    }

    public static void addRepairCategory() {

        RepairCategory rc = new RepairCategory();
        rc.setReCateDesc("Test123");
        rc.setReCateStatus(1);
        rc.setUser("D05");

        ValidationWrapper vw = RepairCategoryController.addRepairCategory(rc, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Msg   " + vw.getReturnMsg());
    }

    public static void addUserBussinessstruct_LDAP() {

        UserBusinessStructuresController dic = new UserBusinessStructuresController();
        BSUserMapper bsm = new BSUserMapper();

        ArrayList<User> userList1 = new ArrayList<>();

//        User us1 = new User();
//        us1.setUserId("J01");
//        us1.setFirstName("Nalaka");
//        us1.setLastName("Gunasena");
//        us1.setExtraParams("6");
//////
//////        User us2 = new User();
//////        us2.setUserId("cxz");
//////        us2.setFirstName("Sanka");
//////        us2.setLastName("Pradeep");
//////        us2.setExtraParams("6");
//////
////        User us3 = new User();
////        us3.setUserId("pwi");
////        us3.setFirstName("Suranga");
////        us3.setLastName("Rajapaksha");
////        us3.setExtraParams("6");
//////
//       userList1.add(us1);
//////        userList1.add(us2);
////        userList1.add(us3);
        ArrayList<User> userList2 = new ArrayList<>();

//        User us4 = new User();
//        us4.setUserId("J01");
//        us4.setFirstName("Nalaka");
//        us4.setLastName("Gunasena");
//        us4.setExtraParams("6");
//
//        User us5 = new User();
//        us5.setUserId("pwi");
//        us5.setFirstName("Suranga");
//        us5.setLastName("Rajapaksha");
//        us5.setExtraParams("6");
////
////        userList2.add(us4);
//       userList2.add(us5);
        bsm.setAssignedUsers(userList1);
        bsm.setUnassignedUsers(userList2);

        ValidationWrapper vw = new ValidationWrapper();
        vw = dic.editBusinessStructureUser_LDAP_DB(bsm, "hasha41", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }

    public static void getBussinessStrucuture() {

        BussStrucController bc = new BussStrucController();
        MessageWrapper mw = bc.getBussinessStructure(0, 0);

        ArrayList<BussinessStructure> utlList = mw.getData();

        for (BussinessStructure ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getBisDesc());
            System.out.print("   " + ut.getBisPrtnId());
            System.out.println("   " + ut.getBisUserCount());
        }
    }

    public static void getBussinessStructurePaticulerBisId() {

        BussStrucController bc = new BussStrucController();
        MessageWrapper mw = bc.getBussinessStructureParticulerBisId(220);

        ArrayList<BussinessStructure> utlList = mw.getData();

        for (BussinessStructure ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisPrtnId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getBisDesc());
            System.out.println("   " + ut.getBisUserCount());
        }
    }

    public static void getAcceptDebitNote() {

        MessageWrapper mw = AcceptDebitNoteController.getAcceptDebitNote("all", "all", "all", "all", "all", 0, 0, "all", 0, "all", 0, 282, "all", "all", 0, 200);

        ArrayList<ImportDebitNote> utlList = mw.getData();

        for (ImportDebitNote ut : utlList) {
            System.out.print("   " + ut.getDebitNoteNo());
            System.out.print("   " + ut.getContactNo());
            System.out.print("   " + ut.getCustomerNo());
            System.out.print("   " + ut.getDeleveryDate());
            System.out.print("   " + ut.getInvoiceNo());
            System.out.print("   " + ut.getOrderNo());
            System.out.print("   " + ut.getBisId());
            System.out.println("   " + ut.getPartNo());

        }
    }

    public static void editLevel() {
        RepairLevel rl = new RepairLevel();
        rl.setLevelName("asd");
        rl.setLevelId(1);
        rl.setLevelDesc("DMS426");
        rl.setLevelCatId(1);
        rl.setLevelPrice(456.62);
        rl.setLevelStatus(1);

        ValidationWrapper vw = LevelContraller.editLevel(rl, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");
//
        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void addLevel() {
        RepairLevel rl = new RepairLevel();
        rl.setLevelName("lvl2");
        rl.setLevelDesc("DMS426");
        rl.setLevelCatId(1);
        rl.setLevelPrice(456.62);
        rl.setLevelStatus(1);

        ValidationWrapper vw = LevelContraller.addLevel(rl, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());
    }

    public static void getUserBussinessLevelList() {

        MessageWrapper mw = UserTypeListController.getUpperBusinessLevel(105, 4);

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.println("   " + ut.getBisName());
        }

    }

    public static void getUserUpperList() {

        MessageWrapper mw = UserTypeListController.getUpperLevelList(108, 3);

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("---   " + ut.getBisDesc());
            System.out.print("***   " + ut.getAddress());
            System.out.println("   " + ut.getTeleNumber());
        }

    }

    public static void getBusinessDownTypeList() {

        MessageWrapper mw = UserTypeListController.getBusinessDownTypeList(94, 3);
        System.out.println("Totla Recode  " + mw.getTotalRecords());

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getLongitude());
            System.out.print("   " + ut.getLatitude());
            System.out.println("   " + ut.getBisName());
        }

    }

    
    public static void getBusinessDownListWithoutBussCategory() {

        MessageWrapper mw = UserTypeListController.getBusinessDownListWithoutBussCategory(220);
        System.out.println("Totla Recode  " + mw.getTotalRecords());

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getUserId());
            System.out.print("   " + ut.getFirstName());
            System.out.print("   " + ut.getLongitude());
            System.out.print("   " + ut.getLatitude());
            System.out.println("   " + ut.getLastName());
        }

    }
    
    
    public static void getBusinessDownList() {

        MessageWrapper mw = UserTypeListController.getBusinessDownList(225, 1);
        System.out.println("Totla Recode  " + mw.getTotalRecords());

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getUserId());
            System.out.print("   " + ut.getFirstName());
            System.out.print("   " + ut.getLongitude());
            System.out.print("   " + ut.getLatitude());
            System.out.println("   " + ut.getLastName());
        }

    }

    public static void getBussinessTypesList() {

        UserTypeListController utl = new UserTypeListController();
        MessageWrapper mw = utl.getBussinessTypeList(7);

        ArrayList<BussinessStructure> utlList = mw.getData();

        for (BussinessStructure ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getBisDesc());
            System.out.println("   " + ut.getStatus());
        }

    }

    public static void getUserTypeListLowUpper() {

        UserTypeListController utl = new UserTypeListController();
        MessageWrapper mw = utl.getUserTypeListLowUpper("defaultcs", "all");

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getUserId());
            System.out.print("   " + ut.getFirstName());
            System.out.println("   " + ut.getLastName());
        }

    }

    public static void getUserTypeList() {

        UserTypeListController utl = new UserTypeListController();
        MessageWrapper mw = utl.getUserTypeList("shk91", 2);

        ArrayList<UserTypeList> utlList = mw.getData();

        for (UserTypeList ut : utlList) {
            System.out.print("   " + ut.getBisId());
            System.out.print("   " + ut.getBisName());
            System.out.print("   " + ut.getUserId());
            System.out.print("   " + ut.getFirstName());
            System.out.println("   " + ut.getLastName());
        }

    }

    public static void addUserBussinessStruct() {

        UserBusinessStructuresController dic = new UserBusinessStructuresController();

        UserBusinessStructure dii1 = new UserBusinessStructure();

        dii1.setBisId(2);
        dii1.setUserId("H01");
        dii1.setFirstName("Hashan");
        dii1.setLastName("Jayakody");
        dii1.setUser("M01");

        UserBusinessStructure dii2 = new UserBusinessStructure();

        dii2.setBisId(2);
        dii2.setUserId("F01");
        dii2.setFirstName("Sajith");
        dii2.setLastName("Gamage");
        dii2.setUser("M01");

        ArrayList<UserBusinessStructure> di = new ArrayList<UserBusinessStructure>();
        di.add(dii1);
        di.add(dii2);

        UserBusinessStructure dis = new UserBusinessStructure();

        dis.setBisId(2);
        dis.setUbsList(di);

        ValidationWrapper vw = new ValidationWrapper();
        vw = dic.addUserBusinessStructure(dis);

        System.out.println(vw.getReturnFlag());
        System.out.println(vw.getReturnMsg());

    }

    public static void getuserbussinessStru() {

        UserBusinessStructuresController ubc = new UserBusinessStructuresController();

        MessageWrapper mw = ubc.getUserBusinessStructures(1);

        ArrayList<UserBusinessStructure> ubsList = mw.getData();

        for (UserBusinessStructure ub : ubsList) {
            System.out.print("    " + ub.getBisId());
            System.out.print("    " + ub.getUserId());
            System.out.print("    " + ub.getFirstName());
            System.out.println("    " + ub.getLastName());
        }

    }

    public static void addDeviceIssue() {

        DeviceIssueIme dii1 = new DeviceIssueIme();
        dii1.setImeiNo("911406755166235");
        dii1.setModleNo(39);
        dii1.setMargin(1);
        dii1.setSalesPrice(23825.9);

//        DeviceIssueIme dii3 = new DeviceIssueIme();
//        dii3.setImeiNo("100000000000040");
//        dii3.setModleNo(2);
//        dii3.setMargin(1);
//        dii3.setSalesPrice(0);
//
//        DeviceIssueIme dii2 = new DeviceIssueIme();
//        dii2.setImeiNo("123");
//        dii2.setModleNo(3);
//        dii2.setStatus(1);
        ArrayList<DeviceIssueIme> di = new ArrayList<DeviceIssueIme>();
        di.add(dii1);
//        di.add(dii2);
        //       di.add(dii3);

        DeviceIssue dis = new DeviceIssue();
        dis.setdSRId(245);
        dis.setDelerMargin(0);
        dis.setDiscount(1);
        dis.setDistributerID(226);
        dis.setDistributorMargin(0);
        dis.setPrice(113877.5);
        dis.setStatus(2);
        dis.setIssueNo(0);
        dis.setIssueDate("2015-06-01");

        dis.setDeviceImei(di);

        ValidationWrapper vw = DeviceIssueController.addDeviceIssue(dis, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Return Falg  :" + vw.getReturnFlag());
        System.out.println("Return Msg  :" + vw.getReturnMsg());
    }

    public static void getUsers() {

//        ObjectMapper om = new ObjectMapper();
//        LdapUserEnty ld = new LdapUserEnty();
//        ld.setExtraParams("94");
//        try {
//            // ld.setUserId("sha");
//            //  ld.setNic("85");
//            System.out.println("  "+om.writeValueAsString(ld));
////        HashMap<String, String> searchParams = new HashMap<>();
////        //searchParams.put("displayName", "Dist");
////        searchParams.put("userId", "sha");
//        } catch (IOException ex) {
//            Logger.getLogger(ComonTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
        ResponseWrapper mw = UserController.getUsers123("all", "all", "all", "all", "all", "all", "all", "all", "all", "all", "0", "0", "100", "sqs", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");

        // getUserGroups("M01", "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");
        ArrayList<HashMap> uslist = mw.getData();
        for (HashMap h : uslist) {
            System.out.println("" + h);
        }

    }

    public static void getUserGroups() {

        UserController ucr = new UserController();

        ResponseWrapper mw = ucr.getUserGroups("M01", "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");

        ArrayList<HashMap> uslist = mw.getData();
        System.out.println(uslist);

    }

    public static void addReplacementReturn() {
        try {

            // ArrayList<ReplasementReturn> rrlist = new ArrayList<>();
            ReplasementReturn rr = new ReplasementReturn();
            rr.setExchangeReferenceNo("DE000022");
            rr.setImeiNo("11");
            rr.setReturnDate("2014/12/05");
            rr.setDistributerId(2);

            // rrlist.add(rr);
            ValidationWrapper vw = ReturnReplacementController.addReplasementReturn(rr, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

            System.out.println("Flag  " + vw.getReturnFlag());
            System.out.println("Message  " + vw.getReturnMsg());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void getReplacementReturn() {
        try {
            MessageWrapper mw = ReturnReplacementController.getReturnReplacement(0, "all", 0, "all", "all", "all", 0, "all", "all", 0, 10);

            System.out.println("Total recode : " + mw.getTotalRecords());

            ArrayList<ReplasementReturn> rr = mw.getData();
            for (ReplasementReturn rer : rr) {
                System.out.print("    " + rer.getReplRetutnNo());
                System.out.print("    " + rer.getExchangeReferenceNo());
                System.out.print("    " + rer.getImeiNo());
                System.out.print("    " + rer.getReturnDate());
                System.out.print("    " + rer.getDistributerId());
                System.out.print("    " + rer.getStatus());
                System.out.print("    " + rer.getStatusMsg());
                System.out.println("    " + rer.getModleDesc());

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void approveReplacementReturn() {

        ReplasementReturn rr = new ReplasementReturn();
        rr.setReplRetutnNo(11);
        rr.setImeiNo("701346401054648");
        rr.setDistributerId(94);

        ValidationWrapper vw = ReturnReplacementController.approveReplasementReturn(rr, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

        System.out.println("Flag  " + vw.getReturnFlag());
        System.out.println("Message  " + vw.getReturnMsg());

    }

    public static void editReplacementReturn() {
        try {

            // ArrayList<ReplasementReturn> rrlist = new ArrayList<>();
            ReplasementReturn rr = new ReplasementReturn();
            rr.setReplRetutnNo(32);
            rr.setExchangeReferenceNo("DE000022");
            rr.setImeiNo("11");
            rr.setReturnDate("2014/12/05");

            // rrlist.add(rr);
            ValidationWrapper vw = ReturnReplacementController.editReplasementReturn(rr, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

            System.out.println("Flag  " + vw.getReturnFlag());
            System.out.println("Message  " + vw.getReturnMsg());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void deleteReplacementReturn() {
        try {

            // ArrayList<ReplasementReturn> rrlist = new ArrayList<>();
            ReplasementReturn rr = new ReplasementReturn();
            rr.setReplRetutnNo(2);
            rr.setUser("B01");

            // rrlist.add(rr);
            ValidationWrapper vw = ReturnReplacementController.deleteReplasementReturn(rr, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

            System.out.println("Flag  " + vw.getReturnFlag());
            System.out.println("Message  " + vw.getReturnMsg());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void getImeiAdjusment() {
        try {
            MessageWrapper mw = ImeiAdjusmentController.getImeiAdjusmentDetails(0, "all", "all", "all", "all", "all", 0, 0, "all", 0, 0, "all", "all", 0, 10);

            System.out.println("Total recode : " + mw.getTotalRecords());

            ArrayList<ImeiAdjusment> rr = mw.getData();
            for (ImeiAdjusment rer : rr) {
                System.out.print("    " + rer.getSeqNo());
                System.out.print("    " + rer.getDebitNoteNo());
                System.out.print("    " + rer.getImeiNo());
                System.out.print("    " + rer.getNewImeiNo());
                System.out.print("    " + rer.getSalerPrice());
                System.out.print("    " + rer.getStatus());
                System.out.println("    " + rer.getSite());

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void addImeiAdjusment() {
        try {

            ImeiAdjusment ai = new ImeiAdjusment();
            ai.setDebitNoteNo("1");
            ai.setNewImeiNo("00123");
            ai.setModleNo(1);
            ai.setCustomerCode("cus45");
            ai.setShopCode("shp1");
            ai.setSalerPrice(2000);
            ai.setLocation("2");
            ai.setBisId(94);
            ai.setOrderNo("123");

            // rrlist.add(rr);
            ValidationWrapper vw = ImeiAdjusmentController.addImeiAdjusment(ai, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

            System.out.println("Flag  " + vw.getReturnFlag());
            System.out.println("Message  " + vw.getReturnMsg());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void editImeiAdjusment() {
        try {

            ImeiAdjusment ai = new ImeiAdjusment();
            ai.setDebitNoteNo("G02");
            ai.setOldImei("100000000000042");
            ai.setImeiNo("120202020200210");
            ai.setModleNo(1);
            ai.setCustomerCode("C005");
            ai.setShopCode("S04");
            ai.setBisId(105);
            ai.setSalerPrice(550);
            ai.setLocation("Kurunegala");
            ai.setUser("G01");

            // rrlist.add(rr);
            ValidationWrapper vw = ImeiAdjusmentController.editImeiAdjusment(ai, "admin", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer");

            System.out.println("Flag  " + vw.getReturnFlag());
            System.out.println("Message  " + vw.getReturnMsg());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void getGropus() {

        GroupController.getGroups("all", "all", "M01", "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");
    }

//    public static void getImeiMaster() {
//
//        ImeiMasterController imc = new ImeiMasterController();
//
//        MessageWrapper mw = imc.getImeiMasterDetails("all", 0, 0, "all", "all", "all", "all", "all", 0, 0, "all", "all", "all", 0, 10);
//
//        System.out.println("Total Recode" + mw.getTotalRecords());
//        //System.out.println("Data     "+mw.getData());
//        ArrayList<ImeiMaster> imList = mw.getData();
//
//        for (ImeiMaster im : imList) {
//            System.out.print("     " + im.getImeiNo());
//            System.out.print("     " + im.getModleNo());
//            System.out.print("     " + im.getNetPrice());
//            System.out.print("     " + im.getDebitnoteNo());
//            System.out.print("     " + im.getProduct());
//            System.out.println("     " + im.getModleDesc());
//
//        }
//    }

//    public static void getImeiMaster123() {
//
//        ImeiMasterController imc = new ImeiMasterController();
//
//        MessageWrapper mw = imc.getImeiMasterDetails("all", 0, 0, "all", "all", "all", "all", "all", 0, 0, "all", "all", "all", 0, 10);
//
//        System.out.println("Total Recode" + mw.getTotalRecords());
//        //System.out.println("Data     "+mw.getData());
//        ArrayList<ImeiMaster> imList = mw.getData();
//
//        for (ImeiMaster im : imList) {
//            System.out.print("     " + im.getImeiNo());
//            System.out.print("     " + im.getModleNo());
//            System.out.print("     " + im.getBisId());
//            System.out.print("     " + im.getBisName());
//            System.out.print("     " + im.getDebitnoteNo());
//            System.out.print("     " + im.getProduct());
//            System.out.print("     " + im.getBrand());
//            System.out.print("     " + im.getModleDesc());
//            System.out.println("     " + im.getLocation());
//        }
//    }

    public static void getWorkOrderStautsInquiryListLoad() {

        MessageWrapper mw = WorkOrderInquireController.getWorkOrderStatusInquiryListLoad("all", "90", "all", "all","all","all","all");

        System.out.println("Total Recods" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<WorkOrderStatusInquiry> workOrderData = mw.getData();

        for (WorkOrderStatusInquiry im : workOrderData) {
            System.out.println(im.getWorkOrderNo());
            System.out.println("tel: "+im.getCustomerTelephoneNo());
            System.out.println("Action: "+im.getActionTaken());
            //System.out.println("ACCESSORIES_RETAINED"+im.getAccesoriesRetained());
            System.out.println(im.getCustomerName());
            System.out.println(im.getCustomerAddress());
            System.out.println(im.getCustomerNic());
            System.out.println(im.getCustomerTelephoneNo());
            System.out.println(im.getEmail());
            System.out.println(im.getImeiNo());
            System.out.println(im.getProduct());
            System.out.println(im.getBrand());
            System.out.println(im.getDateOfSale());
            System.out.println(im.getWarrantyType());
            System.out.println(im.getTechnician());
            System.out.println(im.getDeliveryType());
            System.out.println(im.getTransferLocation());
            System.out.println(im.getDeliveryDate());
            System.out.println(im.getEstClosingDate());
            System.out.println("----------Spare Parts----------------");
            for (Part p : im.getPartList()) {
                System.out.println(p.getPartDesc() + " - " + p.getPartCost());
            }
            System.out.println("----------Defects----------------");
            for (WorkOrderDefect q : im.getWoDefect()) {
                System.out.println(q.getMajDesc() + " - " + q.getMinDesc());
            }
            System.out.println("----------Work Order Maintainance----------------");

            System.out.println(im.getRepairLevelId());
            System.out.println(im.getRepairLevelName());
            System.out.println(im.getRepairStatus());
            System.out.println(im.getRepairStatusMsg());
            System.out.println(im.getRepairAmount());

            System.out.println("Total Cost:   " + im.getTotalCost());
        }
    }

    //start New
//    public static void getWorkOrderStautsInquiryList() {
//
//        MessageWrapper mw = WorkOrderInquireController.getWorkOrderStatusInquiryList("all", "90", "all", "all","all","all","all");
//
//        System.out.println("Total Recods" + mw.getTotalRecords());
//        //System.out.println("Data     "+mw.getData());
//        ArrayList<WorkOrderStatusInquiry> workOrderData = mw.getData();
//
//        for (WorkOrderStatusInquiry im : workOrderData) {
//            System.out.println(im.getWorkOrderNo());
//            System.out.println("tel: "+im.getCustomerTelephoneNo());
//            System.out.println("Action: "+im.getActionTaken());
//            //System.out.println("ACCESSORIES_RETAINED"+im.getAccesoriesRetained());
//            System.out.println(im.getCustomerName());
//            System.out.println(im.getCustomerAddress());
//            System.out.println(im.getCustomerNic());
//            System.out.println(im.getCustomerTelephoneNo());
//            System.out.println(im.getEmail());
//            System.out.println(im.getImeiNo());
//            System.out.println(im.getProduct());
//            System.out.println(im.getBrand());
//            System.out.println(im.getDateOfSale());
//            System.out.println(im.getWarrantyType());
//            System.out.println(im.getTechnician());
//            System.out.println(im.getDeliveryType());
//            System.out.println(im.getTransferLocation());
//            System.out.println(im.getDeliveryDate());
//            System.out.println(im.getEstClosingDate());
//            System.out.println("----------Spare Parts----------------");
//            for (Part p : im.getPartList()) {
//                System.out.println(p.getPartDesc() + " - " + p.getPartCost());
//            }
//            System.out.println("----------Defects----------------");
//            for (WorkOrderDefect q : im.getWoDefect()) {
//                System.out.println(q.getMajDesc() + " - " + q.getMinDesc());
//            }
//            System.out.println("----------Work Order Maintainance----------------");
//
//            System.out.println(im.getRepairLevelId());
//            System.out.println(im.getRepairLevelName());
//            System.out.println(im.getRepairStatus());
//            System.out.println(im.getRepairStatusMsg());
//            System.out.println(im.getRepairAmount());
//
//            System.out.println("Total Cost:   " + im.getTotalCost());
//        }
//    }
    
    public static void getWorkOrderListForInquiry() {
        MessageWrapper mw = WorkOrderInquireController.getWorkOrderList();

        System.out.println("Total Recods" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<WorkOrder> workOrderData = mw.getData();

        for (WorkOrder w : workOrderData) {
            System.out.println(w.getWorkOrderNo());
        }
    }

    ////////////////UserBuisiness Mapping
    public static void mapUserCustomer() {
        try {
            UserBusinessMapping um1 = new UserBusinessMapping();
            um1.setBisId(282);
            um1.setCustomerNo("36952");
            um1.setBisName("DMS Distributor");
            um1.setStatus(0);
            
            UserBusinessMapping um2 = new UserBusinessMapping();
            um2.setBisId(282);
            um2.setCustomerNo("6544");
            um2.setBisName("DMS Distributor");
            um2.setStatus(0);
            
            UserBusinessMapping um3 = new UserBusinessMapping();
            um3.setBisId(282);
            um3.setCustomerNo("46762464");
            um3.setBisName("DMS Distributor");
            um3.setStatus(0);
            
            

            ArrayList<UserBusinessMapping> userList = new ArrayList<>();
            userList.add(um1);
            userList.add(um2);
            userList.add(um3);

            
            ValidationWrapper vw = UserBusinessMappingController.addUserBusiness(userList, "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");
        
            System.out.println("Flag       "+vw.getReturnFlag());
            System.out.println("Message    "+vw.getReturnMsg());

        
        
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public static void getUserBusinessMapping() {
        MessageWrapper mw = UserBusinessMappingController.getUserBusinessDetails(105, "all", "all", "all", "all", 0, 100);

        System.out.println("Total Recods" + mw.getTotalRecords());
        //System.out.println("Data     "+mw.getData());
        ArrayList<UserBusinessMapping> custData = mw.getData();

        for (UserBusinessMapping w : custData) {
            System.out.print(w.getSeqNo() + "  ");
            System.out.print(w.getBisId() + "  ");
            System.out.print(w.getBisName() + "  ");
            System.out.println(w.getCustomerNo());
        }
    }

    public static void editUserCustomer() {
        try {
            UserBusinessMapping um1 = new UserBusinessMapping();
            um1.setBisId(117);
            um1.setCustomerNo("DE0009");

            UserBusinessMapping um2 = new UserBusinessMapping();
            um2.setSeqNo(10);
            um2.setBisId(94);
            um2.setCustomerNo("DE0010");

            ArrayList<UserBusinessMapping> userList = new ArrayList<>();
            userList.add(um1);
            userList.add(um2);

            UserBusinessMappingController.editUserBusiness(um2, "DefaultRoom", "DefaultDepartment", "HeadOffice", "LK", "SingerLK", "Singer", "SingerISS");
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public static void checkCusCode(){
        try{
            
            ValidationWrapper vw = UserBusinessMappingController.checkCustometCode("DMI40060");
            System.out.println("Flag: "+vw.getReturnFlag());
            System.out.println("Msg: "+vw.getReturnMsg());
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
