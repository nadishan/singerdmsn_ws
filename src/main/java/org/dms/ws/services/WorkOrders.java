/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.dms.ws.singer.controllers.WorkOrderController;
import org.dms.ws.singer.entities.QuickWorkOrder;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.utils.AppParams;

/**
 *
 * @author SDU
 */
@Path("/WorkOrders")
public class WorkOrders {

    @GET
    @Path("/getWorkOrderList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderList(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("defectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("delayDate") @DefaultValue("all") String delayDate,
            @QueryParam("paymentType") @DefaultValue("0") int paymentType,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("workOrderStatus") @DefaultValue("0") int wo_type,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderController.getWorkOrderList(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, delayDate, paymentType, order, type, wo_type, start, limit);

    }

    @GET
    @Path("/getWorkOrderListForClearance")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderListForClearance(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("defectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("delayDate") @DefaultValue("all") String delayDate,
            @QueryParam("paymentType") @DefaultValue("0") int paymentType,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("workOrderStatus") @DefaultValue("0") int wo_type,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderController.getWorkOrderListForClearance(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, delayDate, paymentType, order, type, wo_type, start, limit);

    }

    @GET
    @Path("/getWorkOrderListHistory")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderListForHistory(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("defectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("delayDate") @DefaultValue("all") String delayDate,
            @QueryParam("paymentType") @DefaultValue("0") int paymentType,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("workOrderStatus") @DefaultValue("0") int wo_type,
            @QueryParam("limit") @DefaultValue("200") int limit) {
        System.out.println("HHHHHHHHHHHHHHHHHH>>>> ");
        return WorkOrderController.getWorkOrderListforHistory(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, delayDate, paymentType, order, type, wo_type, start, limit);

    }

    @GET
    @Path("/getWorkOrderListForSelectWO")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderListForSelectWO(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("defectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("delayDate") @DefaultValue("all") String delayDate,
            @QueryParam("paymentType") @DefaultValue("0") int paymentType,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("workOrderStatus") @DefaultValue("0") int wo_type,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderController.getWorkOrderListForSelectWO(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, delayDate, paymentType, order, type, wo_type, start, limit);

    }

    @GET
    @Path("/getQuickWorkOrderList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getQuickWorkOrderList(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerAddress") @DefaultValue("all") String customerAddress,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("dateOfSale") @DefaultValue("all") String dateOfSale,
            @QueryParam("proofOfPurches") @DefaultValue("all") String proofOfPurches,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("schemeId") @DefaultValue("0") int schemeId,
            @QueryParam("workOrderStatus") @DefaultValue("0") int workOrderStatus,
            @QueryParam("warrentyVrifType") @DefaultValue("0") int warrentyVrifType,
            @QueryParam("nonWarrentyVrifType") @DefaultValue("0") int nonWarrentyVrifType,
            @QueryParam("tecnician") @DefaultValue("all") String tecnician,
            @QueryParam("actionTaken") @DefaultValue("all") String actionTaken,
            @QueryParam("levelId") @DefaultValue("0") int levelId,
            @QueryParam("remarks") @DefaultValue("all") String remarks,
            @QueryParam("repairStatus") @DefaultValue("0") int repairStatus,
            @QueryParam("deleveryType") @DefaultValue("0") int deleveryType,
            @QueryParam("transferLocation") @DefaultValue("0") int transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("binName") @DefaultValue("all") String binName,
            @QueryParam("warrantyStatusMsg") @DefaultValue("all") String warrantyStatusMsg,
            @QueryParam("serviceBisId") @DefaultValue("0") int serviceBisId,
            @QueryParam("serviceBisDesc") @DefaultValue("all") String serviceBisDesc,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("defectDesc") @DefaultValue("all") String mjrDefectDesc,
            @QueryParam("delayDate") @DefaultValue("all") String delayDate,
            @QueryParam("paymentType") @DefaultValue("0") int paymentType,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("workOrderStatus") @DefaultValue("0") int wo_type,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderController.getQuickWorkOrderList(workOrderNo, customerName, customerAddress, workTelephoneNo, email, customerNic, imeiNo, product, brand, modleNo, assessoriesRetained, defectNo, rccReference, dateOfSale, proofOfPurches, bisId, schemeId, workOrderStatus, warrentyVrifType, nonWarrentyVrifType, tecnician, actionTaken, levelId, remarks, repairStatus, deleveryType, transferLocation, courierNo, deleveryDate, gatePass, status, binName, warrantyStatusMsg, serviceBisId, serviceBisDesc, modleDesc, mjrDefectDesc, delayDate, paymentType, order, type, wo_type, start, limit);

    }

    @PUT
    @Path("/addWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrder(WorkOrder wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderController.addWorkOrder(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    @PUT
    @Path("/addStockClearanceWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addStockClearanceWorkOrder(WorkOrder wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderController.addStockClearanceWorkOrder(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWorkOrder(WorkOrder wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderController.editWorkOrder(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getQuickWorkOrder")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getQuickWorkOrder(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("createDate") @DefaultValue("all") String createDate,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderController.getQuickWorkOrder(workOrderNo, imeiNo, createDate, fromDate, toDate, customerNic, order, type,bisId, start, limit);

    }

    @PUT
    @Path("/addQuickWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addQuickWorkOrder(QuickWorkOrder wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderController.addQuickWorkOrder(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editQuickWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editQuickWorkOrder(QuickWorkOrder qwo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderController.editQuickWorkOrder(qwo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/removeQuickWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response removeQuickWorkOrder(QuickWorkOrder qwo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderController.removeQuickWorkOrder(qwo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getWorkOrderDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderDetails(@QueryParam("workOrderNo") String workOrderNo) {

        return WorkOrderController.getWorkOrderDetails(workOrderNo);

    }

    @GET
    @Path("/getQuickWorkOrderOpenList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getQuickWorkOrderOpenList() {

        return WorkOrderController.getQuickWorkOrderOpenList();

    }

    @POST
    @Path("/deleteWorkOrder")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteWorkOrder(WorkOrder wo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return Response.status(201).entity(WorkOrderController.deleteWorkOrder(wo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getQuickWorkOrderDefectList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getQuickWorkOrderDefectList() {

        return WorkOrderController.getQuickWorkOrderDefectList();

    }

    @GET
    @Path("/getQuickWorkOrderServiceCenterList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getQuickWorkOrderServiceCenterList() {

        return WorkOrderController.getQuickWorkOrderServiceCenterList();

    }

    @GET
    @Path("/getWorkOrderDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderDetail(@QueryParam("workOrderNo") String workOrderNo) {

        return WorkOrderController.getWorkOrderDetail(workOrderNo);

    }

    @GET
    @Path("/getWorkOrderDetailsByWo")
    @Produces(MediaType.APPLICATION_JSON)
    public MessageWrapper getWorkOrderDetailsByWo(@QueryParam("woNumber") String woNumber) {

        return WorkOrderController.getWorkOrderDetailsByWo(woNumber);
    }

    @GET
    @Path("/getQcDetailsByWo")
    @Produces(MediaType.APPLICATION_JSON)
    public MessageWrapper getQcDetailsByWo(@QueryParam("woNumber") String woNumber) {

        return WorkOrderController.getQcDetailsByWo(woNumber);
    }

    @GET
    @Path("/getWorkOrderCollectorDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderCollectorDetail(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("imeiNo") @DefaultValue("0") int imeiNo,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("workTelephoneNo") @DefaultValue("all") String workTelephoneNo,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("defectNo") @DefaultValue("all") String defectNo,
            @QueryParam("assessoriesRetained") @DefaultValue("all") String assessoriesRetained,
            @QueryParam("podNo") @DefaultValue("0") int podNo,
            @QueryParam("collectorName") @DefaultValue("all") String collectorName,
            @QueryParam("collectorNic") @DefaultValue("all") String collectorNic,
            @QueryParam("contactNo") @DefaultValue("all") String contactNo,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit
    ) {

        return WorkOrderController.getWorkOrderCollectorDetail(workOrderNo,
                order,
                imeiNo,
                product,
                modleDesc,
                customerName,
                customerNic,
                workTelephoneNo,
                email,
                defectNo,
                assessoriesRetained,
                podNo,
                collectorName,
                collectorNic,
                contactNo,
                start,
                limit);

    }

    @POST
    @Path("/editWorkOrderCollectorDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWorkOrderCollectorDetail(QuickWorkOrder qwo,
            @HeaderParam("workOrderNo") String workOrderNo,
            @HeaderParam("order") String order,
            @HeaderParam("start") int start,
            @HeaderParam("limit") int limit
    ) {

        return Response.status(201).entity(WorkOrderController.editWorkOrderCollectorDetail(qwo, workOrderNo, order, start, limit)).build();
    }
}
