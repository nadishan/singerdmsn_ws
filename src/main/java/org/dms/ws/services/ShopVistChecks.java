/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ShopVisitController;
import org.dms.ws.singer.entities.ShopVistCheck;

/**
 *
 * @author SDU
 */
@Path("/ShopVistChecks")
public class ShopVistChecks {

    @GET
    @Path("/getShopVisitList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getShopVisitList(@QueryParam("latitude") String latitude,
            @QueryParam("longtitude") String longtitude,
            @QueryParam("distance") int distance) {

        return ShopVisitController.getShopVisitList(latitude, longtitude, distance);

    }

    @PUT
    @Path("/addShopVisitCheck")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addShopVisitCheck(ShopVistCheck svc,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,@HeaderParam("system") String system) {

        return Response.status(201).entity(ShopVisitController.addShopVisitCheck(svc, userId, room, department, branch, countryCode, division, organization)).build();
    }
    
    
    @GET
    @Path("/getDayMovement")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDayMovement(@QueryParam("dSR") int dSR,
            @QueryParam("visitDate") String visitDate,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("start") @DefaultValue("999") int limit) {

        return ShopVisitController.getDayMovement(dSR, visitDate, start, limit);

    }

}
