/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.dms.ws.singer.controllers.BussCategoryController;

/**
 *
 * @author SDU
 */
@Path("/BussinessCategory")
public class BussinessCategories {
    
    @GET
    @Path("/getBussinessCategorys")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessCategoryDetails(){
        return BussCategoryController.getBussinessCategorys();
    }
    
  
    
}
