/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.CustomerController;
import org.dms.ws.singer.controllers.UserBusinessMappingController;
import org.dms.ws.singer.controllers.UserController;
import org.dms.ws.singer.controllers.WorkOrderController;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserBusinessMapping;
import org.dms.ws.singer.entities.WorkOrder;

/**
 *
 * @author dmd
 */
@Path("/Distributors")
public class UserBusinessMappings {

    @PUT
    @Path("/addDistributors")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addUser(
            ArrayList<UserBusinessMapping> userBusinessInfo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization) {

        return Response.status(201).entity(UserBusinessMappingController.addUserBusiness(userBusinessInfo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editDistributors")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWorkOrder(
            UserBusinessMapping userBusinessInfo,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization) {
        return Response.status(201).entity(UserBusinessMappingController.editUserBusiness(userBusinessInfo, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getDistributors")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getCustomerDetals(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("bisName") @DefaultValue("all") String bisName,
            @QueryParam("customerNo") @DefaultValue("all") String customerCode,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("20000") int limit) { 

        return UserBusinessMappingController.getUserBusinessDetails(bisId, bisName, customerCode, order, type, start, limit);
    }

    @GET
    @Path("/getBussinessStructure")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getBussinessStructureDetails(
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("bisType") @DefaultValue("0") int bisType) {
        return UserBusinessMappingController.getBussinessStructure(bisId, bisType);
    }

    @GET
    @Path("/checkCustomerCode")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper checkCustomerCode(
            @QueryParam("customerCode") String customerCode) {
        return UserBusinessMappingController.checkCustometCode(customerCode);
    }
}
