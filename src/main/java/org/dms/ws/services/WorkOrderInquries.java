/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.controllers.WorkOrderInquireController;

/**
 *
 * @author SDU
 */
@Path("/WorkOrderInquries")
public class WorkOrderInquries {

    @GET
    @Path("/getWorkOrderInquire")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderInquire(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        

        return WorkOrderInquireController.getWorkOrderInquire(workOrderNo,imeiNo,customerNic, start, limit);

    }
    
    @GET
    @Path("/getWorkOrderList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderList() {
        return WorkOrderInquireController.getWorkOrderList();
    }

    @GET
    @Path("/getWorkOrderStatusInquireLoad")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderStatusInquire(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference,
            @QueryParam("telephoneNo") @DefaultValue("all") String telephoneNo,
            @QueryParam("customerName") @DefaultValue("all") String customerName,
            @QueryParam("shopName") @DefaultValue("all") String shopName){
        
        logger.info("INSIDE WS getWorkOrderInquireLoad =>>>>>>" );
        
        return WorkOrderInquireController.getWorkOrderStatusInquiryListLoad(workOrderNo,customerNic,imeiNo,rccReference,telephoneNo,customerName,shopName);
    }


    @GET
    @Path("/getWorkOrderStatusInquire")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderStatusInquire(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("customerNic") @DefaultValue("all") String customerNic,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("rccReference") @DefaultValue("all") String rccReference) {
        return WorkOrderInquireController.getWorkOrderStatusInquiryList(workOrderNo,customerNic,imeiNo,rccReference);
    }
}
