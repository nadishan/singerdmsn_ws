/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.AcceptDebitNoteController;
import org.dms.ws.singer.controllers.AcceptInventoryController;
import org.dms.ws.singer.entities.AcceptInventory;

/**
 *
 * @author SDU
 */
@Path("/AcceptInventories")
public class AcceptInventories {
    
    
    @GET
    @Path("/getAcceptInventory")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getAcceptInventory(@QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("issueDate") @DefaultValue("all") String issueDate,
            @QueryParam("distributorId") @DefaultValue("0") int distributorId,
            @QueryParam("dsrId") @DefaultValue("0") int dsrId,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("view") @DefaultValue("0") int view,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return AcceptInventoryController.getAcceptInventory(issueNo, status, issueDate, distributorId, dsrId, fromDate, toDate, order, type, view, start, limt);

    }

    @GET
    @Path("/getAcceptInventoryImei")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getAcceptInventoryImei(@QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("Status") @DefaultValue("0") int status,
            @QueryParam("dsrId") @DefaultValue("0") int dsrId,
            @QueryParam("view") @DefaultValue("0") int view,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return AcceptInventoryController.getAcceptInventoryImei(issueNo, status,dsrId,view,start, limt);

    }

    @PUT
    @Path("/addAcceptInventory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addAcceptInventory(AcceptInventory im,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(201).entity(AcceptInventoryController.addAcceptInventory(im, userId, room, department, branch, countryCode, division, organization)).build();
    } 
    
    
    
    @POST
    @Path("/editAcceptInventory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editAcceptInventory(AcceptInventory im,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(201).entity(AcceptInventoryController.editAcceptInventory(im, userId, room, department, branch, countryCode, division, organization)).build();
    } 
    
    @GET
    @Path("/getIssuedDeviceList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getIssuedDeviceList(@QueryParam("issueNo") @DefaultValue("0") int issueNo,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("issueDate") @DefaultValue("all") String issueDate,
            @QueryParam("distributorId") @DefaultValue("0") int distributorId,
            @QueryParam("dsrId") @DefaultValue("0") int dsrId,
            @QueryParam("fromDate") @DefaultValue("all") String fromDate,
            @QueryParam("toDate") @DefaultValue("all") String toDate,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("view") @DefaultValue("0") int view,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return AcceptInventoryController.getAcceptInventory(issueNo, status, issueDate, distributorId, dsrId, fromDate, toDate, order, type, view, start, limt);

    }
    
}
