/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WorkOrderFeedBackController;
import org.dms.ws.singer.entities.WorkOrderFeedBack;
import static org.dms.ws.singer.utils.DBMapper.TransferRepair;

/**
 *
 * @author SDU
 */
@Path("/WorkOrderFeedBacks")
public class WorkOrderFeedBacks {
    
    
    @GET
    @Path("/getWorkOrderFeedbackList")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderFeedbackList(@QueryParam("feedBackId") @DefaultValue("0") int feedBackId,
            @QueryParam("complainDate") @DefaultValue("all") String complainDate,
            @QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("feedbackTypeDesc") @DefaultValue("all") String feedbackTypeDesc,
            @QueryParam("prduct") @DefaultValue("all") String prduct,
            @QueryParam("modelName") @DefaultValue("all") String modelName,
            @QueryParam("purchaseShop") @DefaultValue("all") String purchaseShop,
            @QueryParam("purchaseDate") @DefaultValue("all") String purchaseDate,
            @QueryParam("serviceCenter") @DefaultValue("all") String serviceCenter,
            @QueryParam("woOpenDate") @DefaultValue("all") String woOpenDate,
            @QueryParam("deleveryDate") @DefaultValue("all") String deleveryDate,
            @QueryParam("warrenty") @DefaultValue("all") String warrenty,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("200") int limit) {

        return WorkOrderFeedBackController.getWorkOrderFeedbackList(feedBackId, complainDate, workOrderNo, feedbackTypeDesc, prduct, modelName, purchaseShop, purchaseDate, serviceCenter, woOpenDate, deleveryDate, warrenty, status, order, type, start, limit);

    }
    
    
    
    @POST
    @Path("/replyWorkOrderFeedBack")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response replyWorkOrderFeedBack(WorkOrderFeedBack wf,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderFeedBackController.replyWorkOrderFeedBack(wf, userId, room, department, branch, countryCode, division, organization)).build();
    } 
    
    
    
  
    
    
}
