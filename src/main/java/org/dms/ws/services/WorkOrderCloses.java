/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.WorkOrderCloseController;
import org.dms.ws.singer.entities.WorkOrderClose;

/**
 *
 * @author SDU
 */
@Path("/WorkOrderCloses")
public class WorkOrderCloses {

    @GET
    @Path("/getWorkOrderClossing")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderClossing(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo,
            @QueryParam("repairSatus") @DefaultValue("all") String repairSatus,
            @QueryParam("deleveryType") @DefaultValue("all") String deleveryType,
            @QueryParam("transferLocation") @DefaultValue("all") String transferLocation,
            @QueryParam("courierNo") @DefaultValue("all") String courierNo,
            @QueryParam("deleverDate") @DefaultValue("all") String deleverDate,
            @QueryParam("gatePass") @DefaultValue("all") String gatePass,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return WorkOrderCloseController.getWorkOrderClossing(workOrderNo, repairSatus, deleveryType, transferLocation, courierNo, deleverDate, gatePass,bisId, order, type, start, limt);

    }

    @GET
    @Path("/verifyWorkOrder")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper verifyWorkOrder(@QueryParam("workOrderNo") @DefaultValue("all") String workOrderNo) {
        return WorkOrderCloseController.verifyWorkOrder(workOrderNo);

    }

    @PUT
    @Path("/addWorkOrderClose")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addWorkOrderClose(WorkOrderClose wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderCloseController.addWorkOrderClose(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editWorkOrderClose")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editWorkOrderClose(WorkOrderClose wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderCloseController.editWorkOrderClose(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/deleteWorkOrderClose")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteWorkOrderClose(WorkOrderClose wot,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {

        return Response.status(201).entity(WorkOrderCloseController.deleteWorkOrderClose(wot, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getRepairStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getRepairStatus() {
        return WorkOrderCloseController.getRepairStatus();

    }

    @GET
    @Path("/getWorkOrderCloseStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderCloseStatus() {
        return WorkOrderCloseController.getWorkOrderCloseStatus();
    }
    
     @GET
    @Path("/getWorkOrderEstimateStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderEstimateStatus() {
        return WorkOrderCloseController.getWorkOrderEstimateStatus();
    }

    @GET
    @Path("/getDeleveryType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getDeleveryType() {
        return WorkOrderCloseController.getDeleveryType();

    }

    @GET
    @Path("/getWarrantyType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWarrantyType() {
        return WorkOrderCloseController.getWarrantyType();

    }

    @GET
    @Path("/getNonWarrantyType")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getNonWarrantyType() {
        return WorkOrderCloseController.getNonWarrantyType();

    }

    @GET
    @Path("/getWorkOrderToClose")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getWorkOrderToClose(@QueryParam("bisId") @DefaultValue("0") int bisId) {
        return WorkOrderCloseController.getWorkOrderToClose(bisId);

    }
    
    

}
