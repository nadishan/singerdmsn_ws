/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ImeiMasterController;
import org.dms.ws.singer.entities.ImeiMaster;

/**
 *
 * @author SDU
 */
@Path("/ImeiMaster")
public class ImeiMasters {

    @GET
    @Path("/getImeiMasterDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiMasterDetails(@QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("debitnoteNo") @DefaultValue("all") String debitnoteNo,
            @QueryParam("purchaseDate") @DefaultValue("all") String purchaseDate,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("location") @DefaultValue("all") String location,
            @QueryParam("salesPrice") @DefaultValue("0") double salesPrice,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("erpPartCode") @DefaultValue("all") String erpPartCode,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ImeiMasterController.getImeiMasterDetails(imeiNo, modleNo, bisId, debitnoteNo, purchaseDate, brand, product, location, salesPrice, status, modleDesc, order, type, erpPartCode, start, limt);

    }

    
    @GET
    @Path("/getImeiMasterDetailsVerify")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiMasterDetailsVerify(@QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("debitnoteNo") @DefaultValue("all") String debitnoteNo,
            @QueryParam("purchaseDate") @DefaultValue("all") String purchaseDate,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("location") @DefaultValue("all") String location,
            @QueryParam("salesPrice") @DefaultValue("0") double salesPrice,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("modleDesc") @DefaultValue("all") String modleDesc,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("erpPartCode") @DefaultValue("all") String erpPartCode,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ImeiMasterController.getImeiMasterDetailsVerify(imeiNo, modleNo, bisId, debitnoteNo, purchaseDate, brand, product, location, salesPrice, status, modleDesc, order, type, erpPartCode, start, limt);

    }
    
    @GET
    @Path("/getImeiDetailsForIssue")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiDetailsForIssue(@QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
    @QueryParam("bisId") @DefaultValue("all") String bisId) {

        return ImeiMasterController.getImeiDetailsForIssue(imeiNo,bisId);

    }
    

    @GET
    @Path("/getImeiDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiDetails(@QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
    @QueryParam("bisId") @DefaultValue("all") String bisId) {

        return ImeiMasterController.getImeiDetails(imeiNo,bisId);

    }

    @GET
    @Path("/getImeiDetailsAccept")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiDetailsAccept(@QueryParam("imeiNo") @DefaultValue("all") String imeiNo) {

        return ImeiMasterController.getImeiDetailsAccept(imeiNo);

    }

    @POST
    @Path("/deactivateImeiMaster")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deactivateImeiMaster(ImeiMaster ia,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(ImeiMasterController.deactivateImeiMaster(ia, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/activateImeiMaster")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response activateImeiMaster(ImeiMaster ia,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(ImeiMasterController.activateImeiMaster(ia, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getQuickImeiMasterDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getQuickImeiMasterDetails(@QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("debitnoteNo") @DefaultValue("all") String debitnoteNo,
            @QueryParam("purchaseDate") @DefaultValue("all") String purchaseDate,
            @QueryParam("brand") @DefaultValue("all") String brand,
            @QueryParam("product") @DefaultValue("all") String product,
            @QueryParam("location") @DefaultValue("all") String location,
            @QueryParam("salesPrice") @DefaultValue("0") double salesPrice,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ImeiMasterController.getQuickImeiMasterDetails(imeiNo, modleNo, bisId, debitnoteNo, purchaseDate, brand, product, location, salesPrice, status, order, type, start, limt);

    }

    @GET
    @Path("/validateImeiInLocation")
    @Produces({MediaType.APPLICATION_JSON})
    public ValidationWrapper validateImeiInLocation(@QueryParam("imeiNo") String imeiNo,
            @QueryParam("bisId") int bisId) {
        return ImeiMasterController.validateImeiInLocation(imeiNo, bisId);

    }

    @GET
    @Path("/getImeiValidationIssueInventory")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiValidationIssueInventory(@QueryParam("imeiNo") String imeiNo,
            @QueryParam("bisId") int bisId) {
        return ImeiMasterController.getImeiValidationIssueInventory(imeiNo, bisId);

    }

    @GET
    @Path("/getImeiValidationAcceptInventory")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiValidationAcceptInventory(@QueryParam("imeiNo") String imeiNo,
            @QueryParam("bisId") int bisId,
            @QueryParam("status")  @DefaultValue("0") int status) {
        return ImeiMasterController.getImeiValidationAcceptInventory(imeiNo, bisId, status);

    }
}
