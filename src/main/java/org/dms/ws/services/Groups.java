/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.services;

import javax.print.attribute.standard.Media;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.singer.controllers.GroupController;
import org.dms.ws.singer.entities.Group;

/**
 *
 * @author SDU
 */
@Path("/Groups")
public class Groups {
    
    @GET
    @Path("/getGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getGroupsDetials(@QueryParam("start") @DefaultValue("all") String start,
            @QueryParam("limit") @DefaultValue("all") String limit,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){
        return GroupController.getGroups(start,limit,userId,room,department,branch,countryCode,division,organization,system);
    } 
    
}
