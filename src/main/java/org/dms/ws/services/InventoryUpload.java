/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import java.io.File;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.dms.ws.singer.controllers.ImeisFromExcelController;
import org.dms.ws.singer.controllers.WorkOrderInquireController;
import org.dms.ws.singer.entities.ImeiTransferList;
import org.dms.ws.singer.entities.WarrentyRegistration;

/**
 *
 * @author shk
 */
@Path("/InventoryUpload")
public class InventoryUpload {

    @PUT
    @Path("/putExcel")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_JSON})
    public int putExcel(
            String excelBase64) throws Exception {
        return ImeisFromExcelController.getImeisFromExcelSheet(excelBase64);

    }

    @PUT
    @Path("/putExcelImeiUpload")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_JSON})
    public int putExcelImeiUpload(String excelBase64) throws Exception{
       return ImeisFromExcelController.getImeisFromExcelSheetUpload(excelBase64);
    }

    @PUT
    @Path("/deleteImeis")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_JSON})
    public int deleteData(
            ) throws Exception {
        return ImeisFromExcelController.deleteImeis();

    }
    @POST
    @Path("/updateMaster")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_JSON})
    public int updateMaster(
            String excelBase64) throws Exception {
        return ImeisFromExcelController.updateMaster(excelBase64);

    }

    @GET
    @Path("/includeExcel")
    @Produces({MediaType.APPLICATION_JSON})
    public Response Excel(
            String excelBase64) throws Exception {
        
        ImeisFromExcelController.putImeisToExcelSheet(excelBase64);
        
        String FILE_PATH = "\\new.xls";
        File file = new File(FILE_PATH);

        ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
                "attachment; filename=\"new.xls\"");
        return response.build();

//        return ImeisFromExcelController.putImeisToExcelSheet(excelBase64);
    }

    @GET
    @Path("/includeImeiExcel")
    @Produces({MediaType.APPLICATION_JSON})
    public Response ExcelImeis(
            String excelBase64) throws Exception {
        
        ImeisFromExcelController.putUploadImeisToExcelSheet(excelBase64);
        
        String FILE_PATH = "\\new.xls";
        File file = new File(FILE_PATH);

        ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
                "attachment; filename=\"new.xls\"");
        return response.build();

//        return ImeisFromExcelController.putImeisToExcelSheet(excelBase64);
    }

    @POST
    @Path("/updateBisId")
    @Produces({MediaType.APPLICATION_JSON})
    public org.dms.ldap.util.ResponseWrapper updateBisId(ImeiTransferList imeitr) {
        //ResponseWrapper response = ResponseWrapper.initResponse();
        return ImeisFromExcelController.updateBisId(imeitr);
    }

    @POST
    @Path("/updateDebitNoteBisID")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateDebitNoteBisID(@QueryParam("bisId")  int bisId) throws Exception
             {
        return Response.status(201).entity (ImeisFromExcelController.updateDebitNoteBisID(bisId)).build();
    }
    
    @PUT
    @Path("/addUploadImiesWarrentyReg")
   // @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addUploadImiesWarrentyReg(
            //WarrentyRegistration wr
    ) throws Exception
            //@QueryParam("bisId")  int bisId) throws Exception
             {
        return Response.status(201).entity (ImeisFromExcelController.addUploadImiesWarrentyReg()).build();
}
    
}
