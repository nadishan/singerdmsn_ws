/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.DsrAllowOfflineController;

/**
 *
 * @author dilhan
 */
@Path("/DsrAllowOffline")
public class DsrAllowOffline {

    @GET
    @Path("/editDsrAllowOffline")
    @Produces({MediaType.APPLICATION_JSON})
    public Response editBussinessStructure(
            @QueryParam("bisId") int bisId,
            @QueryParam("statusOfline") int statusOfline
    ) throws Exception {

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>" + bisId);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>" + statusOfline);
        
       
        return Response.status(201).entity(DsrAllowOfflineController.editDsrAllowOffline(bisId,statusOfline)).build();
    }

    
    @GET
    @Path("/getDsrAllowOfflineStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getDsrAllowOfflineStatus(
            @QueryParam("bisId") int bisId,
            @QueryParam("statusOfline") int statusOfline
    )throws Exception {
      return Response.status(201).entity(DsrAllowOfflineController.getDsrAllowOfflineStatus(bisId,statusOfline)).build();
}
}