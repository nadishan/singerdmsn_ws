/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.dms.ws.singer.controllers.ImeiAdjusmentController;
import org.dms.ws.singer.entities.ImeiAdjusment;

/**
 *
 * @author SDU
 */
@Path("/ImeiAdjusments")
public class ImeiAdjusments {

    @GET
    @Path("/getImeiAdjusmentDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiAdjusmentDetails(@QueryParam("seqNo") @DefaultValue("0") int seqNo,
            @QueryParam("debitNoteNo") @DefaultValue("all") String debitNoteNo,
            @QueryParam("newImeiNo") @DefaultValue("all") String newImeiNo,
            @QueryParam("imeiNo") @DefaultValue("all") String imeiNo,
            @QueryParam("customerCode") @DefaultValue("all") String customerCode,
            @QueryParam("shopCode") @DefaultValue("all") String shopCode,
            @QueryParam("modleNo") @DefaultValue("0") int modleNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId,
            @QueryParam("orderNo") @DefaultValue("all") String orderNo,
            @QueryParam("salesPrice") @DefaultValue("0") double salesPrice,
            @QueryParam("status") @DefaultValue("0") int status,
            @QueryParam("order") @DefaultValue("all") String order,
            @QueryParam("type") @DefaultValue("all") String type,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("999") int limt) {

        return ImeiAdjusmentController.getImeiAdjusmentDetails(seqNo, debitNoteNo, newImeiNo, imeiNo, customerCode, shopCode, modleNo, bisId, orderNo, salesPrice, status, order, type, start, limt);

    }

    @PUT
    @Path("/addImeiAdjusment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addImeiAdjusment(ImeiAdjusment ia,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(ImeiAdjusmentController.addImeiAdjusment(ia, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @POST
    @Path("/editImeiAdjusment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editImeiAdjusment(ImeiAdjusment ia,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization, @HeaderParam("system") String system) {
        return Response.status(201).entity(ImeiAdjusmentController.editImeiAdjusment(ia, userId, room, department, branch, countryCode, division, organization)).build();
    }

    @GET
    @Path("/getImeiAdjusmentDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiAdjusmentDetail(@QueryParam("imeiNo") @DefaultValue("0") String imeiNo) {
        return ImeiAdjusmentController.getImeiAdjusmentDetail(imeiNo);

    }

    @GET
    @Path("/getImeiToAdjusment")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper getImeiToAdjusment(@QueryParam("imeiNo") @DefaultValue("0") String imeiNo,
            @QueryParam("bisId") @DefaultValue("0") int bisId) {
        return ImeiAdjusmentController.getIMEIsToAdjust(imeiNo, bisId);

    }

    @GET
    @Path("/verfiedImeiDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageWrapper verfiedImeiDetails(@QueryParam("imeiNo") @DefaultValue("0") String imeiNo) {
        return ImeiAdjusmentController.verfiedImeiDetails(imeiNo);

    }

}
