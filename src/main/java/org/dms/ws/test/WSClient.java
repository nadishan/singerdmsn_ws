/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.codehaus.jackson.map.ObjectMapper;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.services.ValidationWrapper;
import org.dms.ws.singer.controllers.BussStrucController;
import org.dms.ws.singer.controllers.CategoryContraller;
import org.dms.ws.singer.controllers.UserController;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.Category;
import org.dms.ws.singer.entities.ClearTransitItem;
import org.dms.ws.singer.entities.Group;
import org.dms.ws.singer.entities.MajorDefect;
import org.dms.ws.singer.entities.RepairCategory;
import org.dms.ws.singer.entities.RepairLevel;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserGroup;
import org.dms.ws.singer.entities.WarrantyAdjustment;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

/**
 *
 * @author SDU
 */
public class WSClient {

 //   public static String addUserClient() {
    // System.out.println("call AddUser Client......");
//        ClientRequest request = new ClientRequest("http://localhost:8080/SingerPortalWebService-1.0/Services/MajorDefects/editMajorDefect");
    //ClientRequest request = new ClientRequest("http://localhost:8080/SingerPortalWebService-1.0/Services/Categories/addCaterory");
    //ClientRequest request = new ClientRequest("http://localhost:8080/SingerPortalWebService-1.0/Services/BussinessStructures/addBussinesStructure");
    // ClientRequest request = new ClientRequest("http://localhost:8080/SingerPortalWebService-1.0/Services/Users/getUsers");
    //ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ClearTransit/ClearTransitEdit");
    // System.out.println("Request Ok.............");
    //request.put();
//            
//            WarrantyAdjustment wa = new WarrantyAdjustment();
//            wa.setiMEI("23");
//            wa.setExtendedDays(100);
//            wa.setUser("To2");
//            wa.setWarrantyId(10);
//-----------------------------------------------------------------
//            MajorDefect md = new MajorDefect();
//            md.setMjrDefectCode(4);
//            md.setMjrDefectDesc("Data");
//            md.setMjrDefectStatus(1);
//            md.setUser("D03");
//------------------------------------------------------------
//            Category ct = new Category();
//          //  ct.setCateId(1);
//            ct.setCateName("Category 1234");
//            ct.setCateDesc("system category");
//            ct.setCateStatus(1);
//            ct.setUser("DO1");
//           // ct.setRemarks("my Remarks");
//-------------------------------------------------------------
    // ValidationWrapper vw = CategoryContraller.addCaterory(ct);
    //System.out.println(vw.getReturnFlag());
    //System.out.println(vw.getReturnMsg());        
    //----------------------------------------------------------------------           
//            ArrayList<User> user = new ArrayList<>();
//            User us = new User();
//            us.setUserId("t04");
//            us.setBisId(1);
//
//            User us1 = new User();
//            us1.setUserId("t05");
//            us1.setBisId(2);
//
//            User us2 = new User();
//            us2.setUserId("t06");
//            us2.setBisId(1);
//            
//            user.add(us);
//            user.add(us1);
//            user.add(us2);
    //BussStrucController.addBussinessUsers(user);           
//---------------------------------------------------------------------------------------
//            BussinessStructure bs = new BussinessStructure();
//
//            bs.setBisName("DMS");
//            bs.setBisDesc("Software Divition");
//            bs.setBisPrtnId(1);
//            bs.setCategory1(1);
//            bs.setCategory2(1);
//            bs.setLatitude("Test");
//            bs.setLogitude("Test");
//            bs.setAddress("Borella");
//            bs.setMapFlag(1);
//            bs.setStatus(1);
//            bs.setStructType(1);
//            bs.setTeleNumber("0123456789");
//            bs.setUser("D01");
    // bs.setBisId(1);
    // bs.setBisUserCount(4);
//------------------------------------------------------------------------------
//            UserGroup ug1 = new UserGroup();
//
//            ug1.setUserId("Q01");
//            ug1.setGroupId(1);
//            ug1.setStatus(1);
//            ug1.setUser("D01");
//
//            UserGroup ug2 = new UserGroup();
//
//            ug2.setUserId("Q01");
//            ug2.setGroupId(2);
//            ug2.setStatus(1);
//            ug2.setUser("D01");
//
//            UserGroup ug3 = new UserGroup();
//
//            ug3.setUserId("Q01");
//            ug3.setGroupId(3);
//            ug3.setStatus(1);
//            ug3.setUser("D01");
//
//            ArrayList<UserGroup> ug = new ArrayList<UserGroup>();
//            ug.add(ug2);
//            ug.add(ug1);
//            ug.add(ug3);
//            User us = new User();
//
//            us.setUserId("Q01");
//            us.setPassword("dms");
//            us.setFName("Dasun");
//            us.setLName("Maha");
//            us.setComName("ComanName");
//            us.setNIC("112233432V");
//            us.setTelePhone("9876543210");
//            us.setEMail("dms@mail.com");
//            us.setDesignation("Testing");
//            us.setRegDate("2014/08/25");
//            us.setUser("D01");
//            us.setUserGroups(ug);
//-----------------------------------------------------------------------------            
//            ClearTransitItem ct = new ClearTransitItem();
//            ct.setDebitNoteNo("1");
//            ct.setiMEI("1");
//            ct.setStatus(2);
//            ct.setRemarks("DMS");
//------------------------------------------------------------------------------
//            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/addUser");
//
//            UserController uc = new UserController();
//            Group ug1 = new Group();
//            ug1.setGroupId("DealerGrp");
//            ug1.setGroupName("Dealer Group");
//
//            ArrayList<Group> ug = new ArrayList<Group>();
//            ug.add(ug1);
//
//            User u = new User();
//            u.setUserId("B41");
//            u.setFirstName("Rasika");
//            u.setLastName("Thilakarathna");
//            u.setCommonName("RTK");
//            u.setEmail("rasika@dmssw.com");
//            u.setDesignation("Tech_Lead");
//            u.setTelephoneNumber("0773428463");
//            u.setStatus("1");
//            u.setGroups(ug);
//------------------------------------------------------------------------------
//              ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/editUser");
//
//            UserController uc = new UserController();
//            Group ug1 = new Group();
//            ug1.setGroupId("AdminGrp");
//            ug1.setGroupName("Admin Group");
//
//            ArrayList<Group> ug = new ArrayList<Group>();
//            ug.add(ug1);
//
//            User u = new User();
//            u.setUserId("M01");
//            u.setFirstName("Dasun");
//            u.setLastName("Mahawattha");
//            u.setCommonName("DMD");
//            u.setEmail("dasunm@dmssw.com");
//            u.setDesignation("Tech_Lead");
//            u.setTelephoneNumber("0773428463");
//            u.setStatus("1");
//            u.setGroups(ug);          
//------------------------------------------------------------------------------            
//            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/editUserWithoutGroup");
//
//            
//
//            User u = new User();
//            u.setUserId("V01");
//            u.setFirstName("Dhananjaya");
//            u.setLastName("Premarathna");
//            u.setEmail("dhananjaya@dmssw.com");
//            u.setStatus("1");
//------------------------------------------------------------------------------
//            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/changePassword");
//
//            
//
//            User u = new User();
//            u.setUserId("hasha41");
//            u.setPassword("k64MHv3!");
//            u.setNewPassword("12345678");
//------------------------------------------------------------------------------
//            ObjectMapper mapper = new ObjectMapper();
//            request.body("application/json", mapper.writeValueAsString(u));
//            System.out.println(request.getBody());
//            //ValidationWrapper vw;
//
//            ClientResponse<String> response = request.put(String.class);
//
//
//            if (response.getStatus() != 201) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
//            }
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(
//                    new ByteArrayInputStream(response.getEntity().getBytes())));
//
//            String output;
//            System.out.println("Output from Server .... \n");
//            while ((output = br.readLine()) != null) {
//                System.out.println("Aaaaaaaaaaa" + output);
//            }
//            
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//
//        return null;
//    } 
    //K01  5&sZ5vT4g
    public static void main(String[] args) {

        WSComonTest.addUser();
    }
}

//B41  2bIy5oxO2!
//M01   12345678
