/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.codehaus.jackson.map.ObjectMapper;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import org.dms.ws.singer.controllers.BussStrucController;
import org.dms.ws.singer.controllers.DeviceReturnController;
import org.dms.ws.singer.controllers.ImeiMasterController;
import org.dms.ws.singer.controllers.ImportDebitNoteController;
import org.dms.ws.singer.controllers.UserBusinessStructuresController;
import org.dms.ws.singer.controllers.UserController;
import org.dms.ws.singer.entities.AcceptInvenroryImei;
import org.dms.ws.singer.entities.AcceptInventory;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.CreditNoteIssue;
import org.dms.ws.singer.entities.CreditNoteIssueDetail;
import org.dms.ws.singer.entities.DSRTrack;
import org.dms.ws.singer.entities.DeviceReturn;
import org.dms.ws.singer.entities.DeviceReturnDetail;
import org.dms.ws.singer.entities.Group;
import org.dms.ws.singer.entities.ImeiAdjusment;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.ImportDebitNote;
import org.dms.ws.singer.entities.ImportDebitNoteDetails;
import org.dms.ws.singer.entities.MinorDefect;
import org.dms.ws.singer.entities.ReplasementReturn;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserBusinessStructure;
import org.dms.ws.singer.entities.WarrentyRegistration;
import org.dms.ws.singer.entities.WarrenyReplacement;
import org.dms.ws.singer.entities.WorkOrder;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

/**
 *
 * @author SDU
 */
public class WSComonTest {

        public static void addLocationTrackiing() {

        try {  
            
        DSRTrack dt = new DSRTrack();
        dt.setBisId(64);
        //dt.setVistiorBisId(5);
        dt.setLogtitute("3.25");
        dt.setLatitite("4.23");
        dt.setVisitDate("2014-11-24T09:23Z");

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/BussinessTrackings/addLocationTracking");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(dt));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    
    
    
    
    public static void addWarrentRegistration() {

        try {  
            
            WarrentyRegistration idn = new WarrentyRegistration();

            idn.setContactNo("1234567890");
            idn.setCustomerNIC("987654321V");
            idn.setCustomerName("DMS");
            idn.setDateOfBirth("1880/01/22");
            idn.setImeiNo("9999");
            idn.setBisId(1);
            idn.setSellingPrice(18);

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/WarrentyRegistrations/addWarrentyRegistration");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(idn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addWarrentReplacement() {

        try {
            WarrenyReplacement idn = new WarrenyReplacement();

            idn.setWorkOrderNo("WO000002");
            idn.setOldImei("234");
            idn.setNewImei("432");
            idn.setExchangeReferenceNo("DE000006");

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/WarrentyReplacements/addWarrentyReplacement");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(idn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addAcceptInventory() {

        try {
            AcceptInventory idn = new AcceptInventory();

            idn.setIssueNo(2);
            idn.setStatus(4);
            idn.setDsrId(13);

            ArrayList<AcceptInvenroryImei> idnllist = new ArrayList<>();

            AcceptInvenroryImei idnl = new AcceptInvenroryImei();
            idnl.setIssueNo(2);
            idnl.setImeiNo("234");
            idnl.setModleNo(1);
            idnl.setStatus(4);
            idnllist.add(idnl);

            idn.setAcceptInvImei(idnllist);

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/AcceptDebitNotes/addAcceptInventory");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(idn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void editWorkOrder() {

        try {

            WorkOrder wo = new WorkOrder();
            wo.setWorkOrderNo("WO000003");
            wo.setCustomerName("Thusitha");
            wo.setCustomerAddress("KAtunayake");
            wo.setBisId(2);
            wo.setAssessoriesRetained("test");
            wo.setActionTaken("test");
            wo.setDateOfSale("2014/01/01");
            wo.setDefectNo("1");
            wo.setDeleveryDate("2014/02/01");
            wo.setDeleveryType(1);
            wo.setEmail("dasunm@dmssw.com");
            wo.setGatePass("test");
            wo.setImeiNo("234");
            wo.setModleNo(1);
            wo.setLevelId(1);
            wo.setNonWarrentyVrifType(1);
            wo.setProduct("test");
            wo.setProofOfPurches("test");
            wo.setRccReference("tets");
            wo.setRemarks("test");
            wo.setRepairStatus(1);
            wo.setSchemeId(2);
            wo.setStatus(1);
            wo.setTecnician("tets");
            wo.setTransferLocation(2);
            wo.setWarrentyVrifType(1);

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/WorkOrders/editWorkOrder");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(wo));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addWorkOrder() {

        try {

            WorkOrder wo = new WorkOrder();
            wo.setCustomerName("Hashan");
            wo.setCustomerAddress("Palawatha");
            wo.setBisId(2);
            wo.setAssessoriesRetained("test");
            wo.setActionTaken("test");
            wo.setDateOfSale("2014/01/01");
            wo.setDefectNo("1");
            wo.setDeleveryDate("2014/02/01");
            wo.setDeleveryType(1);
            wo.setEmail("hashanj@dmssw.com");
            wo.setGatePass("test");
            wo.setImeiNo("234");
            wo.setModleNo(1);
            wo.setLevelId(1);
            wo.setNonWarrentyVrifType(1);
            wo.setProduct("test");
            wo.setProofOfPurches("test");
            wo.setRccReference("tets");
            wo.setRemarks("test");
            wo.setRepairStatus(1);
            wo.setSchemeId(2);
            wo.setStatus(1);
            wo.setTecnician("tets");
            wo.setTransferLocation(2);
            wo.setWarrentyVrifType(1);

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/WorkOrders/addWorkOrder");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(wo));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addAcceptDebitNote() {

        try {
            ImportDebitNote idn = new ImportDebitNote();

            idn.setDebitNoteNo("1");
            idn.setOrderNo("1");
            idn.setInvoiceNo("1");
            idn.setContactNo("12");
            idn.setPartNo("1");
            idn.setOrderQty(1);
            idn.setDeleveryQty(2);
            idn.setDeleveryDate("2014/09/10");
            idn.setHeadStatus(1);
            idn.setCustomerNo("2");
            idn.setSiteDescription("Hello");
            idn.setStatus(1);

            ArrayList<ImportDebitNoteDetails> idnllist = new ArrayList<>();

            ImportDebitNoteDetails idnl = new ImportDebitNoteDetails();
            idnl.setDebitNoteNo("1");
            idnl.setImeiNo("123");
            idnl.setCustomerCode("1");
            idnl.setShopCode("1");
            idnl.setModleNo(1);
            idnl.setBisId(1);
            idnl.setOrderNo("1");
            idnl.setSalerPrice(0);
            idnl.setDateAccepted("2014/04/16");
            idnl.setStatus(0);
            idnl.setRemarks("IMEI is Cleared");

            idnllist.add(idnl);

            idn.setImportDebitList(idnllist);

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/AcceptDebitNotes/addacceptDebitDetials");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(idn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void editMinorCode() {

        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/MinorDefects/editMinorDefect");

            MinorDefect md = new MinorDefect();
            md.setMinDefectCode(7);
            md.setMinDefectDesc("System");
            md.setMrjDefectCode(2);
            md.setMinDefectStatus(1);

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(md));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addImportDebitNote() {

        try {

            ImportDebitNote idn = new ImportDebitNote();
            idn.setDebitNoteNo("10");
            idn.setContactNo("123456780");
            idn.setCustomerNo("5");
            idn.setDeleveryDate("2014/11/11");
            idn.setHeadStatus(1);
            idn.setInvoiceNo("15");
            idn.setOrderNo("20");
            idn.setOrderQty(115);
            idn.setPartNo("111");
            idn.setSiteDescription("Location");
            idn.setStatus(1);
            idn.setUser("S08");

            ArrayList<ImportDebitNoteDetails> impList = new ArrayList<>();

            ImportDebitNoteDetails imlist = new ImportDebitNoteDetails();

            imlist.setDebitNoteNo("10");
            imlist.setCustomerCode("5");
            imlist.setDateAccepted("2014/11/16");
            imlist.setImeiNo("234");
            imlist.setModleNo(1);
            imlist.setOrderNo("20");
            imlist.setRemarks("aaaaaa");
            imlist.setSalerPrice(185.20);
            imlist.setShopCode("10");
            imlist.setStatus(1);
            imlist.setUser("S08");

            impList.add(imlist);

            idn.setImportDebitList(impList);

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ImportDebitNotes/addImportDebitNote");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            request.body("application/json", mapper.writeValueAsString(idn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void editBussinessstruct() {

        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/BussinessStructures/editBussinessStructure");

            BussinessStructure bs = new BussinessStructure();
            bs.setBisId(19);
            bs.setBisName("chanaka");
            bs.setBisDesc("prius");
            bs.setBisPrtnId(0);
            bs.setCategory1(0);
            bs.setCategory2(0);
            bs.setLatitude("");
            bs.setLogitude("");
            bs.setMapFlag(0);
            bs.setStatus(0);
            bs.setStructType(1);
            bs.setTeleNumber("");
            bs.setUser("");
            bs.setAddress("");

            ObjectMapper mapper = new ObjectMapper();

            request.body("application/json", mapper.writeValueAsString(bs));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void editCreditNote() {
        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/CreditNoteIssues/editCreditNoteIssue");

            CreditNoteIssue cn = new CreditNoteIssue();
            cn.setCreditNoteIssueNo(3);
            cn.setDebitNoteNo("1");
            cn.setDistributerId(1);
            cn.setIssueDate("2014/05/27");
            cn.setStatus(1);
            cn.setUser("D08");

            ArrayList<CreditNoteIssueDetail> drList = new ArrayList<>();

            CreditNoteIssueDetail dr1 = new CreditNoteIssueDetail();
            dr1.setSeqNo(1);
            dr1.setImeiNo("234");
            dr1.setModleNo(1);
            dr1.setStatus(1);
            dr1.setUser("D08");

            CreditNoteIssueDetail dr2 = new CreditNoteIssueDetail();
            dr2.setSeqNo(2);
            dr2.setImeiNo("456");
            dr2.setModleNo(1);
            dr2.setStatus(1);
            dr2.setUser("D08");

            drList.add(dr1);
            drList.add(dr2);

            cn.setCerditNoteList(drList);

            ObjectMapper mapper = new ObjectMapper();

            request.body("application/json", mapper.writeValueAsString(cn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addCreditNote() {
        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/CreditNoteIssues/addCreditNoteIssue");

            CreditNoteIssue cn = new CreditNoteIssue();

            cn.setDebitNoteNo("1");
            cn.setDistributerId(1);
            cn.setIssueDate("2014/12/27");
            cn.setStatus(1);
            cn.setUser("D03");

            ArrayList<CreditNoteIssueDetail> drList = new ArrayList<>();

            CreditNoteIssueDetail dr1 = new CreditNoteIssueDetail();
            dr1.setImeiNo("234");
            dr1.setModleNo(1);
            dr1.setStatus(1);
            dr1.setUser("D03");

            CreditNoteIssueDetail dr2 = new CreditNoteIssueDetail();
            dr2.setImeiNo("456");
            dr2.setModleNo(1);
            dr2.setStatus(1);
            dr2.setUser("D03");

            drList.add(dr1);
            drList.add(dr2);

            cn.setCerditNoteList(drList);

            ObjectMapper mapper = new ObjectMapper();

            request.body("application/json", mapper.writeValueAsString(cn));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void addDeviceReturn() {
        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/DeviceReturns/addDeviceReturn");

            DeviceReturn dr = new DeviceReturn();

            dr.setDistributorId(1);
            dr.setReturnDate("2013/09/27");
            dr.setStatus(1);
            dr.setUser("D08");

            ArrayList<DeviceReturnDetail> drList = new ArrayList<>();

            DeviceReturnDetail dr1 = new DeviceReturnDetail();
            dr1.setImeiNo("963");
            dr1.setModleNo(1);
            dr1.setStatus(1);
            dr1.setUser("D03");

            DeviceReturnDetail dr2 = new DeviceReturnDetail();
            dr2.setImeiNo("258");
            dr2.setModleNo(1);
            dr2.setStatus(1);
            dr2.setUser("D03");

            drList.add(dr1);
            drList.add(dr2);

            dr.setDeviceReturnList(drList);

            ObjectMapper mapper = new ObjectMapper();

            request.body("application/json", mapper.writeValueAsString(dr));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void editDeviceReturn() {
        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/DeviceReturns/editDeviceReturn");

            DeviceReturn dr = new DeviceReturn();
            dr.setReturnNo(3);
            dr.setDistributorId(1);
            dr.setReturnDate("2014/12/30");
            dr.setStatus(1);
            dr.setUser("D03");

            ArrayList<DeviceReturnDetail> drList = new ArrayList<>();

            DeviceReturnDetail dr1 = new DeviceReturnDetail();
            dr1.setReturnNo(3);
            dr1.setSeqNo(1);
            dr1.setImeiNo("951");
            dr1.setModleNo(1);
            dr1.setStatus(1);
            dr1.setUser("D03");

            DeviceReturnDetail dr2 = new DeviceReturnDetail();
            dr2.setReturnNo(3);
            dr2.setSeqNo(2);
            dr2.setImeiNo("456");
            dr2.setModleNo(1);
            dr2.setStatus(1);
            dr2.setUser("D03");

            drList.add(dr1);
            drList.add(dr2);

            dr.setDeviceReturnList(drList);

            ObjectMapper mapper = new ObjectMapper();

            request.body("application/json", mapper.writeValueAsString(dr));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void deactivateImeiMaster() {
        try {

            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ImeiMaster/deactivateImeiMaster");

            ImeiMaster imList = new ImeiMaster();
            imList.setImeiNo("456");
            imList.setUser("D01");

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(imList));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void editUserBussineStruct_LDAP() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/UserBussinessStructures/editBusinessStructureUser_LDAP_DB");

            ArrayList<User> userList = new ArrayList<>();

            User us1 = new User();
            us1.setUserId("teat");
            us1.setFirstName("Dumesh");
            us1.setLastName("Madhusanka");
            us1.setExtraParams("9");

            User us2 = new User();
            us2.setUserId("cxz");
            us1.setFirstName("Sanka");
            us1.setLastName("Pradeep");
            us2.setExtraParams("8");

            User us3 = new User();
            us3.setUserId("pwi");
            us1.setFirstName("Suranga");
            us1.setLastName("Rajapaksha");
            us3.setExtraParams("7");

            userList.add(us1);
            userList.add(us2);
            userList.add(us3);

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "hasha41");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            request.body("application/json", mapper.writeValueAsString(userList));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addUserBussinesstruct() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/UserBussinessStructures/addUserBusinessStructure");

            UserBusinessStructure dii1 = new UserBusinessStructure();

            dii1.setBisId(2);
            dii1.setUserId("B01");
            dii1.setFirstName("Rasika");
            dii1.setLastName("Thialka");
            dii1.setUser("M01");

            UserBusinessStructure dii2 = new UserBusinessStructure();

            dii2.setBisId(2);
            dii2.setUserId("K01");
            dii2.setFirstName("Nalaka");
            dii2.setLastName("Gunasena");
            dii2.setUser("M01");

            ArrayList<UserBusinessStructure> di = new ArrayList<UserBusinessStructure>();
            di.add(dii1);
            di.add(dii2);

            UserBusinessStructure dis = new UserBusinessStructure();

            dis.setBisId(2);
            dis.setUbsList(di);

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(dis));
            System.out.println(request.getBody());
            //ValidationWrapper vw;

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void changePassword() {
        try {

            ClientRequest request = new ClientRequest("http://192.0.0.222:8080/SingerPortalWebService-1.0/Services/Users/changePassword");

            ObjectMapper mapper = new ObjectMapper();

            User us = new User();
            us.setUserId("hasha41");
            us.setPassword("management");
            us.setNewPassword("12345678");
//             us.setPassword("12345678");
//            us.setNewPassword("management");
            us.setCommonName("");
            // us.setDesignation("");
            us.setEmail("");
            // us.setLastName("");
            //us.setNic("");
            // us.setStatus("");
            //us.setTelephoneNumber("");

            request.header("userId", "hasha41");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            request.body("application/json", mapper.writeValueAsString(us));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void resetPassword() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/resetUserPassword?userId=M01&email=shakthim@dmssw.com");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "M01");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            ClientResponse<String> response = request.get(String.class
            );

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void editUser() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/editUser");

            //   Group ug1 = new Group();
            //   ug1.setGroupId("DealerGrp");
            //   ug1.setGroupName("Dealer Group");
            //   ArrayList<Group> ug = new ArrayList<Group>();
            //   ug.add(ug1);
            User us = new User();
            us.setUserId("M01");
            us.setFirstName("Shakthi");
            us.setLastName("Madhuranga");
            us.setCommonName("SMD");
            us.setNic("900298536V");
            us.setEmail("shakthim@dmssw.com");
            us.setDesignation("Software Engineer");
            us.setStatus("9");
            // us.setGroups(ug);

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            request.body("application/json", mapper.writeValueAsString(us));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.post(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addUser() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/addUser");

//            Group ug1 = new Group();
//            ug1.setGroupId("DealerGrp");
//            ug1.setGroupName("Dealer Group");
//
//            ArrayList<Group> ug = new ArrayList<Group>();
//            ug.add(ug1);
            User us = new User();
            us.setUserId("dudd");
            us.setFirstName("Buddika");
            us.setLastName("Chathuranga");
            us.setCommonName("BC");
            us.setNic("883265981V");
            us.setEmail("sajithd@dmssw.com");
            us.setDesignation("Web developer");
            us.setStatus("1");
            us.setTelephoneNumber("0771234567");

            // us.setGroups(ug);
            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "shk91");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

//            request.header("LoggedInDN", "uid=admin,cn=DefaultRoom,ou=DefaultDepartment,ou=HeadOffice,c=LK,ou=SingerLK,o=Singer,ou=Organizations,dc=dmssw,dc=com");
//            request.header("SystemId", "SingerISS");
            request.body("application/json", mapper.writeValueAsString(us));

            System.out.println(request.getBody());

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void loginValidation() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Login/UserValidation?username=hasha41&password=123456");

            ObjectMapper mapper = new ObjectMapper();

            ClientResponse<String> response = request.get(String.class
            );

//            if (response.getStatus() != 201) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
//            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void logout() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Login/logoutUser?username=hasha41");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "M01");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            ClientResponse<String> response = request.get(String.class
            );

//            if (response.getStatus() != 201) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
//            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void getUser() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Users/getUsers?key=extraParams&value=null");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "M01");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            ClientResponse<String> response = request.get(String.class
            );

//            if (response.getStatus() != 201) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
//            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void getGroups() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/Groups/getGroups");

            ObjectMapper mapper = new ObjectMapper();

            request.header("userId", "admin");
            request.header("room", "DefaultRoom");
            request.header("department", "DefaultDepartment");
            request.header("branch", "HeadOffice");
            request.header("countryCode", "LK");
            request.header("division", "SingerLK");
            request.header("organization", "Singer");

            System.out.println("Heder------ :  " + request.getHeaders());

            ClientResponse<String> response = request.get(String.class
            );

//            if (response.getStatus() != 201) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
//            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addRepalcementReturn() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ReplacementReturns/addReplasementReturn");

            ReplasementReturn rr = new ReplasementReturn();
            rr.setExchangeReferenceNo("123");
            rr.setImeiNo("3");
            rr.setReturnDate("");
            rr.setDistributerId(0);
            rr.setReplRetutnNo(0);
            rr.setReturnDate("");
            rr.setStatus(0);
            rr.setUser("");

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(rr));
            System.out.println(request.getBody());
            //ValidationWrapper vw;

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void editRepalcementReturn() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ReplacementReturns/editReplasementReturn");

            ReplasementReturn rr = new ReplasementReturn();
            rr.setReplRetutnNo(3);
            rr.setExchangeReferenceNo("12345");
            rr.setImeiNo("11");
            rr.setUser("S01");

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(rr));
            System.out.println(request.getBody());
            ClientResponse<String> response = request.post(String.class
            );
            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));
            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void deleteRepalcementReturn() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ReplacementReturns/deleteReplasementReturn");

            ReplasementReturn rr = new ReplasementReturn();
            rr.setReplRetutnNo(3);
            rr.setUser("B01");

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(rr));
            System.out.println(request.getBody());
            ClientResponse<String> response = request.post(String.class
            );
            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));
            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addImeiAdjusment() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ImeiAdjusments/addImeiAdjusment");

            ImeiAdjusment ai = new ImeiAdjusment();
            ai.setDebitNoteNo("1");
            ai.setImeiNo("87");
            ai.setModleNo(1);
            ai.setCustomerCode("sdfg");
            ai.setShopCode("25");
            ai.setBisId(1);
            ai.setSalerPrice(53);
            ai.setLocation("dsf");
            ai.setOrderNo("33");

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(ai));
            System.out.println(request.getBody());
            //ValidationWrapper vw;

            ClientResponse<String> response = request.put(String.class
            );

            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void editImeiAdjusment() {
        try {
            ClientRequest request = new ClientRequest("http://192.0.0.70:8080/SingerPortalWebService-1.0/Services/ImeiAdjusments/editImeiAdjusment");

            ImeiAdjusment ai = new ImeiAdjusment();
            ai.setSeqNo(2);
            ai.setDebitNoteNo("5");
            ai.setNewImeiNo("098");
            ai.setImeiNo("678");
            ai.setModleNo(1);
            ai.setCustomerCode("321");
            ai.setShopCode("12");
            ai.setBisId(1);
            ai.setSalerPrice(52.63);
            ai.setStatus(1);
            ai.setUser("G01");

            ObjectMapper mapper = new ObjectMapper();
            request.body("application/json", mapper.writeValueAsString(ai));
            System.out.println(request.getBody());
            ClientResponse<String> response = request.post(String.class
            );
            if (response.getStatus()
                    != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));
            String output;

            System.out.println(
                    "Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println("Aaaaaaaaaaa" + output);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
