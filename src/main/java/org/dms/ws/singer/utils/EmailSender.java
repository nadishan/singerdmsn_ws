/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.utils;

import com.sun.mail.smtp.SMTPTransport;
import java.io.FileInputStream;
import static java.lang.ProcessBuilder.Redirect.to;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author SDU
 */
public class EmailSender {

    @SuppressWarnings("empty-statement")
    
            static String mailTransport ;
            static String mailIp ;
            static String smtpsHost ;
            static String mailAddress ;
            static String smtpsAuth ;
            static String sentFrom ;
            static String sentFromPword ;
            static String mailPort ;
            static int port;

    public static boolean sendHTMLMail(String toMailAdd, String password, String username, String firstname) throws MessagingException {
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();


            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth, "true");
            //Session session = Session.getInstance(props, null);
            Session session = Session.getInstance(props,  new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication(){
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));;
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("Singer Application");
            
             msg.setContent("<html><head><title>Page Title</title></head>"
                    + "<body><p>Dear " + firstname + ",</p>"
                    + "<h3>Welcome to Singer Customer Portal..!</h3>"
                    + "<p>* Download Singer Customer Mobile Application: http://yourlinkhere.... </p> "
                    + "<p>* Access Singer Customer Web Application: http://yourlinkhere...   </p>"
                    + "<p>Your User Id          : " + username + "</p>"
                    + "<p>Your New Password is  : " + password + "</p>"
                    + "<p style='color:#9A2EFE;'>Note: You can register in Singer Customer Network via your NIC Number</p>"
                    + "<p>Thank you!</p>"
                    + "<p>Best Regards,</p>"
                    + "<p>Singer Admin</p>"
                    + "</body></html>", "text/html; charset=utf-8");
            
            msg.setHeader("X-Mailer", "Singer ISS registration mail");

            msg.setSentDate(new Date());
            
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);

            t.connect(mailIp, port, sentFrom, sentFromPword);

            t.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
            ex.printStackTrace();
        }

        return isSent;
    }
    
    
      public static boolean sendHTMLMail123(String toMailAdd, String password, String username, String firstname) throws MessagingException {
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();
            
//            String mailTransport = "smtp";
//            String mailIp = "mail.singersl.com";
//            String smtpsHost = "mail.singersl.com";
//            String mailAddress = "test1@singersl.com";
//            String smtpsAuth = "mail.smtp.auth";
//           // String sentFrom = "test1@singersl.com";
//            String sentFrom = "test1";
//            String sentFromPword = "test@123";
//            String mailPort = "2020";
            
            
            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth,"true");
            Session session = Session.getInstance(props,  new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication(){
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));;
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("Singer Application");
            msg.setText("Dear " + firstname + " \n\nWelCome to Singer Application \n\n      Your User Id                : " + username + " \n      Your New Password is  : " + password + "\n\n\nThanks\nBest Regards \n\nSinger Admin");
            msg.setHeader("X-Mailer", "Singer ISS registration mail");

            msg.setSentDate(new Date());
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);

            t.connect(mailIp, port, sentFrom, sentFromPword);

            t.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
            ex.printStackTrace();
        }

        return isSent;
    }
    
    
    

    public static HashMap getMailConfigurations() {

//        String mailTransport = null;
//        String mailIp = null;
//        String smtpsHost = null;
//        String mailAddress = null;
//        String smtpsAuth = null;
//        String sentFrom = null;
//        String sentFromPword = null;
//        String mailPort = null;
        HashMap mailConfig = new HashMap();

        try {

//
//            mailTransport = pro.getProperty("MAIL_TRANSPORT");
//            mailConfig.put("mailTransport", mailTransport);
//
//            mailIp = pro.getProperty("MAIL_CONNECT_IP");
//            mailConfig.put("mailIp", mailIp);
//
//            smtpsHost = pro.getProperty("MAIL_SMTPS_HOST");
//            mailConfig.put("smtpsHost", smtpsHost);
//
//            mailAddress = pro.getProperty("MAIL_ADDRESS");
//            mailConfig.put("mailAddress", mailAddress);
//
//            smtpsAuth = pro.getProperty("MAIL_SMTPS_AUTH");
//            mailConfig.put("smtpsAuth", smtpsAuth);
//
//            sentFrom = pro.getProperty("MAIL_SENT_FROM");
//            mailConfig.put("sentFrom", sentFrom);
//
//            sentFromPword = pro.getProperty("MAIL_SENT_FROM_PASSWORD");
//            mailConfig.put("sentFromPword", sentFromPword);
//            
//            mailPort = pro.getProperty("MAIL_PORT");
//            mailConfig.put("mailPort", mailPort);
            mailConfig.put("mailTransport", AppParams.MAIL_TRANSPORT);

            //mailIp = pro.getProperty("MAIL_CONNECT_IP");
            mailConfig.put("mailIp", AppParams.MAIL_CONNECT_IP);

            //smtpsHost = pro.getProperty("MAIL_SMTPS_HOST");
            mailConfig.put("smtpsHost", AppParams.MAIL_SMTPS_HOST);

            //mailAddress = pro.getProperty("MAIL_ADDRESS");
            mailConfig.put("mailAddress", AppParams.MAIL_ADDRESS);

            // smtpsAuth = pro.getProperty("MAIL_SMTPS_AUTH");
            mailConfig.put("smtpsAuth", AppParams.MAIL_SMTPS_AUTH);

            //sentFrom = pro.getProperty("MAIL_SENT_FROM");
            mailConfig.put("sentFrom", AppParams.MAIL_SENT_FROM);

            //sentFromPword = pro.getProperty("MAIL_SENT_FROM_PASSWORD");
            mailConfig.put("sentFromPword", AppParams.MAIL_SENT_FROM_PASSWORD);

            //mailPort = pro.getProperty("MAIL_PORT");
            mailConfig.put("mailPort", AppParams.MAIL_PORT);

        } catch (Exception e) {
            e.getMessage();
        }
        return mailConfig;
    }

}
