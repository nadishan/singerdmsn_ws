/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.utils;

import org.dms.ws.singer.controllers.LogHelper;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hashan
 */
public class AppParams {

    static Properties props = new Properties();
    static String wspath = System.getenv("WS_HOME");

    static String pathset = wspath.replace("\\", "/");
    static String ippath = pathset + "/SINDMA_Conn.properties";

    public static final String APP_NAME;
    public static final String DB_UNAME;
    public static final String DB_PASSWORD;
    public static final String DB_URL;
    public static final String DB_CONNECTION_TYPE;

    public static final String LDAP_WS_PROTOCOL;
    public static final String LDAP_HOST;
    public static final String LDAP_SYSTEM_ID;
    public static final String LDAP_WS_PORT;
    public static final String LDAP_WS_CONTEXT_URL;

    public static final String MAIL_TRANSPORT;
    public static final String MAIL_CONNECT_IP;
    public static final String MAIL_SMTPS_HOST;
    public static final String MAIL_ADDRESS;
    public static final String MAIL_SMTPS_AUTH;
    public static final String MAIL_SENT_FROM;
    public static final String MAIL_SENT_FROM_PASSWORD;
    public static final String MAIL_PORT;

    public static final int ACCEPT_DEBIT_ERROR = 252525;
    public static final int ACCEPT_DEBIT_ERROR_sold = 252530;

    public static Logger logger;

    public static final String LDAP_WS_URL;

    static {
        System.out.println(ippath);
        try {
            props.load(new FileInputStream(ippath));
        } catch (IOException ex) {
            System.out.println("prop File Read Exception");
        }

        /* --------------- APPLICATION PARAMETERS --------------------- */
        APP_NAME = props.getProperty("SYSTEM_NAME");

        /* --------------- DATABASE PARAMETERS --------------------- */
        DB_UNAME = props.getProperty("USER_NAME");
        DB_PASSWORD = props.getProperty("PASSWORD");
        DB_URL = props.getProperty("CONNECTION_ORA");
        DB_CONNECTION_TYPE = props.getProperty("DRIVER");

        /* --------------- LDAP PARAMETERS --------------------- */
        LDAP_WS_PROTOCOL = props.getProperty("LDAP_PROTOCAL");
        LDAP_HOST = props.getProperty("LDAP_HOST");
        LDAP_SYSTEM_ID = props.getProperty("LDAP_SYSTEM_SID");
        LDAP_WS_PORT = props.getProperty("LDAP_WS_PORT");
        LDAP_WS_CONTEXT_URL = props.getProperty("LDAP_WS_CONTEXT_URL");

        LDAP_WS_URL = LDAP_WS_PROTOCOL + "://" + LDAP_HOST + ":" + LDAP_WS_PORT + "/" + LDAP_WS_CONTEXT_URL;

        /* --------------- MAIL PARAMETERS --------------------- */
        MAIL_TRANSPORT = props.getProperty("MAIL_TRANSPORT");
        MAIL_CONNECT_IP = props.getProperty("MAIL_CONNECT_IP");
        MAIL_SMTPS_HOST = props.getProperty("MAIL_SMTPS_HOST");
        MAIL_ADDRESS = props.getProperty("MAIL_ADDRESS");
        MAIL_SMTPS_AUTH = props.getProperty("MAIL_SMTPS_AUTH");
        MAIL_SENT_FROM = props.getProperty("MAIL_SENT_FROM");
        MAIL_SENT_FROM_PASSWORD = props.getProperty("MAIL_SENT_FROM_PASSWORD");
        MAIL_PORT = props.getProperty("MAIL_PORT");

        logger = new LogHelper().createLogger("SingerPortalService", wspath);

        System.out.println(DB_URL);
    }
}
