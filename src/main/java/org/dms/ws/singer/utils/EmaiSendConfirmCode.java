/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.utils;

import com.sun.mail.smtp.SMTPTransport;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.dms.ws.singer.controllers.DbCon;
import static org.dms.ws.singer.utils.EmailSender.mailAddress;
import static org.dms.ws.singer.utils.EmailSender.mailIp;
import static org.dms.ws.singer.utils.EmailSender.mailPort;
import static org.dms.ws.singer.utils.EmailSender.mailTransport;
import static org.dms.ws.singer.utils.EmailSender.port;
import static org.dms.ws.singer.utils.EmailSender.sentFrom;
import static org.dms.ws.singer.utils.EmailSender.sentFromPword;
import static org.dms.ws.singer.utils.EmailSender.smtpsAuth;
import static org.dms.ws.singer.utils.EmailSender.smtpsHost;

/**
 *
 * @author SDU
 */
public class EmaiSendConfirmCode {

    @SuppressWarnings("empty-statement")

    public static boolean sendHTMLMail(String toMailAdd, String firstname, String confirm_code) throws MessagingException{
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();
            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth, "true");
            //Session session = Session.getInstance(props, null);
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("Acknowledgment _ Singer Digital Service Centre/ Service Agent");

            ////////////////Get Model///////////////////////////
            msg.setContent("<html><head><title>Page Title</title></head>"
                    + "<body><p>Dear " + firstname + ",</p>"
                    + "<h3>Welcome to Singer Customer Portal..!</h3>"
                    + "<p>* Download Singer Customer Mobile Application: http://yourlinkhere.... </p> "
                    + "<p>* Access Singer Customer Web Application: http://yourlinkhere...   </p>"
                    + "<p>Your Confirmation Code  : " + confirm_code + "</p>"
                    + "<p style='color:#9A2EFE;'>Note: You can register in Singer Customer Network via your NIC Number</p>"
                    + "<p>Thank you!</p>"
                    + "<p>Best Regards,</p>"
                    + "<p>Singer Admin</p>"
                    + "</body></html>", "text/html; charset=utf-8");

            msg.setHeader("X-Mailer", "Singer ISS registration mail");

            msg.setSentDate(new Date());
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, port, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());
            //   System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
        }

        return isSent;
    }

    public static boolean sendHTMLMailWorkOrder(String toMailAdd, String firstname, String confirm_code, String woNumber, String refNo, String model, String imei, int warranty) throws MessagingException {
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();
            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth, "true");
            //Session session = Session.getInstance(props, null);
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("Acknowledgment _ Singer Digital Service Centre/ Service Agent");

            ////////////////Get Model///////////////////////////
            boolean checkModel = EmaiSendConfirmCode.checkModel(model);
            String itemModel = model;
            if (checkModel == true) {
                DbCon dbCon = new DbCon();
                Connection con = dbCon.getCon();
                ResultSet rs = dbCon.search(con, "select MODEL_DESCRIPTION "
                        + "from SD_MODEL_LISTS  "
                        + "WHERE MODEL_NO= " + model + "");

                if (rs.next()) {
                    itemModel = rs.getString("MODEL_DESCRIPTION");
                }
            }

            String confirmationCode = "";

            if (confirm_code == null || confirm_code.equals("")) {
//                msg.setContent("<html><head><title>Page Title</title></head>"
//                        + "<body><p>Dear " + firstname + ",</p>"
//                        + "<h3>Welcome to Singer Customer Portal..!</h3>"
//                        + "<p>* Download Singer Customer Mobile Application: http://yourlinkhere.... </p> "
//                        + "<p>* Access Singer Customer Web Application: http://yourlinkhere...   </p>"
//                        + "<p>WorkOrder Number            : " + woNumber + "</p>"
//                        + "<p style='color:#9A2EFE;'>Note: You can register in Singer Customer Network via your NIC Number</p>"
//                        + "<p>Thank you!</p>"
//                        + "<p>Best Regards,</p>"
//                        + "<p>Singer Admin</p>"
//                        + "</body></html>", "text/html; charset=utf-8");

            } else {
                confirmationCode = "<p>&#160;&#160;&#160;&#160;&#8226;&#160;&#160;Your Confirmation Code      : " + confirm_code + "</p>";

//                msg.setContent("<html><head><title>Page Title</title></head>"
//                        + "<body><p>Dear " + firstname + ",</p>"
//                        + "<h3>Welcome to Singer Customer Portal..!</h3>"
//                        + "<p>* Download Singer Customer Mobile Application: http://yourlinkhere.... </p> "
//                        + "<p>* Access Singer Customer Web Application: http://yourlinkhere...   </p>"
//                        + "<p>Your Confirmation Code      : " + confirm_code + "</p>"
//                        + "<p>WorkOrder Number            : " + woNumber + "</p>"
//                        + "<p style='color:#9A2EFE;'>Note: You can register in Singer Customer Network via your NIC Number and above Confrimation Code.</p>"
//                        + "<p>Thank you!</p>"
//                        + "<p>Best Regards,</p>"
//                        + "<p>Singer Admin</p>"
//                        + "</body></html>", "text/html; charset=utf-8");
            }

            ///////////////Check Warranty is Expired///////////////////
            String warrantyExpired = "";
            if (warranty == 2) {
                warrantyExpired = "<p>Warranty Period of this Device has Expired.</p>";
            }

            msg.setContent("<html><head><title></title></head>"
                    + "<body><p>Dear Valued Customer,</p>"
                    + "<p>This is to acknowledge that your product repair from Singer has been accepted and reference number is " + woNumber + " of model " + itemModel + " Serial/IMEI " + imei + ".</p>"
                    + "<p>You may log on to following website to view status of the repair. You can register in Singer customer network via your NIC number.</p>"
                    + confirmationCode
                    + "<p>&#160;&#160;&#160;&#160;&#8226;&#160;&#160;Download singer customer mobile application: http://yourlinkhere....</p>"
                    + "<p>&#160;&#160;&#160;&#160;&#8226;&#160;&#160;Access Singer Customer web application: link: http://yourlinkhere....</p>"
                    + warrantyExpired
                    + "<p>For further inquiries, please call us on 0115 400 400 during business hours.   </p>"
                    + "<p>We thank you for buying our products.   </p>"
                    + "<p>SINGER   </p>"
                    + "<p><br><br>&#42;&#42;Please note that this is an automated response and replies to this email will not be answered.   </p>"
                    + "</body></html>", "text/html; charset=utf-8");

            msg.setHeader("X-Mailer", "Singer ISS registration mail");

            msg.setSentDate(new Date());
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, port, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());
            //   System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
        }

        return isSent;
    }

    public static boolean sendHTMLMailWorkOrderClose(String toMailAdd, String firstname, String woNumber, String product, String reffRcc, String model, String imei, String deliveryType, String deliveryDate, String CourierNo, String gatePass) throws MessagingException {
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();
            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth, "true");
            //Session session = Session.getInstance(props, null);
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("Confirmation of the Work Order Completed-{" + woNumber + "}");

//                msg.setContent("<html><head><title>Page Title</title></head>"
//                        + "<body><p>Dear " + firstname + ",</p>"
//                        + "<h3>Welcome to Singer Customer Portal..!</h3>"
//                        + "<p>* Download Singer Customer Mobile Application: http://yourlinkhere.... </p> "
//                        + "<p>* Access Singer Customer Web Application: http://yourlinkhere...   </p>"
//                        + "<p>Your Work Order Closed, Pleace Collect Your "+product+" </p>"
//                        + "<p>WorkOrder Number : " + woNumber + "</p>"
//                        + "<p style='color:#9A2EFE;'>Note: You can register in Singer Customer Network via your NIC Number</p>"
//                        + "<p>Thank you!</p>"
//                        + "<p>Best Regards,</p>"
//                        + "<p>Singer Admin</p>"
//                        + "</body></html>", "text/html; charset=utf-8");
            String courierDetails = "";
            if (CourierNo != null && CourierNo.length() > 0) {
                courierDetails = "<p>Courier No:" + CourierNo + "</p>";
            }

            msg.setContent("<html><head><title></title></head>"
                    + "<body><p>Dear Valued Customer,</p>"
                    + "<p>This is to inform that your product repair reference number is " + woNumber + " of model " + model + " Serial/IMEI " + imei + " has </p>"
                    + "<p>been completed and delivered to, </p>"
                    + "<p>Delivery type:" + deliveryType + "</p>"
                    + courierDetails
                    + "<p>Delivery Date:" + deliveryDate + "</p>"
                    + "<p>Gate Pass or Any Reference:" + gatePass + "</p>"
                    + "<p>If you have not received the unit within 2 days of the Delivery Date, Please call to the Singer Call Center number (011 5 400 400) for more information.</p>"
                    + "<p>You may log on to following website to view status of the repair. You can register in Singer customer network via your NIC number. </p>"
                    + "<p>&#160;&#160;&#160;&#160;&#8226;&#160;&#160;Download singer customer mobile application: http://yourlinkhere....</p>"
                    + "<p>&#160;&#160;&#160;&#160;&#8226;&#160;&#160;Access Singer Customer web application: link: http://yourlinkhere....</p>"
                    + "<p>We thank you for buying our products.</p>"
                    + "<p>SINGER</p>"
                    + "<p><br><br>&#42;&#42;Please note that this is an automated response and replies to this email will not be answered.</p>"
                    + "</body></html>", "text/html; charset=utf-8");

            msg.setHeader("X-Mailer", "Singer ISS registration mail");

            msg.setSentDate(new Date());
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, port, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());
            //   System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
        }

        return isSent;
    }

    public static HashMap getMailConfigurations() {

//        String mailTransport = null;
//        String mailIp = null;
//        String smtpsHost = null;
//        String mailAddress = null;
//        String smtpsAuth = null;
//        String sentFrom = null;
//        String sentFromPword = null;
//        String mailPort = null;
        HashMap mailConfig = new HashMap();

        try {

            //mailTransport = pro.getProperty("MAIL_TRANSPORT");
            mailConfig.put("mailTransport", AppParams.MAIL_TRANSPORT);

            //mailIp = pro.getProperty("MAIL_CONNECT_IP");
            mailConfig.put("mailIp", AppParams.MAIL_CONNECT_IP);

            //smtpsHost = pro.getProperty("MAIL_SMTPS_HOST");
            mailConfig.put("smtpsHost", AppParams.MAIL_SMTPS_HOST);

            //mailAddress = pro.getProperty("MAIL_ADDRESS");
            mailConfig.put("mailAddress", AppParams.MAIL_ADDRESS);

            // smtpsAuth = pro.getProperty("MAIL_SMTPS_AUTH");
            mailConfig.put("smtpsAuth", AppParams.MAIL_SMTPS_AUTH);

            //sentFrom = pro.getProperty("MAIL_SENT_FROM");
            mailConfig.put("sentFrom", AppParams.MAIL_SENT_FROM);

            //sentFromPword = pro.getProperty("MAIL_SENT_FROM_PASSWORD");
            mailConfig.put("sentFromPword", AppParams.MAIL_SENT_FROM_PASSWORD);

            //mailPort = pro.getProperty("MAIL_PORT");
            mailConfig.put("mailPort", AppParams.MAIL_PORT);

        } catch (Exception e) {
            e.getMessage();
        }
        return mailConfig;
    }

    public static boolean checkModel(String modelNo) {
        boolean int_Model = true;
        try {
            int a = Integer.parseInt(modelNo);
        } catch (NumberFormatException nfe) {
            int_Model = false;
        }
        return int_Model;
    }
}
