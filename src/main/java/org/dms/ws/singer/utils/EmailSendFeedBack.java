/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.utils;

import com.sun.mail.smtp.SMTPTransport;
import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import static org.dms.ws.singer.utils.EmailSender.mailAddress;
import static org.dms.ws.singer.utils.EmailSender.mailIp;
import static org.dms.ws.singer.utils.EmailSender.mailPort;
import static org.dms.ws.singer.utils.EmailSender.mailTransport;
import static org.dms.ws.singer.utils.EmailSender.port;
import static org.dms.ws.singer.utils.EmailSender.sentFrom;
import static org.dms.ws.singer.utils.EmailSender.sentFromPword;
import static org.dms.ws.singer.utils.EmailSender.smtpsAuth;
import static org.dms.ws.singer.utils.EmailSender.smtpsHost;

/**
 *
 * @author SDU
 */
public class EmailSendFeedBack {

    @SuppressWarnings("empty-statement")

    public static boolean sendHTMLMail(String toMailAdd, String firstname, String feedback) throws MessagingException {
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();
            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth, "true");

            //Session session = Session.getInstance(props, null);
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });

            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("Singer WorkOrder");

            msg.setContent("<html><head><title>Page Title</title></head>"
                    + "<body><p>Dear " + firstname + ",</p>"
                    + "<h3><b>Thank you for your feed back..!</b></h3>"
                    + "<p>" + feedback + "</p>"
                    + "<p>Thank you!</p>"
                    + "<p>Best Regards,</p>"
                    + "<p>Singer Admin</p>"
                    + "</body></html>", "text/html; charset=utf-8");

            msg.setHeader("X-Mailer", "Singer ISS WorkOrder mail");

            msg.setSentDate(new Date());
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, port, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
        }

        return isSent;
    }

    public static HashMap getMailConfigurations() {

        HashMap mailConfig = new HashMap();

        try {

            mailConfig.put("mailTransport", AppParams.MAIL_TRANSPORT);
            mailConfig.put("mailIp", AppParams.MAIL_CONNECT_IP);
            mailConfig.put("smtpsHost", AppParams.MAIL_SMTPS_HOST);
            mailConfig.put("mailAddress", AppParams.MAIL_ADDRESS);
            mailConfig.put("smtpsAuth", AppParams.MAIL_SMTPS_AUTH);
            mailConfig.put("sentFrom", AppParams.MAIL_SENT_FROM);
            mailConfig.put("sentFromPword", AppParams.MAIL_SENT_FROM_PASSWORD);
            mailConfig.put("mailPort", AppParams.MAIL_PORT);

        } catch (Exception e) {
            e.getMessage();
        }
        return mailConfig;
    }

}
