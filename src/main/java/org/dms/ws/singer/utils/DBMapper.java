/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.utils;

import static org.dms.ws.singer.controllers.DbCon.logger;

/**
 *
 * @author SDU
 */
public class DBMapper {

    public static String eventBussinesss(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("userId")) {
                dBColumn = "UPPER(U.USER_ID)";
            } else if (cloumn.equals("bisTypeDesc")) {
                dBColumn = "UPPER(T.BIS_STRU_TYPE_DESC)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper eventBussinesss");
        }
        return dBColumn;
    }

    public static String eventParticipant(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("paticipantName")) {
                dBColumn = "UPPER(BIS_NAME)";
            } else if (cloumn.equals("location")) {
                dBColumn = "UPPER(ADDRESS)";
            } else if (cloumn.equals("paticipantType")) {
                dBColumn = "UPPER(BIS_STRU_TYPE_DESC)";
            } else if (cloumn.equals("achivePoint")) {
                dBColumn = "POINT_VAL";
            } else if (cloumn.equals("posotion")) {
                dBColumn = "RN";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper eventParticipant");
        }
        return dBColumn;
    }

    public static String eventInquire(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("eventId")) {
                dBColumn = "EVENT_ID";
            } else if (cloumn.equals("eventName")) {
                dBColumn = "UPPER(EVENT_NAME)";
            } else if (cloumn.equals("eventDesc")) {
                dBColumn = "UPPER(EVENT_DESC)";
            } else if (cloumn.equals("startDate")) {
                dBColumn = "START_DATE";
            } else if (cloumn.equals("endDate")) {
                dBColumn = "END_DATE";
            } else if (cloumn.equals("threshold")) {
                dBColumn = "THRESHOLD";
            } else if (cloumn.equals("eventAchievement")) {
                dBColumn = "ACHIVE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper eventInquire");
        }
        return dBColumn;
    }

    public static String eventMarks(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("userId")) {
                dBColumn = "UPPER(USER_ID)";
            } else if (cloumn.equals("pointAddDate")) {
                dBColumn = "POINTS_ADDED_DATE";
            } else if (cloumn.equals("assignBy")) {
                dBColumn = "UPPER(ASSIGN)";
            } else if (cloumn.equals("nonSalePoint")) {
                dBColumn = "NON_SALES_POINTS";
            } else if (cloumn.equals("status")) {
                dBColumn = "STATUS";
            } else if (cloumn.equals("statusMsg")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper eventMaster");
        }
        return dBColumn;
    }

    public static String eventMaster(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("eventId")) {
                dBColumn = "EVENT_ID";
            } else if (cloumn.equals("eventName")) {
                dBColumn = "UPPER(EVENT_NAME)";
            } else if (cloumn.equals("eventDesc")) {
                dBColumn = "UPPER(EVENT_DESC)";
            } else if (cloumn.equals("startDate")) {
                dBColumn = "START_DATE";
            } else if (cloumn.equals("endDate")) {
                dBColumn = "END_DATE";
            } else if (cloumn.equals("threshold")) {
                dBColumn = "THRESHOLD";
            } else if (cloumn.equals("statusMsg")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper eventMaster");
        }
        return dBColumn;
    }

    public static String clearTransit(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("issueNo")) {
                dBColumn = "D.ISSUE_NO";
            } else if (cloumn.equals("fromName")) {
                dBColumn = "UPPER(B1.BIS_NAME)";
            } else if (cloumn.equals("toName")) {
                dBColumn = "UPPER(B2.BIS_NAME)";
            } else if (cloumn.equals("issueDate")) {
                dBColumn = "D.ISSUE_DATE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String workOrderClose(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(W.WORK_ORDER_NO)";
            } else if (cloumn.equals("repairSatus")) {
                dBColumn = "UPPER(M.DESCRIPTION)";
            } else if (cloumn.equals("deleveryType")) {
                dBColumn = "UPPER(M1.DESCRIPTION)";
            } else if (cloumn.equals("transferLocation")) {
                dBColumn = "UPPER(B.BIS_NAME)";
            } else if (cloumn.equals("courierNo")) {
                dBColumn = "UPPER(W.COURIER_NO)";
            } else if (cloumn.equals("deleverDate")) {
                dBColumn = "W.DELIVERY_DATE";
            } else if (cloumn.equals("gatePass")) {
                dBColumn = "UPPER(W.GATE_PASS)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String paymentApprove(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("setviceName")) {
                dBColumn = "UPPER(B.FIRST_NAME)";
            } else if (cloumn.equals("paymentPrice")) {
                dBColumn = "P.AMOUNT";
            } else if (cloumn.equals("paymentId")) {
                dBColumn = "P.PAYMENT_ID";
            } else if (cloumn.equals("refDate")) {
                dBColumn = "P.PAYMENT_DATE";
            } else if (cloumn.equals("status")) {
                dBColumn = "P.STATUS";
            } else if (cloumn.equals("tax")) {
                dBColumn = "P.TAX";
            } else if (cloumn.equals("essdAccountNo")) {
                dBColumn = "UPPER(P.ESSD_ACCOUNT_NO)";
            } else if (cloumn.equals("poNumber")) {
                dBColumn = "UPPER(P.PO_NUMBER)";
            } else if (cloumn.equals("poDate")) {
                dBColumn = "P.PO_DATE";
            } else if (cloumn.equals("settleAmount")) {
                dBColumn = "P.SETTLE_AMOUNT";
            } else if (cloumn.equals("remarks")) {
                dBColumn = "UPPER(P.REMARKS)";
            } else if (cloumn.equals("chequeNo")) {
                dBColumn = "UPPER(P.CHEQUE_NO)";
            } else if (cloumn.equals("chequeDetails")) {
                dBColumn = "UPPER(P.CHEQUE_DETIALS)";
            } else if (cloumn.equals("chequeDate")) {
                dBColumn = "P.CHEQUE_COLLECTED_DATE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String workOrderTransfer(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(W.WORK_ORDER_NO)";
            } else if (cloumn.equals("bisName")) {
                dBColumn = "UPPER(B.BIS_NAME)";
            } else if (cloumn.equals("transferDate")) {
                dBColumn = "W.TRANSFER_DATE";
            } else if (cloumn.equals("transferReason")) {
                dBColumn = "W.TRANSFER_REASON";
            } else if (cloumn.equals("deleveryType")) {
                dBColumn = "W.DELIVERY_TYPE";
            } else if (cloumn.equals("courierEmail")) {
                dBColumn = "W.COURIER_EMAIL";
            } else if (cloumn.equals("status")) {
                dBColumn = "W.STATUS";
            } else if (cloumn.equals("deleveryTypeDesc")) {
                dBColumn = "M.DESCRIPTION";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String workOrderPaymentwithPrice(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(W.WORK_ORDER_NO)";
            } else if (cloumn.equals("warrentyTypeDesc")) {
                dBColumn = "UPPER(M.DESCRIPTION)";
            } else if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(W.IMEI_NO)";
            } else if (cloumn.equals("defectDesc")) {
                dBColumn = "UPPER(D.MAJ_CODE_DESC)";
            } else if (cloumn.equals("laborCost")) {
                dBColumn = "L.PRICE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String workOrderPayment(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(P.WORK_ORDER_NO)";
            } else if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(P.IMEI_NO)";
            } else if (cloumn.equals("paymentId")) {
                dBColumn = "PAYMENT_ID";
            } else if (cloumn.equals("warrentyTypeDesc")) {
                dBColumn = "all";
            } else if (cloumn.equals("defectDesc")) {
                dBColumn = "D.MAJ_CODE_DESC";
            } else if (cloumn.equals("status")) {
                dBColumn = "P.STATUS";
            } else if (cloumn.equals("amount")) {
                dBColumn = "P.AMOUNT";
            } else if (cloumn.equals("poNo")) {
                dBColumn = "P.PO_NUMBER";
            } else if (cloumn.equals("poDate")) {
                dBColumn = "P.PO_DATE";
            } else if (cloumn.equals("chequeNo")) {
                dBColumn = "P.CHEQUE_NO";
            }else if (cloumn.equals("chequeDate")) {
                dBColumn = "P.CHEQUE_COLLECTED_DATE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String workOrderPayment_New(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("paymentId")) {
                dBColumn = "P.PAYMENT_ID";
            } else if (cloumn.equals("amount")) {
                dBColumn = "P.AMOUNT";
            } else if (cloumn.equals("paymentDate")) {
                dBColumn = "P.PAYMENT_DATE";
            } else if (cloumn.equals("poNo")) {
                dBColumn = "P.PO_NUMBER";
            } else if (cloumn.equals("poDate")) {
                dBColumn = "P.P.PO_DATE";
            } else if (cloumn.equals("chequeNo")) {
                dBColumn = "P.CHEQUE_NO";
            } else if (cloumn.equals("chequeDate")) {
                dBColumn = "P.CHEQUE_COLLECTED_DATE";
            } else if (cloumn.equals("status")) {
                dBColumn = "P.STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderPayment");
        }
        return dBColumn;
    }

    public static String TransferRepair(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("transRepairNo")) {
                dBColumn = "T.REP_TRANS_NO";
            } else if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(T.IMEI_NO)";
            } else if (cloumn.equals("bisId")) {
                dBColumn = "T.BIS_ID";
            } else if (cloumn.equals("status")) {
                dBColumn = "T.STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper bussinessType");
        }
        return dBColumn;
    }

    public static String workOrderInquire(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("bisTypeId")) {
                dBColumn = "BIS_STRU_TYPE_ID";
            } else if (cloumn.equals("bisTypeDesc")) {
                dBColumn = "UPPER(BIS_STRU_TYPE_DESC)";
            } else if (cloumn.equals("entyMode")) {
                dBColumn = "ENTRY_MODE";
            } else if (cloumn.equals("status")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper bussinessType");
        }
        return dBColumn;
    }

    public static String bussinessType(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("bisTypeId")) {
                dBColumn = "BIS_STRU_TYPE_ID";
            } else if (cloumn.equals("bisTypeDesc")) {
                dBColumn = "UPPER(BIS_STRU_TYPE_DESC)";
            } else if (cloumn.equals("entyMode")) {
                dBColumn = "ENTRY_MODE";
            } else if (cloumn.equals("status")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper bussinessType");
        }
        return dBColumn;
    }

    public static String warrentyRegistration(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(IMEI_NO)";
            } else if (cloumn.equals("customerName")) {
                dBColumn = "UPPER(CUSTOMER_NIC)";
            } else if (cloumn.equals("customerNIC")) {
                dBColumn = "UPPER(CUSTOMER_NAME)";
            } else if (cloumn.equals("contactNo")) {
                dBColumn = "UPPER(CONTACT_NO )";
            } else if (cloumn.equals("dateOfBirth")) {
                dBColumn = "DATE_OF_BIRTH";
            } else if (cloumn.equals("sellingPrice")) {
                dBColumn = "SELLING_PRICE";
            } else if (cloumn.equals("modleNo")) {
                dBColumn = "MODEL_NO";
            } else if (cloumn.equals("modleDescription")) {
                dBColumn = "UPPER(MODEL_DESCRIPTION)";
            } else if (cloumn.equals("product")) {
                dBColumn = "UPPER(PRODUCT)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String warrentyReplacement(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(WORK_ORDER_NO)";
            } else if (cloumn.equals("oldImei")) {
                dBColumn = "UPPER(OLD_IMEI)";
            } else if (cloumn.equals("newImei")) {
                dBColumn = "UPPER(NEW_IMEI)";
            } else if (cloumn.equals("exRefNo")) {
                dBColumn = "UPPER(EX_REF_NO)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String deviceExchange(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("exchangeReferenceNo")) {
                dBColumn = "UPPER(REFERENCE_NO)";
            } else if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(WORK_ORDER_NO)";
            } else if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(E.IMEI_NO)";
            } else if (cloumn.equals("reasonDesc")) {
                dBColumn = "UPPER(MD.DESCRIPTION)";
            } else if (cloumn.equals("exchangeDesc")) {
                dBColumn = "UPPER(MD1.DESCRIPTION)";
            } else if (cloumn.equals("customerNIC")) {
                dBColumn = "UPPER(W.CUSTOMER_NIC)";
            } else if (cloumn.equals("customerName")) {
                dBColumn = "UPPER(W.CUSTOMER_NAME)";
            } else if (cloumn.equals("warrantyRegDate")) {
                dBColumn = "W.DATE_INSERTED";
            } else if (cloumn.equals("product")) {
                dBColumn = "UPPER(D.PRODUCT)";
            } else if (cloumn.equals("brand")) {
                dBColumn = "UPPER(D.BRAND)";
            } else if (cloumn.equals("modelDesc")) {
                dBColumn = "UPPER(M.MODEL_DESCRIPTION)";
            } else if (cloumn.equals("warrantyDesc")) {
              //  dBColumn = "UPPER(MD2.DESCRIPTION)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String acceptDebitNote(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("debitNoteNo")) {
                dBColumn = "UPPER(D.DEBIT_NOTE_NO)";
            } else if (cloumn.equals("orderNo")) {
                dBColumn = "UPPER(D.ORDER_NO)";
            } else if (cloumn.equals("invoiceNo")) {
                dBColumn = "UPPER(D.INVOICE_NO)";
            } else if (cloumn.equals("customerNo")) {
                dBColumn = "UPPER(D.CUSTOMER_NO)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String repairCategory(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("reCateId")) {
                dBColumn = "REP_CAT_ID";
            } else if (cloumn.equals("reCateDesc")) {
                dBColumn = "UPPER(REP_CAT_DESC)";
            } else if (cloumn.equals("reCateStatus")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String categoty(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("cateId")) {
                dBColumn = "BCAT_ID";
            } else if (cloumn.equals("cateName")) {
                dBColumn = "UPPER(BCAT_NAME)";
            } else if (cloumn.equals("cateDesc")) {
                dBColumn = "UPPER(BCAT_DESC)";
            } else if (cloumn.equals("cateStatus")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String clearTransmit(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("debitNoteNo")) {
                dBColumn = "UPPER(DEBIT_NOTE_NO)";
            } else if (cloumn.equals("iMEI")) {
                dBColumn = "UPPER(IMEI_NO)";
            } else if (cloumn.equals("customerCode")) {
                dBColumn = "UPPER(CUSTOMER_CODE)";
            } else if (cloumn.equals("shopCode")) {
                dBColumn = "UPPER(SHOP_CODE)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String creditnoteIssue(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("creditNoteIssueNo")) {
                dBColumn = "CR_NOTE_ISS_NO";
            } else if (cloumn.equals("issueDate")) {
                dBColumn = "ISSUE_DATE";
            } else if (cloumn.equals("status")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper acceptDebitNote");
        }
        return dBColumn;
    }

    public static String repairLevel(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("levelCode")) {
                dBColumn = "UPPER(LEVEL_CODE)";
            } else if (cloumn.equals("levelName")) {
                dBColumn = "UPPER(LEVEL_NAME)";
            } else if (cloumn.equals("levelDesc")) {
                dBColumn = "UPPER(LEVEL_DESC)";
            } else if (cloumn.equals("levelCatId")) {
                dBColumn = "REP_CAT_ID";
            } else if (cloumn.equals("levelPrice")) {
                dBColumn = "PRICE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper repairLevel");
        }
        return dBColumn;
    }

    public static String deviceIssue(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("issueNo")) {
                dBColumn = "D.ISSUE_NO";
            } else if (cloumn.equals("distributerID")) {
                dBColumn = "D.DISTRIBUTER_ID";
            } else if (cloumn.equals("dSRId")) {
                dBColumn = "D.DSR_ID";
            } else if (cloumn.equals("status")) {
                dBColumn = "D.STATUS";
            }else if (cloumn.equals("issueDate")) {
                dBColumn = "D.ISSUE_DATE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper repairLevel");
        }
        return dBColumn;
    }

    public static String devicereturn(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("returnNo")) {
                dBColumn = "RETURN_NO";
            } else if (cloumn.equals("distributorName")) {
                dBColumn = "UPPER(DISTRIBUTER_ID)";
            } else if (cloumn.equals("dsrName")) {
                dBColumn = "UPPER(DSR_ID)";
            } else if (cloumn.equals("returnDate")) {
                dBColumn = "RETURN_DATE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper devicereturn");
        }
        return dBColumn;
    }

    public static String imeiAdjusment(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("debitNoteNo")) {
                dBColumn = "RETURN_NO";
            } else if (cloumn.equals("distributorId")) {
                dBColumn = "DISTRIBUTER_ID";
            } else if (cloumn.equals("dsrId")) {
                dBColumn = "DSR_ID";
            } else if (cloumn.equals("returnDate")) {
                dBColumn = "RETURN_DATE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper devicereturn");
        }
        return dBColumn;
    }

    public static String imeiMaster(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(M.IMEI_NO)";
            } else if (cloumn.equals("modleNo")) {
                dBColumn = "M.MODEL_NO";
            } else if (cloumn.equals("product")) {
                dBColumn = "UPPER(M.PRODUCT)";
            } else if (cloumn.equals("brand")) {
                dBColumn = "UPPER(M.BRAND)";
            } else if (cloumn.equals("location")) {
                dBColumn = "UPPER(M.LOCATION)";
            } else if (cloumn.equals("salesPrice")) {
                dBColumn = "M.SALES_PRICE";
            } else if (cloumn.equals("status")) {
                dBColumn = "M.STATUS";
            } else if (cloumn.equals("modleDesc")) {
                dBColumn = "L.MODEL_DESCRIPTION";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper imeiMaster");
        }
        return dBColumn;
    }

    public static String importdebitNote(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("debitNoteNo")) {
                dBColumn = "UPPER(DH.DEBIT_NOTE_NO)";
            } else if (cloumn.equals("orderNo")) {
                dBColumn = "UPPER(DH.ORDER_NO)";
            } else if (cloumn.equals("invoiceNo")) {
                dBColumn = "UPPER(DH.INVOICE_NO)";
            } else if (cloumn.equals("contactNo")) {
                dBColumn = "UPPER(DH.CONTRACT_NO)";
            } else if (cloumn.equals("partNo")) {
                dBColumn = "UPPER(DH.PART_NO)";
            } else if (cloumn.equals("orderQty")) {
                dBColumn = "DH.ORDER_QTY";
            } else if (cloumn.equals("deleveryQty")) {
                dBColumn = "DH.DELIVERED_QTY";
            } else if (cloumn.equals("deleveryDate")) {
                dBColumn = "DH.DELIVERY_DATE";
            } else if (cloumn.equals("headStatus")) {
                dBColumn = "DH.HEAD_STATUS";
            } else if (cloumn.equals("customerNo")) {
                dBColumn = "UPPER(DH.CUSTOMER_NO)";
            } else if (cloumn.equals("status")) {
                dBColumn = "DH.STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper importdebitNote");
        }
        return dBColumn;
    }

    public static String importdebitNoteDetails(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(D.IMEI_NO)";
            } else if (cloumn.equals("customerCode")) {
                dBColumn = "UPPER(D.CUSTOMER_CODE)";
            } else if (cloumn.equals("shopCode")) {
                dBColumn = "UPPER(D.SHOP_CODE)";
            } else if (cloumn.equals("modleNo")) {
                dBColumn = "D.MODEL_NO";
            } else if (cloumn.equals("site")) {
                dBColumn = "UPPER(D.REMARKS)";
            } else if (cloumn.equals("orderNo")) {
                dBColumn = "UPPER(D.ORDER_NO)";
            } else if (cloumn.equals("salerPrice")) {
                dBColumn = "D.SALES_PRICE";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper importdebitNoteDetails");
        }
        return dBColumn;
    }

    public static String majorDefectCode(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("mjrDefectCode")) {
                dBColumn = "MAJ_CODE";
            } else if (cloumn.equals("mjrDefectDesc")) {
                dBColumn = "UPPER(MAJ_CODE_DESC)";
            } else if (cloumn.equals("mjrDefectStatus")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper majorDefectCode");
        }
        return dBColumn;
    }

    public static String minorDefectCode(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("minDefectCode")) {
                dBColumn = "MIN_CODE";
            } else if (cloumn.equals("minDefectDesc")) {
                dBColumn = "UPPER(MIN_CODE_DESC)";
            } else if (cloumn.equals("minDefectStatus")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

    public static String modle(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("model_No")) {
                dBColumn = "MODEL_NO";
            } else if (cloumn.equals("model_Description")) {
                dBColumn = "UPPER(MODEL_DESCRIPTION)";
            } else if (cloumn.equals("status")) {
                dBColumn = "STATUS";
            } else if (cloumn.equals("part_Prd_Code")) {
                dBColumn = "UPPER((PART_PRD_CODE)";
            } else if (cloumn.equals("art_Prd_Code_Desc")) {
                dBColumn = "UPPER(PART_PRD_CODE_DESC)";
            } else if (cloumn.equals("part_Prd_Family")) {
                dBColumn = "UPPER(PART_PRD_FAMILY)";
            } else if (cloumn.equals("part_Prd_Family_Desc")) {
                dBColumn = "UPPER(PART_PRD_FAMILY_DESC)";
            } else if (cloumn.equals("commodity_Group_1")) {
                dBColumn = "UPPER(COMMODITY_GROUP_1)";
            } else if (cloumn.equals("commodity_Group_2")) {
                dBColumn = "UPPER(COMMODITY_GROUP_2)";
            } else if (cloumn.equals("selling_Price")) {
                dBColumn = "SELLING_PRICE";
            } else if (cloumn.equals("cost_Price")) {
                dBColumn = "COST_PRICE";
            } else if (cloumn.equals("dealer_Margin")) {
                dBColumn = "DEALER_MARGIN";
            } else if (cloumn.equals("distributor_Margin")) {
                dBColumn = "DISTRIBUTOR_MARGIN";
            } else if (cloumn.equals("rep_Category")) {
                dBColumn = "REP_CAT_DESC";
            } else if (cloumn.equals("wrn_Scheme")) {
                dBColumn = "UPPER(SCHEME_NAME)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

    public static String part(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("partNo")) {
                dBColumn = "SPARE_PART_NO";
            } else if (cloumn.equals("partDesc")) {
                dBColumn = "UPPER(PART_DESCRIPTION)";
            } else if (cloumn.equals("partExist")) {
                dBColumn = "ALTERNATE_PART_EXISTS";
            } else if (cloumn.equals("partPrdCode")) {
                dBColumn = "UPPER(PART_PRD_CODE)";
            } else if (cloumn.equals("partPrdDesc")) {
                dBColumn = "UPPER(PART_PRD_CODE_DESC)";
            } else if (cloumn.equals("partPrdFamily")) {
                dBColumn = "UPPER(PART_PRD_FAMILY)";
            } else if (cloumn.equals("partPrdFamilyDesc")) {
                dBColumn = "UPPER(PART_PRD_FAMILY_DESC)";
            } else if (cloumn.equals("partGroup1")) {
                dBColumn = "UPPER(COMMODITY_GROUP_1)";
            } else if (cloumn.equals("partGroup2")) {
                dBColumn = "UPPER(COMMODITY_GROUP_2)";
            } else if (cloumn.equals("partSellPrice")) {
                dBColumn = "SELLING_PRICE";
            } else if (cloumn.equals("partCost")) {
                dBColumn = "COST_PRICE";
            }else if (cloumn.equals("erpPartNo")) {
                dBColumn = "ERP_SPARE_PART_NO";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

    public static String returnReplacement(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("replRetutnNo")) {
                dBColumn = "R.REPL_RETURN_NO";
            } else if (cloumn.equals("exchangeReferenceNo")) {
                dBColumn = "UPPER(R.EXG_REF_NO)";
            } else if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(R.IMEI_NO)";
            } else if (cloumn.equals("returnDate")) {
                dBColumn = "R.RETURN_DATE";
            } else if (cloumn.equals("modleDesc")) {
                dBColumn = "L.MODEL_DESCRIPTION";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

    public static String warrentyAdjusment(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("iMEI")) {
                dBColumn = "UPPER(IMEI)";
            } else if (cloumn.equals("extendedDays")) {
                dBColumn = "EXTENDED_DAYS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper warrentyAdjusment");
        }
        return dBColumn;
    }

    public static String warrentSchema(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("schemeId")) {
                dBColumn = "SCHEME_ID";
            } else if (cloumn.equals("schemeName")) {
                dBColumn = "UPPER(SCHEME_NAME)";
            } else if (cloumn.equals("period")) {
                dBColumn = "PERIOD";
            } else if (cloumn.equals("status")) {
                dBColumn = "STATUS";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

    public static String workOrderFeedBack(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("feedBackId")) {
                dBColumn = "F.FEEDBACK_ID";
            } else if (cloumn.equals("complainDate")) {
                dBColumn = "F.DATE_INSERTED";
            } else if (cloumn.equals("feedbackTypeDesc")) {
                dBColumn = "UPPER(D.DESCRIPTION)";
            } else if (cloumn.equals("prduct")) {
                dBColumn = "UPPER(W.PRODUCT)";
            } else if (cloumn.equals("modelName")) {
                dBColumn = "UPPER(M.MODEL_DESCRIPTION)";
            } else if (cloumn.equals("purchaseShop")) {
                dBColumn = "UPPER(B.BIS_NAME)";
            } else if (cloumn.equals("purchaseDate")) {
                dBColumn = "R.REGISTER_DATE";
            } else if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(F.WORKORDER_NO)";
            } else if (cloumn.equals("serviceCenter")) {
                dBColumn = "UPPER(B1.BIS_NAME)";
            } else if (cloumn.equals("woOpenDate")) {
                dBColumn = "W.DATE_INSERTED";
            } else if (cloumn.equals("deleveryDate")) {
                dBColumn = "W.DELIVERY_DATE";
            } else if (cloumn.equals("status")) {
                dBColumn = "W.STATUS";
            } else if (cloumn.equals("warrenty")) {
                dBColumn = "D1.DESCRIPTION";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper workOrderFeedBack");
        }
        return dBColumn;
    }

    public static String workOrderMaintain(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(M.WORK_ORDER_NO)";
            } else if (cloumn.equals("customerName")) {
                dBColumn = "UPPER(M.TECHNICIAN)";
            } else if (cloumn.equals("iemiNo")) {
                dBColumn = "UPPER(W.IMEI_NO)";
            } else if (cloumn.equals("modelDesc")) {
                dBColumn = "UPPER(L.MODEL_DESCRIPTION)";
            } else if (cloumn.equals("levelDesc")) {
                dBColumn = "UPPER(R.LEVEL_NAME)";
            } else if (cloumn.equals("statusMsg")) {
                dBColumn = "D1.DESCRIPTION";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

    public static String workOrder(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("workOrderNo")) {
                dBColumn = "UPPER(W.WORK_ORDER_NO)";
            } else if (cloumn.equals("customerName")) {
                dBColumn = "UPPER(W.CUSTOMER_NAME)";
            } else if (cloumn.equals("customerAddress")) {
                dBColumn = "UPPER(W.CUSTOMER_ADDRESS)";
            } else if (cloumn.equals("workTelephoneNo")) {
                dBColumn = "UPPER(W.TELEPHONE_NO)";
            } else if (cloumn.equals("email")) {
                dBColumn = "UPPER(W.EMAIL)";
            } else if (cloumn.equals("customerNic")) {
                dBColumn = "UPPER(W.CUSTOMER_NIC)";
            } else if (cloumn.equals("imeiNo")) {
                dBColumn = "UPPER(W.IMEI_NO)";
            } else if (cloumn.equals("product")) {
                dBColumn = "UPPER(W.PRODUCT)";
            } else if (cloumn.equals("brand")) {
                dBColumn = "UPPER(W.BRAND)";
            } else if (cloumn.equals("modleNo")) {
                dBColumn = "W.MODEL_NO";
            } else if (cloumn.equals("defectNo")) {
                dBColumn = "W.DEFECTS";
            } else if (cloumn.equals("rccReference")) {
                dBColumn = "UPPER(W.RCC_REFERENCE)";
            } else if (cloumn.equals("dateOfSale")) {
                dBColumn = "W.DATE_OF_SALE";
            } else if (cloumn.equals("proofOfPurches")) {
                dBColumn = "UPPER(W.PROOF_OF_PURCHASE)";
            } else if (cloumn.equals("bisId")) {
                dBColumn = "W.BIS_ID";
            } else if (cloumn.equals("warrentyVrifType")) {
                dBColumn = "W.WARRANTY_VRIF_TYPE";
            } else if (cloumn.equals("deleveryDate")) {
                dBColumn = "W.DELIVERY_DATE";
            } else if (cloumn.equals("statusMsg")) {
                dBColumn = "W.STATUS";
            } else if (cloumn.equals("createDate")) {
                dBColumn = "W.DATE_INSERTED";
            } else if (cloumn.equals("serviceBisDesc")) {
                dBColumn = "B1.BIS_NAME";
            } else if (cloumn.equals("modleDesc")) {
                dBColumn = "S.MODEL_DESCRIPTION";
            } else if (cloumn.equals("defectDesc")) {
                dBColumn = "D.MAJ_CODE_DESC";
            } else if (cloumn.equals("warrantyStatusMsg")) {
                dBColumn = "M.DESCRIPTION";
            } else if (cloumn.equals("delayDate")) {
                dBColumn = "(TRUNC(SYSDATE - W.DELIVERY_DATE))";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }
    
    public static String userBusinessMapping(String cloumn) {
        String dBColumn = null;
        try {
            if (cloumn.equals("bisName")) {
                dBColumn = "UPPER(B.FIRST_NAME)";
            } else if (cloumn.equals("customerNo")) {
                dBColumn = "UPPER(U.CUSTOMER_CODE)";
            } else {
                dBColumn = "all";
            }
        } catch (Exception ex) {
            logger.info("Error in DB Mapper minorDefectCode");
        }
        return dBColumn;
    }

}
