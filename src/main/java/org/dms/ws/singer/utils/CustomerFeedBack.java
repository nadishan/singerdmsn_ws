/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.utils;

import com.sun.mail.smtp.SMTPTransport;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.dms.ws.singer.entities.EstimatedParts;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import static org.dms.ws.singer.utils.EmailSender.mailAddress;
import static org.dms.ws.singer.utils.EmailSender.mailIp;
import static org.dms.ws.singer.utils.EmailSender.mailPort;
import static org.dms.ws.singer.utils.EmailSender.mailTransport;
import static org.dms.ws.singer.utils.EmailSender.port;
import static org.dms.ws.singer.utils.EmailSender.sentFrom;
import static org.dms.ws.singer.utils.EmailSender.sentFromPword;
import static org.dms.ws.singer.utils.EmailSender.smtpsAuth;
import static org.dms.ws.singer.utils.EmailSender.smtpsHost;

/**
 *
 * @author SDU
 */
public class CustomerFeedBack {

    public static boolean sendHTMLMail(String toMailAdd, String firstname, String amount, String estimate_number, String refernce, String woNumber, ArrayList<EstimatedParts> partDetailsList, double totalAmount, String repairLevel, double repairPrice, String model, String imei) throws MessagingException {
        boolean isSent = false;
        try {

            HashMap mailConfig = EmailSender.getMailConfigurations();
            mailTransport = mailConfig.get("mailTransport").toString();
            mailIp = mailConfig.get("mailIp").toString();
            smtpsHost = mailConfig.get("smtpsHost").toString();
            mailAddress = mailConfig.get("mailAddress").toString();
            smtpsAuth = mailConfig.get("smtpsAuth").toString();
            sentFrom = mailConfig.get("sentFrom").toString();
            sentFromPword = mailConfig.get("sentFromPword").toString();
            mailPort = mailConfig.get("mailPort").toString();
            port = Integer.parseInt(mailPort);

            Properties props = System.getProperties();
            props.put(smtpsHost, sentFrom);
            props.put(smtpsAuth, "true");
            //Session session = Session.getInstance(props, null);
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(sentFrom, sentFromPword);
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailAddress));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            msg.setSubject("ESTIMATE FOR REPAIR AND SERVICE-{" + woNumber + "}");

            String tableColumn = "";
            if (partDetailsList.size() > 0) {

                for (EstimatedParts est : partDetailsList) {
                    tableColumn += "<tr>";
                    tableColumn += "<td>" + est.getPartNo() + "</td>"
                            + "<td>" + est.getPartDescription() + "</td>"
                            + "<td>" + est.getQty() + "</td>"
                            + "<td>" + est.getUnitPrice() + "</td>"
                            + "<td>" + est.getAmount() + "</td>";
                    tableColumn += "</tr>";
                }

            }
            
            ///////////////////////repair reference number is set to Work Order No as other emails
            String html = "<html><head><title>Page Title</title></head>"
                    + "<body>"
                    + "<p>Dear Valued Customer,</p>"
                    + "<p>This is to inform you the estimate of your product repair reference number is: " + woNumber + " of model: " + model + " Serial/IMEI: " + imei + ".</p>"
                    + "<table style=\"width:100%\">"
                    + "<tr>"
                    + "<td>Part No</td>"
                    + "<td>Description</td>"
                    + "<td>Qty</td>"
                    + "<td>Unit Price(Rs.)</td>"
                    + "<td>Amount(Rs.)</td>"
                    + "</tr>"
                    + tableColumn
                    + "<tr>"
                    + "<td>Repair Level: </td>"
                    + "<td>" + repairLevel + "</td>"
                    + "<td></td>"
                    + "<td></td>"
                    + "<td>" + repairPrice + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td></td>"
                    + "<td></td>"
                    + "<td></td>"
                    + "<td>Total: </td>"
                    + "<td><u>" + totalAmount + "</u></td>"
                    + "</tr>"
                    + "</table>"
                    + "<br>"
                    + "<p>Please do the payment to the Singer Service Center/ Service Agent in order to precede the repair.</p>"
                    + "<br>"
                    + "<p>The estimate is valid only for 14 days. Beyond 14 days without a reply the service will regard the repair has been cancelled, will send back the unit without repair. Singer is not taking any responsibility for products not removed within 14 days after notice of removal has been given.</p>"
                    + "<br>"
                    + "<p>For further inquiries, please call us on 0115 400 400 during business hours.   </p>"
                    + "<br>"
                    + "<p>We thank you for buying our products.</p>"
                    + "<p>SINGER</p>"
                    + "<br>"
                    + "<p>&#42;&#42;Please note that this is an automated response and replies to this email will not be answered.</p>"
                    + "</body></html>";

            // System.out.println("Email: "+toMailAdd);
            // System.out.println(html);
            msg.setContent(html, "text/html; charset=utf-8");

//            msg.setContent("<html><head><title>Page Title</title></head>"
//                    + "<body><p>Dear " + firstname + ",</p>"
//                    + "<h3>Your Work Order Estimate Created</h3>"
//                    + "<p>* Download Singer Customer Mobile Application: http://yourlinkhere.... </p> "
//                    + "<p>* Access Singer Customer Web Application: http://yourlinkhere...   </p>"
//                    + "<p>WorkOrder Estimate Number            : " + estimate_number + "</p>"
//                    + "<p>Amount is                            : " + amount + "</p>"
//                    + "<p>WorkOrder Work Order NUmber          : " + woNumber + "</p>"
//                    + "<p>Customer Refernce                    : " + refernce + "</p>"
//                    + "<p style='color:#9A2EFE;'>Note: You can register in Singer Customer Network via your NIC Number</p>"
//                    + "<p>Thank you!</p>"
//                    + "<p>Best Regards,</p>"
//                    + "<p>Singer Admin</p>"
//                    + "</body></html>", "text/html; charset=utf-8");
            msg.setHeader("X-Mailer", "Singer ISS WorkOrder mail");

            msg.setSentDate(new Date());
            SMTPTransport t = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, port, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........");
            ex.printStackTrace();
        }

        return isSent;
    }

    public static HashMap getMailConfigurations() {

        HashMap mailConfig = new HashMap();

        try {

            mailConfig.put("mailTransport", AppParams.MAIL_TRANSPORT);
            mailConfig.put("mailIp", AppParams.MAIL_CONNECT_IP);
            mailConfig.put("smtpsHost", AppParams.MAIL_SMTPS_HOST);
            mailConfig.put("mailAddress", AppParams.MAIL_ADDRESS);
            mailConfig.put("smtpsAuth", AppParams.MAIL_SMTPS_AUTH);
            mailConfig.put("sentFrom", AppParams.MAIL_SENT_FROM);
            mailConfig.put("sentFromPword", AppParams.MAIL_SENT_FROM_PASSWORD);
            mailConfig.put("mailPort", AppParams.MAIL_PORT);

        } catch (Exception e) {
            e.getMessage();
        }
        return mailConfig;
    }

}
