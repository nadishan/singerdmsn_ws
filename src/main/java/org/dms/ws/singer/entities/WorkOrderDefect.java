/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderDefect {
    
    //WRK_ORD_DEFT_ID,WRK_ORD_MTN_ID,MAJ_CODE,MIN_CODE,REMARKS,STATUS
    
    private int woDefectId;
    private int woMaintainId;
    private int majorCode;
    private int minorCode;
    private String remarks;
    private int status;
    private String majDesc;
    private String minDesc;

    /**
     * @return the woDefectId
     */
    public int getWoDefectId() {
        return woDefectId;
    }

    /**
     * @param woDefectId the woDefectId to set
     */
    public void setWoDefectId(int woDefectId) {
        this.woDefectId = woDefectId;
    }

    /**
     * @return the woMaintainId
     */
    public int getWoMaintainId() {
        return woMaintainId;
    }

    /**
     * @param woMaintainId the woMaintainId to set
     */
    public void setWoMaintainId(int woMaintainId) {
        this.woMaintainId = woMaintainId;
    }

    /**
     * @return the majorCode
     */
    public int getMajorCode() {
        return majorCode;
    }

    /**
     * @param majorCode the majorCode to set
     */
    public void setMajorCode(int majorCode) {
        this.majorCode = majorCode;
    }

    /**
     * @return the minorCode
     */
    public int getMinorCode() {
        return minorCode;
    }

    /**
     * @param minorCode the minorCode to set
     */
    public void setMinorCode(int minorCode) {
        this.minorCode = minorCode;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the majDesc
     */
    public String getMajDesc() {
        return majDesc;
    }

    /**
     * @param majDesc the majDesc to set
     */
    public void setMajDesc(String majDesc) {
        this.majDesc = majDesc;
    }

    /**
     * @return the minDesc
     */
    public String getMinDesc() {
        return minDesc;
    }

    /**
     * @param minDesc the minDesc to set
     */
    public void setMinDesc(String minDesc) {
        this.minDesc = minDesc;
    }
    
}
