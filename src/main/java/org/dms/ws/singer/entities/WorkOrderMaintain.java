/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class WorkOrderMaintain {
    
    //WRK_ORD_MTN_ID,WORK_ORDER_NO,WARRANTY_VRIF_TYPE,NON_WARRANTY_VRIF_TYPE,TECHNICIAN,
//ACTION_TAKEN,REPAIR_LEVEL_ID,REPAIR_LEVEL_AMOUNT,REMARKS,REPAIR_STATUS,EST_CLOSING_DATE
    
    private int workorderMaintainId;
    private String workOrderNo;
    private int warrantyVrifType;
    private String warrantyTypedesc;
    private int nonWarrantyType;
    private String nonWarrantyTypeDesc;
    private String technician;
    private String actionTaken;
    private int repairLevelId;
    private double repairAmount;
    private String remarks;
    private int repairStatus;
    private String estimateClosingDate;
    private String warantyStatus;
    private String iemiNo;
    private String modelDesc;
    private String levelDesc;
    private String estimateStatusMsg;
    private String statusMsg;
    private String invoiceNo;
    
    private ArrayList<WorkOrderMaintainDetail> woDetials;
    private ArrayList<WorkOrderDefect> woDefect;
    private ArrayList<WorkOrderEstimate> woEstimate;
    
    private int bisId;
    private int expireStatus;
    
    private String transferStatus;
 
    /**
     * @return the workorderMaintainId
     */
    public int getWorkorderMaintainId() {
        return workorderMaintainId;
    }

    /**
     * @param workorderMaintainId the workorderMaintainId to set
     */
    public void setWorkorderMaintainId(int workorderMaintainId) {
        this.workorderMaintainId = workorderMaintainId;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the warrantyVrifType
     */
    public int getWarrantyVrifType() {
        return warrantyVrifType;
    }

    /**
     * @param warrantyVrifType the warrantyVrifType to set
     */
    public void setWarrantyVrifType(int warrantyVrifType) {
        this.warrantyVrifType = warrantyVrifType;
    }

    /**
     * @return the warrantyTypedesc
     */
    public String getWarrantyTypedesc() {
        return warrantyTypedesc;
    }

    /**
     * @param warrantyTypedesc the warrantyTypedesc to set
     */
    public void setWarrantyTypedesc(String warrantyTypedesc) {
        this.warrantyTypedesc = warrantyTypedesc;
    }

    /**
     * @return the nonWarrantyType
     */
    public int getNonWarrantyType() {
        return nonWarrantyType;
    }

    /**
     * @param nonWarrantyType the nonWarrantyType to set
     */
    public void setNonWarrantyType(int nonWarrantyType) {
        this.nonWarrantyType = nonWarrantyType;
    }

    /**
     * @return the nonWarrantyTypeDesc
     */
    public String getNonWarrantyTypeDesc() {
        return nonWarrantyTypeDesc;
    }

    /**
     * @param nonWarrantyTypeDesc the nonWarrantyTypeDesc to set
     */
    public void setNonWarrantyTypeDesc(String nonWarrantyTypeDesc) {
        this.nonWarrantyTypeDesc = nonWarrantyTypeDesc;
    }

    /**
     * @return the technician
     */
    public String getTechnician() {
        return technician;
    }

    /**
     * @param technician the technician to set
     */
    public void setTechnician(String technician) {
        this.technician = technician;
    }

    /**
     * @return the actionTaken
     */
    public String getActionTaken() {
        return actionTaken;
    }

    /**
     * @param actionTaken the actionTaken to set
     */
    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    /**
     * @return the repairLevelId
     */
    public int getRepairLevelId() {
        return repairLevelId;
    }

    /**
     * @param repairLevelId the repairLevelId to set
     */
    public void setRepairLevelId(int repairLevelId) {
        this.repairLevelId = repairLevelId;
    }

    /**
     * @return the repairAmount
     */
    public double getRepairAmount() {
        return repairAmount;
    }

    /**
     * @param repairAmount the repairAmount to set
     */
    public void setRepairAmount(double repairAmount) {
        this.repairAmount = repairAmount;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the repairStatus
     */
    public int getRepairStatus() {
        return repairStatus;
    }

    /**
     * @param repairStatus the repairStatus to set
     */
    public void setRepairStatus(int repairStatus) {
        this.repairStatus = repairStatus;
    }

    /**
     * @return the estimateClosingDate
     */
    public String getEstimateClosingDate() {
        return estimateClosingDate;
    }

    /**
     * @param estimateClosingDate the estimateClosingDate to set
     */
    public void setEstimateClosingDate(String estimateClosingDate) {
        this.estimateClosingDate = estimateClosingDate;
    }

    /**
     * @return the warantyStatus
     */
    public String getWarantyStatus() {
        return warantyStatus;
    }

    /**
     * @param warantyStatus the warantyStatus to set
     */
    public void setWarantyStatus(String warantyStatus) {
        this.warantyStatus = warantyStatus;
    }

    /**
     * @return the iemiNo
     */
    public String getIemiNo() {
        return iemiNo;
    }

    /**
     * @param iemiNo the iemiNo to set
     */
    public void setIemiNo(String iemiNo) {
        this.iemiNo = iemiNo;
    }

    /**
     * @return the modelDesc
     */
    public String getModelDesc() {
        return modelDesc;
    }

    /**
     * @param modelDesc the modelDesc to set
     */
    public void setModelDesc(String modelDesc) {
        this.modelDesc = modelDesc;
    }

    /**
     * @return the levelDesc
     */
    public String getLevelDesc() {
        return levelDesc;
    }

    /**
     * @param levelDesc the levelDesc to set
     */
    public void setLevelDesc(String levelDesc) {
        this.levelDesc = levelDesc;
    }

    /**
     * @return the estimateStatusMsg
     */
    public String getEstimateStatusMsg() {
        return estimateStatusMsg;
    }

    /**
     * @param estimateStatusMsg the estimateStatusMsg to set
     */
    public void setEstimateStatusMsg(String estimateStatusMsg) {
        this.estimateStatusMsg = estimateStatusMsg;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the woDetials
     */
    public ArrayList<WorkOrderMaintainDetail> getWoDetials() {
        return woDetials;
    }

    /**
     * @param woDetials the woDetials to set
     */
    public void setWoDetials(ArrayList<WorkOrderMaintainDetail> woDetials) {
        this.woDetials = woDetials;
    }

    /**
     * @return the woDefect
     */
    public ArrayList<WorkOrderDefect> getWoDefect() {
        return woDefect;
    }

    /**
     * @param woDefect the woDefect to set
     */
    public void setWoDefect(ArrayList<WorkOrderDefect> woDefect) {
        this.woDefect = woDefect;
    }

    /**
     * @return the woEstimate
     */
    public ArrayList<WorkOrderEstimate> getWoEstimate() {
        return woEstimate;
    }

    /**
     * @param woEstimate the woEstimate to set
     */
    public void setWoEstimate(ArrayList<WorkOrderEstimate> woEstimate) {
        this.woEstimate = woEstimate;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getBisId() {
        return bisId;
    }

    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    public int getExpireStatus() {
        return expireStatus;
    }

    public void setExpireStatus(int expireStatus) {
        this.expireStatus = expireStatus;
    }

    public String getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus) {
        this.transferStatus = transferStatus;
    }
    
    
    
}
