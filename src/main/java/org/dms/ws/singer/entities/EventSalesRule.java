/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class EventSalesRule {

    private int ruleId;
    private int eventId;
    private String productFamily;
    private String barnd;
    private String modleDesc;
    private int modleNo;
    private int period;
    private int intervals;
    private int acceptedInteval;
    private int salesType;
    private double point;
    private double minReq;
    private int pointSchemeFlag;
    private double totalAchivement;
    private int status;
    private double scheamfixedPoint;
    private int schmefloatFlag;
    private String schmefloagFlagString;
    private double schmefloatPoint;
    private int schemeSaleType;
    private int schmeNoOfUnit;
    private int schmeAddnSalesType;
    private double schmeAddnSalesPoint;
    private double schmeAssignPoint;

    
    
    
    private ArrayList<EventRulePointScheme> eventRulePointSchemaList;
    
    
    

    /**
     * @return the ruleId
     */
    public int getRuleId() {
        return ruleId;
    }

    /**
     * @param ruleId the ruleId to set
     */
    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * @return the eventId
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the productFamily
     */
    public String getProductFamily() {
        return productFamily;
    }

    /**
     * @param productFamily the productFamily to set
     */
    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    /**
     * @return the barnd
     */
    public String getBarnd() {
        return barnd;
    }

    /**
     * @param barnd the barnd to set
     */
    public void setBarnd(String barnd) {
        this.barnd = barnd;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the period
     */
    public int getPeriod() {
        return period;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(int period) {
        this.period = period;
    }

    /**
     * @return the intervals
     */
    public int getIntervals() {
        return intervals;
    }

    /**
     * @param intervals the intervals to set
     */
    public void setIntervals(int intervals) {
        this.intervals = intervals;
    }

    /**
     * @return the acceptedInteval
     */
    public int getAcceptedInteval() {
        return acceptedInteval;
    }

    /**
     * @param acceptedInteval the acceptedInteval to set
     */
    public void setAcceptedInteval(int acceptedInteval) {
        this.acceptedInteval = acceptedInteval;
    }

    /**
     * @return the salesType
     */
    public int getSalesType() {
        return salesType;
    }

    /**
     * @param salesType the salesType to set
     */
    public void setSalesType(int salesType) {
        this.salesType = salesType;
    }

    /**
     * @return the pointSchemeFlag
     */
    public int getPointSchemeFlag() {
        return pointSchemeFlag;
    }

    /**
     * @param pointSchemeFlag the pointSchemeFlag to set
     */
    public void setPointSchemeFlag(int pointSchemeFlag) {
        this.pointSchemeFlag = pointSchemeFlag;
    }

    

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the point
     */
    public double getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(double point) {
        this.point = point;
    }

    /**
     * @return the minReq
     */
    public double getMinReq() {
        return minReq;
    }

    /**
     * @param minReq the minReq to set
     */
    public void setMinReq(double minReq) {
        this.minReq = minReq;
    }

    /**
     * @return the totalAchivement
     */
    public double getTotalAchivement() {
        return totalAchivement;
    }

    /**
     * @param totalAchivement the totalAchivement to set
     */
    public void setTotalAchivement(double totalAchivement) {
        this.totalAchivement = totalAchivement;
    }


    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }


    /**
     * @return the eventRulePointSchemaList
     */
    public ArrayList<EventRulePointScheme> getEventRulePointSchemaList() {
        return eventRulePointSchemaList;
    }

    /**
     * @param eventRulePointSchemaList the eventRulePointSchemaList to set
     */
    public void setEventRulePointSchemaList(ArrayList<EventRulePointScheme> eventRulePointSchemaList) {
        this.eventRulePointSchemaList = eventRulePointSchemaList;
    }

    /**
     * @return the scheamfixedPoint
     */
    public double getScheamfixedPoint() {
        return scheamfixedPoint;
    }

    /**
     * @param scheamfixedPoint the scheamfixedPoint to set
     */
    public void setScheamfixedPoint(double scheamfixedPoint) {
        this.scheamfixedPoint = scheamfixedPoint;
    }



    /**
     * @return the schemeSaleType
     */
    public int getSchemeSaleType() {
        return schemeSaleType;
    }

    /**
     * @param schemeSaleType the schemeSaleType to set
     */
    public void setSchemeSaleType(int schemeSaleType) {
        this.schemeSaleType = schemeSaleType;
    }

    /**
     * @return the schmeNoOfUnit
     */
    public int getSchmeNoOfUnit() {
        return schmeNoOfUnit;
    }

    /**
     * @param schmeNoOfUnit the schmeNoOfUnit to set
     */
    public void setSchmeNoOfUnit(int schmeNoOfUnit) {
        this.schmeNoOfUnit = schmeNoOfUnit;
    }

    /**
     * @return the schmeAddnSalesType
     */
    public int getSchmeAddnSalesType() {
        return schmeAddnSalesType;
    }

    /**
     * @param schmeAddnSalesType the schmeAddnSalesType to set
     */
    public void setSchmeAddnSalesType(int schmeAddnSalesType) {
        this.schmeAddnSalesType = schmeAddnSalesType;
    }

    /**
     * @return the schmeAddnSalesPoint
     */
    public double getSchmeAddnSalesPoint() {
        return schmeAddnSalesPoint;
    }

    /**
     * @param schmeAddnSalesPoint the schmeAddnSalesPoint to set
     */
    public void setSchmeAddnSalesPoint(double schmeAddnSalesPoint) {
        this.schmeAddnSalesPoint = schmeAddnSalesPoint;
    }

    /**
     * @return the schmeAssignPoint
     */
    public double getSchmeAssignPoint() {
        return schmeAssignPoint;
    }

    /**
     * @param schmeAssignPoint the schmeAssignPoint to set
     */
    public void setSchmeAssignPoint(double schmeAssignPoint) {
        this.schmeAssignPoint = schmeAssignPoint;
    }

    /**
     * @return the schmefloatFlag
     */
    public int getSchmefloatFlag() {
        return schmefloatFlag;
    }

    /**
     * @param schmefloatFlag the schmefloatFlag to set
     */
    public void setSchmefloatFlag(int schmefloatFlag) {
        this.schmefloatFlag = schmefloatFlag;
    }

    /**
     * @return the schmefloagFlagString
     */
    public String getSchmefloagFlagString() {
        return schmefloagFlagString;
    }

    /**
     * @param schmefloagFlagString the schmefloagFlagString to set
     */
    public void setSchmefloagFlagString(String schmefloagFlagString) {
        this.schmefloagFlagString = schmefloagFlagString;
    }

    /**
     * @return the schmefloatPoint
     */
    public double getSchmefloatPoint() {
        return schmefloatPoint;
    }

    /**
     * @param schmefloatPoint the schmefloatPoint to set
     */
    public void setSchmefloatPoint(double schmefloatPoint) {
        this.schmefloatPoint = schmefloatPoint;
    }



}
