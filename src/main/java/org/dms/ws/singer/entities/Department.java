/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class Department {
    
    private int departmentType;
    private String depatmentDesc;
    private int entryCode;
    private int status;

    /**
     * @return the departmentType
     */
    public int getDepartmentType() {
        return departmentType;
    }

    /**
     * @param departmentType the departmentType to set
     */
    public void setDepartmentType(int departmentType) {
        this.departmentType = departmentType;
    }

    /**
     * @return the depatmentDesc
     */
    public String getDepatmentDesc() {
        return depatmentDesc;
    }

    /**
     * @param depatmentDesc the depatmentDesc to set
     */
    public void setDepatmentDesc(String depatmentDesc) {
        this.depatmentDesc = depatmentDesc;
    }

    /**
     * @return the entryCode
     */
    public int getEntryCode() {
        return entryCode;
    }

    /**
     * @param entryCode the entryCode to set
     */
    public void setEntryCode(int entryCode) {
        this.entryCode = entryCode;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
