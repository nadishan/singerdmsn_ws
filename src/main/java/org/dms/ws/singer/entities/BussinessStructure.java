/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class BussinessStructure {
    
    private int bisId;
    private String bisName;
    private String bisDesc;
    private int bisPrtnId;
    private int bisUserCount;
    private int category1;
    private int category2;
    private String teleNumber;
    private String address;
    private String logitude;
    private String latitude;
    private int structType;
    private int mapFlag;
    private int status;
    private int statusOfline;
    private String user;
    private String erpCode;
    private String poTitle;
    private String cheqTitle;
    private String essdAcntNo;
    private String eddsPct;
    private String invoiceNo;
            

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the bisDesc
     */
    public String getBisDesc() {
        return bisDesc;
    }

    /**
     * @param bisDesc the bisDesc to set
     */
    public void setBisDesc(String bisDesc) {
        this.bisDesc = bisDesc;
    }

    /**
     * @return the bisPrtnId
     */
    public int getBisPrtnId() {
        return bisPrtnId;
    }

    /**
     * @param bisPrtnId the bisPrtnId to set
     */
    public void setBisPrtnId(int bisPrtnId) {
        this.bisPrtnId = bisPrtnId;
    }

    /**
     * @return the bisUserCount
     */
    public int getBisUserCount() {
        return bisUserCount;
    }

    /**
     * @param bisUserCount the bisUserCount to set
     */
    public void setBisUserCount(int bisUserCount) {
        this.bisUserCount = bisUserCount;
    }

    /**
     * @return the category1
     */
    public int getCategory1() {
        return category1;
    }

    /**
     * @param category1 the category1 to set
     */
    public void setCategory1(int category1) {
        this.category1 = category1;
    }

    /**
     * @return the category2
     */
    public int getCategory2() {
        return category2;
    }

    /**
     * @param category2 the category2 to set
     */
    public void setCategory2(int category2) {
        this.category2 = category2;
    }

    /**
     * @return the teleNumber
     */
    public String getTeleNumber() {
        return teleNumber;
    }

    /**
     * @param teleNumber the teleNumber to set
     */
    public void setTeleNumber(String teleNumber) {
        this.teleNumber = teleNumber;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the logitude
     */
    public String getLogitude() {
        return logitude;
    }

    /**
     * @param logitude the logitude to set
     */
    public void setLogitude(String logitude) {
        this.logitude = logitude;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the structType
     */
    public int getStructType() {
        return structType;
    }

    /**
     * @param structType the structType to set
     */
    public void setStructType(int structType) {
        this.structType = structType;
    }

    /**
     * @return the mapFlag
     */
    public int getMapFlag() {
        return mapFlag;
    }

    /**
     * @param mapFlag the mapFlag to set
     */
    public void setMapFlag(int mapFlag) {
        this.mapFlag = mapFlag;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the erpCode
     */
    public String getErpCode() {
        return erpCode;
    }

    /**
     * @param erpCode the erpCode to set
     */
    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    /**
     * @return the poTitle
     */
    public String getPoTitle() {
        return poTitle;
    }

    /**
     * @param poTitle the poTitle to set
     */
    public void setPoTitle(String poTitle) {
        this.poTitle = poTitle;
    }

    /**
     * @return the cheqTitle
     */
    public String getCheqTitle() {
        return cheqTitle;
    }

    /**
     * @param cheqTitle the cheqTitle to set
     */
    public void setCheqTitle(String cheqTitle) {
        this.cheqTitle = cheqTitle;
    }

    /**
     * @return the essdAcntNo
     */
    public String getEssdAcntNo() {
        return essdAcntNo;
    }

    /**
     * @param essdAcntNo the essdAcntNo to set
     */
    public void setEssdAcntNo(String essdAcntNo) {
        this.essdAcntNo = essdAcntNo;
    }

    /**
     * @return the eddsPct
     */
    public String getEddsPct() {
        return eddsPct;
    }

    /**
     * @param eddsPct the eddsPct to set
     */
    public void setEddsPct(String eddsPct) {
        this.eddsPct = eddsPct;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getStatusOfline() {
        return statusOfline;
    }

    public void setStatusOfline(int statusOfline) {
        this.statusOfline = statusOfline;
    }
            

   
    
}
