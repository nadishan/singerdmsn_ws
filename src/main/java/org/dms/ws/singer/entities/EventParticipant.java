/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class EventParticipant {
    
    private String paticipantName;
    private String location;
    private String paticipantType;
    private String achivePoint;
    private String posotion;

    /**
     * @return the paticipantName
     */
    public String getPaticipantName() {
        return paticipantName;
    }

    /**
     * @param paticipantName the paticipantName to set
     */
    public void setPaticipantName(String paticipantName) {
        this.paticipantName = paticipantName;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the paticipantType
     */
    public String getPaticipantType() {
        return paticipantType;
    }

    /**
     * @param paticipantType the paticipantType to set
     */
    public void setPaticipantType(String paticipantType) {
        this.paticipantType = paticipantType;
    }

    /**
     * @return the achivePoint
     */
    public String getAchivePoint() {
        return achivePoint;
    }

    /**
     * @param achivePoint the achivePoint to set
     */
    public void setAchivePoint(String achivePoint) {
        this.achivePoint = achivePoint;
    }

    /**
     * @return the posotion
     */
    public String getPosotion() {
        return posotion;
    }

    /**
     * @param posotion the posotion to set
     */
    public void setPosotion(String posotion) {
        this.posotion = posotion;
    }
    
}
