/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderFeedBack {
    
    private String workOrderNo;
    private int feedBackId;
    private int feedBackType;
    private String feedBack;
    private String prduct;
    private String modelName;
    private String feedbackTypeDesc;
    private String user;
    private String complainDate;
    private String purchaseDate;
    private String purchaseShop;
    private String serviceCenter;
    private String woOpenDate;
    private String deleveryDate;
    private int status;
    private String statusMsg;
    private String customerName;
    private String telephoneNo;
    private String address;
    private String imeiNo;
    private String email;
    private String customerNic;
    private String warrenty;
    private String rccRefe;
    private String woReply;
    private String replyStatus;

    


    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the feedBackId
     */
    public int getFeedBackId() {
        return feedBackId;
    }

    /**
     * @param feedBackId the feedBackId to set
     */
    public void setFeedBackId(int feedBackId) {
        this.feedBackId = feedBackId;
    }

    /**
     * @return the feedBackType
     */
    public int getFeedBackType() {
        return feedBackType;
    }

    /**
     * @param feedBackType the feedBackType to set
     */
    public void setFeedBackType(int feedBackType) {
        this.feedBackType = feedBackType;
    }

    /**
     * @return the feedBack
     */
    public String getFeedBack() {
        return feedBack;
    }

    /**
     * @param feedBack the feedBack to set
     */
    public void setFeedBack(String feedBack) {
        this.feedBack = feedBack;
    }

    /**
     * @return the prduct
     */
    public String getPrduct() {
        return prduct;
    }

    /**
     * @param prduct the prduct to set
     */
    public void setPrduct(String prduct) {
        this.prduct = prduct;
    }

    /**
     * @return the modelName
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * @param modelName the modelName to set
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     * @return the feedbackTypeDesc
     */
    public String getFeedbackTypeDesc() {
        return feedbackTypeDesc;
    }

    /**
     * @param feedbackTypeDesc the feedbackTypeDesc to set
     */
    public void setFeedbackTypeDesc(String feedbackTypeDesc) {
        this.feedbackTypeDesc = feedbackTypeDesc;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the complainDate
     */
    public String getComplainDate() {
        return complainDate;
    }

    /**
     * @param complainDate the complainDate to set
     */
    public void setComplainDate(String complainDate) {
        this.complainDate = complainDate;
    }

    /**
     * @return the purchaseDate
     */
    public String getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * @param purchaseDate the purchaseDate to set
     */
    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    /**
     * @return the purchaseShop
     */
    public String getPurchaseShop() {
        return purchaseShop;
    }

    /**
     * @param purchaseShop the purchaseShop to set
     */
    public void setPurchaseShop(String purchaseShop) {
        this.purchaseShop = purchaseShop;
    }

    /**
     * @return the serviceCenter
     */
    public String getServiceCenter() {
        return serviceCenter;
    }

    /**
     * @param serviceCenter the serviceCenter to set
     */
    public void setServiceCenter(String serviceCenter) {
        this.serviceCenter = serviceCenter;
    }

    /**
     * @return the woOpenDate
     */
    public String getWoOpenDate() {
        return woOpenDate;
    }

    /**
     * @param woOpenDate the woOpenDate to set
     */
    public void setWoOpenDate(String woOpenDate) {
        this.woOpenDate = woOpenDate;
    }

    /**
     * @return the deleveryDate
     */
    public String getDeleveryDate() {
        return deleveryDate;
    }

    /**
     * @param deleveryDate the deleveryDate to set
     */
    public void setDeleveryDate(String deleveryDate) {
        this.deleveryDate = deleveryDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the telephoneNo
     */
    public String getTelephoneNo() {
        return telephoneNo;
    }

    /**
     * @param telephoneNo the telephoneNo to set
     */
    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the customerNic
     */
    public String getCustomerNic() {
        return customerNic;
    }

    /**
     * @param customerNic the customerNic to set
     */
    public void setCustomerNic(String customerNic) {
        this.customerNic = customerNic;
    }

    /**
     * @return the warrenty
     */
    public String getWarrenty() {
        return warrenty;
    }

    /**
     * @param warrenty the warrenty to set
     */
    public void setWarrenty(String warrenty) {
        this.warrenty = warrenty;
    }

    /**
     * @return the rccRefe
     */
    public String getRccRefe() {
        return rccRefe;
    }

    /**
     * @param rccRefe the rccRefe to set
     */
    public void setRccRefe(String rccRefe) {
        this.rccRefe = rccRefe;
    }

    /**
     * @return the woReply
     */
    public String getWoReply() {
        return woReply;
    }

    /**
     * @param woReply the woReply to set
     */
    public void setWoReply(String woReply) {
        this.woReply = woReply;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the replyStatus
     */
    public String getReplyStatus() {
        return replyStatus;
    }

    /**
     * @param replyStatus the replyStatus to set
     */
    public void setReplyStatus(String replyStatus) {
        this.replyStatus = replyStatus;
    }
    
}
