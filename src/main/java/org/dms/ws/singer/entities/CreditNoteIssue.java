/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class CreditNoteIssue {
    
    private int creditNoteIssueNo;
    private String debitNoteNo;
    private int distributerId;
    private String issueDate;
    private int status;
    private String user; 
    
    private ArrayList<CreditNoteIssueDetail> cerditNoteList;

    /**
     * @return the creditNoteIssueNo
     */
    public int getCreditNoteIssueNo() {
        return creditNoteIssueNo;
    }

    /**
     * @param creditNoteIssueNo the creditNoteIssueNo to set
     */
    public void setCreditNoteIssueNo(int creditNoteIssueNo) {
        this.creditNoteIssueNo = creditNoteIssueNo;
    }

    /**
     * @return the debitNoteNo
     */
    public String getDebitNoteNo() {
        return debitNoteNo;
    }

    /**
     * @param debitNoteNo the debitNoteNo to set
     */
    public void setDebitNoteNo(String debitNoteNo) {
        this.debitNoteNo = debitNoteNo;
    }

    /**
     * @return the distributerId
     */
    public int getDistributerId() {
        return distributerId;
    }

    /**
     * @param distributerId the distributerId to set
     */
    public void setDistributerId(int distributerId) {
        this.distributerId = distributerId;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the cerditNoteList
     */
    public ArrayList<CreditNoteIssueDetail> getCerditNoteList() {
        return cerditNoteList;
    }

    /**
     * @param cerditNoteList the cerditNoteList to set
     */
    public void setCerditNoteList(ArrayList<CreditNoteIssueDetail> cerditNoteList) {
        this.cerditNoteList = cerditNoteList;
    }
    
    
}
