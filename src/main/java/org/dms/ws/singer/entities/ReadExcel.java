/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author Dasun Chathuranga
 */
public class ReadExcel {
    private String ReportID;
    private String ReportName;
    private String ReportQuery;
    private int Status;
    private String cond1;
    private String cond2;
    private String cond3;
    private String cond4;
    private String cond5;
    private String cond6;
    private String cond1a;
    private String cond2a;
    private String cond3a;
    private String cond4a;
    private String cond5a;
    private String cond6a;
    
    

    public String getCond1() {
        return cond1;
    }

    public void setCond1(String cond1) {
        this.cond1 = cond1;
    }

    public String getCond2() {
        return cond2;
    }

    public void setCond2(String cond2) {
        this.cond2 = cond2;
    }

    public String getCond3() {
        return cond3;
    }

    public void setCond3(String cond3) {
        this.cond3 = cond3;
    }

    public String getCond4() {
        return cond4;
    }

    public void setCond4(String cond4) {
        this.cond4 = cond4;
    }

    public String getCond5() {
        return cond5;
    }

    public void setCond5(String cond5) {
        this.cond5 = cond5;
    }

    public String getCond6() {
        return cond6;
    }

    public void setCond6(String cond6) {
        this.cond6 = cond6;
    }
    
    public String getReportID() {
        return ReportID;
    }

    public void setReportID(String ReportID) {
        this.ReportID = ReportID;
    }

    public String getReportName() {
        return ReportName;
    }

    public void setReportName(String ReportName) {
        this.ReportName = ReportName;
    }

    public String getReportQuery() {
        return ReportQuery;
    }

    public void setReportQuery(String ReportQuery) {
        this.ReportQuery = ReportQuery;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public String getCond1a() {
        return cond1a;
    }

    public void setCond1a(String cond1a) {
        this.cond1a = cond1a;
    }

    public String getCond2a() {
        return cond2a;
    }

    public void setCond2a(String cond2a) {
        this.cond2a = cond2a;
    }

    public String getCond3a() {
        return cond3a;
    }

    public void setCond3a(String cond3a) {
        this.cond3a = cond3a;
    }

    public String getCond4a() {
        return cond4a;
    }

    public void setCond4a(String cond4a) {
        this.cond4a = cond4a;
    }

    public String getCond5a() {
        return cond5a;
    }

    public void setCond5a(String cond5a) {
        this.cond5a = cond5a;
    }

    public String getCond6a() {
        return cond6a;
    }

    public void setCond6a(String cond6a) {
        this.cond6a = cond6a;
    }
    
    
}
