/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderInquire {
    
    private String workOrderNo;
    private String deleveryDate;
    private String product;
    private String modle;
    private String openDate;
    private String woStatus;
    private String estimateNo;
    private String estimateStatus;
    private String estimateCost;
    private String imeiNo;
    private String customerNic;
    private String invoiceNo;   
    private String actionTaken;
    private String remark;

    
    // -> Changes - Dilip
    
    
     public String getRemarks() {
        return remark;
    }

    public void setRemarks(String remarks) {
        this.remark = remarks;
    }
    
    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }
    
    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
    // <- 
    
    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the deleveryDate
     */
    public String getDeleveryDate() {
        return deleveryDate;
    }

    /**
     * @param deleveryDate the deleveryDate to set
     */
    public void setDeleveryDate(String deleveryDate) {
        this.deleveryDate = deleveryDate;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the modle
     */
    public String getModle() {
        return modle;
    }

    /**
     * @param modle the modle to set
     */
    public void setModle(String modle) {
        this.modle = modle;
    }

    /**
     * @return the openDate
     */
    public String getOpenDate() {
        return openDate;
    }

    /**
     * @param openDate the openDate to set
     */
    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    /**
     * @return the woStatus
     */
    public String getWoStatus() {
        return woStatus;
    }

    /**
     * @param woStatus the woStatus to set
     */
    public void setWoStatus(String woStatus) {
        this.woStatus = woStatus;
    }

    /**
     * @return the estimateNo
     */
    public String getEstimateNo() {
        return estimateNo;
    }

    /**
     * @param estimateNo the estimateNo to set
     */
    public void setEstimateNo(String estimateNo) {
        this.estimateNo = estimateNo;
    }

    /**
     * @return the estimateStatus
     */
    public String getEstimateStatus() {
        return estimateStatus;
    }

    /**
     * @param estimateStatus the estimateStatus to set
     */
    public void setEstimateStatus(String estimateStatus) {
        this.estimateStatus = estimateStatus;
    }

    /**
     * @return the estimateCost
     */
    public String getEstimateCost() {
        return estimateCost;
    }

    /**
     * @param estimateCost the estimateCost to set
     */
    public void setEstimateCost(String estimateCost) {
        this.estimateCost = estimateCost;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the customerNic
     */
    public String getCustomerNic() {
        return customerNic;
    }

    /**
     * @param customerNic the customerNic to set
     */
    public void setCustomerNic(String customerNic) {
        this.customerNic = customerNic;
    }
    
}
