/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author Dasun Chathuranga
 */
public class ErpCodes {
    private int bisId;
    private String cusCode;
    private int status;

    public int getBisId() {
        return bisId;
    }

    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
