/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class RepairCategory {
    
    private int reCateId;
    private String reCateDesc;
    private int reCateStatus;
    private String user;

    /**
     * @return the reCateId
     */
    public int getReCateId() {
        return reCateId;
    }

    /**
     * @param reCateId the reCateId to set
     */
    public void setReCateId(int reCateId) {
        this.reCateId = reCateId;
    }

    /**
     * @return the reCateDesc
     */
    public String getReCateDesc() {
        return reCateDesc;
    }

    /**
     * @param reCateDesc the reCateDesc to set
     */
    public void setReCateDesc(String reCateDesc) {
        this.reCateDesc = reCateDesc;
    }

    /**
     * @return the reCateStatus
     */
    public int getReCateStatus() {
        return reCateStatus;
    }

    /**
     * @param reCateStatus the reCateStatus to set
     */
    public void setReCateStatus(int reCateStatus) {
        this.reCateStatus = reCateStatus;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

   
    
}
