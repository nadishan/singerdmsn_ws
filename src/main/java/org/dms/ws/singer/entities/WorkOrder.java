/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.dms.ws.singer.utils.AppParams;

/**
 *
 * @author SDU
 */
public class WorkOrder {
    
    private String paymentTypeDesc;
    private String workOrderNo;
    private String customerName;
    private String customerAddress;
    private String workTelephoneNo;
    private String email;
    private String customerNic;
    private String imeiNo;
    private String product;
    private String brand;
    private int modleNo;
    private String assessoriesRetained;
    private String defectNo;
    private String rccReference;
    private String dateOfSale;
    private String proofOfPurches;
    private int bisId;
    private int schemeId;
    private int workOrderStatus;
    private int warrentyVrifType;
    private int nonWarrentyVrifType;
    private String tecnician;
    private String actionTaken;
    private int levelId;
    private String remarks;
    private int repairStatus;
    private int deleveryType;
    private int transferLocation;
    private String courierNo;
    private String deleveryDate;
    private String gatePass;
    private int status;
    private String statusMsg;
    private String createDate;
    private String delayDate;
    private String repairDate;
    private String defectDesc;
    private int minorDefectNo;
    private String minorDefectDesc;
    private String repairLevelDesc;
    private String saleDate;
    private int warrantyStatus;
    private String warrantyStatusMsg;
    private int paymentStatus;
    private String mjrDefectDesc;
    private String minDefecDesc;
    private String modleDesc;
    private String levelName;
    private String binName;
    private String estimateRefNo;
    private int estimateStatus;
    private String estimateStatusMsg;
    private int repairPayment;
    private String repairPaymentStatusMsg;
    private String nonWarrantyRemarks;
    private String customerComplain;
    private int serviceBisId;
    private String serviceBisDesc;
    private String registerDate;
    private String repairStatusMsg;
    private int expireStatus;
    private int paymentType;
           // private String workOrderDate;

           // private ArrayList<WorkOrderMaintainDetail> woDetials;
    private ArrayList<WorkOrderMaintain> woDetials;
    private ArrayList<WorkOrderMaintainDetail> woMaintainDetials;
    private ArrayList<WorkOrderDefect> woDefect;

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerAddress
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * @param customerAddress the customerAddress to set
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * @return the workTelephoneNo
     */
    public String getWorkTelephoneNo() {
        return workTelephoneNo;
    }

    /**
     * @param workTelephoneNo the workTelephoneNo to set
     */
    public void setWorkTelephoneNo(String workTelephoneNo) {
        this.workTelephoneNo = workTelephoneNo;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the customerNic
     */
    public String getCustomerNic() {
        return customerNic;
    }

    /**
     * @param customerNic the customerNic to set
     */
    public void setCustomerNic(String customerNic) {
        this.customerNic = customerNic;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the assessoriesRetained
     */
    public String getAssessoriesRetained() {
        return assessoriesRetained;
    }

    /**
     * @param assessoriesRetained the assessoriesRetained to set
     */
    public void setAssessoriesRetained(String assessoriesRetained) {
        this.assessoriesRetained = assessoriesRetained;
    }

    /**
     * @return the rccReference
     */
    public String getRccReference() {
        return rccReference;
    }

    /**
     * @param rccReference the rccReference to set
     */
    public void setRccReference(String rccReference) {
        this.rccReference = rccReference;
    }

    /**
     * @return the dateOfSale
     */
    public String getDateOfSale() {
        return dateOfSale;
    }

    /**
     * @param dateOfSale the dateOfSale to set
     */
    public void setDateOfSale(String dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    /**
     * @return the proofOfPurches
     */
    public String getProofOfPurches() {
        return proofOfPurches;
    }

    /**
     * @param proofOfPurches the proofOfPurches to set
     */
    public void setProofOfPurches(String proofOfPurches) {
        this.proofOfPurches = proofOfPurches;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the schemeId
     */
    public int getSchemeId() {
        return schemeId;
    }

    /**
     * @param schemeId the schemeId to set
     */
    public void setSchemeId(int schemeId) {
        this.schemeId = schemeId;
    }

    /**
     * @return the workOrderStatus
     */
    public int getWorkOrderStatus() {
        return workOrderStatus;
    }

    /**
     * @param workOrderStatus the workOrderStatus to set
     */
    public void setWorkOrderStatus(int workOrderStatus) {
        this.workOrderStatus = workOrderStatus;
    }

    /**
     * @return the warrentyVrifType
     */
    public int getWarrentyVrifType() {
        return warrentyVrifType;
    }

    /**
     * @param warrentyVrifType the warrentyVrifType to set
     */
    public void setWarrentyVrifType(int warrentyVrifType) {
        this.warrentyVrifType = warrentyVrifType;
    }

    /**
     * @return the nonWarrentyVrifType
     */
    public int getNonWarrentyVrifType() {
        return nonWarrentyVrifType;
    }

    /**
     * @param nonWarrentyVrifType the nonWarrentyVrifType to set
     */
    public void setNonWarrentyVrifType(int nonWarrentyVrifType) {
        this.nonWarrentyVrifType = nonWarrentyVrifType;
    }

    /**
     * @return the tecnician
     */
    public String getTecnician() {
        return tecnician;
    }

    /**
     * @param tecnician the tecnician to set
     */
    public void setTecnician(String tecnician) {
        this.tecnician = tecnician;
    }

    /**
     * @return the actionTaken
     */
    public String getActionTaken() {
        return actionTaken;
    }

    /**
     * @param actionTaken the actionTaken to set
     */
    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    /**
     * @return the levelId
     */
    public int getLevelId() {
        return levelId;
    }

    /**
     * @param levelId the levelId to set
     */
    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the repairStatus
     */
    public int getRepairStatus() {
        return repairStatus;
    }

    /**
     * @param repairStatus the repairStatus to set
     */
    public void setRepairStatus(int repairStatus) {
        this.repairStatus = repairStatus;
    }

    /**
     * @return the deleveryType
     */
    public int getDeleveryType() {
        return deleveryType;
    }

    /**
     * @param deleveryType the deleveryType to set
     */
    public void setDeleveryType(int deleveryType) {
        this.deleveryType = deleveryType;
    }

    /**
     * @return the transferLocation
     */
    public int getTransferLocation() {
        return transferLocation;
    }

    /**
     * @param transferLocation the transferLocation to set
     */
    public void setTransferLocation(int transferLocation) {
        this.transferLocation = transferLocation;
    }

    /**
     * @return the courierNo
     */
    public String getCourierNo() {
        return courierNo;
    }

    /**
     * @param courierNo the courierNo to set
     */
    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    /**
     * @return the deleveryDate
     */
    public String getDeleveryDate() {
        return deleveryDate;
    }

    /**
     * @param deleveryDate the deleveryDate to set
     */
    public void setDeleveryDate(String deleveryDate) {
        this.deleveryDate = deleveryDate;
    }

    /**
     * @return the gatePass
     */
    public String getGatePass() {
        return gatePass;
    }

    /**
     * @param gatePass the gatePass to set
     */
    public void setGatePass(String gatePass) {
        this.gatePass = gatePass;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the delayDate
     */
    public String getDelayDate() {
        return delayDate;
    }

    /**
     * @param delayDate the delayDate to set
     */
    public void setDelayDate(String delayDate) {
        this.delayDate = delayDate;
    }

    /**
     * @return the repairDate
     */
    public String getRepairDate() {
        return repairDate;
    }

    /**
     * @param repairDate the repairDate to set
     */
    public void setRepairDate(String repairDate) {
        this.repairDate = repairDate;
    }

    /**
     * @return the defectDesc
     */
    public String getDefectDesc() {
        return defectDesc;
    }

    /**
     * @param defectDesc the defectDesc to set
     */
    public void setDefectDesc(String defectDesc) {
        this.defectDesc = defectDesc;
    }

    /**
     * @return the minorDefectNo
     */
    public int getMinorDefectNo() {
        return minorDefectNo;
    }

    /**
     * @param minorDefectNo the minorDefectNo to set
     */
    public void setMinorDefectNo(int minorDefectNo) {
        this.minorDefectNo = minorDefectNo;
    }

    /**
     * @return the minorDefectDesc
     */
    public String getMinorDefectDesc() {
        return minorDefectDesc;
    }

    /**
     * @param minorDefectDesc the minorDefectDesc to set
     */
    public void setMinorDefectDesc(String minorDefectDesc) {
        this.minorDefectDesc = minorDefectDesc;
    }

    /**
     * @return the repairLevelDesc
     */
    public String getRepairLevelDesc() {
        return repairLevelDesc;
    }

    /**
     * @param repairLevelDesc the repairLevelDesc to set
     */
    public void setRepairLevelDesc(String repairLevelDesc) {
        this.repairLevelDesc = repairLevelDesc;
    }

    /**
     * @return the saleDate
     */
    public String getSaleDate() {
        return saleDate;
    }

    /**
     * @param saleDate the saleDate to set
     */
    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    /**
     * @return the warrantyStatus
     */
    public int getWarrantyStatus() {
        return warrantyStatus;
    }

    /**
     * @param warrantyStatus the warrantyStatus to set
     */
    public void setWarrantyStatus(int warrantyStatus) {
        this.warrantyStatus = warrantyStatus;
    }

    /**
     * @return the warrantyStatusMsg
     */
    public String getWarrantyStatusMsg() {
        return warrantyStatusMsg;
    }

    /**
     * @param warrantyStatusMsg the warrantyStatusMsg to set
     */
    public void setWarrantyStatusMsg(String warrantyStatusMsg) {
        this.warrantyStatusMsg = warrantyStatusMsg;
    }

    /**
     * @return the paymentStatus
     */
    public int getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * @param paymentStatus the paymentStatus to set
     */
    public void setPaymentStatus(int paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     * @return the mjrDefectDesc
     */
    public String getMjrDefectDesc() {
        return mjrDefectDesc;
    }

    /**
     * @param mjrDefectDesc the mjrDefectDesc to set
     */
    public void setMjrDefectDesc(String mjrDefectDesc) {
        this.mjrDefectDesc = mjrDefectDesc;
    }

    /**
     * @return the minDefecDesc
     */
    public String getMinDefecDesc() {
        return minDefecDesc;
    }

    /**
     * @param minDefecDesc the minDefecDesc to set
     */
    public void setMinDefecDesc(String minDefecDesc) {
        this.minDefecDesc = minDefecDesc;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }

    /**
     * @return the levelName
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * @param levelName the levelName to set
     */
    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    /**
     * @return the binName
     */
    public String getBinName() {
        return binName;
    }

    /**
     * @param binName the binName to set
     */
    public void setBinName(String binName) {
        this.binName = binName;
    }

    /**
     * @return the estimateRefNo
     */
    public String getEstimateRefNo() {
        return estimateRefNo;
    }

    /**
     * @param estimateRefNo the estimateRefNo to set
     */
    public void setEstimateRefNo(String estimateRefNo) {
        this.estimateRefNo = estimateRefNo;
    }

    /**
     * @return the estimateStatus
     */
    public int getEstimateStatus() {
        return estimateStatus;
    }

    /**
     * @param estimateStatus the estimateStatus to set
     */
    public void setEstimateStatus(int estimateStatus) {
        this.estimateStatus = estimateStatus;
    }

    /**
     * @return the estimateStatusMsg
     */
    public String getEstimateStatusMsg() {
        return estimateStatusMsg;
    }

    /**
     * @param estimateStatusMsg the estimateStatusMsg to set
     */
    public void setEstimateStatusMsg(String estimateStatusMsg) {
        this.estimateStatusMsg = estimateStatusMsg;
    }

    /**
     * @return the defectNo
     */
    public String getDefectNo() {
        return defectNo;
    }

    /**
     * @param defectNo the defectNo to set
     */
    public void setDefectNo(String defectNo) {
        this.defectNo = defectNo;
    }

    /**
     * @return the woDetials
     */
    public ArrayList<WorkOrderMaintain> getWoDetials() {
        return woDetials;
    }

    /**
     * @param woDetials the woDetials to set
     */
    public void setWoDetials(ArrayList<WorkOrderMaintain> woDetials) {
        this.woDetials = woDetials;
    }

    /**
     * @return the repairPaymentStatusMsg
     */
    public String getRepairPaymentStatusMsg() {
        return repairPaymentStatusMsg;
    }

    /**
     * @param repairPaymentStatusMsg the repairPaymentStatusMsg to set
     */
    public void setRepairPaymentStatusMsg(String repairPaymentStatusMsg) {
        this.repairPaymentStatusMsg = repairPaymentStatusMsg;
    }

    /**
     * @return the repairPayment
     */
    public int getRepairPayment() {
        return repairPayment;
    }

    /**
     * @param repairPayment the repairPayment to set
     */
    public void setRepairPayment(int repairPayment) {
        this.repairPayment = repairPayment;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the nonWarrantyRemarks
     */
    public String getNonWarrantyRemarks() {
        return nonWarrantyRemarks;
    }

    /**
     * @param nonWarrantyRemarks the nonWarrantyRemarks to set
     */
    public void setNonWarrantyRemarks(String nonWarrantyRemarks) {
        this.nonWarrantyRemarks = nonWarrantyRemarks;
    }

    /**
     * @return the customerComplain
     */
    public String getCustomerComplain() {
        return customerComplain;
    }

    /**
     * @param customerComplain the customerComplain to set
     */
    public void setCustomerComplain(String customerComplain) {
        this.customerComplain = customerComplain;
    }

    /**
     * @return the serviceBisId
     */
    public int getServiceBisId() {
        return serviceBisId;
    }

    /**
     * @param serviceBisId the serviceBisId to set
     */
    public void setServiceBisId(int serviceBisId) {
        this.serviceBisId = serviceBisId;
    }

    /**
     * @return the serviceBisDesc
     */
    public String getServiceBisDesc() {
        return serviceBisDesc;
    }

    /**
     * @param serviceBisDesc the serviceBisDesc to set
     */
    public void setServiceBisDesc(String serviceBisDesc) {
        this.serviceBisDesc = serviceBisDesc;
    }

    /**
     * @return the registerDate
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * @param registerDate the registerDate to set
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * @return the woMaintainDetials
     */
    public ArrayList<WorkOrderMaintainDetail> getWoMaintainDetials() {
        return woMaintainDetials;
    }

    /**
     * @param woMaintainDetials the woMaintainDetials to set
     */
    public void setWoMaintainDetials(ArrayList<WorkOrderMaintainDetail> woMaintainDetials) {
        this.woMaintainDetials = woMaintainDetials;
    }

    /**
     * @return the woDefect
     */
    public ArrayList<WorkOrderDefect> getWoDefect() {
        return woDefect;
    }

    /**
     * @param woDefect the woDefect to set
     */
    public void setWoDefect(ArrayList<WorkOrderDefect> woDefect) {
        this.woDefect = woDefect;
    }

    /**
     * @return the repairStatusMsg
     */
    public String getRepairStatusMsg() {
        return repairStatusMsg;
    }

    /**
     * @param repairStatusMsg the repairStatusMsg to set
     */
    public void setRepairStatusMsg(String repairStatusMsg) {
        this.repairStatusMsg = repairStatusMsg;
    }

    public int getExpireStatus() {
        return expireStatus;
    }

    public void setExpireStatus(int expireStatus) {
        this.expireStatus = expireStatus;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeDesc() {
        return paymentTypeDesc;
    }

    public void setPaymentTypeDesc(String paymentTypeDesc) {
        this.paymentTypeDesc = paymentTypeDesc;
    }



   
    
    

}
