/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class DeviceExchange {
    
            private String exchangeReferenceNo;
            private String workOrderNo;
            private String imeiNo;
            private int reason;
            private int typeOfExchange;
            private int bisId;
            private int status;
            private String remarks;
            private String customerNIC;
            private String customerName;
            private String warrantyRegDate;
            private String warrantyStatus;
            private String product;
            private String brand;
            private String modelDesc;
            private int wrtPeriodFlag;
            private int wrtImeiFlag;
            private int wrtDamageFlag;
            private int unAuthRepairFlag;
            private int waterDamage;
            private int reprodicbleFlag;
            private String reasonDesc;
            private String erpCode;
            private String exchangeDesc;
            private String warrantyDesc;
            private String exchangeDate;
    /**
     * @return the exchangeReferenceNo
     */
    public String getExchangeReferenceNo() {
        return exchangeReferenceNo;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    /**
     * @param exchangeReferenceNo the exchangeReferenceNo to set
     */
    public void setExchangeReferenceNo(String exchangeReferenceNo) {
        this.exchangeReferenceNo = exchangeReferenceNo;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the reason
     */
    public int getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(int reason) {
        this.reason = reason;
    }

    /**
     * @return the typeOfExchange
     */
    public int getTypeOfExchange() {
        return typeOfExchange;
    }

    /**
     * @param typeOfExchange the typeOfExchange to set
     */
    public void setTypeOfExchange(int typeOfExchange) {
        this.typeOfExchange = typeOfExchange;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the customerNIC
     */
    public String getCustomerNIC() {
        return customerNIC;
    }

    /**
     * @param customerNIC the customerNIC to set
     */
    public void setCustomerNIC(String customerNIC) {
        this.customerNIC = customerNIC;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the warrantyRegDate
     */
    public String getWarrantyRegDate() {
        return warrantyRegDate;
    }

    /**
     * @param warrantyRegDate the warrantyRegDate to set
     */
    public void setWarrantyRegDate(String warrantyRegDate) {
        this.warrantyRegDate = warrantyRegDate;
    }

    /**
     * @return the warrantyStatus
     */
    public String getWarrantyStatus() {
        return warrantyStatus;
    }

    /**
     * @param warrantyStatus the warrantyStatus to set
     */
    public void setWarrantyStatus(String warrantyStatus) {
        this.warrantyStatus = warrantyStatus;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the modelDesc
     */
    public String getModelDesc() {
        return modelDesc;
    }

    /**
     * @param modelDesc the modelDesc to set
     */
    public void setModelDesc(String modelDesc) {
        this.modelDesc = modelDesc;
    }

    /**
     * @return the wrtPeriodFlag
     */
    public int getWrtPeriodFlag() {
        return wrtPeriodFlag;
    }

    /**
     * @param wrtPeriodFlag the wrtPeriodFlag to set
     */
    public void setWrtPeriodFlag(int wrtPeriodFlag) {
        this.wrtPeriodFlag = wrtPeriodFlag;
    }

    /**
     * @return the wrtImeiFlag
     */
    public int getWrtImeiFlag() {
        return wrtImeiFlag;
    }

    /**
     * @param wrtImeiFlag the wrtImeiFlag to set
     */
    public void setWrtImeiFlag(int wrtImeiFlag) {
        this.wrtImeiFlag = wrtImeiFlag;
    }

    /**
     * @return the wrtDamageFlag
     */
    public int getWrtDamageFlag() {
        return wrtDamageFlag;
    }

    /**
     * @param wrtDamageFlag the wrtDamageFlag to set
     */
    public void setWrtDamageFlag(int wrtDamageFlag) {
        this.wrtDamageFlag = wrtDamageFlag;
    }

    /**
     * @return the unAuthRepairFlag
     */
    public int getUnAuthRepairFlag() {
        return unAuthRepairFlag;
    }

    /**
     * @param unAuthRepairFlag the unAuthRepairFlag to set
     */
    public void setUnAuthRepairFlag(int unAuthRepairFlag) {
        this.unAuthRepairFlag = unAuthRepairFlag;
    }

    /**
     * @return the waterDamage
     */
    public int getWaterDamage() {
        return waterDamage;
    }

    /**
     * @param waterDamage the waterDamage to set
     */
    public void setWaterDamage(int waterDamage) {
        this.waterDamage = waterDamage;
    }

    /**
     * @return the reprodicbleFlag
     */
    public int getReprodicbleFlag() {
        return reprodicbleFlag;
    }

    /**
     * @param reprodicbleFlag the reprodicbleFlag to set
     */
    public void setReprodicbleFlag(int reprodicbleFlag) {
        this.reprodicbleFlag = reprodicbleFlag;
    }

    /**
     * @return the reasonDesc
     */
    public String getReasonDesc() {
        return reasonDesc;
    }

    /**
     * @param reasonDesc the reasonDesc to set
     */
    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }

    /**
     * @return the exchangeDesc
     */
    public String getExchangeDesc() {
        return exchangeDesc;
    }

    /**
     * @param exchangeDesc the exchangeDesc to set
     */
    public void setExchangeDesc(String exchangeDesc) {
        this.exchangeDesc = exchangeDesc;
    }

    /**
     * @return the warrantyDesc
     */
    public String getWarrantyDesc() {
        return warrantyDesc;
    }

    /**
     * @param warrantyDesc the warrantyDesc to set
     */
    public void setWarrantyDesc(String warrantyDesc) {
        this.warrantyDesc = warrantyDesc;
    }

    public String getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(String exchangeDate) {
        this.exchangeDate = exchangeDate;
    }
            
    
}
