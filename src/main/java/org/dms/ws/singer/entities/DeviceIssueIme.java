/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class DeviceIssueIme {
    
    private int seqNo;
    private int issueNo;
    private String imeiNo;
    private int modleNo;
    private int status;
    private String user;
    private String modleDesc;
    private double salesPrice;
    private double margin;
    private String erpModel;
    
    private String distributorName;
    private String distributorAddress;
    private String distributorCode;
    private String distributorTelephone;
    private String subdealerName;
    private String subdealerAddress;
    private String subdealerCode;
    private String subdealerTelephone;
    
    private String type;
    private String brand;

    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return the issueNo
     */
    public int getIssueNo() {
        return issueNo;
    }

    /**
     * @param issueNo the issueNo to set
     */
    public void setIssueNo(int issueNo) {
        this.issueNo = issueNo;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }


    /**
     * @return the margin
     */
    public double getMargin() {
        return margin;
    }

    /**
     * @param margin the margin to set
     */
    public void setMargin(double margin) {
        this.margin = margin;
    }

    /**
     * @return the salesPrice
     */
    public double getSalesPrice() {
        return salesPrice;
    }

    /**
     * @param salesPrice the salesPrice to set
     */
    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDistributorTelephone() {
        return distributorTelephone;
    }

    public void setDistributorTelephone(String distributorTelephone) {
        this.distributorTelephone = distributorTelephone;
    }

    public String getSubdealerName() {
        return subdealerName;
    }

    public void setSubdealerName(String subdealerName) {
        this.subdealerName = subdealerName;
    }

    public String getSubdealerAddress() {
        return subdealerAddress;
    }

    public void setSubdealerAddress(String subdealerAddress) {
        this.subdealerAddress = subdealerAddress;
    }

    public String getSubdealerCode() {
        return subdealerCode;
    }

    public void setSubdealerCode(String subdealerCode) {
        this.subdealerCode = subdealerCode;
    }

    public String getSubdealerTelephone() {
        return subdealerTelephone;
    }

    public void setSubdealerTelephone(String subdealerTelephone) {
        this.subdealerTelephone = subdealerTelephone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getErpModel() {
        return erpModel;
    }

    public void setErpModel(String erpModel) {
        this.erpModel = erpModel;
    }

    

   
    
}
