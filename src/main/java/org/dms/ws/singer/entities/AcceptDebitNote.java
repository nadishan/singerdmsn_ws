/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class AcceptDebitNote {
    
    private String debitNoteNo;  
    private String orderNo; 
    private String invoiceNo; 
    private String contactNo; 
    private String partNo; 
    private int orderQty; 
    private int deleveryQty; 
    private String deleveryDate; 
    private int headStatus; 
    private String customerNo; 
    private String siteDescription; 
    private int status; 
    private String user; 

    /**
     * @return the debitNoteNo
     */
    public String getDebitNoteNo() {
        return debitNoteNo;
    }

    /**
     * @param debitNoteNo the debitNoteNo to set
     */
    public void setDebitNoteNo(String debitNoteNo) {
        this.debitNoteNo = debitNoteNo;
    }

    /**
     * @return the orderNo
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the partNo
     */
    public String getPartNo() {
        return partNo;
    }

    /**
     * @param partNo the partNo to set
     */
    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the deleveryQty
     */
    public int getDeleveryQty() {
        return deleveryQty;
    }

    /**
     * @param deleveryQty the deleveryQty to set
     */
    public void setDeleveryQty(int deleveryQty) {
        this.deleveryQty = deleveryQty;
    }

    /**
     * @return the deleveryDate
     */
    public String getDeleveryDate() {
        return deleveryDate;
    }

    /**
     * @param deleveryDate the deleveryDate to set
     */
    public void setDeleveryDate(String deleveryDate) {
        this.deleveryDate = deleveryDate;
    }

    /**
     * @return the headStatus
     */
    public int getHeadStatus() {
        return headStatus;
    }

    /**
     * @param headStatus the headStatus to set
     */
    public void setHeadStatus(int headStatus) {
        this.headStatus = headStatus;
    }

    /**
     * @return the customerNo
     */
    public String getCustomerNo() {
        return customerNo;
    }

    /**
     * @param customerNo the customerNo to set
     */
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    /**
     * @return the siteDescription
     */
    public String getSiteDescription() {
        return siteDescription;
    }

    /**
     * @param siteDescription the siteDescription to set
     */
    public void setSiteDescription(String siteDescription) {
        this.siteDescription = siteDescription;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }
    
}
