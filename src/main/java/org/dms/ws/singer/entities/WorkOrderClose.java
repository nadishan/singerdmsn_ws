/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderClose {

    private String workOrderNo;
    private int repairid;
    private String repairSatus;
    private int deleveryId;
    private String deleveryType;
    private int locationId;
    private String transferLocation;
    private String courierNo;
    private String deleverDate;
    private int status;
    private String gatePass;
    private String remarks;
    private String callSms;
    private String engineeringTest;
    private String accessories_delivered;
    private String resolveDefect;
    private String visualInspectionTest;
    private String swVersion;

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the repairSatus
     */
    public String getRepairSatus() {
        return repairSatus;
    }

    /**
     * @param repairSatus the repairSatus to set
     */
    public void setRepairSatus(String repairSatus) {
        this.repairSatus = repairSatus;
    }

    /**
     * @return the deleveryType
     */
    public String getDeleveryType() {
        return deleveryType;
    }

    /**
     * @param deleveryType the deleveryType to set
     */
    public void setDeleveryType(String deleveryType) {
        this.deleveryType = deleveryType;
    }

    /**
     * @return the transferLocation
     */
    public String getTransferLocation() {
        return transferLocation;
    }

    /**
     * @param transferLocation the transferLocation to set
     */
    public void setTransferLocation(String transferLocation) {
        this.transferLocation = transferLocation;
    }

    /**
     * @return the courierNo
     */
    public String getCourierNo() {
        return courierNo;
    }

    /**
     * @param courierNo the courierNo to set
     */
    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    /**
     * @return the deleverDate
     */
    public String getDeleverDate() {
        return deleverDate;
    }

    /**
     * @param deleverDate the deleverDate to set
     */
    public void setDeleverDate(String deleverDate) {
        this.deleverDate = deleverDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the gatePass
     */
    public String getGatePass() {
        return gatePass;
    }

    /**
     * @param gatePass the gatePass to set
     */
    public void setGatePass(String gatePass) {
        this.gatePass = gatePass;
    }

    /**
     * @return the locationId
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the deleveryId
     */
    public int getDeleveryId() {
        return deleveryId;
    }

    /**
     * @param deleveryId the deleveryId to set
     */
    public void setDeleveryId(int deleveryId) {
        this.deleveryId = deleveryId;
    }

    /**
     * @return the repairid
     */
    public int getRepairid() {
        return repairid;
    }

    /**
     * @param repairid the repairid to set
     */
    public void setRepairid(int repairid) {
        this.repairid = repairid;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCallSms() {
        return callSms;
    }

    public void setCallSms(String callSms) {
        this.callSms = callSms;
    }

    public String getEngineeringTest() {
        return engineeringTest;
    }

    public void setEngineeringTest(String engineeringTest) {
        this.engineeringTest = engineeringTest;
    }

    public String getAccessories_delivered() {
        return accessories_delivered;
    }

    public void setAccessories_delivered(String accessories_delivered) {
        this.accessories_delivered = accessories_delivered;
    }

    public String getResolveDefect() {
        return resolveDefect;
    }

    public void setResolveDefect(String resolveDefect) {
        this.resolveDefect = resolveDefect;
    }

    public String getVisualInspectionTest() {
        return visualInspectionTest;
    }

    public void setVisualInspectionTest(String visualInspectionTest) {
        this.visualInspectionTest = visualInspectionTest;
    }

    public String getSwVersion() {
        return swVersion;
    }

    public void setSwVersion(String swVersion) {
        this.swVersion = swVersion;
    }

}
