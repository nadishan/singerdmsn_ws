/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author Dasun Chathuranga
 */
public class DSRReportEntity {
    
    private String dsrName;
    private String distributorName;
    private String issueNo;
    private String modelNo;
    private String modelDescription;
    private String modelCnt;
    private String status;
    private String price;
    private String totValue;

    public String getIssueNo() {
        return issueNo;
    }

    public void setIssueNo(String issueNo) {
        this.issueNo = issueNo;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getModelCnt() {
        return modelCnt;
    }

    public void setModelCnt(String modelCnt) {
        this.modelCnt = modelCnt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotValue() {
        return totValue;
    }

    public void setTotValue(String totValue) {
        this.totValue = totValue;
    }

    public String getModelDescription() {
        return modelDescription;
    }

    public void setModelDescription(String modelDescription) {
        this.modelDescription = modelDescription;
    }

    public String getDsrName() {
        return dsrName;
    }

    public void setDsrName(String dsrName) {
        this.dsrName = dsrName;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }
    
    
    
}
