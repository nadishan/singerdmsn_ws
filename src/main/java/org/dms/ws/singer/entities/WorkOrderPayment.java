/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class WorkOrderPayment {

    private int paymentId;
    private double amount;
    private double nbtValue;
    private double vatValue;
    private String essdAccountNo;
    private String paymentDate;
    private int status;

    private String poNo;
    private String poDate;
    private String chequeNo;
    private String chequeDate;
    
    private int nbtApplicable;
    private int vatApplicable;
    private double deductionAmount;
    

    private ArrayList<WorkOrderPaymentDetail> woPaymentDetials;
    //private ArrayList<WorkOrderPaymentDetail> WorkOrderPaymentDetail;

    private int payBisId;

    /**
     * @return the paymentId
     */
    public int getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getNbtValue() {
        return nbtValue;
    }

    public void setNbtValue(double nbtValue) {
        this.nbtValue = nbtValue;
    }

    public double getVatValue() {
        return vatValue;
    }

    public void setVatValue(double vatValue) {
        this.vatValue = vatValue;
    }

    /**
     * @return the tax
     */
    /**
     * @return the essdAccountNo
     */
    public String getEssdAccountNo() {
        return essdAccountNo;
    }

    /**
     * @param essdAccountNo the essdAccountNo to set
     */
    public void setEssdAccountNo(String essdAccountNo) {
        this.essdAccountNo = essdAccountNo;
    }

    /**
     * @return the paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<WorkOrderPaymentDetail> getWoPaymentDetials() {
        return woPaymentDetials;
    }

    public void setWoPaymentDetials(ArrayList<WorkOrderPaymentDetail> woPaymentDetials) {
        this.woPaymentDetials = woPaymentDetials;
    }
    
    /**
     * @return the woPaymentDetials
     */
    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getPoDate() {
        return poDate;
    }

    public void setPoDate(String poDate) {
        this.poDate = poDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public int getPayBisId() {
        return payBisId;
    }

    public void setPayBisId(int payBisId) {
        this.payBisId = payBisId;
    }

    public int getNbtApplicable() {
        return nbtApplicable;
    }

    public void setNbtApplicable(int nbtApplicable) {
        this.nbtApplicable = nbtApplicable;
    }

    public int getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(int vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public double getDeductionAmount() {
        return deductionAmount;
    }

    public void setDeductionAmount(double deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    
}
