/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class EventBisType {
    
    private int eventPartId;
    private int eventId;
    private int status;
    private int bisStruTypeId;
    private String bisStruTypeName;
    private int bisStruTypeCount;
    
    private ArrayList<EventBussinessList> eventBussinessList;

    /**
     * @return the bisStruTypeId
     */
    public int getBisStruTypeId() {
        return bisStruTypeId;
    }

    /**
     * @param bisStruTypeId the bisStruTypeId to set
     */
    public void setBisStruTypeId(int bisStruTypeId) {
        this.bisStruTypeId = bisStruTypeId;
    }

    /**
     * @return the bisStruTypeName
     */
    public String getBisStruTypeName() {
        return bisStruTypeName;
    }

    /**
     * @param bisStruTypeName the bisStruTypeName to set
     */
    public void setBisStruTypeName(String bisStruTypeName) {
        this.bisStruTypeName = bisStruTypeName;
    }

    /**
     * @return the bisStruTypeCount
     */
    public int getBisStruTypeCount() {
        return bisStruTypeCount;
    }

    /**
     * @param bisStruTypeCount the bisStruTypeCount to set
     */
    public void setBisStruTypeCount(int bisStruTypeCount) {
        this.bisStruTypeCount = bisStruTypeCount;
    }

    /**
     * @return the eventPartId
     */
    public int getEventPartId() {
        return eventPartId;
    }

    /**
     * @param eventPartId the eventPartId to set
     */
    public void setEventPartId(int eventPartId) {
        this.eventPartId = eventPartId;
    }

    /**
     * @return the eventId
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the eventBussinessList
     */
    public ArrayList<EventBussinessList> getEventBussinessList() {
        return eventBussinessList;
    }

    /**
     * @param eventBussinessList the eventBussinessList to set
     */
    public void setEventBussinessList(ArrayList<EventBussinessList> eventBussinessList) {
        this.eventBussinessList = eventBussinessList;
    }
    
}
