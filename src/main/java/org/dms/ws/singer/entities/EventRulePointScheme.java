/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class EventRulePointScheme {

    private int pointSchemaId;
    private int schmeruleId;
    private int schmestatus;
    private double scheamfixedPoint;
    private int schmefloatFlag;
    private String schmefloagFlagString;
    private double schmefloatPoint;
    private int schemeSaleType;
    private int schmeNoOfUnit;
    private int schmeAddnSalesType;
    private double schmeAddnSalesPoint;
    private double schmeAssignPoint;

    /**
     * @return the pointSchemaId
     */
    public int getPointSchemaId() {
        return pointSchemaId;
    }

    /**
     * @param pointSchemaId the pointSchemaId to set
     */
    public void setPointSchemaId(int pointSchemaId) {
        this.pointSchemaId = pointSchemaId;
    }

    /**
     * @return the schmeruleId
     */
    public int getSchmeruleId() {
        return schmeruleId;
    }

    /**
     * @param schmeruleId the schmeruleId to set
     */
    public void setSchmeruleId(int schmeruleId) {
        this.schmeruleId = schmeruleId;
    }

    /**
     * @return the schmestatus
     */
    public int getSchmestatus() {
        return schmestatus;
    }

    /**
     * @param schmestatus the schmestatus to set
     */
    public void setSchmestatus(int schmestatus) {
        this.schmestatus = schmestatus;
    }

    /**
     * @return the scheamfixedPoint
     */
    public double getScheamfixedPoint() {
        return scheamfixedPoint;
    }

    /**
     * @param scheamfixedPoint the scheamfixedPoint to set
     */
    public void setScheamfixedPoint(double scheamfixedPoint) {
        this.scheamfixedPoint = scheamfixedPoint;
    }

    /**
     * @return the schmefloatFlag
     */
    public int getSchmefloatFlag() {
        return schmefloatFlag;
    }

    /**
     * @param schmefloatFlag the schmefloatFlag to set
     */
    public void setSchmefloatFlag(int schmefloatFlag) {
        this.schmefloatFlag = schmefloatFlag;
    }

    /**
     * @return the schmefloagFlagString
     */
    public String getSchmefloagFlagString() {
        return schmefloagFlagString;
    }

    /**
     * @param schmefloagFlagString the schmefloagFlagString to set
     */
    public void setSchmefloagFlagString(String schmefloagFlagString) {
        this.schmefloagFlagString = schmefloagFlagString;
    }

    /**
     * @return the schmefloatPoint
     */
    public double getSchmefloatPoint() {
        return schmefloatPoint;
    }

    /**
     * @param schmefloatPoint the schmefloatPoint to set
     */
    public void setSchmefloatPoint(double schmefloatPoint) {
        this.schmefloatPoint = schmefloatPoint;
    }

    /**
     * @return the schemeSaleType
     */
    public int getSchemeSaleType() {
        return schemeSaleType;
    }

    /**
     * @param schemeSaleType the schemeSaleType to set
     */
    public void setSchemeSaleType(int schemeSaleType) {
        this.schemeSaleType = schemeSaleType;
    }

    /**
     * @return the schmeNoOfUnit
     */
    public int getSchmeNoOfUnit() {
        return schmeNoOfUnit;
    }

    /**
     * @param schmeNoOfUnit the schmeNoOfUnit to set
     */
    public void setSchmeNoOfUnit(int schmeNoOfUnit) {
        this.schmeNoOfUnit = schmeNoOfUnit;
    }

    /**
     * @return the schmeAddnSalesType
     */
    public int getSchmeAddnSalesType() {
        return schmeAddnSalesType;
    }

    /**
     * @param schmeAddnSalesType the schmeAddnSalesType to set
     */
    public void setSchmeAddnSalesType(int schmeAddnSalesType) {
        this.schmeAddnSalesType = schmeAddnSalesType;
    }

    /**
     * @return the schmeAddnSalesPoint
     */
    public double getSchmeAddnSalesPoint() {
        return schmeAddnSalesPoint;
    }

    /**
     * @param schmeAddnSalesPoint the schmeAddnSalesPoint to set
     */
    public void setSchmeAddnSalesPoint(double schmeAddnSalesPoint) {
        this.schmeAddnSalesPoint = schmeAddnSalesPoint;
    }

    /**
     * @return the schmeAssignPoint
     */
    public double getSchmeAssignPoint() {
        return schmeAssignPoint;
    }

    /**
     * @param schmeAssignPoint the schmeAssignPoint to set
     */
    public void setSchmeAssignPoint(double schmeAssignPoint) {
        this.schmeAssignPoint = schmeAssignPoint;
    }

   

}
