/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderTransfer {
    
    private int woTransNo;
    private String workOrderNo;
    private int bisId;
    private String transferDate;
    private String transferReason;
    private String courierEmail;
    private String deleverDetils;
    private int status;
    private String bisName;
    private int deleveryType;
    private String deleveryTypeDesc;
    private String statusMsg;
    private int tansferLocationId;
    private String transferLocationDesc;
    private int retunDeleveryType;
    private String returnDeleveryDesc;
    private String returnDate;
    private String returnReason;
    private String returnDeleveryDetails;
    
    

    /**
     * @return the woTransNo
     */
    public int getWoTransNo() {
        return woTransNo;
    }

    /**
     * @param woTransNo the woTransNo to set
     */
    public void setWoTransNo(int woTransNo) {
        this.woTransNo = woTransNo;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the transferDate
     */
    public String getTransferDate() {
        return transferDate;
    }

    /**
     * @param transferDate the transferDate to set
     */
    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }

    /**
     * @return the transferReason
     */
    public String getTransferReason() {
        return transferReason;
    }

    /**
     * @param transferReason the transferReason to set
     */
    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    /**
     * @return the courierEmail
     */
    public String getCourierEmail() {
        return courierEmail;
    }

    /**
     * @param courierEmail the courierEmail to set
     */
    public void setCourierEmail(String courierEmail) {
        this.courierEmail = courierEmail;
    }

    /**
     * @return the deleverDetils
     */
    public String getDeleverDetils() {
        return deleverDetils;
    }

    /**
     * @param deleverDetils the deleverDetils to set
     */
    public void setDeleverDetils(String deleverDetils) {
        this.deleverDetils = deleverDetils;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the deleveryType
     */
    public int getDeleveryType() {
        return deleveryType;
    }

    /**
     * @param deleveryType the deleveryType to set
     */
    public void setDeleveryType(int deleveryType) {
        this.deleveryType = deleveryType;
    }

    /**
     * @return the deleveryTypeDesc
     */
    public String getDeleveryTypeDesc() {
        return deleveryTypeDesc;
    }

    /**
     * @param deleveryTypeDesc the deleveryTypeDesc to set
     */
    public void setDeleveryTypeDesc(String deleveryTypeDesc) {
        this.deleveryTypeDesc = deleveryTypeDesc;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the tansferLocationId
     */
    public int getTansferLocationId() {
        return tansferLocationId;
    }

    /**
     * @param tansferLocationId the tansferLocationId to set
     */
    public void setTansferLocationId(int tansferLocationId) {
        this.tansferLocationId = tansferLocationId;
    }

    /**
     * @return the transferLocationDesc
     */
    public String getTransferLocationDesc() {
        return transferLocationDesc;
    }

    /**
     * @param transferLocationDesc the transferLocationDesc to set
     */
    public void setTransferLocationDesc(String transferLocationDesc) {
        this.transferLocationDesc = transferLocationDesc;
    }

    /**
     * @return the retunDeleveryType
     */
    public int getRetunDeleveryType() {
        return retunDeleveryType;
    }

    /**
     * @param retunDeleveryType the retunDeleveryType to set
     */
    public void setRetunDeleveryType(int retunDeleveryType) {
        this.retunDeleveryType = retunDeleveryType;
    }

    /**
     * @return the returnDeleveryDesc
     */
    public String getReturnDeleveryDesc() {
        return returnDeleveryDesc;
    }

    /**
     * @param returnDeleveryDesc the returnDeleveryDesc to set
     */
    public void setReturnDeleveryDesc(String returnDeleveryDesc) {
        this.returnDeleveryDesc = returnDeleveryDesc;
    }

    /**
     * @return the returnDate
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * @return the returnReason
     */
    public String getReturnReason() {
        return returnReason;
    }

    /**
     * @param returnReason the returnReason to set
     */
    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    /**
     * @return the returnDeleveryDetails
     */
    public String getReturnDeleveryDetails() {
        return returnDeleveryDetails;
    }

    /**
     * @param returnDeleveryDetails the returnDeleveryDetails to set
     */
    public void setReturnDeleveryDetails(String returnDeleveryDetails) {
        this.returnDeleveryDetails = returnDeleveryDetails;
    }
    
    
}
