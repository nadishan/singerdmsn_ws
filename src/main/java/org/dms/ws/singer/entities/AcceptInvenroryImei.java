/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class AcceptInvenroryImei {

    private int seqNo;
    private int issueNo;
    private String ImeiNo;
    private String modleDescription;
    private int modleNo;
    private int Status;
    private int bisId;
    private String statusMsg;
    private boolean isPrint;

    private String erpModel;
    private double salesPrice;
    private String distributorName;
    private String distributorAddress;
    private String distributorCode;
    private String distributorTelephone;
    private String subdealerName;
    private String subdealerAddress;
    private String subdealerCode;
    private String subdealerTelephone;
    
    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return the issueNo
     */
    public int getIssueNo() {
        return issueNo;
    }

    /**
     * @param issueNo the issueNo to set
     */
    public void setIssueNo(int issueNo) {
        this.issueNo = issueNo;
    }

    /**
     * @return the ImeiNo
     */
    public String getImeiNo() {
        return ImeiNo;
    }

    /**
     * @param ImeiNo the ImeiNo to set
     */
    public void setImeiNo(String ImeiNo) {
        this.ImeiNo = ImeiNo;
    }

    /**
     * @return the modleDescription
     */
    public String getModleDescription() {
        return modleDescription;
    }

    /**
     * @param modleDescription the modleDescription to set
     */
    public void setModleDescription(String modleDescription) {
        this.modleDescription = modleDescription;
    }

    /**
     * @return the Status
     */
    public int getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(int Status) {
        this.Status = Status;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public boolean isIsPrint() {
        return isPrint;
    }

    public void setIsPrint(boolean isPrint) {
        this.isPrint = isPrint;
    }

    public String getErpModel() {
        return erpModel;
    }

    public void setErpModel(String erpModel) {
        this.erpModel = erpModel;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDistributorTelephone() {
        return distributorTelephone;
    }

    public void setDistributorTelephone(String distributorTelephone) {
        this.distributorTelephone = distributorTelephone;
    }

    public String getSubdealerName() {
        return subdealerName;
    }

    public void setSubdealerName(String subdealerName) {
        this.subdealerName = subdealerName;
    }

    public String getSubdealerAddress() {
        return subdealerAddress;
    }

    public void setSubdealerAddress(String subdealerAddress) {
        this.subdealerAddress = subdealerAddress;
    }

    public String getSubdealerCode() {
        return subdealerCode;
    }

    public void setSubdealerCode(String subdealerCode) {
        this.subdealerCode = subdealerCode;
    }

    public String getSubdealerTelephone() {
        return subdealerTelephone;
    }

    public void setSubdealerTelephone(String subdealerTelephone) {
        this.subdealerTelephone = subdealerTelephone;
    }

    
}
