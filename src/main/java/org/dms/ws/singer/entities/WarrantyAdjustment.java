/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WarrantyAdjustment {
    
    private int warrantyId;
    private String iMEI;   
    private int extendedDays;
    private String user;
    private int status;

    /**
     * @return the warrantyId
     */
    public int getWarrantyId() {
        return warrantyId;
    }

    /**
     * @param warrantyId the warrantyId to set
     */
    public void setWarrantyId(int warrantyId) {
        this.warrantyId = warrantyId;
    }

    /**
     * @return the iMEI
     */
    public String getiMEI() {
        return iMEI;
    }

    /**
     * @param iMEI the iMEI to set
     */
    public void setiMEI(String iMEI) {
        this.iMEI = iMEI;
    }

    /**
     * @return the extendedDays
     */
    public int getExtendedDays() {
        return extendedDays;
    }

    /**
     * @param extendedDays the extendedDays to set
     */
    public void setExtendedDays(int extendedDays) {
        this.extendedDays = extendedDays;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

 

   
    
    
    
}
