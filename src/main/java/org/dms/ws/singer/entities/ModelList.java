/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU   
 */

public class ModelList {

    private int model_No;
    private String model_Description;
    private String part_Prd_Code;
    private String art_Prd_Code_Desc;
    private String part_Prd_Family;
    private String part_Prd_Family_Desc;
    private String commodity_Group_1;
    private String commodity_Group_2;
    private String wrn_Scheme;
    private int wrn_Scheme_id;
    private String rep_Category;
    private int rep_Category_id;
    private double selling_Price;
    private double cost_Price;
    private double dealer_Margin;
    private double distributor_Margin;
    private int status;
    private String user;
    private String Erp_part;
    private int altPartsExist; 
    private String altPartsExistDesc; 

    /**
     * @return the model_No
     */
    public int getModel_No() {
        return model_No;
    }

    public String getErp_part() {
        return Erp_part;
    }

    public void setErp_part(String Erp_part) {
        this.Erp_part = Erp_part;
    }

    /**
     * @param model_No the model_No to set
     */
    public void setModel_No(int model_No) {
        this.model_No = model_No;
    }

    /**
     * @return the model_Description
     */
    public String getModel_Description() {
        return model_Description;
    }

    /**
     * @param model_Description the model_Description to set
     */
    public void setModel_Description(String model_Description) {
        this.model_Description = model_Description;
    }

    /**
     * @return the part_Prd_Code
     */
    public String getPart_Prd_Code() {
        return part_Prd_Code;
    }

    /**
     * @param part_Prd_Code the part_Prd_Code to set
     */
    public void setPart_Prd_Code(String part_Prd_Code) {
        this.part_Prd_Code = part_Prd_Code;
    }

    /**
     * @return the art_Prd_Code_Desc
     */
    public String getArt_Prd_Code_Desc() {
        return art_Prd_Code_Desc;
    }

    /**
     * @param art_Prd_Code_Desc the art_Prd_Code_Desc to set
     */
    public void setArt_Prd_Code_Desc(String art_Prd_Code_Desc) {
        this.art_Prd_Code_Desc = art_Prd_Code_Desc;
    }

    /**
     * @return the part_Prd_Family
     */
    public String getPart_Prd_Family() {
        return part_Prd_Family;
    }

    /**
     * @param part_Prd_Family the part_Prd_Family to set
     */
    public void setPart_Prd_Family(String part_Prd_Family) {
        this.part_Prd_Family = part_Prd_Family;
    }

    /**
     * @return the part_Prd_Family_Desc
     */
    public String getPart_Prd_Family_Desc() {
        return part_Prd_Family_Desc;
    }

    /**
     * @param part_Prd_Family_Desc the part_Prd_Family_Desc to set
     */
    public void setPart_Prd_Family_Desc(String part_Prd_Family_Desc) {
        this.part_Prd_Family_Desc = part_Prd_Family_Desc;
    }

    /**
     * @return the commodity_Group_1
     */
    public String getCommodity_Group_1() {
        return commodity_Group_1;
    }

    /**
     * @param commodity_Group_1 the commodity_Group_1 to set
     */
    public void setCommodity_Group_1(String commodity_Group_1) {
        this.commodity_Group_1 = commodity_Group_1;
    }

    /**
     * @return the commodity_Group_2
     */
    public String getCommodity_Group_2() {
        return commodity_Group_2;
    }

    /**
     * @param commodity_Group_2 the commodity_Group_2 to set
     */
    public void setCommodity_Group_2(String commodity_Group_2) {
        this.commodity_Group_2 = commodity_Group_2;
    }

    /**
     * @return the wrn_Scheme
     */
    public String getWrn_Scheme() {
        return wrn_Scheme;
    }

    /**
     * @param wrn_Scheme the wrn_Scheme to set
     */
    public void setWrn_Scheme(String wrn_Scheme) {
        this.wrn_Scheme = wrn_Scheme;
    }

    /**
     * @return the wrn_Scheme_id
     */
    public int getWrn_Scheme_id() {
        return wrn_Scheme_id;
    }

    /**
     * @param wrn_Scheme_id the wrn_Scheme_id to set
     */
    public void setWrn_Scheme_id(int wrn_Scheme_id) {
        this.wrn_Scheme_id = wrn_Scheme_id;
    }

    /**
     * @return the rep_Category
     */
    public String getRep_Category() {
        return rep_Category;
    }

    /**
     * @param rep_Category the rep_Category to set
     */
    public void setRep_Category(String rep_Category) {
        this.rep_Category = rep_Category;
    }

    /**
     * @return the rep_Category_id
     */
    public int getRep_Category_id() {
        return rep_Category_id;
    }

    /**
     * @param rep_Category_id the rep_Category_id to set
     */
    public void setRep_Category_id(int rep_Category_id) {
        this.rep_Category_id = rep_Category_id;
    }

    /**
     * @return the selling_Price
     */
    public double getSelling_Price() {
        return selling_Price;
    }

    /**
     * @param selling_Price the selling_Price to set
     */
    public void setSelling_Price(double selling_Price) {
        this.selling_Price = selling_Price;
    }

    /**
     * @return the cost_Price
     */
    public double getCost_Price() {
        return cost_Price;
    }

    /**
     * @param cost_Price the cost_Price to set
     */
    public void setCost_Price(double cost_Price) {
        this.cost_Price = cost_Price;
    }

    /**
     * @return the dealer_Margin
     */
    public double getDealer_Margin() {
        return dealer_Margin;
    }

    /**
     * @param dealer_Margin the dealer_Margin to set
     */
    public void setDealer_Margin(double dealer_Margin) {
        this.dealer_Margin = dealer_Margin;
    }

    /**
     * @return the distributor_Margin
     */
    public double getDistributor_Margin() {
        return distributor_Margin;
    }

    /**
     * @param distributor_Margin the distributor_Margin to set
     */
    public void setDistributor_Margin(double distributor_Margin) {
        this.distributor_Margin = distributor_Margin;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the altPartsExist
     */
    public int getAltPartsExist() {
        return altPartsExist;
    }

    /**
     * @param altPartsExist the altPartsExist to set
     */
    public void setAltPartsExist(int altPartsExist) {
        this.altPartsExist = altPartsExist;
    }

    /**
     * @return the altPartsExistDesc
     */
    public String getAltPartsExistDesc() {
        return altPartsExistDesc;
    }

    /**
     * @param altPartsExistDesc the altPartsExistDesc to set
     */
    public void setAltPartsExistDesc(String altPartsExistDesc) {
        this.altPartsExistDesc = altPartsExistDesc;
    }

   
}
