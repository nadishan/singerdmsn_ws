/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class ShopVistCheck {
    
        
        private int shopVistId;
        private int dSR;
        private String longtitude;
        private String latitude;
        private String visitDate;
        private int subDealer;
        private int status;
        private int bisId;
        private String userId;
        private String firstName;
        private String lastName;
        private String bisName;
        private String contactNo;
        private String subDelerName;
        private String parentName;
        
        private ArrayList<ShopVisitItemList> shopVisitItemList ;

    /**
     * @return the shopVistId
     */
    public int getShopVistId() {
        return shopVistId;
    }

    /**
     * @param shopVistId the shopVistId to set
     */
    public void setShopVistId(int shopVistId) {
        this.shopVistId = shopVistId;
    }

    /**
     * @return the dSR
     */
    public int getdSR() {
        return dSR;
    }

    /**
     * @param dSR the dSR to set
     */
    public void setdSR(int dSR) {
        this.dSR = dSR;
    }

    /**
     * @return the longtitude
     */
    public String getLongtitude() {
        return longtitude;
    }

    /**
     * @param longtitude the longtitude to set
     */
    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the visitDate
     */
    public String getVisitDate() {
        return visitDate;
    }

    /**
     * @param visitDate the visitDate to set
     */
    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    /**
     * @return the subDealer
     */
    public int getSubDealer() {
        return subDealer;
    }

    /**
     * @param subDealer the subDealer to set
     */
    public void setSubDealer(int subDealer) {
        this.subDealer = subDealer;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the shopVisitItemList
     */
    public ArrayList<ShopVisitItemList> getShopVisitItemList() {
        return shopVisitItemList;
    }

    /**
     * @param shopVisitItemList the shopVisitItemList to set
     */
    public void setShopVisitItemList(ArrayList<ShopVisitItemList> shopVisitItemList) {
        this.shopVisitItemList = shopVisitItemList;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the subDelerName
     */
    public String getSubDelerName() {
        return subDelerName;
    }

    /**
     * @param subDelerName the subDelerName to set
     */
    public void setSubDelerName(String subDelerName) {
        this.subDelerName = subDelerName;
    }

    /**
     * @return the parentName
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName the parentName to set
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }


   
}
