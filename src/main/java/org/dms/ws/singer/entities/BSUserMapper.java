/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class BSUserMapper {
    
    private ArrayList<User> assignedUsers;
    private ArrayList<User> unassignedUsers;

    /**
     * @return the assignedUsers
     */
    public ArrayList<User> getAssignedUsers() {
        return assignedUsers;
    }

    /**
     * @param assignedUsers the assignedUsers to set
     */
    public void setAssignedUsers(ArrayList<User> assignedUsers) {
        this.assignedUsers = assignedUsers;
    }

    /**
     * @return the unassignedUsers
     */
    public ArrayList<User> getUnassignedUsers() {
        return unassignedUsers;
    }

    /**
     * @param unassignedUsers the unassignedUsers to set
     */
    public void setUnassignedUsers(ArrayList<User> unassignedUsers) {
        this.unassignedUsers = unassignedUsers;
    }

   
    
}
