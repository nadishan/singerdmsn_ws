/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author Shanka
 */
public class DeviceIssueOffline {

    String id;
    DeviceIssueOfflineList[] deviceIssue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeviceIssueOfflineList[] getDeviceIssue() {
        return deviceIssue;
    }

    public void setDeviceIssue(DeviceIssueOfflineList[] deviceIssue) {
        this.deviceIssue = deviceIssue;
    }

}
