/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author dmd
 */
public class WorkOrderStatusInquiry {

    private String workOrderNo;
    private String customerName;
    private String customerAddress;
    private String rccReference;
    private String customerNic;
    private String customerTelephoneNo;
    private String email;
    private String imeiNo;
    private String product;
    private String brand;
    private String dateOfSale;
    private String modleDesc;
    private String warrantyType;
    private String technician;
    private String deliveryType;
    private String transferLocation;
    private String deliveryDate;
    private String estClosingDate;
    private ArrayList<Part> partList;
    private ArrayList<WorkOrderDefect> woDefect;
    private double totalCost;
    private double repairAmount;
    private int repairStatus;
    private String repairStatusMsg;
    private int repairLevelId;
    private String repairLevelName;
    private String nonWarrentyVrifType;
    private String nonWarrantyRemarks;
    private String createDate;
    private String actionTaken;
    //new
    private String accesoriesRetained;
    private String remark;
    private String customerComplain;
    private String invoiceNo;
    private String remark2;
    private Date estClosingdate;
    private String defects;
    private String courierNo;
    private String gatePass;
    private String distShop;

    public String getCourierNo() {
        return courierNo;
    }

    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    public String getGatePass() {
        return gatePass;
    }

    public void setGatePass(String gatePass) {
        this.gatePass = gatePass;
    }
        
    public String getDefects() {
        return defects;
    }

    public void setDefects(String defects) {
        this.defects = defects;
    }
    
    private ArrayList<WorkOrderEstimate> woEstimate;

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    
    
    public String getRepairLevelName() {
        return repairLevelName;
    }

    public void setRepairLevelName(String repairLevelName) {
        this.repairLevelName = repairLevelName;
    }

    
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerNic() {
        return customerNic;
    }

    public void setCustomerNic(String customerNic) {
        this.customerNic = customerNic;
    }

    public String getCustomerTelephoneNo() {
        return customerTelephoneNo;
    }

    public void setCustomerTelephoneNo(String customerTelephoneNo) {
        this.customerTelephoneNo = customerTelephoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImeiNo() {
        return imeiNo;
    }

    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(String dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    public String getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(String warrantyType) {
        this.warrantyType = warrantyType;
    }

    public String getTechnician() {
        return technician;
    }

    public void setTechnician(String technician) {
        this.technician = technician;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getTransferLocation() {
        return transferLocation;
    }

    public void setTransferLocation(String transferLocation) {
        this.transferLocation = transferLocation;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getEstClosingDate() {
        return estClosingDate;
    }

    public void setEstClosingDate(String estClosingDate) {
        this.estClosingDate = estClosingDate;
    }

    public ArrayList<Part> getPartList() {
        return partList;
    }

    public void setPartList(ArrayList<Part> partList) {
        this.partList = partList;
    }

    public ArrayList<WorkOrderDefect> getWoDefect() {
        return woDefect;
    }

    public void setWoDefect(ArrayList<WorkOrderDefect> woDefect) {
        this.woDefect = woDefect;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public double getRepairAmount() {
        return repairAmount;
    }

    public void setRepairAmount(double repairAmount) {
        this.repairAmount = repairAmount;
    }

    public int getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(int repairStatus) {
        this.repairStatus = repairStatus;
    }

    public int getRepairLevelId() {
        return repairLevelId;
    }

    public void setRepairLevelId(int repairLevelId) {
        this.repairLevelId = repairLevelId;
    }

    public ArrayList<WorkOrderEstimate> getWoEstimate() {
        return woEstimate;
    }

    public void setWoEstimate(ArrayList<WorkOrderEstimate> woEstimate) {
        this.woEstimate = woEstimate;
    }

    public String getNonWarrentyVrifType() {
        return nonWarrentyVrifType;
    }

    public void setNonWarrentyVrifType(String nonWarrentyVrifType) {
        this.nonWarrentyVrifType = nonWarrentyVrifType;
    }

   
    public String getNonWarrantyRemarks() {
        return nonWarrantyRemarks;
    }

    public void setNonWarrantyRemarks(String nonWarrantyRemarks) {
        this.nonWarrantyRemarks = nonWarrantyRemarks;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the repairStatusMsg
     */
    public String getRepairStatusMsg() {
        return repairStatusMsg;
    }

    /**
     * @param repairStatusMsg the repairStatusMsg to set
     */
    public void setRepairStatusMsg(String repairStatusMsg) {
        this.repairStatusMsg = repairStatusMsg;
    }

    /**
     * @return the rccReference
     */
    public String getRccReference() {
        return rccReference;
    }

    /**
     * @param rccReference the rccReference to set
     */
    public void setRccReference(String rccReference) {
        this.rccReference = rccReference;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }

    /**
     * @return the accesoriesRetained
     */
    public String getAccesoriesRetained() {
        return accesoriesRetained;
    }

    /**
     * @param accesoriesRetained the accesoriesRetained to set
     */
    public void setAccesoriesRetained(String accesoriesRetained) {
        this.accesoriesRetained = accesoriesRetained;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the customerComplain
     */
    public String getCustomerComplain() {
        return customerComplain;
    }

    /**
     * @param customerComplain the customerComplain to set
     */
    public void setCustomerComplain(String customerComplain) {
        this.customerComplain = customerComplain;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * @return the remark2
     */
    public String getRemark2() {
        return remark2;
    }

    /**
     * @param remark2 the remark2 to set
     */
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    /**
     * @return the estClosingdate
     */
    public Date getEstClosingdate() {
        return estClosingdate;
    }

    /**
     * @param estClosingdate the estClosingdate to set
     */
    public void setEstClosingdate(Date estClosingdate) {
        this.estClosingdate = estClosingdate;
    }

    public String getDistShop() {
        return distShop;
    }

    public void setDistShop(String distShop) {
        this.distShop = distShop;
    }

    


    }
