/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WarrenyReplacement {
    
    private int seqNo;
    private String workOrderNo;
    private String oldImei;
    private String newImei;
    private String exchangeReferenceNo;
    private String modleDesc;
    private int bisId;
    private String replaceDate;
    private String cusName;
    private String reaminDate;
    

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the oldImei
     */
    public String getOldImei() {
        return oldImei;
    }

    /**
     * @param oldImei the oldImei to set
     */
    public void setOldImei(String oldImei) {
        this.oldImei = oldImei;
    }

    /**
     * @return the newImei
     */
    public String getNewImei() {
        return newImei;
    }

    /**
     * @param newImei the newImei to set
     */
    public void setNewImei(String newImei) {
        this.newImei = newImei;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }

    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the exchangeReferenceNo
     */
    public String getExchangeReferenceNo() {
        return exchangeReferenceNo;
    }

    /**
     * @param exchangeReferenceNo the exchangeReferenceNo to set
     */
    public void setExchangeReferenceNo(String exchangeReferenceNo) {
        this.exchangeReferenceNo = exchangeReferenceNo;
    }

    /**
     * @return the cusName
     */
    public String getCusName() {
        return cusName;
    }

    /**
     * @param cusName the cusName to set
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return the reaminDate
     */
    public String getReaminDate() {
        return reaminDate;
    }

    /**
     * @param reaminDate the reaminDate to set
     */
    public void setReaminDate(String reaminDate) {
        this.reaminDate = reaminDate;
    }

    /**
     * @return the replaceDate
     */
    public String getReplaceDate() {
        return replaceDate;
    }

    /**
     * @param replaceDate the replaceDate to set
     */
    public void setReplaceDate(String replaceDate) {
        this.replaceDate = replaceDate;
    }
    
    
}
