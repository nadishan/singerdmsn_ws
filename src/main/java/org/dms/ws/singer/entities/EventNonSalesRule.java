/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class EventNonSalesRule {

    private int nonSalesRule;
    private int eventId;
    private String description;
    private int taget;
    private int point;
    private int salesType;
    private int status;

    /**
     * @return the nonSalesRule
     */
    public int getNonSalesRule() {
        return nonSalesRule;
    }

    /**
     * @param nonSalesRule the nonSalesRule to set
     */
    public void setNonSalesRule(int nonSalesRule) {
        this.nonSalesRule = nonSalesRule;
    }

    /**
     * @return the eventIdl
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventIdl the eventIdl to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the taget
     */
    public int getTaget() {
        return taget;
    }

    /**
     * @param taget the taget to set
     */
    public void setTaget(int taget) {
        this.taget = taget;
    }

    /**
     * @return the point
     */
    public int getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(int point) {
        this.point = point;
    }

    /**
     * @return the salesType
     */
    public int getSalesType() {
        return salesType;
    }

    /**
     * @param salesType the salesType to set
     */
    public void setSalesType(int salesType) {
        this.salesType = salesType;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

}
