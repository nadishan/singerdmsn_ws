/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class DeviceReturn {

    private int returnNo;
    private int distributorId;
    private int dsrId;
    private String returnDate;
    private double price;
    private double distributorMargin;
    private double dealerMargin;
    private double discount;
    private int status;
    private String reason;
    private String distributorName;
    private String dsrName;
    private String user;
    
    private ArrayList<DeviceReturnDetail> deviceReturnList;
    

    /**
     * @return the returnNo
     */
    public int getReturnNo() {
        return returnNo;
    }

    /**
     * @param returnNo the returnNo to set
     */
    public void setReturnNo(int returnNo) {
        this.returnNo = returnNo;
    }

    /**
     * @return the distributorId
     */
    public int getDistributorId() {
        return distributorId;
    }

    /**
     * @param distributorId the distributorId to set
     */
    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    /**
     * @return the dsrId
     */
    public int getDsrId() {
        return dsrId;
    }

    /**
     * @param dsrId the dsrId to set
     */
    public void setDsrId(int dsrId) {
        this.dsrId = dsrId;
    }

    /**
     * @return the returnDate
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the distributorMargin
     */
    public double getDistributorMargin() {
        return distributorMargin;
    }

    /**
     * @param distributorMargin the distributorMargin to set
     */
    public void setDistributorMargin(double distributorMargin) {
        this.distributorMargin = distributorMargin;
    }

    /**
     * @return the dealerMargin
     */
    public double getDealerMargin() {
        return dealerMargin;
    }

    /**
     * @param dealerMargin the dealerMargin to set
     */
    public void setDealerMargin(double dealerMargin) {
        this.dealerMargin = dealerMargin;
    }

    /**
     * @return the discount
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the deviceReturnList
     */
    public ArrayList<DeviceReturnDetail> getDeviceReturnList() {
        return deviceReturnList;
    }

    /**
     * @param deviceReturnList the deviceReturnList to set
     */
    public void setDeviceReturnList(ArrayList<DeviceReturnDetail> deviceReturnList) {
        this.deviceReturnList = deviceReturnList;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the distributorName
     */
    public String getDistributorName() {
        return distributorName;
    }

    /**
     * @param distributorName the distributorName to set
     */
    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    /**
     * @return the dsrName
     */
    public String getDsrName() {
        return dsrName;
    }

    /**
     * @param dsrName the dsrName to set
     */
    public void setDsrName(String dsrName) {
        this.dsrName = dsrName;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    

}
