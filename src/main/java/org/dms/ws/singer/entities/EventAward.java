/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class EventAward {

    private int awardId;
    private int eventid;
    private String palce;
    private String award;
    private int status;

    /**
     * @return the awardId
     */
    public int getAwardId() {
        return awardId;
    }

    /**
     * @param awardId the awardId to set
     */
    public void setAwardId(int awardId) {
        this.awardId = awardId;
    }

    /**
     * @return the eventid
     */
    public int getEventid() {
        return eventid;
    }

    /**
     * @param eventid the eventid to set
     */
    public void setEventid(int eventid) {
        this.eventid = eventid;
    }

    /**
     * @return the palce
     */
    public String getPalce() {
        return palce;
    }

    /**
     * @param palce the palce to set
     */
    public void setPalce(String palce) {
        this.palce = palce;
    }

    /**
     * @return the award
     */
    public String getAward() {
        return award;
    }

    /**
     * @param award the award to set
     */
    public void setAward(String award) {
        this.award = award;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

}
