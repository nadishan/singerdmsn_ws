/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class DSRTrack {
    
    private int bisId;
    private String bisName;
    private String bisDesc;
    private String logtitute;
    private String latitite;
    private String bisTypeDesc;
    private int bisTypeId;
    private int enableFlag;
    private int vistiorBisId;
    private String visitDate;
    private int seqNo;
    private String userId;
    private String firstName;
    private String lastName;
    private String parantName;
    private String address;
    private String telePhoneNum;

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the bisDesc
     */
    public String getBisDesc() {
        return bisDesc;
    }

    /**
     * @param bisDesc the bisDesc to set
     */
    public void setBisDesc(String bisDesc) {
        this.bisDesc = bisDesc;
    }

    /**
     * @return the logtitute
     */
    public String getLogtitute() {
        return logtitute;
    }

    /**
     * @param logtitute the logtitute to set
     */
    public void setLogtitute(String logtitute) {
        this.logtitute = logtitute;
    }

    /**
     * @return the latitite
     */
    public String getLatitite() {
        return latitite;
    }

    /**
     * @param latitite the latitite to set
     */
    public void setLatitite(String latitite) {
        this.latitite = latitite;
    }

    /**
     * @return the bisTypeDesc
     */
    public String getBisTypeDesc() {
        return bisTypeDesc;
    }

    /**
     * @param bisTypeDesc the bisTypeDesc to set
     */
    public void setBisTypeDesc(String bisTypeDesc) {
        this.bisTypeDesc = bisTypeDesc;
    }

    /**
     * @return the bisTypeId
     */
    public int getBisTypeId() {
        return bisTypeId;
    }

    /**
     * @param bisTypeId the bisTypeId to set
     */
    public void setBisTypeId(int bisTypeId) {
        this.bisTypeId = bisTypeId;
    }

    /**
     * @return the enableFlag
     */
    public int getEnableFlag() {
        return enableFlag;
    }

    /**
     * @param enableFlag the enableFlag to set
     */
    public void setEnableFlag(int enableFlag) {
        this.enableFlag = enableFlag;
    }

    /**
     * @return the vistiorBisId
     */
    public int getVistiorBisId() {
        return vistiorBisId;
    }

    /**
     * @param vistiorBisId the vistiorBisId to set
     */
    public void setVistiorBisId(int vistiorBisId) {
        this.vistiorBisId = vistiorBisId;
    }

    /**
     * @return the visitDate
     */
    public String getVisitDate() {
        return visitDate;
    }

    /**
     * @param visitDate the visitDate to set
     */
    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the parantName
     */
    public String getParantName() {
        return parantName;
    }

    /**
     * @param parantName the parantName to set
     */
    public void setParantName(String parantName) {
        this.parantName = parantName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the telePhoneNum
     */
    public String getTelePhoneNum() {
        return telePhoneNum;
    }

    /**
     * @param telePhoneNum the telePhoneNum to set
     */
    public void setTelePhoneNum(String telePhoneNum) {
        this.telePhoneNum = telePhoneNum;
    }
    
     
    
    
}
