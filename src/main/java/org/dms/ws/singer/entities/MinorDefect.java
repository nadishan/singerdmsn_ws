/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class MinorDefect {
    
    private int minDefectCode;
    private String minDefectDesc;
    private int mrjDefectCode;
    private int minDefectStatus;
    private String user;

    /**
     * @return the minDefectCode
     */
    public int getMinDefectCode() {
        return minDefectCode;
    }

    /**
     * @param minDefectCode the minDefectCode to set
     */
    public void setMinDefectCode(int minDefectCode) {
        this.minDefectCode = minDefectCode;
    }

    /**
     * @return the minDefectDesc
     */
    public String getMinDefectDesc() {
        return minDefectDesc;
    }

    /**
     * @param minDefectDesc the minDefectDesc to set
     */
    public void setMinDefectDesc(String minDefectDesc) {
        this.minDefectDesc = minDefectDesc;
    }

    /**
     * @return the mrjDefectCode
     */
    public int getMrjDefectCode() {
        return mrjDefectCode;
    }

    /**
     * @param mrjDefectCode the mrjDefectCode to set
     */
    public void setMrjDefectCode(int mrjDefectCode) {
        this.mrjDefectCode = mrjDefectCode;
    }

    /**
     * @return the minDefectStatus
     */
    public int getMinDefectStatus() {
        return minDefectStatus;
    }

    /**
     * @param minDefectStatus the minDefectStatus to set
     */
    public void setMinDefectStatus(int minDefectStatus) {
        this.minDefectStatus = minDefectStatus;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

   
    
    
}
