/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class AcceptInventory {

    private int issueNo;
    private int status;
    private String issueDate;
    private int dsrId;
    private String statusMsg;
    private boolean isPrint;

    private ArrayList<AcceptInvenroryImei> acceptInvImei;

    private int distributerID;
    private double price;
    private double distributorMargin;
    private double delerMargin;
    private double discount;
    private String user;
    private String toName;
    private String type;

    private String distributorName;
    private String distributorAddress;
    private String distributorCode;
    private String distributorTelephone;

    private String subdealerName;
    private String subdealerAddress;
    private String subdealerCode;
    private String subdealerTelephone;
    
    private String manualReceiptNo;

    /**
     * @return the issueNo
     */
    public int getIssueNo() {
        return issueNo;
    }

    /**
     * @param issueNo the issueNo to set
     */
    public void setIssueNo(int issueNo) {
        this.issueNo = issueNo;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the dsrId
     */
    public int getDsrId() {
        return dsrId;
    }

    /**
     * @param dsrId the dsrId to set
     */
    public void setDsrId(int dsrId) {
        this.dsrId = dsrId;
    }

    /**
     * @return the acceptInvImei
     */
    public ArrayList<AcceptInvenroryImei> getAcceptInvImei() {
        return acceptInvImei;
    }

    /**
     * @param acceptInvImei the acceptInvImei to set
     */
    public void setAcceptInvImei(ArrayList<AcceptInvenroryImei> acceptInvImei) {
        this.acceptInvImei = acceptInvImei;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public boolean isIsPrint() {
        return isPrint;
    }

    public void setIsPrint(boolean isPrint) {
        this.isPrint = isPrint;
    }

    public int getDistributerID() {
        return distributerID;
    }

    public void setDistributerID(int distributerID) {
        this.distributerID = distributerID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDistributorMargin() {
        return distributorMargin;
    }

    public void setDistributorMargin(double distributorMargin) {
        this.distributorMargin = distributorMargin;
    }

    public double getDelerMargin() {
        return delerMargin;
    }

    public void setDelerMargin(double delerMargin) {
        this.delerMargin = delerMargin;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDistributorTelephone() {
        return distributorTelephone;
    }

    public void setDistributorTelephone(String distributorTelephone) {
        this.distributorTelephone = distributorTelephone;
    }

    public String getSubdealerName() {
        return subdealerName;
    }

    public void setSubdealerName(String subdealerName) {
        this.subdealerName = subdealerName;
    }

    public String getSubdealerAddress() {
        return subdealerAddress;
    }

    public void setSubdealerAddress(String subdealerAddress) {
        this.subdealerAddress = subdealerAddress;
    }

    public String getSubdealerCode() {
        return subdealerCode;
    }

    public void setSubdealerCode(String subdealerCode) {
        this.subdealerCode = subdealerCode;
    }

    public String getSubdealerTelephone() {
        return subdealerTelephone;
    }

    public void setSubdealerTelephone(String subdealerTelephone) {
        this.subdealerTelephone = subdealerTelephone;
    }

    public String getManualReceiptNo() {
        return manualReceiptNo;
    }

    public void setManualReceiptNo(String manualReceiptNo) {
        this.manualReceiptNo = manualReceiptNo;
    }
    
    

}
