/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class RepairLevel {
    
    private int levelId;
    private String levelCode;
    private String levelName;
    private String levelDesc;
    private int levelCatId;
    private double levelPrice;
    private int levelStatus;
    private String user;

    /**
     * @return the levelId
     */
    public int getLevelId() {
        return levelId;
    }

    /**
     * @param levelId the levelId to set
     */
    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    /**
     * @return the levelCode
     */
    public String getLevelCode() {
        return levelCode;
    }

    /**
     * @param levelCode the levelCode to set
     */
    public void setLevelCode(String levelCode) {
        this.levelCode = levelCode;
    }

    /**
     * @return the levelName
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * @param levelName the levelName to set
     */
    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    /**
     * @return the levelDesc
     */
    public String getLevelDesc() {
        return levelDesc;
    }

    /**
     * @param levelDesc the levelDesc to set
     */
    public void setLevelDesc(String levelDesc) {
        this.levelDesc = levelDesc;
    }

    /**
     * @return the levelCatId
     */
    public int getLevelCatId() {
        return levelCatId;
    }

    /**
     * @param levelCatId the levelCatId to set
     */
    public void setLevelCatId(int levelCatId) {
        this.levelCatId = levelCatId;
    }

    /**
     * @return the levelPrice
     */
    public double getLevelPrice() {
        return levelPrice;
    }

    /**
     * @param levelPrice the levelPrice to set
     */
    public void setLevelPrice(double levelPrice) {
        this.levelPrice = levelPrice;
    }

    /**
     * @return the levelStatus
     */
    public int getLevelStatus() {
        return levelStatus;
    }

    /**
     * @param levelStatus the levelStatus to set
     */
    public void setLevelStatus(int levelStatus) {
        this.levelStatus = levelStatus;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    
    
}
