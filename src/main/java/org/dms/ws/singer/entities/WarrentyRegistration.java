/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WarrentyRegistration {
    

      private int warrntyRegRefNo;
      private String imeiNo;
      private String customerName;
      private String customerNIC;
      private String contactNo;
      private String dateOfBirth;
      private double sellingPrice;
      private int modleNo;
      private String modleDescription;
      private String product;
      private int bisId;
      private String registerDate; 
      private String email;
      private String customerCode;
      private String shopCode;
      private String siteDesc;
      private String orderNo;
      private String proofPurchas;
      private int warantyType;
      private int remainsDays;
      private String customerAddress;


    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerNIC
     */
    public String getCustomerNIC() {
        return customerNIC;
    }

    /**
     * @param customerNIC the customerNIC to set
     */
    public void setCustomerNIC(String customerNIC) {
        this.customerNIC = customerNIC;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the sellingPrice
     */
    public double getSellingPrice() {
        return sellingPrice;
    }

    /**
     * @param sellingPrice the sellingPrice to set
     */
    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the modleDescription
     */
    public String getModleDescription() {
        return modleDescription;
    }

    /**
     * @param modleDescription the modleDescription to set
     */
    public void setModleDescription(String modleDescription) {
        this.modleDescription = modleDescription;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the warrntyRegRefNo
     */
    public int getWarrntyRegRefNo() {
        return warrntyRegRefNo;
    }

    /**
     * @param warrntyRegRefNo the warrntyRegRefNo to set
     */
    public void setWarrntyRegRefNo(int warrntyRegRefNo) {
        this.warrntyRegRefNo = warrntyRegRefNo;
    }

    /**
     * @return the registerDate
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * @param registerDate the registerDate to set
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the customerCode
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * @param customerCode the customerCode to set
     */
    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    /**
     * @return the shopCode
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * @param shopCode the shopCode to set
     */
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    /**
     * @return the siteDesc
     */
    public String getSiteDesc() {
        return siteDesc;
    }

    /**
     * @param siteDesc the siteDesc to set
     */
    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    /**
     * @return the orderNo
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the proofPurchas
     */
    public String getProofPurchas() {
        return proofPurchas;
    }

    /**
     * @param proofPurchas the proofPurchas to set
     */
    public void setProofPurchas(String proofPurchas) {
        this.proofPurchas = proofPurchas;
    }

    /**
     * @return the warantyType
     */
    public int getWarantyType() {
        return warantyType;
    }

    /**
     * @param warantyType the warantyType to set
     */
    public void setWarantyType(int warantyType) {
        this.warantyType = warantyType;
    }

    /**
     * @return the remainsDays
     */
    public int getRemainsDays() {
        return remainsDays;
    }

    /**
     * @param remainsDays the remainsDays to set
     */
    public void setRemainsDays(int remainsDays) {
        this.remainsDays = remainsDays;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }
      
    
}
