/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class PaymentApprove {
    
    private String setviceName;
    private double paymentPrice;
    private int paymentId;
    private String refDate;
    private int status;
    private String poNumber;
    private String poDate;
    private double settleAmount;
    private String remarks;
    private String chequeNo;
    private String chequeDetails;
    private String chequeDate;
    private double tax;
    private String essdAccountNo;
    private String paymentDate;
    private String statusMsg;
    private double vatValue;
    private double nbtValue;
    
    private int approveBisId;
    private String delayDate;
    
    private String workOrderNo;
    private int deductionAmount;
    private double totalAmountWithDeduct;
    private double lineDeductSum;
    
    private ArrayList<WorkOrderPaymentDetail> woPaymentDetials;
            

    /**
     * @return the setviceName
     */
    public String getSetviceName() {
        return setviceName;
    }

    /**
     * @param setviceName the setviceName to set
     */
    public void setSetviceName(String setviceName) {
        this.setviceName = setviceName;
    }

    /**
     * @return the paymentPrice
     */
    public double getPaymentPrice() {
        return paymentPrice;
    }

    /**
     * @param paymentPrice the paymentPrice to set
     */
    public void setPaymentPrice(double paymentPrice) {
        this.paymentPrice = paymentPrice;
    }

    /**
     * @return the paymentId
     */
    public int getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the refDate
     */
    public String getRefDate() {
        return refDate;
    }

    /**
     * @param refDate the refDate to set
     */
    public void setRefDate(String refDate) {
        this.refDate = refDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the poNumber
     */
    public String getPoNumber() {
        return poNumber;
    }

    /**
     * @param poNumber the poNumber to set
     */
    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    /**
     * @return the poDate
     */
    public String getPoDate() {
        return poDate;
    }

    /**
     * @param poDate the poDate to set
     */
    public void setPoDate(String poDate) {
        this.poDate = poDate;
    }

    /**
     * @return the settleAmount
     */
    public double getSettleAmount() {
        return settleAmount;
    }

    /**
     * @param settleAmount the settleAmount to set
     */
    public void setSettleAmount(double settleAmount) {
        this.settleAmount = settleAmount;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the chequeNo
     */
    public String getChequeNo() {
        return chequeNo;
    }

    /**
     * @param chequeNo the chequeNo to set
     */
    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    /**
     * @return the chequeDetails
     */
    public String getChequeDetails() {
        return chequeDetails;
    }

    /**
     * @param chequeDetails the chequeDetails to set
     */
    public void setChequeDetails(String chequeDetails) {
        this.chequeDetails = chequeDetails;
    }

    /**
     * @return the chequeDate
     */
    public String getChequeDate() {
        return chequeDate;
    }

    /**
     * @param chequeDate the chequeDate to set
     */
    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    /**
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(double tax) {
        this.tax = tax;
    }

    /**
     * @return the essdAccountNo
     */
    public String getEssdAccountNo() {
        return essdAccountNo;
    }

    /**
     * @param essdAccountNo the essdAccountNo to set
     */
    public void setEssdAccountNo(String essdAccountNo) {
        this.essdAccountNo = essdAccountNo;
    }

    /**
     * @return the paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public double getVatValue() {
        return vatValue;
    }

    public void setVatValue(double vatValue) {
        this.vatValue = vatValue;
    }

    public double getNbtValue() {
        return nbtValue;
    }

    public void setNbtValue(double nbtValue) {
        this.nbtValue = nbtValue;
    }
    
    

    /**
     * @return the woPaymentDetials
     */
    public ArrayList<WorkOrderPaymentDetail> getWoPaymentDetials() {
        return woPaymentDetials;
    }

    /**
     * @param woPaymentDetials the woPaymentDetials to set
     */
    public void setWoPaymentDetials(ArrayList<WorkOrderPaymentDetail> woPaymentDetials) {
        this.woPaymentDetials = woPaymentDetials;
    }

    public int getApproveBisId() {
        return approveBisId;
    }

    public void setApproveBisId(int approveBisId) {
        this.approveBisId = approveBisId;
    }

    /**
     * @return the delayDate
     */
    public String getDelayDate() {
        return delayDate;
    }

    /**
     * @param delayDate the delayDate to set
     */
    public void setDelayDate(String delayDate) {
        this.delayDate = delayDate;
    }

   
    

   

    /**
     * @return the deductionAmount
     */
    public int getDeductionAmount() {
        return deductionAmount;
    }

    /**
     * @param deductionAmount the deductionAmount to set
     */
    public void setDeductionAmount(int deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public double getTotalAmountWithDeduct() {
        return totalAmountWithDeduct;
    }

    public void setTotalAmountWithDeduct(double totalAmountWithDeduct) {
        this.totalAmountWithDeduct = totalAmountWithDeduct;
    }

    public double getLineDeductSum() {
        return lineDeductSum;
    }

    public void setLineDeductSum(double lineDeductSum) {
        this.lineDeductSum = lineDeductSum;
    }

  
    
    
    
    
    
}
