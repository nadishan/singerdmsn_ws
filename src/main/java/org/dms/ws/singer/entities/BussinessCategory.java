/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class BussinessCategory {
    private int bsCatId;
    private String bsCateName;
    private String bsCateDesc;
    private int bsCateStatus;
    private String bsCateRemarks;

    /**
     * @return the bsCatId
     */
    public int getBsCatId() {
        return bsCatId;
    }

    /**
     * @param bsCatId the bsCatId to set
     */
    public void setBsCatId(int bsCatId) {
        this.bsCatId = bsCatId;
    }

    /**
     * @return the bsCateName
     */
    public String getBsCateName() {
        return bsCateName;
    }

    /**
     * @param bsCateName the bsCateName to set
     */
    public void setBsCateName(String bsCateName) {
        this.bsCateName = bsCateName;
    }

    /**
     * @return the bsCateDesc
     */
    public String getBsCateDesc() {
        return bsCateDesc;
    }

    /**
     * @param bsCateDesc the bsCateDesc to set
     */
    public void setBsCateDesc(String bsCateDesc) {
        this.bsCateDesc = bsCateDesc;
    }

    /**
     * @return the bsCateStatus
     */
    public int getBsCateStatus() {
        return bsCateStatus;
    }

    /**
     * @param bsCateStatus the bsCateStatus to set
     */
    public void setBsCateStatus(int bsCateStatus) {
        this.bsCateStatus = bsCateStatus;
    }

    /**
     * @return the bsCateRemarks
     */
    public String getBsCateRemarks() {
        return bsCateRemarks;
    }

    /**
     * @param bsCateRemarks the bsCateRemarks to set
     */
    public void setBsCateRemarks(String bsCateRemarks) {
        this.bsCateRemarks = bsCateRemarks;
    }

  
    
    
}
