/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class ImeiMaster {
            private String imeiNo;
            private int modleNo;
            private int bisId;
            private String debitnoteNo;
            private String purchaseDate;
            private String brand;
            private String product;
            private String location;
            private double salesPrice;
            private int status;
            private String user;
            private String bisName;
            private String modleDesc;
            private String erpCode;
            private String warrantyExpireDate;
            private int expireStatus;
            private int warrantyScheme;
            private double netPrice;

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the debitnoteNo
     */
    public String getDebitnoteNo() {
        return debitnoteNo;
    }

    /**
     * @param debitnoteNo the debitnoteNo to set
     */
    public void setDebitnoteNo(String debitnoteNo) {
        this.debitnoteNo = debitnoteNo;
    }

    /**
     * @return the purchaseDate
     */
    public String getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * @param purchaseDate the purchaseDate to set
     */
    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the salesPrice
     */
    public double getSalesPrice() {
        return salesPrice;
    }

    /**
     * @param salesPrice the salesPrice to set
     */
    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }

    /**
     * @return the warrantyExpireDate
     */
    public String getWarrantyExpireDate() {
        return warrantyExpireDate;
    }

    /**
     * @param warrantyExpireDate the warrantyExpireDate to set
     */
    public void setWarrantyExpireDate(String warrantyExpireDate) {
        this.warrantyExpireDate = warrantyExpireDate;
    }

    /**
     * @return the expireStatus
     */
    public int getExpireStatus() {
        return expireStatus;
    }

    /**
     * @param expireStatus the expireStatus to set
     */
    public void setExpireStatus(int expireStatus) {
        this.expireStatus = expireStatus;
    }

    /**
     * @return the warrantyScheme
     */
    public int getWarrantyScheme() {
        return warrantyScheme;
    }

    /**
     * @param warrantyScheme the warrantyScheme to set
     */
    public void setWarrantyScheme(int warrantyScheme) {
        this.warrantyScheme = warrantyScheme;
    }

    /**
     * @return the netPrice
     */
    public double getNetPrice() {
        return netPrice;
    }

    /**
     * @param netPrice the netPrice to set
     */
    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

            
    
}
