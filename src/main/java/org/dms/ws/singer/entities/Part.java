/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class Part {

    private int partNo;
    private String partDesc;
    private int partExist;
    private String partPrdCode;
    private String partPrdDesc;
    private String partPrdFamily;
    private String partPrdFamilyDesc;
    private String partGroup1;
    private String partGroup2;
    private double partSellPrice;
    private double partCost;
    private String erpPartNo;
    private int status;
    

    /**
     * @return the partNo
     */
    public int getPartNo() {
        return partNo;
    }

    /**
     * @param partNo the partNo to set
     */
    public void setPartNo(int partNo) {
        this.partNo = partNo;
    }

    /**
     * @return the partDesc
     */
    public String getPartDesc() {
        return partDesc;
    }

    /**
     * @param partDesc the partDesc to set
     */
    public void setPartDesc(String partDesc) {
        this.partDesc = partDesc;
    }

    /**
     * @return the partExist
     */
    public int getPartExist() {
        return partExist;
    }

    /**
     * @param partExist the partExist to set
     */
    public void setPartExist(int partExist) {
        this.partExist = partExist;
    }

    /**
     * @return the partPrdCode
     */
    public String getPartPrdCode() {
        return partPrdCode;
    }

    /**
     * @param partPrdCode the partPrdCode to set
     */
    public void setPartPrdCode(String partPrdCode) {
        this.partPrdCode = partPrdCode;
    }

    /**
     * @return the partPrdDesc
     */
    public String getPartPrdDesc() {
        return partPrdDesc;
    }

    /**
     * @param partPrdDesc the partPrdDesc to set
     */
    public void setPartPrdDesc(String partPrdDesc) {
        this.partPrdDesc = partPrdDesc;
    }

    /**
     * @return the partPrdFamily
     */
    public String getPartPrdFamily() {
        return partPrdFamily;
    }

    /**
     * @param partPrdFamily the partPrdFamily to set
     */
    public void setPartPrdFamily(String partPrdFamily) {
        this.partPrdFamily = partPrdFamily;
    }

    /**
     * @return the partPrdFamilyDesc
     */
    public String getPartPrdFamilyDesc() {
        return partPrdFamilyDesc;
    }

    /**
     * @param partPrdFamilyDesc the partPrdFamilyDesc to set
     */
    public void setPartPrdFamilyDesc(String partPrdFamilyDesc) {
        this.partPrdFamilyDesc = partPrdFamilyDesc;
    }

    /**
     * @return the partGroup1
     */
    public String getPartGroup1() {
        return partGroup1;
    }

    /**
     * @param partGroup1 the partGroup1 to set
     */
    public void setPartGroup1(String partGroup1) {
        this.partGroup1 = partGroup1;
    }

    /**
     * @return the partGroup2
     */
    public String getPartGroup2() {
        return partGroup2;
    }

    /**
     * @param partGroup2 the partGroup2 to set
     */
    public void setPartGroup2(String partGroup2) {
        this.partGroup2 = partGroup2;
    }

    /**
     * @return the partSellPrice
     */
    public double getPartSellPrice() {
        return partSellPrice;
    }

    /**
     * @param partSellPrice the partSellPrice to set
     */
    public void setPartSellPrice(double partSellPrice) {
        this.partSellPrice = partSellPrice;
    }

    /**
     * @return the partCost
     */
    public double getPartCost() {
        return partCost;
    }

    /**
     * @param partCost the partCost to set
     */
    public void setPartCost(double partCost) {
        this.partCost = partCost;
    }

    /**
     * @return the erpPartNo
     */
    public String getErpPartNo() {
        return erpPartNo;
    }

    /**
     * @param erpPartNo the erpPartNo to set
     */
    public void setErpPartNo(String erpPartNo) {
        this.erpPartNo = erpPartNo;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

   

}
