/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class QuickWorkOrder {

    private String workOrderNo;
    private String customerName;
//    private String customerAddress;
    private String workTelephoneNo;
    private String email;
    private String customerNic;
    private String imeiNo;
    private String product;
//    private String brand;
    private int modleNo;
    private String  modleDesc;
    private String assessoriesRetained;
    private String defectNo;
//    private String rccReference;
//    private String dateOfSale;
//    private String proofOfPurches;
    private int bisId;
//    private int schemeId;
//    private int workOrderStatus;
//    private int warrentyVrifType;
//    private int nonWarrentyVrifType;
//    private String actionTaken;
//    private int levelId;
    private String remarks;
    private int repairStatus;
//    private int deleveryType;
    private int transferLocation;
    private String serviceCenter;
//    private String courierNo;
    private String deleveryDate;
//    private String gatePass;
    private int status;
    private String createDate;
    private String repairStatusMsq;
    private String collectorNic;
    private String podNo;
    private String collectorName;
    private String contactNo;
    private String collectorNo;
    private String bisAddress;
    private String bisName;
    private String serviceCenterAddress;
    private int serviceCenterId;
    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the workTelephoneNo
     */
    public String getWorkTelephoneNo() {
        return workTelephoneNo;
    }

    /**
     * @param workTelephoneNo the workTelephoneNo to set
     */
    public void setWorkTelephoneNo(String workTelephoneNo) {
        this.workTelephoneNo = workTelephoneNo;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the customerNic
     */
    public String getCustomerNic() {
        return customerNic;
    }

    /**
     * @param customerNic the customerNic to set
     */
    public void setCustomerNic(String customerNic) {
        this.customerNic = customerNic;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the modleNo
     */
    public int getModleNo() {
        return modleNo;
    }

    /**
     * @param modleNo the modleNo to set
     */
    public void setModleNo(int modleNo) {
        this.modleNo = modleNo;
    }

    /**
     * @return the assessoriesRetained
     */
    public String getAssessoriesRetained() {
        return assessoriesRetained;
    }

    /**
     * @param assessoriesRetained the assessoriesRetained to set
     */
    public void setAssessoriesRetained(String assessoriesRetained) {
        this.assessoriesRetained = assessoriesRetained;
    }


    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the repairStatus
     */
    public int getRepairStatus() {
        return repairStatus;
    }

    /**
     * @param repairStatus the repairStatus to set
     */
    public void setRepairStatus(int repairStatus) {
        this.repairStatus = repairStatus;
    }

    /**
     * @return the transferLocation
     */
    public int getTransferLocation() {
        return transferLocation;
    }

    /**
     * @param transferLocation the transferLocation to set
     */
    public void setTransferLocation(int transferLocation) {
        this.transferLocation = transferLocation;
    }

    /**
     * @return the deleveryDate
     */
    public String getDeleveryDate() {
        return deleveryDate;
    }

    /**
     * @param deleveryDate the deleveryDate to set
     */
    public void setDeleveryDate(String deleveryDate) {
        this.deleveryDate = deleveryDate;
    }

    /**
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }

    /**
     * @return the repairStatusMsq
     */
    public String getRepairStatusMsq() {
        return repairStatusMsq;
    }

    /**
     * @param repairStatusMsq the repairStatusMsq to set
     */
    public void setRepairStatusMsq(String repairStatusMsq) {
        this.repairStatusMsq = repairStatusMsq;
    }

    /**
     * @return the defectNo
     */
    public String getDefectNo() {
        return defectNo;
    }

    /**
     * @param defectNo the defectNo to set
     */
    public void setDefectNo(String defectNo) {
        this.defectNo = defectNo;
    }

    /**
     * @return the serviceCenter
     */
    public String getServiceCenter() {
        return serviceCenter;
    }

    /**
     * @param serviceCenter the serviceCenter to set
     */
    public void setServiceCenter(String serviceCenter) {
        this.serviceCenter = serviceCenter;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the collectorNic
     */
    public String getCollectorNic() {
        return collectorNic;
    }
    
    /**
     * @param collectorNic the collectorNic to set
     */
    public void setCollectorNic(String collectorNic) {
        this.collectorNic = collectorNic;
}

    

    /**
     * @return the collectorName
     */
    public String getCollectorName() {
        return collectorName;
    }

    /**
     * @param collectorName the collectorName to set
     */
    public void setCollectorName(String collectorName) {
        this.collectorName = collectorName;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the podNo
     */
    public String getPodNo() {
        return podNo;
    }

    /**
     * @param podNo the podNo to set
     */
    public void setPodNo(String podNo) {
        this.podNo = podNo;
    }

    /**
     * @return the collectorNo
     */
    public String getCollectorNo() {
        return collectorNo;
    }

    /**
     * @param collectorNo the collectorNo to set
     */
    public void setCollectorNo(String collectorNo) {
        this.collectorNo = collectorNo;
    }

    /**
     * @return the bisAddress
     */
    public String getBisAddress() {
        return bisAddress;
    }

    /**
     * @param bisAddress the bisAddress to set
     */
    public void setBisAddress(String bisAddress) {
        this.bisAddress = bisAddress;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    public String getServiceCenterAddress() {
        return serviceCenterAddress;
    }

    public void setServiceCenterAddress(String serviceCenterAddress) {
        this.serviceCenterAddress = serviceCenterAddress;
    }

    public int getServiceCenterId() {
        return serviceCenterId;
    }

    public void setServiceCenterId(int serviceCenterId) {
        this.serviceCenterId = serviceCenterId;
    }

    
}
