/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class EventBussinessList {
    
    private int bisId;
    private String bisName;
    private String userId;
    private String bisDesc;
    private int bisTypeId;
    private String bisTypeDesc;
    private int eventPartId;
    private int status;
    private int seqNo;
    private int usedStatus;

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the bisDesc
     */
    public String getBisDesc() {
        return bisDesc;
    }

    /**
     * @param bisDesc the bisDesc to set
     */
    public void setBisDesc(String bisDesc) {
        this.bisDesc = bisDesc;
    }

    /**
     * @return the bisTypeId
     */
    public int getBisTypeId() {
        return bisTypeId;
    }

    /**
     * @param bisTypeId the bisTypeId to set
     */
    public void setBisTypeId(int bisTypeId) {
        this.bisTypeId = bisTypeId;
    }

    /**
     * @return the bisTypeDesc
     */
    public String getBisTypeDesc() {
        return bisTypeDesc;
    }

    /**
     * @param bisTypeDesc the bisTypeDesc to set
     */
    public void setBisTypeDesc(String bisTypeDesc) {
        this.bisTypeDesc = bisTypeDesc;
    }

    /**
     * @return the eventPartId
     */
    public int getEventPartId() {
        return eventPartId;
    }

    /**
     * @param eventPartId the eventPartId to set
     */
    public void setEventPartId(int eventPartId) {
        this.eventPartId = eventPartId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the usedStatus
     */
    public int getUsedStatus() {
        return usedStatus;
    }

    /**
     * @param usedStatus the usedStatus to set
     */
    public void setUsedStatus(int usedStatus) {
        this.usedStatus = usedStatus;
    }
    
}
