/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class EventTransaction {
    
    private int txnId;
    private int eventId;
    private String userId;
    private String assignTo;
    private String pointAddDate;
    private int ruleId;
    private double salePoint;
    private int nonSaleId;
    private double nonSalePoint;
    private int assignId;
    private String assignBy;
    private int status;
    private String statusMsg;
    private String remarks;
    private int salesType;
    
    private ArrayList<EventNonSalesRule> eventNonSalesRuleList;

    /**
     * @return the txnId
     */
    public int getTxnId() {
        return txnId;
    }

    /**
     * @param txnId the txnId to set
     */
    public void setTxnId(int txnId) {
        this.txnId = txnId;
    }

    /**
     * @return the eventId
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }


    /**
     * @return the pointAddDate
     */
    public String getPointAddDate() {
        return pointAddDate;
    }

    /**
     * @param pointAddDate the pointAddDate to set
     */
    public void setPointAddDate(String pointAddDate) {
        this.pointAddDate = pointAddDate;
    }

    /**
     * @return the ruleId
     */
    public int getRuleId() {
        return ruleId;
    }

    /**
     * @param ruleId the ruleId to set
     */
    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * @return the salePoint
     */
    public double getSalePoint() {
        return salePoint;
    }

    /**
     * @param salePoint the salePoint to set
     */
    public void setSalePoint(double salePoint) {
        this.salePoint = salePoint;
    }

    /**
     * @return the nonSaleId
     */
    public int getNonSaleId() {
        return nonSaleId;
    }

    /**
     * @param nonSaleId the nonSaleId to set
     */
    public void setNonSaleId(int nonSaleId) {
        this.nonSaleId = nonSaleId;
    }

    /**
     * @return the nonSalePoint
     */
    public double getNonSalePoint() {
        return nonSalePoint;
    }

    /**
     * @param nonSalePoint the nonSalePoint to set
     */
    public void setNonSalePoint(double nonSalePoint) {
        this.nonSalePoint = nonSalePoint;
    }

    /**
     * @return the assignBy
     */
    public String getAssignBy() {
        return assignBy;
    }

    /**
     * @param assignBy the assignBy to set
     */
    public void setAssignBy(String assignBy) {
        this.assignBy = assignBy;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
    /**
     * @return the assignTo
     */
    public String getAssignTo() {
        return assignTo;
    }

    /**
     * @param assignTo the assignTo to set
     */
    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    /**
     * @return the assignId
     */
    public int getAssignId() {
        return assignId;
    }

    /**
     * @param assignId the assignId to set
     */
    public void setAssignId(int assignId) {
        this.assignId = assignId;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the salesType
     */
    public int getSalesType() {
        return salesType;
    }

    /**
     * @param salesType the salesType to set
     */
    public void setSalesType(int salesType) {
        this.salesType = salesType;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the eventNonSalesRuleList
     */
    public ArrayList<EventNonSalesRule> getEventNonSalesRuleList() {
        return eventNonSalesRuleList;
    }

    /**
     * @param eventNonSalesRuleList the eventNonSalesRuleList to set
     */
    public void setEventNonSalesRuleList(ArrayList<EventNonSalesRule> eventNonSalesRuleList) {
        this.eventNonSalesRuleList = eventNonSalesRuleList;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }


    
    
    
}
