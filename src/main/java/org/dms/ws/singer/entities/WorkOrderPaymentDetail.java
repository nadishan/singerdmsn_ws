/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class WorkOrderPaymentDetail {
    
    private int seqNo;
    private int paymentId;
    private String workOrderNo;
    private String imeiNo;
    private int warrantyVerifType;
    private int defectType;
    private double amount;
    private String referenceNo;
    private String paymentDate;
    private int status;
    private String defectDesc;
    private String warrentyTypeDesc;
    private double laborCost;
    private double spareCost;
    private String remarks;
    private String tecnician;
    private String actionTake;
    private int nonWarrantyVerifType;
    private String invoice;
    private String statusMsg;
    private int delayDays;
    private double deductionAmount;
    private double initDeduct;
    private int nbtApplicable;
    private int vatApplicable;
    private double lineDeductSum;
    
    private ArrayList<WorkOrderMaintainDetail> woDetials;

    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return the paymentId
     */
    public int getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the warrantyVerifType
     */
    public int getWarrantyVerifType() {
        return warrantyVerifType;
    }

    /**
     * @param warrantyVerifType the warrantyVerifType to set
     */
    public void setWarrantyVerifType(int warrantyVerifType) {
        this.warrantyVerifType = warrantyVerifType;
    }

    /**
     * @return the defectType
     */
    public int getDefectType() {
        return defectType;
    }

    /**
     * @param defectType the defectType to set
     */
    public void setDefectType(int defectType) {
        this.defectType = defectType;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the referenceNo
     */
    public String getReferenceNo() {
        return referenceNo;
    }

    /**
     * @param referenceNo the referenceNo to set
     */
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    /**
     * @return the paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the defectDesc
     */
    public String getDefectDesc() {
        return defectDesc;
    }

    /**
     * @param defectDesc the defectDesc to set
     */
    public void setDefectDesc(String defectDesc) {
        this.defectDesc = defectDesc;
    }

    /**
     * @return the warrentyTypeDesc
     */
    public String getWarrentyTypeDesc() {
        return warrentyTypeDesc;
    }

    /**
     * @param warrentyTypeDesc the warrentyTypeDesc to set
     */
    public void setWarrentyTypeDesc(String warrentyTypeDesc) {
        this.warrentyTypeDesc = warrentyTypeDesc;
    }

    /**
     * @return the laborCost
     */
    public double getLaborCost() {
        return laborCost;
    }

    /**
     * @param laborCost the laborCost to set
     */
    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    /**
     * @return the spareCost
     */
    public double getSpareCost() {
        return spareCost;
    }

    /**
     * @param spareCost the spareCost to set
     */
    public void setSpareCost(double spareCost) {
        this.spareCost = spareCost;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the tecnician
     */
    public String getTecnician() {
        return tecnician;
    }

    /**
     * @param tecnician the tecnician to set
     */
    public void setTecnician(String tecnician) {
        this.tecnician = tecnician;
    }

    /**
     * @return the actionTake
     */
    public String getActionTake() {
        return actionTake;
    }

    /**
     * @param actionTake the actionTake to set
     */
    public void setActionTake(String actionTake) {
        this.actionTake = actionTake;
    }

    /**
     * @return the nonWarrantyVerifType
     */
    public int getNonWarrantyVerifType() {
        return nonWarrantyVerifType;
    }

    /**
     * @param nonWarrantyVerifType the nonWarrantyVerifType to set
     */
    public void setNonWarrantyVerifType(int nonWarrantyVerifType) {
        this.nonWarrantyVerifType = nonWarrantyVerifType;
    }

    /**
     * @return the woDetials
     */
    public ArrayList<WorkOrderMaintainDetail> getWoDetials() {
        return woDetials;
    }

    /**
     * @param woDetials the woDetials to set
     */
    public void setWoDetials(ArrayList<WorkOrderMaintainDetail> woDetials) {
        this.woDetials = woDetials;
    }

    /**
     * @return the invoice
     */
    public String getInvoice() {
        return invoice;
    }

    /**
     * @param invoice the invoice to set
     */
    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public void setWoPaymentDetials(ArrayList<WorkOrderPaymentDetail> paymentDetails) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the delayDays
     */
    public int getDelayDays() {
        return delayDays;
    }

    /**
     * @param delayDays the delayDays to set
     */
    public void setDelayDays(int delayDays) {
        this.delayDays = delayDays;
    }

    /**
     * @return the deductionAmount
     */
    public double getDeductionAmount() {
        return deductionAmount;
    }

    /**
     * @param deductionAmount the deductionAmount to set
     */
    public void setDeductionAmount(double deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    public double getInitDeduct() {
        return initDeduct;
    }

    public void setInitDeduct(double initDeduct) {
        this.initDeduct = initDeduct;
    }

    
    public int getNbtApplicable() {
        return nbtApplicable;
    }

    public void setNbtApplicable(int nbtApplicable) {
        this.nbtApplicable = nbtApplicable;
    }

    public int getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(int vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public double getLineDeductSum() {
        return lineDeductSum;
    }

    public void setLineDeductSum(double lineDeductSum) {
        this.lineDeductSum = lineDeductSum;
    }
    
    
    
}
