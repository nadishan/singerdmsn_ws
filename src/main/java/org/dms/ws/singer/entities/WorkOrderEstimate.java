/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderEstimate {

    private String esRefNo;
    private String workOrderNo;
    private String imeiNo;
    private int bisId;
    private double costSparePart;
    private double costLabor;
    private int status;
    private String statusMsg;
    private String customerRef;
    private int paymentOption;

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the esRefNo
     */
    public String getEsRefNo() {
        return esRefNo;
    }

    /**
     * @param esRefNo the esRefNo to set
     */
    public void setEsRefNo(String esRefNo) {
        this.esRefNo = esRefNo;
    }

    /**
     * @return the workOrderNo
     */
    public String getWorkOrderNo() {
        return workOrderNo;
    }

    /**
     * @param workOrderNo the workOrderNo to set
     */
    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the costSparePart
     */
    public double getCostSparePart() {
        return costSparePart;
    }

    /**
     * @param costSparePart the costSparePart to set
     */
    public void setCostSparePart(double costSparePart) {
        this.costSparePart = costSparePart;
    }

    /**
     * @return the costLabor
     */
    public double getCostLabor() {
        return costLabor;
    }

    /**
     * @param costLabor the costLabor to set
     */
    public void setCostLabor(double costLabor) {
        this.costLabor = costLabor;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the customerRef
     */
    public String getCustomerRef() {
        return customerRef;
    }

    /**
     * @param customerRef the customerRef to set
     */
    public void setCustomerRef(String customerRef) {
        this.customerRef = customerRef;
    }

    /**
     * @return the paymentOption
     */
    public int getPaymentOption() {
        return paymentOption;
    }

    /**
     * @param paymentOption the paymentOption to set
     */
    public void setPaymentOption(int paymentOption) {
        this.paymentOption = paymentOption;
    }

}
