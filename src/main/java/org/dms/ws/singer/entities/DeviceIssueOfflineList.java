/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author Shanka
 */
public class DeviceIssueOfflineList {
    
    private String id;
    private String debitNoteNo;
    private String issueNo;
    private int distributerID;
    private String fromName;
    private int dSRId;
    private String toName;
    private String issueDate;
    private double price;
    private double distributorMargin;
    private double delerMargin;
    private double discount;
    private int status;
    private String user;
    private String statusMsg;
    private String remarks;
    private int dtlCnt;
    private String type;

    private ArrayList<DeviceIssueIme> deviceImei;

    private String distributorName;
    private String distributorAddress;
    private String distributorCode;
    private String distributorTelephone;
    private String subdealerName;
    private String subdealerAddress;
    private String subdealerCode;
    private String subdealerTelephone;

    private String dateInserted;

    private String manualReceiptNo;
    
    private String offlineIssueNo;
    
    private int isSubmit;

    public String getDebitNoteNo() {
        return debitNoteNo;
    }

    public void setDebitNoteNo(String debitNoteNo) {
        this.debitNoteNo = debitNoteNo;
    }

    public String getIssueNo() {
        return issueNo;
    }

    public void setIssueNo(String issueNo) {
        this.issueNo = issueNo;
    }

    public int getDistributerID() {
        return distributerID;
    }

    public void setDistributerID(int distributerID) {
        this.distributerID = distributerID;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public int getdSRId() {
        return dSRId;
    }

    public void setdSRId(int dSRId) {
        this.dSRId = dSRId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDistributorMargin() {
        return distributorMargin;
    }

    public void setDistributorMargin(double distributorMargin) {
        this.distributorMargin = distributorMargin;
    }

    public double getDelerMargin() {
        return delerMargin;
    }

    public void setDelerMargin(double delerMargin) {
        this.delerMargin = delerMargin;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getDtlCnt() {
        return dtlCnt;
    }

    public void setDtlCnt(int dtlCnt) {
        this.dtlCnt = dtlCnt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<DeviceIssueIme> getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(ArrayList<DeviceIssueIme> deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDistributorTelephone() {
        return distributorTelephone;
    }

    public void setDistributorTelephone(String distributorTelephone) {
        this.distributorTelephone = distributorTelephone;
    }

    public String getSubdealerName() {
        return subdealerName;
    }

    public void setSubdealerName(String subdealerName) {
        this.subdealerName = subdealerName;
    }

    public String getSubdealerAddress() {
        return subdealerAddress;
    }

    public void setSubdealerAddress(String subdealerAddress) {
        this.subdealerAddress = subdealerAddress;
    }

    public String getSubdealerCode() {
        return subdealerCode;
    }

    public void setSubdealerCode(String subdealerCode) {
        this.subdealerCode = subdealerCode;
    }

    public String getSubdealerTelephone() {
        return subdealerTelephone;
    }

    public void setSubdealerTelephone(String subdealerTelephone) {
        this.subdealerTelephone = subdealerTelephone;
    }

    public String getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(String dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getManualReceiptNo() {
        return manualReceiptNo;
    }

    public void setManualReceiptNo(String manualReceiptNo) {
        this.manualReceiptNo = manualReceiptNo;
    }

    public String getOfflineIssueNo() {
        return offlineIssueNo;
    }

    public void setOfflineIssueNo(String offlineIssueNo) {
        this.offlineIssueNo = offlineIssueNo;
    }

    public int getIsSubmit() {
        return isSubmit;
    }

    public void setIsSubmit(int isSubmit) {
        this.isSubmit = isSubmit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
}
