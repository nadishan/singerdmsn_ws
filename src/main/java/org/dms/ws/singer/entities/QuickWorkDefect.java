/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class QuickWorkDefect {
    
    private int defectCode;
    private String defectDesc;

    

    /**
     * @return the defectDesc
     */
    public String getDefectDesc() {
        return defectDesc;
    }

    /**
     * @param defectDesc the defectDesc to set
     */
    public void setDefectDesc(String defectDesc) {
        this.defectDesc = defectDesc;
    }

    /**
     * @return the defectCode
     */
    public int getDefectCode() {
        return defectCode;
    }

    /**
     * @param defectCode the defectCode to set
     */
    public void setDefectCode(int defectCode) {
        this.defectCode = defectCode;
    }
    
}
