/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */

public class DeviceIssue {
    
    private String debitNoteNo;
    private int issueNo;
    private int distributerID;
    private String fromName;
    private int dSRId;
    private String toName;
    private String issueDate;
    private double price;
    private double distributorMargin;
    private double delerMargin;
    private double discount;
    private int status;
    private String user;
    private String statusMsg;
    private String remarks;
    private int dtlCnt;
    private String type;
    
    private ArrayList<DeviceIssueIme> deviceImei;
    
    private String distributorName;
    private String distributorAddress;
    private String distributorCode;
    private String distributorTelephone;
    private String subdealerName;
    private String subdealerAddress;
    private String subdealerCode;
    private String subdealerTelephone;
    
    private String dateInserted;
    
    private String manualReceiptNo;

    /**
     * @return the issueNo
     */
    public int getIssueNo() {
        return issueNo;
    }

    /**
     * @param issueNo the issueNo to set
     */
    public void setIssueNo(int issueNo) {
        this.issueNo = issueNo;
    }

    /**
     * @return the distributerID
     */
    public int getDistributerID() {
        return distributerID;
    }

    /**
     * @param distributerID the distributerID to set
     */
    public void setDistributerID(int distributerID) {
        this.distributerID = distributerID;
    }

    /**
     * @return the dSRId
     */
    public int getdSRId() {
        return dSRId;
    }

    /**
     * @param dSRId the dSRId to set
     */
    public void setdSRId(int dSRId) {
        this.dSRId = dSRId;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the distributorMargin
     */
    public double getDistributorMargin() {
        return distributorMargin;
    }

    /**
     * @param distributorMargin the distributorMargin to set
     */
    public void setDistributorMargin(double distributorMargin) {
        this.distributorMargin = distributorMargin;
    }

    /**
     * @return the delerMargin
     */
    public double getDelerMargin() {
        return delerMargin;
    }

    /**
     * @param delerMargin the delerMargin to set
     */
    public void setDelerMargin(double delerMargin) {
        this.delerMargin = delerMargin;
    }

    /**
     * @return the discount
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the deviceImei
     */
    public ArrayList<DeviceIssueIme> getDeviceImei() {
        return deviceImei;
    }

    /**
     * @param deviceImei the deviceImei to set
     */
    public void setDeviceImei(ArrayList<DeviceIssueIme> deviceImei) {
        this.deviceImei = deviceImei;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the debitNoteNo
     */
    public String getDebitNoteNo() {
        return debitNoteNo;
    }

    /**
     * @param debitNoteNo the debitNoteNo to set
     */
    public void setDebitNoteNo(String debitNoteNo) {
        this.debitNoteNo = debitNoteNo;
    }

    /**
     * @return the fromName
     */
    public String getFromName() {
        return fromName;
    }

    /**
     * @param fromName the fromName to set
     */
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    /**
     * @return the toName
     */
    public String getToName() {
        return toName;
    }

    /**
     * @param toName the toName to set
     */
    public void setToName(String toName) {
        this.toName = toName;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the dtlCnt
     */
    public int getDtlCnt() {
        return dtlCnt;
    }

    /**
     * @param dtlCnt the dtlCnt to set
     */
    public void setDtlCnt(int dtlCnt) {
        this.dtlCnt = dtlCnt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDistributorTelephone() {
        return distributorTelephone;
    }

    public void setDistributorTelephone(String distributorTelephone) {
        this.distributorTelephone = distributorTelephone;
    }

    public String getSubdealerName() {
        return subdealerName;
    }

    public void setSubdealerName(String subdealerName) {
        this.subdealerName = subdealerName;
    }

    public String getSubdealerAddress() {
        return subdealerAddress;
    }

    public void setSubdealerAddress(String subdealerAddress) {
        this.subdealerAddress = subdealerAddress;
    }

    public String getSubdealerCode() {
        return subdealerCode;
    }

    public void setSubdealerCode(String subdealerCode) {
        this.subdealerCode = subdealerCode;
    }

    public String getSubdealerTelephone() {
        return subdealerTelephone;
    }

    public void setSubdealerTelephone(String subdealerTelephone) {
        this.subdealerTelephone = subdealerTelephone;
    }

    public String getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(String dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getManualReceiptNo() {
        return manualReceiptNo;
    }

    public void setManualReceiptNo(String manualReceiptNo) {
        this.manualReceiptNo = manualReceiptNo;
    }

   
    
    
}
