/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class MajorDefect {
    
    private int mjrDefectCode;
    private String mjrDefectDesc;
    private int mjrDefectStatus;
    private String user;

    /**
     * @return the mjrDefectCode
     */
    public int getMjrDefectCode() {
        return mjrDefectCode;
    }

    /**
     * @param mjrDefectCode the mjrDefectCode to set
     */
    public void setMjrDefectCode(int mjrDefectCode) {
        this.mjrDefectCode = mjrDefectCode;
    }

    /**
     * @return the mjrDefectDesc
     */
    public String getMjrDefectDesc() {
        return mjrDefectDesc;
    }

    /**
     * @param mjrDefectDesc the mjrDefectDesc to set
     */
    public void setMjrDefectDesc(String mjrDefectDesc) {
        this.mjrDefectDesc = mjrDefectDesc;
    }

    /**
     * @return the mjrDefectStatus
     */
    public int getMjrDefectStatus() {
        return mjrDefectStatus;
    }

    /**
     * @param mjrDefectStatus the mjrDefectStatus to set
     */
    public void setMjrDefectStatus(int mjrDefectStatus) {
        this.mjrDefectStatus = mjrDefectStatus;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

   
    
}
