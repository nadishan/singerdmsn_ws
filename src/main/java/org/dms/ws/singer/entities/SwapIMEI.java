/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author Dasun Chathuranga
 */
public class SwapIMEI {
    private int bisId;
    private String bisName;
    private String statusMsg;

    public int getBisId() {
        return bisId;
    }

    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    public String getBisName() {
        return bisName;
    }

    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

   
    
    
    
}
