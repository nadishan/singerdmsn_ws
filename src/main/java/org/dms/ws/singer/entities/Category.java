/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class Category {
    
    private int cateId;
    private String cateName;
    private String cateDesc;
    private int cateStatus;
    private String remarks;
    private String user;

    /**
     * @return the cateId
     */
    public int getCateId() {
        return cateId;
    }

    /**
     * @param cateId the cateId to set
     */
    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    /**
     * @return the cateName
     */
    public String getCateName() {
        return cateName;
    }

    /**
     * @param cateName the cateName to set
     */
    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    /**
     * @return the cateDesc
     */
    public String getCateDesc() {
        return cateDesc;
    }

    /**
     * @param cateDesc the cateDesc to set
     */
    public void setCateDesc(String cateDesc) {
        this.cateDesc = cateDesc;
    }

    /**
     * @return the cateStatus
     */
    public int getCateStatus() {
        return cateStatus;
    }

    /**
     * @param cateStatus the cateStatus to set
     */
    public void setCateStatus(int cateStatus) {
        this.cateStatus = cateStatus;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

  
    
    
}
