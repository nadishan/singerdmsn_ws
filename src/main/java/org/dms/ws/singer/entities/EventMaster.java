/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class EventMaster {

    private int eventId;
    private String eventName;
    private String eventDesc;
    private String startDate;
    private String endDate;
    private String threshold;
    private int status;
    private String statusMsg;
    private String eventMrgComment;
    private String mktMrgComment;
    private String eventAchievement;
    private String retunType;
    private int eventPoint;
    
    private ArrayList<EventAward> evenAwardList;
    private ArrayList<EventNonSalesRule> eventNonSalesRuleList;
    private ArrayList<EventSalesRule> eventSalesRuleList;
    private ArrayList<EventBisType> eventBisTypeList;

    /**
     * @return the eventId
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * @param eventName the eventName to set
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    /**
     * @return the eventDesc
     */
    public String getEventDesc() {
        return eventDesc;
    }

    /**
     * @param eventDesc the eventDesc to set
     */
    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the eventMrgComment
     */
    public String getEventMrgComment() {
        return eventMrgComment;
    }

    /**
     * @param eventMrgComment the eventMrgComment to set
     */
    public void setEventMrgComment(String eventMrgComment) {
        this.eventMrgComment = eventMrgComment;
    }

    /**
     * @return the mktMrgComment
     */
    public String getMktMrgComment() {
        return mktMrgComment;
    }

    /**
     * @param mktMrgComment the mktMrgComment to set
     */
    public void setMktMrgComment(String mktMrgComment) {
        this.mktMrgComment = mktMrgComment;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * @return the threshold
     */
    public String getThreshold() {
        return threshold;
    }

    /**
     * @param threshold the threshold to set
     */
    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    /**
     * @return the evenAwardList
     */
    public ArrayList<EventAward> getEvenAwardList() {
        return evenAwardList;
    }

    /**
     * @param evenAwardList the evenAwardList to set
     */
    public void setEvenAwardList(ArrayList<EventAward> evenAwardList) {
        this.evenAwardList = evenAwardList;
    }

    /**
     * @return the eventNonSalesRuleList
     */
    public ArrayList<EventNonSalesRule> getEventNonSalesRuleList() {
        return eventNonSalesRuleList;
    }

    /**
     * @param eventNonSalesRuleList the eventNonSalesRuleList to set
     */
    public void setEventNonSalesRuleList(ArrayList<EventNonSalesRule> eventNonSalesRuleList) {
        this.eventNonSalesRuleList = eventNonSalesRuleList;
    }

    /**
     * @return the eventSalesRuleList
     */
    public ArrayList<EventSalesRule> getEventSalesRuleList() {
        return eventSalesRuleList;
    }

    /**
     * @param eventSalesRuleList the eventSalesRuleList to set
     */
    public void setEventSalesRuleList(ArrayList<EventSalesRule> eventSalesRuleList) {
        this.eventSalesRuleList = eventSalesRuleList;
    }

    /**
     * @return the eventBisTypeList
     */
    public ArrayList<EventBisType> getEventBisTypeList() {
        return eventBisTypeList;
    }

    /**
     * @param eventBisTypeList the eventBisTypeList to set
     */
    public void setEventBisTypeList(ArrayList<EventBisType> eventBisTypeList) {
        this.eventBisTypeList = eventBisTypeList;
    }

    /**
     * @return the eventAchievement
     */
    public String getEventAchievement() {
        return eventAchievement;
    }

    /**
     * @param eventAchievement the eventAchievement to set
     */
    public void setEventAchievement(String eventAchievement) {
        this.eventAchievement = eventAchievement;
    }

    /**
     * @return the retunType
     */
    public String getRetunType() {
        return retunType;
    }

    /**
     * @param retunType the retunType to set
     */
    public void setRetunType(String retunType) {
        this.retunType = retunType;
    }

    /**
     * @return the eventPoint
     */
    public int getEventPoint() {
        return eventPoint;
    }

    /**
     * @param eventPoint the eventPoint to set
     */
    public void setEventPoint(int eventPoint) {
        this.eventPoint = eventPoint;
    }

  



    
    
}
