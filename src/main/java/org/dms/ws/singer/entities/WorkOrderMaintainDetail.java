/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class WorkOrderMaintainDetail {
    
    private String wrokOrderNo;
    private int workOrderMainId;
    private int seqNo;
    private int partNo;
    private String partDesc;
    private double partSellPrice;            
    private String invoiceNo;
    private int thirdPartyFlag;
    private int status;

    /**
     * @return the wrokOrderNo
     */
    public String getWrokOrderNo() {
        return wrokOrderNo;
    }

    /**
     * @param wrokOrderNo the wrokOrderNo to set
     */
    public void setWrokOrderNo(String wrokOrderNo) {
        this.wrokOrderNo = wrokOrderNo;
    }

    /**
     * @return the seqNo
     */
    public int getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

   

    /**
     * @return the partDesc
     */
    public String getPartDesc() {
        return partDesc;
    }

    /**
     * @param partDesc the partDesc to set
     */
    public void setPartDesc(String partDesc) {
        this.partDesc = partDesc;
    }


    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * @return the thirdPartyFlag
     */
    public int getThirdPartyFlag() {
        return thirdPartyFlag;
    }

    /**
     * @param thirdPartyFlag the thirdPartyFlag to set
     */
    public void setThirdPartyFlag(int thirdPartyFlag) {
        this.thirdPartyFlag = thirdPartyFlag;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the partNo
     */
    public int getPartNo() {
        return partNo;
    }

    /**
     * @param partNo the partNo to set
     */
    public void setPartNo(int partNo) {
        this.partNo = partNo;
    }

    /**
     * @return the workOrderMainId
     */
    public int getWorkOrderMainId() {
        return workOrderMainId;
    }

    /**
     * @param workOrderMainId the workOrderMainId to set
     */
    public void setWorkOrderMainId(int workOrderMainId) {
        this.workOrderMainId = workOrderMainId;
    }

    /**
     * @return the partSellPrice
     */
    public double getPartSellPrice() {
        return partSellPrice;
    }

    /**
     * @param partSellPrice the partSellPrice to set
     */
    public void setPartSellPrice(double partSellPrice) {
        this.partSellPrice = partSellPrice;
    }
    
    
}
