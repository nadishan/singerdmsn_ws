/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class BussinessType {
    
    private int bisTypeId;
    private String bisTypeDesc;
    private int entyMode;
    private int status;

    /**
     * @return the bisTypeId
     */
    public int getBisTypeId() {
        return bisTypeId;
    }

    /**
     * @param bisTypeId the bisTypeId to set
     */
    public void setBisTypeId(int bisTypeId) {
        this.bisTypeId = bisTypeId;
    }

    /**
     * @return the bisTypeDesc
     */
    public String getBisTypeDesc() {
        return bisTypeDesc;
    }

    /**
     * @param bisTypeDesc the bisTypeDesc to set
     */
    public void setBisTypeDesc(String bisTypeDesc) {
        this.bisTypeDesc = bisTypeDesc;
    }

    /**
     * @return the entyMode
     */
    public int getEntyMode() {
        return entyMode;
    }

    /**
     * @param entyMode the entyMode to set
     */
    public void setEntyMode(int entyMode) {
        this.entyMode = entyMode;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
    
}
