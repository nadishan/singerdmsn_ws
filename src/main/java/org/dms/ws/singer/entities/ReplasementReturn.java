/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class ReplasementReturn {
   // REPL_RETURN_NO,EXG_REF_NO,DISTRIBUTER_ID,IMEI_NO,RETURN_DATE,STATUS
            
    private int replRetutnNo;
    private String exchangeReferenceNo;
    private int distributerId;
    private String imeiNo;
    private String returnDate;
    private int status;
    private String user;
    private String modleDesc;
    private String statusMsg;

    /**
     * @return the replRetutnNo
     */
    public int getReplRetutnNo() {
        return replRetutnNo;
    }

    /**
     * @param replRetutnNo the replRetutnNo to set
     */
    public void setReplRetutnNo(int replRetutnNo) {
        this.replRetutnNo = replRetutnNo;
    }


    /**
     * @return the distributerId
     */
    public int getDistributerId() {
        return distributerId;
    }

    /**
     * @param distributerId the distributerId to set
     */
    public void setDistributerId(int distributerId) {
        this.distributerId = distributerId;
    }

    /**
     * @return the imeiNo
     */
    public String getImeiNo() {
        return imeiNo;
    }

    /**
     * @param imeiNo the imeiNo to set
     */
    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    /**
     * @return the returnDate
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the exchangeReferenceNo
     */
    public String getExchangeReferenceNo() {
        return exchangeReferenceNo;
    }

    /**
     * @param exchangeReferenceNo the exchangeReferenceNo to set
     */
    public void setExchangeReferenceNo(String exchangeReferenceNo) {
        this.exchangeReferenceNo = exchangeReferenceNo;
    }

    /**
     * @return the modleDesc
     */
    public String getModleDesc() {
        return modleDesc;
    }

    /**
     * @param modleDesc the modleDesc to set
     */
    public void setModleDesc(String modleDesc) {
        this.modleDesc = modleDesc;
    }

    /**
     * @return the statusMsg
     */
    public String getStatusMsg() {
        return statusMsg;
    }

    /**
     * @param statusMsg the statusMsg to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }
    
    
}
