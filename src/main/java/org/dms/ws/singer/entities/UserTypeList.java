/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.entities;

/**
 *
 * @author SDU
 */
public class UserTypeList {
    
    private int bisId;
    private String userId;
    private String firstName;
    private String lastName;
    private String bisName;
    private String longitude;
    private String latitude;
    private String bisDesc;
    private String address;
    private String teleNumber;
    private int parent_bisId;
    private String parent_bisName;

    /**
     * @return the bisId
     */
    public int getBisId() {
        return bisId;
    }

    /**
     * @param bisId the bisId to set
     */
    public void setBisId(int bisId) {
        this.bisId = bisId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the bisName
     */
    public String getBisName() {
        return bisName;
    }

    /**
     * @param bisName the bisName to set
     */
    public void setBisName(String bisName) {
        this.bisName = bisName;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the bisDesc
     */
    public String getBisDesc() {
        return bisDesc;
    }

    /**
     * @param bisDesc the bisDesc to set
     */
    public void setBisDesc(String bisDesc) {
        this.bisDesc = bisDesc;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the teleNumber
     */
    public String getTeleNumber() {
        return teleNumber;
    }

    /**
     * @param teleNumber the teleNumber to set
     */
    public void setTeleNumber(String teleNumber) {
        this.teleNumber = teleNumber;
    }

    public int getParent_bisId() {
        return parent_bisId;
    }

    public void setParent_bisId(int parent_bisId) {
        this.parent_bisId = parent_bisId;
    }

    public String getParent_bisName() {
        return parent_bisName;
    }

    public void setParent_bisName(String parent_bisName) {
        this.parent_bisName = parent_bisName;
    }
    
    
}
