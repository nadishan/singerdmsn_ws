/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.EventBussinessList;
import org.dms.ws.singer.entities.EventNonSalesRule;
import org.dms.ws.singer.entities.EventTransaction;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class EventAssignMarkController {

    public static MessageWrapper getEventNonSalesRuleList(int event_id) {
        logger.info("getEventNonSalesRuleList method Call..........");
        ArrayList<EventNonSalesRule> nonSalesList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectNonSales = "NS_RULE_ID,EVENT_ID,"
                    + "DESCRIPTION,TARGET,"
                    + "POINTS,SALE_TYPE,STATUS";

            ResultSet rs_nonSales = dbCon.search(con, "SELECT (SELECT COUNT(*) FROM SD_EVENT_NON_SALE_RULES WHERE EVENT_ID = " + event_id + ") CNT,"
                    + "" + selectNonSales + " FROM SD_EVENT_NON_SALE_RULES "
                    + "WHERE EVENT_ID = " + event_id + "");

            while (rs_nonSales.next()) {
                EventNonSalesRule ns = new EventNonSalesRule();
                totalrecode = rs_nonSales.getInt("CNT");
                ns.setNonSalesRule(rs_nonSales.getInt("NS_RULE_ID"));;
                ns.setEventId(rs_nonSales.getInt("EVENT_ID"));
                ns.setDescription(rs_nonSales.getString("DESCRIPTION"));
                ns.setTaget(rs_nonSales.getInt("TARGET"));
                ns.setPoint(rs_nonSales.getInt("POINTS"));
                ns.setSalesType(rs_nonSales.getInt("SALE_TYPE"));
                ns.setStatus(0);
                nonSalesList.add(ns);
            }

        } catch (Exception ex) {
            logger.info("Error getEventMasterDetails......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(nonSalesList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getEventAssignNonSalesRule(String user_id, int event_id) {
        logger.info("getEventAssignNonSalesRule method Call..........");
        ArrayList<EventNonSalesRule> nonSalesList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<String> rule_id = new ArrayList();

            ResultSet rs = dbCon.search(con, "SELECT NS_RULE_ID FROM SD_EVENT_TXN "
                    + "WHERE EVENT_ID = " + event_id + " "
                    + "AND USER_ID ='" + user_id + "'");

            while (rs.next()) {
                rule_id.add(rs.getString("NS_RULE_ID"));
            }

            String selectNonSales = "NS_RULE_ID,EVENT_ID,"
                    + "DESCRIPTION,TARGET,"
                    + "POINTS,SALE_TYPE,STATUS";

            ResultSet rs_nonSales = dbCon.search(con, "SELECT (SELECT COUNT(*) FROM SD_EVENT_NON_SALE_RULES WHERE EVENT_ID = " + event_id + ") CNT,"
                    + "" + selectNonSales + " FROM SD_EVENT_NON_SALE_RULES "
                    + "WHERE EVENT_ID = " + event_id + "");

            while (rs_nonSales.next()) {
                EventNonSalesRule ns = new EventNonSalesRule();
                totalrecode = rs_nonSales.getInt("CNT");
                ns.setNonSalesRule(rs_nonSales.getInt("NS_RULE_ID"));;
                ns.setEventId(rs_nonSales.getInt("EVENT_ID"));
                ns.setDescription(rs_nonSales.getString("DESCRIPTION"));
                ns.setTaget(rs_nonSales.getInt("TARGET"));
                ns.setPoint(rs_nonSales.getInt("POINTS"));
                ns.setSalesType(rs_nonSales.getInt("SALE_TYPE"));
                ns.setStatus(0);
                for (String s : rule_id) {
                    if (rs_nonSales.getString("NS_RULE_ID").equals(s)) {
                        ns.setStatus(1);
                    }
                }

                nonSalesList.add(ns);
            }

        } catch (Exception ex) {
            logger.info("Error getEventAssignNonSalesRule......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(nonSalesList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getEventAssignMarksList(int point,
            String user_id,
            String assign_by,
            String point_add_date,
            int status,
            int event_id,
            String stuMsg,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getEventAssignMarksList method Call..........");
        ArrayList<EventTransaction> eventMarkList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            order = DBMapper.eventMarks(order);

            if(!stuMsg.equalsIgnoreCase("all") ){
               stuMsg = stuMsg.toUpperCase();
               stuMsg = stuMsg.substring(0, 1) ;
               if(stuMsg.equals("A")){
                   status = 1;
               }else if (stuMsg.equals("I")){
                   status = 2;
               }else{
                  status = 0; 
               }
            }

            String rulePoint = ((point == 0) ? "" : "AND NON_SALES_POINTS = " + point + " ");
            String user = (user_id.equalsIgnoreCase("all") ? "NVL(USER_ID,'AA') = NVL(USER_ID,'AA') " : "UPPER(USER_ID) LIKE UPPER('%" + user_id + "%')");
            String assignBy = (assign_by.equalsIgnoreCase("all") ? "NVL(ASSIGN_BY,'AA') = NVL(ASSIGN_BY,'AA') " : "UPPER(ASSIGN_BY) LIKE UPPER('%" + assign_by + "%')");
            String poinAddDate = (point_add_date.equalsIgnoreCase("all") ? "NVL(POINTS_ADDED_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(POINTS_ADDED_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(POINTS_ADDED_DATE,'YYYY/MM/DD') = TO_DATE('" + point_add_date + "','YYYY/MM/DD')");
            String Status = ((status == 0) ? "" : "AND STATUS = " + status + " ");
            String eventId = ((event_id == 0) ? "" : "AND E.EVENT_ID = " + event_id + " ");

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY USER_ID" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "USER_ID,"
                    + "POINTS_ADDED_DATE,"
                    + "NON_SALES_POINTS,"
                    + "ASSIGN_BY,STATUS,REMARKS";

            String whereClous = "" + user + "  "
                    + " " + rulePoint + "  "
                    + " AND " + assignBy + " "
                    + " AND " + poinAddDate + " "
                    + " " + Status + "";

            String tables = "(SELECT E.USER_ID,"
                    + "TO_CHAR(E.POINTS_ADDED_DATE,'YYYY/MM/DD') POINTS_ADDED_DATE,"
                    + "SUM(E.NON_SALES_POINTS) NON_SALES_POINTS,"
                    + "ASSIGN_BY,"
                    + "E.STATUS,E.REMARKS "
                    + "FROM SD_EVENT_TXN E "
                    + "WHERE  E.SALES_TYPE = 2  "
                    + "" + eventId + " "
                    + "GROUP BY E.ASSIGN_BY,E.STATUS,E.POINTS_ADDED_DATE,E.USER_ID,E.REMARKS)";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventTransaction et = new EventTransaction();
                totalrecode = rs.getInt("CNT");
                et.setUserId(rs.getString("USER_ID"));
                et.setAssignTo(rs.getString("USER_ID"));
                et.setPointAddDate(rs.getString("POINTS_ADDED_DATE"));
                et.setNonSalePoint(rs.getDouble("NON_SALES_POINTS"));
                et.setAssignBy(rs.getString("ASSIGN_BY"));
                et.setStatus(rs.getInt("STATUS"));
                et.setRemarks(rs.getString("REMARKS"));
                if (rs.getInt("STATUS") == 1) {
                    et.setStatusMsg("Active");
                } else if (rs.getInt("STATUS") == 2) {
                    et.setStatusMsg("Inactive");
                } else {
                    et.setStatusMsg("None");
                }
                eventMarkList.add(et);
            }

        } catch (Exception ex) {
            logger.info("Error in getEventAssignMarksList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventMarkList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getEventAssignMarksDetails(String user_id,
            int event_id) {
        logger.info("getEventAssignMarksDetails method Call..........");
        ArrayList<EventTransaction> eventMarkList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<EventNonSalesRule> eventNonSalesList = new ArrayList<>();
            ResultSet rs_nonsale = dbCon.search(con, "SELECT NS_RULE_ID,"
                    + "EVENT_ID,"
                    + "DESCRIPTION,"
                    + "TARGET,"
                    + "POINTS,"
                    + "STATUS "
                    + "FROM SD_EVENT_NON_SALE_RULES "
                    + "WHERE EVENT_ID = " + event_id + "");

            while (rs_nonsale.next()) {
                EventNonSalesRule nsr = new EventNonSalesRule();
                nsr.setNonSalesRule(rs_nonsale.getInt("NS_RULE_ID"));
                nsr.setEventId(rs_nonsale.getInt("NS_RULE_ID"));
                nsr.setDescription(rs_nonsale.getString("DESCRIPTION"));
                nsr.setTaget(rs_nonsale.getInt("TARGET"));
                nsr.setPoint(rs_nonsale.getInt("POINTS"));
                nsr.setStatus(rs_nonsale.getInt("STATUS"));
                eventNonSalesList.add(nsr);
            }

            String selectColumn = "T.EVENT_ID,"
                    + "T.USER_ID,"
                    + "TO_CHAR(T.POINTS_ADDED_DATE,'YYYY/MM/DD') POINTS_ADDED_DATE,"
                    + "T.STATUS,"
                    + "T.REMARKS,"
                    + "T.NS_RULE_ID";

            String whereClous = "T.EVENT_ID = " + event_id + " "
                    + "AND T.USER_ID = '" + user_id + "' "
                    + "AND SALES_TYPE = 2 "
                    + "GROUP BY T.EVENT_ID,T.USER_ID,T.POINTS_ADDED_DATE,T.STATUS,T.REMARKS,T.NS_RULE_ID";

            String tables = "SD_EVENT_TXN T";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM " + tables + " WHERE  " + whereClous + "");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                ArrayList<EventNonSalesRule> temNonSaleList = new ArrayList<>();
                EventTransaction et = new EventTransaction();
                et.setEventId(rs.getInt("EVENT_ID"));
                et.setUserId(rs.getString("USER_ID"));
                et.setPointAddDate(rs.getString("POINTS_ADDED_DATE"));
                et.setNonSaleId(rs.getInt("NS_RULE_ID"));
                et.setRemarks(rs.getString("REMARKS"));
                et.setStatus(rs.getInt("STATUS"));

                for (EventNonSalesRule nsr : eventNonSalesList) {
                    EventNonSalesRule ensr = new EventNonSalesRule();
                    ensr.setEventId(nsr.getEventId());
                    ensr.setDescription(nsr.getDescription());
                    ensr.setNonSalesRule(nsr.getNonSalesRule());
                    if (rs.getInt("NS_RULE_ID") == ensr.getNonSalesRule()) {
                        ensr.setStatus(1);
                    } else {
                        ensr.setStatus(0);
                    }
                    ensr.setPoint(nsr.getPoint());
                    temNonSaleList.add(ensr);
                }
                et.setEventNonSalesRuleList(temNonSaleList);
                eventMarkList.add(et);
            }

        } catch (Exception ex) {
            logger.info("Error in getEventAssignMarksDetails......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventMarkList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addEventMarks(ArrayList<EventTransaction> EventTransList,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addEventMarks Method Call............");
        int result = 0;
        int event_txnmax = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            if (EventTransList.size() > 0) {

                ResultSet rs_max_event = dbCon.search(con, "SELECT NVL(MAX(TXN_ID),0) MAX "
                        + "FROM SD_EVENT_TXN ");
                while (rs_max_event.next()) {
                    event_txnmax = rs_max_event.getInt("MAX");
                }

                String insertColumn = "TXN_ID,EVENT_ID,"
                        + "USER_ID,POINTS_ADDED_DATE,"
                        + "NS_RULE_ID,NON_SALES_POINTS,"
                        + "ASSIGN_BY,STATUS,"
                        + "REMARKS,SALES_TYPE,"
                        + "USER_INSERTED,DATE_INSERTED";

                PreparedStatement ps_in_event_marks = dbCon.prepare(con, "INSERT INTO SD_EVENT_TXN(" + insertColumn + ") "
                        + "VALUES(?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                        + "?,?,?,?,"
                        + "?,2,?,SYSDATE)");

                for (EventTransaction etl : EventTransList) {

                    event_txnmax = event_txnmax + 1;
                    ps_in_event_marks.setInt(1, event_txnmax);
                    ps_in_event_marks.setInt(2, etl.getEventId());
                    ps_in_event_marks.setString(3, etl.getUserId());
                    ps_in_event_marks.setString(4, etl.getPointAddDate());
                    ps_in_event_marks.setInt(5, etl.getNonSaleId());
                    ps_in_event_marks.setDouble(6, etl.getNonSalePoint());
                    ps_in_event_marks.setString(7, etl.getAssignBy());
                    ps_in_event_marks.setInt(8, etl.getStatus());
                    ps_in_event_marks.setString(9, etl.getRemarks());
                    ps_in_event_marks.setString(10, user_id);
                    ps_in_event_marks.addBatch();
                }

                ps_in_event_marks.executeBatch();
                result = 1;

            }

        } catch (Exception ex) {
            logger.info("Error in addEventMarks Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Event Marks Assign Successfully");
            logger.info("Event Marks Assign Successfully............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Marks Assign");
            logger.info("Error in Marks Assign..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editEventMarks(ArrayList<EventTransaction> EventTransList,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editEventMarks Method Call............");
        int result = 0;
        int event_txnmax = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            if (EventTransList.size() > 0) {

                int eventId = 0;
                String userid = null;

                for (EventTransaction et : EventTransList) {
                    eventId = et.getEventId();
                    userid = et.getUserId();
                }

                dbCon.save(con, "DELETE FROM SD_EVENT_TXN WHERE EVENT_ID = " + eventId + " "
                        + "AND USER_ID  = '" + userid + "' "
                        + "AND SALES_TYPE = 2");

                ResultSet rs_max_event = dbCon.search(con, "SELECT NVL(MAX(TXN_ID),0) MAX "
                        + "FROM SD_EVENT_TXN");
                while (rs_max_event.next()) {
                    event_txnmax = rs_max_event.getInt("MAX");
                }

                String insertColumn = "TXN_ID,EVENT_ID,"
                        + "USER_ID,POINTS_ADDED_DATE,"
                        + "NS_RULE_ID,NON_SALES_POINTS,"
                        + "ASSIGN_BY,STATUS,"
                        + "REMARKS,SALES_TYPE,"
                        + "USER_INSERTED,DATE_INSERTED";

                PreparedStatement ps_in_event_marks = dbCon.prepare(con, "INSERT INTO SD_EVENT_TXN(" + insertColumn + ") "
                        + "VALUES(?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                        + "?,?,?,?,"
                        + "?,2,?,SYSDATE)");

                for (EventTransaction etl : EventTransList) {

                    event_txnmax = event_txnmax + 1;
                    ps_in_event_marks.setInt(1, event_txnmax);
                    ps_in_event_marks.setInt(2, etl.getEventId());
                    ps_in_event_marks.setString(3, etl.getUserId());
                    ps_in_event_marks.setString(4, etl.getPointAddDate());
                    ps_in_event_marks.setInt(5, etl.getNonSaleId());
                    ps_in_event_marks.setDouble(6, etl.getNonSalePoint());
                    ps_in_event_marks.setString(7, etl.getAssignBy());
                    ps_in_event_marks.setInt(8, etl.getStatus());
                    ps_in_event_marks.setString(9, etl.getRemarks());
                    ps_in_event_marks.setString(10, user_id);
                    ps_in_event_marks.addBatch();
                }

                ps_in_event_marks.executeBatch();
                result = 1;

            }
        } catch (Exception ex) {
            logger.info("Error in editEventMarks Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Event Marks Assign Update Successfully");
            logger.info("Event Marks Assign Update Successfully............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Marks Assign Update");
            logger.info("Error in Marks Assign Update..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getEventParticipant(int event_id) {
        logger.info("getEventParticipant method Call..........");
        ArrayList<EventBussinessList> eventParticipantList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<String> asingEvent = new ArrayList<>();

            ResultSet rs_non_sale = dbCon.search(con, "SELECT DISTINCT USER_ID FROM SD_EVENT_TXN "
                    + "WHERE STATUS = 1 "
                    + "AND SALES_TYPE = 2");
            while (rs_non_sale.next()) {
                asingEvent.add(rs_non_sale.getString("USER_ID"));
            }

            String selectColumn = "EVENT_PART_ID,SEQ_NO,BIS_ID,USER_ID,STATUS";

            String whereClous = "EVENT_ID IN (" + event_id + ")";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + "  "
                    + "FROM SD_EVENT_PARTICIPANT_DTL "
                    + "WHERE EVENT_PART_ID IN (SELECT EVENT_PART_ID FROM SD_EVENT_PARTICIPANTS WHERE " + whereClous + ") "
                    + "AND STATUS = 1");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventBussinessList ep = new EventBussinessList();
                ep.setEventPartId(rs.getInt("EVENT_PART_ID"));
                ep.setSeqNo(rs.getInt("SEQ_NO"));
                ep.setBisId(rs.getInt("BIS_ID"));
                ep.setUserId(rs.getString("USER_ID"));
                for (String s : asingEvent) {
                    if (rs.getString("USER_ID").equals(s)) {
                        ep.setUsedStatus(1);
                    }
                }
                ep.setStatus(rs.getInt("STATUS"));
                eventParticipantList.add(ep);
            }

        } catch (Exception ex) {
            logger.info("Error in getEventParticipant......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = eventParticipantList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventParticipantList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;

    }

}
