/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.mail.MessagingException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.dms.ldap.util.LdapGroup;
import org.dms.ldap.util.LdapUser;
import org.dms.ldap.util.LoginWrapper;
import org.dms.ldap.util.ResetUserResponseWrapper;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Group;
import org.dms.ws.singer.entities.LdapUserEnty;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserGroup;
import org.dms.ws.singer.utils.EmailSender;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

/**
 *
 * @author SDU
 */
public class UserController {

    public static ResponseWrapper validateUser(LdapUser user_info, String system) {

        logger.info("validateUser Method Call.....................");
        ResponseWrapper urw = null;
        try {

            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            logger.info("Call LDAP Login Method...........");
            urw = LDAPWSController.login(user_info.getUserId(), user_info.getPassword(), system);

            String qry = " SELECT B.BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES B, SD_USER_BUSINESS_STRUCTURES U WHERE B.BIS_ID=U.BIS_ID AND U.USER_ID='" + user_info.getUserId() + "' ";
            ResultSet rs = dbCon.search(con, qry);
            if (rs.next()) {
                urw.setTotalRecords(rs.getInt("BIS_STRU_TYPE_ID"));
            }

            dbCon.ConectionClose(con);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error validateUser Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");

        return urw;

    }

    public static LoginWrapper validateUserAndroidP3(LdapUser user_info, String system, String imei) {

        logger.info("validateUser Method Call.....................");
        LoginWrapper urw = null;

        String bisId = null;

        try {

            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            logger.info("Call LDAP Login Method...........");
            urw = LDAPWSController.loginAndroid(user_info.getUserId(), user_info.getPassword(), system);

            String qry = " SELECT B.BIS_STRU_TYPE_ID, B.ALLOW_OFFLINE FROM SD_BUSINESS_STRUCTURES B, SD_USER_BUSINESS_STRUCTURES U WHERE B.BIS_ID=U.BIS_ID AND U.USER_ID='" + user_info.getUserId() + "' ";
            ResultSet rs = dbCon.search(con, qry);

            System.out.println("Login query >>>>>>> " + qry);

            Date d1 = new Date();
            SimpleDateFormat df = new SimpleDateFormat("ddMMYYYY");
            String formattedDate = df.format(d1);

            ArrayList<String> extraData = new ArrayList<>();

            if (rs.next()) {
                urw.setTotalRecords(rs.getInt("BIS_STRU_TYPE_ID"));

                extraData.add(String.valueOf(rs.getInt("ALLOW_OFFLINE")));
                extraData.add(formattedDate);

            }

//            String queryVer = "SELECT * "
//                    + "FROM SD_VERSION_RELEASE";
            String qryForVersion = "SELECT * "
                    + "FROM SD_VERSION_RELEASE "
                    + "WHERE STATUS = 1";

            ResultSet rsVer = dbCon.search(con, qryForVersion);

//            ResultSet rsVer = dbCon.search(con, queryVer);
            String version = null;
            String link = null;
            Date effectiveDate = null;
            String formattedEffectiveDate = null;
            String isComplete = "";

            while (rsVer.next()) {
                version = rsVer.getString("VERSION_ID");
                link = rsVer.getString("DOWNLOAD_LINK");
                effectiveDate = rsVer.getDate("EFFECTIVE_DATE");

//                SimpleDateFormat sdate = new SimpleDateFormat("ddMMYYYYMMdd");
                SimpleDateFormat sdate = new SimpleDateFormat("YYYYMMdd");
                formattedEffectiveDate = sdate.format(effectiveDate);

            }

            extraData.add(version);
            extraData.add(link);

            System.out.println("NEW VERSION >>> " + link);

            String distQry = "  SELECT *    "
                    + " FROM (  "
                    + " SELECT B1.* "
                    + " FROM SD_BUSINESS_STRUCTURES B1  "
                    + " START WITH BIS_ID = (   "
                    + " SELECT B.BIS_ID "
                    + " FROM SD_BUSINESS_STRUCTURES B, SD_USER_BUSINESS_STRUCTURES U    "
                    + " WHERE B.BIS_ID=U.BIS_ID     "
                    + " AND U.USER_ID='" + user_info.getUserId() + "' ) "
                    + " CONNECT BY PRIOR B1.PARENT_BIS_ID = B1.BIS_ID   "
                    + " ) A "
                    + " WHERE A.BIS_STRU_TYPE_ID=3  ";

            System.out.println("User query " + distQry);

            ResultSet rs1 = dbCon.search(con, distQry);

            if (rs1.next()) {

                urw.setDistributorCode(rs1.getString("BIS_ID"));
                urw.setDistributorName(rs1.getString("BIS_NAME"));
                urw.setDistributorAddress(rs1.getString("ADDRESS"));
                urw.setDistributorTelephone(rs1.getString("CONTACT_NO"));

            }

            String getBisId = "SELECT BIS_ID "
                    + "FROM SD_USER_BUSINESS_STRUCTURES B "
                    + "WHERE B.USER_ID = '" + user_info.getUserId() + "'";

            System.out.println("Get BIS ID query " + getBisId);

            ResultSet rsBisId = dbCon.search(con, getBisId);

            while (rsBisId.next()) {
                bisId = rsBisId.getString("BIS_ID");
            }

            String qryForChk = " SELECT * FROM (SELECT * "
                    + "FROM SD_DAY_END_LOG "
                    + "WHERE BIS_ID = '" + bisId + "' "
                    + "ORDER BY DATE_START DESC) "
                    + "WHERE ROWNUM = 1";

            System.out.println("Query for check day end " + qryForChk);

            ResultSet rsForChk = dbCon.search(con, qryForChk);
            String imeiFromDb = null;
            String status = "no";

            System.out.println("getting results from bisID and status...");

            while (rsForChk.next()) {
                imeiFromDb = rsForChk.getString("IMEI_NO");
                status = rsForChk.getString("STATUS");
            }

            if (status.equals("complete") || status.equals("no")) {

                String qryforChkOfflineEnable = "SELECT ALLOW_OFFLINE "
                        + "FROM SD_BUSINESS_STRUCTURES "
                        + "WHERE BIS_ID='" + bisId + "'";

                ResultSet rsForOfflineChk = dbCon.search(con, qryforChkOfflineEnable);

                int isAllow = 0;

                while (rsForOfflineChk.next()) {

                    isAllow = rsForOfflineChk.getInt("ALLOW_OFFLINE");

                }

                if (isAllow == 1) {

                    String column = "BIS_ID, DATE_START,IMEI_NO,STATUS";
                    String valColumn = "? , SYSDATE , ?, ?";
                    String qryAddDayEndRec = "INSERT INTO SD_DAY_END_LOG (" + column + ") "
                            + "VALUES (" + valColumn + ")";

                    System.out.println("DAY END QRY " + qryAddDayEndRec);

                    PreparedStatement dayEndRecInsert = dbCon.prepare(con, qryAddDayEndRec);

                    dayEndRecInsert.setInt(1, Integer.parseInt(bisId));
                    dayEndRecInsert.setString(2, imei);
                    dayEndRecInsert.setString(3, "pending");

                    dayEndRecInsert.executeUpdate();

                    isComplete = "1";
                } else {

                    System.out.println("DSR NOT ALLOWED TO OFFLINE............");

                }
            } else if (status.equals("pending")) {

                if (imeiFromDb.equals(imei)) {
                    isComplete = "5";
                } else {
                    isComplete = "4";
                }

            } else {
                isComplete = "3";
            }

            extraData.add(isComplete);

            extraData.add(formattedEffectiveDate);

            Date dForValidate = new Date();
            SimpleDateFormat dfValidate = new SimpleDateFormat("YYYYMMdd");
            String formattedDateValidate = dfValidate.format(dForValidate);
            
            extraData.add(formattedDateValidate);

            urw.setExceptionMessages(extraData);

            dbCon.ConectionClose(con);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error validateUser Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");

        return urw;

    }

    public static LoginWrapper validateUserAndroid(LdapUser user_info, String system) {

        logger.info("validateUser Method Call.....................");
        LoginWrapper urw = null;
        try {

            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            logger.info("Call LDAP Login Method...........");
            urw = LDAPWSController.loginAndroid(user_info.getUserId(), user_info.getPassword(), system);

            String qry = " SELECT B.BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES B, SD_USER_BUSINESS_STRUCTURES U WHERE B.BIS_ID=U.BIS_ID AND U.USER_ID='" + user_info.getUserId() + "' ";
            ResultSet rs = dbCon.search(con, qry);
            if (rs.next()) {
                urw.setTotalRecords(rs.getInt("BIS_STRU_TYPE_ID"));
            }

            String distQry = "  SELECT *    "
                    + " FROM (  "
                    + " SELECT B1.* "
                    + " FROM SD_BUSINESS_STRUCTURES B1  "
                    + " START WITH BIS_ID = (   "
                    + " SELECT B.BIS_ID "
                    + " FROM SD_BUSINESS_STRUCTURES B, SD_USER_BUSINESS_STRUCTURES U    "
                    + " WHERE B.BIS_ID=U.BIS_ID     "
                    + " AND U.USER_ID='" + user_info.getUserId() + "' ) "
                    + " CONNECT BY PRIOR B1.PARENT_BIS_ID = B1.BIS_ID   "
                    + " ) A "
                    + " WHERE A.BIS_STRU_TYPE_ID=3  ";

            ResultSet rs1 = dbCon.search(con, distQry);
            if (rs1.next()) {
                urw.setDistributorCode(rs1.getString("BIS_ID"));
                urw.setDistributorName(rs1.getString("BIS_NAME"));
                urw.setDistributorAddress(rs1.getString("ADDRESS"));
                urw.setDistributorTelephone(rs1.getString("CONTACT_NO"));
            }

            dbCon.ConectionClose(con);
        } catch (Exception ex) {
            logger.info("Error validateUser Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");

        return urw;

    }

    public static ResponseWrapper addUser(User userinfo, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        logger.info("addUser Method Call.............................");
        int result = 0;
        ResponseWrapper urw = null;
        String eMail = null;
        String password = null;
        String firname = null;
        String username = null;
        ArrayList<HashMap> userList = null;
        try {
            logger.info("Call LDAP addUserWithGroups Method....");
            urw = LDAPWSController.addUserWithGroups(userinfo, user_id, room, department, branch, countryCode, division, organaization, system);

            logger.info("-----------------OutPut------------------------------------");
            ObjectMapper mapper = new ObjectMapper();

            logger.info(mapper.writeValueAsString(urw));

        } catch (Exception ex) {
            logger.info("Error Add User Method..:" + ex.toString());
        }
        if (urw.getData() == null) {

        } else {
            logger.info("-----------E Mail Send-------------------");
            userList = urw.getData();

            for (HashMap us : userList) {

                eMail = (String) us.get("email");
                //System.out.println("Email    "+eMail);
                password = (String) us.get("password");
                firname = (String) us.get("firstName");
                username = (String) us.get("userId");

                //System.out.println("Password    "+password);
                try {
                    logger.info("Send Genarated Password to -" + eMail);
                    EmailSender.sendHTMLMail(eMail, password, username, firname);
                } catch (MessagingException ex) {
                    logger.info("Error in E-mail Sending........");
                }

            }
        }

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return urw;
    }

    public static ResponseWrapper getUsers(String key,
            String value,
            String status,
            String start,
            String limit,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {
        logger.info("getUsersMethod Call.....");
        ResponseWrapper urs = null;
        //int totalrecode = 0;
        ArrayList<User> userInfo = new ArrayList<>();
        try {

            urs = LDAPWSController.getAllUsers(status, start, limit, key, value, user_id, room, department, branch, countryCode, division, organaization, system);
            ArrayList<HashMap> userList = urs.getData();

            for (HashMap us : userList) {

                User u = new User();
                u.setUserId((String) us.get("userId"));
                u.setFirstName((String) us.get("firstName"));
                u.setLastName((String) us.get("lastName"));
                u.setCommonName((String) us.get("commonName"));
                u.setDesignation((String) us.get("designation"));
                u.setTelephoneNumber((String) us.get("telephoneNumber"));
                u.setEmail((String) us.get("email"));
                u.setCreatedOn((String) us.get("createdOn"));
                u.setInactivedOn((String) us.get("inactivedOn"));
                u.setStatus((String) us.get("status"));
                u.setExtraParams((String) us.get("extraParams"));
                u.setExtraParams((String) us.get("extraParams"));
                userInfo.add(u);
            }

        } catch (Exception ex) {
            logger.info("Error  :" + ex.getMessage());
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urs;
    }

    public static ResponseWrapper editUser(User userinfo, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {
        logger.info("editUser Method Call.....................");
        ResponseWrapper urw = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            urw = LDAPWSController.modifyUserWithGroups(userinfo, user_id, room, department, branch, countryCode, division, organaization, system);

            if (userinfo.getStatus().equals("9")) {
                try {
                    PreparedStatement ps_up_event = dbCon.prepare(con, "UPDATE SD_EVENT_PARTICIPANT_DTL "
                            + "SET STATUS = 9,USER_MODIFIED = ?,DATE_MODIFIED = SYSDATE "
                            + "WHERE USER_ID = ?");

                    ps_up_event.setString(1, user_id);
                    ps_up_event.setString(2, userinfo.getUserId());
                    ps_up_event.executeUpdate();
                    logger.info("Event Paricipant Inativate.................");

                    PreparedStatement ps_up_bussiness = dbCon.prepare(con, "UPDATE SD_USER_BUSINESS_STRUCTURES SET STATUS = 9,USER_MODIFIED = ?,DATE_MODIFIED = SYSDATE "
                            + "WHERE  USER_ID = ?");

                    ps_up_bussiness.setString(1, user_id);
                    ps_up_bussiness.setString(2, userinfo.getUserId());
                    ps_up_bussiness.executeUpdate();
                    logger.info("Bussiness Structhure User Inativate.................");
                } catch (Exception ex) {
                    System.out.println("Error...." + ex.getMessage());
                }
            } else {
                try {
                    PreparedStatement ps_up_event = dbCon.prepare(con, "UPDATE SD_EVENT_PARTICIPANT_DTL "
                            + "SET STATUS = 1,USER_MODIFIED = ?,DATE_MODIFIED = SYSDATE "
                            + "WHERE USER_ID = ?");

                    ps_up_event.setString(1, user_id);
                    ps_up_event.setString(2, userinfo.getUserId());
                    ps_up_event.executeUpdate();
                    logger.info("Event Paricipant Inativate.................");

                    PreparedStatement ps_up_bussiness = dbCon.prepare(con, "UPDATE SD_USER_BUSINESS_STRUCTURES "
                            + "SET STATUS = 1,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE  USER_ID = ?");

                    ps_up_bussiness.setString(1, user_id);
                    ps_up_bussiness.setString(2, userinfo.getUserId());
                    ps_up_bussiness.executeUpdate();
                    logger.info("Bussiness Structhure User Inativate.................");
                } catch (Exception ex) {
                    System.out.println("Error...." + ex.getMessage());
                }
            }

        } catch (Exception ex) {
            logger.info("Error editUser Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        dbCon.ConectionClose(con);
        return urw;
    }

    public static ResponseWrapper resetUserPassword(String userid, String e_mail, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        logger.info("resetUserPassword Method Call.....................");
        ResponseWrapper urw = null;
        String eMail = null;
        String password = null;
        String frsname = null;
        String username = null;
        try {

            urw = LDAPWSController.generateResetPassowrd(userid, e_mail, user_id, room, department, branch, countryCode, division, organaization, system);

            ArrayList<HashMap> userList = urw.getData();

            for (HashMap us : userList) {

                eMail = (String) us.get("email");
                password = (String) us.get("password");
                username = (String) us.get("userId");
                frsname = (String) us.get("firstName");
            }
            // logger.info("Email     " + eMail);
            System.out.println("Username    " + username);
            System.out.println("Fname   " + frsname);
            try {
                logger.info("Send Genarated Password to -" + eMail);
                EmailSender.sendHTMLMail(eMail, password, username, username);
            } catch (Exception ex) {
                logger.info("Error in E-mail Sending........");
            }

        } catch (Exception ex) {
            logger.info("Error resetUserPassword Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urw;
    }

    public static ResponseWrapper editUserWithoutGroup(User userinfo, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {
        logger.info("editUser Method Call.....................");
        ResponseWrapper urw = null;
        try {

            urw = LDAPWSController.modifyUser(userinfo, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            logger.info("Error editUser Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urw;
    }

    public static ResponseWrapper changePassword(User userinfo, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {
        logger.info("Change Password Method Call.....................");
        ResponseWrapper urw = null;
        try {
            logger.info("Call LDAP changeUserPassowrd.....................");
            urw = LDAPWSController.changeUserPassowrd(userinfo, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            logger.info("Error Change Password Method...........");
        }
        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urw;
    }

    public static ResponseWrapper logoutUser(String username, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        logger.info("logoutUser Method Call.....................");
        ResponseWrapper urw = null;
        try {

            logger.info("Call LDAP Logout Method...........");
            urw = LDAPWSController.logout(username, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            logger.info("Error logoutUser Method...........");
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");

        return urw;

    }

    public static ResponseWrapper getUserGroups(String userid, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {
        logger.info("getUserGroups Call.....");
        ResponseWrapper urs = null;
        //int totalrecode = 0;
        // ArrayList<User> userInfo = new ArrayList<>();
        try {
            logger.info("Call LDAP getAllUserGroups Method.......");
            urs = LDAPWSController.getUserGroups(userid, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            logger.info("Error in getUserGroups Method  :" + ex.getMessage());
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urs;
    }

    public static ResponseWrapper getUsers123(String userId,
            String firstName,
            String lastName,
            String commonName,
            String designation,
            String telephoneNumber,
            String email,
            String createdOn,
            String inactivedOn,
            String status,
            String extraparm,
            String start,
            String limit,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {
        logger.info("getUsersMethod Call.....");
        ResponseWrapper urs = null;
        ArrayList<User> userInfo = new ArrayList<>();
        try {

            System.out.println("Extra   " + extraparm);

            HashMap<String, String> searchParams = new HashMap<>();
            if (userId.equals("all")) {

            } else {
                searchParams.put("uid", userId);
            }

            if (firstName.equals("all")) {

            } else {
                searchParams.put("cn", firstName);
            }

            if (lastName.equals("all")) {

            } else {
                searchParams.put("sn", lastName);
            }

            if (commonName.equals("all")) {

            } else {
                searchParams.put("displayName", commonName);
            }

            if (designation.equals("all")) {

            } else {
                searchParams.put("title", designation);
            }

            if (telephoneNumber.equals("all")) {

            } else {
                searchParams.put("telephoneNumber", telephoneNumber);
            }

            if (email.equals("all")) {

            } else {
                searchParams.put("email", email);
            }

            if (createdOn.equals("all")) {

            } else {
                searchParams.put("createdOn", createdOn);
            }

            if (inactivedOn.equals("all")) {

            } else {
                searchParams.put("inactivedOn", inactivedOn);
            }

            if (status.equals("all")) {

            } else {
                searchParams.put("status", status);
            }

            if (extraparm.equals("all")) {

            } else {
                searchParams.put("extraParams", extraparm);
            }

            if (searchParams.isEmpty()) {
                searchParams.put("all", "all");
            }

//            System.out.println(user_id);
//            System.out.println(room);
//            System.out.println(department);
//            System.out.println(branch);
//            System.out.println(countryCode);
//            System.out.println(division);
//            System.out.println(organaization);
//            System.out.println(system);
            urs = LDAPWSController.getAllUsers123(status, start, limit, searchParams, user_id, room, department, branch, countryCode, division, organaization, system);
            ArrayList<HashMap> userList = urs.getData();
            System.out.println("Size   " + userList.size());

            for (HashMap us : userList) {

                User u = new User();
                u.setUserId((String) us.get("userId"));
                u.setFirstName((String) us.get("firstName"));
                u.setLastName((String) us.get("lastName"));
                u.setCommonName((String) us.get("commonName"));
                u.setDesignation((String) us.get("designation"));
                u.setTelephoneNumber((String) us.get("telephoneNumber"));
                u.setEmail((String) us.get("email"));
                u.setCreatedOn((String) us.get("createdOn"));
                u.setInactivedOn((String) us.get("inactivedOn"));
                u.setStatus((String) us.get("status"));
                u.setExtraParams((String) us.get("extraParams"));
                u.setExtraParams((String) us.get("extraParams"));
                userInfo.add(u);
            }

        } catch (Exception ex) {
            logger.info("Error  :" + ex.getMessage());
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urs;
    }

    public int DBCheck() {
        int status = 1;
        try {
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();
            if (con == null) {
                status = 9;
            }
        } catch (Exception e) {
            status = 9;
            e.printStackTrace();
        }

        return status;
    }

}
