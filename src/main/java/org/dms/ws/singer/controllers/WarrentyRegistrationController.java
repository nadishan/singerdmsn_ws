/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;
import javax.mail.MessagingException;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.WarrentyRegistration;
import org.dms.ws.singer.utils.DBMapper;
import org.dms.ws.singer.utils.EmaiSendConfirmCode;
import org.dms.ws.singer.utils.CommonTask;

/**
 *
 * @author SDU
 */
public class WarrentyRegistrationController {

    public static MessageWrapper getWarrentyRegistration(int bisId,
            int war_ref_no,
            String imei_no,
            String cus_nic,
            String cus_name,
            String contact_no,
            String date_of_birth,
            double selling_price,
            int modle_no,
            String modle_desc,
            String product,
            String fromdate,
            String todate,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWarrentyRegistration Method Call....................");
        ArrayList<WarrentyRegistration> warrentRegistrationList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.warrentyRegistration(order);
            String BIS_ID = (bisId == 0 ? "" : " AND W.BIS_ID = " + bisId + " ");
            String warRefNo = (war_ref_no == 0 ? "" : "AND WRR_REG_REF_NO = " + war_ref_no + "");
            String imeiNo = (imei_no.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei_no + "%')");
            String customerNIC = (cus_nic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + cus_nic + "%')");
            String customerName = (cus_name.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + cus_name + "%')");
            String contactNo = (contact_no.equalsIgnoreCase("all") ? "NVL(W.CONTACT_NO,'AA') = NVL(W.CONTACT_NO,'AA')" : "UPPER(W.CONTACT_NO) LIKE UPPER('%" + contact_no + "%')");
            String sellingPrice = (selling_price == 0 ? "" : "AND W.SELLING_PRICE = '" + selling_price + "'");
            String dateOfBirth = (date_of_birth.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_BIRTH,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_BIRTH,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_BIRTH = TO_DATE('" + date_of_birth + "','YYYY/MM/DD')");
            String modleNo = (modle_no == 0 ? "" : "AND M.MODEL_NO = " + modle_no + "");
            String modleDesc = (modle_desc.equalsIgnoreCase("all") ? "NVL(M.MODEL_DESCRIPTION,'AA') = NVL(M.MODEL_DESCRIPTION,'AA')" : "UPPER(M.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(D.PRODUCT,'AA') = NVL(D.PRODUCT,'AA')" : "UPPER(D.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WRR_REG_REF_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "W.WRR_REG_REF_NO WRR_REG_REF_NO,"
                    + "W.IMEI_NO IMEI_NO,"
                    + "W.CUSTOMER_NIC CUSTOMER_NIC,"
                    + "W.CUSTOMER_NAME CUSTOMER_NAME,"
                    + "W.CONTACT_NO CONTACT_NO,"
                    + "TO_CHAR(W.DATE_OF_BIRTH,'YYYY/MM/DD') DATE_OF_BIRTH,"
                    + "W.SELLING_PRICE SELLING_PRICE,"
                    + "D.PRODUCT PRODUCT,"
                    + "M.MODEL_NO MODEL_NO,"
                    + "M.MODEL_DESCRIPTION MODEL_DESCRIPTION,"
                    + "W.BIS_ID BIS_ID,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') REGISTER_DATE,"
                    + "(ROUND(((W.DATE_INSERTED) + "
                    + "(SELECT PERIOD FROM SD_WARRANTY_SCHEMES WHERE SCHEME_ID = W.WARANTY_TYPE)+"
                    + " (NVL((SELECT NVL(A.EXTENDED_DAYS,0) FROM SD_WARRANTY_ADJUSTMENT A WHERE IMEI = W.IMEI_NO),0)))-SYSDATE)) REMAIN_DAYS";

            String whereClous = " D.IMEI_NO = W.IMEI_NO "
                    + " AND D.MODEL_NO = M.MODEL_NO "
                    + "" + warRefNo + ""
                    + " " + BIS_ID + " "
                    + " AND " + imeiNo + " "
                    + " AND " + customerNIC + " "
                    + " AND " + customerName + " "
                    + " AND " + contactNo + " "
                    + " " + sellingPrice + "  "
                    + " AND " + dateOfBirth + " "
                    + " " + modleNo + " "
                    + " AND " + modleDesc + " "
                    + " AND " + Product + " "
                    + " " + Range + " ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WarrentyRegistration wr = new WarrentyRegistration();
                totalrecode = rs.getInt("CNT");
                wr.setWarrntyRegRefNo(rs.getInt("WRR_REG_REF_NO"));
                wr.setImeiNo(rs.getString("IMEI_NO"));
                wr.setCustomerNIC(rs.getString("CUSTOMER_NIC"));
                wr.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wr.setContactNo(rs.getString("CONTACT_NO"));
                wr.setDateOfBirth(rs.getString("DATE_OF_BIRTH"));
                wr.setSellingPrice(rs.getDouble("SELLING_PRICE"));
                wr.setProduct(rs.getString("PRODUCT"));
                wr.setModleNo(rs.getInt("MODEL_NO"));
                wr.setModleDescription(rs.getString("MODEL_DESCRIPTION"));
                wr.setBisId(rs.getInt("BIS_ID"));
                wr.setRegisterDate(rs.getString("REGISTER_DATE"));
                wr.setRemainsDays(rs.getInt("REMAIN_DAYS"));
                warrentRegistrationList.add(wr);
            }

        } catch (Exception ex) {
            logger.info("Error getWarrentyRegistration Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(warrentRegistrationList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getDsrWarrentyRegistration(int bisId,
            int war_ref_no,
            String imei_no,
            String cus_nic,
            String cus_name,
            String contact_no,
            String date_of_birth,
            double selling_price,
            int modle_no,
            String modle_desc,
            String product,
            String fromdate,
            String todate,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getDSRWarrentyRegistration Method Call....................");
        ArrayList<WarrentyRegistration> warrentRegistrationList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.warrentyRegistration(order);
            String BIS_ID = (bisId == 0 ? "" : " AND W.BIS_ID = " + bisId + " ");
            String warRefNo = (war_ref_no == 0 ? "" : "AND WRR_REG_REF_NO = " + war_ref_no + "");
            String imeiNo = (imei_no.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei_no + "%')");
            String customerNIC = (cus_nic.equalsIgnoreCase("all") ? "" : " AND UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + cus_nic + "%')");
            String customerName = (cus_name.equalsIgnoreCase("all") ? "" : " AND UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + cus_name + "%')");
            String contactNo = (contact_no.equalsIgnoreCase("all") ? "" : " AND UPPER(W.CONTACT_NO) LIKE UPPER('%" + contact_no + "%')");
            String sellingPrice = (selling_price == 0 ? "" : " AND W.SELLING_PRICE = '" + selling_price + "'");
            String dateOfBirth = (date_of_birth.equalsIgnoreCase("all") ? "" : " AND W.DATE_OF_BIRTH = TO_DATE('" + date_of_birth + "','YYYY/MM/DD')");
            String modleNo = (modle_no == 0 ? "" : "AND M.MODEL_NO = " + modle_no + "");
            String modleDesc = (modle_desc.equalsIgnoreCase("all") ? "" : " AND UPPER(M.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "" : " AND UPPER(D.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WRR_REG_REF_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "W.WRR_REG_REF_NO WRR_REG_REF_NO,"
                    + "W.IMEI_NO IMEI_NO,"
                    + "W.CUSTOMER_NIC CUSTOMER_NIC,"
                    + "W.CUSTOMER_NAME CUSTOMER_NAME,"
                    + "W.CONTACT_NO CONTACT_NO,"
                    + "TO_CHAR(W.DATE_OF_BIRTH,'YYYY/MM/DD') DATE_OF_BIRTH,"
                    + "W.SELLING_PRICE SELLING_PRICE,"
                    + "D.PRODUCT PRODUCT,"
                    + "M.MODEL_NO MODEL_NO,"
                    + "M.MODEL_DESCRIPTION MODEL_DESCRIPTION,"
                    + "W.BIS_ID BIS_ID,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') REGISTER_DATE,"
                    + "(ROUND(((W.DATE_INSERTED) + "
                    + "(SELECT PERIOD FROM SD_WARRANTY_SCHEMES WHERE SCHEME_ID = W.WARANTY_TYPE)+"
                    + " (NVL((SELECT NVL(A.EXTENDED_DAYS,0) FROM SD_WARRANTY_ADJUSTMENT A WHERE IMEI = W.IMEI_NO),0)))-SYSDATE)) REMAIN_DAYS";

            String whereClous = " D.IMEI_NO = W.IMEI_NO "
                    + " AND D.MODEL_NO = M.MODEL_NO "
                    + "" + warRefNo + ""
                    + " " + BIS_ID + " "
                    + " AND " + imeiNo + " "
                    + customerNIC + " "
                    + contactNo + " "
                    + " " + sellingPrice + "  "
                    + dateOfBirth + " "
                    + " " + modleNo + " "
                    + modleDesc + " "
                    + Product + " "
                    + " " + Range + " ";

            System.out.println("Query >>>>> SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WarrentyRegistration wr = new WarrentyRegistration();
                totalrecode = rs.getInt("CNT");
                wr.setWarrntyRegRefNo(rs.getInt("WRR_REG_REF_NO"));
                wr.setImeiNo(rs.getString("IMEI_NO"));
                wr.setCustomerNIC(rs.getString("CUSTOMER_NIC"));
                wr.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wr.setContactNo(rs.getString("CONTACT_NO"));
                wr.setDateOfBirth(rs.getString("DATE_OF_BIRTH"));
                wr.setSellingPrice(rs.getDouble("SELLING_PRICE"));
                wr.setProduct(rs.getString("PRODUCT"));
                wr.setModleNo(rs.getInt("MODEL_NO"));
                wr.setModleDescription(rs.getString("MODEL_DESCRIPTION"));
                wr.setBisId(rs.getInt("BIS_ID"));
                wr.setRegisterDate(rs.getString("REGISTER_DATE"));
                wr.setRemainsDays(rs.getInt("REMAIN_DAYS"));
                warrentRegistrationList.add(wr);
            }

        } catch (Exception ex) {
            logger.info("Error getWarrentyRegistration Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(warrentRegistrationList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addWarrentyRegistration(WarrentyRegistration wr_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("addWarrentyRegistration Mehtod Call.......");
        int result = 0;
        int warrenty_reg = 0;
        int customer_reg = 0;
        int master_cnt = 0;
        int con_code = 0;
        int warany_reg_num = 0;
        int warrant_schem_id = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_master_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + wr_info.getImeiNo() + "'");

            while (rs_master_cnt.next()) {
                master_cnt = rs_master_cnt.getInt("CNT");
            }
            rs_master_cnt.close();

            if (master_cnt > 0) {

                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_WARRENTY_REGISTRATION "
                        + "WHERE IMEI_NO = '" + wr_info.getImeiNo() + "'");

                while (rs_cnt.next()) {
                    warrenty_reg = rs_cnt.getInt("CNT");
                }
                rs_cnt.close();

                if (warrenty_reg > 0) {
                    result = 2;
                } else {

                    try {

                        ResultSet rs_wrt_reg = dbCon.search(con, "SELECT NVL(MAX(WRR_REG_REF_NO),0)+1 MAX "
                                + "FROM  SD_WARRENTY_REGISTRATION");

                        while (rs_wrt_reg.next()) {
                            warany_reg_num = rs_wrt_reg.getInt("MAX");
                        }
                        rs_wrt_reg.close();

                        ResultSet rs_schem = dbCon.search(con, "SELECT M.WRN_SCHEME_ID "
                                + "FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M "
                                + "WHERE D.MODEL_NO = M.MODEL_NO "
                                + "AND D.IMEI_NO = '" + wr_info.getImeiNo() + "'");

                        while (rs_schem.next()) {
                            warrant_schem_id = rs_schem.getInt("WRN_SCHEME_ID");
                        }
                        rs_schem.close();

                        String column = "WRR_REG_REF_NO,IMEI_NO,"
                                + "CUSTOMER_NIC,CUSTOMER_NAME,"
                                + "CONTACT_NO,DATE_OF_BIRTH,"
                                + "SELLING_PRICE,BIS_ID,STATUS,USER_INSERTED,"
                                + "DATE_INSERTED,REGISTER_DATE,WARANTY_TYPE";

                        String imeiRegisterDate = " TO_DATE('" + wr_info.getRegisterDate() + "','YYYY/MM/DD'), "; ////REGISTER_DATE is set to registration date coming from the front end. Bz, Warrannty period is calculating from the REGISTER_DATE value 
                        if (wr_info.getRegisterDate() == null || wr_info.getRegisterDate().length() == 0) {
                            imeiRegisterDate = " SYSDATE, ";
                        }

                        String values = "" + warany_reg_num + ","
                                + "'" + wr_info.getImeiNo() + "',"
                                + "'" + wr_info.getCustomerNIC() + "',"
                                + "'" + wr_info.getCustomerName() + "',"
                                + "'" + wr_info.getContactNo() + "',"
                                + "TO_DATE('" + wr_info.getDateOfBirth() + "','YYYY/MM/DD'),"
                                + "" + wr_info.getSellingPrice() + ","
                                + "" + wr_info.getBisId() + ","
                                + "1,"
                                + "'" + user_id + "',"
                                + "SYSDATE,"
                                + imeiRegisterDate
                                + "" + warrant_schem_id + "";

                        dbCon.save(con, "INSERT INTO SD_WARRENTY_REGISTRATION(" + column + ") VALUES(" + values + ")");

                        dbCon.save(con, "UPDATE SD_DEVICE_MASTER "
                                + "SET CUSTOMER_ID = '" + wr_info.getCustomerNIC() + "', "
                                + "STATUS =3, "
                                + "BIS_ID = 9999 "
                                + "WHERE  IMEI_NO = '" + wr_info.getImeiNo() + "'");

                        ResultSet rs_cus_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                                + "FROM SD_CUSTOMER_DETAILS "
                                + "WHERE CUSTOMER_NIC = '" + wr_info.getCustomerNIC() + "'");

                        while (rs_cus_cnt.next()) {
                            customer_reg = rs_cus_cnt.getInt("CNT");
                        }
                        rs_cus_cnt.close();
                        if (customer_reg > 0) {
                            logger.info("This user already registered..............");
                        } else {

                            //String cus_nic = wr_info.getCustomerNIC();
                            //String d = cus_nic.substring(0, 9);
                            Random rand = new Random();
                            con_code = CommonTask.getRandomInteger(0, Integer.MAX_VALUE, rand);
                            //con_code = String.valueOf(rand.nextInt(Integer.parseInt(d)));

                            dbCon.save(con, "INSERT INTO SD_CUSTOMER_DETAILS(CUSTOMER_NIC,"
                                    + "CUSTOMER_NAME,"
                                    + "DATE_OF_BIRTH,"
                                    + "ADDRESS,"
                                    + "CONTACT_NO,"
                                    + "EMAIL,"
                                    + "STATUS,"
                                    + "USER_INSERTED,"
                                    + "DATE_INSERTED,CONFIRM_CODE) VALUES('" + wr_info.getCustomerNIC() + "',"
                                    + "'" + wr_info.getCustomerName() + "',"
                                    + "TO_DATE('" + wr_info.getDateOfBirth() + "','YYYY/MM/DD'),"
                                    + "'" + wr_info.getCustomerAddress() + "',"
                                    + "'" + wr_info.getContactNo() + "',"
                                    + "'" + wr_info.getEmail() + "',"
                                    + "1,"
                                    + "'" + user_id + "',"
                                    + "SYSDATE,"
                                    + "'" + con_code + "')");

                            try {
                                logger.info("Confirmation Code -" + con_code);
                                EmaiSendConfirmCode.sendHTMLMail(wr_info.getEmail(), wr_info.getCustomerName(), Integer.toString(con_code));
                            } catch (MessagingException ex) {
                                logger.info("Error in E-mail Sending........" + ex.getMessage());
                            }

                        }

                        result = 1;

                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.info("Error in WarrentyRegistration insert  :" + e.toString());
                    }

                }
            } else {
                result = 3;
            }

        } catch (Exception ex) {
            logger.info("Error in addWarrentyRegistration Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(String.valueOf(warany_reg_num));
            vw.setReturnMsg("Warranty Registration Insert Successfully");
            logger.info("Warrernty Registration Insert Succesfully..........");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI is Already Registered");
            logger.info("This IMEI is already Registered..........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This IMEI is not in Master Record");
            logger.info("This IMEI is not in Master Record..........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Registration Insert");
            logger.info("Error in Warrernty Registration Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addWorkOrderWarrentyRegistration(WarrentyRegistration wr_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("addWorkOrderWarrentyRegistration Mehtod Call.......");
        int result = 0;
        int warrenty_reg = 0;
        int customer_reg = 0;
        int master_cnt = 0;
        String con_code = null;
        int warany_reg_num = 0;
        int warrant_schem_id = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_master_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + wr_info.getImeiNo() + "'");

            while (rs_master_cnt.next()) {
                master_cnt = rs_master_cnt.getInt("CNT");
            }

            if (master_cnt > 0) {

                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_WARRENTY_REGISTRATION "
                        + "WHERE IMEI_NO = '" + wr_info.getImeiNo() + "'");

                while (rs_cnt.next()) {
                    warrenty_reg = rs_cnt.getInt("CNT");
                }

                if (warrenty_reg > 0) {
                    result = 2;
                } else {

                    try {

                        ResultSet rs_wrt_reg = dbCon.search(con, "SELECT NVL(MAX(WRR_REG_REF_NO),0)+1 MAX "
                                + "FROM  SD_WARRENTY_REGISTRATION");

                        while (rs_wrt_reg.next()) {
                            warany_reg_num = rs_wrt_reg.getInt("MAX");
                        }

                        ResultSet rs_schem = dbCon.search(con, "SELECT M.WRN_SCHEME_ID "
                                + "FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M "
                                + "WHERE D.MODEL_NO = M.MODEL_NO "
                                + "AND D.IMEI_NO = '" + wr_info.getImeiNo() + "'");

                        while (rs_schem.next()) {
                            warrant_schem_id = rs_schem.getInt("WRN_SCHEME_ID");
                        }

                        String column = "WRR_REG_REF_NO,"
                                + "IMEI_NO,"
                                + "CUSTOMER_NIC,"
                                + "CUSTOMER_NAME,"
                                + "CONTACT_NO,"
                                + "DATE_OF_BIRTH,"
                                + "SELLING_PRICE,"
                                + "BIS_ID,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,"
                                + "REGISTER_DATE,"
                                + "CUSTOMER_CODE,"
                                + "SHOP_CODE,"
                                + "MODLE_NO,"
                                + "SITE_DESC,"
                                + "ORDER_NO,"
                                + "PROOF_OF_PURCHASE,"
                                + "WARANTY_TYPE";

                        String values = "?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                                + "?,?,1,?,SYSDATE,TO_DATE(?,'YYYY/MM/DD'),"
                                + "?,?,?,?,?,?,?";

                        PreparedStatement ps_in_wr = dbCon.prepare(con, "INSERT INTO SD_WARRENTY_REGISTRATION(" + column + ") VALUES(" + values + ")");
                        ps_in_wr.setInt(1, warany_reg_num);
                        ps_in_wr.setString(2, wr_info.getImeiNo());
                        ps_in_wr.setString(3, wr_info.getCustomerNIC());
                        ps_in_wr.setString(4, wr_info.getCustomerName());
                        ps_in_wr.setString(5, wr_info.getContactNo());
                        ps_in_wr.setString(6, wr_info.getDateOfBirth());
                        ps_in_wr.setDouble(7, wr_info.getSellingPrice());
                        ps_in_wr.setInt(8, wr_info.getBisId());
                        ps_in_wr.setString(9, user_id);
                        ps_in_wr.setString(10, wr_info.getRegisterDate());
                        ps_in_wr.setString(11, wr_info.getCustomerCode());
                        ps_in_wr.setString(12, wr_info.getShopCode());
                        ps_in_wr.setInt(13, wr_info.getModleNo());
                        ps_in_wr.setString(14, wr_info.getSiteDesc());
                        ps_in_wr.setString(15, wr_info.getOrderNo());
                        ps_in_wr.setString(16, wr_info.getProofPurchas());
                        ps_in_wr.setInt(17, warrant_schem_id);
                        ps_in_wr.executeUpdate();

                        PreparedStatement ps_up_dm = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                + "SET CUSTOMER_ID = ?,"
                                + "STATUS =3, "
                                + "BIS_ID = 9999 "
                                + "WHERE  IMEI_NO = ?");

                        ps_up_dm.setString(1, wr_info.getCustomerNIC());
                        ps_up_dm.setString(2, wr_info.getImeiNo());
                        ps_up_dm.executeUpdate();

                        ResultSet rs_cus_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                                + "FROM SD_CUSTOMER_DETAILS "
                                + "WHERE CUSTOMER_NIC = '" + wr_info.getCustomerNIC() + "'");

                        while (rs_cus_cnt.next()) {
                            customer_reg = rs_cus_cnt.getInt("CNT");
                        }
                        if (customer_reg > 0) {
                            logger.info("This user already registered..............");
                        } else {
                            String cus_nic = wr_info.getCustomerNIC();
                            String d = cus_nic.substring(0, 9);
                            Random rand = new Random();
                            con_code = String.valueOf(rand.nextInt(Integer.parseInt(d)));

                            PreparedStatement ps_in_cd = dbCon.prepare(con, "INSERT INTO SD_CUSTOMER_DETAILS(CUSTOMER_NIC,"
                                    + "CUSTOMER_NAME,"
                                    + "DATE_OF_BIRTH,"
                                    + "CONTACT_NO,"
                                    + "EMAIL,"
                                    + "STATUS,"
                                    + "USER_INSERTED,"
                                    + "DATE_INSERTED,CONFIRM_CODE) VALUES(?,"
                                    + "?,TO_DATE(?,'YYYY/MM/DD'),?,"
                                    + "?,"
                                    + "1,"
                                    + "?,"
                                    + "SYSDATE,"
                                    + "?)");

                            ps_in_cd.setString(1, wr_info.getCustomerNIC());
                            ps_in_cd.setString(2, wr_info.getCustomerName());
                            ps_in_cd.setString(3, wr_info.getDateOfBirth());
                            ps_in_cd.setString(4, wr_info.getContactNo());
                            ps_in_cd.setString(5, wr_info.getEmail());
                            ps_in_cd.setString(6, user_id);
                            ps_in_cd.setString(7, con_code);
                            ps_in_cd.executeUpdate();

                        }

                        result = 1;

                        try {
                            logger.info("Confirmation Code -" + con_code);
                            EmaiSendConfirmCode.sendHTMLMail(wr_info.getEmail(), wr_info.getCustomerName(), con_code);
                        } catch (MessagingException ex) {
                            logger.info("Error in E-mail Sending........" + ex.getMessage());
                        }

                    } catch (Exception e) {
                        logger.info("Error in addWorkOrderWarrentyRegistration insert  :" + e.toString());
                    }

                }
            } else {
                result = 3;
            }

        } catch (Exception ex) {
            logger.info("Error in addWarrentyRegistration Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(String.valueOf(warany_reg_num));
            vw.setReturnMsg("Warranty Registration Insert Successfully");
            logger.info("Warrernty Registration Insert Succesfully..........");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI is already Registered");
            logger.info("This IMEI is already Registered..........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This IMEI is not in Master Record");
            logger.info("This IMEI is not in Master Record..........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Registration Insert");
            logger.info("Error in Warrernty Registration Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

}
