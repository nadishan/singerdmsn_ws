/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;

/**
 *
 * @author SDU
 */
public class DbProcedureController {
    
    
    
    public static ValidationWrapper debitNoteTransferProcedureCall(String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("debitNoteTransferProcedureCall Method Call.....");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {
            PreparedStatement ps_procedure = dbCon.prepare(con, "CALL SINDMA.LOAD_ERP_DEBIT_NOTE ('S')");
            logger.info("Ready to Execute Debit Note Transfer.........");
            ps_procedure.executeQuery();
            logger.info("Transfer Done..........");
            result = 1;
            
        } catch (Exception e) {
            logger.info("Error in debitNoteTransferProcedureCall..." + e.getMessage());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Procedure run Successfully");
            logger.info("Procedure run Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Procedure run");
            logger.info("Error in Procedure run.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    } 
    
}
