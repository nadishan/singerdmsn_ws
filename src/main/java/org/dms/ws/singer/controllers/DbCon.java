/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import org.dms.ws.singer.utils.AppParams;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import oracle.jdbc.driver.OracleDriver;
import org.apache.log4j.Logger;

/**
 *
 * @author hashan
 */
public class DbCon {
    public static Logger logger = AppParams.logger;
    public Connection getCon() {
        return getJDBCConnection();
    }

    private Connection getJDBCConnection() {
        String dbCon = null;
        if (AppParams.DB_CONNECTION_TYPE.equalsIgnoreCase("oracle")) {
            try {
                DriverManager.registerDriver(new OracleDriver());
            } catch (SQLException ex) {
                System.out.println("Driver load exception.....");
            }
            dbCon = AppParams.DB_URL;
        }

        try {
            return DriverManager.getConnection(dbCon, AppParams.DB_UNAME, AppParams.DB_PASSWORD);
        } catch (SQLException ex) {
            System.out.println("Error Creating JDBC Connection.....");
            return null;
        }
    }

    public ResultSet search(Connection connection, String sql) throws Exception {
        AppParams.logger.info("DBCon Search Calling............");
        // logger.info("Ready to return Value............");
        return connection.createStatement().executeQuery(sql);
        //connection.close();
    }

    public PreparedStatement prepare(Connection connection, String sql) throws Exception {
        AppParams.logger.info("DBCon Perpare Calling............");
        return connection.prepareStatement(sql);
    }

    public PreparedStatement prepareAutoId(Connection connection, String sql) throws Exception {
        AppParams.logger.info("DBCon Perpare Calling............");
        return connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    }

    public void executePreparedStatement(Connection connection, PreparedStatement statement) throws Exception {
        AppParams.logger.info("DBCon Prepare Save Calling............");
        statement.executeUpdate();
    }
    
    

//    public void insertPrepareQuery(DbCon dbcon, String tableName, String columns, Object[] values) throws Exception {
//        String[] fieldsArray = columns.split(",");
//        String valuesPhantom = "";
//        for (String string : fieldsArray) {
//            valuesPhantom += "?,";
//        }
//        valuesPhantom = valuesPhantom.substring(0, valuesPhantom.length() - 1);
//        String query = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + valuesPhantom + ")";
//        PreparedStatement preparedStatement = dbCon.prepare(con,dbcon.getCon(), query);
//        int i = 1;
//        for (Object obj : values) {
//            try {
//                if (obj.equals(Integer.TYPE)) {
//                    preparedStatement.setInt(i, Integer.parseInt(String.valueOf(obj)));
//                } else if (obj.equals(Double.TYPE)) {
//                    preparedStatement.setDouble(i, Double.parseDouble(String.valueOf(obj)));
//                } else if (obj.equals(Float.TYPE)) {
//                    preparedStatement.setFloat(i, Float.parseFloat(String.valueOf(obj)));
//                } else {
//                    preparedStatement.setString(i, String.valueOf(obj));
//                }
//            } catch (NullPointerException ex) {
//                preparedStatement.setString(i, "");
//            }
//            ++i;
//        }
//        dbcon.executePreparedStatement(dbcon.getCon(), preparedStatement);
//
//    }

//    public void updatePrepareQuery(DbCon dbcon, String tableName, String columns, Object[] values, String where) throws Exception {
//        String[] fieldsArray = columns.split(",");
//        String valuesPhantom = "";
//        for (String string : fieldsArray) {
//            valuesPhantom += "?,";
//        }
//        valuesPhantom = valuesPhantom.substring(0, valuesPhantom.length() - 1);
//        String query = "UPDATE " + tableName + " SET " + columns + " WHERE " + where + " ";
//        PreparedStatement preparedStatement = dbCon.prepare(con,dbcon.getCon(), query);
//        int i = 1;
//        for (Object obj : values) {
//            try {
//                if (obj.equals(Integer.TYPE)) {
//                    preparedStatement.setInt(i, Integer.parseInt(String.valueOf(obj)));
//                } else if (obj.equals(Double.TYPE)) {
//                    preparedStatement.setDouble(i, Double.parseDouble(String.valueOf(obj)));
//                } else if (obj.equals(Float.TYPE)) {
//                    preparedStatement.setFloat(i, Float.parseFloat(String.valueOf(obj)));
//                } else {
//                    preparedStatement.setString(i, String.valueOf(obj));
//                }
//            } catch (NullPointerException ex) {
//                preparedStatement.setString(i, "");
//            }
//            ++i;
//        }
//        dbcon.executePreparedStatement(dbcon.getCon(), preparedStatement);
//
//    }

    public void save(Connection connection, String sql) throws Exception {
        AppParams.logger.info("DBCon Save Calling............");
        connection.createStatement().executeUpdate(sql);
         AppParams.logger.info("DBCon Save Success............");
    }

    public void ConectionClose(Connection connection) {
        try {
            if (connection != null) {
                AppParams.logger.info("DB Connection Close...............");
                connection.close();
            }
        } catch (SQLException ex) {
            AppParams.logger.info("Error in DB Connection Close...." + ex.toString());
        }

    }

    public void Batchsave(Connection connection, String sql) throws Exception {
        AppParams.logger.info("DBCon BatchSave Calling............");
        Statement smt = connection.createStatement();
        smt.addBatch(sql);

    }

    public void ExecuteBatch(Connection connection) throws Exception {
        AppParams.logger.info("DBCon Execute Calling............");
        Statement smt = connection.createStatement();
        //logger.info(smt);
        smt.executeBatch();
    }

    public void CreateStatement(Connection connection) throws Exception {
        AppParams.logger.info("DBCon CreateStatement Calling............");
        Statement smt = connection.createStatement();
        smt = connection.createStatement();
    }
    
    public void offAutoCommit(Connection connection) {

        try {
            AppParams.logger.info("DBCon off auto commit Calling............");
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
             AppParams.logger.info("DBCon error off auto commit Calling............");
            ex.printStackTrace();
        }
    }

    public void onAutoCommit(Connection connection) {

        try {
            AppParams.logger.info("DBCon on auto commit Calling............");
            connection.setAutoCommit(true);
        } catch (SQLException ex) {
             AppParams.logger.info("DBCon error off auto commit Calling............");
            ex.printStackTrace();
        }
    }

    public void doCommit(Connection connection) throws SQLException {

        try {
            AppParams.logger.info("DBCon do commit Calling............");

            connection.commit();

        } catch (SQLException ex) {
            connection.rollback();
            AppParams.logger.info("DBCon rolled back............");
            ex.printStackTrace();
        }
    }

    public void doRollback(Connection connection) {
        try {
            connection.rollback();
            AppParams.logger.info("DBCon rolled back............");
        } catch (SQLException ex) {

            ex.printStackTrace();

        }
    }
}