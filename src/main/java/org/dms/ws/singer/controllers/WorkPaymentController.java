/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import org.dms.ws.singer.entities.WorkOrderPayment;
import org.dms.ws.singer.entities.WorkOrderPaymentDetail;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WorkPaymentController {

    public static ArrayList<ImeiMaster> checkImeiWarranty() {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String qry = "  SELECT D.IMEI_NO  "
                    + "     FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A, SD_WARRENTY_REGISTRATION R     "
                    + "     WHERE D.MODEL_NO = M.MODEL_NO (+)           "
                    + "     AND   D.IMEI_NO = R.IMEI_no (+)             "
                    + "     AND M.WRN_SCHEME_ID = W.SCHEME_ID (+)       "
                    + "     AND D.IMEI_NO = A.IMEI (+)                  "
                    + "     AND  TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') >= SYSDATE       ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            while (rs_master_cnt.next()) {
                ImeiMaster imie = new ImeiMaster();

                imie.setImeiNo(rs_master_cnt.getString("IMEI_NO"));
                imeiMasterList.add(imie);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }

        dbCon.ConectionClose(con);
        return imeiMasterList;

    }

    public static ArrayList<ImeiMaster> getOutWarrantyImei() {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String qry = "  SELECT D.IMEI_NO  "
                    + "     FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A, SD_WARRENTY_REGISTRATION R     "
                    + "     WHERE D.MODEL_NO = M.MODEL_NO (+)           "
                    + "     AND   D.IMEI_NO = R.IMEI_no (+)             "
                    + "     AND M.WRN_SCHEME_ID = W.SCHEME_ID (+)       "
                    + "     AND D.IMEI_NO = A.IMEI (+)                  "
                    + "     AND  TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') >= SYSDATE       ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            while (rs_master_cnt.next()) {
                ImeiMaster imie = new ImeiMaster();

                imie.setImeiNo(rs_master_cnt.getString("IMEI_NO"));
                imeiMasterList.add(imie);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }

        dbCon.ConectionClose(con);
        return imeiMasterList;

    }

    public static MessageWrapper getWorkOrderPaymentList(String work_order_no,
            String imei_no,
            String warnty_verif_type,
            String defect,
            int status,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderPaymentList Method Call....................");
        ArrayList<WorkOrderPaymentDetail> workOrderPaymentList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrderPayment(order);

            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(P.WORK_ORDER_NO,'AA') = NVL(P.WORK_ORDER_NO,'AA')" : "UPPER(P.WORK_ORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
            String imeiNo = (imei_no.equalsIgnoreCase("all") ? "NVL(P.IMEI_NO,'AA') = NVL(P.IMEI_NO,'AA')" : "UPPER(P.IMEI_NO) LIKE UPPER('%" + imei_no + "%')");
            String warantyVeriType = (warnty_verif_type.equalsIgnoreCase("all") ? "" : "AND P.WARRANTY_VERIFY_TYPE = " + warnty_verif_type + "");
            String defectDesc = (defect.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect + "%')");
            String Status = (status == 0 ? "" : "AND P.STATUS = " + status + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY P.PAYMENT_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "P.PAYMENT_ID,"
                    + "P.WORK_ORDER_NO,"
                    + "P.IMEI_NO,"
                    + "P.WARRANTY_VERIFY_TYPE,"
                    + "P.DEFECT_TYPE,"
                    + "D.MAJ_CODE_DESC,"
                    + "P.STATUS,"
                    + "P.REFERENCE_NO,"
                    + "W.TECHNICIAN, "
                    + "W.ACTION_TAKEN, "
                    + "W.REMARKS, "
                    + "WP.AMOUNT ";

            String whereClous = " P.WORK_ORDER_NO=W.WORK_ORDER_NO "
                    + " AND P.PAYMENT_ID=WP.PAYMENT_ID "
                    + " AND P.DEFECT_TYPE = D.MAJ_CODE (+) "
                    + " AND " + workOrderNo + " "
                    + " AND " + imeiNo + " "
                    + " " + warantyVeriType + " "
                    + " AND " + defectDesc + " "
                    + " " + Status + " ";

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WORK_ORDER_PAYMENTS_DTL P,SD_DEFECT_CODES D, SD_WORK_ORDERS W, SD_WORK_ORDER_PAYMENTS WP WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WORK_ORDER_PAYMENTS_DTL P,SD_DEFECT_CODES D, SD_WORK_ORDERS W, SD_WORK_ORDER_PAYMENTS WP  WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("get WO Payment List"+qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = WorkPaymentController.checkImeiWarranty();

            while (rs.next()) {

                for (ImeiMaster i : warrantImie) {
                    if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                        WorkOrderPaymentDetail wp = new WorkOrderPaymentDetail();
                        totalrecode = rs.getInt("CNT");
                        wp.setPaymentId(rs.getInt("PAYMENT_ID"));
                        wp.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                        wp.setImeiNo(rs.getString("IMEI_NO"));
                        //wp.setWarrentyTypeDesc(rs.getString("WARRANTY_VERIFY_TYPE"));
                        wp.setWarrentyTypeDesc("Under Warranty");
                        wp.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                        wp.setStatus(rs.getInt("STATUS"));
                        wp.setReferenceNo(rs.getString("REFERENCE_NO"));
                        if (rs.getInt("STATUS") == 1) {
                            wp.setStatusMsg("Pending");
                        } else if (rs.getInt("STATUS") == 2) {
                            wp.setStatusMsg("Complete");
                        } else {
                            wp.setStatusMsg("None");
                        }

                        wp.setTecnician(rs.getString("TECHNICIAN"));
                        wp.setActionTake(rs.getString("ACTION_TAKEN"));
                        wp.setRemarks(rs.getString("REMARKS"));
                        wp.setAmount(rs.getDouble("AMOUNT"));

                        workOrderPaymentList.add(wp);

                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderPaymentList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderPaymentList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderPaymentListwithPrice(String work_order_no,
            String warranty_desc,
            String imei_no,
            String defect_desc,
            double labor_cost,
            double spare_cost,
            int bisId,
            String order,
            String type) {

        logger.info("getWorkOrderPaymentListwithPrice Method Call....................");
        ArrayList<WorkOrderPaymentDetail> workOrderPaymentList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrderPaymentwithPrice(order);

            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
            String warrantyDesc = (warranty_desc.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_desc + "%')");
            String imeiNo = (imei_no.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei_no + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String laborCost = (labor_cost == 0 ? "" : "ANF L.PRICE = " + labor_cost + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");
            //String BisId = (bisId == 0 ? " " : " AND W.BIS_ID= '" + bisId + "' ");
            String BisId = (bisId == 0 ? " " : " AND O.BIS_ID= '" + bisId + "' ");

            ResultSet rs_parts_amount = dbCon.search(con, "SELECT WORK_ORDER_NO,"
                    + "SUM(SELLING_PRICE) TOTAL "
                    + "FROM SD_WORK_ORDER_MAINTAIN_DTL "
                    + "WHERE WRK_ORD_MTN_ID IN (SELECT WRK_ORD_MTN_ID "
                    + "FROM SD_WORK_ORDER_MAINTAIN) "
                    + "GROUP BY WORK_ORDER_NO");

            ArrayList<WorkOrderPaymentDetail> workOrderTemPriceList = new ArrayList<>();

            while (rs_parts_amount.next()) {
                WorkOrderPaymentDetail wop = new WorkOrderPaymentDetail();
                wop.setWorkOrderNo(rs_parts_amount.getString("WORK_ORDER_NO"));
                wop.setSpareCost(rs_parts_amount.getDouble("TOTAL"));
                workOrderTemPriceList.add(wop);

            }

            ArrayList<WorkOrderPaymentDetail> tempworkOrderPaymentList = new ArrayList<>();

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "M.DESCRIPTION,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.IMEI_NO,"
                    + "W.DEFECTS,"
                    + "W.STATUS,"
                    + "L.PRICE LABOR_COST,"
                    + "D.MAJ_CODE_DESC, "
                    + "O.INVOICE_NO ";

            String whereClous = "O.REPAIR_LEVEL_ID = L.LEVEL_ID (+)  "
                    + " AND W.DEFECTS = D.MAJ_CODE (+) "
                    + " AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + " AND M.DOMAIN_NAME = 'WARANTY' "
                    + " AND W.STATUS = 3  "
                    + " AND W.WORK_ORDER_NO=O.WORK_ORDER_NO    "
                    + " AND W.WORK_ORDER_NO "
                    + " NOT IN   "
                    + " (SELECT WORK_ORDER_NO   "
                    + " FROM    SD_WORK_ORDER_PAYMENTS_DTL   )"
                    + BisId
                    + " AND " + workOrderNo + " "
                    + " AND " + warrantyDesc + " "
                    + " AND " + imeiNo + " "
                    + " AND " + defectDesc + " "
                    + " " + laborCost + "   ";

            String tables = "SD_WORK_ORDERS W, SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_DEFECT_CODES D, SD_WORK_ORDER_MAINTAIN O";

            System.out.println("RRRRRRRRRRRRRRRr  " + "SELECT " + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + " " + Order + " ");
            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + " " + Order + " ");

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = WorkPaymentController.getOutWarrantyImei();
             System.out.println("length###############" + warrantImie.size());
            while (rs.next()) {
//                for (ImeiMaster i : warrantImie) {
                    if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                        WorkOrderPaymentDetail wp = new WorkOrderPaymentDetail();
                        wp.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                        wp.setImeiNo(rs.getString("IMEI_NO"));
                        wp.setWarrantyVerifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                        wp.setWarrentyTypeDesc(rs.getString("DESCRIPTION"));
                        wp.setDefectType(rs.getInt("DEFECTS"));
                        wp.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                        wp.setStatus(rs.getInt("STATUS"));
                        wp.setLaborCost(rs.getDouble("LABOR_COST"));
                        wp.setInvoice(rs.getString("INVOICE_NO"));
                        for (WorkOrderPaymentDetail wopt : workOrderTemPriceList) {
                            if (wopt.getWorkOrderNo().equals(rs.getString("WORK_ORDER_NO"))) {
                                wp.setSpareCost(wopt.getSpareCost());
                            }
                        }
                        tempworkOrderPaymentList.add(wp);
//                    }
                }

            }

           

            if (spare_cost == 0) {
                System.out.println("spare_cost -- " + "00000000000");
                workOrderPaymentList = tempworkOrderPaymentList;
                System.out.println("spare_size -- " + workOrderPaymentList.size());
            } else {
                System.out.println("spare_cost -- " + "0111111111111111");
                for (WorkOrderPaymentDetail wopd : tempworkOrderPaymentList) {
                    System.out.println("  " + wopd.getSpareCost());
                    if (wopd.getSpareCost() == spare_cost) {
                        WorkOrderPaymentDetail wpp = new WorkOrderPaymentDetail();
                        wpp.setWorkOrderNo(wopd.getWorkOrderNo());
                        wpp.setImeiNo(wopd.getImeiNo());
                        wpp.setWarrantyVerifType(wopd.getWarrantyVerifType());
                        wpp.setWarrentyTypeDesc(wopd.getWarrentyTypeDesc());
                        wpp.setDefectType(wopd.getDefectType());
                        wpp.setDefectDesc(wopd.getDefectDesc());
                        wpp.setStatus(wopd.getStatus());
                        wpp.setLaborCost(wopd.getLaborCost());
                        wpp.setSpareCost(wopd.getSpareCost());
                        workOrderPaymentList.add(wpp);

                    }

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderPaymentListwithPrice Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = workOrderPaymentList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderPaymentList);

        logger.info(
                "Ready to Return Values...............");
        dbCon.ConectionClose(con);

        logger.info(
                "----------------------------------------------------");
        return mw;
    }

    
    
    
    
//     public static MessageWrapper getWorkOrderPaymentListwithPrice(String work_order_no,
//            String imei_no,
//            String warnty_verif_type,
//            String defect,
//            int status,
//            String order,
//            String type,
//            int start,
//            int limit) {
//
//        logger.info("getWorkOrderPaymentListwithPrice Method Call....................");
//        ArrayList<WorkOrderPaymentDetail> workOrderPaymentList = new ArrayList<>();
//        int totalrecode = 0;
//        DbCon dbCon = new DbCon();
//        Connection con = dbCon.getCon();
//        try {
//            
//
//            order = DBMapper.workOrder(order);
//
//            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
//            String imeiNo = (imei_no.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei_no + "%')");
//            String warantyVeriType = (warnty_verif_type.equalsIgnoreCase("all") ? "" : "AND W.WARRANTY_VERIFY_TYPE = " + warnty_verif_type + "");
//            String defectDesc = (defect.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defect + "%')");
//            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
//            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");
//
//            String seletColunm = "W.WORK_ORDER_NO,"
//                    + "W.WARRANTY_VRIF_TYPE,"
//                    + "M.DESCRIPTION,"
//                    + "W.NON_WARRANTY_VRIF_TYPE,"
//                    + "W.IMEI_NO,"
//                    + "W.DEFECTS,"
//                    + "W.STATUS,"
//                    + "L.PRICE LABOR_COST,"
//                    + "(SELECT SUM(SELLING_PRICE) FROM SD_WORK_ORDER_MAINTAIN_DTL WHERE WORK_ORDER_NO = W.WORK_ORDER_NO) PARTS_COST";
//
//            String whereClous = "W.LEVEL_ID = L.LEVEL_ID (+) "
//                    + " AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
//                    + " AND M.DOMAIN_NAME = 'WARANTY' "
//                    + " AND " + workOrderNo + " "
//                    + " AND " + imeiNo + " "
//                    + " " + warantyVeriType + " "
//                    + " AND " + defectDesc + " "
//                    + " " + Status + " ";
//
//            String tables = "SD_WORK_ORDERS W, SD_REPAIR_LEVELS L,SD_MDECODE M";
//
//            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
//                    + " FROM " + tables + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//
//            logger.info("Get Data to Resultset...");
//
//            while (rs.next()) {
//                WorkOrderPaymentDetail wp = new WorkOrderPaymentDetail();
//                totalrecode = rs.getInt("CNT");
//                wp.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
//                wp.setImeiNo(rs.getString("IMEI_NO"));
//                wp.setWarrentyTypeDesc(rs.getString("DESCRIPTION"));
//                wp.setDefectDesc(rs.getString("DEFECTS"));
//                wp.setStatus(rs.getInt("STATUS"));
//                wp.setLaborCost(rs.getDouble("LABOR_COST"));
//                wp.setSpareCost(rs.getDouble("PARTS_COST"));
//                workOrderPaymentList.add(wp);
//            }
//
//        } catch (Exception ex) {
//            logger.info("Error getWorkOrderPaymentListwithPrice Methd call.." + ex.toString());
//        }
//        MessageWrapper mw = new MessageWrapper();
//        mw.setTotalRecords(totalrecode);
//        mw.setData(workOrderPaymentList);
//        logger.info("Ready to Return Values...............");
//        dbCon.ConectionClose(con);
//        logger.info("----------------------------------------------------");
//        return mw;
//    }
    public static ValidationWrapper addWorkOrderPayment(WorkOrderPayment wop_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("addWorkOrderPayment Mehtod Call.......");
        int result = 0;
        int max_pament_id = 0;
        int pay_cnt = 0;
        try {

            ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(PAYMENT_ID),0)+1 MAX "
                    + "FROM SD_WORK_ORDER_PAYMENTS");

            if (rs_max.next()) {
                max_pament_id = rs_max.getInt("MAX");
            }

            String column = "PAYMENT_ID,"
                    + "AMOUNT,"
                    + "ESSD_ACCOUNT_NO,"
                    + "PAYMENT_DATE,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED, "
                    + "NBT_VALUE, "
                    + "VAT_VALUE, "
                    + "PAY_BIS_ID,"
                    + "NBT_APPLICABLE,"
                    + "VAT_APPLICABLE,"
                    + "DEDUCT_AMOUNT ";

            String values = "?,?,?,SYSDATE,1,?,SYSDATE, ?, ?, ?,?,?,?";

            PreparedStatement ps = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_PAYMENTS(" + column + ") VALUES(" + values + ")");
            ps.setInt(1, max_pament_id);
            ps.setDouble(2, wop_info.getAmount());
            ps.setString(3, wop_info.getEssdAccountNo());
            ps.setString(4, user_id);
            ps.setDouble(5, wop_info.getNbtValue());
            ps.setDouble(6, wop_info.getVatValue());
            ps.setDouble(7, wop_info.getPayBisId());
            ps.setInt(8, wop_info.getNbtApplicable());
            ps.setInt(9, wop_info.getVatApplicable());
            ps.setDouble(10, wop_info.getDeductionAmount());
            ps.executeUpdate();
            result = 1;

            try {
                if (wop_info.getWoPaymentDetials() != null) {

                    ArrayList<WorkOrderPaymentDetail> wolist = new ArrayList<WorkOrderPaymentDetail>();
                    wolist = wop_info.getWoPaymentDetials();

                    if (wolist.size() > 0) {

                        String paymenrDetialColumn = "SEQ_NO,"
                                + "PAYMENT_ID,"
                                + "WORK_ORDER_NO,"
                                + "IMEI_NO,"
                                + "WARRANTY_VERIFY_TYPE,"
                                + "DEFECT_TYPE,"
                                + "AMOUNT,"
                                + "PAYMENT_DATE,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED";

                        String woPaymentValue = "?,?,?,?,?,?,?,SYSDATE,1,?,SYSDATE";

                        ResultSet rs_cnt = dbCon.search(con, "SELECT (NVL(MAX(SEQ_NO),0)) MAX "
                                + "FROM SD_WORK_ORDER_PAYMENTS_DTL "
                                + "WHERE PAYMENT_ID = " + max_pament_id + "");

                        while (rs_cnt.next()) {
                            pay_cnt = rs_cnt.getInt("MAX");
                        }

                        PreparedStatement ps_dt = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_PAYMENTS_DTL(" + paymenrDetialColumn + ") "
                                + "VALUES(" + woPaymentValue + ")");

                        for (WorkOrderPaymentDetail di : wolist) {

                            pay_cnt = pay_cnt + 1;
                            System.out.println(pay_cnt);
                            System.out.println(max_pament_id);
                            System.out.println(di.getWorkOrderNo());
                            System.out.println(di.getImeiNo());
                            System.out.println(di.getWarrantyVerifType());
                            System.out.println(di.getDefectType());
                            System.out.println(di.getAmount());

                            ps_dt.setInt(1, pay_cnt);
                            ps_dt.setInt(2, max_pament_id);
                            ps_dt.setString(3, di.getWorkOrderNo());
                            ps_dt.setString(4, di.getImeiNo());
                            ps_dt.setInt(5, di.getWarrantyVerifType());
                            ps_dt.setInt(6, di.getDefectType());
                            ps_dt.setDouble(7, di.getAmount());
                            ps_dt.setString(8, user_id);
                            ps_dt.addBatch();

                        }

                        ps_dt.executeBatch();

                        try {

                            String updateQry = " UPDATE SD_WORK_ORDERS   "
                                    + "         SET    STATUS = 3       "
                                    + "         WHERE  WORK_ORDER_NO = ? ";

                            PreparedStatement prep = dbCon.prepare(con, updateQry);

                            for (WorkOrderPaymentDetail wo : wolist) {
                                prep.setString(1, wo.getWorkOrderNo());
                                prep.addBatch();
                            }

                            prep.executeBatch();

                            result = 1;

                        } catch (Exception e) {
                            result = 9;
                            e.getMessage();
                        }

                    }

                }

            } catch (Exception e) {
                logger.info("Error in Work Order Payment Details Inserte....." + e.toString());
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error in addWorkOrderPayment Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(String.valueOf(max_pament_id));
            vw.setReturnMsg("Work Order Payment Successfully");
            logger.info("WorkOrder Payment Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Payment Insert");
            logger.info("Error in Work Order Payment Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

//    public static ValidationWrapper addWorkOrderPayment(WorkOrderPayment wop_info,
//            String user_id,
//            String room,
//            String department,
//            String branch,
//            String countryCode,
//            String division,
//            String organaization) {
//
//        logger.info("addWorkOrderPayment Mehtod Call.......");
//        int result = 0;
//        int max_pament_id = 0;
//        try {
//
//            ResultSet rs_max = dbCon.search(con,"SELECT NVL(MAX(PAYMENT_ID),0)+1 MAX "
//                    + "FROM SD_WORK_ORDER_PAYMENTS");
//
//            if (rs_max.next()) {
//                max_pament_id = rs_max.getInt("MAX");
//            }
//
//            String column = "PAYMENT_ID,AMOUNT,"
//                    + "TAX,ESSD_ACCOUNT_NO,"
//                    + "PAYMENT_DATE,STATUS,"
//                    + "USER_INSERTED,DATE_INSERTED";
//
//            String values = "" + max_pament_id + ","
//                    + "" + wop_info.getAmount() + ","
//                    + "" + wop_info.getTax() + ","
//                    + "'" + wop_info.getEssdAccountNo() + "',"
//                    + "SYSDATE,"
//                    + "1,"
//                    + "'" + user_id + "',"
//                    + "SYSDATE";
//
//            dbCon.save(con,"INSERT INTO SD_WORK_ORDER_PAYMENTS(" + column + ") VALUES(" + values + ")");
//            result = 1;
//
//            try {
//                if (wop_info.getWoPaymentDetials() != null) {
//
//                    ArrayList<WorkOrderPaymentDetail> wolist = new ArrayList<WorkOrderPaymentDetail>();
//                    wolist = wop_info.getWoPaymentDetials();
//
//                    if (wolist.size() > 0) {
//
//                        String paymenrDetialColumn = "SEQ_NO,"
//                                + "PAYMENT_ID,"
//                                + "WORK_ORDER_NO,"
//                                + "IMEI_NO,"
//                                + "WARRANTY_VERIFY_TYPE,"
//                                + "DEFECT_TYPE,"
//                                + "AMOUNT,"
//                                + "PAYMENT_DATE,"
//                                + "STATUS,"
//                                + "USER_INSERTED,"
//                                + "DATE_INSERTED";
//
//                        dbCon.CreateStatement(con);
//                        for (WorkOrderPaymentDetail di : wolist) {
//
//                            String woPaymentValue = "(SELECT (NVL(MAX(SEQ_NO),0)+1) FROM SD_WORK_ORDER_PAYMENTS_DTL),"
//                                    + "" + max_pament_id + ","
//                                    + "'" + di.getWorkOrderNo() + "',"
//                                    + "'" + di.getImeiNo() + "',"
//                                    + "" + di.getWarrantyVerifType() + ","
//                                    + "" + di.getDefectType() + ","
//                                    + "" + di.getAmount() + ","
//                                    + "SYSDATE,"
//                                    + "1,"
//                                    + "'" + user_id + "',"
//                                    + "SYSDATE";
//
//                            
//                            dbCon.Batchsave(con,"INSERT INTO SD_WORK_ORDER_PAYMENTS_DTL(" + paymenrDetialColumn + ") "
//                                    + "VALUES(" + woPaymentValue + ")");
//
//                            logger.info("Work Order Payment Details Inserted........");
//
//                            result = 1;
//                        }
//                        dbCon.ExecuteBatch(con);
//                    }
//
//                }
//
//            } catch (Exception e) {
//                logger.info("Error in Device Issue Details IME  Data Inserte....." + e.toString());
//                result = 9;
//            }
//
//        } catch (Exception ex) {
//            logger.info("Error in addWorkOrderPayment Mehtod  :" + ex.toString());
//            result = 9;
//        }
//        ValidationWrapper vw = new ValidationWrapper();
//        if (result == 1) {
//            vw.setReturnFlag("1");
//            vw.setReturnRefNo(String.valueOf(max_pament_id));
//            vw.setReturnMsg("WorkOrder Payment Succesfully");
//            logger.info("WorkOrder Payment Succesfully............");
//
//        } else {
//            vw.setReturnFlag("9");
//            vw.setReturnMsg("Error in WorkOrder Payment Insert");
//            logger.info("Error in WorkOrder Payment Insert................");
//        }
//        logger.info("Ready to Return Values..........");
//        dbCon.ConectionClose(con);
//        logger.info("-----------------------------------------------------------");
//        return vw;
//    }
    public static MessageWrapper getWorkOrderPaymentDetails(String work_order_no) {

        logger.info("getWorkOrderPaymentDetails Method Call....................");
        ArrayList<WorkOrderPaymentDetail> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "P.WORK_ORDER_NO,"
                    + "P.WARRANTY_VERIFY_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.ACTION_TAKEN,"
                    + "P.IMEI_NO,"
                    + "W.REMARKS";

            String whereClous = "P.WORK_ORDER_NO = W.WORK_ORDER_NO (+) "
                    + " AND P.DEFECT_TYPE = D.MAJ_CODE (+) "
                    + " AND P.WORK_ORDER_NO = '" + work_order_no + "'";

            String tables = "SD_WORK_ORDER_PAYMENTS_DTL P,SD_WORK_ORDERS W,SD_DEFECT_CODES D";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE   " + whereClous + ") ");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderPaymentDetail wp = new WorkOrderPaymentDetail();
                totalrecode = rs.getInt("CNT");
                wp.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wp.setImeiNo(rs.getString("IMEI_NO"));
                wp.setWarrantyVerifType(rs.getInt("WARRANTY_VERIFY_TYPE"));
                wp.setNonWarrantyVerifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wp.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                wp.setActionTake(rs.getString("ACTION_TAKEN"));
                wp.setTecnician(rs.getString("TECHNICIAN"));
                wp.setRemarks(rs.getString("REMARKS"));

                ArrayList<WorkOrderMaintainDetail> woDetailList = new ArrayList<>();

                ResultSet rs_details = dbCon.search(con, "SELECT SPARE_PART_NO,"
                        + "PART_DESCRIPTION,"
                        + "SELLING_PRICE,"
                        + "THIRD_PARTY_FLAG,"
                        + "STATUS "
                        + "FROM SD_WORK_ORDER_MAINTAIN_DTL "
                        + "WHERE WORK_ORDER_NO = '" + work_order_no + "'");
                while (rs_details.next()) {
                    WorkOrderMaintainDetail wod = new WorkOrderMaintainDetail();
                    wod.setPartNo(rs_details.getInt("SPARE_PART_NO"));
                    wod.setPartDesc(rs_details.getString("PART_DESCRIPTION"));
                    wod.setPartSellPrice(rs_details.getDouble("SELLING_PRICE"));
                    wod.setThirdPartyFlag(rs_details.getInt("THIRD_PARTY_FLAG"));
                    wod.setStatus(rs_details.getInt("STATUS"));
                    woDetailList.add(wod);
                }

                wp.setWoDetials(woDetailList);
                workOrderList.add(wp);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderPaymentDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    //////////////////////////GetWorkOrderPaymentDetails New Change/////////////////////////////////////
    public static MessageWrapper getWorkOrderPaymentList_New(int bisId,
            int paymentId,
            double amount,
            String payDate,
            String poNo,
            String poDate,
            String cheqNo,
            String cheqDate,
            int status,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderPaymentList_New Method Call....................");
        ArrayList<WorkOrderPayment> workOrderPaymentList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrderPayment(order);

            String BisId = (bisId == 0 ? " " : " AND W.BIS_ID = " + bisId + " ");
            String PaymenetId = (paymentId == 0 ? "P.PAYMENT_ID=P.PAYMENT_ID" : "P.PAYMENT_ID=" + paymentId + "");
            String Amount = (amount == 0 ? "  " : " AND P.AMOUNT=" + amount + "");
            String PayDate = (payDate.equalsIgnoreCase("all") ? "  " : " AND TO_CHAR(P.PAYMENT_DATE , 'YYYY/MM/DD')  = '" + payDate + "'");
            String PoNo = (poNo.equalsIgnoreCase("all") ? "  " : " AND P.PO_NUMBER='" + poNo + "'");
            String PoDate = (poDate.equalsIgnoreCase("all") ? "  " : " AND TO_CHAR(P.PO_DATE , 'YYYY/MM/DD')  = '" + poDate + "'");
            String CheqNo = (poNo.equalsIgnoreCase("all") ? "  " : " AND P.CHEQUE_NO='" + cheqNo + "'");
            String CheqDate = (cheqDate.equalsIgnoreCase("all") ? "  " : " AND TO_CHAR(P.CHEQUE_COLLECTED_DATE , 'YYYY/MM/DD')  = '" + cheqDate + "'");

            String Status = (status == 0 ? "" : " AND P.STATUS = " + status + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY P.PAYMENT_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "  P.PAYMENT_ID,"
                    + "P.AMOUNT,"
                    + "TO_CHAR(P.PAYMENT_DATE,'YYYY/MM/DD') AS PAYMENT_DATE,"
                    + "P.PO_NUMBER,"
                    + "TO_CHAR(P.PO_DATE,'YYYY/MM/DD') AS PO_DATE,"
                    + "P.CHEQUE_NO,"
                    + "TO_CHAR(P.CHEQUE_COLLECTED_DATE,'YYYY/MM/DD') AS CHEQUE_COLLECTED_DATE,"
                    + "P.STATUS ";

            String whereClous = "   P.PAYMENT_ID=D.PAYMENT_ID AND W.WORK_ORDER_NO=D.WORK_ORDER_NO AND   " + PaymenetId + " " + Amount + " " + PayDate + " " + PoNo + " " + PoDate + " " + CheqNo + " " + CheqDate + " " + Status + "  " + BisId;

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WORK_ORDER_PAYMENTS P, SD_WORK_ORDER_PAYMENTS_DTL D, SD_WORK_ORDERS W WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WORK_ORDER_PAYMENTS P, SD_WORK_ORDER_PAYMENTS_DTL D, SD_WORK_ORDERS W  WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("XXXXXXXXXXXXX");
            System.out.println(qry);
            System.out.println("XXXXXXXXXXXXX");
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            MessageWrapper msgWrap = WorkPaymentController.getWorkOrderPaymentList("all", "all", "all", "all", 0, "all", "all", 0, 10000);
            ArrayList<WorkOrderPaymentDetail> workOrders = msgWrap.getData();

            while (rs.next()) {
                WorkOrderPayment workPay = new WorkOrderPayment();
                workPay.setPaymentId(rs.getInt("PAYMENT_ID"));
                workPay.setAmount(rs.getInt("AMOUNT"));
                workPay.setPaymentDate(rs.getString("PAYMENT_DATE"));

                workPay.setPoNo(rs.getString("PO_NUMBER"));
                workPay.setPoDate(rs.getString("PO_DATE"));
                workPay.setChequeNo(rs.getString("CHEQUE_NO"));
                workPay.setChequeDate(rs.getString("CHEQUE_COLLECTED_DATE"));

                workPay.setStatus(rs.getInt("STATUS"));

                ArrayList<WorkOrderPaymentDetail> paymentDetails = new ArrayList<>();
                for (WorkOrderPaymentDetail d : workOrders) {
                    if (d.getPaymentId() == rs.getInt("PAYMENT_ID")) {
                        paymentDetails.add(d);
                    }
                }
                workPay.setWoPaymentDetials(paymentDetails);

                totalrecode = rs.getInt("CNT");

                workOrderPaymentList.add(workPay);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderPaymentList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderPaymentList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    ////////////New Change//////////////////////
    public static MessageWrapper getWorkOrderPaymentList_for_Grid(int bisId,
            int paymentId,
            double amount,
            String payDate,
            String poNo,
            String poDate,
            String cheqNo,
            String cheqDate,
            int status,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderPaymentList_New Method Call....................");
        ArrayList<WorkOrderPayment> workOrderPaymentList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrderPayment(order);

            String BisId = (bisId == 0 ? " " : " AND P.PAY_BIS_ID = " + bisId + " ");
            String PaymenetId = (paymentId == 0 ? "P.PAYMENT_ID=P.PAYMENT_ID" : "P.PAYMENT_ID=" + paymentId + "");
            String Amount = (amount == 0 ? "  " : " AND P.AMOUNT=" + amount + "");
            String PayDate = (payDate.equalsIgnoreCase("all") ? "  " : " AND TO_CHAR(P.PAYMENT_DATE , 'YYYY/MM/DD')  = '" + payDate + "'");
            String PoNo = (poNo.equalsIgnoreCase("all") ? "  " : " AND P.PO_NUMBER='" + poNo + "'");
            String PoDate = (poDate.equalsIgnoreCase("all") ? "  " : " AND TO_CHAR(P.PO_DATE , 'YYYY/MM/DD')  = '" + poDate + "'");
            String CheqNo = (poNo.equalsIgnoreCase("all") ? "  " : " AND P.CHEQUE_NO='" + cheqNo + "'");
            String CheqDate = (cheqDate.equalsIgnoreCase("all") ? "  " : " AND TO_CHAR(P.CHEQUE_COLLECTED_DATE , 'YYYY/MM/DD')  = '" + cheqDate + "'");

            String Status = (status == 0 ? "" : " AND P.STATUS = " + status + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY P.PAYMENT_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "P.PAYMENT_ID,"
                    + "P.AMOUNT,"
                    + "TO_CHAR(P.PAYMENT_DATE,'YYYY/MM/DD') AS PAYMENT_DATE,"
                    + "P.PO_NUMBER,"
                    + "TO_CHAR(P.PO_DATE,'YYYY/MM/DD') AS PO_DATE,"
                    + "P.CHEQUE_NO,"
                    + "TO_CHAR(P.CHEQUE_COLLECTED_DATE,'YYYY/MM/DD') AS CHEQUE_COLLECTED_DATE,"
                    + "P.STATUS ";

            String whereClous = "   " + PaymenetId + " " + Amount + " " + PayDate + " " + PoNo + " " + PoDate + " " + CheqNo + " " + CheqDate + " " + Status + "  " + BisId;

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WORK_ORDER_PAYMENTS P WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WORK_ORDER_PAYMENTS P  WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("###");
            System.out.println("WWWOOOO Payment"+qry);
            System.out.println("###");

            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            MessageWrapper msgWrap = WorkPaymentController.getWorkOrderPaymentList("all", "all", "all", "all", 0, "all", "all", 0, 10000);
            ArrayList<WorkOrderPaymentDetail> workOrders = msgWrap.getData();

            while (rs.next()) {
                WorkOrderPayment workPay = new WorkOrderPayment();
                workPay.setPaymentId(rs.getInt("PAYMENT_ID"));
                workPay.setAmount(rs.getDouble("AMOUNT"));
                workPay.setPaymentDate(rs.getString("PAYMENT_DATE"));

                workPay.setPoNo(rs.getString("PO_NUMBER"));
                workPay.setPoDate(rs.getString("PO_DATE"));
                workPay.setChequeNo(rs.getString("CHEQUE_NO"));
                workPay.setChequeDate(rs.getString("CHEQUE_COLLECTED_DATE"));

                workPay.setStatus(rs.getInt("STATUS"));

                ArrayList<WorkOrderPaymentDetail> paymentDetails = new ArrayList<>();
                for (WorkOrderPaymentDetail d : workOrders) {
                    if (d.getPaymentId() == rs.getInt("PAYMENT_ID")) {
                        paymentDetails.add(d);
                    }
                }
                workPay.setWoPaymentDetials(paymentDetails);

                totalrecode = rs.getInt("CNT");

                workOrderPaymentList.add(workPay);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderPaymentList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderPaymentList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderPaymentList_grid(int bisId,
            int paymentId,
            int status,
            int start,
            int limit) {

        logger.info("getWorkOrderPaymentList_grid Method Call....................");
        ArrayList<WorkOrderPaymentDetail> workOrderPaymentList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {
            String BisId = (bisId == 0 ? " " : " AND WOPD.PAY_BIS_ID = " + bisId + " ");
            String PaymenetId = (paymentId == 0 ? "WOPD.PAYMENT_ID=WOPD.PAYMENT_ID" : "" + paymentId + "");

            String seletColunm = "WO.WORK_ORDER_NO,"
                    + "WO.WARRANTY_VRIF_TYPE,"
                    + "WO.IMEI_NO,"
                    + "WOPD.PAYMENT_ID,"
                    + "DC.MAJ_CODE_DESC";

            String whereClous = "WOPD.WORK_ORDER_NO=WO.WORK_ORDER_NO"
                    + " AND DC.MAJ_CODE=WOPD.DEFECT_TYPE"
                    + " AND WOPD.PAYMENT_ID=" + PaymenetId;

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WORK_ORDER_PAYMENTS_DTL WOPD,SD_WORK_ORDERS WO,SD_DEFECT_CODES DC WHERE "
                    + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + "ORDER BY WOPD.PAYMENT_ID " + " ) RN "
                    + " FROM SD_WORK_ORDER_PAYMENTS_DTL WOPD,SD_WORK_ORDERS WO,SD_DEFECT_CODES DC  WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("ssssssssssss4");
            System.out.println(qry);
            System.out.println("ssssssssssss4");

            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                WorkOrderPaymentDetail workPay = new WorkOrderPaymentDetail();

                workPay.setPaymentId(rs.getInt("PAYMENT_ID"));
                workPay.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                workPay.setWarrantyVerifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                workPay.setImeiNo(rs.getString("IMEI_NO"));
                workPay.setDefectDesc(rs.getString("MAJ_CODE_DESC"));

                totalrecode = rs.getInt("CNT");

                workOrderPaymentList.add(workPay);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderPaymentList_grid Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderPaymentList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;

    }

}
