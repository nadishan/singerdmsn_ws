/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.DSRTrack;

/**
 *
 * @author SDU
 */
public class DSRTrakingController {

    public static MessageWrapper getBussinessTrackingList(int bis_id, int shops, int subdeler, int distributor, int service) {

        logger.info("getBussinessTrackingList Method Call....................");
        ArrayList<DSRTrack> trackingList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            System.out.println("6666666666666666");
            if (shops == 9) {
                String select_clomn = "B.BIS_ID,"
                        + "B.BIS_NAME,"
                        + "B.BIS_DESC,"
                        + "B.LONGITUDE,"
                        + "B.LATITUDE,"
                        + "T.BIS_STRU_TYPE_DESC,"
                        + "B.BIS_STRU_TYPE_ID,"
                        + "B.ADDRESS,"
                        + "B.CONTACT_NO";

                String where_clos = "B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID (+)  "
                        + "AND B.STATUS != 2 "
                        + "AND B.SHOW_IN_MAP_FLAG = 1 "
                        + "AND B.BIS_STRU_TYPE_ID = " + shops + "";

                
                System.out.println("SELECT " + select_clomn + "  FROM SD_BUSINESS_STRUCTURES B,SD_BIS_STRU_TYPES T WHERE " + where_clos + "");
                
                System.out.println("1111");
                ResultSet rs_shop = dbCon.search(con,"SELECT " + select_clomn + "  FROM SD_BUSINESS_STRUCTURES B,SD_BIS_STRU_TYPES T WHERE " + where_clos + "");

                logger.info("Get Data to Resultset........");

                while (rs_shop.next()) {
                    DSRTrack tr = new DSRTrack();
                    tr.setBisId(rs_shop.getInt("BIS_ID"));
                    tr.setBisName(rs_shop.getString("BIS_NAME"));
                    tr.setBisDesc(rs_shop.getString("BIS_DESC"));
                    tr.setLogtitute(rs_shop.getString("LONGITUDE"));
                    tr.setLatitite(rs_shop.getString("LATITUDE"));
                    tr.setBisTypeDesc(rs_shop.getString("BIS_STRU_TYPE_DESC"));
                    tr.setBisTypeId(rs_shop.getInt("BIS_STRU_TYPE_ID"));
                    tr.setAddress(rs_shop.getString("ADDRESS"));
                    tr.setTelePhoneNum(rs_shop.getString("CONTACT_NO"));
                    trackingList.add(tr);
                }
               // rs_shop.close();
            }

            if (service == 8) {

                String select_clomn = "B.BIS_ID,"
                        + "B.BIS_NAME,"
                        + "B.BIS_DESC,"
                        + "B.LONGITUDE,"
                        + "B.LATITUDE,"
                        + "T.BIS_STRU_TYPE_DESC,"
                        + "B.BIS_STRU_TYPE_ID";

                String where_clos = "B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID (+)  "
                        + "AND B.STATUS != 2 "
                        + "AND B.SHOW_IN_MAP_FLAG = 1 "
                        + "AND B.BIS_STRU_TYPE_ID = " + service + "";

                ResultSet rs_shop = dbCon.search(con,"SELECT " + select_clomn + "  FROM SD_BUSINESS_STRUCTURES B,SD_BIS_STRU_TYPES T WHERE " + where_clos + "");

                logger.info("Get Data to Resultset........");

                while (rs_shop.next()) {
                    DSRTrack tr = new DSRTrack();
                    tr.setBisId(rs_shop.getInt("BIS_ID"));
                    tr.setBisName(rs_shop.getString("BIS_NAME"));
                    tr.setBisDesc(rs_shop.getString("BIS_DESC"));
                    tr.setLogtitute(rs_shop.getString("LONGITUDE"));
                    tr.setLatitite(rs_shop.getString("LATITUDE"));
                    tr.setBisTypeDesc(rs_shop.getString("BIS_STRU_TYPE_DESC"));
                    tr.setBisTypeId(rs_shop.getInt("BIS_STRU_TYPE_ID"));
                    trackingList.add(tr);
                }
                //rs_shop.close();
            }
            if (bis_id != 0) {

                String condition = "";
                // condition = condition + (shops == 0 ? "" : "" + shops + ",");
                condition = condition + (subdeler == 0 ? "" : "" + subdeler + ",");
                condition = condition + (distributor == 0 ? "" : "" + distributor + ",");
                // condition = condition + (service == 0 ? "" : "" + service + ",");

                String clous = "";
                if (condition.equals("")) {
                    clous = "";
                } else {
                    if (condition.length() > 0) {
                        condition = condition.substring(0, condition.length() - 1);
                    }
                    clous = "AND B.BIS_STRU_TYPE_ID IN (" + condition + ")";
                }

                if (clous.equals("")) {
                    logger.info("No Condition to Get Data.......");
                } else {

                    String seletColunm = "B.BIS_ID,"
                            + "B.BIS_NAME,"
                            + "B.BIS_DESC,"
                            + "B.LONGITUDE,"
                            + "B.LATITUDE,"
                            + "T.BIS_STRU_TYPE_DESC,"
                            + "B.BIS_STRU_TYPE_ID";

                    String whereClous = "B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID (+)  "
                            + " AND B.STATUS != 2 "
                            + " AND B.SHOW_IN_MAP_FLAG = 1 "
                            + " " + clous + "";

                    String tables = "(SELECT * FROM SD_BUSINESS_STRUCTURES START WITH BIS_ID =" + bis_id + " CONNECT BY PRIOR   BIS_ID = PARENT_BIS_ID) B ,"
                            + "SD_BIS_STRU_TYPES T";

                    
                    System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                            + "" + seletColunm + " "
                            + " FROM " + tables + " WHERE " + whereClous + ")");
                    
                    
                    ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                            + "" + seletColunm + " "
                            + " FROM " + tables + " WHERE " + whereClous + ")");

                    logger.info("Get Data to Resultset...");

                    while (rs.next()) {
                        DSRTrack tr = new DSRTrack();
                        totalrecode = rs.getInt("CNT");
                        tr.setBisId(rs.getInt("BIS_ID"));
                        tr.setBisName(rs.getString("BIS_NAME"));
                        tr.setBisDesc(rs.getString("BIS_DESC"));
                        tr.setLogtitute(rs.getString("LONGITUDE"));
                        tr.setLatitite(rs.getString("LATITUDE"));
                        tr.setBisTypeDesc(rs.getString("BIS_STRU_TYPE_DESC"));
                        tr.setBisTypeId(rs.getInt("BIS_STRU_TYPE_ID"));

                        trackingList.add(tr);
                    }
                   // rs.close();

                }
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessTrackingList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(trackingList.size());
        mw.setData(trackingList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getTrackingEnableList(int bisId, int bisTypeId) {

        logger.info("getTrackingEnableList Method Call....................");
        ArrayList<DSRTrack> trackingList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            
            String seletColunm = "U.BIS_ID,B.PARENT_BIS_ID,B1.BIS_NAME,U.USER_ID,U.FIRST_NAME,U.LAST_NAME,U.ENABLE_TRACKING";

            String whereClous = "U.BIS_ID = B.BIS_ID (+) "
                    + "AND B.PARENT_BIS_ID = B1.BIS_ID (+) "
                    + "AND U.BIS_ID IN (SELECT BIS_ID FROM (SELECT *  FROM SD_BUSINESS_STRUCTURES "
                    + "START WITH BIS_ID = " + bisId + " "
                    + "CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID) "
                    + "WHERE BIS_STRU_TYPE_ID = " + bisTypeId + ")";

            String tables = "SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES  B1 ";


            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + "  WHERE  " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                DSRTrack tr = new DSRTrack();
                totalrecode = rs.getInt("CNT");
                tr.setBisId(rs.getInt("BIS_ID"));
                tr.setUserId(rs.getString("USER_ID"));
                tr.setFirstName(rs.getString("FIRST_NAME"));
                tr.setLastName(rs.getString("LAST_NAME"));
                tr.setEnableFlag(rs.getInt("ENABLE_TRACKING"));
                tr.setParantName(rs.getString("BIS_NAME"));
                trackingList.add(tr);
            }

        } catch (Exception ex) {
            logger.info("Error getTrackingEnableList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(trackingList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addEnableTracking(ArrayList<DSRTrack> trackingList,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addEnableTracking Method Call............");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_dt = dbCon.prepare(con,"UPDATE SD_USER_BUSINESS_STRUCTURES  "
                    + "SET ENABLE_TRACKING = ?,"
                    + "USER_MODIFIED = ?,"
                    + "DATE_MODIFIED = SYSDATE "
                    + "WHERE USER_ID = ?");

            for (DSRTrack dt : trackingList) {

                ps_dt.setInt(1, dt.getEnableFlag());
                ps_dt.setString(2, user_id);
                ps_dt.setString(3, dt.getUserId());
                ps_dt.addBatch();

            }

            ps_dt.executeBatch();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in addEnableTracking Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Tracking Enable Successfully");
            logger.info("Tracking Enable Successfully............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Tracking Enable");
            logger.info("Error in Tracking Enable..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addLocationTracking(DSRTrack tracking_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addLocationTracking Method Call............");
        int result = 0;
        int max_seq = 0;
        int enable_flag = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            

            
            ResultSet rs_get_Enable = dbCon.search(con,"SELECT ENABLE_TRACKING "
                    + "FROM SD_USER_BUSINESS_STRUCTURES "
                    + "WHERE USER_ID ='"+user_id+"'");
            
            while(rs_get_Enable.next()){
                enable_flag = rs_get_Enable.getInt("ENABLE_TRACKING");
            }
          //  rs_get_Enable.close();
            


            ResultSet rs_max = dbCon.search(con,"SELECT NVL(MAX(SEQ_NO),0)+1 MAX FROM SD_MOVEMENT");

            if (rs_max.next()) {
                max_seq = rs_max.getInt("MAX");
            }
           // rs_max.close();

            String datetime = tracking_info.getVisitDate();
            String fromatedDate = datetime.substring(0, 10) + " " + datetime.substring(11, 16);

            String column = "SEQ_NO,"
                    + "BIS_ID,"
                    + "LONGITUDE,"
                    + "LATITUDE,"
                    + "LOCATION_VISIT_DATE,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED,"
                    + "USER_ID,"
                    + "TRACKING_ENABLE";

            String values = "?,?,?,?,TO_DATE(?,'YYYY/MM/DD HH24:MI:SS'),?,SYSDATE,?,?";

            PreparedStatement ps = dbCon.prepare(con,"INSERT INTO SD_MOVEMENT(" + column + ") VALUES(" + values + ")");

            ps.setInt(1, max_seq);
            ps.setInt(2, tracking_info.getBisId());
            ps.setString(3, tracking_info.getLogtitute());
            ps.setString(4, tracking_info.getLatitite());
            ps.setString(5, fromatedDate);
            ps.setString(6, user_id);
            ps.setString(7, user_id);
            ps.setInt(8, enable_flag);
            ps.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in addLocationTracking Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Tracking Location Insert Successfully");
            logger.info("Tracking Location Insert Successfully............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Tracking Location Insert");
            logger.info("Error in Tracking Location Insert..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getTrackingHistoryList(int bisId, String from_date, String to_date,String uId, int start, int limit) {

        logger.info("getTrackingHistoryList Method Call....................");
        ArrayList<DSRTrack> trackingList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String BisId = (bisId == 0 ? "" : "AND M.BIS_ID  = " + bisId + "");
            String UserId = (uId.equalsIgnoreCase("all") ? "NVL(M.USER_INSERTED,'AA') = NVL(M.USER_INSERTED,'AA')" : "M.USER_INSERTED = '" + uId + "'");
            String fromDate = (from_date.equalsIgnoreCase("all") ? "NVL(M.LOCATION_VISIT_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(M.LOCATION_VISIT_DATE,TO_DATE('20100101','YYYYMMDD'))" : "M.LOCATION_VISIT_DATE >= TO_DATE('" + from_date + "','YYYY/MM/DD HH24:MI')");
            String toDate = (to_date.equalsIgnoreCase("all") ? "NVL(M.LOCATION_VISIT_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(M.LOCATION_VISIT_DATE,TO_DATE('20100101','YYYYMMDD'))" : "M.LOCATION_VISIT_DATE <= TO_DATE('" + to_date + "','YYYY/MM/DD HH24:MI')");

            String seletColunm = "M.SEQ_NO,M.BIS_ID,"
                    + "M.LONGITUDE,M.LATITUDE,"
                    + "M.VISITOR_BIS_ID,"
                    + "M.LOCATION_VISIT_DATE,"
                    + "S.BIS_NAME,"
                    + "S.ADDRESS,"
                    + "S.CONTACT_NO,"
                    + "M.USER_INSERTED";

            String whereClous = "M.USER_ID = B.USER_ID (+)  "
                    + " AND M.BIS_ID = S.BIS_ID (+) "
                    + " AND M.TRACKING_ENABLE = 1 "
                    + " AND " + fromDate + " "
                    + " AND " + toDate + " "
                    + " AND " + UserId + " "
                    + " " + BisId + "";

            String tables = "SD_MOVEMENT M,SD_USER_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES S";
 
            
            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY M.LOCATION_VISIT_DATE ASC) RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                DSRTrack tr = new DSRTrack();
                totalrecode = rs.getInt("CNT");
                tr.setSeqNo(rs.getInt("SEQ_NO"));
                tr.setBisId(rs.getInt("BIS_ID"));
                tr.setLogtitute(rs.getString("LONGITUDE"));
                tr.setLatitite(rs.getString("LATITUDE"));
                tr.setVistiorBisId(rs.getInt("VISITOR_BIS_ID"));
                tr.setVisitDate(rs.getString("LOCATION_VISIT_DATE"));
                tr.setBisName(rs.getString("BIS_NAME"));
                tr.setUserId(rs.getString("USER_INSERTED"));
                trackingList.add(tr);
            }

        } catch (Exception ex) {
            logger.info("Error getTrackingHistoryList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(trackingList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getLastTrackingDSRList(int bisId, int bis_type) {

        logger.info("getLastTrackingDSRList Method Call....................");
        ArrayList<DSRTrack> trackingList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            
            
             ResultSet rs_buss_type = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_BUSINESS_STRUCTURES  "
                    + "WHERE BIS_STRU_TYPE_ID = "+bis_type+" "
                    + "START WITH BIS_ID = "+bisId+" "
                    + "CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID");
            
            while(rs_buss_type.next()){
                totalrecode = rs_buss_type.getInt("CNT");
            }
            
            

            ResultSet rs = dbCon.search(con,"SELECT M.SEQ_NO,"
                    + " M.BIS_ID,"
                    + " M.LONGITUDE,"
                    + " M.LATITUDE,"
                    + " S.BIS_NAME,"
                    + " S.BIS_DESC,"
                    + " TO_CHAR(M.LOCATION_VISIT_DATE,'YYYY/MM/DD HH24:MI:SS') LOCATION_VISIT_DATE,"
                    + " M.USER_ID,T.FIRST_NAME,T.LAST_NAME,S.CONTACT_NO,S.ADDRESS,Z.BIS_STRU_TYPE_DESC,"
                    + " S1.BIS_NAME DISTRIBUTOR "
                    + " FROM SD_USER_BUSINESS_STRUCTURES T,SD_BUSINESS_STRUCTURES S,SD_BUSINESS_STRUCTURES S1,SD_BIS_STRU_TYPES Z,SD_MOVEMENT M,(SELECT USER_ID,TO_CHAR(LDATE,'YYYY/MM/DD HH24:MI:SS') LDATE "
                    + " FROM (SELECT USER_ID,MAX(LOCATION_VISIT_DATE) LDATE "
                    + " FROM SD_MOVEMENT "
                    + " WHERE BIS_ID IN (SELECT BIS_ID "
                    + " FROM (SELECT * "
                    + " FROM SD_BUSINESS_STRUCTURES "
                    + " START WITH BIS_ID = " + bisId + " "
                    + " CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID) "
                    + " WHERE BIS_STRU_TYPE_ID IN(" + bis_type + ")) "
                    + " GROUP BY  USER_ID)) B "
                    + " WHERE M.USER_ID = B.USER_ID (+)"
                    + " AND M.USER_ID = T.USER_ID (+)"
                    + " AND TO_CHAR(M.LOCATION_VISIT_DATE,'YYYY/MM/DD HH24:MI:SS') = B.LDATE "
                    + " AND T.BIS_ID = S.BIS_ID (+) "
                    + " AND S.PARENT_BIS_ID = S1.BIS_ID (+) "
                    + " AND S.BIS_STRU_TYPE_ID = Z.BIS_STRU_TYPE_ID (+) "
                    + " ORDER BY M.LOCATION_VISIT_DATE ASC");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                DSRTrack tr = new DSRTrack();
                tr.setSeqNo(rs.getInt("SEQ_NO"));
                tr.setBisId(rs.getInt("BIS_ID"));
                tr.setBisName(rs.getString("BIS_NAME"));
                tr.setBisDesc(rs.getString("BIS_DESC"));
                tr.setLogtitute(rs.getString("LONGITUDE"));
                tr.setLatitite(rs.getString("LATITUDE"));
                tr.setVisitDate(rs.getString("LOCATION_VISIT_DATE"));
                tr.setUserId(rs.getString("USER_ID"));
                tr.setFirstName(rs.getString("FIRST_NAME"));
                tr.setLastName(rs.getString("LAST_NAME"));
                tr.setTelePhoneNum(rs.getString("CONTACT_NO"));
                tr.setParantName(rs.getString("DISTRIBUTOR"));
                tr.setAddress(rs.getString("ADDRESS"));
                tr.setBisTypeDesc(rs.getString("BIS_STRU_TYPE_DESC"));
                tr.setBisTypeId(bis_type);
                trackingList.add(tr);
            }

        } catch (Exception ex) {
            logger.info("Error getLastTrackingDSRList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(trackingList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

}
