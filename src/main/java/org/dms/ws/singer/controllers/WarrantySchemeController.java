/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.WarrantyScheme;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WarrantySchemeController {

    public static MessageWrapper getWarrantySchemeList(int schemeId,
            String schemeName,
            int period,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getWarrantySchemeDetails Mehtod Call.......");
        int totalrecode = 0;
        ArrayList<WarrantyScheme> warrantySchemeList = new ArrayList<WarrantyScheme>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            order = DBMapper.warrentSchema(order);

            String SchemeId = (schemeId == 0 ? "" : "AND SCHEME_ID = " + schemeId + " ");
            String ShemeName = (schemeName.equalsIgnoreCase("all") ? " NVL(SCHEME_NAME,'AA') = NVL(SCHEME_NAME,'AA')" : "  UPPER(SCHEME_NAME) LIKE UPPER('%" + schemeName + "%')");
            String Period = (period == 0 ? "" : " AND PERIOD = " + period + " ");
            String Status = (status == 0 ? "  AND STATUS =1 " : " AND STATUS = " + status + " ");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY SCHEME_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = " SCHEME_ID, SCHEME_NAME, PERIOD, STATUS ";

            String whereClous = " " + ShemeName + " "
                    + " " + SchemeId + " "
                    + " " + Period + " "
                    + " " + Status + " ";

            String selectQry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WARRANTY_SCHEMES WHERE   " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_WARRANTY_SCHEMES  "
                    + "WHERE   " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            //System.out.println(selectQry);
            ResultSet rs = dbCon.search(con, selectQry);

            logger.info("Get Data to ResultSet............");

            while (rs.next()) {
                WarrantyScheme warSch = new WarrantyScheme();
                totalrecode = rs.getInt("CNT");
                warSch.setSchemeId(rs.getInt("SCHEME_ID"));
                warSch.setSchemeName(rs.getString("SCHEME_NAME"));
                warSch.setPeriod(rs.getInt("PERIOD"));
                warSch.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    warSch.setStatusMsg("Active");
                } else {
                    warSch.setStatusMsg("Inactive");
                }
                warrantySchemeList.add(warSch);
            }

        } catch (Exception ex) {
            logger.info("Error in getWarrantySchemeDetails mehtod Error... :" + ex.getMessage());
        }
        logger.info("Ready Return Values.............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(warrantySchemeList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper getWarrantyDays(String imei_no,
            String date_of_sales) {
        logger.info("getWarrantyDays Mehtod Call.......");
        int imei_cnt = 0;
        int result = 0;
        String expire_day = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_master_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + imei_no + "'");

            while (rs_master_cnt.next()) {
                imei_cnt = rs_master_cnt.getInt("CNT");
            }
            System.out.println(imei_cnt);
            if (imei_cnt == 0) {
                result = 2;
            } else {

                ResultSet rs = dbCon.search(con, "SELECT D.IMEI_NO,"
                        + "W.PERIOD,"
                        + "A.EXTENDED_DAYS,"
                        + "(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0)) TOTAL,"
                        + "TO_CHAR(((TO_DATE('2014/11/30','YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD') EXPIRE_DAY  "
                        + "FROM SD_DEVICE_MASTER D,"
                        + "SD_MODEL_LISTS M,"
                        + "SD_WARRANTY_SCHEMES W,"
                        + "SD_WARRANTY_ADJUSTMENT A "
                        + "WHERE D.MODEL_NO = M.MODEL_NO (+) "
                        + "AND M.WRN_SCHEME_ID = W.SCHEME_ID (+) "
                        + "AND D.IMEI_NO = A.IMEI (+) "
                        + "AND D.IMEI_NO = '" + imei_no + "'");

                logger.info("Get Data to ResultSet............");

                while (rs.next()) {
                    expire_day = rs.getString("EXPIRE_DAY");

                }
                result = 1;
            }
        } catch (Exception ex) {
            logger.info("Error in getWarrantySchemeDetails mehtod Error... :" + ex.getMessage());
            result = 9;
        }
        logger.info("Ready Return Values.............");
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg(expire_day);
            vw.setReturnRefNo(expire_day);
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("This IMEI not in Master Record");
        }

        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addWarrantySchema(WarrantyScheme wSchemeInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWarrantySchema Method Call.....");
        int result = 0;
        int max_schme_id = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_maxschema = dbCon.search(con, "SELECT NVL(MAX(SCHEME_ID),0) MAX FROM SD_WARRANTY_SCHEMES");
            while (rs_maxschema.next()) {
                max_schme_id = rs_maxschema.getInt("MAX");
            }

            max_schme_id = max_schme_id + 1;

            String wschrmacol = "SCHEME_ID,SCHEME_NAME,PERIOD,STATUS,USER_INSERTED,DATE_INSERTED";

            String wschrma_values = "?,?,?,?,?,SYSDATE";
            PreparedStatement ps_in_shema = dbCon.prepare(con, "INSERT INTO SD_WARRANTY_SCHEMES(" + wschrmacol + ") VALUES(" + wschrma_values + ")");
            ps_in_shema.setInt(1, max_schme_id);
            ps_in_shema.setString(2, wSchemeInfo.getSchemeName());
            ps_in_shema.setInt(3, wSchemeInfo.getPeriod());
            ps_in_shema.setInt(4, wSchemeInfo.getStatus());
            ps_in_shema.setString(5, user_id);

            ps_in_shema.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in addWarrantySchema Method  :" + ex.getMessage());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Schema Insert Successfully");
            logger.info("Warranty Schema Insert Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Schema Insert");
            logger.info("Error in Warranty Schema Insert.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editWarrantySchema(WarrantyScheme wSchemeInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editWarrantySchema Method Call.....");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_shema = dbCon.prepare(con, "UPDATE SD_WARRANTY_SCHEMES SET SCHEME_NAME = ?,"
                    + " PERIOD =?,"
                    + " STATUS = ?,"
                    + " USER_MODIFIED = ?,"
                    + " DATE_MODIFIED = SYSDATE"
                    + " WHERE SCHEME_ID = ?");

            ps_up_shema.setString(1, wSchemeInfo.getSchemeName());
            ps_up_shema.setInt(2, wSchemeInfo.getPeriod());
            ps_up_shema.setInt(3, wSchemeInfo.getStatus());
            ps_up_shema.setString(4, user_id);
            ps_up_shema.setInt(5, wSchemeInfo.getSchemeId());
            ps_up_shema.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in editWarrantySchema Method  :" + ex.getMessage());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Schema Update Successfully");
            logger.info("Warranty Schema Update Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Schema Update");
            logger.info("Error in Warranty Schema Update.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getImeiMasterDetailswithWarranty(String imeino) {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int imeiPendingWorkOrders = 0;
        int imei_cnt = 0;
        int wart_cnt = 0;
        int result = 0;
        String reg_date = "";
        try {

            String qry = " SELECT COUNT(*) CNT     "
                    + " FROM SD_WORK_ORDERS     "
                    + " WHERE STATUS=1          "
                    + " AND   IMEI_NO='" + imeino + "' ";

            ResultSet chkImeiWOt = dbCon.search(con, qry);

            while (chkImeiWOt.next()) {
                imeiPendingWorkOrders = chkImeiWOt.getInt("CNT");
            }

            ResultSet rs_master_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + imeino + "'");

            while (rs_master_cnt.next()) {
                imei_cnt = rs_master_cnt.getInt("CNT");
            }

            // System.out.println(imeiPendingWorkOrders+ " : IMEI Count : "+imei_cnt);
            if (imeiPendingWorkOrders == 0) {
                if (imei_cnt > 0) {
                    String regqry = "SELECT TO_CHAR(REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE,"
                            + "COUNT(*) CNT "
                            + "FROM SD_WARRENTY_REGISTRATION "
                            + "WHERE IMEI_NO = '" + imeino + "' "
                            + "GROUP BY REGISTER_DATE";

                    ResultSet rs_waranty_cnt1 = dbCon.search(con, regqry);

                    while (rs_waranty_cnt1.next()) {
                        wart_cnt = rs_waranty_cnt1.getInt("CNT");
                        reg_date = rs_waranty_cnt1.getString("REGISTER_DATE");
                    }
                    System.out.println("Warranty Count: " + wart_cnt);
                    if (wart_cnt > 0) {

                        ResultSet rs_expiredate = dbCon.search(con, "SELECT D.IMEI_NO,"
                                + "W.PERIOD,"
                                + "A.EXTENDED_DAYS,"
                                + "(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0)) TOTAL,"
                                + "TO_CHAR(((TO_DATE('" + reg_date + "','YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD') EXPIRE_DAY  "
                                + "FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A "
                                + "WHERE D.MODEL_NO = M.MODEL_NO (+) "
                                + "AND M.WRN_SCHEME_ID = W.SCHEME_ID (+) "
                                + "AND D.IMEI_NO = A.IMEI (+) "
                                + "AND D.IMEI_NO = '" + imeino + "'");
                        ArrayList<ImeiMaster> temMasterList = new ArrayList<>();
                        while (rs_expiredate.next()) {
                            ImeiMaster im = new ImeiMaster();
                            im.setImeiNo(rs_expiredate.getString("IMEI_NO"));
                            im.setWarrantyExpireDate(rs_expiredate.getString("EXPIRE_DAY"));

                            System.out.println(reg_date + " => Warranty Calculation: " + rs_expiredate.getString("PERIOD") + " + " + rs_expiredate.getString("EXTENDED_DAYS") + " = " + rs_expiredate.getString("EXPIRE_DAY"));

                            String warantyQry = " SELECT COUNT(*) CNT      "
                                    + " FROM SD_DEVICE_MASTER   "
                                    + " WHERE WARRANTY_STATUS=1 "
                                    + " AND TO_DATE('" + rs_expiredate.getString("EXPIRE_DAY") + "','YYYY/MM/DD') >= TO_DATE(TO_CHAR(SYSDATE,'YYYY/MM/DD'),'YYYY/MM/DD')   "
                                    + " AND IMEI_NO = '" + imeino + "' ";

                            ResultSet rs_cnt = dbCon.search(con, warantyQry);

                            int warrantyStatus = 2;
                            while (rs_cnt.next()) {
                                //   System.out.println(rs_cnt.getInt("CNT"));
                                if (rs_cnt.getInt("CNT") > 0) {
                                    warrantyStatus = 1;
                                }

                            }
                            im.setExpireStatus(warrantyStatus);

                            System.out.println("Expires on : " + warrantyStatus);

                            temMasterList.add(im);
                        }

                        String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                                + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                                + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                                + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,L.WRN_SCHEME_ID";

                        String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                                + "AND M.MODEL_NO = L.MODEL_NO (+)"
                                + "AND M.IMEI_NO = '" + imeino + "'";

                        String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L";

                        ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM " + table + " WHERE " + whereClous + "  ");

                        logger.info("Get Data to ResultSet...........");
                        while (rs.next()) {
                            ImeiMaster im = new ImeiMaster();
                            im.setImeiNo(rs.getString("IMEI_NO"));
                            im.setModleNo(rs.getInt("MODEL_NO"));
                            im.setBisId(rs.getInt("BIS_ID"));
                            im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                            im.setBrand(rs.getString("BRAND"));
                            im.setProduct(rs.getString("PRODUCT"));
                            im.setLocation(rs.getString("LOCATION"));
                            im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                            im.setStatus(rs.getInt("STATUS"));
                            im.setBisName(rs.getString("BIS_NAME"));
                            im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            im.setWarrantyScheme(rs.getInt("WRN_SCHEME_ID"));
                            im.setPurchaseDate(reg_date);

                            for (ImeiMaster ime : temMasterList) {
                                if (ime.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                                    im.setWarrantyExpireDate(ime.getWarrantyExpireDate());
//                                    im.setExpireStatus(ime.getExpireStatus());
//                                    change by alter
                                    if (ime.getExpireStatus() == 1) {
                                        im.setExpireStatus(0);
//                                        im.setExpireStatus(1);
                                    } else {
                                        im.setExpireStatus(1);
//                                        im.setExpireStatus(0);
                                    }
                                }
                            }
                            imeiMasterList.add(im);
                            result = 1;
                        }
                        
                        System.out.println("WR Count 111111111111111111");

                    } else {
                           System.out.println("WR Count 2");
                        String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                                + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                                + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                                + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,L.WRN_SCHEME_ID";

                        String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                                + "AND M.MODEL_NO = L.MODEL_NO (+)"
                                + "AND M.IMEI_NO = '" + imeino + "'";

                        String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L";

                        ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM " + table + " WHERE " + whereClous + "  ");

                        logger.info("Get Data to ResultSet...........");
                        while (rs.next()) {
                            ImeiMaster im = new ImeiMaster();
                            im.setImeiNo(rs.getString("IMEI_NO"));
                            im.setModleNo(rs.getInt("MODEL_NO"));
                            im.setBisId(rs.getInt("BIS_ID"));
                            im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                            im.setBrand(rs.getString("BRAND"));
                            im.setProduct(rs.getString("PRODUCT"));
                            im.setLocation(rs.getString("LOCATION"));
                            im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                            im.setStatus(rs.getInt("STATUS"));
                            im.setBisName(rs.getString("BIS_NAME"));
                            im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            im.setWarrantyScheme(rs.getInt("WRN_SCHEME_ID"));
                            im.setPurchaseDate(reg_date);

                            imeiMasterList.add(im);
                            result = 1;
                        }

                        result = 3;
                    }
                } else {
                    result = 2;
                }
            } else {
                result = 4;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }
        totalrecode = result;
        MessageWrapper mw = new MessageWrapper();
        if(con == null){
            mw.setTotalRecords(1000000);
        }else{
            mw.setTotalRecords(totalrecode);
        }
        
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiMasterDetails(String imeino, int warrentyType) {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int imeiPendingWorkOrders = 0;
        int imei_cnt = 0;
        int wart_cnt = 0;
        int result = 0;
        String reg_date = "";
        try {

            String qry = " SELECT COUNT(*) CNT     "
                    + " FROM SD_WORK_ORDERS     "
                    + " WHERE STATUS=1          "
                    + " AND   IMEI_NO='" + imeino + "' ";

            ResultSet chkImeiWOt = dbCon.search(con, qry);

            while (chkImeiWOt.next()) {
                imeiPendingWorkOrders = chkImeiWOt.getInt("CNT");
            }

            ResultSet rs_master_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + imeino + "'");

            while (rs_master_cnt.next()) {
                imei_cnt = rs_master_cnt.getInt("CNT");
            }

            // System.out.println(imeiPendingWorkOrders+ " : IMEI Count : "+imei_cnt);
            if (imeiPendingWorkOrders == 0) {
                if (imei_cnt > 0) {
                    
                    if(warrentyType == 3){
                        
                        String sql = "SELECT sdm.PRODUCT,"
                                + " sdm.BRAND,"
                                + " sdml.MODEL_DESCRIPTION,"
                                + " TO_CHAR(sdm.PURCHASE_DATE,'YYYY/MM/DD') AS salesDate "
                                + " FROM SD_DEVICE_MASTER sdm, SD_MODEL_LISTS sdml"
                                + " WHERE sdm.MODEL_NO = sdml.MODEL_NO AND sdm.IMEI_NO = '"+imeino+"'";
                        
                        ResultSet product_info = dbCon.search(con, sql);
                        
                        
                        while (product_info.next()) {
                            
                            ImeiMaster im = new ImeiMaster();
                            im.setProduct(product_info.getString("PRODUCT"));
                            im.setBrand(product_info.getString("BRAND"));
                            im.setModleDesc(product_info.getString("MODEL_DESCRIPTION"));
                            im.setPurchaseDate(product_info.getString("salesDate"));

                            imeiMasterList.add(im);
                            
                        }
                        
                        result = 5;
                        
                    }else{
                    String regqry = "SELECT TO_CHAR(REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE,"
                            + "COUNT(*) CNT "
                            + "FROM SD_WARRENTY_REGISTRATION "
                            + "WHERE IMEI_NO = '" + imeino + "' "
                            + "GROUP BY REGISTER_DATE";

                    ResultSet rs_waranty_cnt1 = dbCon.search(con, regqry);

                    while (rs_waranty_cnt1.next()) {
                        wart_cnt = rs_waranty_cnt1.getInt("CNT");
                        reg_date = rs_waranty_cnt1.getString("REGISTER_DATE");
                    }
                    System.out.println("Warranty Count: " + wart_cnt);
                    if (wart_cnt == 0) {

                        ResultSet rs_expiredate = dbCon.search(con, "SELECT D.IMEI_NO,"
                                + "W.PERIOD,"
                                + "A.EXTENDED_DAYS,"
                                + "(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0)) TOTAL,"
                                + "TO_CHAR(((TO_DATE('" + reg_date + "','YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD') EXPIRE_DAY  "
                                + "FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A "
                                + "WHERE D.MODEL_NO = M.MODEL_NO (+) "
                                + "AND M.WRN_SCHEME_ID = W.SCHEME_ID (+) "
                                + "AND D.IMEI_NO = A.IMEI (+) "
                                + "AND D.IMEI_NO = '" + imeino + "'");
                        ArrayList<ImeiMaster> temMasterList = new ArrayList<>();
                        while (rs_expiredate.next()) {
                            ImeiMaster im = new ImeiMaster();
                            im.setImeiNo(rs_expiredate.getString("IMEI_NO"));
                            im.setWarrantyExpireDate("-");


                            int warrantyStatus = 2;
                           
                            im.setExpireStatus(warrantyStatus);

                            System.out.println("Expires on : " + warrantyStatus);

                            temMasterList.add(im);
                        }

                        String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                                + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                                + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                                + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,L.WRN_SCHEME_ID";

                        String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                                + "AND M.MODEL_NO = L.MODEL_NO (+)"
                                + "AND M.IMEI_NO = '" + imeino + "'";

                        String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L";

                        ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM " + table + " WHERE " + whereClous + "  ");

                        logger.info("Get Data to ResultSet...........");
                        while (rs.next()) {
                            ImeiMaster im = new ImeiMaster();
                            im.setImeiNo(rs.getString("IMEI_NO"));
                            im.setModleNo(rs.getInt("MODEL_NO"));
                            im.setBisId(rs.getInt("BIS_ID"));
                            im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                            im.setBrand(rs.getString("BRAND"));
                            im.setProduct(rs.getString("PRODUCT"));
                            im.setLocation(rs.getString("LOCATION"));
                            im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                            im.setStatus(rs.getInt("STATUS"));
                            im.setBisName(rs.getString("BIS_NAME"));
                            im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            im.setWarrantyScheme(rs.getInt("WRN_SCHEME_ID"));
                            im.setPurchaseDate(reg_date);

                            for (ImeiMaster ime : temMasterList) {
                                if (ime.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                                    im.setWarrantyExpireDate(ime.getWarrantyExpireDate());
//                                    im.setExpireStatus(ime.getExpireStatus());
//                                    change by alter
                                    if (ime.getExpireStatus() == 1) {
                                        im.setExpireStatus(0);
                                    } else {
                                        im.setExpireStatus(1);
                                    }
                                }
                            }
                            imeiMasterList.add(im);
                            result = 1;
                        }

                    } else {

//                        String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
//                                + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
//                                + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
//                                + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,L.WRN_SCHEME_ID";
//
//                        String whereClous = " M.BIS_ID = B.BIS_ID (+) "
//                                + "AND M.MODEL_NO = L.MODEL_NO (+)"
//                                + "AND M.IMEI_NO = '" + imeino + "'";
//
//                        String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L";
//
//                        ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM " + table + " WHERE " + whereClous + "  ");
//
//                        logger.info("Get Data to ResultSet...........");
//                        while (rs.next()) {
//                            ImeiMaster im = new ImeiMaster();
//                            im.setImeiNo(rs.getString("IMEI_NO"));
//                            im.setModleNo(rs.getInt("MODEL_NO"));
//                            im.setBisId(rs.getInt("BIS_ID"));
//                            im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
//                            im.setBrand(rs.getString("BRAND"));
//                            im.setProduct(rs.getString("PRODUCT"));
//                            im.setLocation(rs.getString("LOCATION"));
//                            im.setSalesPrice(rs.getDouble("SALES_PRICE"));
//                            im.setStatus(rs.getInt("STATUS"));
//                            im.setBisName(rs.getString("BIS_NAME"));
//                            im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
//                            im.setWarrantyScheme(rs.getInt("WRN_SCHEME_ID"));
//                            im.setPurchaseDate(reg_date);
//
//                            imeiMasterList.add(im);
//                            result = 1;
//                        }

                        result = 3;
                    }
                    }
                } else {
                    result = 2;
                }
            } else {
                result = 4;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }
        totalrecode = result;
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

}
