/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.EstimatedParts;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.MdeCode;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderDefect;
import org.dms.ws.singer.entities.WorkOrderEstimate;
import org.dms.ws.singer.entities.WorkOrderMaintain;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import org.dms.ws.singer.entities.WorkOrderTransfer;
import org.dms.ws.singer.utils.CustomerFeedBack;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WorkOrderMaintainaceController {

    public static int checkImeiWarranty(String imei) {
        logger.info("checkImeiWarranty Method Call...........");

        int imeiWarranty = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String qry = "  SELECT D.IMEI_NO,   CASE  "
                    + "                         WHEN   TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') >= SYSDATE THEN '0'           "
                    + "                         WHEN   TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') < SYSDATE THEN '1'            "
                    + "                         END AS WARRANTY_STATUS      "
                    + "     FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A, SD_WARRENTY_REGISTRATION R     "
                    + "     WHERE D.MODEL_NO = M.MODEL_NO (+)           "
                    + "     AND   D.IMEI_NO = R.IMEI_no (+)             "
                    + "     AND M.WRN_SCHEME_ID = W.SCHEME_ID (+)       "
                    + "     AND D.IMEI_NO = A.IMEI (+)                  "
                    + "     AND D.IMEI_NO='" + imei + "'    ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            if (rs_master_cnt.next()) {
                imeiWarranty = rs_master_cnt.getInt("WARRANTY_STATUS");
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiWarranty  " + ex.getMessage());
        }

        dbCon.ConectionClose(con);
        return imeiWarranty;

    }

    public static MessageWrapper getWorkOrderList(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            String modle_desc,
            String assessoriesretained,
            String mjr_defect_desc,
            String min_defect_desc,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            String bis_name,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            String level_desc,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            int repair_payment,
            String warrant_Type,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleDesc = (modle_desc.equalsIgnoreCase("all") ? "NVL(ML.MODEL_DESCRIPTION,'AA') = NVL(ML.MODEL_DESCRIPTION,'AA')" : "UPPER(ML.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String mjrDefecDesc = (mjr_defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + mjr_defect_desc + "%')");
            //String minDefecDesc = (min_defect_desc.equalsIgnoreCase("all") ? "NVL(M.MIN_CODE_DESC,'AA') = NVL(M.MIN_CODE_DESC,'AA')" : "UPPER(M.MIN_CODE_DESC) LIKE UPPER('%" + min_defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelDesc = (level_desc.equalsIgnoreCase("all") ? "NVL(L.LEVEL_NAME,'AA') = NVL(L.LEVEL_NAME,'AA')" : "UPPER(L.LEVEL_NAME) LIKE UPPER('%" + level_desc + "%')");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String warrantyType = (warrant_Type.equalsIgnoreCase("all") ? "NVL(MD.DESCRIPTION,'AA') = NVL(MD.DESCRIPTION,'AA')" : "UPPER(MD.DESCRIPTION) LIKE UPPER('%" + warrant_Type + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = '" + status + "'");
            //   String RepairPayment = (repair_payment == 0 ? "" : "AND P.STATUS = '" + repair_payment + "'");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "W.WORK_ORDER_NO,W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,W.TELEPHONE_NO,"
                    + "W.EMAIL,W.CUSTOMER_NIC,W.IMEI_NO,"
                    + "W.PRODUCT,W.BRAND,W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,W.DEFECTS,"
                    + "W.RCC_REFERENCE,W.DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,W.BIS_ID,"
                    + "W.SCHEME_ID,W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,W.ACTION_TAKEN,W.LEVEL_ID,"
                    + "W.REMARKS,W.REPAIR_STATUS,W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,W.COURIER_NO,"
                    + "W.DELIVERY_DATE,W.GATE_PASS,W.STATUS,"
                    + "TRUNC(SYSDATE - W.DATE_INSERTED) DELAY_DATE,"
                    + "EST_REF_NO,"
                    + "E.STATUS ES_STATUS,"
                    + "L.LEVEL_NAME,"
                    + "ML.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,D.MAJ_CODE_DESC,MD.DESCRIPTION";

            String whereClous = "W.WORK_ORDER_NO = E.WORK_ORDER_NO (+) "
                    + "AND W.LEVEL_ID = L.LEVEL_ID (+) "
                    + "AND W.MODEL_NO = ML.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.WARRANTY_VRIF_TYPE =  MD.CODE (+) "
                    + "AND MD.DOMAIN_NAME = 'WARANTY' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + "AND " + modleDesc + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + mjrDefecDesc + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + "AND " + bisName + ""
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "AND " + levelDesc + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + warrantyType + " "
                    + "" + Status + " "
                    //   + "" + RepairPayment + ""
                    + "AND W.STATUS !=9";

            String tables = "SD_WORK_ORDERS W,"
                    + "SD_WORK_ORDER_ESTIMATE E,"
                    + "SD_REPAIR_LEVELS L,"
                    + "SD_MODEL_LISTS ML,"
                    + "SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_MDECODE MD";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                totalrecode = rs.getInt("CNT");
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setBrand(rs.getString("BRAND"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setRccReference(rs.getString("RCC_REFERENCE"));
                wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                wo.setBisId(rs.getInt("BIS_ID"));
                wo.setSchemeId(rs.getInt("SCHEME_ID"));
                wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wo.setTecnician(rs.getString("TECHNICIAN"));
                wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                wo.setLevelId(rs.getInt("LEVEL_ID"));
                wo.setRemarks(rs.getString("REMARKS"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setCourierNo(rs.getString("COURIER_NO"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setGatePass(rs.getString("GATE_PASS"));
                wo.setStatus(rs.getInt("STATUS"));
                wo.setDelayDate(rs.getString("DELAY_DATE"));
                wo.setEstimateRefNo(rs.getString("EST_REF_NO"));
                wo.setEstimateStatus(rs.getInt("ES_STATUS"));
                wo.setLevelName(rs.getString("LEVEL_NAME"));
                wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setBinName(rs.getString("BIS_NAME"));
                // wo.setRepairPayment(rs.getInt("RP_STATUS"));
                wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
//                if (rs.getInt("RP_STATUS") == 1) {
//                    wo.setRepairPaymentStatusMsg("Pending");
//                } else if (rs.getInt("RP_STATUS") == 2) {
//                    wo.setRepairPaymentStatusMsg("Aprrove");
//                } else if (rs.getInt("RP_STATUS") == 3) {
//                    wo.setRepairPaymentStatusMsg("Settle");
//                } else {
//
//                }
                workOrderList.add(wo);
                
                System.out.println("WO MAINTAINCE-************");
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderMaintainaceDetails(int woMainTainId) {

        logger.info("getWorkOrderMaintainaceDetails Method Call....................");
        ArrayList<WorkOrderMaintain> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        String wo_num = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String workOrderNo = (woMainTainId == 0 ? "" : " M.WRK_ORD_MTN_ID = " + woMainTainId + "");

            String seletColunm = "M.WRK_ORD_MTN_ID,M.WORK_ORDER_NO,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "(SELECT DESCRIPTION FROM SD_MDECODE WHERE DOMAIN_NAME = 'WARANTY' AND CODE = M.WARRANTY_VRIF_TYPE) WARRANTY_DESC,"
                    + "M.NON_WARRANTY_VRIF_TYPE,"
                    + "(SELECT DESCRIPTION FROM SD_MDECODE WHERE DOMAIN_NAME = 'NON_WARANTY_TYPE' AND CODE = M.NON_WARRANTY_VRIF_TYPE) NON_WARRANTY_DESC,"
                    + "M.TECHNICIAN,W.IMEI_NO,"
                    + "L.MODEL_DESCRIPTION,M.REPAIR_LEVEL_ID,"
                    + "M.REPAIR_LEVEL_AMOUNT,M.REPAIR_STATUS,"
                    + "R.LEVEL_NAME,E.STATUS ESTIMATE_STATUS,TO_CHAR(EST_CLOSING_DATE,'YYYY/MM/DD') EST_CLOSING_DATE,"
                    + "M.ACTION_TAKEN,M.REMARKS,"
                    + "M.REPAIR_STATUS,M.INVOICE_NO ";

            String whereClous = "M.WORK_ORDER_NO = W.WORK_ORDER_NO (+) "
                    + "AND W.MODEL_NO = L.MODEL_NO (+) "
                    + "AND M.REPAIR_LEVEL_ID = R.LEVEL_ID (+) "
                    + "AND W.WORK_ORDER_NO = E.WORK_ORDER_NO (+) "
                    + "AND " + workOrderNo + "";

            String tables = "SD_WORK_ORDER_MAINTAIN M,SD_WORK_ORDERS W,"
                    + "SD_MODEL_LISTS L,SD_REPAIR_LEVELS R,"
                    + "SD_WORK_ORDER_ESTIMATE E";

            ResultSet rs = dbCon.search(con, "SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + "");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderMaintain wo = new WorkOrderMaintain();
                totalrecode = rs.getInt("CNT");
                wo.setWorkorderMaintainId(rs.getInt("WRK_ORD_MTN_ID"));
                wo_num = rs.getString("WORK_ORDER_NO");
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setWarrantyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                wo.setWarrantyTypedesc(rs.getString("WARRANTY_DESC"));
                wo.setNonWarrantyType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wo.setNonWarrantyTypeDesc(rs.getString("NON_WARRANTY_DESC"));
                wo.setTechnician(rs.getString("TECHNICIAN"));
                wo.setIemiNo(rs.getString("IMEI_NO"));
                wo.setModelDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setRepairLevelId(rs.getInt("REPAIR_LEVEL_ID"));
                wo.setRepairAmount(rs.getInt("REPAIR_LEVEL_AMOUNT"));
                wo.setLevelDesc(rs.getString("LEVEL_NAME"));
                wo.setEstimateStatusMsg(rs.getString("ESTIMATE_STATUS"));
                wo.setEstimateClosingDate(rs.getString("EST_CLOSING_DATE"));
                wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                wo.setRemarks(rs.getString("REMARKS"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setInvoiceNo(rs.getString("INVOICE_NO"));

                int warrantyStatus = WorkOrderMaintainaceController.checkImeiWarranty(rs.getString("IMEI_NO"));
                if (rs.getInt("WARRANTY_VRIF_TYPE") == 2) {
                    warrantyStatus = 1;
                }
                wo.setExpireStatus(warrantyStatus);

                String woDefectColumn = "W.WRK_ORD_DEFT_ID,W.WRK_ORD_MTN_ID,W.MAJ_CODE,W.MIN_CODE,W.REMARKS,W.STATUS,"
                        + "(SELECT MAJ_CODE_DESC FROM SD_DEFECT_CODES WHERE MAJ_CODE =W.MAJ_CODE) MAJ_DESC,"
                        + "(SELECT MIN_CODE_DESC FROM SD_DEFECT_MINOR_CODES WHERE MIN_CODE =W.MIN_CODE) MIN_DESC";

                String woDefectswhere = "WRK_ORD_MTN_ID = " + woMainTainId + "";

                ResultSet rs_defect = dbCon.search(con, "SELECT " + woDefectColumn + " FROM SD_WORK_ORDER_DEFECTS W WHERE " + woDefectswhere + "");

                ArrayList<WorkOrderDefect> woDefectList = new ArrayList<>();

                while (rs_defect.next()) {
                    WorkOrderDefect wod = new WorkOrderDefect();
                    wod.setWoDefectId(rs_defect.getInt("WRK_ORD_DEFT_ID"));
                    wod.setWoMaintainId(rs_defect.getInt("WRK_ORD_MTN_ID"));
                    wod.setMajorCode(rs_defect.getInt("MAJ_CODE"));
                    wod.setMinorCode(rs_defect.getInt("MIN_CODE"));
                    wod.setRemarks(rs_defect.getString("REMARKS"));
                    wod.setStatus(rs_defect.getInt("STATUS"));
                    wod.setMajDesc(rs_defect.getString("MAJ_DESC"));
                    wod.setMinDesc(rs_defect.getString("MIN_DESC"));
                    woDefectList.add(wod);
                }

                wo.setWoDefect(woDefectList);

                String woDetailColumn = "SEQ_NO,WORK_ORDER_NO,WRK_ORD_MTN_ID,SPARE_PART_NO,"
                        + "PART_DESCRIPTION,SELLING_PRICE,INVOICE_NO,THIRD_PARTY_FLAG,STATUS";
                String woDetailswhere = "WRK_ORD_MTN_ID = " + woMainTainId + "";

                ResultSet rs_details = dbCon.search(con, "SELECT " + woDetailColumn + " FROM SD_WORK_ORDER_MAINTAIN_DTL W WHERE " + woDetailswhere + "");

                ArrayList<WorkOrderMaintainDetail> woDetailsList = new ArrayList<>();

                while (rs_details.next()) {
                    WorkOrderMaintainDetail wodl = new WorkOrderMaintainDetail();
                    wodl.setSeqNo(rs_details.getInt("SEQ_NO"));
                    wodl.setWrokOrderNo(rs_details.getString("WORK_ORDER_NO"));
                    wodl.setWorkOrderMainId(rs_details.getInt("WRK_ORD_MTN_ID"));
                    wodl.setPartNo(rs_details.getInt("SPARE_PART_NO"));
                    wodl.setPartDesc(rs_details.getString("PART_DESCRIPTION"));
                    wodl.setPartSellPrice(rs_details.getDouble("SELLING_PRICE"));
                    wodl.setInvoiceNo(rs_details.getString("INVOICE_NO"));
                    wodl.setThirdPartyFlag(rs_details.getInt("THIRD_PARTY_FLAG"));
                    wodl.setStatus(rs_details.getInt("STATUS"));
                    woDetailsList.add(wodl);
                }

                wo.setWoDetials(woDetailsList);

                String woEstimateColumn = "EST_REF_NO,WORK_ORDER_NO,"
                        + "IMEI_NO,BIS_ID,COST_FOR_SPARE_PARTS,"
                        + "COST_FOR_LABOUR,STATUS,CUSTOMER_REF,PAYMENT_OPTION";

                String woEstimateValue = "WORK_ORDER_NO = '" + wo_num + "'";

                ResultSet rs_estimate = dbCon.search(con, "SELECT " + woEstimateColumn + " FROM SD_WORK_ORDER_ESTIMATE  WHERE " + woEstimateValue + "");

                ArrayList<WorkOrderEstimate> woEstimateList = new ArrayList<>();

                while (rs_estimate.next()) {
                    WorkOrderEstimate woel = new WorkOrderEstimate();
                    woel.setEsRefNo(rs_estimate.getString("EST_REF_NO"));
                    woel.setWorkOrderNo(rs_estimate.getString("WORK_ORDER_NO"));
                    woel.setImeiNo(rs_estimate.getString("IMEI_NO"));
                    woel.setBisId(rs_estimate.getInt("BIS_ID"));
                    woel.setCostSparePart(rs_estimate.getDouble("COST_FOR_SPARE_PARTS"));
                    woel.setCostLabor(rs_estimate.getDouble("COST_FOR_LABOUR"));
                    woel.setStatus(rs_estimate.getInt("STATUS"));
                    woel.setCustomerRef(rs_estimate.getString("CUSTOMER_REF"));
                    woel.setPaymentOption(rs_estimate.getInt("PAYMENT_OPTION"));
                    woEstimateList.add(woel);
                }

                wo.setWoEstimate(woEstimateList);

                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper GenarateEstimateNo() {
        logger.info("GenarateEstimateNo Method Call....................");
        String last_estimate_id = "ES000000";
        String nxtEstimateId = null;
        String nxtNewEstimate = "ES";
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ResultSet rs_wocnt = dbCon.search(con, "SELECT EST_REF_NO "
                    + "FROM SD_WORK_ORDER_ESTIMATE "
                    + "WHERE ROWNUM = 1 "
                    + "ORDER BY EST_REF_NO DESC ");

            if (rs_wocnt.next()) {
                last_estimate_id = rs_wocnt.getString(1);
                logger.info("Last Work Order id   " + last_estimate_id);
            }

            last_estimate_id = last_estimate_id.split("ES")[1];
            int nxt_wo_num = Integer.parseInt(last_estimate_id) + 1;
            nxtEstimateId = String.valueOf(nxt_wo_num);
            int nxtEstimate_Length = nxtEstimateId.length();

            if (nxtEstimate_Length < 6) {
                for (int i = 0; i < (6 - nxtEstimate_Length); i++) {
                    nxtNewEstimate += "0";
                }
            }
            nxtNewEstimate += nxtEstimateId;
            result = 1;
            logger.info("Inserting................Estimate No = " + nxtNewEstimate);

        } catch (Exception ex) {
            logger.info("Error GenarateEstimateNo Methd call.." + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Estimate Number Genarate");
            vw.setReturnRefNo(nxtNewEstimate);
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Estimate Number Genarate");
            //vw.setReturnRefNo(nxtEstimateId);
        }

        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper sendEstimateCustomerMail(String WONumber, String amount, String estNumber, String reference, int workorderMaintainId) {
        logger.info("sendEstimateCustomerMail Method Call....................");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String mail = "";
            String name = "";
            String ref = "";
            String model = "";
            String imei = "";

            String serachQry = " SELECT WORK_ORDER_NO , W.CUSTOMER_NAME, W.EMAIL, W.RCC_REFERENCE, M.MODEL_DESCRIPTION, W.IMEI_NO   "
                    + " FROM SD_WORK_ORDERS W, SD_MODEL_LISTS M "
                    + " WHERE W.MODEL_NO=M.MODEL_NO "
                    + " AND WORK_ORDER_NO = '" + WONumber + "'  ";

            //ResultSet rs_wo_details = dbCon.search(con, "SELECT CUSTOMER_NAME,EMAIL FROM SD_WORK_ORDERS  WHERE WORK_ORDER_NO = '" + WONumber + "'");
            ResultSet rs_wo_details = dbCon.search(con, serachQry);

            while (rs_wo_details.next()) {
                name = rs_wo_details.getString("CUSTOMER_NAME");
                mail = rs_wo_details.getString("EMAIL");
                ref = rs_wo_details.getString("RCC_REFERENCE");
                model = rs_wo_details.getString("MODEL_DESCRIPTION");
                imei = rs_wo_details.getString("IMEI_NO");
            }

            if (!mail.equals("")) {

                String woDetailColumn = " SPARE_PART_NO, PART_DESCRIPTION, TO_CHAR(SELLING_PRICE, '999999999.99') UNIT_PRICE, COUNT(WRK_ORD_MTN_ID) QTY, TO_CHAR((COUNT(WRK_ORD_MTN_ID)*SELLING_PRICE), '999999999.99') AMOUNT ";
                String woDetailswhere = " WRK_ORD_MTN_ID = " + workorderMaintainId + "  "
                        + " GROUP BY SPARE_PART_NO, PART_DESCRIPTION, SELLING_PRICE  ";
                String qry = "SELECT " + woDetailColumn + " FROM SD_WORK_ORDER_MAINTAIN_DTL  WHERE " + woDetailswhere + "";

                ResultSet rs_details = dbCon.search(con, qry);

                ArrayList<EstimatedParts> estimatedSparePartsList = new ArrayList<>();

                double totalAmount = 0.0;
                while (rs_details.next()) {
                    EstimatedParts wodl = new EstimatedParts();
                    wodl.setPartNo(rs_details.getInt("SPARE_PART_NO"));
                    wodl.setPartDescription(rs_details.getString("PART_DESCRIPTION"));
                    wodl.setUnitPrice(rs_details.getDouble("UNIT_PRICE"));
                    wodl.setQty(rs_details.getInt("QTY"));
                    wodl.setAmount(rs_details.getDouble("AMOUNT"));
                    totalAmount += rs_details.getDouble("AMOUNT");

                    estimatedSparePartsList.add(wodl);
                }

                String repairLevelQry = " SELECT  M.WORK_ORDER_NO, M.REPAIR_LEVEL_ID, L.LEVEL_DESC, TO_CHAR(L.PRICE, '999999999.99') AS PRICE "
                        + " FROM    SD_WORK_ORDER_MAINTAIN M, SD_REPAIR_LEVELS L    "
                        + " WHERE   M.REPAIR_LEVEL_ID=L.LEVEL_ID    "
                        + " AND     M.WORK_ORDER_NO='" + WONumber + "'  ";

                ResultSet rs_repLevel = dbCon.search(con, repairLevelQry);

                String repairLevel = "";
                double repairPrice = 0.00;

                if (rs_repLevel.next()) {
                    repairLevel = rs_repLevel.getString("LEVEL_DESC");
                    repairPrice = rs_repLevel.getDouble("PRICE");
                }

                totalAmount += repairPrice;
                totalAmount = Math.round(totalAmount * 100);
                totalAmount = totalAmount / 100;

                CustomerFeedBack.sendHTMLMail(mail, name, amount, estNumber, reference, WONumber, estimatedSparePartsList, totalAmount, repairLevel, repairPrice, model, imei);
            }
            result = 1;

        } catch (Exception ex) {
            logger.info("Error sendEstimateCustomerMail Methd call.." + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Estimate Mail Send Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Estimate Mail Send");
        }

        dbCon.ConectionClose(con);
        logger.info("Ready to Return Values...............");
        logger.info("----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addWorkOrderMaintainces(WorkOrderMaintain wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWorkOrderMaintainces Mehtod Call.......");
        int result = 0;
        String returnRefNo = null;
        int workorder_dtl = 0;
        int max_wo_main = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            int maintain_cnt = 0;
            ResultSet rs_cnt_maintain = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDER_MAINTAIN "
                    + "WHERE REPAIR_STATUS = 1 "
                    + "AND WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "'");

            while (rs_cnt_maintain.next()) {
                maintain_cnt = rs_cnt_maintain.getInt("CNT");
            }

            if (maintain_cnt > 0) {
                result = 2;

            } else {

                ResultSet rs_max_main = dbCon.search(con, "SELECT NVL(MAX(WRK_ORD_MTN_ID),0) MAX "
                        + "FROM SD_WORK_ORDER_MAINTAIN");
                while (rs_max_main.next()) {
                    max_wo_main = rs_max_main.getInt("MAX");
                }
                rs_max_main.close();

                max_wo_main = max_wo_main + 1;

                String wo_main_column = "WRK_ORD_MTN_ID,WORK_ORDER_NO,"
                        + "WARRANTY_VRIF_TYPE,NON_WARRANTY_VRIF_TYPE,"
                        + "TECHNICIAN,ACTION_TAKEN,"
                        + "REPAIR_LEVEL_ID,REPAIR_LEVEL_AMOUNT,"
                        + "REMARKS,REPAIR_STATUS,EST_CLOSING_DATE,"
                        + "USER_INSERTED,DATE_INSERTED,INVOICE_NO, BIS_ID ";

                String wo_main_value = "?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,SYSDATE,?, ?";

                PreparedStatement ps_in_womain = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_MAINTAIN(" + wo_main_column + ") VALUES(" + wo_main_value + ")");
                ps_in_womain.setInt(1, max_wo_main);
                ps_in_womain.setString(2, wo_info.getWorkOrderNo());
                ps_in_womain.setInt(3, wo_info.getWarrantyVrifType());
                ps_in_womain.setInt(4, wo_info.getNonWarrantyType());
                ps_in_womain.setString(5, wo_info.getTechnician());
                ps_in_womain.setString(6, wo_info.getActionTaken());
                ps_in_womain.setInt(7, wo_info.getRepairLevelId());
                ps_in_womain.setDouble(8, wo_info.getRepairAmount());
                ps_in_womain.setString(9, wo_info.getRemarks());
                ps_in_womain.setInt(10, wo_info.getRepairStatus());
                ps_in_womain.setString(11, wo_info.getEstimateClosingDate());
                ps_in_womain.setString(12, user_id);
                ps_in_womain.setString(13, wo_info.getInvoiceNo());
                ps_in_womain.setInt(14, wo_info.getBisId());
                ps_in_womain.executeUpdate();
                result = 1;

                if (wo_info.getWoDetials() != null) {

                    dbCon.search(con, "DELETE FROM SD_WORK_ORDER_MAINTAIN_DTL "
                            + "WHERE WRK_ORD_MTN_ID = " + max_wo_main + "");

                    ArrayList<WorkOrderMaintainDetail> woDetailList = new ArrayList<>();
                    woDetailList = wo_info.getWoDetials();

                    ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                            + "FROM SD_WORK_ORDER_MAINTAIN_DTL WHERE WRK_ORD_MTN_ID = " + max_wo_main + "");
                    while (rs_max_dtl.next()) {
                        workorder_dtl = rs_max_dtl.getInt("MAX");
                    }
                    rs_max_dtl.close();

                    String workOrderDetailsColumn = "SEQ_NO,"
                            + "WORK_ORDER_NO,"
                            + "WRK_ORD_MTN_ID,"
                            + "SPARE_PART_NO,"
                            + "PART_DESCRIPTION,"
                            + "SELLING_PRICE,"
                            + "INVOICE_NO,"
                            + "THIRD_PARTY_FLAG,"
                            + "STATUS,"
                            + "USER_INSERTED,"
                            + "DATE_INSERTED";

                    String woDetailsValue = "?,?,?,?,?,?,?,?,1,?,SYSDATE";

                    PreparedStatement ps_in_wodtl = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_MAINTAIN_DTL(" + workOrderDetailsColumn + ") "
                            + "VALUES(" + woDetailsValue + ")");

                    for (WorkOrderMaintainDetail wd : woDetailList) {

                        workorder_dtl = workorder_dtl + 1;

                        ps_in_wodtl.setInt(1, workorder_dtl);
                        ps_in_wodtl.setString(2, wd.getWrokOrderNo());
                        ps_in_wodtl.setInt(3, max_wo_main);
                        ps_in_wodtl.setInt(4, wd.getPartNo());
                        ps_in_wodtl.setString(5, wd.getPartDesc());
                        ps_in_wodtl.setDouble(6, wd.getPartSellPrice());
                        ps_in_wodtl.setString(7, wd.getInvoiceNo());
                        ps_in_wodtl.setInt(8, wd.getThirdPartyFlag());
                        ps_in_wodtl.setString(9, user_id);
                        ps_in_wodtl.addBatch();

                    }
                    ps_in_wodtl.executeBatch();
                    result = 1;
                }

                int wo_defect_max = 0;

                if (wo_info.getWoDefect().size() > 0) {

                    dbCon.search(con, "DELETE FROM SD_WORK_ORDER_DEFECTS "
                            + "WHERE WRK_ORD_MTN_ID = " + max_wo_main + "");

                    ArrayList<WorkOrderDefect> woDefectList = new ArrayList<>();
                    woDefectList = wo_info.getWoDefect();

                    ResultSet rs_max_def = dbCon.search(con, "SELECT NVL(MAX(WRK_ORD_DEFT_ID),0) MAX "
                            + "FROM SD_WORK_ORDER_DEFECTS WHERE WRK_ORD_MTN_ID = " + max_wo_main + "");
                    while (rs_max_def.next()) {
                        wo_defect_max = rs_max_def.getInt("MAX");
                    }

                    String workOrderDefectColumn = "WRK_ORD_DEFT_ID,WRK_ORD_MTN_ID,"
                            + "MAJ_CODE,MIN_CODE,"
                            + "REMARKS,STATUS,"
                            + "USER_INSERTED,DATE_INSERTED";

                    String woDefectValue = "?,?,?,?,?,1,?,SYSDATE";

                    PreparedStatement ps_in_wodft = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_DEFECTS(" + workOrderDefectColumn + ") "
                            + "VALUES(" + woDefectValue + ")");

                    for (WorkOrderDefect wd : woDefectList) {

                        wo_defect_max = wo_defect_max + 1;

                        ps_in_wodft.setInt(1, wo_defect_max);
                        ps_in_wodft.setInt(2, max_wo_main);
                        ps_in_wodft.setInt(3, wd.getMajorCode());
                        ps_in_wodft.setInt(4, wd.getMinorCode());
                        ps_in_wodft.setString(5, wd.getRemarks());
                        ps_in_wodft.setString(6, user_id);
                        ps_in_wodft.addBatch();
                    }

                    ps_in_wodft.executeBatch();
                    result = 1;
                }

                if (result == 1) {

                    PreparedStatement ps_up_wo = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                            + "SET STATUS = 2,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE WORK_ORDER_NO = ?");
                    ps_up_wo.setString(1, user_id);
                    ps_up_wo.setString(2, wo_info.getWorkOrderNo());
                    ps_up_wo.executeUpdate();

                }

            }

        } catch (Exception ex) {
            logger.info("Error in addWorkOrderMaintainces Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(returnRefNo);
            vw.setReturnMsg("Work Order Update Successfully");
            logger.info("WorkOrder Update Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This Work Order have in Progress Maintain Record");
            logger.info("This Work Order have in Progress Maintain Record...............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in WorkOrder Update");
            logger.info("Error in Work Order Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editWorkOrderMaintainces(WorkOrderMaintain wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editWorkOrderMaintainces Mehtod Call.......");
        int result = 0;
        int workorder_dtl = 0;
        String returnRefNo = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_woMain = dbCon.prepare(con, "UPDATE SD_WORK_ORDER_MAINTAIN "
                    + " SET WORK_ORDER_NO = ?,"
                    + " WARRANTY_VRIF_TYPE = ?,"
                    + " NON_WARRANTY_VRIF_TYPE = ?,"
                    + " TECHNICIAN = ?,"
                    + " ACTION_TAKEN =?,"
                    + " REPAIR_LEVEL_ID =?,"
                    + " REPAIR_LEVEL_AMOUNT =?,"
                    + " REMARKS = ?,"
                    + " REPAIR_STATUS =?,"
                    + " EST_CLOSING_DATE = TO_DATE(?,'YYYY/MM/DD'),"
                    + " USER_MODIFIED = ?,"
                    + " DATE_MODIFIED = SYSDATE,"
                    + " INVOICE_NO=?  "
                    + " WHERE WRK_ORD_MTN_ID = ?");

            ps_up_woMain.setString(1, wo_info.getWorkOrderNo());
            ps_up_woMain.setInt(2, wo_info.getWarrantyVrifType());
            ps_up_woMain.setInt(3, wo_info.getNonWarrantyType());
            ps_up_woMain.setString(4, wo_info.getTechnician());
            ps_up_woMain.setString(5, wo_info.getActionTaken());
            ps_up_woMain.setInt(6, wo_info.getRepairLevelId());
            ps_up_woMain.setDouble(7, wo_info.getRepairAmount());
            ps_up_woMain.setString(8, wo_info.getRemarks());
            ps_up_woMain.setInt(9, wo_info.getRepairStatus());
            ps_up_woMain.setString(10, wo_info.getEstimateClosingDate());
            ps_up_woMain.setString(11, user_id);
            ps_up_woMain.setString(12, wo_info.getInvoiceNo());
            ps_up_woMain.setInt(13, wo_info.getWorkorderMaintainId());
            ps_up_woMain.executeUpdate();

            result = 1;

            if (wo_info.getWoDetials() != null) {

                dbCon.search(con, "DELETE FROM SD_WORK_ORDER_MAINTAIN_DTL "
                        + "WHERE WRK_ORD_MTN_ID = " + wo_info.getWorkorderMaintainId() + "");

                ArrayList<WorkOrderMaintainDetail> woDetailList = new ArrayList<>();
                woDetailList = wo_info.getWoDetials();

                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                        + "FROM SD_WORK_ORDER_MAINTAIN_DTL "
                        + "WHERE WRK_ORD_MTN_ID = " + wo_info.getWorkorderMaintainId() + "");
                while (rs_max_dtl.next()) {
                    workorder_dtl = rs_max_dtl.getInt("MAX");
                }

                String workOrderDetailsColumn = "SEQ_NO,"
                        + "WORK_ORDER_NO,"
                        + "WRK_ORD_MTN_ID,"
                        + "SPARE_PART_NO,"
                        + "PART_DESCRIPTION,"
                        + "SELLING_PRICE,"
                        + "INVOICE_NO,"
                        + "THIRD_PARTY_FLAG,"
                        + "STATUS,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED";

                String woDetailsValue = "?,?,?,?,?,?,?,?,1,?,SYSDATE";

                PreparedStatement ps_in_wodtl = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_MAINTAIN_DTL(" + workOrderDetailsColumn + ") "
                        + "VALUES(" + woDetailsValue + ")");

                for (WorkOrderMaintainDetail wd : woDetailList) {

                    workorder_dtl = workorder_dtl + 1;
                    //System.out.println("   " + workorder_dtl);

                    ps_in_wodtl.setInt(1, workorder_dtl);
                    ps_in_wodtl.setString(2, wd.getWrokOrderNo());
                    ps_in_wodtl.setInt(3, wo_info.getWorkorderMaintainId());
                    ps_in_wodtl.setInt(4, wd.getPartNo());
                    ps_in_wodtl.setString(5, wd.getPartDesc());
                    ps_in_wodtl.setDouble(6, wd.getPartSellPrice());
                    ps_in_wodtl.setString(7, wd.getInvoiceNo());
                    ps_in_wodtl.setInt(8, wd.getThirdPartyFlag());
                    ps_in_wodtl.setString(9, user_id);
                    ps_in_wodtl.addBatch();

                }
                ps_in_wodtl.executeBatch();
                result = 1;
            }

            int wo_defect_max = 0;

            if (wo_info.getWoDefect().size() > 0) {

                dbCon.search(con, "DELETE FROM SD_WORK_ORDER_DEFECTS "
                        + "WHERE WRK_ORD_MTN_ID = " + wo_info.getWorkorderMaintainId() + "");

                ArrayList<WorkOrderDefect> woDefectList = new ArrayList<>();
                woDefectList = wo_info.getWoDefect();

                ResultSet rs_max_def = dbCon.search(con, "SELECT NVL(MAX(WRK_ORD_DEFT_ID),0) MAX "
                        + "FROM SD_WORK_ORDER_DEFECTS "
                        + "WHERE WRK_ORD_MTN_ID = " + wo_info.getWorkorderMaintainId() + "");
                while (rs_max_def.next()) {
                    wo_defect_max = rs_max_def.getInt("MAX");
                }

                String workOrderDefectColumn = "WRK_ORD_DEFT_ID,WRK_ORD_MTN_ID,"
                        + "MAJ_CODE,MIN_CODE,"
                        + "REMARKS,STATUS,"
                        + "USER_INSERTED,DATE_INSERTED";

                String woDefectValue = "?,?,?,?,?,1,?,SYSDATE";

                PreparedStatement ps_in_wodft = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_DEFECTS(" + workOrderDefectColumn + ") "
                        + "VALUES(" + woDefectValue + ")");

                for (WorkOrderDefect wd : woDefectList) {

                    wo_defect_max = wo_defect_max + 1;

                    ps_in_wodft.setInt(1, wo_defect_max);
                    ps_in_wodft.setInt(2, wo_info.getWorkorderMaintainId());
                    ps_in_wodft.setInt(3, wd.getMajorCode());
                    ps_in_wodft.setInt(4, wd.getMinorCode());
                    ps_in_wodft.setString(5, wd.getRemarks());
                    ps_in_wodft.setString(6, user_id);
                    ps_in_wodft.addBatch();

                }
                ps_in_wodft.executeBatch();
                result = 1;
            }

            if (wo_info.getWoEstimate().size() > 0) {

                int wo_EstimateCnt = 0;
                ArrayList<WorkOrderEstimate> woExtimateList = new ArrayList<>();

                woExtimateList = wo_info.getWoEstimate();

                ResultSet rs_cnt_est = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_WORK_ORDER_ESTIMATE "
                        + "WHERE WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "' "
                        + "AND STATUS != 9");
                while (rs_cnt_est.next()) {
                    wo_EstimateCnt = rs_cnt_est.getInt("CNT");
                }

                if (wo_EstimateCnt > 0) {
                    PreparedStatement ps_up_woEstimate = dbCon.prepare(con, "UPDATE SD_WORK_ORDER_ESTIMATE "
                            + "SET IMEI_NO =?,"
                            + "BIS_ID=?,"
                            + "CUSTOMER_REF=?,"
                            + "PAYMENT_OPTION=?,"
                            + "COST_FOR_SPARE_PARTS=?,"
                            + "COST_FOR_LABOUR=?,"
                            + "STATUS=?,"
                            + "USER_MODIFIED=?,"
                            + "DATE_MODIFIED=SYSDATE "
                            + "WHERE EST_REF_NO= ?");
                    for (WorkOrderEstimate we : woExtimateList) {
                        ps_up_woEstimate.setString(1, we.getImeiNo());
                        ps_up_woEstimate.setInt(2, we.getBisId());
                        ps_up_woEstimate.setString(3, we.getCustomerRef());
                        ps_up_woEstimate.setInt(4, we.getPaymentOption());
                        ps_up_woEstimate.setDouble(5, we.getCostSparePart());
                        ps_up_woEstimate.setDouble(6, we.getCostLabor());
                        ps_up_woEstimate.setInt(7, we.getStatus());
                        ps_up_woEstimate.setString(8, user_id);
                        ps_up_woEstimate.setString(9, we.getEsRefNo());
                        ps_up_woEstimate.addBatch();
                    }
                    ps_up_woEstimate.executeBatch();
                    result = 1;

                } else {

                    String workOrderEstimateColumn = "EST_REF_NO,"
                            + "WORK_ORDER_NO,"
                            + "IMEI_NO,BIS_ID,"
                            + "CUSTOMER_REF,PAYMENT_OPTION,"
                            + "COST_FOR_SPARE_PARTS,COST_FOR_LABOUR,"
                            + "STATUS,USER_INSERTED,DATE_INSERTED";

                    String woEstimateValue = "?,?,?,?,?,?,?,?,?,?,SYSDATE";

                    PreparedStatement ps_in_woest = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_ESTIMATE(" + workOrderEstimateColumn + ") "
                            + "VALUES(" + woEstimateValue + ")");

                    for (WorkOrderEstimate we : woExtimateList) {
                        ps_in_woest.setString(1, we.getEsRefNo());
                        ps_in_woest.setString(2, we.getWorkOrderNo());
                        ps_in_woest.setString(3, we.getImeiNo());
                        ps_in_woest.setInt(4, we.getBisId());
                        ps_in_woest.setString(5, we.getCustomerRef());
                        ps_in_woest.setInt(6, we.getPaymentOption());
                        ps_in_woest.setDouble(7, we.getCostSparePart());
                        ps_in_woest.setDouble(8, we.getCostLabor());
                        ps_in_woest.setInt(9, we.getStatus());
                        ps_in_woest.setString(10, user_id);
                        ps_in_woest.addBatch();

                    }
                    ps_in_woest.executeBatch();

                    result = 1;

                }
            }
            if (result == 1) {
                if (wo_info.getRepairStatus() == 1) {
                    PreparedStatement ps_up_wo = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                            + "SET STATUS = 1,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE, "
                            + "SERVICE_BIS_ID = ? "
                            + "WHERE WORK_ORDER_NO = ?");
                    ps_up_wo.setString(1, user_id);
                    ps_up_wo.setInt(2, wo_info.getBisId());
                    ps_up_wo.setString(3, wo_info.getWorkOrderNo());

                    ps_up_wo.executeUpdate();
                } else if (wo_info.getRepairStatus() == 2 || wo_info.getRepairStatus() == 3) {

                    PreparedStatement ps_up_wo = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                            + "SET STATUS = 2 ,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE WORK_ORDER_NO = ?");
                    ps_up_wo.setString(1, user_id);
                    ps_up_wo.setString(2, wo_info.getWorkOrderNo());
                    ps_up_wo.executeUpdate();
                }
            }

        } catch (Exception ex) {
            logger.info("Error in editWorkOrderMaintainces Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(returnRefNo);
            vw.setReturnMsg("Work Order Maintenance Update Successfully");
            logger.info("WorkOrder Maintainance Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Maintenance Update");
            logger.info("Error in WorkOrder Maintainance Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addWorkOrderEstimate(WorkOrderEstimate wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWorkOrderEstimate Mehtod Call.......");
        int result = 0;
        String last_estimate_id = "ES000000";
        String returnRefNo = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_wocnt = dbCon.search(con, "SELECT EST_REF_NO "
                    + "FROM SD_WORK_ORDER_ESTIMATE "
                    + "WHERE ROWNUM = 1 "
                    + "ORDER BY EST_REF_NO DESC ");

            if (rs_wocnt.next()) {
                last_estimate_id = rs_wocnt.getString(1);
                logger.info("Last Estimate id   " + last_estimate_id);
            }

            last_estimate_id = last_estimate_id.split("ES")[1];
            int nxt_wo_num = Integer.parseInt(last_estimate_id) + 1;
            String nxtWorkOrderId = String.valueOf(nxt_wo_num);
            int nxtWorkOrderNo_Length = nxtWorkOrderId.length();

            String nxtNewWorkOrderId = "ES";
            if (nxtWorkOrderNo_Length < 6) {
                for (int i = 0; i < (6 - nxtWorkOrderNo_Length); i++) {
                    nxtNewWorkOrderId += "0";
                }
            }
            nxtNewWorkOrderId += nxtWorkOrderId;

            logger.info("Inserting................Estimate No = " + nxtNewWorkOrderId);

            returnRefNo = nxtNewWorkOrderId;
            
            
            int imeiWaranty = WorkOrderController.checkImeiWarranty(wo_info.getImeiNo());
            
            
            if(imeiWaranty == 2)
            {
                String insrtColumn = "EST_REF_NO,"
                    + "WORK_ORDER_NO,"
                    + "IMEI_NO,"
                    + "BIS_ID,"
                    + "COST_FOR_SPARE_PARTS,"
                    + "COST_FOR_LABOUR,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED";

                    String insertvalues = "'" + returnRefNo + "',"
                            + "'" + wo_info.getWorkOrderNo() + "',"
                            + "'" + wo_info.getImeiNo() + "',"
                            + "" + wo_info.getBisId() + ","
                            + "" + wo_info.getCostSparePart() + ","
                            + "" + wo_info.getCostLabor() + ","
                            + "1,"
                            + "'" + user_id + "',"
                            + "SYSDATE";

                    dbCon.save(con, "INSERT INTO SD_WORK_ORDER_ESTIMATE(" + insrtColumn + ") VALUES(" + insertvalues + ")");

                    result = 1;
                
            }else{
                
                result = 3;
                
            }

            

        } catch (Exception ex) {
            logger.info("Error in addWorkOrderEstimate Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(returnRefNo);
            vw.setReturnMsg("Work Order Estimate Insert Successfully");
            logger.info("Work Order Estimate Insert Succesfully............");

        }else if  (result == 3){
            vw.setReturnFlag("3");
            vw.setReturnMsg("This IMEI in warrant therefor can't create estimate");
            logger.info("This IMEI in warrant therefor can't create estimate................");
        }else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Estimate Insert");
            logger.info("Error in Work Order Estimate Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper deleteWorkOrderMaintainces(WorkOrderMaintain wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deleteWorkOrderMaintainces Mehtod Call.......");
        int result = 0;
        String returnRefNo = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_woMain = dbCon.prepare(con, "UPDATE SD_WORK_ORDER_MAINTAIN SET REPAIR_STATUS = 9,"
                    + "USER_MODIFIED = ?,"
                    + "DATE_MODIFIED = SYSDATE "
                    + "WHERE WRK_ORD_MTN_ID = ?");

            ps_up_woMain.setString(1, user_id);
            ps_up_woMain.setInt(2, wo_info.getWorkorderMaintainId());
            ps_up_woMain.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in deleteWorkOrderMaintainces Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(returnRefNo);
            vw.setReturnMsg("Work Order Delete Successfully");
            logger.info("WorkOrder Delete Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Delete");
            logger.info("Error in Work Order Delete................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getWorkOrderMaintainList(int bisId,
            String workorderno,
            String warranty_typedesc,
            String technician,
            String imei,
            String modle_desc,
            String repair_level,
            String estimate_status,
            String status,
            String invoice,
            String status_msg,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getWorkOrderMaintainList Method Call....................");
        ArrayList<WorkOrderMaintain> workOrderMainList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.workOrderMaintain(order);

            String BisId = (bisId == 0 ? " " : "AND M.BIS_ID = '" + bisId + "'");
            String RepairStatus = (status.equalsIgnoreCase("all") ? " " : " AND M.REPAIR_STATUS=" + status + "");
            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(M.WORK_ORDER_NO,'AA') = NVL(M.WORK_ORDER_NO,'AA')" : "UPPER(M.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            // String warrantyStatus = (warranty_typedesc.equalsIgnoreCase("all") ? "NVL(D2.DESCRIPTION,'AA') = NVL(D2.DESCRIPTION,'AA')" : "UPPER(D2.DESCRIPTION) LIKE UPPER('%" + warranty_typedesc + "%')");
            String imeNo = (imei.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei + "%')");
            String modleDesc = (modle_desc.equalsIgnoreCase("all") ? "NVL(L.MODEL_DESCRIPTION,'AA') = NVL(L.MODEL_DESCRIPTION,'AA')" : "UPPER(L.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String Tecnician = (technician.equalsIgnoreCase("all") ? "NVL(M.TECHNICIAN,'AA') = NVL(M.TECHNICIAN,'AA')" : "UPPER(M.TECHNICIAN) LIKE UPPER('%" + technician + "%')");
            //String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelDesc = (repair_level.equalsIgnoreCase("all") ? "NVL(R.LEVEL_NAME,'AA') = NVL(R.LEVEL_NAME,'AA')" : "UPPER(R.LEVEL_NAME) LIKE UPPER('%" + repair_level + "%')");
            // String estimateStatus = (estimate_status.equalsIgnoreCase("all") ? "" : "AND E.STATUS = " + estimate_status + "");
            //   String workOrderStatus = (status.equalsIgnoreCase("all") ? "" : "AND M.REPAIR_STATUS = '" + status + "'");
            String statusMsg = (status_msg.equalsIgnoreCase("all") ? "NVL(D1.DESCRIPTION,'AA') = NVL(D1.DESCRIPTION,'AA')" : "UPPER(D1.DESCRIPTION) LIKE UPPER('%" + status_msg + "%')");
            String Invoice = (invoice.equalsIgnoreCase("all") ? "NVL(M.INVOICE_NO,'AA') = NVL(M.INVOICE_NO,'AA')" : "UPPER(M.INVOICE_NO) LIKE UPPER('%" + invoice + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY M.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "M.WRK_ORD_MTN_ID,"
                    + "M.WORK_ORDER_NO,"
                    + "M.TECHNICIAN,"
                    + "W.IMEI_NO,"
                    + "L.MODEL_DESCRIPTION,"
                    + "R.LEVEL_NAME,"
                    + "M.REPAIR_LEVEL_ID,M.INVOICE_NO,"
                    + "W.DEFECTS,"
                    + "W.STATUS WO_STATUS,"
                    + "M.REPAIR_STATUS,"
                    + "D1.DESCRIPTION RP_DESCRIPTION,"
                    + "W.WARRANTY_VRIF_TYPE";

            String whereClous = "M.WORK_ORDER_NO = W.WORK_ORDER_NO (+) "
                    + "AND W.MODEL_NO = L.MODEL_NO (+) "
                    + "AND M.REPAIR_LEVEL_ID = R.LEVEL_ID (+) "
                    + "AND M.REPAIR_STATUS = D1.CODE (+) "
                    + "AND D1.DOMAIN_NAME = 'RP_STATUS'  "
                    + "AND " + workOrderNo + " "
                    + "AND " + imeNo + " "
                    + "AND " + modleDesc + " "
                    + "AND " + Tecnician + " "
                    + "AND " + levelDesc + " "
                    + "AND " + Invoice + " "
                    + "AND " + statusMsg + " "
                    + "AND M.REPAIR_STATUS !=9  "
                    + RepairStatus
                    + BisId;

            String tables = "SD_WORK_ORDER_MAINTAIN M,SD_WORK_ORDERS W,"
                    + "SD_MODEL_LISTS L,SD_REPAIR_LEVELS R"
                    + ",SD_MDECODE D1";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + tables + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//            
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = WorkPaymentController.checkImeiWarranty();

            MessageWrapper mw = WorkOrderTransferController.getWorkOrderTransfer("all", "all", 0, "all", "all", 0, "all", 0, "all", 0, "all", "all", "all", "all", "all", "all", 0, 100000);
            ArrayList<WorkOrderTransfer> woTransferList = mw.getData();

            while (rs.next()) {

                int warrantyStatus = 1;  //Out of Warranty

                for (ImeiMaster i : warrantImie) {
                    if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                        if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                            warrantyStatus = 0;  //Under Warranty
                        }

                    }
                }

                String transferStatus = "Inside";

                for (WorkOrderTransfer wot : woTransferList) {
                    if (wot.getWorkOrderNo().equals(rs.getString("WORK_ORDER_NO"))) {
                        transferStatus = "Outside";
                    }
                }

                WorkOrderMaintain wo = new WorkOrderMaintain();
                totalrecode = rs.getInt("CNT");
                wo.setWorkorderMaintainId(rs.getInt("WRK_ORD_MTN_ID"));
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setTechnician(rs.getString("TECHNICIAN"));
                wo.setIemiNo(rs.getString("IMEI_NO"));
                wo.setInvoiceNo(rs.getString("INVOICE_NO"));
                wo.setModelDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setRepairLevelId(rs.getInt("REPAIR_LEVEL_ID"));
                wo.setLevelDesc(rs.getString("LEVEL_NAME"));
                wo.setStatusMsg(rs.getString("WO_STATUS"));
                wo.setRemarks(rs.getString("DEFECTS"));
                //    wo.setEstimateStatusMsg(rs.getString("DESCRIPTION"));
                wo.setStatusMsg(rs.getString("RP_DESCRIPTION"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                //   wo.setWarrantyTypedesc(rs.getString("WARRANTY"));
                //   wo.setWarantyStatus(rs.getString("WARRANTY"));
                wo.setWarrantyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));

                wo.setExpireStatus(warrantyStatus);
                wo.setTransferStatus(transferStatus);

                workOrderMainList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderMaintainList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderMainList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getRepairType() {

        logger.info("getRepairType Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'WOM_RP_STATUS'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getRepairType Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getEstimateStatus() {

        logger.info("getEstimateStatus Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'ESTIMATE_STATUS'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getEstimateStatus Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getPaymentOption() {

        logger.info("getPaymentOption Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'PAYMENT_OPTION'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getPaymentOption Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getMaintainWarranty() {

        logger.info("getMaintainWarranty Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'MAIN_WARRNTY'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getMaintainWarranty Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    ////////////////////////////Grid to select Work Order for Maintenance
    public static MessageWrapper getWorkOrderList_To_Add_Maintain(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,"
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9    "
                    + "AND  W.WORK_ORDER_NO NOT IN (SELECT WORK_ORDER_NO FROM SD_WORK_ORDER_MAINTAIN)   "
                    + "AND  W.WORK_ORDER_NO NOT IN (SELECT WORK_ORDER_NO FROM SD_WORK_ORDER_TRANSFERS WHERE STATUS=1)   ";

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//            
//            
            
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

               System.out.println("AAAAAAAAAAAA "+ qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            if (status == 99) {
                warrantImie = WorkPaymentController.checkImeiWarranty();
            }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));
                            wo.setStatus(rs.getInt("STATUS"));
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getInt("STATUS") == 1) {
                                wo.setStatusMsg("Open");
                            } else if (rs.getInt("STATUS") == 2) {
                                wo.setStatusMsg("in Progress");
                            } else if (rs.getInt("STATUS") == 3) {
                                wo.setStatusMsg("Close");
                            } else {
                                wo.setStatusMsg("None");
                            }
                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            workOrderList.add(wo);

                        }
                    }

                } else {

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));
                    wo.setStatus(rs.getInt("STATUS"));
                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getInt("STATUS") == 1) {
                        wo.setStatusMsg("Open");
                    } else if (rs.getInt("STATUS") == 2) {
                        wo.setStatusMsg("in Progress");
                    } else if (rs.getInt("STATUS") == 3) {
                        wo.setStatusMsg("Close");
                    } else {
                        wo.setStatusMsg("None");
                    }
                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));
                    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
    
    
    public static MessageWrapper getWorkOrderFullList(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            int paymentType,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderFullList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String payType = (paymentType == 0 ? " NVL(W.PAY_TYPE, 0) = NVL(W.PAY_TYPE, 0)" : " W.PAY_TYPE = "+paymentType+" ");
             if(paymentType == 1 || paymentType == 0){
                payType = " (W.PAY_TYPE = 0 OR W.PAY_TYPE = 1 OR W.PAY_TYPE IS NULL)  ";
            }
            if(paymentType == 4){
                payType = " W.PAY_TYPE IN (2, 3) ";
            }
            
            
            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,"
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG ";

            String whereClous = ""
                    + "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "   
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9    ";
//                    + " AND "+payType+" ";
             //       + "AND  W.WORK_ORDER_NO NOT IN (SELECT WORK_ORDER_NO FROM SD_WORK_ORDER_MAINTAIN)   "
             //       + "AND  W.WORK_ORDER_NO NOT IN (SELECT WORK_ORDER_NO FROM SD_WORK_ORDER_TRANSFERS WHERE STATUS=1)   ";

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//            
//            
            
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            //   System.out.println(qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            if (status == 99) {
                warrantImie = WorkPaymentController.checkImeiWarranty();
            }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));
                            wo.setStatus(rs.getInt("STATUS"));
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getInt("STATUS") == 1) {
                                wo.setStatusMsg("Open");
                            } else if (rs.getInt("STATUS") == 2) {
                                wo.setStatusMsg("in Progress");
                            } else if (rs.getInt("STATUS") == 3) {
                                wo.setStatusMsg("Close");
                            } else {
                                wo.setStatusMsg("None");
                            }
                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            workOrderList.add(wo);

                        }
                    }

                } else {

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));
                    wo.setStatus(rs.getInt("STATUS"));
                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getInt("STATUS") == 1) {
                        wo.setStatusMsg("Open");
                    } else if (rs.getInt("STATUS") == 2) {
                        wo.setStatusMsg("in Progress");
                    } else if (rs.getInt("STATUS") == 3) {
                        wo.setStatusMsg("Close");
                    } else {
                        wo.setStatusMsg("None");
                    }
                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));
                    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        if(con ==null){
             mw.setTotalRecords(1000000);
        }else{
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        }
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    
    
    

    public static MessageWrapper getWorkOrderListToClose(String workorderno,
            String warranty_typedesc,
            String technician,
            String imei,
            String modle_desc,
            String repair_level,
            String estimate_status,
            String status,
            String invoice,
            String status_msg,
            int bisId,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getWorkOrderMaintainList Method Call....................");
        ArrayList<WorkOrderMaintain> workOrderMainList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.workOrderMaintain(order);

            String RepairStatus = (status.equalsIgnoreCase("all") ? " " : " AND M.REPAIR_STATUS=" + status + "");
            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(M.WORK_ORDER_NO,'AA') = NVL(M.WORK_ORDER_NO,'AA')" : "UPPER(M.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String imeNo = (imei.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei + "%')");
            String modleDesc = (modle_desc.equalsIgnoreCase("all") ? "NVL(L.MODEL_DESCRIPTION,'AA') = NVL(L.MODEL_DESCRIPTION,'AA')" : "UPPER(L.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String Tecnician = (technician.equalsIgnoreCase("all") ? "NVL(M.TECHNICIAN,'AA') = NVL(M.TECHNICIAN,'AA')" : "UPPER(M.TECHNICIAN) LIKE UPPER('%" + technician + "%')");
            String levelDesc = (repair_level.equalsIgnoreCase("all") ? "NVL(R.LEVEL_NAME,'AA') = NVL(R.LEVEL_NAME,'AA')" : "UPPER(R.LEVEL_NAME) LIKE UPPER('%" + repair_level + "%')");
            String statusMsg = (status_msg.equalsIgnoreCase("all") ? "NVL(D1.DESCRIPTION,'AA') = NVL(D1.DESCRIPTION,'AA')" : "UPPER(D1.DESCRIPTION) LIKE UPPER('%" + status_msg + "%')");
            String Invoice = (invoice.equalsIgnoreCase("all") ? "NVL(M.INVOICE_NO,'AA') = NVL(M.INVOICE_NO,'AA')" : "UPPER(M.INVOICE_NO) LIKE UPPER('%" + invoice + "%')");
            String BisId = (bisId == 0 ? " " : " AND W.BIS_ID= '" + bisId + "' ");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY M.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "M.WRK_ORD_MTN_ID,"
                    + "M.WORK_ORDER_NO,"
                    + "M.TECHNICIAN,"
                    + "W.IMEI_NO,"
                    + "L.MODEL_DESCRIPTION,"
                    + "R.LEVEL_NAME,"
                    + "M.REPAIR_LEVEL_ID,M.INVOICE_NO,"
                    + "W.DEFECTS,"
                    + "W.STATUS WO_STATUS,"
                    + "M.REPAIR_STATUS,"
                    + "D1.DESCRIPTION RP_DESCRIPTION,"
                    + "W.WARRANTY_VRIF_TYPE";

            String whereClous = "M.WORK_ORDER_NO = W.WORK_ORDER_NO (+) "
                    + "AND W.MODEL_NO = L.MODEL_NO (+) "
                    + "AND M.REPAIR_LEVEL_ID = R.LEVEL_ID (+) "
                    + "AND M.REPAIR_STATUS = D1.CODE (+) "
                    + "AND D1.DOMAIN_NAME = 'RP_STATUS' "
                    //+ "AND W.STATUS != 3 "
                    + "AND W.STATUS IN (2)"//added by Shanka according to Kasun's request
                    + "AND W.STATUS NOT IN (3,4,5,6,7)"//added by Shanka according to Kasun's request
                    + "AND " + workOrderNo + " "
                    + "AND " + imeNo + " "
                    + "AND " + modleDesc + " "
                    + "AND " + Tecnician + " "
                    + "AND " + levelDesc + " "
                    + "AND " + Invoice + " "
                    + "AND " + statusMsg + " "
                    + "AND (M.REPAIR_STATUS=3 OR M.REPAIR_STATUS=2 OR M.REPAIR_STATUS=9)  "
                    + BisId;

            String tables = "SD_WORK_ORDER_MAINTAIN M,SD_WORK_ORDERS W,"
                    + "SD_MODEL_LISTS L,SD_REPAIR_LEVELS R"
                    + ",SD_MDECODE D1";


//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + tables + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//            
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderMaintain wo = new WorkOrderMaintain();
                totalrecode = rs.getInt("CNT");
                wo.setWorkorderMaintainId(rs.getInt("WRK_ORD_MTN_ID"));
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setTechnician(rs.getString("TECHNICIAN"));
                wo.setIemiNo(rs.getString("IMEI_NO"));
                wo.setInvoiceNo(rs.getString("INVOICE_NO"));
                wo.setModelDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setRepairLevelId(rs.getInt("REPAIR_LEVEL_ID"));
                wo.setLevelDesc(rs.getString("LEVEL_NAME"));
                wo.setStatusMsg(rs.getString("WO_STATUS"));
                wo.setRemarks(rs.getString("DEFECTS"));
                wo.setStatusMsg(rs.getString("RP_DESCRIPTION"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setWarrantyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                workOrderMainList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderMaintainList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderMainList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

}
