/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 *
 * @author SDU
 */
public class LogHelper {

    public Logger createLogger(String filename,String jbossPath) {
        Logger logger = Logger.getLogger("name");

        ConsoleAppender appender = null;

        DatedFileAppender outFile = null;

        PatternLayout pl = null;

        logger.setAdditivity(false);
        logger.removeAllAppenders();

        pl = new PatternLayout();
        pl.setConversionPattern("[%d{yyyy/MMM/dd- hh:mm:ss}][%p] %m%n");
        pl.activateOptions();
        appender = new ConsoleAppender(pl);
        try {
            String pathSep = jbossPath.replace("\\", "/");
            String log4jPath = pathSep + "/Log4J";

            System.out.println(log4jPath);
            
            outFile = new DatedFileAppender();

            outFile.activateOptions();
            outFile.setAppend(true);
            outFile.setPrefix(filename + " ");
            outFile.setSuffix(".log");
            outFile.setDirectory(log4jPath);
            outFile.setLayout(pl);

            logger.setLevel(Level.DEBUG);
            logger.addAppender(appender);
            logger.addAppender(outFile);
        } catch (Exception e) {
            logger.fatal("log4j initializing failed:" + e.getMessage());
            e.printStackTrace();
        }
        return logger;
    }

}
