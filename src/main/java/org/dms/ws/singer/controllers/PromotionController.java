/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Promotion;

/**
 *
 * @author SDU
 */
public class PromotionController {
    
    
     public static MessageWrapper getPromotionList(int pro_id,
            String pro_name,
            int start,
            int limit) {
        logger.info("getPromotionList Method Call....................");
        ArrayList<Promotion> promotionList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String promotionId = (pro_id == 0 ? "" : "AND PROMOTION_ID = " + pro_id + "");
            String promotionName = (pro_name.equalsIgnoreCase("all") ? "NVL(PROMOTION_NAME,'AA') = NVL(PROMOTION_NAME,'AA')" : "UPPER(PROMOTION_NAME) LIKE UPPER('%" + pro_name + "%')");
            

            String seletColunm = "PROMOTION_ID,"
                    + "PROMOTION_NAME,"
                    + "TO_CHAR(START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(END_DATE,'YYYY/MM/DD') END_DATE";

            String whereClous = ""+promotionName+" "
                    + ""+promotionId+" "
                    + "AND STATUS =1";

            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_PROMOTION WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY PROMOTION_ID DESC) RN "
                    + " FROM SD_PROMOTION WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                Promotion po = new Promotion();
                totalrecode = rs.getInt("CNT");
                po.setPromotionId(rs.getInt("PROMOTION_ID"));
                po.setPromotionName(rs.getString("PROMOTION_NAME"));
                po.setStartDate(rs.getString("START_DATE"));
                po.setEndDate(rs.getString("END_DATE"));
                promotionList.add(po);
            }

        } catch (Exception ex) {
            logger.info("Error getPromotionList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(promotionList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
    
}
