/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.mail.MessagingException;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.QuickWorkDefect;
import org.dms.ws.singer.entities.QuickWorkOrder;
import org.dms.ws.singer.entities.QuickWorkOrderServiceCenter;
import org.dms.ws.singer.entities.UserBusinessStructure;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderDefect;
import org.dms.ws.singer.entities.WorkOrderMaintain;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import org.dms.ws.singer.utils.DBMapper;
import org.dms.ws.singer.utils.EmaiSendConfirmCode;

/**
 *
 * @author SDU
 */
public class WorkOrderController {

    public static MessageWrapper getWorkOrderList(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String delayDate,
            int paymentType,
            String order,
            String type,
            int wo_type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            //String bisId = (bisid == 0 ? "" : "AND W.SERVICE_BIS_ID = '" + bisid + "'");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String DelayDate = (delayDate.equalsIgnoreCase("all") ? " " : " AND (TRUNC(SYSDATE - W.DELIVERY_DATE)) = " + delayDate + "");
            String payType = (paymentType == 0 ? " NVL(W.PAY_TYPE, 0) = NVL(W.PAY_TYPE, 0)" : " W.PAY_TYPE = " + paymentType + " ");

            String wo_typeQry = (wo_type == 0) ? "" : " AND W.WORK_ORD_STATUS='" + wo_type + "' ";

            System.out.println("wo_typewo_typewo_type" + wo_type);

            if (paymentType == 1 || paymentType == 0) {
                payType = " (W.PAY_TYPE = 0 OR W.PAY_TYPE = 1 OR W.PAY_TYPE IS NULL)  ";
            }
            if (paymentType == 4) {
                payType = " W.PAY_TYPE IN (2, 3) ";
            }

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            ////////////////Check the Workorder is transfered and not accepted by the logged in bis_id. (It happens when the transfer is returned and accepted)
            String chkTrfs = " SELECT COUNT(*)  "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " MINUS   "
                    + " SELECT COUNT(*) "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " AND   STATUS = 2    ";

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + " CASE   "
                    + "     WHEN  W.STATUS=3 "
                    + "     THEN  ROUND(W.DATE_MODIFIED - W.DATE_INSERTED) "
                    + "     ELSE  ROUND(SYSDATE - W.DATE_INSERTED)   "
                    + "     END AS DELAY_DATE,  "
                    // + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,     "
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG,   "
                    + "( " + chkTrfs + " ) AS TRFS, "
                    + "W.PAY_TYPE ";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + DelayDate
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9 "
                    + wo_typeQry
                    + " AND " + payType;

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("qryyyyy " + qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            //  if (status == 99) {
            warrantImie = WorkPaymentController.checkImeiWarranty();
            //   }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));

                            if (rs.getInt("TRFS") == 0) {
                                //////////////////////If there are no Transfers for this Work Order//////////////////
                                wo.setStatus(rs.getInt("STATUS"));

                                if (rs.getInt("STATUS") == 1 || rs.getInt("STATUS") == 4 || rs.getInt("STATUS") == 5 || rs.getInt("STATUS") == 7 || rs.getInt("STATUS") == 8) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 2) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 3) {
                                    wo.setStatusMsg("Close");
                                } else if (rs.getInt("STATUS") == 6) {
                                    wo.setStatusMsg("Close");
                                } else {
                                    wo.setStatusMsg("None");
                                }

                            } else {
                                wo.setStatus(10); ///Change the Status, if there is a Transfer
                                wo.setStatusMsg("Transfered");
                            }

                            //wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                                if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                                    wo.setDelayDate(String.valueOf(0));
                                } else {
                                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                                }
                            }

                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder
                            wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setPaymentType(rs.getInt("PAY_TYPE"));
                            String payDesc = "Customer repairs";
                            if (rs.getInt("PAY_TYPE") == 2) {
                                payDesc = "Stock Repair";
                            }
                            if (rs.getInt("PAY_TYPE") == 3) {
                                payDesc = "Clearance Repairs";
                            }
                            wo.setPaymentTypeDesc(payDesc);

                            workOrderList.add(wo);

                        }
                    }

                } else {

                    int warrantyStatus = 1;

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                            if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                                warrantyStatus = 0;
                            }
                        }
                    }

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));

                    if (rs.getInt("TRFS") == 0) {
                        //////////////////////If there are no Transfers for this Work Order//////////////////
                        wo.setStatus(rs.getInt("STATUS"));

                        if (rs.getInt("STATUS") == 1) {
                            wo.setStatusMsg("Open");
                        } else if (rs.getInt("STATUS") == 2) {
                            wo.setStatusMsg("in Progress");
                        } else if (rs.getInt("STATUS") == 3) {
                            wo.setStatusMsg("Close");
                        } else if (rs.getInt("STATUS") == 6) {
                            wo.setStatusMsg("Close");
                        } else {
                            wo.setStatusMsg("None");
                        }

                    } else {
                        wo.setStatus(10); ///Change the Status, if there is a Transfer
                        wo.setStatusMsg("Transfered");
                    }

                    //wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                        if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                            wo.setDelayDate(String.valueOf(0));
                        } else {
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                        }
                    }

                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));

                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder

                    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    //   wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setExpireStatus(warrantyStatus);

//                    if (warrantyStatus == 0) {
//                        wo.setWarrantyStatusMsg("Under Warranty");
//                    } else {
//                        wo.setWarrantyStatusMsg("Out of Warranty");
//                    }
                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }

        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderListForClearance(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String delayDate,
            int paymentType,
            String order,
            String type,
            int wo_type,
            int start,
            int limit) {

        logger.info("getWorkOrderList for clearance Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            //String bisId = (bisid == 0 ? "" : "AND W.SERVICE_BIS_ID = '" + bisid + "'");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String DelayDate = (delayDate.equalsIgnoreCase("all") ? " " : " AND (TRUNC(SYSDATE - W.DELIVERY_DATE)) = " + delayDate + "");
            String payType = (paymentType == 0 ? " NVL(W.PAY_TYPE, 0) = NVL(W.PAY_TYPE, 0)" : " W.PAY_TYPE = " + paymentType + " ");

            String wo_typeQry = (wo_type == 0) ? "" : " AND W.WORK_ORD_STATUS='" + wo_type + "' ";

            System.out.println("wo_typewo_typewo_type" + wo_type);

            if (paymentType == 1 || paymentType == 0) {
                payType = " (W.PAY_TYPE = 0 OR W.PAY_TYPE = 1 OR W.PAY_TYPE IS NULL)  ";
            }
            if (paymentType == 4) {
                payType = " W.PAY_TYPE IN (2, 3) ";
            }

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            ////////////////Check the Workorder is transfered and not accepted by the logged in bis_id. (It happens when the transfer is returned and accepted)
            String chkTrfs = " SELECT COUNT(*)  "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " MINUS   "
                    + " SELECT COUNT(*) "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " AND   STATUS = 2    ";

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + " CASE   "
                    + "     WHEN  W.STATUS=3 "
                    + "     THEN  ROUND(W.DATE_MODIFIED - W.DATE_INSERTED) "
                    + "     ELSE  ROUND(SYSDATE - W.DATE_INSERTED)   "
                    + "     END AS DELAY_DATE,  "
                    // + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,     "
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG,   "
                    + "( " + chkTrfs + " ) AS TRFS, "
                    + "W.PAY_TYPE ";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + DelayDate
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9 "
                    + wo_typeQry
                    + " AND " + payType;

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("qryyyyy " + qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            //  if (status == 99) {
            warrantImie = WorkPaymentController.checkImeiWarranty();
            //   }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));

                            if (rs.getInt("TRFS") == 0) {
                                //////////////////////If there are no Transfers for this Work Order//////////////////
                                wo.setStatus(rs.getInt("STATUS"));

                                if (rs.getInt("STATUS") == 1 || rs.getInt("STATUS") == 4 || rs.getInt("STATUS") == 5 || rs.getInt("STATUS") == 7 || rs.getInt("STATUS") == 8) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 2) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 3) {
                                    wo.setStatusMsg("Close");
                                } else if (rs.getInt("STATUS") == 6) {
                                    wo.setStatusMsg("Close");
                                } else {
                                    wo.setStatusMsg("None");
                                }

                            } else {
                                wo.setStatus(10); ///Change the Status, if there is a Transfer
                                wo.setStatusMsg("Transfered");
                            }

                            //wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                                if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                                    wo.setDelayDate(String.valueOf(0));
                                } else {
                                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                                }
                            }

                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder
                            wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setPaymentType(rs.getInt("PAY_TYPE"));
                            String payDesc = "Customer repairs";
                            if (rs.getInt("PAY_TYPE") == 2) {
                                payDesc = "Stock Repair";
                            }
                            if (rs.getInt("PAY_TYPE") == 3) {
                                payDesc = "Clearance Repairs";
                            }
                            wo.setPaymentTypeDesc(payDesc);

                            workOrderList.add(wo);

                        }
                    }

                } else {

                    int warrantyStatus = 1;

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                            if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                                warrantyStatus = 0;
                            }
                        }
                    }

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));

                    if (rs.getInt("TRFS") == 0) {
                        //////////////////////If there are no Transfers for this Work Order//////////////////
                        wo.setStatus(rs.getInt("STATUS"));

                        if (rs.getInt("STATUS") == 1) {
                            wo.setStatusMsg("Open");
                        } else if (rs.getInt("STATUS") == 2) {
                            wo.setStatusMsg("in Progress");
                        } else if (rs.getInt("STATUS") == 3) {
                            wo.setStatusMsg("Close");
                        } else if (rs.getInt("STATUS") == 6) {
                            wo.setStatusMsg("Close");
                        } else {
                            wo.setStatusMsg("None");
                        }

                    } else {
                        wo.setStatus(10); ///Change the Status, if there is a Transfer
                        wo.setStatusMsg("Transfered");
                    }

                    //wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                        if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                            wo.setDelayDate(String.valueOf(0));
                        } else {
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                        }
                    }

                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));

                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder

                    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    //   wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setExpireStatus(warrantyStatus);

//                    if (warrantyStatus == 0) {
//                        wo.setWarrantyStatusMsg("Under Warranty");
//                    } else {
//                        wo.setWarrantyStatusMsg("Out of Warranty");
//                    }
                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }

        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderListforHistory(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String delayDate,
            int paymentType,
            String order,
            String type,
            int wo_type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            //String bisId = (bisid == 0 ? "" : "AND W.SERVICE_BIS_ID = '" + bisid + "'");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String DelayDate = (delayDate.equalsIgnoreCase("all") ? " " : " AND (TRUNC(SYSDATE - W.DELIVERY_DATE)) = " + delayDate + "");
            String payType = (paymentType == 0 ? " NVL(W.PAY_TYPE, 0) = NVL(W.PAY_TYPE, 0)" : " W.PAY_TYPE = " + paymentType + " ");

            String wo_typeQry = (wo_type == 0) ? "" : " AND W.WORK_ORD_STATUS='" + wo_type + "' ";

            System.out.println("wo_typewo_typewo_type" + wo_type);

            if (paymentType == 1 || paymentType == 0) {
                payType = " (W.PAY_TYPE = 0 OR W.PAY_TYPE = 1 OR W.PAY_TYPE IS NULL)  ";
            }
            if (paymentType == 4) {
                payType = " W.PAY_TYPE IN (2, 3) ";
            }

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            ////////////////Check the Workorder is transfered and not accepted by the logged in bis_id. (It happens when the transfer is returned and accepted)
            String chkTrfs = " SELECT COUNT(*)  "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " MINUS   "
                    + " SELECT COUNT(*) "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " AND   STATUS = 2    ";

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + " CASE   "
                    + "     WHEN  W.STATUS=3 "
                    + "     THEN  ROUND(W.DATE_MODIFIED - W.DATE_INSERTED) "
                    + "     ELSE  ROUND(SYSDATE - W.DATE_INSERTED)   "
                    + "     END AS DELAY_DATE,  "
                    // + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,     "
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG,   "
                    + "( " + chkTrfs + " ) AS TRFS, "
                    + "W.PAY_TYPE ";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'WOM_RP_STATUS' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + DelayDate
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9 "
                    + wo_typeQry
                    + " AND " + payType;

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("qryyyyy " + qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            //  if (status == 99) {
            warrantImie = WorkPaymentController.checkImeiWarranty();
            //   }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));

                            if (rs.getInt("TRFS") == 0) {
                                //////////////////////If there are no Transfers for this Work Order//////////////////
                                wo.setStatus(rs.getInt("STATUS"));

                                if (rs.getInt("STATUS") == 1 || rs.getInt("STATUS") == 4 || rs.getInt("STATUS") == 5 || rs.getInt("STATUS") == 7 || rs.getInt("STATUS") == 8) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 2) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 3) {
                                    wo.setStatusMsg("Close");
                                } else if (rs.getInt("STATUS") == 6) {
                                    wo.setStatusMsg("Close");
                                } else {
                                    wo.setStatusMsg("None");
                                }

                            } else {
                                wo.setStatus(10); ///Change the Status, if there is a Transfer
                                wo.setStatusMsg("Transfered");
                            }

                            //wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                                if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                                    wo.setDelayDate(String.valueOf(0));
                                } else {
                                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                                }
                            }

                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder
                            wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setPaymentType(rs.getInt("PAY_TYPE"));
                            String payDesc = "Customer repairs";
                            if (rs.getInt("PAY_TYPE") == 2) {
                                payDesc = "Stock Repair";
                            }
                            if (rs.getInt("PAY_TYPE") == 3) {
                                payDesc = "Clearance Repairs";
                            }
                            wo.setPaymentTypeDesc(payDesc);

                            workOrderList.add(wo);

                        }
                    }

                } else {

                    int warrantyStatus = 1;

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                            if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                                warrantyStatus = 0;
                            }
                        }
                    }

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));

                    if (rs.getInt("TRFS") == 0) {
                        //////////////////////If there are no Transfers for this Work Order//////////////////
                        wo.setStatus(rs.getInt("STATUS"));

                        if (rs.getInt("STATUS") == 1) {
                            wo.setStatusMsg("Open");
                        } else if (rs.getInt("STATUS") == 2) {
                            wo.setStatusMsg("in Progress");
                        } else if (rs.getInt("STATUS") == 3) {
                            wo.setStatusMsg("Close");
                        } else if (rs.getInt("STATUS") == 6) {
                            wo.setStatusMsg("Close");
                        } else {
                            wo.setStatusMsg("None");
                        }

                    } else {
                        wo.setStatus(10); ///Change the Status, if there is a Transfer
                        wo.setStatusMsg("Transfered");
                    }

                    //wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                        if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                            wo.setDelayDate(String.valueOf(0));
                        } else {
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                        }
                    }

                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));

                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder

                    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    //   wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setExpireStatus(warrantyStatus);

//                    if (warrantyStatus == 0) {
//                        wo.setWarrantyStatusMsg("Under Warranty");
//                    } else {
//                        wo.setWarrantyStatusMsg("Out of Warranty");
//                    }
                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }

        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderListForSelectWO(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String delayDate,
            int paymentType,
            String order,
            String type,
            int wo_type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            String bisId = (bisid == 0 ? "" : "AND W.SERVICE_BIS_ID = '" + bisid + "'");
            // String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String DelayDate = (delayDate.equalsIgnoreCase("all") ? " " : " AND (TRUNC(SYSDATE - W.DELIVERY_DATE)) = " + delayDate + "");
            String payType = (paymentType == 0 ? " NVL(W.PAY_TYPE, 0) = NVL(W.PAY_TYPE, 0)" : " W.PAY_TYPE = " + paymentType + " ");

            String wo_typeQry = (wo_type == 0) ? "" : " AND W.WORK_ORD_STATUS='" + wo_type + "' ";

            System.out.println("wo_typewo_typewo_type" + wo_type);

            if (paymentType == 1 || paymentType == 0) {
                payType = " (W.PAY_TYPE = 0 OR W.PAY_TYPE = 1 OR W.PAY_TYPE IS NULL)  ";
            }
            if (paymentType == 4) {
                payType = " W.PAY_TYPE IN (2, 3) ";
            }

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            ////////////////Check the Workorder is transfered and not accepted by the logged in bis_id. (It happens when the transfer is returned and accepted)
            String chkTrfs = " SELECT COUNT(*)  "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " MINUS   "
                    + " SELECT COUNT(*) "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " AND   STATUS = 2    ";

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + " CASE   "
                    + "     WHEN  W.STATUS=3 "
                    + "     THEN  ROUND(W.DATE_MODIFIED - W.DATE_INSERTED) "
                    + "     ELSE  ROUND(SYSDATE - W.DATE_INSERTED)   "
                    + "     END AS DELAY_DATE,  "
                    // + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,     "
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG,   "
                    + "( " + chkTrfs + " ) AS TRFS, "
                    + "W.PAY_TYPE ";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + DelayDate
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9 "
                    + wo_typeQry
                    + " AND " + payType;

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("qryyyyy " + qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            //  if (status == 99) {
            warrantImie = WorkPaymentController.checkImeiWarranty();
            //   }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));

                            if (rs.getInt("TRFS") == 0) {
                                //////////////////////If there are no Transfers for this Work Order//////////////////
                                wo.setStatus(rs.getInt("STATUS"));

                                if (rs.getInt("STATUS") == 1 || rs.getInt("STATUS") == 4 || rs.getInt("STATUS") == 5 || rs.getInt("STATUS") == 7 || rs.getInt("STATUS") == 8) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 2) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 3) {
                                    wo.setStatusMsg("Close");
                                } else if (rs.getInt("STATUS") == 6) {
                                    wo.setStatusMsg("Close");
                                } else {
                                    wo.setStatusMsg("None");
                                }

                            } else {
                                wo.setStatus(10); ///Change the Status, if there is a Transfer
                                wo.setStatusMsg("Transfered");
                            }

                            //wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                                if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                                    wo.setDelayDate(String.valueOf(0));
                                } else {
                                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                                }
                            }

                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder
                            wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setPaymentType(rs.getInt("PAY_TYPE"));
                            String payDesc = "Customer repairs";
                            if (rs.getInt("PAY_TYPE") == 2) {
                                payDesc = "Stock Repair";
                            }
                            if (rs.getInt("PAY_TYPE") == 3) {
                                payDesc = "Clearance Repairs";
                            }
                            wo.setPaymentTypeDesc(payDesc);

                            workOrderList.add(wo);

                        }
                    }

                } else {

                    int warrantyStatus = 1;

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                            if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                                warrantyStatus = 0;
                            }
                        }
                    }

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));

                    if (rs.getInt("TRFS") == 0) {
                        //////////////////////If there are no Transfers for this Work Order//////////////////
                        wo.setStatus(rs.getInt("STATUS"));

                        if (rs.getInt("STATUS") == 1) {
                            wo.setStatusMsg("Open");
                        } else if (rs.getInt("STATUS") == 2) {
                            wo.setStatusMsg("in Progress");
                        } else if (rs.getInt("STATUS") == 3) {
                            wo.setStatusMsg("Close");
                        } else if (rs.getInt("STATUS") == 6) {
                            wo.setStatusMsg("Close");
                        } else {
                            wo.setStatusMsg("None");
                        }

                    } else {
                        wo.setStatus(10); ///Change the Status, if there is a Transfer
                        wo.setStatusMsg("Transfered");
                    }

                    //wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                        if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                            wo.setDelayDate(String.valueOf(0));
                        } else {
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                        }
                    }

                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));

                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder

                    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    //   wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setExpireStatus(warrantyStatus);

//                    if (warrantyStatus == 0) {
//                        wo.setWarrantyStatusMsg("Under Warranty");
//                    } else {
//                        wo.setWarrantyStatusMsg("Out of Warranty");
//                    }
                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        if (con == null) {
            mw.setTotalRecords(1000000);

        } else {
            mw.setTotalRecords(totalrecode);

        }
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getQuickWorkOrderList(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String delayDate,
            int paymentType,
            String order,
            String type,
            int wo_type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_ADDRESS,'AA') = NVL(W.CUSTOMER_ADDRESS,'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String DelayDate = (delayDate.equalsIgnoreCase("all") ? " " : " AND (TRUNC(SYSDATE - W.DELIVERY_DATE)) = " + delayDate + "");
            String payType = (paymentType == 0 ? " NVL(W.PAY_TYPE, 0) = NVL(W.PAY_TYPE, 0)" : " W.PAY_TYPE = " + paymentType + " ");

            String wo_typeQry = (wo_type == 0) ? "" : " AND W.WORK_ORD_STATUS='" + wo_type + "' ";

            System.out.println("wo_typewo_typewo_type" + wo_type);

            if (paymentType == 1) {
                payType = " (W.PAY_TYPE = 0 OR W.PAY_TYPE = 1 OR W.PAY_TYPE IS NULL)  ";
            }
            if (paymentType == 2 || paymentType == 3) {
                payType = " W.PAY_TYPE IN (2, 3) ";
            }

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            ////////////////Check the Workorder is transfered and not accepted by the logged in bis_id. (It happens when the transfer is returned and accepted)
            String chkTrfs = " SELECT COUNT(*)  "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " MINUS   "
                    + " SELECT COUNT(*) "
                    + " FROM SD_WORK_ORDER_TRANSFERS    "
                    + " WHERE WORK_ORDER_NO = W.WORK_ORDER_NO   "
                    + " AND   (BIS_ID = W.BIS_ID)   "
                    + " AND   STATUS = 2    ";

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + " CASE   "
                    + "     WHEN  W.STATUS=3 "
                    + "     THEN  ROUND(W.DATE_MODIFIED - W.DATE_INSERTED) "
                    + "     ELSE  ROUND(SYSDATE - W.DATE_INSERTED)   "
                    + "     END AS DELAY_DATE,  "
                    // + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,     "
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG,   "
                    + "( " + chkTrfs + " ) AS TRFS, "
                    + "W.PAY_TYPE ";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + DelayDate
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9 "
                    + wo_typeQry
                    + " AND " + payType;

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("qryyyyy " + qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = new ArrayList<>();

            /*
            
             // Status = 99 = Work Order Maintainance record should be added only if it is an out of warranty IMEI.
             Therefore, to identify this webservice is called to select imei to create a IMEI maintainance
             status value is sent as 99.
             As a result in CMS WorkOrder Maintainance -> Add new and click Select Work Order button the loading grid takes status==99 section
           
             */
            //  if (status == 99) {
            warrantImie = WorkPaymentController.checkImeiWarranty();
            //   }

            while (rs.next()) {

                if (status == 99) {

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {

                            WorkOrder wo = new WorkOrder();
                            totalrecode = rs.getInt("CNT");
                            wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                            wo.setEmail(rs.getString("EMAIL"));
                            wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            wo.setImeiNo(rs.getString("IMEI_NO"));
                            wo.setProduct(rs.getString("PRODUCT"));
                            wo.setBrand(rs.getString("BRAND"));
                            wo.setModleNo(rs.getInt("MODEL_NO"));
                            wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            wo.setDefectNo(rs.getString("DEFECTS"));
                            wo.setRccReference(rs.getString("RCC_REFERENCE"));
                            wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                            wo.setBisId(rs.getInt("BIS_ID"));
                            wo.setSchemeId(rs.getInt("SCHEME_ID"));
                            wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                            wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                            wo.setTecnician(rs.getString("TECHNICIAN"));
                            wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                            wo.setLevelId(rs.getInt("LEVEL_ID"));
                            wo.setRemarks(rs.getString("REMARKS"));
                            wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                            wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                            wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                            wo.setCourierNo(rs.getString("COURIER_NO"));
                            wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                            wo.setGatePass(rs.getString("GATE_PASS"));

                            if (rs.getInt("TRFS") == 0) {
                                //////////////////////If there are no Transfers for this Work Order//////////////////
                                wo.setStatus(rs.getInt("STATUS"));

                                if (rs.getInt("STATUS") == 1 || rs.getInt("STATUS") == 4 || rs.getInt("STATUS") == 5 || rs.getInt("STATUS") == 7 || rs.getInt("STATUS") == 8) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 2) {
                                    wo.setStatusMsg("in Progress");
                                } else if (rs.getInt("STATUS") == 3) {
                                    wo.setStatusMsg("Close");
                                } else if (rs.getInt("STATUS") == 6) {
                                    wo.setStatusMsg("Close");
                                } else {
                                    wo.setStatusMsg("None");
                                }

                            } else {
                                wo.setStatus(10); ///Change the Status, if there is a Transfer
                                wo.setStatusMsg("Transfered");
                            }

                            //wo.setDelayDate(rs.getString("DELAY_DATE"));
                            if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                                if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                                    wo.setDelayDate(String.valueOf(0));
                                } else {
                                    wo.setDelayDate(rs.getString("DELAY_DATE"));
                                }
                            }

                            wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            wo.setCreateDate(rs.getString("WO_DATE"));
                            System.out.println("warrantyStatusMsg" + rs.getString("DESCRIPTION"));
                            wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                            wo.setLevelName(rs.getString("LEVEL_NAME"));
                            wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            wo.setBinName(rs.getString("BIS_NAME"));
                            wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                            wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                            wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                            wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                            wo.setRepairDate(rs.getString("REPAIR_DATE"));
                            wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                            wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder
                            wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                            wo.setPaymentType(rs.getInt("PAY_TYPE"));
                            String payDesc = "Customer repairs";
                            if (rs.getInt("PAY_TYPE") == 2) {
                                payDesc = "Stock Repair";
                            }
                            if (rs.getInt("PAY_TYPE") == 3) {
                                payDesc = "Clearance Repairs";
                            }
                            wo.setPaymentTypeDesc(payDesc);

                            workOrderList.add(wo);

                        }
                    }

                } else {

                    int warrantyStatus = 1;

                    for (ImeiMaster i : warrantImie) {
                        if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                            if (rs.getInt("WARRANTY_VRIF_TYPE") != 2) {
                                warrantyStatus = 0;
                            }
                        }
                    }

                    WorkOrder wo = new WorkOrder();
                    totalrecode = rs.getInt("CNT");
                    wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                    wo.setEmail(rs.getString("EMAIL"));
                    wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    wo.setImeiNo(rs.getString("IMEI_NO"));
                    wo.setProduct(rs.getString("PRODUCT"));
                    wo.setBrand(rs.getString("BRAND"));
                    wo.setModleNo(rs.getInt("MODEL_NO"));
                    wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    wo.setDefectNo(rs.getString("DEFECTS"));
                    wo.setRccReference(rs.getString("RCC_REFERENCE"));
                    wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                    wo.setBisId(rs.getInt("BIS_ID"));
                    wo.setSchemeId(rs.getInt("SCHEME_ID"));
                    wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                    wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                    wo.setTecnician(rs.getString("TECHNICIAN"));
                    wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                    wo.setLevelId(rs.getInt("LEVEL_ID"));
                    wo.setRemarks(rs.getString("REMARKS"));
                    wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                    wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                    wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                    wo.setCourierNo(rs.getString("COURIER_NO"));
                    wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                    wo.setGatePass(rs.getString("GATE_PASS"));

                    if (rs.getInt("TRFS") == 0) {
                        //////////////////////If there are no Transfers for this Work Order//////////////////
                        wo.setStatus(rs.getInt("STATUS"));

                        if (rs.getInt("STATUS") == 1) {
                            wo.setStatusMsg("Open");
                        } else if (rs.getInt("STATUS") == 2) {
                            wo.setStatusMsg("in Progress");
                        } else if (rs.getInt("STATUS") == 3) {
                            wo.setStatusMsg("Close");
                        } else if (rs.getInt("STATUS") == 6) {
                            wo.setStatusMsg("Close");
                        } else {
                            wo.setStatusMsg("None");
                        }

                    } else {
                        wo.setStatus(10); ///Change the Status, if there is a Transfer
                        wo.setStatusMsg("Transfered");
                    }

                    //wo.setDelayDate(rs.getString("DELAY_DATE"));
                    if (rs.getString("DELAY_DATE") != null && rs.getString("DELAY_DATE").length() > 0) {
                        if (Integer.parseInt(rs.getString("DELAY_DATE")) < 0) {
                            wo.setDelayDate(String.valueOf(0));
                        } else {
                            wo.setDelayDate(rs.getString("DELAY_DATE"));
                        }
                    }

                    wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    wo.setCreateDate(rs.getString("WO_DATE"));

                    wo.setLevelName(rs.getString("LEVEL_NAME"));
                    wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    wo.setBinName(rs.getString("BIS_NAME"));
                    wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                    wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                    wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                    wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                    wo.setRepairDate(rs.getString("REPAIR_DATE"));
                    wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                    wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder

                    //    wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                    //   wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));
                    wo.setExpireStatus(warrantyStatus);

                    if (warrantyStatus == 0) {
                        wo.setWarrantyStatusMsg("Under Warranty");
                    } else {
                        wo.setWarrantyStatusMsg("Out of Warranty");
                    }

                    workOrderList.add(wo);

                }

            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderList123(String workorderno,
            String customername,
            String customeraddress,
            String worktelephoneNo,
            String email,
            String customernic,
            String imeino,
            String product,
            String brand,
            int modleno,
            String assessoriesretained,
            String defectno,
            String rccreference,
            String dateofsale,
            String proofofpurches,
            int bisid,
            int schemeid,
            int workorderstatus,
            int warrentyvriftype,
            int nonwarrentyvriftype,
            String tecnician,
            String actiontaken,
            int levelid,
            String remarks,
            int repairstatus,
            int deleverytype,
            int transferlocation,
            String courierno,
            String deleverydate,
            String gatepass,
            int status,
            String bis_name,
            String warranty_msg,
            int service_id,
            String servie_desc,
            String model_desc,
            String defect_desc,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(WORK_ORDER_NO,'AA') = NVL(WORK_ORDER_NO,'AA')" : "UPPER(WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(CUSTOMER_NAME,'AA') = NVL(CUSTOMER_NAME,'AA')" : "UPPER(CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String customerAddress = (customeraddress.equalsIgnoreCase("all") ? "NVL(CUSTOMER_ADDRESS,'AA') = NVL(CUSTOMER_ADDRESS,'AA')" : "UPPER(CUSTOMER_ADDRESS) LIKE UPPER('%" + customeraddress + "%')");
            String workTelePhoneNo = (worktelephoneNo.equalsIgnoreCase("all") ? "NVL(W.TELEPHONE_NO,'AA') = NVL(W.TELEPHONE_NO,'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + worktelephoneNo + "%')");
            String Email = (email.equalsIgnoreCase("all") ? "NVL(W.EMAIL,'AA') = NVL(W.EMAIL,'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')");
            String customerNic = (customernic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customernic + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(W.BRAND,'AA') = NVL(W.BRAND,'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleNo = (modleno == 0 ? "" : "AND W.MODEL_NO = '" + modleno + "'");
            String assessorieRetained = (assessoriesretained.equalsIgnoreCase("all") ? "NVL(W.ACCESSORIES_RETAINED,'AA') = NVL(W.ACCESSORIES_RETAINED,'AA')" : "UPPER(W.ACCESSORIES_RETAINED) LIKE UPPER('%" + assessoriesretained + "%')");
            String defectNo = (defectno.equalsIgnoreCase("all") ? "NVL(W.DEFECTS,'AA') = NVL(W.DEFECTS,'AA')" : "UPPER(W.DEFECTS) LIKE UPPER('%" + defectno + "%')");
            String defectDesc = (defect_desc.equalsIgnoreCase("all") ? "NVL(D.MAJ_CODE_DESC,'AA') = NVL(D.MAJ_CODE_DESC,'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defect_desc + "%')");
            String rccReference = (rccreference.equalsIgnoreCase("all") ? "NVL(W.RCC_REFERENCE,'AA') = NVL(W.RCC_REFERENCE,'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccreference + "%')");
            String dateOfSale = (dateofsale.equalsIgnoreCase("all") ? "NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_OF_SALE,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_OF_SALE = TO_DATE('" + dateofsale + "','YYYY/MM/DD')");
            String proofOfPurchace = (proofofpurches.equalsIgnoreCase("all") ? "NVL(W.PROOF_OF_PURCHASE,'AA') = NVL(W.PROOF_OF_PURCHASE,'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofofpurches + "%')");
            String bisId = (bisid == 0 ? "" : "AND W.BIS_ID = '" + bisid + "'");
            String schemaId = (schemeid == 0 ? "" : "AND W.SCHEME_ID = '" + schemeid + "'");
            String workOrderStatus = (workorderstatus == 0 ? "" : "AND W.WORK_ORD_STATUS = '" + workorderstatus + "'");
            String warrentVerifType = (warrentyvriftype == 0 ? "" : "AND W.WARRANTY_VRIF_TYPE = '" + warrentyvriftype + "'");
            String nonwarrentVerifType = (nonwarrentyvriftype == 0 ? "" : "AND W.NON_WARRANTY_VRIF_TYPE = '" + nonwarrentyvriftype + "'");
            String Tecnician = (tecnician.equalsIgnoreCase("all") ? "NVL(W.TECHNICIAN,'AA') = NVL(W.TECHNICIAN,'AA')" : "UPPER(W.TECHNICIAN) LIKE UPPER('%" + tecnician + "%')");
            String actionTaken = (actiontaken.equalsIgnoreCase("all") ? "NVL(W.ACTION_TAKEN,'AA') = NVL(W.ACTION_TAKEN,'AA')" : "UPPER(W.ACTION_TAKEN) LIKE UPPER('%" + actiontaken + "%')");
            String levelId = (levelid == 0 ? "" : "AND W.LEVEL_ID = '" + levelid + "'");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(W.REMARKS,'AA') = NVL(W.REMARKS,'AA')" : "UPPER(W.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String repairStatus = (repairstatus == 0 ? "" : "AND W.REPAIR_STATUS = '" + repairstatus + "'");
            String deleveryType = (deleverytype == 0 ? "" : "AND W.DELIVERY_TYPE = '" + deleverytype + "'");
            String transferLocation = (transferlocation == 0 ? "" : "AND W.TRANSFER_LOCATION = '" + transferlocation + "'");
            String couriorNo = (courierno.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + courierno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String gatePass = (gatepass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gatepass + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            String warrantyMsg = (warranty_msg.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warranty_msg + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String ServiceBisId = (service_id == 0 ? "" : "AND W.SERVICE_BIS_ID = " + service_id + "");
            String serviceBisName = (servie_desc.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + servie_desc + "%')");
            String modleDesc = (model_desc.equalsIgnoreCase("all") ? "NVL(S.MODEL_DESCRIPTION,'AA') = NVL(S.MODEL_DESCRIPTION,'AA')" : "UPPER(S.MODEL_DESCRIPTION) LIKE UPPER('%" + model_desc + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "WORK_ORDER_NO,"
                    + "CUSTOMER_NAME,"
                    + "CUSTOMER_ADDRESS,"
                    + "TELEPHONE_NO,"
                    + "EMAIL,"
                    + "CUSTOMER_NIC,"
                    + "IMEI_NO,"
                    + "PRODUCT,"
                    + "BRAND,"
                    + "MODEL_NO,"
                    + "ACCESSORIES_RETAINED,"
                    + "DEFECTS,"
                    + "RCC_REFERENCE,"
                    + "DATE_OF_SALE,"
                    + "PROOF_OF_PURCHASE,"
                    + "BIS_ID,"
                    + "SCHEME_ID,"
                    + "WORK_ORD_STATUS,"
                    + "WARRANTY_VRIF_TYPE,"
                    + "NON_WARRANTY_VRIF_TYPE,"
                    + "TECHNICIAN,"
                    + "ACTION_TAKEN,"
                    + "LEVEL_ID,"
                    + "REMARKS,"
                    + "REPAIR_STATUS,"
                    + "DELIVERY_TYPE,"
                    + "TRANSFER_LOCATION,"
                    + "COURIER_NO,"
                    + "DELIVERY_DATE,"
                    + "GATE_PASS,"
                    + "STATUS,"
                    + "DELAY_DATE,"
                    + "NON_WARRANTY_REMARKS,"
                    + "CUSTOMER_COMPLAIN,"
                    + "WO_DATE,"
                    + "LEVEL_NAME,"
                    + "DESCRIPTION,"
                    + "MODEL_DESCRIPTION,"
                    + "BIS_NAME,"
                    + "MAJ_CODE_DESC,"
                    + "SERVICE_BIS_ID,"
                    + "BIS_NAME SERVICE_NAME,"
                    + "REPAIR_DATE,"
                    + "DESCRIPTION REPAIR_MSG";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND " + workOrderNo + " "
                    + "AND " + customerName + " "
                    + "AND " + customerAddress + " "
                    + "AND " + workTelePhoneNo + " "
                    + "AND " + Email + " "
                    + "AND " + customerNic + " "
                    + "AND " + imeNo + " "
                    + "AND " + Product + " "
                    + "AND " + Brand + " "
                    + " " + modleNo + " "
                    + "AND " + assessorieRetained + " "
                    + "AND " + defectNo + " "
                    + "AND " + rccReference + " "
                    + "AND " + dateOfSale + " "
                    + "AND " + proofOfPurchace + " "
                    + " " + bisId + " "
                    + " " + schemaId + " "
                    + " " + workOrderStatus + " "
                    + " " + warrentVerifType + " "
                    + " " + nonwarrentVerifType + " "
                    + "AND " + Tecnician + " "
                    + "AND " + actionTaken + " "
                    + "" + levelId + " "
                    + "AND " + Remarks + " "
                    + "" + repairStatus + " "
                    + "" + deleveryType + " "
                    + "" + transferLocation + " "
                    + "AND " + couriorNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + " "
                    + "AND " + bisName + " "
                    + "AND " + warrantyMsg + " "
                    + "" + Status + " "
                    + "" + ServiceBisId + "  "
                    + "AND " + serviceBisName + " "
                    + "AND " + modleDesc + " "
                    + "AND " + defectDesc + " "
                    + "AND W.STATUS != 9";

            String table = "SELECT W.WORK_ORDER_NO,W.CUSTOMER_NAME,W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,W.EMAIL,W.CUSTOMER_NIC,W.IMEI_NO,"
                    + "W.PRODUCT,W.BRAND,W.MODEL_NO,W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,W.RCC_REFERENCE,TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + " W.PROOF_OF_PURCHASE,W.BIS_ID,W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,W.WARRANTY_VRIF_TYPE,W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,W.ACTION_TAKEN,W.LEVEL_ID,W.REMARKS,W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,W.TRANSFER_LOCATION,W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,W.GATE_PASS,"
                    + " W.STATUS,TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,"
                    + " W.NON_WARRANTY_REMARKS,W.CUSTOMER_COMPLAIN,TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,M.DESCRIPTION,S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,D.MAJ_CODE_DESC,W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG "
                    + "FROM SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,"
                    + "SD_MDECODE S2 "
                    + " WHERE W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + " AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + " AND M.DOMAIN_NAME = 'WARANTY'  "
                    + " AND W.MODEL_NO = S.MODEL_NO (+) "
                    + " AND W.BIS_ID = B.BIS_ID (+) "
                    + " AND W.DEFECTS = D.MAJ_CODE (+) "
                    + " AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + " AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS'  "
                    + "AND W.STATUS != 9 ";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                totalrecode = rs.getInt("CNT");
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setBrand(rs.getString("BRAND"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setRccReference(rs.getString("RCC_REFERENCE"));
                wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                wo.setBisId(rs.getInt("BIS_ID"));
                wo.setSchemeId(rs.getInt("SCHEME_ID"));
                wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wo.setTecnician(rs.getString("TECHNICIAN"));
                wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                wo.setLevelId(rs.getInt("LEVEL_ID"));
                wo.setRemarks(rs.getString("REMARKS"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setCourierNo(rs.getString("COURIER_NO"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setGatePass(rs.getString("GATE_PASS"));
                wo.setStatus(rs.getInt("STATUS"));
                wo.setDelayDate(rs.getString("DELAY_DATE"));
                if (rs.getInt("STATUS") == 1) {
                    wo.setStatusMsg("Open");
                } else if (rs.getInt("STATUS") == 2) {
                    wo.setStatusMsg("in Progress");
                } else if (rs.getInt("STATUS") == 3) {
                    wo.setStatusMsg("Close");
                } else {
                    wo.setStatusMsg("Close");
                }
                wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                wo.setCreateDate(rs.getString("WO_DATE"));
                wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                wo.setLevelName(rs.getString("LEVEL_NAME"));
                wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setBinName(rs.getString("BIS_NAME"));
                wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                wo.setRepairDate(rs.getString("REPAIR_DATE"));
                wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static int checkImeiWarranty(String imei) {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        int imeiWarranty = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String qry = "  SELECT D.IMEI_NO,   CASE  "
                    + "                         WHEN   TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') >= SYSDATE THEN '1'           "
                    + "                         WHEN   TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') < SYSDATE THEN '2'            "
                    + "                         END AS WARRANTY_STATUS      "
                    + "     FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A, SD_WARRENTY_REGISTRATION R     "
                    + "     WHERE D.MODEL_NO = M.MODEL_NO (+)           "
                    + "     AND   D.IMEI_NO = R.IMEI_no (+)             "
                    + "     AND M.WRN_SCHEME_ID = W.SCHEME_ID (+)       "
                    + "     AND D.IMEI_NO = A.IMEI (+)                  "
                    + "     AND D.IMEI_NO='" + imei + "'    ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            if (rs_master_cnt.next()) {
                imeiWarranty = rs_master_cnt.getInt("WARRANTY_STATUS");
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }

        dbCon.ConectionClose(con);
        return imeiWarranty;

    }

    public static ValidationWrapper addWorkOrder(WorkOrder wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWorkOrder Mehtod Call.......");
        int result = 0;
        String last_workorder_id = "WO000000";
        String returnRefNo = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int WOM_cnt = 0;

        if (con != null) {

            try {

                ResultSet rs_wom_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_WORK_ORDER_MAINTAIN "
                        + "WHERE REPAIR_STATUS IN (1,4,5,6,7,8) "
                        + "AND WORK_ORDER_NO IN (SELECT WORK_ORDER_NO FROM SD_WORK_ORDERS WHERE IMEI_NO  = '" + wo_info.getImeiNo() + "')");

                while (rs_wom_cnt.next()) {
                    WOM_cnt = rs_wom_cnt.getInt("CNT");
                }
                if (WOM_cnt > 0) {
                    result = 2;
                } else {

                    ResultSet rs_wocnt = dbCon.search(con, "SELECT WORK_ORDER_NO "
                            + "FROM SD_WORK_ORDERS "
                            + "WHERE ROWNUM = 1 "
                            + "ORDER BY WORK_ORDER_NO DESC ");

                    if (rs_wocnt.next()) {
                        last_workorder_id = rs_wocnt.getString(1);
                        logger.info("Last Work Order id   " + last_workorder_id);
                    }

                    last_workorder_id = last_workorder_id.split("WO")[1];
                    int nxt_wo_num = Integer.parseInt(last_workorder_id) + 1;
                    String nxtWorkOrderId = String.valueOf(nxt_wo_num);
                    int nxtWorkOrderNo_Length = nxtWorkOrderId.length();

                    String nxtNewWorkOrderId = "WO";
                    if (nxtWorkOrderNo_Length < 6) {
                        for (int i = 0; i < (6 - nxtWorkOrderNo_Length); i++) {
                            nxtNewWorkOrderId += "0";
                        }
                    }
                    nxtNewWorkOrderId += nxtWorkOrderId;

                    logger.info("Inserting................Work Order No = " + nxtNewWorkOrderId);

                    //calling send sms controller for send sms
                    SmsController sms = new SmsController();

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();

                    System.out.println(df.format(date));

                    String smsString = "Dear Customer, \n"
                            + "Your device received to the service center on " + df.format(date) + " REF NO: " + nxtNewWorkOrderId + ". Repair progress will be informed soon.\n"
                            + "Tel: 0115400400. T&C apply";

                    System.out.println(smsString);

                    //send sms
                    sms.send(wo_info.getWorkTelephoneNo(), smsString);

                    logger.info("Submitting to the SMS gateway..........");
                    returnRefNo = nxtNewWorkOrderId;

                    String column = "WORK_ORDER_NO,"
                            + "CUSTOMER_NAME,"
                            + "CUSTOMER_ADDRESS,"
                            + "TELEPHONE_NO,"
                            + "EMAIL,"
                            + "CUSTOMER_NIC,"
                            + "IMEI_NO,"
                            + "PRODUCT,"
                            + "BRAND,"
                            + "MODEL_NO,"
                            + "ACCESSORIES_RETAINED,"
                            + "DEFECTS,"
                            + "RCC_REFERENCE,"
                            + "DATE_OF_SALE,"
                            + "PROOF_OF_PURCHASE,"
                            + "BIS_ID,"
                            + "WORK_ORD_STATUS,"
                            + "WARRANTY_VRIF_TYPE,"
                            + "NON_WARRANTY_VRIF_TYPE,"
                            + "REMARKS,"
                            + "STATUS,"
                            + "USER_INSERTED,"
                            + "DATE_INSERTED,"
                            + "NON_WARRANTY_REMARKS,"
                            + "CUSTOMER_COMPLAIN,"
                            + "SERVICE_BIS_ID,"
                            + "REPAIR_STATUS,"
                            + "PAY_TYPE ";

                    String values = "?,?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                            + "?,?,?,?,?,?,1,?,SYSDATE,?,?,?,1,?";

                    PreparedStatement ps_in_wo = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDERS(" + column + ") VALUES(" + values + ")");

                    //////////////////////Checking IMEI Warranty Status///////////////////////////
                    int warranty = WorkOrderController.checkImeiWarranty(wo_info.getImeiNo());

                    ps_in_wo.setString(1, nxtNewWorkOrderId);
                    ps_in_wo.setString(2, wo_info.getCustomerName());
                    ps_in_wo.setString(3, wo_info.getCustomerAddress());
                    ps_in_wo.setString(4, wo_info.getWorkTelephoneNo());
                    ps_in_wo.setString(5, wo_info.getEmail());
                    ps_in_wo.setString(6, wo_info.getCustomerNic());
                    ps_in_wo.setString(7, wo_info.getImeiNo());
                    ps_in_wo.setString(8, wo_info.getProduct());
                    ps_in_wo.setString(9, wo_info.getBrand());
                    ps_in_wo.setInt(10, wo_info.getModleNo());
                    ps_in_wo.setString(11, wo_info.getAssessoriesRetained());
                    ps_in_wo.setString(12, wo_info.getDefectNo());
                    ps_in_wo.setString(13, wo_info.getRccReference());
                    ps_in_wo.setString(14, wo_info.getDateOfSale());
                    ps_in_wo.setString(15, wo_info.getProofOfPurches());
                    ps_in_wo.setInt(16, wo_info.getBisId());
                    ps_in_wo.setInt(17, 1);
                    ps_in_wo.setInt(18, warranty);
                    ps_in_wo.setInt(19, wo_info.getNonWarrentyVrifType());
                    ps_in_wo.setString(20, wo_info.getRemarks());
                    ps_in_wo.setString(21, user_id);
                    ps_in_wo.setString(22, wo_info.getNonWarrantyRemarks());
                    ps_in_wo.setString(23, wo_info.getCustomerComplain());
                    ps_in_wo.setInt(24, wo_info.getServiceBisId());
                    ps_in_wo.setInt(25, wo_info.getPaymentType());

                    ps_in_wo.executeUpdate();

                    int customer_reg = 0;
                    String con_code = null;
                    ResultSet rs_cus_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                            + "FROM SD_CUSTOMER_DETAILS "
                            + "WHERE CUSTOMER_NIC = '" + wo_info.getCustomerNic() + "'");

                    while (rs_cus_cnt.next()) {
                        customer_reg = rs_cus_cnt.getInt("CNT");
                    }
                    if (customer_reg > 0) {
                        logger.info("This user already registered..............");
                        try {
                            EmaiSendConfirmCode.sendHTMLMailWorkOrder(wo_info.getEmail(), wo_info.getCustomerName(), "", nxtNewWorkOrderId, wo_info.getRccReference(), String.valueOf(wo_info.getModleNo()), wo_info.getImeiNo(), warranty);
                        } catch (MessagingException ex) {
                            logger.info("Error in E-mail Sending........" + ex.getMessage());
                        }
                    } else {
                        String cus_nic = wo_info.getCustomerNic();
                        String d = cus_nic.substring(0, 9);
                        Random rand = new Random();
                        con_code = String.valueOf(rand.nextInt(Integer.parseInt(d)));

                        PreparedStatement ps_in_cus = dbCon.prepare(con, "INSERT INTO SD_CUSTOMER_DETAILS(CUSTOMER_NIC,"
                                + "CUSTOMER_NAME,"
                                + "ADDRESS,"
                                + "CONTACT_NO,"
                                + "EMAIL,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,"
                                + "CONFIRM_CODE) VALUES(?,?,?,?,?,1,?,SYSDATE,?)");
                        ps_in_cus.setString(1, wo_info.getCustomerNic());
                        ps_in_cus.setString(2, wo_info.getCustomerName());
                        ps_in_cus.setString(3, wo_info.getCustomerAddress());
                        ps_in_cus.setString(4, wo_info.getWorkTelephoneNo());
                        ps_in_cus.setString(5, wo_info.getEmail());
                        ps_in_cus.setString(6, user_id);
                        ps_in_cus.setString(7, con_code);
                        ps_in_cus.executeUpdate();

                        try {
                            logger.info("Confirmation Code -" + con_code);

                            EmaiSendConfirmCode.sendHTMLMailWorkOrder(wo_info.getEmail(), wo_info.getCustomerName(), con_code, nxtNewWorkOrderId, wo_info.getRccReference(), String.valueOf(wo_info.getModleNo()), wo_info.getImeiNo(), warranty);

                            System.out.println("Work orderrRRRRRRRrr" + nxtNewWorkOrderId);

                        } catch (MessagingException ex) {
                            logger.info("Error in E-mail Sending........" + ex.getMessage());
                        }

                    }

                    result = 1;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                logger.info("Error in addWorkOrder Mehtod  :" + ex.toString());
                result = 9;
            }

        } else {
            result = 3;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(returnRefNo);
            vw.setReturnMsg("Work Order Insert Successfully");
            logger.info("Work Order Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI no have an pending or in progress maintenance record");
            logger.info("This IMEI no have an pending or in progress maintenance record...........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Cannot connect to the database. Please contact the system admin");
            logger.info("Cannot connect to the database. Please contact the system admin");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Insert");
            logger.info("Error in Work Order Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addStockClearanceWorkOrder(WorkOrder wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWorkOrder Mehtod Call.......");
        int result = 0;
        String last_workorder_id = "WO000000";
        String returnRefNo = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int WOM_cnt = 0;
        try {

            ResultSet rs_wom_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDER_MAINTAIN "
                    + "WHERE REPAIR_STATUS IN (1,4,5,6,7,8) "
                    + "AND WORK_ORDER_NO IN (SELECT WORK_ORDER_NO FROM SD_WORK_ORDERS WHERE IMEI_NO  = '" + wo_info.getImeiNo() + "')");

            while (rs_wom_cnt.next()) {
                WOM_cnt = rs_wom_cnt.getInt("CNT");
            }
            if (WOM_cnt > 0) {
                result = 2;
            } else {

                ResultSet rs_wocnt = dbCon.search(con, "SELECT WORK_ORDER_NO "
                        + "FROM SD_WORK_ORDERS "
                        + "WHERE ROWNUM = 1 "
                        + "ORDER BY WORK_ORDER_NO DESC ");

                if (rs_wocnt.next()) {
                    last_workorder_id = rs_wocnt.getString(1);
                    logger.info("Last Work Order id   " + last_workorder_id);
                }

                last_workorder_id = last_workorder_id.split("WO")[1];
                int nxt_wo_num = Integer.parseInt(last_workorder_id) + 1;
                String nxtWorkOrderId = String.valueOf(nxt_wo_num);
                int nxtWorkOrderNo_Length = nxtWorkOrderId.length();

                String nxtNewWorkOrderId = "WO";
                if (nxtWorkOrderNo_Length < 6) {
                    for (int i = 0; i < (6 - nxtWorkOrderNo_Length); i++) {
                        nxtNewWorkOrderId += "0";
                    }
                }
                nxtNewWorkOrderId += nxtWorkOrderId;

                logger.info("Inserting................Work Order No = " + nxtNewWorkOrderId);

//              
                logger.info("Submitting to the SMS gateway..........");
                returnRefNo = nxtNewWorkOrderId;

                String column = "WORK_ORDER_NO,"
                        + "CUSTOMER_NAME,"
                        + "CUSTOMER_ADDRESS,"
                        + "TELEPHONE_NO,"
                        + "EMAIL,"
                        + "CUSTOMER_NIC,"
                        + "IMEI_NO,"
                        + "PRODUCT,"
                        + "BRAND,"
                        + "MODEL_NO,"
                        + "ACCESSORIES_RETAINED,"
                        + "DEFECTS,"
                        + "RCC_REFERENCE,"
                        + "DATE_OF_SALE,"
                        + "PROOF_OF_PURCHASE,"
                        + "BIS_ID,"
                        + "WORK_ORD_STATUS,"
                        + "WARRANTY_VRIF_TYPE,"
                        + "NON_WARRANTY_VRIF_TYPE,"
                        + "REMARKS,"
                        + "STATUS,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED,"
                        + "NON_WARRANTY_REMARKS,"
                        + "CUSTOMER_COMPLAIN,"
                        + "SERVICE_BIS_ID,"
                        + "REPAIR_STATUS,"
                        + "PAY_TYPE ";

                String values = "?,?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                        + "?,?,?,?,?,?,1,?,SYSDATE,?,?,?,1,?";

                PreparedStatement ps_in_wo = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDERS(" + column + ") VALUES(" + values + ")");

//////////////////////Checking IMEI Warranty Status///////////////////////////
                int warranty = WorkOrderController.checkImeiWarranty(wo_info.getImeiNo());

                ps_in_wo.setString(1, nxtNewWorkOrderId);
                ps_in_wo.setString(2, wo_info.getCustomerName());
                ps_in_wo.setString(3, wo_info.getCustomerAddress());
                ps_in_wo.setString(4, wo_info.getWorkTelephoneNo());
                ps_in_wo.setString(5, wo_info.getEmail());
                ps_in_wo.setString(6, wo_info.getCustomerNic());
                ps_in_wo.setString(7, wo_info.getImeiNo());
                ps_in_wo.setString(8, wo_info.getProduct());
                ps_in_wo.setString(9, wo_info.getBrand());
                ps_in_wo.setInt(10, wo_info.getModleNo());
                ps_in_wo.setString(11, wo_info.getAssessoriesRetained());
                ps_in_wo.setString(12, wo_info.getDefectNo());
                ps_in_wo.setString(13, wo_info.getRccReference());
                ps_in_wo.setString(14, wo_info.getDateOfSale());
                ps_in_wo.setString(15, wo_info.getProofOfPurches());
                ps_in_wo.setInt(16, wo_info.getBisId());
                ps_in_wo.setInt(17, 1);
                // ps_in_wo.setInt(18, warranty);

                System.out.println("warrentyVrifTypeEEEE " + wo_info.getWarrentyVrifType());

                ps_in_wo.setInt(18, wo_info.getWarrentyVrifType());
                ps_in_wo.setInt(19, wo_info.getNonWarrentyVrifType());
                ps_in_wo.setString(20, wo_info.getRemarks());
                ps_in_wo.setString(21, user_id);
                ps_in_wo.setString(22, wo_info.getNonWarrantyRemarks());
                ps_in_wo.setString(23, wo_info.getCustomerComplain());
                ps_in_wo.setInt(24, wo_info.getServiceBisId());
                ps_in_wo.setInt(25, wo_info.getPaymentType());

                ps_in_wo.executeUpdate();

                int customer_reg = 0;
                String con_code = null;
                ResultSet rs_cus_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_CUSTOMER_DETAILS "
                        + "WHERE CUSTOMER_NIC = '" + wo_info.getCustomerNic() + "'");

                while (rs_cus_cnt.next()) {
                    customer_reg = rs_cus_cnt.getInt("CNT");
                }
                if (customer_reg > 0) {
                    logger.info("This user already registered..............");
                    try {
                        EmaiSendConfirmCode.sendHTMLMailWorkOrder(wo_info.getEmail(), wo_info.getCustomerName(), "", nxtNewWorkOrderId, wo_info.getRccReference(), String.valueOf(wo_info.getModleNo()), wo_info.getImeiNo(), warranty);
                    } catch (MessagingException ex) {
                        logger.info("Error in E-mail Sending........" + ex.getMessage());
                    }
                }

                result = 1;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in addWorkOrder Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(returnRefNo);
            vw.setReturnMsg("Work Order Insert Successfully");
            logger.info("Work Order Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI no have an pending or in progress maintenance record");
            logger.info("This IMEI no have an pending or in progress maintenance record...........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Insert");
            logger.info("Error in Work Order Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editWorkOrder(WorkOrder wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editWorkOrder Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int warrantyStatus = 1;
        try {

            PreparedStatement ps_up_workorer = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                    + "SET CUSTOMER_NAME = ?,"
                    + "CUSTOMER_ADDRESS = ?,"
                    + "TELEPHONE_NO = ?,"
                    + "EMAIL = ?,"
                    + "CUSTOMER_NIC = ?,"
                    + "IMEI_NO = ?,"
                    + "PRODUCT = ?,"
                    + "BRAND = ?,"
                    + "MODEL_NO=?,"
                    + "ACCESSORIES_RETAINED=?,"
                    + "DEFECTS=?,"
                    + "RCC_REFERENCE=?,"
                    + "DATE_OF_SALE=TO_DATE(?,'YYYY/MM/DD'),"
                    + "BIS_ID = ?,"
                    + "WORK_ORD_STATUS=1,"
                    + "WARRANTY_VRIF_TYPE=?,"
                    + "NON_WARRANTY_VRIF_TYPE= ?,"
                    + "REMARKS=?,"
                    + "STATUS=1,"
                    + "USER_MODIFIED=?,"
                    + "DATE_MODIFIED=SYSDATE,"
                    + "NON_WARRANTY_REMARKS = ?,"
                    + "CUSTOMER_COMPLAIN = ?,"
                    + "SERVICE_BIS_ID = ?,"
                    + "REPAIR_STATUS = 1, PAY_TYPE = ?"
                    + " WHERE WORK_ORDER_NO = ?");

            warrantyStatus = WorkOrderController.checkImeiWarranty(wo_info.getImeiNo());
            if (warrantyStatus == 0) {
                warrantyStatus = 1;
            }

            ps_up_workorer.setString(1, wo_info.getCustomerName());
            ps_up_workorer.setString(2, wo_info.getCustomerAddress());
            ps_up_workorer.setString(3, wo_info.getWorkTelephoneNo());
            ps_up_workorer.setString(4, wo_info.getEmail());
            ps_up_workorer.setString(5, wo_info.getCustomerNic());
            ps_up_workorer.setString(6, wo_info.getImeiNo());
            ps_up_workorer.setString(7, wo_info.getProduct());
            ps_up_workorer.setString(8, wo_info.getBrand());
            ps_up_workorer.setInt(9, wo_info.getModleNo());
            ps_up_workorer.setString(10, wo_info.getAssessoriesRetained());
            ps_up_workorer.setString(11, wo_info.getDefectNo());
            ps_up_workorer.setString(12, wo_info.getRccReference());
            ps_up_workorer.setString(13, wo_info.getDateOfSale());
            ps_up_workorer.setInt(14, wo_info.getBisId());
//            if (wo_info.getExpireStatus() == 0) {
//                ps_up_workorer.setInt(15, 1);
//            } else {
//                ps_up_workorer.setInt(15, 2);
//            }
//            ps_up_workorer.setInt(15, wo_info.getExpireStatus());
            //ps_up_workorer.setInt(15, warrantyStatus);

            ps_up_workorer.setInt(15, wo_info.getWarrentyVrifType());
//            ps_up_workorer.setInt(15, wo_info.getExpireStatus());

            ps_up_workorer.setInt(16, wo_info.getNonWarrentyVrifType());

            ps_up_workorer.setString(17, wo_info.getRemarks());
            ps_up_workorer.setString(18, user_id);
            ps_up_workorer.setString(19, wo_info.getNonWarrantyRemarks());
            ps_up_workorer.setString(20, wo_info.getCustomerComplain());
            ps_up_workorer.setInt(21, wo_info.getServiceBisId());
            ps_up_workorer.setInt(22, wo_info.getPaymentType());
            ps_up_workorer.setString(23, wo_info.getWorkOrderNo());
            ps_up_workorer.executeUpdate();

            //////////////Update Warranty Status in Devicer Master////////////////
            String updateWarrantyQry = " UPDATE SD_DEVICE_MASTER "
                    + "  SET WARRANTY_STATUS = ?    "
                    + "  WHERE IMEI_NO = ?  ";

            PreparedStatement ps_updateWarranty = dbCon.prepare(con, updateWarrantyQry);
            if (wo_info.getExpireStatus() == 0) {
                ps_updateWarranty.setInt(1, 1);
            } else {
                ps_updateWarranty.setInt(1, 2);
            }

            ps_updateWarranty.setString(2, wo_info.getImeiNo());
            ps_updateWarranty.executeUpdate();

            result = 1;

            //sms function
            SmsController sms = new SmsController();

            String message = "Dear Customer, \n"
                    + "Your device received to the service center on " + wo_info.getDeleveryDate() + " REF NO: " + wo_info.getWorkOrderNo() + ". Repair progress will be informed soon.\n"
                    + "Tel: 0115400400. T&C apply";

            if (wo_info.getWorkTelephoneNo() != "") {
                sms.send(wo_info.getWorkTelephoneNo(), message);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in editWorkOrder Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Update Successfully");
            logger.info("WorkOrder Update Succesfully............");

        } else if (con == null) {
            vw.setReturnFlag("1000000");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Update");
            logger.info("Error in Work Order Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getQuickWorkOrder(String workorderno,
            String imeino,
            String createdate,
            String fromdate,
            String todate,
            String nic,
            String order,
            String type,
            int bisId,
            int start,
            int limit) {

        logger.info("getQuicWorkOrder Method Call....................");
        ArrayList<QuickWorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);

            String workOrderNo = (workorderno.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorderno + "%')");
            String imeNo = (imeino.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String createDate = (createdate.equalsIgnoreCase("all") ? "NVL(W.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD'))" : "W.DATE_INSERTED = TO_DATE('" + createdate + "','YYYY/MM/DD')");
            String NicNo = (nic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + nic + "%')");
            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY WORK_ORDER_NO DESC" : "ORDER BY W." + order + "   " + type + "");

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_NIC,"
                    + "W.MODEL_NO,"
                    + "M.MODEL_DESCRIPTION,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.STATUS,"
                    + "W.DEFECTS,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL, "
                    + "W.WORK_ORD_STATUS, "
                    + "W.REMARKS, "
                    + "W.BIS_ID SHOP_ID, "
                    + "C.BIS_NAME SHOP_NAME,"
                    + "C.ADDRESS SHOP_ADDRESS,"
                    + "W.SERVICE_BIS_ID SERVICE_AGENT_ID, "
                    + "B.BIS_NAME SERVICE_AGENT_NAME,"
                    + "B.ADDRESS SERVICE_AGENT_ADDRESS,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') DATE_INSERTED, W.BIS_ID";

            String whereClous = " W.MODEL_NO = M.MODEL_NO(+)  "
                    + "AND  W.SERVICE_BIS_ID = B.BIS_ID (+)"
                    + "AND  W.BIS_ID = C.BIS_ID (+)"
                    + "AND " + workOrderNo + " "
                    + "AND " + imeNo + " "
                    + "AND " + createDate + " "
                    + "AND " + NicNo + " "
                    + " " + Range + " "
                    + " AND W.WORK_ORD_STATUS = 2 "
                    + "AND W.BIS_ID =" + bisId
                    + " AND W.STATUS != 9";

            String tables = "SD_WORK_ORDERS W,SD_MODEL_LISTS M,SD_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES C";

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println("ABCCCCCCCCCC " + qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                QuickWorkOrder wo = new QuickWorkOrder();
                totalrecode = rs.getInt("CNT");
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setRepairStatus(rs.getInt("STATUS"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setRemarks(rs.getString("Remarks"));
                wo.setBisAddress(rs.getString("SHOP_ADDRESS"));
                wo.setBisName(rs.getString("SHOP_NAME"));
                wo.setServiceCenter(rs.getString("SERVICE_AGENT_NAME"));
                wo.setServiceCenterId(rs.getInt("SERVICE_AGENT_ID"));
                wo.setServiceCenterAddress(rs.getString("SERVICE_AGENT_ADDRESS"));
                wo.setStatus(rs.getInt("WORK_ORD_STATUS"));

                if (rs.getInt("STATUS") == 1) {
                    wo.setRepairStatusMsq("Open");
                } else if (rs.getInt("STATUS") == 2) {
                    wo.setRepairStatusMsq("In Progress");
                } else if (rs.getInt("STATUS") == 3) {
                    wo.setRepairStatusMsq("Complete");
                } else {
                    wo.setRepairStatusMsq("None");
                }
                wo.setCreateDate(rs.getString("DATE_INSERTED"));
                wo.setDeleveryDate(rs.getString("DATE_INSERTED"));
                wo.setBisId(rs.getInt("BIS_ID"));
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getQuicWorkOrder Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper addQuickWorkOrder(QuickWorkOrder wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addQuickWorkOrder Mehtod Call.......");
        int result = 0;
        String last_workorder_id = "WO000000";
        String nxtNewWorkOrderId = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int imei_master_cnt = 0;
        //   int imei_waranty_cnt = 0;
        try {

            ResultSet rs_master_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + wo_info.getImeiNo() + "'");

            while (rs_master_cnt.next()) {
                imei_master_cnt = rs_master_cnt.getInt("CNT");
            }
            // if (imei_master_cnt > 0) {
//            ResultSet rs_waranty_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
//                    + "FROM SD_WARRENTY_REGISTRATION "
//                    + "WHERE IMEI_NO = '" + wo_info.getImeiNo() + "'");
//
//            while (rs_waranty_cnt.next()) {
//                imei_waranty_cnt = rs_waranty_cnt.getInt("CNT");
//
//            }
            // if (imei_waranty_cnt > 0) {
            //////////////////Check Are their not opened work orders (still as a quick work order) for this imei
            String chkImeiWorkOrderQry = " SELECT COUNT(*) AS CNT "
                    + " FROM SD_WORK_ORDERS  "
                    + " WHERE WORK_ORD_STATUS=2 "
                    + " AND IMEI_NO='" + wo_info.getImeiNo() + "'   ";

            ResultSet rs_chkQkWo = dbCon.search(con, chkImeiWorkOrderQry);
            int qickWO = 0;
            if (rs_chkQkWo.next()) {
                qickWO = rs_chkQkWo.getInt("CNT");
            }

            System.out.println("Number of Quick Work Orders which are not Opened : " + qickWO);

            if (qickWO == 0) {

                if (imei_master_cnt > 0) {

                    //////////////////////Checking IMEI Warranty Status///////////////////////////
                    int warranty = WorkOrderController.checkImeiWarranty(wo_info.getImeiNo());

                    int email_send_flag = 0;

                    ResultSet rs_wocnt = dbCon.search(con, "SELECT WORK_ORDER_NO "
                            + "FROM SD_WORK_ORDERS "
                            + "WHERE ROWNUM = 1 "
                            + "ORDER BY WORK_ORDER_NO DESC ");

                    if (rs_wocnt.next()) {
                        last_workorder_id = rs_wocnt.getString(1);
                        logger.info("Last Work Order id   " + last_workorder_id);
                    }

                    last_workorder_id = last_workorder_id.split("WO")[1];
                    int nxt_wo_num = Integer.parseInt(last_workorder_id) + 1;
                    String nxtWorkOrderId = String.valueOf(nxt_wo_num);
                    int nxtWorkOrderNo_Length = nxtWorkOrderId.length();

                    nxtNewWorkOrderId = "WO";
                    if (nxtWorkOrderNo_Length < 6) {
                        for (int i = 0; i < (6 - nxtWorkOrderNo_Length); i++) {
                            nxtNewWorkOrderId += "0";
                        }
                    }
                    nxtNewWorkOrderId += nxtWorkOrderId;

                    logger.info("Inserting................Work Order No = " + nxtNewWorkOrderId);

                    String barnd = null;
                    String product = null;

                    ResultSet rs_getBarnd = dbCon.search(con, "SELECT BRAND,PRODUCT "
                            + "FROM SD_DEVICE_MASTER "
                            + "WHERE IMEI_NO = '" + wo_info.getImeiNo() + "'");
                    while (rs_getBarnd.next()) {
                        barnd = rs_getBarnd.getString("BRAND");
                        product = rs_getBarnd.getString("PRODUCT");
                    }

                    String column = "WORK_ORDER_NO,CUSTOMER_NAME,TELEPHONE_NO,"
                            + "EMAIL,CUSTOMER_NIC,"
                            + "IMEI_NO,PRODUCT,BRAND,MODEL_NO,DEFECTS,BIS_ID,"
                            + "REMARKS,ACCESSORIES_RETAINED,"
                            + "STATUS,USER_INSERTED,DATE_INSERTED,WORK_ORD_STATUS,SERVICE_BIS_ID,WARRANTY_VRIF_TYPE,REPAIR_STATUS,PAY_TYPE ";

                    String values = "?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,SYSDATE,2,?,?,1,1";

                    if (product == null) {
                        product = wo_info.getProduct();
                    }
                    PreparedStatement ps_in_wo = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDERS(" + column + ") VALUES(" + values + ")");
                    ps_in_wo.setString(1, nxtNewWorkOrderId);
                    ps_in_wo.setString(2, wo_info.getCustomerName());
                    ps_in_wo.setString(3, wo_info.getWorkTelephoneNo());
                    ps_in_wo.setString(4, wo_info.getEmail());
                    ps_in_wo.setString(5, wo_info.getCustomerNic());
                    ps_in_wo.setString(6, wo_info.getImeiNo());
                    ps_in_wo.setString(7, product);
                    ps_in_wo.setString(8, barnd);
                    ps_in_wo.setInt(9, wo_info.getModleNo());
                    ps_in_wo.setString(10, wo_info.getDefectNo());
                    ps_in_wo.setInt(11, wo_info.getBisId());
                    ps_in_wo.setString(12, wo_info.getRemarks());
                    ps_in_wo.setString(13, wo_info.getAssessoriesRetained());
                    ps_in_wo.setString(14, user_id);
                    ps_in_wo.setInt(15, wo_info.getServiceCenterId());
                    ps_in_wo.setInt(16, warranty);
                    ps_in_wo.executeUpdate();

                    int customer_reg = 0;
                    String con_code = null;

                    ResultSet rs_cus_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                            + "FROM SD_CUSTOMER_DETAILS "
                            + "WHERE CUSTOMER_NIC = '" + wo_info.getCustomerNic() + "'");

                    while (rs_cus_cnt.next()) {
                        customer_reg = rs_cus_cnt.getInt("CNT");
                    }

                    if (customer_reg > 0) {
                        logger.info("This user already registered..............");
                    } else {
                        String cus_nic = wo_info.getCustomerNic();
                        String d = cus_nic.substring(0, 9);
                        Random rand = new Random();
                        con_code = String.valueOf(rand.nextInt(Integer.parseInt(d)));

                        PreparedStatement ps_in_cus = dbCon.prepare(con, "INSERT INTO SD_CUSTOMER_DETAILS(CUSTOMER_NIC,"
                                + "CUSTOMER_NAME,"
                                + "CONTACT_NO,"
                                + "EMAIL,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,"
                                + "CONFIRM_CODE) VALUES(?,?,?,?,1,?,SYSDATE,?)");
                        ps_in_cus.setString(1, wo_info.getCustomerNic());
                        ps_in_cus.setString(2, wo_info.getCustomerName());
                        ps_in_cus.setString(3, wo_info.getWorkTelephoneNo());
                        ps_in_cus.setString(4, wo_info.getEmail());
                        ps_in_cus.setString(5, user_id);
                        ps_in_cus.setString(6, con_code);
                        ps_in_cus.executeUpdate();

                        //                    try {
                        //                        logger.info("Confirmation Code -" + con_code);
                        //                        email_send_flag = 1;
                        //                        EmaiSendConfirmCode.sendHTMLMailWorkOrder(wo_info.getEmail(), wo_info.getCustomerName(), con_code, nxtNewWorkOrderId, "", String.valueOf(wo_info.getModleNo()), wo_info.getImeiNo(), warranty);
                        //                    } catch (MessagingException ex) {
                        //                        logger.info("Error in E-mail Sending........" + ex.getMessage());
                        //                    }
                    }

                    //                if (email_send_flag == 0) {
                    //                    try {
                    //                        EmaiSendConfirmCode.sendHTMLMailWorkOrder(wo_info.getEmail(), wo_info.getCustomerName(), "", nxtNewWorkOrderId, "", String.valueOf(wo_info.getModleNo()), wo_info.getImeiNo(), warranty);
                    //                    } catch (MessagingException ex) {
                    //                        logger.info("Error in E-mail Sending........" + ex.getMessage());
                    //                    }
                    //                }
                    result = 1;

                    //############## sms function
                    String message = "Dear Customer, \n"
                            + "Your service REF NO: " + nxtNewWorkOrderId + ". Repair progress will be informed soon.\n"
                            + "Tel: 0115400400. T&C apply";

                    SmsController sms = new SmsController();

                    sms.send(wo_info.getWorkTelephoneNo(), message);

                } else {
                    result = 2;
                }

            } else {
                result = 4;
            }
            // } else {
            //     result = 3;
            // }
            // } else {
            //    result = 2;
            // }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in addWorkOrder Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");

            vw.setReturnMsg("Work Order Insert Successfully ");
            vw.setReturnRefNo(nxtNewWorkOrderId);
            logger.info("WorkOrder Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMIE Number in Not in Master Record");
            logger.info("This IMIE Number in Not in Master Record............");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This IMEI Number have't Warranty Register Record");
            logger.info("This IMEI Number have't Warranty Register Record...........");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("This IMEI Number already has a not opened Quick Work Order.");
            logger.info("This IMEI Number have't Warranty Register Record...........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Insert");
            logger.info("Error in Work Order Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editQuickWorkOrder(QuickWorkOrder qwo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editQuickWorkOrder Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            int workOrderStatus = 2;
            ResultSet chkWorkOrderStatus = dbCon.search(con, "SELECT WORK_ORD_STATUS "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE WORK_ORDER_NO = '" + qwo_info.getWorkOrderNo() + "'");

            while (chkWorkOrderStatus.next()) {
                workOrderStatus = chkWorkOrderStatus.getInt("WORK_ORD_STATUS");
            }

            result = 2;

            if (workOrderStatus == 2) {
                PreparedStatement ps_up_workorer = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                        + "SET CUSTOMER_NAME = ?,"
                        + "TELEPHONE_NO = ?,"
                        + "EMAIL = ?,"
                        + "CUSTOMER_NIC = ?,"
                        + "IMEI_NO = ?,"
                        + "PRODUCT = ?,"
                        + "MODEL_NO=?,"
                        + "DEFECTS=?,"
                        + "BIS_ID=?,"
                        + "REMARKS = ?,"
                        + "SERVICE_BIS_ID=?,"
                        + "ACCESSORIES_RETAINED=?,"
                        // + "STATUS= ?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + " WHERE WORK_ORDER_NO = ?");

//                int newBisId = Integer.parseInt(qwo_info.getBisName());
                ps_up_workorer.setString(1, qwo_info.getCustomerName());
                ps_up_workorer.setString(2, qwo_info.getWorkTelephoneNo());
                ps_up_workorer.setString(3, qwo_info.getEmail());
                ps_up_workorer.setString(4, qwo_info.getCustomerNic());
                ps_up_workorer.setString(5, qwo_info.getImeiNo());
                ps_up_workorer.setString(6, qwo_info.getProduct());
                ps_up_workorer.setInt(7, qwo_info.getModleNo());
                ps_up_workorer.setString(8, qwo_info.getDefectNo());
                ps_up_workorer.setInt(9, qwo_info.getBisId());
                //ps_up_workorer.setInt(9, qwo_info.getTransferLocation());
                ps_up_workorer.setString(10, qwo_info.getRemarks());
                ps_up_workorer.setInt(11, qwo_info.getServiceCenterId());
                // ps_up_workorer.setInt(11, newBisId);
                ps_up_workorer.setString(12, qwo_info.getAssessoriesRetained());
                ps_up_workorer.setString(13, user_id);
                ps_up_workorer.setString(14, qwo_info.getWorkOrderNo());
                ps_up_workorer.executeUpdate();

                result = 1;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in editQuickWorkOrder Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Quick Work Order is Successfully updated.");
            logger.info("WorkOrder Update Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Quick Work Order is Opened. You cannot cancel it now.");
            logger.info("WorkOrder Update Succesfully............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Quick Work Order Update");
            logger.info("Error in Work Order Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper removeQuickWorkOrder(QuickWorkOrder qwo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("removeQuickWorkOrder Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            int workOrderStatus = 2;
            ResultSet chkWorkOrderStatus = dbCon.search(con, "SELECT WORK_ORD_STATUS "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE WORK_ORDER_NO = '" + qwo_info.getWorkOrderNo() + "'");

            while (chkWorkOrderStatus.next()) {
                workOrderStatus = chkWorkOrderStatus.getInt("WORK_ORD_STATUS");
            }

            result = 2;

            if (workOrderStatus == 2) {
                PreparedStatement ps_up_workorer = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                        + "SET STATUS= 9,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + " WHERE WORK_ORDER_NO = ?");

                ps_up_workorer.setString(1, user_id);
                ps_up_workorer.setString(2, qwo_info.getWorkOrderNo());
                ps_up_workorer.executeUpdate();

                result = 1;
            }

        } catch (Exception ex) {
            logger.info("Error in removeQuickWorkOrder Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Quick Work Order is Removed");
            logger.info("Quick Work Order is Removed............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Quick Work Order is Opened. You cannot Remove It Now.");
            logger.info("Quick Work Order is Opened. You cannot Remove It Now...........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Quick Work Order Remove");
            logger.info("Error in Quick Work Order Remove............");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getWorkOrderDetails(String workorderno) {

        logger.info("getWorkOrderDetails Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "W.WORK_ORDER_NO,W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,W.TELEPHONE_NO,"
                    + "W.EMAIL,W.CUSTOMER_NIC,W.IMEI_NO,"
                    + "W.PRODUCT,W.BRAND,W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,W.DEFECTS,"
                    + "W.RCC_REFERENCE,TO_CHAR(W.DATE_OF_SALE) DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,W.BIS_ID,"
                    + "W.SCHEME_ID,W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,W.LEVEL_ID,"
                    + "W.REMARKS,W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,TO_CHAR(W.DELIVERY_DATE) DELIVERY_DATE,"
                    + "W.GATE_PASS,W.STATUS,"
                    + "TRUNC(SYSDATE - W.DATE_INSERTED) DELAY_DATE,"
                    + "TO_CHAR(W.DATE_INSERTED) DATE_INSERTED,"
                    + "NON_WARRANTY_REMARKS,"
                    + "CUSTOMER_COMPLAIN,"
                    + "W.SERVICE_BIS_ID, "
                    + "B1.BIS_NAME  ";
            //  + "TO_CHAR(R.REGISTER_DATE, 'YYYY/MM/DD') AS REGISTER_DATE ";

//            String whereClous = " W.IMEI_NO=R.IMEI_NO   "
//                    + " AND     WORK_ORDER_NO = '" + workorderno + "'";
            String whereClous = " WORK_ORDER_NO = '" + workorderno + "' "
                    + "         AND W.SERVICE_BIS_ID = B1.BIS_ID (+)    ";

//            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM SD_WORK_ORDERS W, SD_WARRENTY_REGISTRATION R "
//                    + "WHERE " + whereClous + " ");
            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM SD_WORK_ORDERS W, SD_BUSINESS_STRUCTURES B1 "
                    + "WHERE " + whereClous + " ");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                totalrecode = 1;
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setBrand(rs.getString("BRAND"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setRccReference(rs.getString("RCC_REFERENCE"));
                //wo.setDateOfSale(rs.getString("REGISTER_DATE"));
                wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                wo.setBisId(rs.getInt("BIS_ID"));
                wo.setSchemeId(rs.getInt("SCHEME_ID"));
                wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wo.setTecnician(rs.getString("TECHNICIAN"));
                wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                wo.setLevelId(rs.getInt("LEVEL_ID"));
                wo.setRemarks(rs.getString("REMARKS"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setCourierNo(rs.getString("COURIER_NO"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setGatePass(rs.getString("GATE_PASS"));
                wo.setStatus(rs.getInt("STATUS"));
                wo.setDelayDate(rs.getString("DELAY_DATE"));
                wo.setRepairDate(rs.getString("DATE_INSERTED"));
                wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                wo.setServiceBisDesc(rs.getString("BIS_NAME"));
                wo.setRegisterDate(rs.getString("DATE_OF_SALE"));

                //////////////////////Checking IMEI Warranty Status///////////////////////////
                int warranty = WorkOrderController.checkImeiWarranty(rs.getString("IMEI_NO"));
                wo.setExpireStatus(warranty);

                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderDetails Methd call.." + ex.toString());
            ex.printStackTrace();

        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getQuickWorkOrderOpenList() {

        logger.info("getQuickWorkOrderOpenList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT WORK_ORDER_NO FROM SD_WORK_ORDERS "
                    + "WHERE WORK_ORD_STATUS = 2 ");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getQuickWorkOrderOpenList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = workOrderList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper deleteWorkOrder(WorkOrder wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("deleteWorkOrder Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            /////////////////////Chech are there any maintain records pending/////////////////////////
            int closeWOCnt = 0;
            int chkMaintains = 0;

            String chkMaintainRecs = " SELECT COUNT(*) AS CNT "
                    + " FROM SD_WORK_ORDER_MAINTAIN "
                    + " WHERE WORK_ORDER_NO='" + wo_info.getWorkOrderNo() + "'    ";

            ResultSet rs1 = dbCon.search(con, chkMaintainRecs);

            if (rs1.next()) {
                chkMaintains = rs1.getInt("CNT");
            }

            if (chkMaintains > 0) {

                String chkPendingWOs = " SELECT COUNT(*) AS CNT "
                        + " FROM SD_WORK_ORDER_MAINTAIN "
                        + " WHERE REPAIR_STATUS NOT IN (1, 2, 3)    "
                        + " AND WORK_ORDER_NO='" + wo_info.getWorkOrderNo() + "'    ";

                ResultSet rs = dbCon.search(con, chkPendingWOs);

                if (rs.next()) {
                    closeWOCnt = rs.getInt("CNT");
                }

            }

            int wo_close = 0;
            ResultSet rs_wo_close = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE STATUS = 3 AND WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "'");

            if (rs_wo_close.next()) {
                wo_close = rs_wo_close.getInt("CNT");
            }

            if (wo_close > 0) {
                result = 3;

            } else {

                String update = "UPDATE SD_WORK_ORDERS "
                        + "SET STATUS=9,"
                        + "USER_MODIFIED='" + user_id + "',"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE WORK_ORDER_NO ='" + wo_info.getWorkOrderNo() + "'";

                try {
                    if (closeWOCnt == 0) {
                        dbCon.save(con, update);
                        result = 1;
                    } else {
                        result = 2;
                    }

                } catch (Exception e) {
                    logger.info("Error in deleteWorkOrder ......");
                    result = 9;
                }
            }
        } catch (Exception ex) {
            logger.info("Error in deleteWorkOrder Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Delete Successfully");
            logger.info("Work Order Delete Successfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Sorry cannot delete. Item Maintainance is not completed.");
            logger.info("Work Order Delete Successfully............");

        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Sorry cannot delete. This Work Order allready Closed.");
            logger.info("Sorry cannot delete. This Work Order allready Closed.");

        } else if (con == null) {
            vw.setReturnFlag("1000000");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Delete");
            logger.info("Error in Work Order Delete................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getQuickWorkOrderDefectList() {

        logger.info("getQuickWorkOrderDefectList Method Call....................");
        ArrayList<QuickWorkDefect> defectList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "MAJ_CODE,MAJ_CODE_DESC";

            String whereClous = "STATUS =1";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM SD_DEFECT_CODES "
                    + "WHERE " + whereClous + " ");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                QuickWorkDefect de = new QuickWorkDefect();
                totalrecode = 1;
                de.setDefectCode(rs.getInt("MAJ_CODE"));
                de.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                defectList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error getQuickWorkOrderDefectList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(defectList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderCollectorDetail(String workOrderNo,
            String order,
            int imeiNo,
            String product,
            String modleDesc,
            String customerName,
            String customerNic,
            String workTelephoneNo,
            String email,
            String defectNo,
            String assessoriesRetained,
            int podNo,
            String collectorName,
            String collectorNic,
            String contactNo,
            int start,
            int limit) {

        logger.info("getWorkOrderCollectorDetail Collector Method Call....................");
        ArrayList<QuickWorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrder(order);
            String workOrderNoQry = (workOrderNo.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workOrderNo + "%')");
            String imeNoQry = ((imeiNo == 0) ? "NVL(W.IMEI_NO,0) = NVL(W.IMEI_NO,0)" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imeiNo + "%')");
            String productQry = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String modleDescQry = (modleDesc.equalsIgnoreCase("all") ? "NVL(m.model_description,'AA') = NVL(m.model_description,'AA')" : "UPPER(m.model_description) LIKE UPPER('%" + modleDesc + "%')");
            String customerNameQry = (customerName.equalsIgnoreCase("all") ? "NVL(W.customer_name,'AA') = NVL(W.customer_name,'AA')" : "UPPER(W.customer_name) LIKE UPPER('%" + customerName + "%')");
            String customerNicQry = (customerNic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customerNic + "%')");
            String workTelephoneNoQry = (workTelephoneNo.equalsIgnoreCase("all") ? "NVL(W.telephone_no,'AA') = NVL(W.telephone_no ,'AA')" : "UPPER(W.telephone_no ) LIKE UPPER('%" + workTelephoneNo + "%')");
            String emailQry = (email.equalsIgnoreCase("all") ? "NVL(W.email,'AA') = NVL(W.email,'AA')" : "UPPER(W.email ) LIKE UPPER('%" + email + "%')");
            String defectNoQry = (defectNo.equalsIgnoreCase("all") ? "NVL(W.defects,'AA') = NVL(W.defects,'AA')" : "UPPER(W.defects ) LIKE UPPER('%" + defectNo + "%')");
            String assessoriesRetainedQry = (assessoriesRetained.equalsIgnoreCase("all") ? "NVL(W.accessories_retained,'AA') = NVL(W.accessories_retained,'AA')" : "UPPER(W.accessories_retained ) LIKE UPPER('%" + assessoriesRetained + "%')");
            String podNoQry = ((podNo == 0) ? "NVL(c.pod_no,0) = NVL(c.pod_no, 0)" : "UPPER(c.pod_no) LIKE UPPER('%" + podNo + "%')");
            String collectorNameQry = (collectorName.equalsIgnoreCase("all") ? "NVL(c.collector_name,'AA') = NVL(c.collector_name,'AA')" : "UPPER(c.collector_name) LIKE UPPER('%" + collectorName + "%')");
            String collectorNicQry = (collectorNic.equalsIgnoreCase("all") ? "NVL(c.collector_nic,'AA') = NVL(c.collector_nic,'AA')" : "UPPER(c.collector_nic) LIKE UPPER('%" + collectorNic + "%')");
            String contactNoQry = (contactNo.equalsIgnoreCase("all") ? "NVL(c.telephone_no,'AA') = NVL(c.telephone_no,'AA')" : "UPPER(c.telephone_no) LIKE UPPER('%" + contactNo + "%')");

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY WORK_ORDER_NO DESC" : "ORDER BY W." + order + "   ");

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_NIC,"
                    + "W.MODEL_NO,"
                    + "M.MODEL_DESCRIPTION,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.STATUS,"
                    + "W.DEFECTS,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.TELEPHONE_NO AS WTEL,"
                    + "W.EMAIL, "
                    + "W.WORK_ORD_STATUS, "
                    + "W.REMARKS, "
                    + "B.BIS_ID SERVICE_CENTER_ID, "
                    + "B.BIS_NAME SERVICE_CENTER,"
                    + "B.ADDRESS SERVICE_CENTER_ADDRESS,"
                    + "D.BIS_ID SHOP_ID, "
                    + "D.BIS_NAME SHOP_NAME,"
                    + "D.ADDRESS SHOP_ADDRESS,"
                    + "C.COLLECTOR_NIC,"
                    + "C.POD_NO,"
                    + "C.COLLECTOR_NAME,"
                    + "C.TELEPHONE_NO AS CTEL,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') DATE_INSERTED, W.BIS_ID";

            String whereClous = " W.MODEL_NO = M.MODEL_NO(+)  "
                    + "AND  W.SERVICE_BIS_ID = B.BIS_ID (+)"
                    + "AND  W.BIS_ID = D.BIS_ID (+)"
                    + "AND  W.WORK_ORDER_NO = C.WORK_ORD_NO (+)"
                    + "AND " + workOrderNoQry + " "
                    + "AND " + imeNoQry + " "
                    + "AND " + productQry + " "
                    + "AND " + modleDescQry + " "
                    + "AND " + modleDescQry + " "
                    + "AND " + customerNicQry + " "
                    + "AND " + workTelephoneNoQry + " "
                    + "AND " + emailQry + " "
                    + "AND " + defectNoQry + " "
                    + "AND " + assessoriesRetainedQry + " "
                    + "AND " + podNoQry + " "
                    + "AND " + collectorNameQry + " "
                    + "AND " + collectorNicQry + " "
                    + "AND " + contactNoQry + " "
                    + "AND " + customerNameQry + " "
                    + " AND W.WORK_ORD_STATUS = 2 ";
            //  + " AND W.STATUS != 9";

            String tables = "SD_WORK_ORDERS W,SD_MODEL_LISTS M,SD_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES D,SD_COLLECTOR_DETAILS C";

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            System.out.println(qry);

            ResultSet rs = dbCon.search(con, qry);
            logger.info("====================================================");
            logger.info(qry);
            logger.info("******************************************************");
            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                QuickWorkOrder wo = new QuickWorkOrder();
                totalrecode = rs.getInt("CNT");
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));

                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setRepairStatus(rs.getInt("STATUS"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setWorkTelephoneNo(rs.getString("WTEL"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setRemarks(rs.getString("Remarks"));
                wo.setCollectorNic(rs.getString("COLLECTOR_NIC"));
                wo.setPodNo(rs.getString("POD_NO"));
                wo.setCollectorName(rs.getString("COLLECTOR_NAME"));
                wo.setContactNo(rs.getString("CTEL"));
                wo.setServiceCenter(rs.getString("SERVICE_CENTER"));
                wo.setServiceCenterAddress(rs.getString("SERVICE_CENTER_ADDRESS"));
                wo.setStatus(rs.getInt("WORK_ORD_STATUS"));
                wo.setBisAddress(rs.getString("SHOP_ADDRESS"));
                wo.setBisName(rs.getString("SHOP_NAME"));
                if (rs.getInt("STATUS") == 1) {
                    wo.setRepairStatusMsq("Open");
                } else if (rs.getInt("STATUS") == 2) {
                    wo.setRepairStatusMsq("In Progress");
                } else if (rs.getInt("STATUS") == 3) {
                    wo.setRepairStatusMsq("Complete");
                } else {
                    wo.setRepairStatusMsq("None");
                }
                wo.setCreateDate(rs.getString("DATE_INSERTED"));
                wo.setDeleveryDate(rs.getString("DATE_INSERTED"));
                wo.setBisId(rs.getInt("BIS_ID"));
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderCollectorDetail Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values collect...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper editWorkOrderCollectorDetail(QuickWorkOrder qwo_info,
            String workOrderNo, String order, int start, int limit) {
        logger.info("Insert editWorkOrderCollectorDetail Mehtod Call.......");
        int result = 0;
        int lastCnt = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            System.out.println();
            String qry = "INSERT INTO "
                    + "SD_COLLECTOR_DETAILS ("
                    + "WORK_ORD_NO, "
                    + "POD_NO, "
                    + "COLLECTOR_NAME, "
                    + "TELEPHONE_NO , "
                    + "COLLECTOR_NIC) "
                    + "VALUES (?,?,?,?,?)";

            PreparedStatement pst = dbCon.prepare(con, qry);

            System.out.println("Insert WorkOrderCollectorDetail method try---------------------");

            pst.setString(1, qwo_info.getWorkOrderNo());
            pst.setString(2, qwo_info.getPodNo());
            pst.setString(3, qwo_info.getCollectorName());
            pst.setString(4, qwo_info.getContactNo());
            pst.setString(5, qwo_info.getCollectorNic());

            pst.executeQuery();
            System.out.println(qry);

            System.out.println("Insert WorkOrderCollectorDetail method try end---------------------");

            result = 1;
//            }

        } catch (Exception ex) {
//            ex.printStackTrace();
            logger.info("Update in editQuickWorkOrder Collectoe Mehtod  :" + ex.toString());
            try {

//                String qry = "UPDATE SD_COLLECTOR_DETAILS C "
//                        + "SET C.COLLECTOR_NIC = '"+qwo_info.getCollectorNic()+"',"
//                    + "C.POD_NO = '"+qwo_info.getPodNo()+"',"
//                    + "C.COLLECTOR_NAME = '"+qwo_info.getCollectorName()+"',"
//                    + "C.TELEPHONE_NO = '"+qwo_info.getContactNo()+"',"
////                    + "C.TELEPHONE_NO AS CTEL,"
//                        + " WHERE C.WORK_ORD_NO = '"+qwo_info.getWorkOrderNo()+"' ";
//                System.out.println("RRRRRRRRRRRRRRRRRrr"+ qry);
                String qryup = "UPDATE SD_COLLECTOR_DETAILS "
                        + "SET COLLECTOR_NIC = ?,"
                        + "POD_NO = ?,"
                        + "COLLECTOR_NAME = ?,"
                        + "TELEPHONE_NO = ?"
                        + " WHERE WORK_ORD_NO = ? ";

                PreparedStatement pstup = dbCon.prepare(con, qryup);

                System.out.println("editWorkOrderCollectorDetail method try update---------------------");

                pstup.setString(1, qwo_info.getCollectorNic());
                pstup.setString(2, qwo_info.getPodNo());
                pstup.setString(3, qwo_info.getCollectorName());
                pstup.setString(4, qwo_info.getContactNo());
                pstup.setString(5, qwo_info.getWorkOrderNo());
                pstup.executeUpdate();
                result = 2;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("editWorkOrderCollectorDetail inserted Succesfully.");
            logger.info("Work order collector details inserted succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Edit WorkOrder Collector Detail Updated Succesfully.");
            logger.info("Work order collector detail updated succesfully............");
        } else {
            //vw.setReturnFlag("9");
            //  vw.setReturnMsg("Error in Quick Work Order Update");
            logger.info("Error in work order collector detail Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getQuickWorkOrderServiceCenterList() {

        logger.info("getQuickWorkOrderServiceCenterList Method Call....................");
        ArrayList<QuickWorkOrderServiceCenter> userList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "BIS_ID,BIS_NAME,BIS_DESC";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE BIS_STRU_TYPE_ID = 8");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                QuickWorkOrderServiceCenter de = new QuickWorkOrderServiceCenter();
                totalrecode = 1;
                de.setBisId(rs.getInt("BIS_ID"));
                de.setFirstName(rs.getString("BIS_NAME"));
                de.setLastName(rs.getString("BIS_DESC"));
                userList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error getQuickWorkOrderServiceCenterList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderDetail(String workorderno) {

        logger.info("getWorkOrderDetail Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<WorkOrderMaintain> woMaintainList = new ArrayList<WorkOrderMaintain>();

            String seletColunmMaintain = "W.WRK_ORD_MTN_ID,"
                    + "W.WORK_ORDER_NO,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.REPAIR_LEVEL_ID,"
                    + "W.REPAIR_LEVEL_AMOUNT,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "TO_CHAR(W.EST_CLOSING_DATE,'YYYY/MM/DD') EST_CLOSING_DATE,"
                    + "W.INVOICE_NO";

            String whereClousMaintain = "W.WORK_ORDER_NO = '" + workorderno + "'";

            String tableMaintain = "SD_WORK_ORDER_MAINTAIN W";

            ResultSet rs_maintain = dbCon.search(con, "SELECT " + seletColunmMaintain + " FROM " + tableMaintain + " "
                    + "WHERE " + whereClousMaintain + " ");

            logger.info("Get Data to Resultset...");

            while (rs_maintain.next()) {
                WorkOrderMaintain wom = new WorkOrderMaintain();
                wom.setWorkorderMaintainId(rs_maintain.getInt("WRK_ORD_MTN_ID"));
                wom.setWorkOrderNo(rs_maintain.getString("WORK_ORDER_NO"));
                wom.setWarrantyVrifType(rs_maintain.getInt("WARRANTY_VRIF_TYPE"));
                wom.setNonWarrantyType(rs_maintain.getInt("NON_WARRANTY_VRIF_TYPE"));
                wom.setTechnician(rs_maintain.getString("TECHNICIAN"));
                wom.setActionTaken(rs_maintain.getString("ACTION_TAKEN"));
                wom.setRepairLevelId(rs_maintain.getInt("REPAIR_LEVEL_ID"));
                wom.setRepairAmount(rs_maintain.getDouble("REPAIR_LEVEL_AMOUNT"));
                wom.setRemarks(rs_maintain.getString("REMARKS"));
                wom.setRepairStatus(rs_maintain.getInt("REPAIR_STATUS"));
                wom.setEstimateClosingDate(rs_maintain.getString("EST_CLOSING_DATE"));
                wom.setInvoiceNo(rs_maintain.getString("INVOICE_NO"));
                woMaintainList.add(wom);
            }

            ArrayList<WorkOrderMaintainDetail> woMaintainDetail = new ArrayList<WorkOrderMaintainDetail>();

            String seletColunmMaintainDetail = "SEQ_NO,"
                    + "WORK_ORDER_NO,"
                    + "WRK_ORD_MTN_ID,"
                    + "SPARE_PART_NO,"
                    + "PART_DESCRIPTION,"
                    + "SELLING_PRICE,"
                    + "INVOICE_NO,"
                    + "THIRD_PARTY_FLAG,"
                    + "STATUS";

            String whereClousMaintainDetail = "WRK_ORD_MTN_ID IN (SELECT WRK_ORD_MTN_ID FROM SD_WORK_ORDER_MAINTAIN WHERE WORK_ORDER_NO = '" + workorderno + "')";

            String tableMaintainDetail = "SD_WORK_ORDER_MAINTAIN_DTL ";

            ResultSet rs_maintain_detail = dbCon.search(con, "SELECT " + seletColunmMaintainDetail + " FROM " + tableMaintainDetail + " "
                    + "WHERE " + whereClousMaintainDetail + " ");

            logger.info("Get Data to Resultset...");

            while (rs_maintain_detail.next()) {
                WorkOrderMaintainDetail womd = new WorkOrderMaintainDetail();
                womd.setSeqNo(rs_maintain_detail.getInt("SEQ_NO"));
                womd.setWrokOrderNo(rs_maintain_detail.getString("WORK_ORDER_NO"));
                womd.setWorkOrderMainId(rs_maintain_detail.getInt("WRK_ORD_MTN_ID"));
                womd.setPartNo(rs_maintain_detail.getInt("SPARE_PART_NO"));
                womd.setPartDesc(rs_maintain_detail.getString("PART_DESCRIPTION"));
                womd.setPartSellPrice(rs_maintain_detail.getDouble("SELLING_PRICE"));
                womd.setInvoiceNo(rs_maintain_detail.getString("INVOICE_NO"));
                womd.setThirdPartyFlag(rs_maintain_detail.getInt("THIRD_PARTY_FLAG"));
                womd.setStatus(rs_maintain_detail.getInt("STATUS"));
                woMaintainDetail.add(womd);
            }

            ArrayList<WorkOrderDefect> woMaintainDefect = new ArrayList<WorkOrderDefect>();

            String seletColunmMaintainDefect = "W.WRK_ORD_DEFT_ID,"
                    + "W.WRK_ORD_MTN_ID,"
                    + "W.MAJ_CODE,"
                    + "W.MIN_CODE,"
                    + "W.REMARKS,"
                    + "W.STATUS,"
                    + "D.MAJ_CODE_DESC,"
                    + "M.MIN_CODE_DESC";

            String whereClousMaintainDefect = "WRK_ORD_MTN_ID IN (SELECT WRK_ORD_MTN_ID FROM SD_WORK_ORDER_MAINTAIN WHERE WORK_ORDER_NO = '" + workorderno + "') "
                    + "AND W.MAJ_CODE = D.MAJ_CODE (+) "
                    + "AND W.MIN_CODE = M.MIN_CODE (+)";

            String tableMaintainDefect = "SD_WORK_ORDER_DEFECTS W,SD_DEFECT_CODES D,SD_DEFECT_MINOR_CODES M";

            ResultSet rs_maintain_defect = dbCon.search(con, "SELECT " + seletColunmMaintainDefect + " FROM " + tableMaintainDefect + " "
                    + "WHERE " + whereClousMaintainDefect + " ");

            logger.info("Get Data to Resultset...");

            while (rs_maintain_defect.next()) {
                WorkOrderDefect wod = new WorkOrderDefect();
                wod.setWoDefectId(rs_maintain_defect.getInt("WRK_ORD_DEFT_ID"));
                wod.setWoMaintainId(rs_maintain_defect.getInt("WRK_ORD_MTN_ID"));
                wod.setMajorCode(rs_maintain_defect.getInt("MAJ_CODE"));
                wod.setMinorCode(rs_maintain_defect.getInt("MIN_CODE"));
                wod.setRemarks(rs_maintain_defect.getString("REMARKS"));
                wod.setStatus(rs_maintain_defect.getInt("STATUS"));
                wod.setMajDesc(rs_maintain_defect.getString("MAJ_CODE_DESC"));
                wod.setMinDesc(rs_maintain_defect.getString("MIN_CODE_DESC"));
                woMaintainDefect.add(wod);
            }

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "W.SERVICE_BIS_ID,"
                    + "M.MODEL_DESCRIPTION,"
                    + "D.MAJ_CODE_DESC,"
                    + "L.LEVEL_NAME,"
                    + "B.BIS_NAME,"
                    + "S.DESCRIPTION,"
                    + "S2.DESCRIPTION REPAIR_MSG,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE";

            String whereClous = "W.WORK_ORDER_NO = '" + workorderno + "' "
                    + "AND W.MODEL_NO = M.MODEL_NO (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.LEVEL_ID = L.LEVEL_ID (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.WARRANTY_VRIF_TYPE = S.CODE (+) "
                    + "AND S.DOMAIN_NAME = 'WARANTY' "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS'";

            String table = "SD_WORK_ORDERS W,"
                    + "SD_MODEL_LISTS M,"
                    + "SD_DEFECT_CODES D,"
                    + "SD_REPAIR_LEVELS L,"
                    + "SD_BUSINESS_STRUCTURES B,"
                    + "SD_MDECODE S,SD_MDECODE S2";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM " + table + " "
                    + "WHERE " + whereClous + " ");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setBrand(rs.getString("BRAND"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setRccReference(rs.getString("RCC_REFERENCE"));
                wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                wo.setBisId(rs.getInt("BIS_ID"));
                wo.setSchemeId(rs.getInt("SCHEME_ID"));
                wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wo.setTecnician(rs.getString("TECHNICIAN"));
                wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                wo.setLevelId(rs.getInt("LEVEL_ID"));
                wo.setRemarks(rs.getString("REMARKS"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setCourierNo(rs.getString("COURIER_NO"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setGatePass(rs.getString("GATE_PASS"));
                wo.setStatus(rs.getInt("STATUS"));
                wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                wo.setLevelName(rs.getString("LEVEL_NAME"));
                wo.setBinName(rs.getString("BIS_NAME"));
                wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                wo.setRepairDate(rs.getString("REPAIR_DATE"));
                wo.setWoDetials(woMaintainList);
                wo.setWoMaintainDetials(woMaintainDetail);
                wo.setWoDefect(woMaintainDefect);
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderDetail Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = workOrderList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderDetailsByWo(String woNumber) {

        logger.info("getWorkOrderDetailsByWo Method Call....................");

        MessageWrapper mw = new MessageWrapper();

        ArrayList<String> data = new ArrayList<>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        String sql = "SELECT dc.MAJ_CODE_DESC, wo.ACCESSORIES_RETAINED "
                + "FROM SD_WORK_ORDERS wo, SD_DEFECT_CODES dc "
                + "WHERE wo.WORK_ORDER_NO = '" + woNumber + "' AND wo.DEFECTS = dc.MAJ_CODE";

        try {

            ResultSet result = dbCon.search(con, sql);

            while (result.next()) {
                totalrecode++;
                data.add(result.getString("MAJ_CODE_DESC"));
                data.add(result.getString("ACCESSORIES_RETAINED"));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getWorkOrderDetailsByWo Methd call.." + ex.toString());
        }

        mw.setData(data);
        mw.setTotalRecords(totalrecode);

        return mw;
    }

    public static MessageWrapper getQcDetailsByWo(String woNumber) {

        logger.info("getQcDetailsByWo Method Call....................");

        MessageWrapper mw = new MessageWrapper();

        ArrayList<String> data = new ArrayList<>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        String sql = "SELECT * FROM SD_QC_CHK_LST qc WHERE qc.WORK_ORDER_NO = '" + woNumber + "'";

        try {

            ResultSet result = dbCon.search(con, sql);

            while (result.next()) {
                totalrecode++;
                data.add(result.getString("RESOLVE_DEFECTS"));
                data.add(result.getString("WORK_ORDER_NO"));
                data.add(result.getString("VISUAL_INSPECT"));
                data.add(result.getString("ENG_TEST"));
                data.add(result.getString("CALL_SMS"));
                data.add(result.getString("SW_VERSION"));
                data.add(result.getString("ACCESSORIES"));
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderDetailsByWo Methd call.." + ex.toString());
        }

        mw.setData(data);
        mw.setTotalRecords(totalrecode);

        return mw;
    }

}
