/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Customer;

/**
 *
 * @author SDU
 */
public class CustomerController {

    public static ValidationWrapper checkCustomer(String cus_nic) {
        logger.info("checkCustomer Method Call.........");
        int cus_cnt = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_CUSTOMER_DETAILS WHERE CUSTOMER_NIC='" + cus_nic + "'");
            logger.info("Get Data to ResultSet........");

            while (rs.next()) {
                cus_cnt = rs.getInt("CNT");
            }

        } catch (Exception ex) {
            logger.info("Error in checkCustomer Method  :" + ex.toString());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (cus_cnt > 0) {
            vw.setReturnFlag("9");
            vw.setReturnMsg("This NIC Already Exist");
            logger.info("This NIC Already Exist........");
        } else {
            vw.setReturnFlag("1");
            vw.setReturnMsg("This NIC OK");
            logger.info("This NIC OK................");
        }
        logger.info("Ready to return Values.....");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;

    }

    public static MessageWrapper getCustomerDetals(String cus_nic) {
        logger.info("getCustomerDetals Method Call.........");
        int totalrecode = 0;
        ArrayList<Customer> cusList = new ArrayList<>();
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CUSTOMER_NAME,CONTACT_NO,EMAIL,ADDRESS,CUSTOMER_NIC,TO_CHAR(DATE_OF_BIRTH,'YYYY/MM/DD') DOB";

            ResultSet rs = dbCon.search(con,"SELECT (SELECT COUNT(*) FROM SD_CUSTOMER_DETAILS WHERE CUSTOMER_NIC = '" + cus_nic + "' ) CNT,"
                    + "" + seletColunm + " "
                    + "FROM SD_CUSTOMER_DETAILS "
                    + "WHERE CUSTOMER_NIC = '" + cus_nic + "'");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                Customer cn = new Customer();
                totalrecode = rs.getInt("CNT");
                cn.setCustomerName(rs.getString("CUSTOMER_NAME"));
                cn.setContactNo(rs.getString("CONTACT_NO"));
                cn.setEmail(rs.getString("EMAIL"));
                cn.setAddress(rs.getString("ADDRESS"));
                cn.setCustomerNIC(rs.getString("CUSTOMER_NIC"));
                cn.setDateOfBirth(rs.getString("DOB"));
                cusList.add(cn);
            }

        } catch (Exception ex) {
            logger.info("Error in getCustomerDetals Method  :" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(cusList);
        logger.info("Ready to return Values.....");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }

    public static MessageWrapper checkCustomerDetal(String cus_nic) {
        logger.info("checkCustomerDetal Method Call.........");
        int totalrecode = 0;
        ArrayList<Customer> cusList = new ArrayList<>();
        int cus_cnt = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_cnt = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_CUSTOMER_DETAILS WHERE CUSTOMER_NIC='" + cus_nic + "'");
            logger.info("Get Data to ResultSet........");

            while (rs_cnt.next()) {
                cus_cnt = rs_cnt.getInt("CNT");
            }

            if (cus_cnt > 0) {

                String seletColunm = "CUSTOMER_NAME,CONTACT_NO,EMAIL,ADDRESS,CUSTOMER_NIC,TO_CHAR(DATE_OF_BIRTH,'YYYY/MM/DD') DOB";

                ResultSet rs = dbCon.search(con,"SELECT (SELECT COUNT(*) FROM SD_CUSTOMER_DETAILS WHERE CUSTOMER_NIC = '" + cus_nic + "' ) CNT,"
                        + "" + seletColunm + " "
                        + "FROM SD_CUSTOMER_DETAILS "
                        + "WHERE CUSTOMER_NIC = '" + cus_nic + "'");

                logger.info("Get Data to Resultset...");

                while (rs.next()) {
                    Customer cn = new Customer();
                    totalrecode = rs.getInt("CNT");
                    cn.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    cn.setContactNo(rs.getString("CONTACT_NO"));
                    cn.setEmail(rs.getString("EMAIL"));
                    cn.setAddress(rs.getString("ADDRESS"));
                    cn.setCustomerNIC(rs.getString("CUSTOMER_NIC"));
                    cn.setDateOfBirth(rs.getString("DOB"));
                    cn.setReturnFlag("1");
                    cn.setReturnMessage("Register Customer");
                    cusList.add(cn);
                }
            } else {
                Customer cn = new Customer();
                cn.setReturnFlag("9");
                cn.setReturnMessage("This Customer Not Register");
                cusList.add(cn);
            }

        } catch (Exception ex) {
            logger.info("Error in checkCustomerDetal Method  :" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        if(con == null){
              mw.setTotalRecords(1000000);
        }else{
              mw.setTotalRecords(totalrecode);
        }
      
        mw.setData(cusList);
        logger.info("Ready to return Values.....");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }

}
