/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Function;

/**
 *
 * @author SDU
 */
public class FunctionController {

    public static MessageWrapper getFunctions(int funtid,
            String funtdesc,
            int functtype,
            int funtstatus,
            int start,
            int limit) {
        logger.info("getFunctions Method Call....................");
        ArrayList<Function> funtList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String FuntId = (funtid == 0 ? "" : "AND FUNC_ID = '" + funtid + "'");
            String FuntDesc = (funtdesc.equalsIgnoreCase("all") ? "NVL(FUNC_DESC,'AA') = NVL(FUNC_DESC,'AA')" : "UPPER(FUNC_DESC) LIKE UPPER('%" + funtdesc + "%')");
            String FuntType = (functtype == 0 ? "" : "AND FUNC_TYPE = '" + functtype + "' ");
            String FuntStatus = (funtstatus == 0 ? "" : "AND STATUS = '" + funtstatus + "' ");

            String seletColunm = "FUNC_ID,FUNC_DESC,FUNC_TYPE,STATUS";

            String whereClous = "" + FuntDesc + ""
                    + "" + FuntId + ""
                    + "" + FuntType + ""
                    + "" + FuntStatus + "";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_FUNCTIONS WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY FUNC_ID) RN "
                    + " FROM SD_FUNCTIONS WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                Function fc = new Function();
                totalrecode = rs.getInt("CNT");
     //           fc.setFuntId(rs.getInt("FUNC_ID"));
                //          fc.setFuntDesc(rs.getString("FUNC_DESC"));
                //          fc.setFuntType(rs.getInt("FUNC_TYPE"));
                //          fc.setFuntStatus(rs.getInt("STATUS"));
                funtList.add(fc);
            }

        } catch (Exception ex) {
            logger.info("Error getFunctions methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(funtList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;

    }
    
    

}
