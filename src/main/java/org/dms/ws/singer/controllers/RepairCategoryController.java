/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.RepairCategory;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class RepairCategoryController {

    public static MessageWrapper getRapairCategories(int recatid,
            String recatdesc,
            int recatestatus,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getRapairCategories Method Call.....");
        ArrayList<RepairCategory> recateList = new ArrayList<>();
        int totalrecode = 0;
        try {
            order = DBMapper.repairCategory(order);

            String ReCateId = (recatid == 0 ? "" : "AND REP_CAT_ID = " + recatid + "");
            String ReCateDesc = (recatdesc.equalsIgnoreCase("all") ? "NVL(REP_CAT_DESC,'AA') = NVL(REP_CAT_DESC,'AA')" : "UPPER(REP_CAT_DESC) LIKE UPPER('%" + recatdesc + "%')");
            String ReCateStatus = (recatestatus == 0 ? "" : "AND STATUS = " + recatestatus + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY REP_CAT_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColunm = "REP_CAT_ID,REP_CAT_DESC,STATUS";

            String whereClous = "" + ReCateDesc + ""
                    + "" + ReCateId + ""
                    + "" + ReCateStatus + "";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_REPAIR_CATEGORIES WHERE  " + whereClous + ") CNT,"
                    + "" + selectColunm + ","
                    + "ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_REPAIR_CATEGORIES "
                    + "WHERE " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " "
                    + "AND " + (start + limit) + " "
                    + " ORDER BY RN ");

            logger.info("Get Data to ResultSet......");
            while (rs.next()) {
                RepairCategory cr = new RepairCategory();
                totalrecode = rs.getInt("CNT");
                cr.setReCateId(rs.getInt("REP_CAT_ID"));
                cr.setReCateDesc(rs.getString("REP_CAT_DESC"));
                cr.setReCateStatus(rs.getInt("STATUS"));
                recateList.add(cr);
            }

        } catch (Exception ex) {
            logger.info("Error getRapairCategories Method  :" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(recateList);
        logger.info("Redy to Return Values.........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper addRepairCategory(RepairCategory rc,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("addRepairCategory Method Call..........");
        int result = 0;
        int re_cat_count = 0;
        try {

            ResultSet rs_rcat_cnt = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_REPAIR_CATEGORIES WHERE UPPER(REP_CAT_DESC) = UPPER('" + rc.getReCateDesc() + "')");

            while (rs_rcat_cnt.next()) {
                re_cat_count = rs_rcat_cnt.getInt("CNT");
            }

            String cloumn = "REP_CAT_ID,REP_CAT_DESC,STATUS,USER_INSERTED,DATE_INSERTED";

            String values = "(SELECT NVL(MAX(REP_CAT_ID),0)+1 FROM SD_REPAIR_CATEGORIES),"
                    + "UPPER('" + rc.getReCateDesc() + "'),"
                    + "" + rc.getReCateStatus() + ","
                    + "'" + user_id + "',"
                    + "SYSDATE";
            if (re_cat_count == 0) {

                try {
                    dbCon.save(con, "INSERT INTO SD_REPAIR_CATEGORIES(" + cloumn + ") VALUES(" + values + ")");
                    result = 1;

                } catch (Exception e) {
                    logger.info("Error in Inserting...." + e.toString());
                    result = 9;
                }

            } else {
                result = 2;
                logger.info("Repair Category Description allredy Exist...");
            }

        } catch (Exception ex) {
            logger.info("Error addRepairCategory Method  :" + ex.toString());

        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Repair Category insert Successfully");
            logger.info("Repair Category insert Successfully......");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Repair Category Description Already Exist");
            logger.info("Repair Category Description allredy Exist......");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Repair Category Insert");
            logger.info("Error in Repair Category Insert......");
        }
        logger.info("Redy to Return Values.........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editRepairCategory(RepairCategory rc,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("editRepairCategory Method Call.......");
        int result = 0;
        int re_cat_count = 0;
        int actvie_level = 0;
        try {

            ResultSet rs_rcat_cnt = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_REPAIR_CATEGORIES "
                    + "WHERE UPPER(REP_CAT_DESC) = UPPER('" + rc.getReCateDesc() + "') "
                    + "AND REP_CAT_ID != " + rc.getReCateId() + "");

            while (rs_rcat_cnt.next()) {
                re_cat_count = rs_rcat_cnt.getInt("CNT");
            }

            if (re_cat_count > 0) {
                result = 2;

            } else {

                if (rc.getReCateStatus() == 2) {
                    ResultSet rs_level = dbCon.search(con, "SELECT COUNT(*) CNT "
                            + "FROM SD_REPAIR_LEVELS "
                            + "WHERE REP_CAT_ID = " + rc.getReCateId() + " "
                            + "AND STATUS = 1");
                    while (rs_level.next()) {
                        actvie_level = rs_level.getInt("CNT");
                    }
                }
                if (actvie_level > 0) {
                    result  = 3;
                } else {

                    try {
                        dbCon.save(con, "UPDATE SD_REPAIR_CATEGORIES "
                                + "SET REP_CAT_DESC = UPPER('" + rc.getReCateDesc() + "'),"
                                + "STATUS = " + rc.getReCateStatus() + ","
                                + "USER_MODIFIED = '" + user_id + "',"
                                + "DATE_MODIFIED = SYSDATE "
                                + "WHERE REP_CAT_ID = " + rc.getReCateId() + "");

                        result = 1;

                    } catch (Exception e) {
                        logger.info("Error in Update.............");
                        result = 9;
                    }
                }

            }

        } catch (Exception ex) {
            logger.info("Error editRepairCategory Method  :" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();

        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Repair Category Update Successfully");
            logger.info("Repair Category Update Succesfully........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Repair Category Already Exist");
            logger.info("Repair Category Allredy Exist........");
        }else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Active Repair Level in this Caregory");
            logger.info("Active Repair Level in this Caregory........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Repair Category Update");
            logger.info("Error in Repair Category Update......");
        }
        logger.info("Ready to Returns Values............");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    
    
    
    
}
