/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.BussinessStructures;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.User;

/**
 *
 * @author SDU
 */
public class BussStrucController {

    public static MessageWrapper getBussinessStructure(int bisId, int userCnt) {
        logger.info("getBussinessStructure method Call..........");
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            //String where = "WHERE B.BIS_ID=B.BIS_ID ";
            String where = " WHERE B.BIS_ID=U.BIS_ID (+) ";

            String BisId = "  ";
            if (bisId != 0) {
                where += "  AND B.BIS_ID=" + bisId + "  ";
            }

            if (userCnt != 0) {
               // where += " AND (SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID )>0";
            }

            ResultSet rs = dbCon.search(con, "SELECT B.BIS_ID,"
                    + "B.BIS_NAME,"
                    + "B.BIS_DESC,"
                    + "B.PARENT_BIS_ID,"
                    + "B.CATEGORY_1,"
                    + "B.CATEGORY_2,"
                    + "B.BIS_STRU_TYPE_ID,"
                    + "B.LONGITUDE,"
                    + "B.LATITUDE,"
                    + "B.ADDRESS,"
                    + "B.CONTACT_NO,"
                    + "B.SHOW_IN_MAP_FLAG,"
                    + "B.STATUS,"
                    + "B.ALLOW_OFFLINE,"
                    //+ "(SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID ) CNT,"
                    + " NVL(U.USER_CNT, 0) AS CNT, "
                    + " ERP_CODE,PO_TITLE,CHQ_TITLE,ESSD_ACC_NO,ESSD_PCT,INVOICE_NO "
                    + " FROM SD_BUSINESS_STRUCTURES B, V_BIS_USERS U "
                    + where
                    + "ORDER BY B.BIS_ID ASC");

//                        ResultSet rs = dbCon.search(con,"SELECT              B.BIS_ID BISID,B.BIS_NAME BISNAME, B.BIS_DESC BISDESC, B.PARENT_BIS_ID PARENTBISID,(SELECT COUNT(*) FROM SD_USERS U WHERE U.BIS_ID=B.BIS_ID) CNT"
//                    + "                  FROM                SD_BUSINESS_STRUCTURES B "
//                    + "                  START WITH          B.BIS_ID = B.BIS_ID "
//                    + "                  CONNECT BY PRIOR    B.PARENT_BIS_ID = B.BIS_ID "
//                    + "UNION                                                   "
//                    + "                  SELECT              B.BIS_ID BISID,B.BIS_NAME BISNAME, B.BIS_DESC BISDESC, B.PARENT_BIS_ID PARENTBISID,(SELECT COUNT(*) FROM SD_USERS U WHERE U.BIS_ID=B.BIS_ID) CNT"
//                    + "                  FROM                SD_BUSINESS_STRUCTURES B "
//                    + "                  START WITH          B.BIS_ID = B.BIS_ID "
//                    + "                  CONNECT BY PRIOR    B.BIS_ID =B.PARENT_BIS_ID");
            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                bs.setBisId(rs.getInt("BIS_ID"));;
                bs.setBisName(rs.getString("BIS_NAME"));
                bs.setBisDesc(rs.getString("BIS_DESC"));
                bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                bs.setCategory1(rs.getInt("CATEGORY_1"));
                bs.setCategory2(rs.getInt("CATEGORY_2"));
                bs.setStructType(rs.getInt("BIS_STRU_TYPE_ID"));
                bs.setLogitude(rs.getString("LONGITUDE"));
                bs.setLatitude(rs.getString("LATITUDE"));
                bs.setAddress(rs.getString("ADDRESS"));
                bs.setTeleNumber(rs.getString("CONTACT_NO"));
                bs.setMapFlag(rs.getInt("SHOW_IN_MAP_FLAG"));
                bs.setStatus(rs.getInt("STATUS"));
                bs.setStatusOfline(rs.getInt("ALLOW_OFFLINE"));
                bs.setBisUserCount(rs.getInt("CNT"));
                bs.setErpCode(rs.getString("ERP_CODE"));
                bs.setPoTitle(rs.getString("PO_TITLE"));
                bs.setCheqTitle(rs.getString("CHQ_TITLE"));
                bs.setEssdAcntNo(rs.getString("ESSD_ACC_NO"));
                bs.setEddsPct(rs.getString("ESSD_PCT"));
                bs.setInvoiceNo(rs.getString("INVOICE_NO"));
                bsList.add(bs);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessStruture......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setData(bsList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }
    public static MessageWrapper getBussinessStructureServiceCenters(int bisId, int userCnt) {
        logger.info("getBussinessStructure method Call..........");
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            //String where = "WHERE B.BIS_ID=B.BIS_ID ";
            String where = " WHERE B.BIS_ID=U.BIS_ID (+) ";

            String BisId = "  ";
            if (bisId != 0) {
                where += "  AND B.BIS_ID=" + bisId + "  ";
            }

            if (userCnt != 0) {
               // where += " AND (SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID )>0";
            }

            ResultSet rs = dbCon.search(con, "SELECT DISTINCT  B.BIS_ID,"
                    + "B.BIS_NAME,"
                    + "B.BIS_DESC,"
                    + "B.PARENT_BIS_ID,"
                    + "B.CATEGORY_1,"
                    + "B.CATEGORY_2,"
                    + "B.BIS_STRU_TYPE_ID,"
                    + "B.LONGITUDE,"
                    + "B.LATITUDE,"
                    + "B.ADDRESS,"
                    + "B.CONTACT_NO,"
                    + "B.SHOW_IN_MAP_FLAG,"
                    + "B.STATUS,"
                    //+ "(SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID ) CNT,"
                  //  + " NVL(U.USER_CNT, 0) AS CNT, "
                    + " ERP_CODE,PO_TITLE,CHQ_TITLE,ESSD_ACC_NO,ESSD_PCT,INVOICE_NO "
                    + " FROM SD_BUSINESS_STRUCTURES B, V_BIS_USERS U "
                    + "where B.BIS_STRU_TYPE_ID =8 OR B.BIS_STRU_TYPE_ID = 7 "
                    + "ORDER BY B.BIS_ID ASC");

//                        ResultSet rs = dbCon.search(con,"SELECT              B.BIS_ID BISID,B.BIS_NAME BISNAME, B.BIS_DESC BISDESC, B.PARENT_BIS_ID PARENTBISID,(SELECT COUNT(*) FROM SD_USERS U WHERE U.BIS_ID=B.BIS_ID) CNT"
//                    + "                  FROM                SD_BUSINESS_STRUCTURES B "
//                    + "                  START WITH          B.BIS_ID = B.BIS_ID "
//                    + "                  CONNECT BY PRIOR    B.PARENT_BIS_ID = B.BIS_ID "
//                    + "UNION                                                   "
//                    + "                  SELECT              B.BIS_ID BISID,B.BIS_NAME BISNAME, B.BIS_DESC BISDESC, B.PARENT_BIS_ID PARENTBISID,(SELECT COUNT(*) FROM SD_USERS U WHERE U.BIS_ID=B.BIS_ID) CNT"
//                    + "                  FROM                SD_BUSINESS_STRUCTURES B "
//                    + "                  START WITH          B.BIS_ID = B.BIS_ID "
//                    + "                  CONNECT BY PRIOR    B.BIS_ID =B.PARENT_BIS_ID");
            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                bs.setBisId(rs.getInt("BIS_ID"));;
                bs.setBisName(rs.getString("BIS_NAME"));
                bs.setBisDesc(rs.getString("BIS_DESC"));
                bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                bs.setCategory1(rs.getInt("CATEGORY_1"));
                bs.setCategory2(rs.getInt("CATEGORY_2"));
                bs.setStructType(rs.getInt("BIS_STRU_TYPE_ID"));
                bs.setLogitude(rs.getString("LONGITUDE"));
                bs.setLatitude(rs.getString("LATITUDE"));
                bs.setAddress(rs.getString("ADDRESS"));
                bs.setTeleNumber(rs.getString("CONTACT_NO"));
                bs.setMapFlag(rs.getInt("SHOW_IN_MAP_FLAG"));
                bs.setStatus(rs.getInt("STATUS"));
              //  bs.setBisUserCount(rs.getInt("CNT"));
                bs.setErpCode(rs.getString("ERP_CODE"));
                bs.setPoTitle(rs.getString("PO_TITLE"));
                bs.setCheqTitle(rs.getString("CHQ_TITLE"));
                bs.setEssdAcntNo(rs.getString("ESSD_ACC_NO"));
                bs.setEddsPct(rs.getString("ESSD_PCT"));
                bs.setInvoiceNo(rs.getString("INVOICE_NO"));
                bsList.add(bs);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessStruture......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setData(bsList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }
    
    
    
     public static MessageWrapper getBussinessStructureParticulerBisId(int bisId) {
        logger.info("getBussinessStructurePaticulerBisId method Call..........");
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {


            ResultSet rs = dbCon.search(con, "SELECT BIS_ID,"
                    + "BIS_NAME,"
                    + "BIS_DESC,"
                    + "PARENT_BIS_ID,"
                    + "CATEGORY_1,"
                    + "CATEGORY_2,"
                    + "BIS_STRU_TYPE_ID,"
                    + "LONGITUDE,"
                    + "LATITUDE,"
                    + "ADDRESS,"
                    + "CONTACT_NO,"
                    + "SHOW_IN_MAP_FLAG,"
                    + "STATUS,"
                    + "ERP_CODE,"
                    + "PO_TITLE,"
                    + "CHQ_TITLE,"
                    + "ESSD_ACC_NO,"
                    + "ESSD_PCT,"
                    + "INVOICE_NO "
             + "FROM SD_BUSINESS_STRUCTURES B "
             + "WHERE BIS_ID IN (SELECT BIS_ID FROM (SELECT *  FROM SD_BUSINESS_STRUCTURES "
                                                  + " START WITH BIS_ID = "+bisId+" "
                                                 + "CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID) "
                                 + "WHERE STATUS != 2) "
             + "ORDER BY BIS_ID ASC");

            logger.info("Get data to Result Set.............");
            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                bs.setBisId(rs.getInt("BIS_ID"));
                if(rs.getInt("BIS_ID") == bisId)
                {
                    bs.setBisPrtnId(0);
                }else{
                    bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                }
                //bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                bs.setBisName(rs.getString("BIS_NAME"));
                bs.setBisDesc(rs.getString("BIS_DESC")); 
                bs.setCategory1(rs.getInt("CATEGORY_1"));
                bs.setCategory2(rs.getInt("CATEGORY_2"));
                bs.setStructType(rs.getInt("BIS_STRU_TYPE_ID"));
                bs.setLogitude(rs.getString("LONGITUDE"));
                bs.setLatitude(rs.getString("LATITUDE"));
                bs.setAddress(rs.getString("ADDRESS"));
                bs.setTeleNumber(rs.getString("CONTACT_NO"));
                bs.setMapFlag(rs.getInt("SHOW_IN_MAP_FLAG"));
                bs.setStatus(rs.getInt("STATUS"));
                bs.setErpCode(rs.getString("ERP_CODE"));
                bs.setPoTitle(rs.getString("PO_TITLE"));
                bs.setCheqTitle(rs.getString("CHQ_TITLE"));
                bs.setEssdAcntNo(rs.getString("ESSD_ACC_NO"));
                bs.setEddsPct(rs.getString("ESSD_PCT"));
                bs.setInvoiceNo(rs.getString("INVOICE_NO"));
                bsList.add(bs);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessStructurePaticulerBisId......:" + ex.toString());
        }
        totalrecode = bsList.size();
        MessageWrapper mw = new MessageWrapper();
        mw.setData(bsList);
        mw.setTotalRecords(totalrecode);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }
    
    

    public static ValidationWrapper addBussinessStructure(BussinessStructure busstruinfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addBussinessStructure Method Call.........");
        int result = 0;
        int bs_max = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String colunm = "BIS_ID,"
                    + "BIS_NAME,"
                    + "BIS_DESC,"
                    + "CATEGORY_1,"
                    + "CATEGORY_2,"
                    + "PARENT_BIS_ID,"
                    + "BIS_STRU_TYPE_ID,"
                    + "LONGITUDE,"
                    + "LATITUDE,"
                    + "ADDRESS,"
                    + "CONTACT_NO,"
                    + "SHOW_IN_MAP_FLAG,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED,"
                    + "ERP_CODE,"
                    + "PO_TITLE,"
                    + "CHQ_TITLE,"
                    + "ESSD_ACC_NO,"
                    + "ESSD_PCT,"
                    + "INVOICE_NO,"
                    + "ALLOW_OFFLINE";

            ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(BIS_ID),0) MAX FROM SD_BUSINESS_STRUCTURES");

            while (rs_max.next()) {
                bs_max = rs_max.getInt("MAX");
            }

            String values = "?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,?,?,?,?,?,?,?";

            try {
                PreparedStatement ps_in_bs = dbCon.prepare(con, "INSERT INTO SD_BUSINESS_STRUCTURES(" + colunm + ") VALUES(" + values + ")");
                bs_max = bs_max + 1;
                ps_in_bs.setInt(1, bs_max);
                ps_in_bs.setString(2, busstruinfo.getBisName());
                ps_in_bs.setString(3, busstruinfo.getBisDesc());
                ps_in_bs.setInt(4, busstruinfo.getCategory1());
                ps_in_bs.setInt(5, busstruinfo.getCategory2());
                ps_in_bs.setInt(6, busstruinfo.getBisPrtnId());
                ps_in_bs.setInt(7, busstruinfo.getStructType());
                ps_in_bs.setString(8, busstruinfo.getLogitude());
                ps_in_bs.setString(9, busstruinfo.getLatitude());
                ps_in_bs.setString(10, busstruinfo.getAddress());
                ps_in_bs.setString(11, busstruinfo.getTeleNumber());
                ps_in_bs.setInt(12, busstruinfo.getMapFlag());
                ps_in_bs.setInt(13, busstruinfo.getStatus());
                ps_in_bs.setString(14, user_id);
                ps_in_bs.setString(15, busstruinfo.getErpCode());
                ps_in_bs.setString(16, busstruinfo.getPoTitle());
                ps_in_bs.setString(17, busstruinfo.getCheqTitle());
                ps_in_bs.setString(18, busstruinfo.getEssdAcntNo());
                ps_in_bs.setString(19, busstruinfo.getEddsPct());
                ps_in_bs.setString(20, busstruinfo.getInvoiceNo());
                ps_in_bs.setInt(21, busstruinfo.getStatusOfline());
                ps_in_bs.executeUpdate();
                result = 1;
                logger.info("Bussness Structure Data Inserted...");
            } catch (Exception e) {
                logger.info("Error Data Inserting...:" + e.toString());
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error addBussinessStructure Method..:" + ex.toString());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Business Structure Insert Successfully");
            logger.info("Business Structure Insert Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Data Inserting");
            logger.info("Getting Error in Data Inserting...");
        }
        //logger.info("Error addBussinessStructure Method..:" + ex.toString());
        return vw;
    }

    public static MessageWrapper getBussinessStructureDetail(int bis_id) {
        logger.info("getBussinessStructureDetail method Call.....");
        int totlarecode = 0;
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectQry = "SELECT (SELECT COUNT(*) FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID=" + bis_id + ") CNT,"
                    + "BIS_NAME,"
                    + "BIS_DESC,"
                    + "CATEGORY_1,"
                    + "CATEGORY_2,"
                    + "PARENT_BIS_ID,"
                    + "BIS_STRU_TYPE_ID,"
                    + "LONGITUDE,"
                    + "LATITUDE,"
                    + "ADDRESS,"
                    + "CONTACT_NO,"
                    + "SHOW_IN_MAP_FLAG,"
                    + "STATUS,"
                    + "ERP_CODE,"
                    + "PO_TITLE,"
                    + "CHQ_TITLE,"
                    + "ESSD_ACC_NO,"
                    + "ESSD_PCT,"
                    + "INVOICE_NO "
                    + "FROM  SD_BUSINESS_STRUCTURES "
                    + "WHERE BIS_ID=" + bis_id + "";

            ResultSet rs = dbCon.search(con, selectQry);

            logger.info("Get data to ResultSet.......");
            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                totlarecode = rs.getInt("CNT");
                bs.setBisName(rs.getString("BIS_NAME"));
                bs.setBisDesc(rs.getString("BIS_DESC"));
                bs.setCategory1(rs.getInt("CATEGORY_1"));
                bs.setCategory2(rs.getInt("CATEGORY_2"));
                bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                bs.setStructType(rs.getInt("BIS_STRU_TYPE_ID"));
                bs.setLogitude(rs.getString("LONGITUDE"));
                bs.setLatitude(rs.getString("LATITUDE"));
                bs.setAddress(rs.getString("ADDRESS"));
                bs.setTeleNumber(rs.getString("CONTACT_NO"));
                bs.setMapFlag(rs.getInt("SHOW_IN_MAP_FLAG"));
                bs.setStatus(rs.getInt("STATUS"));
                bs.setErpCode(rs.getString("ERP_CODE"));
                bs.setPoTitle(rs.getString("PO_TITLE"));
                bs.setCheqTitle(rs.getString("CHQ_TITLE"));
                bs.setEssdAcntNo(rs.getString("ESSD_ACC_NO"));
                bs.setEddsPct(rs.getString("ESSD_PCT"));
                bs.setInvoiceNo(rs.getString("INVOICE_NO"));
                bsList.add(bs);
                result = 1;

            }
        } catch (Exception ex) {
            logger.info("Error getBussinessStructureDetail...." + ex.toString());
            result = 9;
        }
        logger.info("Redy to return Values...........");
        MessageWrapper mw = new MessageWrapper();

        mw.setTotalRecords(totlarecode);
        mw.setData(bsList);
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper editBussinessStructure(BussinessStructure busstruinfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editBussinessStructure method Call........");
        int result = 0;
        int active_child = 0;
        int parent_inactive = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            if (busstruinfo.getStatus() == 2) {
                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_BUSINESS_STRUCTURES "
                        + "WHERE PARENT_BIS_ID =  " + busstruinfo.getBisId() + ""
                        + "AND STATUS = 1");
                while (rs_cnt.next()) {
                    active_child = rs_cnt.getInt("CNT");
                }

            }

            if (busstruinfo.getStatus() == 1) {
                ResultSet rs_parant_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_BUSINESS_STRUCTURES  "
                        + "WHERE BIS_ID = (SELECT PARENT_BIS_ID "
                        + "FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID = " + busstruinfo.getBisId() + ") "
                        + "AND STATUS = 2");
                while (rs_parant_cnt.next()) {
                    parent_inactive = rs_parant_cnt.getInt("CNT");
                }

            }

            if (active_child > 0) {
                result = 2;
            } else {
                if (parent_inactive > 0) {
                    result = 3;
                } else {
                    PreparedStatement ps_up_bs = dbCon.prepare(con, "UPDATE SD_BUSINESS_STRUCTURES "
                            + "SET BIS_NAME=?,"
                            + "BIS_DESC=?,"
                            + "CATEGORY_1=?,"
                            + "CATEGORY_2=?,"
                            + "PARENT_BIS_ID=?,"
                            + "BIS_STRU_TYPE_ID=?,"
                            + "LONGITUDE=?,"
                            + "LATITUDE=?,"
                            + "ADDRESS=?,"
                            + "CONTACT_NO=?,"
                            + "SHOW_IN_MAP_FLAG=?,"
                            + "STATUS=?,"
                            + "USER_MODIFIED=?,"
                            + "DATE_MODIFIED=SYSDATE,"
                            + "ERP_CODE=?,"
                            + "PO_TITLE=?,"
                            + "CHQ_TITLE=?,"
                            + "ESSD_ACC_NO=?,"
                            + "ESSD_PCT=?,"
                            + "INVOICE_NO=?, "
                            + "ALLOW_OFFLINE=? "
                            + "WHERE BIS_ID=?");

                    ps_up_bs.setString(1, busstruinfo.getBisName());
                    ps_up_bs.setString(2, busstruinfo.getBisDesc());
                    ps_up_bs.setInt(3, busstruinfo.getCategory1());
                    ps_up_bs.setInt(4, busstruinfo.getCategory2());
                    ps_up_bs.setInt(5, busstruinfo.getBisPrtnId());
                    ps_up_bs.setInt(6, busstruinfo.getStructType());
                    ps_up_bs.setString(7, busstruinfo.getLogitude());
                    ps_up_bs.setString(8, busstruinfo.getLatitude());
                    ps_up_bs.setString(9, busstruinfo.getAddress());
                    ps_up_bs.setString(10, busstruinfo.getTeleNumber());
                    ps_up_bs.setInt(11, busstruinfo.getMapFlag());
                    ps_up_bs.setInt(12, busstruinfo.getStatus());
                    ps_up_bs.setString(13, user_id);
                    ps_up_bs.setString(14, busstruinfo.getErpCode());
                    ps_up_bs.setString(15, busstruinfo.getPoTitle());
                    ps_up_bs.setString(16, busstruinfo.getCheqTitle());
                    ps_up_bs.setString(17, busstruinfo.getEssdAcntNo());
                    ps_up_bs.setString(18, busstruinfo.getEddsPct());
                    ps_up_bs.setString(19, busstruinfo.getInvoiceNo());
                    ps_up_bs.setInt(20, busstruinfo.getStatusOfline());
                    ps_up_bs.setInt(21, busstruinfo.getBisId());
                    ps_up_bs.executeUpdate();

                    result = 1;
                }
            }

        } catch (Exception ex) {
            logger.info("Error editBussinessStructure method..:" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Business Structure Update Successfully.");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Active Sub Business Structures in this Business Level");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Parent Level Inactive");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Business Structure Update.");
        }
        dbCon.ConectionClose(con);
        logger.info("Redy to return values............");
        logger.info("-----------------------------------------");
        return vw;
    }

    public static MessageWrapper getBussinessUsers(int bisId) {
        logger.info("getBussnessUsers Method Call.....");
        logger.info("Bis ID = " + bisId);
        int totalrecode = 0;
        ArrayList<User> user = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String BisID = (bisId == 0 ? "IS NULL" : "=" + bisId + "");

            ResultSet rs = dbCon.search(con, "SELECT (SELECT COUNT(*) FROM SD_USERS WHERE BIS_ID " + BisID + ") CNT,USER_ID,FIRST_NAME,LAST_NAME FROM SD_USERS WHERE BIS_ID " + BisID + "");
            logger.info("Get Data to ResultSet.................");
            while (rs.next()) {
                User us = new User();
                totalrecode = rs.getInt("CNT");
                us.setUserId(rs.getString("USER_ID"));
                us.setFirstName(rs.getString("FIRST_NAME"));
                us.setLastName(rs.getString("LAST_NAME"));
                user.add(us);
            }

        } catch (Exception ex) {
            logger.info("Error getBussnessUsers method :" + ex.toString());
        }
        dbCon.ConectionClose(con);
        logger.info("Ready to Return Values.......");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(user);
        logger.info("---------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addBussinessUsers(ArrayList<User> userList,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addBussinessUsers Method Call............");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            logger.info("Create Statement......");
            try {
                for (User us : userList) {
                    logger.info("Add sql to Batch.....");
//                    dbCon.Batchsave(con,"UPDATE SD_USERS SET BIS_ID=" + us.getBisId() + " WHERE USER_ID='" + us.getUserId() + "'");
                }

                result = 1;
            } catch (Exception e) {
                logger.info("Error User Update...:" + e.toString());
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error addBussinessUsers Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Successfully Add Business ID to User");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Add Business ID to User");
        }
        return vw;
    }

    public static MessageWrapper getBussinessAsignList(String bis_type) {
        logger.info("getBussinessAsignList method Call..........");
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT B.BIS_ID,B.BIS_NAME"
                    + " FROM SD_BUSINESS_STRUCTURES B,SD_BIS_STRU_TYPES T"
                    + " WHERE B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID "
                    + " AND B.STATUS = 1 "
                    + " AND T.BIS_STRU_TYPE_ID  IN( " + bis_type + " ) "
                    + " ORDER BY B.BIS_ID ASC");

            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                bs.setBisId(rs.getInt("BIS_ID"));;
                bs.setBisName(rs.getString("BIS_NAME"));
                bsList.add(bs);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error getBussinessAsignList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setData(bsList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static String getBussinessName(String bis_id) {
        logger.info("getBussinessName method Call..........");
        String bisName = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ResultSet rs = dbCon.search(con, "SELECT BIS_ID,"
                    + "BIS_NAME FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID = " + bis_id + "");

            while (rs.next()) {
                bisName = rs.getString("BIS_NAME");
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessName......:" + ex.toString());
        }
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return bisName;
    }
    
    
    public static MessageWrapper getUserList(int bisId, int userCnt) {
        logger.info("getBussinessStructure method Call..........");
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String where = "WHERE B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID   "
                    + " AND B.STATUS = 1    "
                    + " AND     B.BIS_ID=B.BIS_ID AND T.BIS_STRU_TYPE_ID = 3    ";

            String BisId = "  ";
            if (bisId != 0) {
                where += "  AND B.BIS_ID=" + bisId + "  ";
            }

            if (userCnt != 0) {
                where += " AND (SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID )>0";
            }

            ResultSet rs = dbCon.search(con, "SELECT B.BIS_ID,"
                    + "B.BIS_NAME,"
                    + "B.BIS_DESC,"
                    + "B.PARENT_BIS_ID,"
                    + "B.CATEGORY_1,"
                    + "B.CATEGORY_2,"
                    + "B.BIS_STRU_TYPE_ID,"
                    + "B.LONGITUDE,"
                    + "B.LATITUDE,"
                    + "B.ADDRESS,"
                    + "B.CONTACT_NO,"
                    + "B.SHOW_IN_MAP_FLAG,"
                    + "B.STATUS,"
                  //  + "(SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID ) CNT,"
                    + "B.ERP_CODE,B.PO_TITLE,B.CHQ_TITLE,B.ESSD_ACC_NO,B.ESSD_PCT,B.INVOICE_NO  "
                    + "FROM SD_BUSINESS_STRUCTURES B,SD_BIS_STRU_TYPES T  "
                    + where
                    + "ORDER BY B.BIS_ID ASC");


            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                bs.setBisId(rs.getInt("BIS_ID"));;
                bs.setBisName(rs.getString("BIS_NAME"));
                bs.setBisDesc(rs.getString("BIS_DESC"));
                bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                bs.setCategory1(rs.getInt("CATEGORY_1"));
                bs.setCategory2(rs.getInt("CATEGORY_2"));
                bs.setStructType(rs.getInt("BIS_STRU_TYPE_ID"));
                bs.setLogitude(rs.getString("LONGITUDE"));
                bs.setLatitude(rs.getString("LATITUDE"));
                bs.setAddress(rs.getString("ADDRESS"));
                bs.setTeleNumber(rs.getString("CONTACT_NO"));
                bs.setMapFlag(rs.getInt("SHOW_IN_MAP_FLAG"));
                bs.setStatus(rs.getInt("STATUS"));
             //   bs.setBisUserCount(rs.getInt("CNT"));
                bs.setErpCode(rs.getString("ERP_CODE"));
                bs.setPoTitle(rs.getString("PO_TITLE"));
                bs.setCheqTitle(rs.getString("CHQ_TITLE"));
                bs.setEssdAcntNo(rs.getString("ESSD_ACC_NO"));
                bs.setEddsPct(rs.getString("ESSD_PCT"));
                bs.setInvoiceNo(rs.getString("INVOICE_NO"));
                bsList.add(bs);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessStruture......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setData(bsList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

}
