/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ImeiAdjusment;
import org.dms.ws.singer.entities.ImportDebitNoteDetails;

/**
 *
 * @author SDU
 */
public class ImeiAdjusmentController {

    public static MessageWrapper getImeiAdjusmentDetails(int seqno,
            String debitnoteno,
            String newimeino,
            String imeino,
            String customercode,
            String shopcode,
            int modleno,
            int bisid,
            String orderno,
            double salesprice,
            int status,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getImeiAdjusmentDetails Method Call....................");
        ArrayList<ImeiAdjusment> imeAdujstList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            //  order = DBMapper.
            String seqNo = (seqno == 0 ? "" : "AND SEQ_NO = '" + seqno + "'");
            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(DEBIT_NOTE_NO,'AA') = NVL(DEBIT_NOTE_NO,'AA')" : "UPPER(DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String newImeiNo = (newimeino.equalsIgnoreCase("all") ? "NVL(NEW_IMEI_NO,'AA') = NVL(NEW_IMEI_NO,'AA')" : "UPPER(NEW_IMEI_NO) LIKE UPPER('%" + newimeino + "%')");
            String imeiNo = (imeino.equalsIgnoreCase("all") ? "NVL(IMEI_NO,'AA') = NVL(IMEI_NO,'AA')" : "UPPER(IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String customerCode = (customercode.equalsIgnoreCase("all") ? "NVL(CUSTOMER_CODE,'AA') = NVL(CUSTOMER_CODE,'AA')" : "UPPER(CUSTOMER_CODE) LIKE UPPER('%" + customercode + "%')");
            String shopCode = (shopcode.equalsIgnoreCase("all") ? "NVL(SHOP_CODE,'AA') = NVL(SHOP_CODE,'AA')" : "UPPER(SHOP_CODE) LIKE UPPER('%" + shopcode + "%')");
            String modleNo = (modleno == 0 ? "" : "AND MODEL_NO = '" + modleno + "'");
            String bisId = (bisid == 0 ? "" : "AND BIS_ID = '" + bisid + "'");
            String orderNo = (orderno.equalsIgnoreCase("all") ? "NVL(ORDER_NO,'AA') = NVL(ORDER_NO,'AA')" : "UPPER(ORDER_NO) LIKE UPPER('%" + orderno + "%')");
            String salesPrice = (salesprice == 0 ? "" : "AND SALES_PRICE = '" + salesprice + "' ");
            String Status = (status == 0 ? "" : "AND STATUS = '" + status + "' ");

            String seletColunm = "SEQ_NO,DEBIT_NOTE_NO,"
                    + "NEW_IMEI_NO,IMEI_NO,"
                    + "CUSTOMER_CODE,SHOP_CODE,"
                    + "MODEL_NO,BIS_ID,"
                    + "ORDER_NO,SALES_PRICE,"
                    + "STATUS,SITE";

            String whereClous = " " + debitNoteNo + " "
                    + " " + seqNo + " "
                    + " AND " + newImeiNo + " "
                    + " AND " + imeiNo + " "
                    + " AND " + customerCode + " "
                    + " AND " + shopCode + " "
                    + " " + modleNo + " "
                    + " " + bisId + " "
                    + " AND " + orderNo + " "
                    + " " + salesPrice + " "
                    + " " + Status + " ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_IMEI_ADJUSTMENTS WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY SEQ_NO) RN "
                    + " FROM SD_IMEI_ADJUSTMENTS WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                ImeiAdjusment ia = new ImeiAdjusment();
                totalrecode = rs.getInt("CNT");
                ia.setSeqNo(rs.getInt("SEQ_NO"));
                ia.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                ia.setNewImeiNo(rs.getString("NEW_IMEI_NO"));
                ia.setImeiNo(rs.getString("IMEI_NO"));
                ia.setCustomerCode(rs.getString("CUSTOMER_CODE"));
                ia.setShopCode(rs.getString("SHOP_CODE"));
                ia.setModleNo(rs.getInt("MODEL_NO"));
                ia.setBisId(rs.getInt("BIS_ID"));
                ia.setOrderNo(rs.getString("ORDER_NO"));
                ia.setSalerPrice(rs.getDouble("SALES_PRICE"));
                ia.setStatus(rs.getInt("STATUS"));
                ia.setSite(rs.getString("SITE"));
                imeAdujstList.add(ia);
            }

        } catch (Exception ex) {
            logger.info("Error getImeiAdjusmentDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeAdujstList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addImeiAdjusment(ImeiAdjusment imeiadjusment_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addImeiAdjusment Method Call.............................");
        int result = 0;
        int imei_cnt = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String debitNoteDetialsColumns = "DEBIT_NOTE_NO,IMEI_NO,"
                    + "CUSTOMER_CODE,SHOP_CODE,"
                    + "MODEL_NO,BIS_ID,"
                    + "ORDER_NO,SALES_PRICE,"
                    + "STATUS,USER_INSERTED,"
                    + "DATE_INSERTED,REMARKS,SITE";

            String debitNoteDetialsValue = "?,?,?,?,?,?,?,?,1,?,SYSDATE,?,?";

            String adjustImeiColumns = "SEQ_NO,"
                    + "DEBIT_NOTE_NO,"
                    + "NEW_IMEI_NO,"
                    + "IMEI_NO,"
                    + "CUSTOMER_CODE,"
                    + "SHOP_CODE,"
                    + "MODEL_NO,"
                    + "BIS_ID,"
                    + "ORDER_NO,"
                    + "SALES_PRICE,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED,SITE";

            int max_imei = 0;
            ResultSet rs_max_imei = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_IMEI_ADJUSTMENTS");

            while (rs_max_imei.next()) {
                max_imei = rs_max_imei.getInt("MAX");
            }

            String adjustImeiValue = "?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,?";

            try {

                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_DEBIT_NOTE_DETAIL "
                        + "WHERE IMEI_NO = '" + imeiadjusment_info.getImeiNo() + "' "
                        + "AND DEBIT_NOTE_NO = '" + imeiadjusment_info.getDebitNoteNo() + "'");

                while (rs_cnt.next()) {
                    imei_cnt = rs_cnt.getInt("CNT");
                }
                if (imei_cnt > 0) {
                    PreparedStatement ps_up_debit = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL SET IMEI_NO = ?,"
                            + "CUSTOMER_CODE = ?,"
                            + "SHOP_CODE= ?,"
                            + "MODEL_NO=?,"
                            + "BIS_ID=?,"
                            + "ORDER_NO=?,"
                            + "SALES_PRICE=?,"
                            + "STATUS = 1,"
                            + "REMARKS=?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE,"
                            + "SITE = ? "
                            + "WHERE DEBIT_NOTE_NO = ?");
                    ps_up_debit.setString(1, imeiadjusment_info.getNewImeiNo());
                    ps_up_debit.setString(2, imeiadjusment_info.getCustomerCode());
                    ps_up_debit.setString(3, imeiadjusment_info.getShopCode());
                    ps_up_debit.setInt(4, imeiadjusment_info.getModleNo());
                    ps_up_debit.setInt(5, imeiadjusment_info.getBisId());
                    ps_up_debit.setString(6, imeiadjusment_info.getOrderNo());
                    ps_up_debit.setDouble(7, imeiadjusment_info.getSalerPrice());
                    ps_up_debit.setString(8, imeiadjusment_info.getLocation());
                    ps_up_debit.setString(9, user_id);
                    ps_up_debit.setString(10, imeiadjusment_info.getSite());
                    ps_up_debit.setString(11, imeiadjusment_info.getDebitNoteNo());
                    ps_up_debit.executeUpdate();
                    logger.info("Debit Note Details Updateded.....");

                    PreparedStatement ps_in_imei = dbCon.prepare(con, "INSERT INTO SD_IMEI_ADJUSTMENTS(" + adjustImeiColumns + ") "
                            + "VALUES(" + adjustImeiValue + ")");

                    ps_in_imei.setInt(1, max_imei + 1);
                    ps_in_imei.setString(2, imeiadjusment_info.getDebitNoteNo());
                    ps_in_imei.setString(3, imeiadjusment_info.getNewImeiNo());
                    ps_in_imei.setString(4, imeiadjusment_info.getImeiNo());
                    ps_in_imei.setString(5, imeiadjusment_info.getCustomerCode());
                    ps_in_imei.setString(6, imeiadjusment_info.getShopCode());
                    ps_in_imei.setInt(7, imeiadjusment_info.getModleNo());
                    ps_in_imei.setInt(8, imeiadjusment_info.getBisId());
                    ps_in_imei.setString(9, imeiadjusment_info.getOrderNo());
                    ps_in_imei.setDouble(10, imeiadjusment_info.getSalerPrice());
                    ps_in_imei.setInt(11, imeiadjusment_info.getStatus());
                    ps_in_imei.setString(12, user_id);
                    ps_in_imei.setString(13, imeiadjusment_info.getLocation());
                    ps_in_imei.executeUpdate();
                    logger.info("ImeiAdjusment Details Inseted.....");
                } else {

                    PreparedStatement ps_in_debit = dbCon.prepare(con, "INSERT INTO SD_DEBIT_NOTE_DETAIL(" + debitNoteDetialsColumns + ") "
                            + "VALUES(" + debitNoteDetialsValue + ")");

                    ps_in_debit.setString(1, imeiadjusment_info.getDebitNoteNo());
                    ps_in_debit.setString(2, imeiadjusment_info.getNewImeiNo());
                    ps_in_debit.setString(3, imeiadjusment_info.getCustomerCode());
                    ps_in_debit.setString(4, imeiadjusment_info.getShopCode());
                    ps_in_debit.setInt(5, imeiadjusment_info.getModleNo());
                    ps_in_debit.setInt(6, imeiadjusment_info.getBisId());
                    ps_in_debit.setString(7, imeiadjusment_info.getOrderNo());
                    ps_in_debit.setDouble(8, imeiadjusment_info.getSalerPrice());
                    ps_in_debit.setString(9, user_id);
                    ps_in_debit.setString(10, imeiadjusment_info.getLocation());
                    ps_in_debit.setString(11, imeiadjusment_info.getSite());
                    ps_in_debit.executeUpdate();
                    logger.info("Debit Note Details Inseted.....");

                    PreparedStatement ps_in_imei = dbCon.prepare(con, "INSERT INTO SD_IMEI_ADJUSTMENTS(" + adjustImeiColumns + ") "
                            + "VALUES(" + adjustImeiValue + ")");

                    ps_in_imei.setInt(1, max_imei + 1);
                    ps_in_imei.setString(2, imeiadjusment_info.getDebitNoteNo());
                    ps_in_imei.setString(3, imeiadjusment_info.getNewImeiNo());
                    ps_in_imei.setString(4, imeiadjusment_info.getImeiNo());
                    ps_in_imei.setString(5, imeiadjusment_info.getCustomerCode());
                    ps_in_imei.setString(6, imeiadjusment_info.getShopCode());
                    ps_in_imei.setInt(7, imeiadjusment_info.getModleNo());
                    ps_in_imei.setInt(8, imeiadjusment_info.getBisId());
                    ps_in_imei.setString(9, imeiadjusment_info.getOrderNo());
                    ps_in_imei.setDouble(10, imeiadjusment_info.getSalerPrice());
                    ps_in_imei.setInt(11, imeiadjusment_info.getStatus());
                    ps_in_imei.setString(12, user_id);
                    ps_in_imei.setString(13, imeiadjusment_info.getSite());
                    ps_in_imei.executeUpdate();
                    logger.info("ImeiAdjusment Details Inseted.....");
                }

                result = 1;
            } catch (Exception e) {
                logger.info("Error Imei Adjusment Details Insert....." + e.toString());
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error Imei Adjusment Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("IMEI Adjustment Insert Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("IMEI Adjustment Insert Fail");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editImeiAdjusment(ImeiAdjusment imeiadjusment_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editImeiAdjusment Method Call............");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_debit = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL "
                    + " SET STATUS = ?,"
                    + " USER_MODIFIED =?,"
                    + " DATE_MODIFIED = SYSDATE "
                    + " WHERE IMEI_NO = ? ");

            
            System.out.println(imeiadjusment_info.getOldImei());
            
            
            ps_up_debit.setInt(1, 9);
            ps_up_debit.setString(2, user_id);
            ps_up_debit.setString(3, imeiadjusment_info.getImeiNo());
            ps_up_debit.executeUpdate();

            
            
            String adjustImeiColumns = "SEQ_NO,"
                    + "DEBIT_NOTE_NO,"
                    + "NEW_IMEI_NO,"
                    + "IMEI_NO,"
                    + "CUSTOMER_CODE,"
                    + "SHOP_CODE,"
                    + "MODEL_NO,"
                    + "BIS_ID,"
                    + "ORDER_NO,"
                    + "SALES_PRICE,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED,"
                    + "SITE";

            int max_imei = 0;
            ResultSet rs_max_imei = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_IMEI_ADJUSTMENTS");

            while (rs_max_imei.next()) {
                max_imei = rs_max_imei.getInt("MAX");
            }

            String adjustImeiValue = "?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,?";

            PreparedStatement ps_in_imei = dbCon.prepare(con, "INSERT INTO SD_IMEI_ADJUSTMENTS(" + adjustImeiColumns + ") "
                    + "VALUES(" + adjustImeiValue + ")");

            ps_in_imei.setInt(1, max_imei + 1);
            ps_in_imei.setString(2, imeiadjusment_info.getDebitNoteNo());
            ps_in_imei.setString(3, imeiadjusment_info.getImeiNo());
            ps_in_imei.setString(4, imeiadjusment_info.getOldImei());
            ps_in_imei.setString(5, imeiadjusment_info.getCustomerCode());
            ps_in_imei.setString(6, imeiadjusment_info.getShopCode());
            ps_in_imei.setInt(7, imeiadjusment_info.getModleNo());
            ps_in_imei.setInt(8, imeiadjusment_info.getBisId());
            ps_in_imei.setString(9, imeiadjusment_info.getOrderNo());
            ps_in_imei.setDouble(10, imeiadjusment_info.getSalerPrice());
            ps_in_imei.setInt(11, 1);
            ps_in_imei.setString(12, user_id);
            ps_in_imei.setString(13, imeiadjusment_info.getSite());
            ps_in_imei.executeUpdate();

            
            PreparedStatement ps_update_AcceptDebitNote = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL SET "
                    + "IMEI_NO=? "
                    + "WHERE IMEI_NO=? "
                    + "AND DEBIT_NOTE_NO = ?");
            
            System.out.println(imeiadjusment_info.getDebitNoteNo());

            ps_update_AcceptDebitNote.setString(1, imeiadjusment_info.getImeiNo());
            ps_update_AcceptDebitNote.setString(2, imeiadjusment_info.getOldImei());
            ps_update_AcceptDebitNote.setString(3, imeiadjusment_info.getDebitNoteNo());
            ps_update_AcceptDebitNote.executeUpdate();

            result = 1;

        } catch (Exception ex) {
            logger.info("Error editImeiAdjusment Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Update Successfully in IMEI Adjustment");
            logger.info("Update Successsfully in ImeiAdjusment.............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Update IMEI Adjustment");
            logger.info("Getting Error Update ImeiAdjusment............");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getImeiAdjusmentDetail(String imeino) {
        logger.info("getImeiAdjusmentDetail Method Call....................");

        ArrayList<ImeiAdjusment> imeAdujstList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "M.DEBIT_NOTE_NO,"
                    + "M.IMEI_NO,"
                    + "D.CUSTOMER_CODE,"
                    + "D.SHOP_CODE,"
                    + "M.MODEL_NO,"
                    + "M.BIS_ID,"
                    + "D.ORDER_NO,"
                    + "M.SALES_PRICE,"
                    + "M.LOCATION,"
                    + "D.SITE";

            String whereClous = "D.IMEI_NO = M.IMEI_NO AND M.IMEI_NO = '" + imeino + "'";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM SD_DEBIT_NOTE_DETAIL D,SD_DEVICE_MASTER M WHERE " + whereClous + " ");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                ImeiAdjusment ia = new ImeiAdjusment();
                totalrecode = rs.getInt(1);
                ia.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                ia.setImeiNo(rs.getString("IMEI_NO"));
                ia.setCustomerCode(rs.getString("CUSTOMER_CODE"));
                ia.setShopCode(rs.getString("SHOP_CODE"));
                ia.setModleNo(rs.getInt("MODEL_NO"));
                ia.setBisId(rs.getInt("BIS_ID"));
                ia.setOrderNo(rs.getString("ORDER_NO"));
                ia.setSalerPrice(rs.getInt("SALES_PRICE"));
                ia.setLocation(rs.getString("LOCATION"));
                ia.setSite(rs.getString("SITE"));
                imeAdujstList.add(ia);
            }

        } catch (Exception ex) {
            logger.info("Error getImeiAdjusmentDetail Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeAdujstList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

/////////////////Get IMEIs which are not accepted to add IMEI adjustment//////////////////////////////////    
    public static MessageWrapper getIMEIsToAdjust(String imei, int bisId) {
        logger.info("getIMEIsToAdjust Method Call...........");

        ArrayList<ImportDebitNoteDetails> imeiDetailList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String ImeiNo = (imei.equalsIgnoreCase("0") ? " " : " AND D.IMEI_NO='" + imei + "' ");
            String BisId = (bisId == 0 ? " " : " AND D.BIS_ID= '" + bisId + "' ");

            String selectCls = " D.DEBIT_NOTE_NO, "
                    + "D.IMEI_NO, "
                    + "D.CUSTOMER_CODE, "
                    + "D.SHOP_CODE, "
                    + "D.MODEL_NO, "
                    + "M.MODEL_DESCRIPTION, "
                    + "D.BIS_ID, "
                    + "D.ORDER_NO, "
                    + "D.SALES_PRICE, "
                    + "TO_CHAR(D.DATE_ACCEPTED,'YYYY/MM/DD') AS DATE_ACCEPTED, "
                    + "D.STATUS, "
                    + "D.REMARKS, "
                    + "D.SITE ";
            String fromCls = " SD_DEBIT_NOTE_DETAIL D, SD_MODEL_LISTS M ";
            String whereCls = " D.MODEL_NO=M.MODEL_NO "
                    + " AND  D.STATUS=1 "
                    + ImeiNo
                    + BisId;

            String qry = " SELECT " + selectCls + " "
                    + " FROM " + fromCls + " "
                    + " WHERE " + whereCls + " ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            while (rs_master_cnt.next()) {
                ImportDebitNoteDetails imie = new ImportDebitNoteDetails();
                imie.setDebitNoteNo(rs_master_cnt.getString("DEBIT_NOTE_NO"));
                imie.setImeiNo(rs_master_cnt.getString("IMEI_NO"));
                imie.setCustomerCode(rs_master_cnt.getString("CUSTOMER_CODE"));
                imie.setShopCode(rs_master_cnt.getString("SHOP_CODE"));
                imie.setModleNo(rs_master_cnt.getInt("MODEL_NO"));
                imie.setModleDesc(rs_master_cnt.getString("MODEL_DESCRIPTION"));
                imie.setBisId(rs_master_cnt.getInt("BIS_ID"));
                imie.setOrderNo(rs_master_cnt.getString("ORDER_NO"));
                imie.setSalerPrice(rs_master_cnt.getDouble("SALES_PRICE"));
                imie.setDateAccepted(rs_master_cnt.getString("DATE_ACCEPTED"));
                imie.setStatus(rs_master_cnt.getInt("STATUS"));
                imie.setRemarks(rs_master_cnt.getString("REMARKS"));
                imie.setSite(rs_master_cnt.getString("SITE"));

                imeiDetailList.add(imie);
            }

        } catch (Exception ex) {
            logger.info("Error in getIMEIsToAdjust  " + ex.getMessage());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(imeiDetailList.size());
        mw.setData(imeiDetailList);

        dbCon.ConectionClose(con);
        return mw;

    }

////////////////////////Verify Adjust IMEI webservice//////////////////////////////////////
    public static MessageWrapper verfiedImeiDetails(String imei) {
        logger.info("verfiedImeiDetails Method Call...........");

        ArrayList<ImportDebitNoteDetails> imeiDetailList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String ImeiNo = (imei.equalsIgnoreCase("0") ? " " : " AND D.IMEI_NO='" + imei + "' ");

            String selectCls = " D.DEBIT_NOTE_NO, "
                    + "D.IMEI_NO, "
                    + "D.CUSTOMER_CODE, "
                    + "D.SHOP_CODE, "
                    + "D.MODEL_NO, "
                    + "M.MODEL_DESCRIPTION, "
                    + "D.BIS_ID, "
                    + "D.ORDER_NO, "
                    + "D.SALES_PRICE, "
                    + "TO_CHAR(D.DATE_ACCEPTED,'YYYY/MM/DD') AS DATE_ACCEPTED, "
                    + "D.STATUS, "
                    + "D.REMARKS, "
                    + "D.SITE ";
            String fromCls = " SD_DEBIT_NOTE_DETAIL D, SD_MODEL_LISTS M ";
            String whereCls = " D.MODEL_NO=M.MODEL_NO "
                    + " AND  D.STATUS=1 "
                    + ImeiNo;

            String qry = " SELECT " + selectCls + " "
                    + " FROM " + fromCls + " "
                    + " WHERE " + whereCls + " ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            while (rs_master_cnt.next()) {
                ImportDebitNoteDetails imie = new ImportDebitNoteDetails();
                imie.setDebitNoteNo(rs_master_cnt.getString("DEBIT_NOTE_NO"));
                imie.setImeiNo(rs_master_cnt.getString("IMEI_NO"));
                imie.setCustomerCode(rs_master_cnt.getString("CUSTOMER_CODE"));
                imie.setShopCode(rs_master_cnt.getString("SHOP_CODE"));
                imie.setModleNo(rs_master_cnt.getInt("MODEL_NO"));
                imie.setModleDesc(rs_master_cnt.getString("MODEL_DESCRIPTION"));
                imie.setBisId(rs_master_cnt.getInt("BIS_ID"));
                imie.setOrderNo(rs_master_cnt.getString("ORDER_NO"));
                imie.setSalerPrice(rs_master_cnt.getDouble("SALES_PRICE"));
                imie.setDateAccepted(rs_master_cnt.getString("DATE_ACCEPTED"));
                imie.setStatus(rs_master_cnt.getInt("STATUS"));
                imie.setRemarks(rs_master_cnt.getString("REMARKS"));
                imie.setSite(rs_master_cnt.getString("SITE"));

                imeiDetailList.add(imie);
            }

        } catch (Exception ex) {
            logger.info("Error in getIMEIsToAdjust  " + ex.getMessage());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(imeiDetailList.size());
        mw.setData(imeiDetailList);

        dbCon.ConectionClose(con);
        return mw;

    }

}
