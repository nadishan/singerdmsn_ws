/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.WorkOrder;

/**
 *
 * @author SDU
 */
public class WarrantyMaintainanceController {
    
    
    public static MessageWrapper getWorkOrderMaintainDetail(String workorderno) {

        logger.info("getWorkOrderMaintainDetail Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "W.WORK_ORDER_NO,W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,W.TELEPHONE_NO,"
                    + "W.EMAIL,W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,W.PRODUCT,"
                    + "W.BRAND,W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "W.DATE_OF_SALE,W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,W.REMARKS,"
                    + "W.REPAIR_STATUS,W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,W.COURIER_NO,"
                    + "W.DELIVERY_DATE,W.GATE_PASS,"
                    + "W.STATUS,TRUNC(SYSDATE - W.DATE_INSERTED) DELAY_DATE,"
                    + "W.DATE_INSERTED,"
                    + "L.LEVEL_NAME,TO_CHAR(R.DATE_INSERTED,'YYYY/MM/DD') DATE_OF_SALES,"
                    + "R.STATUS WAR_STATUS";
            
            String tables = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_WARRENTY_REGISTRATION R";

            String whereClous = "W.LEVEL_ID = L.LEVEL_ID (+) "
                    + "AND W.IMEI_NO = R.IMEI_NO"
                    + "AND W.WORK_ORDER_NO = '"+workorderno+"'";

            ResultSet rs = dbCon.search(con,"SELECT "+seletColunm+" FROM "+tables+" "
                    + "WHERE "+whereClous+" ");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                totalrecode = 1;
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setProduct(rs.getString("PRODUCT"));
                wo.setBrand(rs.getString("BRAND"));
                wo.setModleNo(rs.getInt("MODEL_NO"));
                wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                wo.setDefectNo(rs.getString("DEFECTS"));
                wo.setRccReference(rs.getString("RCC_REFERENCE"));
                wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                wo.setBisId(rs.getInt("BIS_ID"));
                wo.setSchemeId(rs.getInt("SCHEME_ID"));
                wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                wo.setTecnician(rs.getString("TECHNICIAN"));
                wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                wo.setLevelId(rs.getInt("LEVEL_ID"));
                wo.setRemarks(rs.getString("REMARKS"));
                wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                wo.setCourierNo(rs.getString("COURIER_NO"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setGatePass(rs.getString("GATE_PASS"));
                wo.setStatus(rs.getInt("STATUS"));
                wo.setDelayDate(rs.getString("DELAY_DATE"));
                wo.setRepairDate(rs.getString("DATE_INSERTED"));
                wo.setRepairLevelDesc(rs.getString("LEVEL_NAME"));
                wo.setSaleDate(rs.getString("DATE_OF_SALES"));
                wo.setWarrantyStatus(rs.getInt("WAR_STATUS"));
                workOrderList.add(wo);
            }
        } catch (Exception ex) {
            logger.info("Error getWorkOrderMaintainDetail Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
    
}
