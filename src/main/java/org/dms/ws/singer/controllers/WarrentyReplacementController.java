/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import org.dms.ws.singer.controllers.DbCon;
import org.dms.ws.singer.entities.WarrenyReplacement;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WarrentyReplacementController {

    public static MessageWrapper getWarrentyReplacement(String work_order_no,
            String old_imei,
            String new_imei,
            String ex_ref_no,
            String fromdate,
            String todate,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWarrentyReplacement Method Call....................");
        ArrayList<WarrenyReplacement> warrentReplacementList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.warrentyReplacement(order);

            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
            String oldImei = (old_imei.equalsIgnoreCase("all") ? "NVL(W.OLD_IMEI,'AA') = NVL(W.OLD_IMEI,'AA')" : "UPPER(W.OLD_IMEI) LIKE UPPER('%" + old_imei + "%')");
            String newImei = (new_imei.equalsIgnoreCase("all") ? "NVL(W.NEW_IMEI,'AA') = NVL(W.NEW_IMEI,'AA')" : "UPPER(W.NEW_IMEI) LIKE UPPER('%" + new_imei + "%')");
            String exRefNo = (ex_ref_no.equalsIgnoreCase("all") ? "NVL(W.EX_REF_NO,'AA') = NVL(W.EX_REF_NO,'AA')" : "UPPER(W.EX_REF_NO) LIKE UPPER('%" + ex_ref_no + "%')");
            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "W.SEQ_NO SEQ_NO,W.WORK_ORDER_NO WORK_ORDER_NO,"
                    + "W.OLD_IMEI OLD_IMEI,"
                    + "W.NEW_IMEI NEW_IMEI,"
                    + "W.EX_REF_NO EX_REF_NO,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') DATE_INSERTED,"
                    + "M.MODEL_DESCRIPTION MODEL_DESCRIPTION,"
                    + "R.CUSTOMER_NAME CUS_NAME,"
                    + "(TRUNC(SYSDATE - R.DATE_INSERTED)) REMAIN_DATE";

            String whereClous = " W.OLD_IMEI =D.IMEI_NO (+)"
                    + " AND D.MODEL_NO = M.MODEL_NO  (+)"
                    + " AND W.OLD_IMEI = R.IMEI_NO (+)"
                    + " AND " + workOrderNo + " "
                    + " AND " + oldImei + " "
                    + " AND " + newImei + " "
                    + " AND " + exRefNo + "  "
                    + " AND W.STATUS != 9 "
                    + " " + Range + " ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WARRENTY_REPLACEMENT W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRENTY_REGISTRATION R WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_WARRENTY_REPLACEMENT W,SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRENTY_REGISTRATION R WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WarrenyReplacement wr = new WarrenyReplacement();
                totalrecode = rs.getInt("CNT");
                wr.setSeqNo(rs.getInt("SEQ_NO"));
                wr.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wr.setOldImei(rs.getString("OLD_IMEI"));
                wr.setNewImei(rs.getString("NEW_IMEI"));
                wr.setExchangeReferenceNo(rs.getString("EX_REF_NO"));
                wr.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                wr.setReplaceDate(rs.getString("DATE_INSERTED"));
                wr.setCusName(rs.getString("CUS_NAME"));
                wr.setReaminDate(rs.getString("REMAIN_DATE"));
                warrentReplacementList.add(wr);
            }

        } catch (Exception ex) {
            logger.info("Error getWarrentyReplacement Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(warrentReplacementList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addWarrentyReplacement(WarrenyReplacement wr_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("addWarrentyReplacement Mehtod Call.......");
        int result = 0;
        String cus_id = null;
        int seq_no = 0;
        int exist_cnt = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_exist = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WARRENTY_REPLACEMENT "
                    + "WHERE WORK_ORDER_NO = '" + wr_info.getWorkOrderNo() + "' "
                    + "AND STATUS = 1");

            while (rs_exist.next()) {
                exist_cnt = rs_exist.getInt("CNT");
            }
            if (exist_cnt > 0) {
                result = 2;
            } else {
                ResultSet rs_max_seq = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM  SD_WARRENTY_REPLACEMENT");

                while (rs_max_seq.next()) {
                    seq_no = rs_max_seq.getInt("MAX");
                }

                String column = "SEQ_NO,WORK_ORDER_NO,OLD_IMEI,"
                        + "NEW_IMEI,EX_REF_NO,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED,"
                        + "STATUS";

                String values = "?,?,?,?,?,?,SYSDATE,1";

                PreparedStatement ps_in_wr = dbCon.prepare(con, "INSERT INTO SD_WARRENTY_REPLACEMENT(" + column + ") VALUES(" + values + ")");
                seq_no = seq_no + 1;
                ps_in_wr.setInt(1, seq_no);
                ps_in_wr.setString(2, wr_info.getWorkOrderNo());
                ps_in_wr.setString(3, wr_info.getOldImei());
                ps_in_wr.setString(4, wr_info.getNewImei());
                ps_in_wr.setString(5, wr_info.getExchangeReferenceNo());
                ps_in_wr.setString(6, user_id);
                ps_in_wr.executeUpdate();

                int distributor_id = 0;
                ResultSet rs_get_distributor = dbCon.search(con, "SELECT PARENT_BIS_ID "
                        + "FROM SD_BUSINESS_STRUCTURES "
                        + "WHERE BIS_ID = " + wr_info.getBisId() + "");

                while (rs_get_distributor.next()) {
                    distributor_id = rs_get_distributor.getInt("PARENT_BIS_ID");
                }

                int next_retr_no = 0;
                ResultSet rs_max_ref = dbCon.search(con, "SELECT NVL(MAX(REPL_RETURN_NO),0) MAX FROM SD_REPLACEMEN_RETURN");

                while (rs_max_ref.next()) {
                    next_retr_no = rs_max_ref.getInt("MAX");
                }
                next_retr_no = next_retr_no + 1;

                String insertColumnsReplasement = "REPL_RETURN_NO,"
                        + "EXG_REF_NO,"
                        + "DISTRIBUTER_ID,"
                        + "IMEI_NO,"
                        + "RETURN_DATE,"
                        + "STATUS,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED";

                String insertValueReplasement = "?,?,?,?,SYSDATE,1,?,SYSDATE";

                PreparedStatement ps_in_return = dbCon.prepare(con, "INSERT INTO SD_REPLACEMEN_RETURN(" + insertColumnsReplasement + ") VALUES(" + insertValueReplasement + ")");

                ps_in_return.setInt(1, next_retr_no);
                ps_in_return.setString(2, wr_info.getExchangeReferenceNo());
                ps_in_return.setInt(3, distributor_id);
                ps_in_return.setString(4, wr_info.getOldImei());
                ps_in_return.setString(5, user_id);
                ps_in_return.executeUpdate();
                logger.info("Replacement Details Inseted.....");

                ////////////////////Insert data to IMEI Replacement table which is used to identify when and what imei is replaced to what imei under which work order
                String insertQry = " INSERT INTO SD_IMEI_REPLACEMENT (WORK_ORDER_NO, "
                        + "OLD_IMEI, "
                        + "NEW_IMEI, "
                        + "STATUS, "
                        + "USER_INSERTED, "
                        + "DATE_INSERTED) "
                        + " VALUES (?, ?, ?, 1, ?, SYSDATE)";

                PreparedStatement insert_ImeiReplace = dbCon.prepare(con, insertQry);
                insert_ImeiReplace.setString(1, wr_info.getWorkOrderNo());
                insert_ImeiReplace.setString(2, wr_info.getOldImei());
                insert_ImeiReplace.setString(3, wr_info.getNewImei());
                insert_ImeiReplace.setString(4, user_id);
                insert_ImeiReplace.executeUpdate();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
                ResultSet rs_cnt = dbCon.search(con, "SELECT CUSTOMER_NIC "
                        + "FROM SD_WARRENTY_REGISTRATION "
                        + "WHERE IMEI_NO = '" + wr_info.getOldImei() + "'");

                while (rs_cnt.next()) {
                    cus_id = rs_cnt.getString("CUSTOMER_NIC");
                }

                dbCon.save(con, " UPDATE SD_DEVICE_MASTER "
                        + "SET STATUS = 1, "
                        + " BIS_ID = " + wr_info.getBisId() + ",    "
                        + " USER_MODIFIED = '" + user_id + "', "
                        + " DATE_MODIFIED = SYSDATE "
                        + " WHERE IMEI_NO = '" + wr_info.getOldImei() + "'");

                dbCon.save(con, " UPDATE SD_DEVICE_MASTER "
                        + " SET CUSTOMER_ID ='" + cus_id + "',"
                        + " STATUS = 3, "
                        + " USER_MODIFIED = '" + user_id + "',"
                        + " DATE_MODIFIED = SYSDATE "
                        + " WHERE IMEI_NO = '" + wr_info.getNewImei() + "'");

////////////////////////////////Update Warrenty Registration table and replace old imei with new imei. So, Warrenty period of new imei is same as the warranty period of old imei.
                String warrantyReg_UpdateQry = " UPDATE SD_WARRENTY_REGISTRATION    "
                        + "                      SET    IMEI_NO = '" + wr_info.getNewImei() + "', "
                        + "                             USER_MODIFIED = '" + user_id + "',   "
                        + "                             DATE_MODIFIED = SYSDATE "
                        + "                      WHERE  IMEI_NO = '" + wr_info.getOldImei() + "'   ";
                dbCon.save(con, warrantyReg_UpdateQry);

                result = 1;
            }

        } catch (Exception ex) {
            logger.info("Error in addWarrentyReplacement Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Replacement Insert Successfully");
            logger.info("Warrernty Replacement Insert Succesfully..........");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Already Exist Record for this Work Order No");
            logger.info("Already Exist Record for this Work Order No............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Replacement Insert");
            logger.info("Error in Warrernty Replacement Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper cancleWarrentyReplacement(WarrenyReplacement wr_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("cancleWarrentyReplacement Mehtod Call.......");
        int result = 0;
        String cus_id = null;
        try {

            ResultSet rs_cnt = dbCon.search(con, "SELECT CUSTOMER_NIC "
                    + "FROM SD_WARRENTY_REGISTRATION "
                    + "WHERE IMEI_NO = '" + wr_info.getNewImei() + "'");

            while (rs_cnt.next()) {
                cus_id = rs_cnt.getString("CUSTOMER_NIC");
            }

            dbCon.save(con, "UPDATE SD_WARRENTY_REPLACEMENT "
                    + " SET STATUS = 9, "
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED = SYSDATE "
                    + " WHERE SEQ_NO = " + wr_info.getSeqNo() + "");

            dbCon.save(con, " UPDATE SD_DEVICE_MASTER "
                    + " SET STATUS = 3,"
                    + " CUSTOMER_ID ='" + cus_id + "',"
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED = SYSDATE "
                    + " WHERE IMEI_NO = '" + wr_info.getOldImei() + "'");

            dbCon.save(con, " UPDATE SD_DEVICE_MASTER "
                    + " SET CUSTOMER_ID = NULL,"
                    + " STATUS = 1 ,"
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED = SYSDATE "
                    + " WHERE IMEI_NO = '" + wr_info.getNewImei() + "'");

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in cancleWarrentyReplacement Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Replacement Cancel Successfully");
            logger.info("Warrernty Replacement Cancle Succesfully..........");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Replacement Cancel");
            logger.info("Error in Warrernty Replacement Cancle................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

}
