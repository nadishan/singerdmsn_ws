/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.MdeCode;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderClose;
import org.dms.ws.singer.utils.DBMapper;
import org.dms.ws.singer.utils.EmaiSendConfirmCode;

/**
 *
 * @author SDU
 */
public class WorkOrderCloseController {

    public static MessageWrapper getWorkOrderClossing(String work_order_no,
            String repair_status,
            String delevery_type,
            String transfer_location,
            String curior_no,
            String delevery_date,
            String gate_pass,
            int bis_id,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getWorkOrderClossing Method Call....................");
        ArrayList<WorkOrderClose> workOrderCloseList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.workOrderClose(order);

            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
            String repairStatus = (repair_status.equalsIgnoreCase("all") ? "NVL(M.DESCRIPTION,'AA') = NVL(M.DESCRIPTION,'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + repair_status + "%')");
            String deleveryType = (delevery_type.equalsIgnoreCase("all") ? "NVL(M1.DESCRIPTION,'AA') = NVL(M1.DESCRIPTION,'AA')" : "UPPER(M1.DESCRIPTION) LIKE UPPER('%" + delevery_type + "%')");
            String location = (transfer_location.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + transfer_location + "%')");
            String curierNo = (curior_no.equalsIgnoreCase("all") ? "NVL(W.COURIER_NO,'AA') = NVL(W.COURIER_NO,'AA')" : "UPPER(W.COURIER_NO) LIKE UPPER('%" + curior_no + "%')");
            String deleveryDate = (delevery_date.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.DELIVERY_DATE = TO_DATE('" + delevery_date + "','YYYY/MM/DD')");
            String gatePass = (gate_pass.equalsIgnoreCase("all") ? "NVL(W.GATE_PASS,'AA') = NVL(W.GATE_PASS,'AA')" : "UPPER(W.GATE_PASS) LIKE UPPER('%" + gate_pass + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORDER_NO DESC" : "ORDER BY " + order + "   " + type + "");
            String bisId = (bis_id == 0 ? "" : " AND W.BIS_ID  = " + bis_id + "");

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "M.DESCRIPTION REAPAIR_STATUS,"
                    + "M1.DESCRIPTION DELEVERY_TYPE,"
                    + "B.BIS_NAME,"
                    + "B1.BIS_NAME AS TRF_LOC ,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.REPAIR_STATUS,"
                    + "W.SERVICE_BIS_ID";

            String whereClous = "W.REPAIR_STATUS = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WO_CLOSE' "
                    + "AND W.DELIVERY_TYPE = M1.CODE (+) "
                    + "AND M1.DOMAIN_NAME = 'DELEVERY_TYPE' "
                    + "AND W.SERVICE_BIS_ID = B.BIS_ID (+) "
                    + "AND W.TRANSFER_LOCATION = B1.BIS_ID (+) "
                    //                    + "AND W.STATUS = 3"
                    + "AND W.STATUS NOT IN (1,2)"
                    + "AND W.STATUS IN (3,4,5,6,7)"
                    + " " + bisId + " "
                    + "AND " + workOrderNo + " "
                    + "AND " + repairStatus + " "
                    + "AND " + deleveryType + " "
                    + "AND " + location + " "
                    + "AND " + curierNo + " "
                    + "AND " + deleveryDate + " "
                    + "AND " + gatePass + "";

            String tables = "SD_WORK_ORDERS W,SD_MDECODE M,SD_MDECODE M1,SD_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES B1 ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderClose wc = new WorkOrderClose();
                totalrecode = rs.getInt("CNT");
                wc.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wc.setRepairSatus(rs.getString("REAPAIR_STATUS"));
                wc.setDeleveryType(rs.getString("DELEVERY_TYPE"));
                wc.setTransferLocation(rs.getString("TRF_LOC"));
                wc.setCourierNo(rs.getString("COURIER_NO"));
                wc.setDeleverDate(rs.getString("DELIVERY_DATE"));
                wc.setGatePass(rs.getString("GATE_PASS"));
                wc.setDeleveryId(rs.getInt("DELIVERY_TYPE"));
                wc.setRepairid(rs.getInt("REPAIR_STATUS"));
                wc.setLocationId(rs.getInt("SERVICE_BIS_ID"));
                workOrderCloseList.add(wc);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderClossing Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderCloseList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getRepairStatus() {

        logger.info("getRepairStatus Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'RP_STATUS'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getRepairStatus Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderCloseStatus() {

        logger.info("getWorkOrderCloseStatus Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'WO_CLOSE'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getWorkOrderCloseStatus Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderEstimateStatus() {

        logger.info("getWorkOrderCloseStatus Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'WO_ESTIMATE'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getWorkOrderCloseStatus Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getDeleveryType() {

        logger.info("getRepairStatus Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'DELEVERY_TYPE'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getRepairStatus Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper verifyWorkOrder(String workorderno) {

        logger.info("verifyWorkOrder Method Call....................");
        int recode_count = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE WORK_ORDER_NO = '" + workorderno + "' "
                    + "AND STATUS != 9");

            while (rs.next()) {
                recode_count = rs.getInt("CNT");
            }

        } catch (Exception ex) {
            logger.info("Error verifyWorkOrder Methd call.." + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (recode_count > 0) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("This Work Order Number Verify.");
            logger.info("This Work Order Number Verify....................");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("This Work Order Not in  List");
            logger.info("This Work Order Not in  List..............");
        }
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addWorkOrderClose(WorkOrderClose wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWorkOrderClose Mehtod Call.......");
        int result = 0;
        int cnt_wom = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_wo_main = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDER_MAINTAIN "
                    + "WHERE REPAIR_STATUS = 1 "
                    + "AND WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "'");

            while (rs_wo_main.next()) {
                cnt_wom = rs_wo_main.getInt("CNT");
            }

            if (cnt_wom > 0) {
                result = 2;
            } else {
                int wo_close_status = 0;

                ResultSet rs_wo_close = dbCon.search(con, "SELECT  COUNT(*) CNT "
                        + "FROM SD_WORK_ORDERS "
                        + "WHERE WORK_ORDER_NO ='" + wo_info.getWorkOrderNo() + "' "
                        + "AND STATUS = 3");
                while (rs_wo_close.next()) {
                    wo_close_status = rs_wo_close.getInt("CNT");
                }

                if (wo_close_status > 0) {
                    result = 3;
                    // System.out.println("This WorkOrder already Closed");
                } else {

//////////Checking this work order has pending transfers. If there is apending transfer, it cannot be allowed to close
                    String chkTransfers = " SELECT COUNT(*) CNT "
                            + " FROM SD_WORK_ORDER_TRANSFERS "
                            + " WHERE (STATUS=1 OR STATUS=3)  "
                            + " AND WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "'";

                    ResultSet rs_wo_trf = dbCon.search(con, chkTransfers);

                    int ckhWoTrf = 0;
                    if (rs_wo_trf.next()) {
                        ckhWoTrf = rs_wo_trf.getInt("CNT");
                    }

                    if (ckhWoTrf == 0) {
                        String qu = "UPDATE SD_WORK_ORDERS SET REPAIR_STATUS = ?,"
                                + "DELIVERY_TYPE = ?,"
                                + "TRANSFER_LOCATION = ?,"
                                + "COURIER_NO = ?,"
                                + "DELIVERY_DATE = TO_DATE(?,'YYYY/MM/DD'),"
                                + "GATE_PASS = ?,"
                                + "USER_MODIFIED = ?,"
                                + "DATE_MODIFIED = SYSDATE,"
                                + "STATUS = ?,"
                                + "REMARKS = ? "
                                + "WHERE WORK_ORDER_NO = ?";

                        PreparedStatement ps = dbCon.prepare(con, qu);
                        ps.setInt(1, wo_info.getRepairid());
                        ps.setInt(2, wo_info.getDeleveryId());
                        ps.setInt(3, wo_info.getLocationId());
                        ps.setString(4, wo_info.getCourierNo());
                        ps.setString(5, wo_info.getDeleverDate());
                        ps.setString(6, wo_info.getGatePass());
                        ps.setString(7, user_id);
                        ps.setInt(8, wo_info.getStatus());
                        ps.setString(9, wo_info.getRemarks());
                        ps.setString(10, wo_info.getWorkOrderNo());
                        ps.executeUpdate();

                        result = 1;

                        String name = null;
                        String mail = null;
                        String product = null;
                        String wonumber = null;
                        String reffRcc = null;
                        String modal = null;
                        String imeiNo = null;
                        String deliveryType = null;
                        String deliveryDate = null;
                        String courierNo = null;
                        String gatePass = null;

                        ResultSet rs_wo_details = dbCon.search(con, "SELECT W.WORK_ORDER_NO,"
                                + "W.CUSTOMER_NAME,"
                                + "W.EMAIL,"
                                + "W.PRODUCT, "
                                + "W.RCC_REFERENCE, "
                                + "W.MODEL_NO,     "
                                + "M.MODEL_DESCRIPTION,     "
                                + "W.IMEI_NO,      "
                                + "( SELECT M.DESCRIPTION FROM SD_WORK_ORDERS W, SD_MDECODE M WHERE M.CODE =W.DELIVERY_TYPE  (+) AND     M.DOMAIN_NAME = 'DELEVERY_TYPE' AND     W.WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "') AS DESCRIPTION,    "
                                + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') AS DELIVERY_DATE,  "
                                + "W.COURIER_NO,     "
                                + "W.GATE_PASS       "
                                + "FROM SD_WORK_ORDERS W, SD_MODEL_LISTS M "
                                + "WHERE    W.MODEL_NO=M.MODEL_NO    "
                                + "AND  W.WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "'");

                        while (rs_wo_details.next()) {
                            wonumber = rs_wo_details.getString("WORK_ORDER_NO");
                            name = rs_wo_details.getString("CUSTOMER_NAME");
                            mail = rs_wo_details.getString("EMAIL");
                            product = rs_wo_details.getString("PRODUCT");
                            reffRcc = rs_wo_details.getString("RCC_REFERENCE");
                            modal = rs_wo_details.getString("MODEL_DESCRIPTION");
                            imeiNo = rs_wo_details.getString("IMEI_NO");
                            deliveryType = rs_wo_details.getString("DESCRIPTION");
                            deliveryDate = rs_wo_details.getString("DELIVERY_DATE");
                            courierNo = rs_wo_details.getString("COURIER_NO");
                            gatePass = rs_wo_details.getString("GATE_PASS");
                        }


//                        int deliveryId = wo_info.getDeleveryId();
//                        
//                        
//                        MessageWrapper repairList = WorkOrderCloseController.getWorkOrderCloseStatus();
//                        repairList.getData()[];
//
//                        //sms function
//                        SmsController sms = new SmsController();
//
//                        String message = "Dear Customer, \n"
//                                + "Your device REF NO: " + wo_info.getWorkOrderNo() + " and delivered [delivery type] ref no [Courier no], [Gate pass or other reference] on [delivery date].\n"
//                                + "Tel: 0115400400. T&C apply ";

                        try {
                            if (mail != null) {
                                EmaiSendConfirmCode.sendHTMLMailWorkOrderClose(mail, name, wonumber, product, reffRcc, modal, imeiNo, deliveryType, deliveryDate, courierNo, gatePass);
                            }
                        } catch (Exception e) {
                            logger.info("Error in Email" + e.getMessage());
                        }
                    } else {
                        /////////This work order has pending Transfers
                        result = 4;
                    }

                }
            }

            String sqlForInsertQcChkList = "INSERT INTO "
                    + "SD_QC_CHK_LST ("
                    + "WORK_ORDER_NO, "
                    + "RESOLVE_DEFECTS, "
                    + "VISUAL_INSPECT, "
                    + "ENG_TEST, "
                    + "CALL_SMS, "
                    + "SW_VERSION, "
                    + "ACCESSORIES) "
                    + "VALUES ( ? , ?, ?,? ,? ,? , ?)";

            PreparedStatement pst = dbCon.prepare(con, sqlForInsertQcChkList);

            pst.setString(1, wo_info.getWorkOrderNo());
            pst.setString(2, wo_info.getResolveDefect());
            pst.setString(3, wo_info.getVisualInspectionTest());
            pst.setString(4, wo_info.getEngineeringTest());
            pst.setString(5, wo_info.getCallSms());
            pst.setString(6, wo_info.getSwVersion());
            pst.setString(7, wo_info.getAccessories_delivered());
            pst.executeQuery();

        } catch (Exception ex) {
            logger.info("Error in addWorkOrderClose Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Close Successfully");
            logger.info("Work Order Close Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Have a Active Work Order Maintain Record");
            logger.info("Have a Active Work Order Maintain Record............");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This WorkOrder already Closed");
            logger.info("This WorkOrder already Closed............");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("There is a Pending Transfer for this Work Order.");
            logger.info("There is a Pending Transfer for this Work Order.");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Close");
            logger.info("Error in Work Order Close................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editWorkOrderClose(WorkOrderClose wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editWorkOrderClose Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String qu = "UPDATE SD_WORK_ORDERS SET REPAIR_STATUS = ?,"
                    + "DELIVERY_TYPE = ?,"
                    + "SERVICE_BIS_ID = ?,"
                    + "COURIER_NO = ?,"
                    + "DELIVERY_DATE = TO_DATE(?,'YYYY/MM/DD'),"
                    + "GATE_PASS = ?,"
                    + "USER_MODIFIED = ?,"
                    + "DATE_MODIFIED = SYSDATE,"
                    + "STATUS = ? "
                    + "WHERE WORK_ORDER_NO = ?";

            PreparedStatement ps = dbCon.prepare(con, qu);
            ps.setInt(1, wo_info.getRepairid());
            ps.setInt(2, wo_info.getDeleveryId());
            ps.setInt(3, wo_info.getLocationId());
            ps.setString(4, wo_info.getCourierNo());
            ps.setString(5, wo_info.getDeleverDate());
            ps.setString(6, wo_info.getGatePass());
            ps.setString(7, user_id);
            ps.setInt(8, wo_info.getStatus());
            ps.setString(9, wo_info.getWorkOrderNo());
            ps.executeUpdate();

            //inert if work worder number does not exists
            try {
                String sqlForInsertQcChkList = "INSERT INTO "
                        + "SD_QC_CHK_LST ("
                        + "WORK_ORDER_NO, "
                        + "RESOLVE_DEFECTS, "
                        + "VISUAL_INSPECT, "
                        + "ENG_TEST, "
                        + "CALL_SMS, "
                        + "SW_VERSION, "
                        + "ACCESSORIES) "
                        + "VALUES ( ? , ?, ?,? ,? ,? , ?)";

                PreparedStatement pst = dbCon.prepare(con, sqlForInsertQcChkList);

                pst.setString(1, wo_info.getWorkOrderNo());
                pst.setString(2, wo_info.getResolveDefect());
                pst.setString(3, wo_info.getVisualInspectionTest());
                pst.setString(4, wo_info.getEngineeringTest());
                pst.setString(5, wo_info.getCallSms());
                pst.setString(6, wo_info.getSwVersion());
                pst.setString(7, wo_info.getAccessories_delivered());
                pst.executeQuery();

            } catch (Exception e) {

                String qryForUpdateQC = "UPDATE SD_QC_CHK_LST SET "
                        + "ACCESSORIES=?, "
                        + "CALL_SMS =?, "
                        + "ENG_TEST=?, "
                        + "RESOLVE_DEFECTS=?, "
                        + "SW_VERSION =?, "
                        + "VISUAL_INSPECT=? "
                        + "WHERE "
                        + "WORK_ORDER_NO = ?";

                PreparedStatement qryPrep = dbCon.prepare(con, qryForUpdateQC);

                qryPrep.setString(1, wo_info.getAccessories_delivered());
                qryPrep.setString(2, wo_info.getCallSms());
                qryPrep.setString(3, wo_info.getEngineeringTest());
                qryPrep.setString(4, wo_info.getResolveDefect());
                qryPrep.setString(5, wo_info.getSwVersion());
                qryPrep.setString(6, wo_info.getVisualInspectionTest());
                qryPrep.setString(7, wo_info.getWorkOrderNo());

                qryPrep.executeUpdate();
            }

            result = 1;

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in editWorkOrderClose Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Close Update Successfully");
            logger.info("Work Order Close Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Update Close");
            logger.info("Error in Work Order Update Close................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper deleteWorkOrderClose(WorkOrderClose wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deleteWorkOrderClose Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String updatetrans = "UPDATE SD_WORK_ORDERS "
                    + "SET STATUS = 9,"
                    + "USER_MODIFIED =?,"
                    + "DATE_MODIFIED = SYSDATE "
                    + "WHERE WORK_ORDER_NO =?";

            PreparedStatement ps2 = dbCon.prepare(con, updatetrans);
            ps2.setString(1, user_id);
            ps2.setString(2, wo_info.getWorkOrderNo());
            ps2.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in deleteWorkOrderClose Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Close Delete Successfully");
            logger.info("Work Order Close Delete Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Close Delete");
            logger.info("Error in Work Order Close Delete................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getNonWarrantyType() {

        logger.info("getNonWarrantyType Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'NON_WARANTY_TYPE'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getNonWarrantyType Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWarrantyType() {

        logger.info("getWarrantyType Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'WARANTY'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");
            System.out.println("warrenty types.." + "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");

                int warrantyStatus = 0;
                if (rs.getInt("CODE") == 2) {
                    warrantyStatus = 1;
                }
                System.out.println("warrantyStatus#######" + warrantyStatus);

                //code has changed
                //de.setCode(warrantyStatus);
                de.setCode(rs.getInt("CODE"));

                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in getWarrantyType Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderToClose(int bis_id) {

        logger.info("getWorkOrderToClose  Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String bisId = (bis_id == 0 ? "" : "AND BIS_ID = " + bis_id + " ");

            ResultSet rs_open_wo = dbCon.search(con, "SELECT WORK_ORDER_NO "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE STATUS NOT IN (9.3)  "
                    + " " + bisId + " ");

            ArrayList<String> woOpenList = new ArrayList<>();

            while (rs_open_wo.next()) {
                woOpenList.add(rs_open_wo.getString("WORK_ORDER_NO"));

            }

            ResultSet rs_wo_main = dbCon.search(con, "SELECT WORK_ORDER_NO "
                    + "FROM SD_WORK_ORDER_MAINTAIN "
                    + "WHERE REPAIR_STATUS IN (2)");

            ArrayList<String> woOpenMaintainList = new ArrayList<>();

            while (rs_wo_main.next()) {
                woOpenMaintainList.add(rs_wo_main.getString("WORK_ORDER_NO"));

            }

            woOpenList.removeAll(woOpenMaintainList);

            ResultSet rs_wo_trans = dbCon.search(con, "SELECT WORK_ORDER_NO FROM SD_WORK_ORDER_TRANSFERS WHERE STATUS IN (1,2)");

            ArrayList<String> woActiveTransList = new ArrayList<>();

            while (rs_wo_trans.next()) {
                woActiveTransList.add(rs_wo_trans.getString("WORK_ORDER_NO"));

            }
            woOpenList.removeAll(woActiveTransList);

            for (String s : woOpenList) {
                WorkOrder wo = new WorkOrder();
                wo.setWorkOrderNo(s);
                workOrderList.add(wo);
            }
        } catch (Exception ex) {
            logger.info("Error getWorkOrderToClose Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = workOrderList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

}
