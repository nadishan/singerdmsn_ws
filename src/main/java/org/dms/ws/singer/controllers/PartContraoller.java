/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Part;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class PartContraoller {

    public static MessageWrapper getPartsDetails(int partno,
            String partdesc,
            int partexist,
            String partprdcode,
            String partprddesc,
            String partfamily,
            String partfamilydesc,
            String partgroup1,
            String partgroup2,
            double partsellprice,
            double partcost,
            String erppartno,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getPartsDetails Mehtod Call.......");
        int totalrecode = 0;
        ArrayList<Part> partList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.part(order);

            String partNo = (partno == 0 ? "" : "AND SPARE_PART_NO = " + partno + "");
            String partDesc = (partdesc.equalsIgnoreCase("all") ? "NVL(PART_DESCRIPTION,'AA') = NVL(PART_DESCRIPTION,'AA')" : "UPPER(PART_DESCRIPTION) LIKE UPPER('%" + partdesc + "%')");
            String partExist = (partexist == 0 ? "" : "AND ALTERNATE_PART_EXISTS = " + partexist + "");
            String partPrdCode = (partprdcode.equalsIgnoreCase("all") ? "NVL(PART_PRD_CODE,'AA') = NVL(PART_PRD_CODE,'AA')" : "UPPER(PART_PRD_CODE) LIKE UPPER('%" + partprdcode + "%')");
            String partPrdDesc = (partprddesc.equalsIgnoreCase("all") ? "NVL(PART_PRD_CODE_DESC,'AA') = NVL(PART_PRD_CODE_DESC,'AA')" : "UPPER(PART_PRD_CODE_DESC) LIKE UPPER('%" + partprddesc + "%')");
            String partFamily = (partfamily.equalsIgnoreCase("all") ? "NVL(PART_PRD_FAMILY,'AA') = NVL(PART_PRD_FAMILY,'AA')" : "UPPER(PART_PRD_FAMILY) LIKE UPPER('%" + partfamily + "%')");
            String partFamilyDesc = (partfamilydesc.equalsIgnoreCase("all") ? "NVL(PART_PRD_FAMILY_DESC,'AA') = NVL(PART_PRD_FAMILY_DESC,'AA')" : "UPPER(PART_PRD_FAMILY_DESC) LIKE UPPER('%" + partfamilydesc + "%')");
            String partGroup1 = (partgroup1.equalsIgnoreCase("all") ? "NVL(COMMODITY_GROUP_1,'AA') = NVL(COMMODITY_GROUP_1,'AA')" : "UPPER(COMMODITY_GROUP_1) LIKE UPPER('%" + partgroup1 + "%')");
            String partGroup2 = (partgroup2.equalsIgnoreCase("all") ? "NVL(COMMODITY_GROUP_2,'AA') = NVL(COMMODITY_GROUP_2,'AA')" : "UPPER(COMMODITY_GROUP_2) LIKE UPPER('%" + partgroup2 + "%')");
            String partSellPrice = (partsellprice == 0 ? "" : "AND SELLING_PRICE = " + partsellprice + "");
            String partCost = (partcost == 0 ? "" : "AND COST_PRICE = " + partcost + "");
            String erpPartNo = (erppartno.equalsIgnoreCase("all") ? "NVL(ERP_SPARE_PART_NO,'AA') = NVL(ERP_SPARE_PART_NO,'AA')" : "UPPER(ERP_SPARE_PART_NO) LIKE UPPER('%" + erppartno + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY SPARE_PART_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColnum = "SPARE_PART_NO,PART_DESCRIPTION,ALTERNATE_PART_EXISTS,"
                    + "PART_PRD_CODE,PART_PRD_CODE_DESC,PART_PRD_FAMILY,PART_PRD_FAMILY_DESC,"
                    + "COMMODITY_GROUP_1,COMMODITY_GROUP_2,SELLING_PRICE,COST_PRICE,ERP_SPARE_PART_NO,STATUS";

            String whereClous = "" + partDesc + ""
                    + "" + partNo + " "
                    + "" + partExist + " "
                    + "AND " + partPrdCode + " "
                    + "AND " + partPrdDesc + " "
                    + "AND " + partFamily + " "
                    + "AND " + partFamilyDesc + " "
                    + "AND " + partGroup1 + " "
                    + "AND " + partGroup2 + ""
                    + "" + partSellPrice + ""
                    + "" + partCost + " "
                    + "AND " + erpPartNo + " ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_SPARE_PARTS WHERE  " + whereClous + ") CNT,"
                    + "" + selectColnum + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_SPARE_PARTS "
                    + "WHERE " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            logger.info("Get Data to ResultSet............");
            while (rs.next()) {
                Part pt = new Part();
                totalrecode = rs.getInt("CNT");
                pt.setPartNo(rs.getInt("SPARE_PART_NO"));
                pt.setPartDesc(rs.getString("PART_DESCRIPTION"));
                pt.setPartExist(rs.getInt("ALTERNATE_PART_EXISTS"));
                pt.setPartPrdCode(rs.getString("PART_PRD_CODE"));
                pt.setPartPrdDesc(rs.getString("PART_PRD_CODE_DESC"));
                pt.setPartPrdFamily(rs.getString("PART_PRD_FAMILY"));
                pt.setPartPrdFamilyDesc(rs.getString("PART_PRD_FAMILY_DESC"));
                pt.setPartGroup1(rs.getString("COMMODITY_GROUP_1"));
                pt.setPartGroup2(rs.getString("COMMODITY_GROUP_2"));
                pt.setPartSellPrice(rs.getDouble("SELLING_PRICE"));
                pt.setPartCost(rs.getDouble("COST_PRICE"));
                pt.setErpPartNo(rs.getString("ERP_SPARE_PART_NO"));
                pt.setStatus(rs.getInt("STATUS"));
                
                partList.add(pt);
            }

        } catch (Exception ex) {
            logger.info("Error in getPartsDetails mehtod  :" + ex.toString());
        }
        logger.info("Ready Return Values.............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(partList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper editParts(Part parts,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editParts Method Call.....");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            PreparedStatement ps_up_parts = dbCon.prepare(con, "UPDATE SD_SPARE_PARTS "
                    + "SET PART_DESCRIPTION = ?,"
                    + "ALTERNATE_PART_EXISTS  = ?,"
                    + "PART_PRD_CODE  = ?,"
                    + "PART_PRD_FAMILY  = ?,"
                    + "PART_PRD_FAMILY_DESC  = ?,"
                    + "COMMODITY_GROUP_1  = ?,"
                    + "COMMODITY_GROUP_2  = ?,"
                    + "SELLING_PRICE  = ?,"
                    + "COST_PRICE = ?,"
                    + "STATUS  = ?,"
                    + "ERP_SPARE_PART_NO  = ?,"
                    + "USER_MODIFIED  = ?,"
                    + "DATE_MODIFIED = SYSDATE "
                    + "WHERE SPARE_PART_NO  = ?");
            
            

            
            ps_up_parts.setString(1, parts.getPartDesc());
            ps_up_parts.setInt(2, parts.getPartExist());
            ps_up_parts.setString(3, parts.getPartPrdCode());
            ps_up_parts.setString(4, parts.getPartPrdFamily());
            ps_up_parts.setString(5, parts.getPartPrdFamilyDesc());
            ps_up_parts.setString(6, parts.getPartGroup1());
            ps_up_parts.setString(7, parts.getPartGroup2());
            ps_up_parts.setDouble(8, parts.getPartSellPrice());
            ps_up_parts.setDouble(9, parts.getPartCost());
            ps_up_parts.setInt(10, parts.getStatus());
            ps_up_parts.setString(11, parts.getErpPartNo());
            ps_up_parts.setString(12, user_id);
            ps_up_parts.setInt(13, parts.getPartNo());
            
            
            ps_up_parts.executeUpdate();

            result = 1;
        } catch (Exception e) {
            logger.info("Error in editParts Update Query..." + e.getMessage());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Spare Parts Update Successfully");
            logger.info("Spare Parts Update Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Spare Parts Update");
            logger.info("Error in Spare Parts Update.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    
    
    
   
    
    
}
