/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.MinorDefect;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class MinorDefectController {

    public static MessageWrapper getMinorDefects(int mindefectcode,
            String mindefectdesc,
            int mjrdefectcode,
            int mindefectstatus,
            String order,
            String type,
            int start,
            int limit) {
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getMinorDefects Method Call.........");
        int totalrecode = 0;
        ArrayList<MinorDefect> mdList = new ArrayList<>();
        try {

            order = DBMapper.minorDefectCode(order);

            String minDefectCode = (mindefectcode == 0 ? "" : "AND MIN_CODE=" + mindefectcode + "");
            String minDefectDesc = (mindefectdesc.equalsIgnoreCase("all") ? "NVL(MIN_CODE_DESC,'AA') = NVL(MIN_CODE_DESC,'AA')" : "UPPER(MIN_CODE_DESC) LIKE UPPER('%" + mindefectdesc + "%')");
            String mjrDefectCode = (mjrdefectcode == 0 ? "" : "AND MAJ_CODE = " + mjrdefectcode + "");
            String minDefectStatus = (mindefectstatus == 0 ? "" : "AND STATUS = " + mindefectstatus + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY MIN_CODE DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "MIN_CODE,MIN_CODE_DESC,MAJ_CODE,STATUS";

            String whereClous = "" + minDefectDesc + ""
                    + "" + minDefectCode + ""
                    + "" + mjrDefectCode + ""
                    + "" + minDefectStatus + "";

            //System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_DEFECT_MINOR_CODES WHERE  " + whereClous + ") CNT," + selectColumn + ",ROW_NUMBER() OVER (ORDER BY REP_CAT_ID) RN FROM SD_DEFECT_MINOR_CODES WHERE " + whereClous + ") WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_DEFECT_MINOR_CODES WHERE  " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_DEFECT_MINOR_CODES "
                    + "WHERE " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " "
                    + "AND " + (start + limit) + " ORDER BY RN");
            logger.info("Get Data to ResultSet........");
            while (rs.next()) {
                MinorDefect md = new MinorDefect();
                totalrecode = rs.getInt("CNT");
                md.setMinDefectCode(rs.getInt("MIN_CODE"));
                md.setMinDefectDesc(rs.getString("MIN_CODE_DESC"));
                md.setMrjDefectCode(rs.getInt("MAJ_CODE"));
                md.setMinDefectStatus(rs.getInt("STATUS"));
                mdList.add(md);
            }

        } catch (Exception ex) {
            logger.info("Error in getMinorDefects Method  :" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdList);
        logger.info("Ready to return Values.....");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper addMinorDefect(MinorDefect md,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addMinorDefect Mehtod Call.......");
        int result = 0;
        int min_count = 0;
        int min_max = 0 ;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet min_cnt = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_DEFECT_MINOR_CODES WHERE UPPER(MIN_CODE_DESC)=UPPER('" + md.getMinDefectDesc() + "')");

            while (min_cnt.next()) {
                min_count = min_cnt.getInt("CNT");
            }

            ResultSet rs_max = dbCon.search(con,"SELECT NVL(MAX(MIN_CODE),0) MAX FROM SD_DEFECT_MINOR_CODES");
              
            while(rs_max.next()){
                min_max = rs_max.getInt("MAX");
            }
            
            
            String column = "MIN_CODE,MIN_CODE_DESC,MAJ_CODE,STATUS,USER_INSERTED,DATE_INSERTED";

            String values = "?,UPPER(?),?,?,?,SYSDATE";
            

            if (min_count == 0) {
                PreparedStatement ps_in_defect = dbCon.prepare(con,"INSERT INTO SD_DEFECT_MINOR_CODES(" + column + ") VALUES(" + values + ")");
                try {
                    min_max = min_max + 1;
                    
                    ps_in_defect.setInt(1, min_max);
                    ps_in_defect.setString(2, md.getMinDefectDesc());
                    ps_in_defect.setInt(3, md.getMrjDefectCode());
                    ps_in_defect.setInt(4, md.getMinDefectStatus());
                    ps_in_defect.setString(5, user_id);
                    ps_in_defect.executeUpdate();
                    result = 1;
                    
                } catch (Exception e) {
                    logger.info("Error in MinorDefect Insert......");
                    result = 9;
                }
            } else {
                result = 2;
                logger.info("Minor Code Description Allredy Exists");
            }

        } catch (Exception ex) {
            logger.info("Error in addMinorDefect Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Minor Defect Insert Successfully");
            logger.info("Minor Defect Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Minor Code Description Already Exist");
            logger.info("Minor Code Description Already Exist................");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Minor Defect Insert");
            logger.info("Error in Minor Defect Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editMinorDefect(MinorDefect md,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editMinorDefect Mehtod Call.......");
        int result = 0;
        int min_count = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet min_cnt = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_DEFECT_MINOR_CODES "
                    + " WHERE UPPER(MIN_CODE_DESC)=UPPER('" + md.getMinDefectDesc() + "') "
                    + " AND MIN_CODE != " + md.getMinDefectCode() + "");

            while (min_cnt.next()) {
                min_count = min_cnt.getInt("CNT");
            }

            if (min_count > 0) {
                result = 2;
            } else {

                try {
                    
                    PreparedStatement ps_up_debit = dbCon.prepare(con,"UPDATE SD_DEFECT_MINOR_CODES "
                            + "SET MIN_CODE_DESC = UPPER(?),"
                            + "STATUS = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE MIN_CODE = ? ");
                    
                    ps_up_debit.setString(1, md.getMinDefectDesc());
                    ps_up_debit.setInt(2, md.getMinDefectStatus());
                    ps_up_debit.setString(3, user_id);
                    ps_up_debit.setInt(4, md.getMinDefectCode());
                    ps_up_debit.executeUpdate();
                    
                    result = 1;
                } catch (Exception e) {
                    logger.info("Error in MinorDefect Update......");
                    result = 9;
                }

            }

        } catch (Exception ex) {
            logger.info("Error in editMinorDefect Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Minor Defect Update Successfully");
            logger.info("Minor Defect Update Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Minor Defect Code Already Exist");
            logger.info("Minor Defect Code Already Exist................");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Minor Defect Update");
            logger.info("Error in Minor Defect Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

}
