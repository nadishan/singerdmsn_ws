package org.dms.ws.singer.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.dms.ws.services.MessageWrapper;
import org.smpp.Data;
import org.smpp.ServerPDUEventListener;
import org.smpp.Session;
import org.smpp.SmppObject;
import org.smpp.TCPIPConnection;
import org.smpp.debug.Debug;
import org.smpp.debug.Event;
import org.smpp.pdu.Address;
import org.smpp.pdu.AddressRange;
import org.smpp.pdu.BindRequest;
import org.smpp.pdu.BindResponse;
import org.smpp.pdu.BindTransmitter;
import org.smpp.pdu.EnquireLink;
import org.smpp.pdu.EnquireLinkResp;
import org.smpp.pdu.SubmitSM;
import org.smpp.pdu.SubmitSMResp;
import org.smpp.pdu.Unbind;
import org.smpp.pdu.UnbindResp;

public class SmsController {

    /**
     * File with default settings for the application.
    static String propsFilePath = "./smppsender.cfg";
     */

    /**
     * This is the SMPP session used for communication with SMSC.
     */
    static Session session = null;

    /**
     * Contains the parameters and default values for this test application such
     * as system id, password, default npi and ton of sender etc.
     */
//	Properties properties = new Properties();
    /**
     * If the application is bound to the SMSC.
     */
    boolean bound = false;

    /**
     * Address of the SMSC.
     */
    String ipAddress = "10.62.230.110";
//    String ipAddress = "127.0.0.1";

    /**
     * The port number to bind to on the SMSC server.
     */
    int port = 2775;

    /**
     * The name which identifies you to SMSC.
     */
    String systemId = "singer123";
//    String systemId = "smppclient1";

    /**
     * The password for authentication to SMSC.
     */
    String password = "sin123";
//    String password = "password";

    /**
     * How you want to bind to the SMSC: transmitter (t), receiver (r) or
     * transciever (tr). Transciever can both send messages and receive
     * messages. Note, that if you bind as receiver you can still receive
     * responses to you requests (submissions).
     */
    String bindOption = "t";

    /**
     * The range of addresses the smpp session will serve.
     */
    AddressRange addressRange = new AddressRange();

    /*
	 * for information about these variables have a look in SMPP 3.4
	 * specification
     */
    String systemType = "";
    String serviceType = "";
    Address sourceAddress = new Address();
    Address destAddress = new Address();
    String scheduleDeliveryTime = "";
    String validityPeriod = "000007233429000R";
    String shortMessage = "";
    int numberOfDestination = 1;
    String messageId = "";
    byte esmClass = 0;
    byte protocolId = 0;
    byte priorityFlag = 3; //default 0
    byte registeredDelivery = 0;
    byte replaceIfPresentFlag = 0;
    byte dataCoding = 0;
    byte smDefaultMsgId = 0;

    int flag = 0;
    /**
     * If you attemt to receive message, how long will the application wait for
     * data.
     */
    long receiveTimeout = Data.RECEIVE_BLOCKING;

    /**
     * Sets global SMPP library debug and event objects. Runs the application.
     *
     * @see SmppObject#setDebug(Debug)
     * @see SmppObject#setEvent(Event)
     */
    public MessageWrapper send(String number, String msg) {

        //for debugging purpose
        MessageWrapper mw = new MessageWrapper();
        int info = 1;  //0 - no error 1 - error

        // Parse the command line
        String sender = "SINGER";  //lib ignore this field
        byte senderTon = (byte) 5; //0; //lib ignore this field
        byte senderNpi = (byte) 0;  //lib ignore this field

        number = number.replaceFirst("0", "");
     
        String numberPart = number.substring(0, 1);
        System.out.println(numberPart);
        String dest = "+94".concat(number);
        String message = msg;
//        String dest = "+94771237730";
//        String message = "df ed f";

        if ((dest == null) || (message == null)) {
            System.out.println("Usage: SMPPSender -dest <dest number on international format> -message <the message, within qoutes if contains whitespaces> [-sender <sender id> [-senderTon <sender ton>] [-senderNpi <sender npi>]]");
            System.exit(0);
        }

        // Dest may contain comma-separated numbers
        Collection<String> destinations = new ArrayList<String>();
        StringTokenizer st = new StringTokenizer(dest, ",");
        while (st.hasMoreTokens()) {
            String d = st.nextToken();
            destinations.add(d);
        }

        System.out.println("Initialising...");
        SmsController smppSender = null;
        smppSender = new SmsController();

        System.out.println("Sending: \"" + message + "\" to " + dest);

        if (smppSender != null) {
            smppSender.bind();

            if (smppSender.bound) {
                Iterator<String> it = destinations.iterator();
                while (it.hasNext()) {
                    String d = it.next();
                    smppSender.submit(d, message, sender, senderTon, senderNpi);
                }
                info = 0;
                smppSender.unbind();

            }else{
                info = 1;
            }
        }
        
        mw.setTotalRecords(info);
        return mw;
    }

    /**
     * The first method called to start communication betwen an ESME and a SMSC.
     * A new instance of <code>TCPIPConnection</code> is created and the IP
     * address and port obtained from user are passed to this instance. New
     * <code>Session</code> is created which uses the created
     * <code>TCPIPConnection</code>. All the parameters required for a bind are
     * set to the <code>BindRequest</code> and this request is passed to the
     * <code>Session</code>'s <code>bind</code> method. If the call is
     * successful, the application should be bound to the SMSC.
     *
     * See "SMPP Protocol Specification 3.4, 4.1 BIND Operation."
     *
     * @see BindRequest
     * @see BindResponse
     * @see TCPIPConnection
     * @see Session#bind(BindRequest)
     * @see Session#bind(BindRequest,ServerPDUEventListener)
     */
    private void bind() {
        try {
            if (bound) {
                System.out.println("Already bound, unbind first.");
                return;
            }

            BindRequest request = null;
            BindResponse response = null;

            request = new BindTransmitter();

            TCPIPConnection connection = new TCPIPConnection(ipAddress, port);
            connection.setReceiveTimeout(20 * 1000);
            session = new Session(connection);

            // set values
            request.setSystemId(systemId);
            request.setPassword(password);
            request.setSystemType(systemType);
            request.setInterfaceVersion((byte) 0x34);
            request.setAddressRange(addressRange);

            // send the request
            System.out.println("Bind request " + request.debugString());
            response = session.bind(request);
            System.out.println("Bind response " + response.debugString());
            if (response.getCommandStatus() == Data.ESME_ROK) {
                bound = true;
            } else {
                System.out.println("Bind failed, code " + response.getCommandStatus());
            }
        } catch (Exception e) {
            System.out.println("Bind operation failed. " + e);
        }
    }

    /**
     * Ubinds (logs out) from the SMSC and closes the connection.
     *
     * See "SMPP Protocol Specification 3.4, 4.2 UNBIND Operation."
     *
     * @see Session#unbind()
     * @see Unbind
     * @see UnbindResp
     */
    private void unbind() {
        try {

            if (!bound) {
                System.out.println("Not bound, cannot unbind.");
                return;
            }

            // send the request
            System.out.println("Going to unbind.");
            if (session.getReceiver().isReceiver()) {
                System.out.println("It can take a while to stop the receiver.");
            }
            UnbindResp response = session.unbind();
            System.out.println("Unbind response " + response.debugString());

            bound = false;
        } catch (Exception e) {
            System.out.println("Unbind operation failed. " + e);
        }
    }

    /**
     * Creates a new instance of <code>SubmitSM</code> class, lets you set
     * subset of fields of it. This PDU is used to send SMS message to a device.
     *
     * See "SMPP Protocol Specification 3.4, 4.4 SUBMIT_SM Operation."
     *
     * @see Session#submit(SubmitSM)
     * @see SubmitSM
     * @see SubmitSMResp
     */
    private void submit(String destAddress, String shortMessage, String sender, byte senderTon, byte senderNpi) {
        try {

            SubmitSM request = new SubmitSM();
            SubmitSMResp response;

            // set values
            request.setServiceType(serviceType);

            if (sender != null) {
                if (sender.startsWith("+")) {
                    sender = sender.substring(1);
                    senderTon = 1;
                    senderNpi = 1;
                }
                if (!sender.matches("\\d+")) {
                    senderTon = 5;
                    senderNpi = 0;
                }

                if (senderTon == 5) {
                    request.setSourceAddr(new Address(senderTon, senderNpi, sender, 11));
                } else {
                    request.setSourceAddr(new Address(senderTon, senderNpi, sender));
                }
            } else {
//                request.setSourceAddr("SINGER");
            }

            if (destAddress.startsWith("+")) {
                destAddress = destAddress.substring(1);
            }

            //Set the NPI and TON with the request 
            //####################################
            request.setDestAddr(new Address((byte) 1, (byte) 1, destAddress));
            request.setSourceAddr(new Address((byte) 5, (byte) 0, "SINGER"));

            request.setReplaceIfPresentFlag(replaceIfPresentFlag);
            request.setShortMessage(shortMessage, Data.ENC_ASCII);
            request.setScheduleDeliveryTime(scheduleDeliveryTime);
            request.setValidityPeriod(validityPeriod);
            request.setEsmClass(esmClass);
            request.setProtocolId(protocolId);
            request.setPriorityFlag(priorityFlag);
            request.setRegisteredDelivery(registeredDelivery);
            request.setDataCoding(dataCoding);
            request.setSmDefaultMsgId(smDefaultMsgId);

            // send the request
            request.assignSequenceNumber(true);
            System.out.println(request.getSourceAddr().getAddress());
            System.out.println("Submit request " + request.debugString());
            response = session.submit(request);
            System.out.println("Submit response " + response.debugString());
            messageId = response.getMessageId();
            enquireLink();
        } catch (Exception e) {
            System.out.println("Submit operation failed. " + e);
        }
    }

    /**
     * Creates a new instance of <code>EnquireSM</code> class. This PDU is used
     * to check that application level of the other party is alive. It can be
     * sent both by SMSC and ESME.
     *
     * See "SMPP Protocol Specification 3.4, 4.11 ENQUIRE_LINK Operation."
     *
     * @see Session#enquireLink(EnquireLink)
     * @see EnquireLink
     * @see EnquireLinkResp
     */
    private void enquireLink() {
        try {
            EnquireLink request = new EnquireLink();
            EnquireLinkResp response;
            System.out.println("Enquire Link request " + request.debugString());
            response = session.enquireLink(request);
            System.out.println("Enquire Link response " + response.debugString());
        } catch (Exception e) {
            System.out.println("Enquire Link operation failed. " + e);
        }
    }

}
