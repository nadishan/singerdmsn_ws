/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.RepairLevel;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class LevelContraller {

    public static MessageWrapper getLevels(int levelid,
            String levelcode,
            String levelname,
            String leveldesc,
            int levelcatid,
            double levelprice,
            int levelstatus,
            String order,
            String type,
            int start,
            int limit) {
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getLevels Method Call.........");
        int totalrecode = 0;
        ArrayList<RepairLevel> reLevelList = new ArrayList<>();
        try {

            order = DBMapper.repairLevel(order);

            String levelId = (levelid == 0 ? "" : "AND LEVEL_ID = " + levelid + "");
            String levelCode = (levelcode.equalsIgnoreCase("all") ? "NVL(LEVEL_CODE,'AA') = NVL(LEVEL_CODE,'AA')" : "UPPER(LEVEL_CODE) LIKE UPPER('%" + levelcode + "%')");
            String levelName = (levelname.equalsIgnoreCase("all") ? "NVL(LEVEL_NAME,'AA') = NVL(LEVEL_NAME,'AA')" : "UPPER(LEVEL_NAME) LIKE UPPER('%" + levelname + "%')");
            String levelDesc = (leveldesc.equalsIgnoreCase("all") ? "NVL(LEVEL_DESC,'AA') = NVL(LEVEL_DESC,'AA')" : "UPPER(LEVEL_DESC) LIKE UPPER('%" + leveldesc + "%')");
            String levelCatId = (levelcatid == 0 ? "" : "AND REP_CAT_ID = " + levelcatid + "");
            String levelPrice = (levelprice == 0 ? "" : "AND PRICE = " + levelprice + "");
            String levelStatus = (levelstatus == 0 ? "" : "AND STATUS = " + levelstatus + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY LEVEL_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColumn = "LEVEL_ID,LEVEL_CODE,LEVEL_NAME,LEVEL_DESC,REP_CAT_ID,PRICE,STATUS";

            String whereClous = "" + levelDesc + " "
                    + "AND " + levelCode + " "
                    + "AND " + levelName + " "
                    + "" + levelId + " "
                    + "" + levelCatId + " "
                    + "" + levelPrice + " "
                    + "" + levelStatus + " ";

            //System.out.println(seletColumn);
            //.out.println(whereClous);
            // System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_REPAIR_LEVELS WHERE  " + whereClous + ") CNT," + seletColumn + ",ROW_NUMBER() OVER (ORDER BY LEVEL_ID) RN FROM SD_REPAIR_LEVELS WHERE " + whereClous + ") WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_REPAIR_LEVELS WHERE  " + whereClous + ") CNT,"
                    + "" + seletColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_REPAIR_LEVELS WHERE " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            logger.info("Get Data to ResultSet.............");

            while (rs.next()) {

                RepairLevel rl = new RepairLevel();
                totalrecode = rs.getInt("CNT");
                rl.setLevelId(rs.getInt("LEVEL_ID"));
                rl.setLevelCode(rs.getString("LEVEL_CODE"));
                rl.setLevelName(rs.getString("LEVEL_NAME"));
                rl.setLevelDesc(rs.getString("LEVEL_DESC"));
                rl.setLevelCatId(rs.getInt("REP_CAT_ID"));
                rl.setLevelPrice(rs.getDouble("PRICE"));
                rl.setLevelStatus(rs.getInt("STATUS"));
                reLevelList.add(rl);
            }

        } catch (Exception ex) {
            logger.info("Error getLevels Method ........");
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(reLevelList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper addLevel(RepairLevel rl,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addLevel Method Call.....   ");
        String result = "0";
        int max_level = 0;
        int name_exist = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_lname_exist = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_REPAIR_LEVELS "
                    + "WHERE UPPER(LEVEL_NAME) = UPPER('" + rl.getLevelName() + "')");
            while (rs_lname_exist.next()) {
                name_exist = rs_lname_exist.getInt("CNT");
            }

            if (name_exist > 0) {
                result = "2";
            } else {

                ResultSet rs_max = dbCon.search(con,"SELECT NVL(MAX(LEVEL_ID),0) MAX FROM SD_REPAIR_LEVELS");

                if (rs_max.next()) {
                    max_level = rs_max.getInt("MAX");
                }

                String column = "LEVEL_ID,LEVEL_CODE,LEVEL_NAME,LEVEL_DESC,REP_CAT_ID,PRICE,STATUS,USER_INSERTED,DATE_INSERTED";

                String values = "?,?,UPPER(?),?,?,?,?,?,SYSDATE";

                PreparedStatement ps_in_level = dbCon.prepare(con,"INSERT INTO SD_REPAIR_LEVELS(" + column + ") VALUES(" + values + ")");

                max_level = max_level + 1;
                String levelcode = String.valueOf(max_level) + rl.getLevelCatId();

                ps_in_level.setInt(1, max_level);
                ps_in_level.setString(2, levelcode);
                ps_in_level.setString(3, rl.getLevelName());
                ps_in_level.setString(4, rl.getLevelDesc());
                ps_in_level.setInt(5, rl.getLevelCatId());
                ps_in_level.setDouble(6, rl.getLevelPrice());
                ps_in_level.setInt(7, rl.getLevelStatus());
                ps_in_level.setString(8, user_id);
                ps_in_level.executeUpdate();
                result = "1";
            }

        } catch (Exception ex) {
            logger.info("Error in addLevel........ " + ex.getMessage());
            result = "9";
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result.equals("1")) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Repair Level Insert Successfully");
            logger.info("Repair Level Insert Succesfully........");
        } else if (result.equals("2")) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Level Name Already Exist ");
            logger.info("Level Name Already Exist........");
        } else {
            vw.setReturnFlag(result);
            vw.setReturnMsg("Error Repair Level Insert");
            logger.info("Error Repair Level Insert........");
        }
        logger.info("Ready to Return Values........." + result);
        dbCon.ConectionClose(con);
        return vw;
    }

    public static ValidationWrapper editLevel(RepairLevel rl,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editLevel Method Call......");
        int result = 0;
        int name_exist = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_lname_exist = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_REPAIR_LEVELS "
                    + "WHERE UPPER(LEVEL_NAME) = UPPER('" + rl.getLevelName() + "') "
                    + "AND LEVEL_ID != " + rl.getLevelId() + "");
            while (rs_lname_exist.next()) {
                name_exist = rs_lname_exist.getInt("CNT");
            }

            if (name_exist > 0) {
                result = 2;
            } else {

                PreparedStatement ps_up_level = dbCon.prepare(con,"UPDATE SD_REPAIR_LEVELS "
                        + "SET LEVEL_NAME = UPPER(?),"
                        + "LEVEL_DESC = ?,"
                        + "PRICE = ?,"
                        + "STATUS = ?,"
                        + "USER_MODIFIED = ?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE LEVEL_ID = ? ");

                ps_up_level.setString(1, rl.getLevelName());
                ps_up_level.setString(2, rl.getLevelDesc());
                ps_up_level.setDouble(3, rl.getLevelPrice());
                ps_up_level.setInt(4, rl.getLevelStatus());
                ps_up_level.setString(5, user_id);
                ps_up_level.setInt(6, rl.getLevelId());
                ps_up_level.executeUpdate();
                result = 1;
            }

        } catch (Exception ex) {
            logger.info("Error editLevel Method ..." + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Repair Level Update Successfully");
            logger.info("Repair Level Update Succesfully........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Level Name Already Exist");
            logger.info("Level Name Already Exist......");
        }else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Repair Level Update");
            logger.info("Error in Repair Level Update......");
        }
        logger.info("Ready to Returns Values............");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    
    
     public static MessageWrapper getRepairLevelsForImei(String imie_no) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getRepairLevelsForImei Method Call.........");
        int totalrecode = 0;
        ArrayList<RepairLevel> reLevelList = new ArrayList<>();
        try {


            String seletColumn = "R.LEVEL_ID,R.LEVEL_CODE,R.LEVEL_NAME,R.LEVEL_DESC,R.REP_CAT_ID,R.PRICE,R.STATUS";

            String whereClous = "R.REP_CAT_ID = C.REP_CAT_ID (+) "
                    + "AND C.REP_CAT_ID = M.REP_CAT_ID (+) "
                    + "AND M.MODEL_NO = D.MODEL_NO (+) "
                    + "AND D.IMEI_NO = '"+imie_no+"'";
            
            String tables = "SD_DEVICE_MASTER D,"
                    + "SD_MODEL_LISTS M,"
                    + "SD_REPAIR_CATEGORIES C,"
                    + "SD_REPAIR_LEVELS R";

            
            ResultSet rs = dbCon.search(con,"SELECT " + seletColumn + " FROM "+tables+" WHERE " + whereClous + "");
            logger.info("Get Data to ResultSet.............");

            while (rs.next()) {

                RepairLevel rl = new RepairLevel();
                rl.setLevelId(rs.getInt("LEVEL_ID"));
                rl.setLevelCode(rs.getString("LEVEL_CODE"));
                rl.setLevelName(rs.getString("LEVEL_NAME"));
                rl.setLevelDesc(rs.getString("LEVEL_DESC"));
                rl.setLevelCatId(rs.getInt("REP_CAT_ID"));
                rl.setLevelPrice(rs.getDouble("PRICE"));
                rl.setLevelStatus(rs.getInt("STATUS"));
                reLevelList.add(rl);
            }

        } catch (Exception ex) {
            logger.info("Error getRepairLevelsForImei Method ........"+ex.getMessage());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(reLevelList.size());
        mw.setData(reLevelList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }
    
}
