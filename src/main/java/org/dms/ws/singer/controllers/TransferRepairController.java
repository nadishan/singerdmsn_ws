/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.TransferRepair;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class TransferRepairController {

    public static MessageWrapper getTransferRepair(int trans_rep_no,
            String imei,
            int bis_id,
            int status,
            String fromdate,
            String todate,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getTransferRepair Method Call....................");

        ArrayList<TransferRepair> transferRepairList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.TransferRepair(order);

            String transRepairNo = ((trans_rep_no == 0) ? "" : "AND T.REP_TRANS_NO = " + trans_rep_no + "");
            String Imei = (imei.equalsIgnoreCase("all") ? "NVL(T.IMEI_NO,'AA') = NVL(T.IMEI_NO,'AA')" : "UPPER(T.IMEI_NO) LIKE UPPER('%" + imei + "%')");
            String bisId = (bis_id == 0 ? "" : "AND T.BIS_ID = " + bis_id +"");
            String Status = (status == 0 ? "" : "AND T.STATUS = " + status + "");
            String Range = todate.equalsIgnoreCase("all") ? "" : (fromdate.equalsIgnoreCase("all") ? "" : "AND T.TRANSFER_DATA >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND T.TRANSFER_DATA <= TO_DATE('" + todate + "','YYYY/MM/DD') ");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY T.REP_TRANS_NO" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "T.REP_TRANS_NO,T.IMEI_NO,"
                    + "T.BIS_ID,TO_CHAR(T.TRANSFER_DATA,'YYYY/MM/DD') TRANSFER_DATA,"
                    + "T.STATUS,D.PRODUCT,"
                    + "M.MODEL_DESCRIPTION";

            String whereClous = "T.IMEI_NO = D.IMEI_NO (+) "
                    + " AND D.MODEL_NO = M.MODEL_NO (+) "
                    + " AND T.STATUS != 9"
                    + " "+transRepairNo+" "
                    + " AND "+Imei+" "
                    + " "+bisId+" "
                    + " "+Status+" "
                    + " "+Range+"";


            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_TRANSFER_REPAIRS T,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_TRANSFER_REPAIRS T,SD_DEVICE_MASTER D,SD_MODEL_LISTS M WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                TransferRepair tr = new TransferRepair();
                totalrecode = rs.getInt("CNT");
                tr.setTransRepairNo(rs.getInt("REP_TRANS_NO"));
                tr.setImeiNo(rs.getString("IMEI_NO"));
                tr.setBisId(rs.getInt("BIS_ID"));
                tr.setTransferDate(rs.getString("TRANSFER_DATA"));
                tr.setStatus(rs.getInt("STATUS"));
                tr.setProduct(rs.getString("PRODUCT"));
                tr.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                transferRepairList.add(tr);
            }

        } catch (Exception ex) {
            logger.info("Error getTransferRepair Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(transferRepairList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    
    public static ValidationWrapper addTransferRepair(TransferRepair transrepair_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addTransferRepair Method Call.............................");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String insertColumns = "REP_TRANS_NO,"
                    + "IMEI_NO,"
                    + "BIS_ID,"
                    + "TRANSFER_DATA,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED,"
                    + "TO_USER";

            String insertValue = "(SELECT NVL(MAX(REP_TRANS_NO),0)+1 FROM SD_TRANSFER_REPAIRS),"
                    + "'" + transrepair_info.getImeiNo() + "',"
                    + "" + transrepair_info.getBisId() + ","
                    + "TO_DATE('" + transrepair_info.getTransferDate() + "','YYYY/MM/DD'),"
                    + "1,"
                    + "'" + user_id + "',"
                    + "SYSDATE,"
                    + ""+transrepair_info.getToUser()+"";

            try {

                dbCon.save(con,"INSERT INTO SD_TRANSFER_REPAIRS(" + insertColumns + ") "
                        + "VALUES(" + insertValue + ")");
                
                dbCon.save(con,"UPDATE SD_DEVICE_MASTER "
                        + "SET BIS_ID = "+transrepair_info.getToUser()+" "
                        + "WHERE IMEI_NO = '"+transrepair_info.getImeiNo()+"'");

                logger.info("Transfer Repair Inseted.....");
                result = 1;
            } catch (Exception e) {
                logger.info("Error Transfer Repair Inseted....." + e.toString());
                result = 9;
            }

           

        } catch (Exception ex) {
            logger.info("Error addTransferRepair Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Transfer Repair Insert Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Transfer Repair Insert");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    } 
    
  
     public static ValidationWrapper cancleTransferRepair(TransferRepair transrepair_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("cancleTransferRepair Method Call.............................");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

                dbCon.save(con,"UPDATE SD_TRANSFER_REPAIRS "
                        + "SET STATUS = 9,"
                        + "USER_MODIFIED = '"+user_id+"',"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE REP_TRANS_NO = "+transrepair_info.getTransRepairNo()+"");
                
                dbCon.save(con,"UPDATE SD_DEVICE_MASTER "
                        + "SET BIS_ID = "+transrepair_info.getBisId()+" "
                        + "WHERE IMEI_NO = '"+transrepair_info.getImeiNo()+"'");
                

                logger.info("Transfer Repair Update Successfully.....");
                result = 1;
                
        } catch (Exception ex) {
            logger.info("Error cancleTransferRepair Method..:" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Transfer Repair Cancel Successfully");
            logger.info("Transfer Repair Cancle Successfully.............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Transfer Repair Cancel");
            logger.info("Error in Transfer Repair Cancle.............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    } 
     
     
    
}
