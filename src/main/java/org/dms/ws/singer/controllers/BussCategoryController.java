/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BussinessCategory;
import org.dms.ws.singer.entities.User;

/**
 *
 * @author SDU
 */
public class BussCategoryController {

    public static MessageWrapper getBussinessCategorys() {
        logger.info("getBussinessCategorys Method Call......");
        ArrayList<BussinessCategory> bcList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ResultSet rs = dbCon.search(con, "SELECT BCAT_ID,"
                    + "BCAT_NAME,"
                    + "BCAT_DESC,"
                    + "STATUS,"
                    + "REMARKS,"
                    + "(SELECT COUNT(*) FROM SD_BUSINESS_CATEGORIES WHERE STATUS=1) CNT "
                    + "FROM SD_BUSINESS_CATEGORIES "
                    + "WHERE STATUS=1");
            logger.info("Get Data to ResultSet.........");
            while (rs.next()) {
                BussinessCategory bc = new BussinessCategory();
                bc.setBsCatId(rs.getInt("BCAT_ID"));
                bc.setBsCateName(rs.getString("BCAT_NAME"));
                bc.setBsCateDesc(rs.getString("BCAT_DESC"));
                bc.setBsCateStatus(rs.getInt("STATUS"));
                bc.setBsCateRemarks(rs.getString("REMARKS"));
                totalrecode = rs.getInt("CNT");
                bcList.add(bc);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessCategory...:" + ex.toString());
        }
        logger.info("Redy Return Values............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(bcList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------");
        return mw;

    }

}
