/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.mail.MessagingException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.AcceptDebitNoteController.addacceptDebitDetials;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ImeiTransferList;
import org.dms.ws.singer.entities.ImeiUploadTransferList;
import org.dms.ws.singer.entities.ImportDebitNote;
import org.dms.ws.singer.entities.ImportDebitNoteDetails;
import org.dms.ws.singer.entities.QuickWorkOrder;
import org.dms.ws.singer.entities.WarrentyRegistration;
import org.dms.ws.singer.utils.CommonTask;
import org.dms.ws.singer.utils.EmaiSendConfirmCode;

/**
 *
 * @author shk
 */
public class ImeisFromExcelController {

    public static int insertImeis(ArrayList<ImeiTransferList> imeiNos) {
        int result = 1;
        try {
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            ///Taking Existing Sites
            // ArrayList<String> siteList = getBackDateSites(dbconn, con);
            String selectColumns = "IMEI_NO,"
                    + "BIS_ID,"
                    + "STATUS";

            String values = "?,?,?";

            PreparedStatement ps = dbCon.prepare(con, "INSERT INTO TEMP_IMEI(" + selectColumns + ") "
                    + "VALUES(" + values + ")");

            for (ImeiTransferList s : imeiNos) {
                //  if(!(siteList.contains(s.getSiteId()))){
                ps.setString(1, s.getImeiNo());
                ps.setInt(2, s.getBisId());
                ps.setString(3, "NOT RECEIVED");

                ps.addBatch();
                //}                
            }

            ps.executeBatch();

//            String updateQry = " UPDATE TEMP_IMEI "
//                    + " SET IMEI_NO = ?, "
//                    + "     BIS_ID = ?,  "
//                    + "     STATUS = ?    ";
////                    + " WHERE  SITEID = ? ";
//
//             PreparedStatement psUpdate = dbCon.prepare(con, updateQry);
//             
//             for (ImeiTransferList s : imeiNos) {
//              //   if(siteList.contains(s.getSiteId())){                     
//                   ps.setInt(1, s.getImeiNo());
//                   ps.setInt(2, s.getBisId());
//                   ps.setString(3, s.getStatus());
//                   
//                    
//                    psUpdate.addBatch();
//              //   }
//             }
//             psUpdate.executeBatch();
            dbCon.ConectionClose(con);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = 9;
        }
        return result;
    }

    //=================UPLOAD IMEI INSERT=======================
    public static int insertUploadImeis(ArrayList<ImeiUploadTransferList> imeiNos) {
        int result = 1;
        try {
            System.out.println("UPLOAD zzz1 ");
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            ///Taking Existing Sites
            // ArrayList<String> siteList = getBackDateSites(dbconn, con);
            String selectColumns = "IMEI_NO,"
                    + "REASON";

            String values = "?,?";

            PreparedStatement ps = dbCon.prepare(con, "INSERT INTO SD_TEMP_IMEI(" + selectColumns + ") "
                    + "VALUES(" + values + ")");

            for (ImeiUploadTransferList s : imeiNos) {
                //  if(!(siteList.contains(s.getSiteId()))){
                ps.setString(1, s.getImeiNo());
                ps.setString(2, s.getReason());

                ps.addBatch();
                //}  
//                System.out.println("UPLOAD QRY>>> "+imeiNos);
            }

            ps.executeBatch();

//            String updateQry = " UPDATE TEMP_IMEI "
//                    + " SET IMEI_NO = ?, "
//                    + "     BIS_ID = ?,  "
//                    + "     STATUS = ?    ";
////                    + " WHERE  SITEID = ? ";
//
//             PreparedStatement psUpdate = dbCon.prepare(con, updateQry);
//             
//             for (ImeiTransferList s : imeiNos) {
//              //   if(siteList.contains(s.getSiteId())){                     
//                   ps.setInt(1, s.getImeiNo());
//                   ps.setInt(2, s.getBisId());
//                   ps.setString(3, s.getStatus());
//                   
//                    
//                    psUpdate.addBatch();
//              //   }
//             }
//             psUpdate.executeBatch();
            dbCon.ConectionClose(con);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = 9;
        }
        return result;
    }
    //----------------END UPLOAD IMEI INSERT--------------------------

    private static final int startColumn = 0;
    private static final int siteColumn = 0;
    private static final int imeiColumn = 1;
    private static final int bisIdColumn = 2;
    private static final int statusColumn = 3;

    private static final String imeiNoColumnHeaderName = "Imei No";
    private static final String bisIdColumnHeaderName = "Bis Id";
    private static final String statusColumnHeaderName = "Status";
    private static final String reasonColumnHeaderName = "Reason";

    public static int getImeisFromExcelSheet(String excelBase64) throws Exception {
        deleteImeis();
        int response = 1;
        ArrayList<ImeiTransferList> imeiTransferListUpload = new ArrayList<ImeiTransferList>();

        try {
            excelBase64 = excelBase64.split(",")[1];
            byte[] bb = new sun.misc.BASE64Decoder().decodeBuffer(excelBase64);
//      
//
            InputStream myInputStream = new ByteArrayInputStream(bb);
            XSSFWorkbook workbook = new XSSFWorkbook(myInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFSheet sheetH = workbook.getSheetAt(0);
            Row rowH = sheetH.iterator().next();
            Iterator<Cell> cellIteratorH = rowH.cellIterator();
            System.out.println("222222222222222222222");

            String imeiNoColumnHeader = "";
            String bisIdColumnHeader = "";
            String statusColumnHeader = "";
            for (Cell rowH1 : rowH) {
                System.out.println("RRR " + rowH1.getStringCellValue());

                if (rowH1.getStringCellValue().equals("Imei No")) {
                    imeiNoColumnHeader = rowH1.getStringCellValue();
                }

                if (rowH1.getStringCellValue().equals("Bis Id")) {
                    bisIdColumnHeader = rowH1.getStringCellValue();
                }

                if (rowH1.getStringCellValue().equals("Status")) {
                    statusColumnHeader = rowH1.getStringCellValue();
                }

//                if (hc == statusColumn) {
//                    switch (cell.getCellType()) {
//                        case Cell.CELL_TYPE_NUMERIC:
//                            statusColumnHeader = (String.valueOf(cell.getNumericCellValue()));
//                            System.out.println("COL 5 " + cell.getNumericCellValue());
//                            break;
//                        case Cell.CELL_TYPE_STRING:
//                            statusColumnHeader = (cell.getStringCellValue());
//                            System.out.println("COL 6 " + cell.getStringCellValue());
//                            break;
//                    }
//                }
            }

            //  int hc = 0;
//            while (cellIteratorH.hasNext()) {
//                Cell cell = cellIteratorH.next();
//                if (hc == imeiColumn) {
//                    System.out.println(cell.getColumnIndex());
//                    switch (cell.getColumnIndex() - 1) {
//                        case 0:
//                            imeiNoColumnHeader = (String.valueOf(cell.getStringCellValue()));
//                            System.out.println("COL 1 " + cell.getStringCellValue());
//                            break;
//                        case 1:
//                            imeiNoColumnHeader = (cell.getStringCellValue());
//                            System.out.println("COL 2 " + cell.getStringCellValue());
//                            break;
//                    }
//                }
//
//                if (hc == bisIdColumn) {
//                    switch (cell.getCellType()) {
//                        case Cell.CELL_TYPE_NUMERIC:
//                            bisIdColumnHeader = (String.valueOf(cell.getNumericCellValue()));
//                            System.out.println("COL 3 " + cell.getNumericCellValue());
//                            break;
//                        case Cell.CELL_TYPE_STRING:
//                            bisIdColumnHeader = (cell.getStringCellValue());
//                            System.out.println("COL 4 " + cell.getStringCellValue());
//                            break;
//                    }
//                }
//
//                if (hc == statusColumn) {
//                    switch (cell.getCellType()) {
//                        case Cell.CELL_TYPE_NUMERIC:
//                            statusColumnHeader = (String.valueOf(cell.getNumericCellValue()));
//                            System.out.println("COL 5 " + cell.getNumericCellValue());
//                            break;
//                        case Cell.CELL_TYPE_STRING:
//                            statusColumnHeader = (cell.getStringCellValue());
//                            System.out.println("COL 6 " + cell.getStringCellValue());
//                            break;
//                    }
//                }
//
//                hc++;
//            }
            if (imeiNoColumnHeader.equalsIgnoreCase(imeiNoColumnHeaderName) && bisIdColumnHeader.equalsIgnoreCase(bisIdColumnHeaderName) && statusColumnHeader.equalsIgnoreCase(statusColumnHeaderName)) {

                Iterator<Row> rowIterator = sheet.iterator();

                int i = 0;

                while (rowIterator.hasNext()) {

                    Row row = rowIterator.next();

                    if (i > startColumn) {

                        ImeiTransferList imeiNoList = new ImeiTransferList();
                        Iterator<Cell> cellIterator = row.cellIterator();
                        Cell cell = row.getCell(0);
//                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        int j = 0;

                        for (Cell row1 : row) {

                            if (row1.getColumnIndex() == 0) {
                                try {

//                                imeiNoList.setImeiNo(String.valueOf(row1.getNumericCellValue()));
                                    //   imeiNoList.setImeiNo(row1.getStringCellValue());
                                    imeiNoList.setImeiNo(NumberToTextConverter.toText(cell.getNumericCellValue()));

                                } catch (Exception e) {
                                    System.out.println(" EX =>>>>>" + e.toString());
                                }
//                                 //  imeiNoList.setImeiNo(""+Long.parseLong(row1.getStringCellValue()));
//                                System.out.println("Imei 33 " + Long.parseLong(row1.getStringCellValue()));
                            }

                            if (row1.getColumnIndex() == 1) {

                                imeiNoList.setBisId((int) row1.getNumericCellValue());
                                System.out.println("Imei 4 " + row1.getNumericCellValue());
                            }

                            if (row1.getColumnIndex() == 2) {

                                imeiNoList.setStatus(row1.getStringCellValue());
//                                cell.setCellValue("NOT RECEIVED");
//                                imeiNoList.setStatus("NOT RECEIVED");
                            }
                        }
//                        
//                        while (cellIterator.hasNext()) {
//                            Cell cell = cellIterator.next();
//                            if (j == imeiColumn) {
//                                switch (cell.getCellType()) {
//                                    case Cell.CELL_TYPE_NUMERIC:
//                                        //  imeiNoList.setImeiNo(Integer.valueOf(cell.getColumnIndex()));
//                                        imeiNoList.setImeiNo((int) cell.getNumericCellValue());
//                                        System.out.println("Imei 1 " + cell.getNumericCellValue());
//                                        break;
//                                    case Cell.CELL_TYPE_STRING:
//                                        imeiNoList.setImeiNo(Integer.parseInt(cell.getStringCellValue()));
//                                        System.out.println("Imei 2 " + cell.getColumnIndex());
//                                        break;
//                                }
//                            }
//                            
//                            if (j == bisIdColumn) {
//                                switch (cell.getCellType()) {
//                                    case Cell.CELL_TYPE_NUMERIC:
//                                        // String mydateStr = df.format(cell.getDateCellValue());
//                                        imeiNoList.setBisId(cell.getColumnIndex());
//                                        System.out.println("Imei 3 " + cell.getColumnIndex());
//                                        break;
//                                    case Cell.CELL_TYPE_STRING:
//                                        imeiNoList.setBisId(Integer.parseInt(cell.getStringCellValue()));
//                                        System.out.println("Imei 4 " + cell.getStringCellValue());
//                                        break;
//                                }
//                            }
//                            
//                            if (j == statusColumn) {
//                                switch (cell.getCellType()) {
//                                    case Cell.CELL_TYPE_NUMERIC:
////                                        String mydateStr = df.format(cell.getDateCellValue());
//                                        imeiNoList.setStatus(String.valueOf(cell.getNumericCellValue()));
//                                        break;
//                                    case Cell.CELL_TYPE_STRING:
//                                        imeiNoList.setStatus(cell.getStringCellValue());
//                                        break;
//                                }
//                            }
//                            
//                            j++;
//                            System.out.println("JJJJj" + j);
//                        }
                        imeiTransferListUpload.add(imeiNoList);
                    }

                    i++;
                }

            } else {
                response = 4;
            }

            if (imeiTransferListUpload.size() > 0) {
                response = insertImeis(imeiTransferListUpload);

            }

        } catch (Exception e) {
            e.printStackTrace();
            response = 9;
        }
        return response;
    }

    //=============IMEIS UPLOAD ===========================
    public static int getImeisFromExcelSheetUpload(String excelBase64) throws Exception {
        deleteImeisUp();
        // insertInvalidImeis();
        int response = 1;
        ArrayList<ImeiUploadTransferList> imeiTransferListUpload = new ArrayList<ImeiUploadTransferList>();

        try {
            excelBase64 = excelBase64.split(",")[1];
            byte[] bb = new sun.misc.BASE64Decoder().decodeBuffer(excelBase64);
//      
//
            InputStream myInputStream = new ByteArrayInputStream(bb);
            XSSFWorkbook workbook = new XSSFWorkbook(myInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFSheet sheetH = workbook.getSheetAt(0);
            Row rowH = sheetH.iterator().next();
            Iterator<Cell> cellIteratorH = rowH.cellIterator();
            // System.out.println("222222222222222222222");

            String imeiNoColumnHeader = "";
            // String bisIdColumnHeader = "";
            String statusColumnHeader = "";
            for (Cell rowH1 : rowH) {
                //   System.out.println("RRR " + rowH1.getStringCellValue());

                if (rowH1.getStringCellValue().equals("Imei No")) {
                    imeiNoColumnHeader = rowH1.getStringCellValue();
                }

//                if (rowH1.getStringCellValue().equals("Bis Id")) {
//                    bisIdColumnHeader = rowH1.getStringCellValue();
//                }
                if (rowH1.getStringCellValue().equals("Reason")) {
                    statusColumnHeader = rowH1.getStringCellValue();
                }

            }

            if (imeiNoColumnHeader.equalsIgnoreCase(imeiNoColumnHeaderName) && statusColumnHeader.equalsIgnoreCase(reasonColumnHeaderName)) {

                Iterator<Row> rowIterator = sheet.iterator();

                int i = 0;

                while (rowIterator.hasNext()) {

                    Row row = rowIterator.next();

                    if (i > startColumn) {

                        ImeiUploadTransferList imeiNoList = new ImeiUploadTransferList();
                        Iterator<Cell> cellIterator = row.cellIterator();
                        Cell cell = row.getCell(0);
//                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        int j = 0;

                        for (Cell row1 : row) {

                            if (row1.getColumnIndex() == 0) {
                                try {

                                    imeiNoList.setImeiNo(NumberToTextConverter.toText(cell.getNumericCellValue()));

                                } catch (Exception e) {
                                    //  System.out.println(" EX =>>>>>" + e.toString());
                                }

                            }

                            if (row1.getColumnIndex() == 1) {

                               // imeiNoList.setReason(row1.getStringCellValue());
                                // imeiNoList.setReason(row1.getStringCellValue());
                                imeiNoList.setReason(imeiNoList.getReason());
                               // System.out.println(" EX1 =>>>>>" + row1.getStringCellValue());

                            }
                        }

                        imeiTransferListUpload.add(imeiNoList);
                    }

                    i++;
                }

            } else {
                response = 4;
            }

            if (imeiTransferListUpload.size() > 0) {
                response = insertUploadImeis(imeiTransferListUpload);

            }

        } catch (Exception e) {
            e.printStackTrace();
            response = 9;
        }
        return response;
    }
    //-------------END IMEIS UPLOAD -----------------

    //Delete Imeis
    public static int deleteImeis() throws Exception {
        int response = 1;

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        PreparedStatement ps = dbCon.prepare(con, "truncate table TEMP_IMEI");

        ps.executeQuery();
        dbCon.ConectionClose(con);
        return response;
    }//updateMaster

    //End Delete Imeis
    //Update Master
    public static int updateMaster(String excelBase64) throws Exception {

        int response = 1;

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        PreparedStatement ps = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET BIS_ID = 226 WHERE IMEI_NO IN (SELECT D.IMEI_NO FROM SD_DEVICE_MASTER D, TEMP_IMEI T WHERE D.IMEI_NO = T.IMEI_NO)");

        ps.executeQuery();
        return response;
    }

    //End Update Master
    //Insert data to a Excelsheet
    public static String putImeisToExcelSheet(String excelBase64) throws FileNotFoundException, IOException, Exception {

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        File file = null;
//      Class.forName("com.mysql.jdbc.Driver");
//      Connection connect = DriverManager.getConnection( 
//      "jdbc:mysql://localhost:3306/test" , 
//      "root" , 
//      "root"
//      );

//            PreparedStatement ps = dbCon.prepare(con);
//                  ps.executeQuery();
        String qry = "SELECT * FROM TEMP_IMEI  A WHERE A.IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER ) AND A.IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEBIT_NOTE_DETAIL)";
        ResultSet rs = dbCon.search(con, qry);
//            PreparedStatement ps = dbCon.prepare(con, "INSERT INTO TEMP_IMEI "
//                    + "VALUES(" + values + ")");

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet("employe db");
        XSSFRow row = spreadsheet.createRow(1);

        XSSFCell cell;

        cell = row.createCell(1);
        cell.setCellValue("IMEI_NO");
        cell = row.createCell(2);
        cell.setCellValue("BIS_ID");
        cell = row.createCell(3);
        cell.setCellValue("STATUS");

        int i = 2;
        while (rs.next()) {
            row = spreadsheet.createRow(i);
            cell = row.createCell(1);
            cell.setCellValue(rs.getString("IMEI_NO"));
            cell = row.createCell(2);
            cell.setCellValue(rs.getInt("BIS_ID"));
            cell = row.createCell(3);
            cell.setCellValue(rs.getString("STATUS"));

            i++;
        }

        try {
//            String path = getServletContext().getRealPath("WEB-INF/../");
            file = new File("\\new.xls");

            System.out.println(file.getCanonicalPath());
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(
                "exceldatabase.xlsx written successfully");

        return file.getCanonicalPath();
    }
    //End Insert data to a Excelsheet

    //UPLOAD IMIES EXCEPTION
    public static String putUploadImeisToExcelSheet(String excelBase64) throws FileNotFoundException, IOException, Exception {

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        File file = null;

//      Class.forName("com.mysql.jdbc.Driver");
//      Connection connect = DriverManager.getConnection( 
//      "jdbc:mysql://localhost:3306/test" , 
//      "root" , 
//      "root"
//      );
//            PreparedStatement ps = dbCon.prepare(con);
//                  ps.executeQuery();
        String qry = "SELECT * FROM SD_TEMP_IMEI WHERE IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER)";
        ResultSet rs = dbCon.search(con, qry);
//            PreparedStatement ps = dbCon.prepare(con, "INSERT INTO TEMP_IMEI "
//                    + "VALUES(" + values + ")");

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet("employe db");
        XSSFRow row = spreadsheet.createRow(1);

        XSSFCell cell;

        cell = row.createCell(1);
        cell.setCellValue("IMEI_NO");
        cell = row.createCell(2);
        cell.setCellValue("REASON");
//        cell = row.createCell(3);
//        cell.setCellValue("STATUS");

        int i = 2;
        while (rs.next()) {
            row = spreadsheet.createRow(i);
            cell = row.createCell(1);
            cell.setCellValue(rs.getString("IMEI_NO"));
            cell = row.createCell(2);
            cell.setCellValue(rs.getInt("STATUS"));
//            cell = row.createCell(3);
//            cell.setCellValue(rs.getString("STATUS"));

            i++;
        }

        try {
//            String path = getServletContext().getRealPath("WEB-INF/../");
            file = new File("\\new.xls");

            System.out.println(file.getCanonicalPath());
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(
                "exceldatabase.xlsx written successfully");

        return file.getCanonicalPath();
    }
    //END UPLOAD IMIES EXCEPTION

    public static org.dms.ldap.util.ResponseWrapper updateBisId(ImeiTransferList imeitr) {
        org.dms.ldap.util.ResponseWrapper rw = new org.dms.ldap.util.ResponseWrapper();
        DbCon dbconn = new DbCon();
        Connection con = dbconn.getCon();
        ArrayList<String> successMsg = new ArrayList<>();
        try {

            // PreparedStatement ps = dbconn.prepare(con, "UPDATE TEMP_IMEI SET " + selectColumns + "  WHERE BIS_ID = ? ");
            PreparedStatement ps = dbconn.prepare(con, "UPDATE SD_DEVICE_MASTER SET BIS_ID = 226 WHERE IMEI_NO IN (SELECT D.IMEI_NO FROM SD_DEVICE_MASTER D, TEMP_IMEI T WHERE D.IMEI_NO = T.IMEI_NO");

            ps.setString(1, imeitr.getImeiNo());
            ps.setInt(2, imeitr.getBisId());
            ps.setString(3, imeitr.getStatus());

            ps.executeUpdate();

            rw.setFlag("100");
            successMsg.add("Sucessfully Updated.");

            String um = "";

            rw.setSuccessMessages(successMsg);

            dbconn.ConectionClose(con);
        } catch (Exception ex) {
            rw.setFlag("999");
            successMsg.add("Update is not successful. " + ex.getMessage());
            rw.setSuccessMessages(successMsg);
        } finally {
            dbconn.ConectionClose(con);
        }
        return rw;
    }

    //Update Debit Note Detail Bis Id
    public static ValidationWrapper updateDebitNoteBisID(int bisId) throws Exception {

        org.dms.ldap.util.ResponseWrapper rw = new org.dms.ldap.util.ResponseWrapper();
        logger.info("updateDebitNoteBisID Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        // dbCon.offAutoCommit(con);
        try {
            // 1
            String sqlForDefaultId = "UPDATE SD_DEVICE_MASTER "
                    + "SET BIS_ID = 226 "
                    + "WHERE IMEI_NO NOT IN (SELECT IMEI_NO FROM TEMP_IMEI WHERE BIS_ID = '" + bisId + "' ) AND BIS_ID = '" + bisId + "'";

            dbCon.save(con, sqlForDefaultId);
            // 2
            String sqlForReceived = "UPDATE TEMP_IMEI SET STATUS = 'RECEIVED' "
                    + "WHERE IMEI_NO IN ("
                    + "SELECT IMEI_NO "
                    + "FROM( SELECT D.* FROM SD_DEVICE_MASTER D, TEMP_IMEI T WHERE T.IMEI_NO = D.IMEI_NO) M "
                    + "WHERE M.BIS_ID = 226 OR M.BIS_ID IN (SELECT BIS_ID FROM  SD_BUSINESS_STRUCTURES CONNECT BY PRIOR  BIS_ID = PARENT_BIS_ID START WITH BIS_ID = '" + bisId + "')"
                    + ")";

            dbCon.save(con, sqlForReceived);

            // 3
            String sqlForReceivedMaster = "UPDATE SD_DEVICE_MASTER SET BIS_ID = '" + bisId + "'"
                    + "WHERE IMEI_NO IN ("
                    + "SELECT IMEI_NO "
                    + "FROM( SELECT D.* FROM SD_DEVICE_MASTER D, TEMP_IMEI T WHERE T.IMEI_NO = D.IMEI_NO) M "
                    + "WHERE M.BIS_ID = 226 OR M.BIS_ID IN (SELECT BIS_ID FROM  SD_BUSINESS_STRUCTURES CONNECT BY PRIOR  BIS_ID = PARENT_BIS_ID START WITH BIS_ID = '" + bisId + "')"
                    + ")";

            dbCon.save(con, sqlForReceivedMaster);

            // 4
            String sqlForIssued = "UPDATE TEMP_IMEI SET STATUS = 'ISSUED' "
                    + "WHERE IMEI_NO IN ("
                    + "SELECT IMEI_NO "
                    + "FROM( SELECT D.* FROM SD_DEVICE_MASTER D, TEMP_IMEI T WHERE T.IMEI_NO = D.IMEI_NO ) M "
                    + "WHERE M.BIS_ID != 226 AND M.BIS_ID NOT IN (SELECT BIS_ID FROM  SD_BUSINESS_STRUCTURES CONNECT BY PRIOR  BIS_ID = PARENT_BIS_ID START WITH BIS_ID = '" + bisId + "')"
                    + ")";

            dbCon.save(con, sqlForIssued);

            // 5
            String sqlForUpdateDebitNote = "UPDATE SD_DEBIT_NOTE_DETAIL M SET BIS_ID = " + bisId + " "
                    + "WHERE M.IMEI_NO IN ( "
                    + "SELECT D.IMEI_NO FROM SD_DEBIT_NOTE_DETAIL D WHERE D.IMEI_NO IN (SELECT IMEI_NO FROM TEMP_IMEI WHERE BIS_ID ='" + bisId + "' "
                    + "AND IMEI_NO NOT IN ("
                    + "SELECT IMEI_NO FROM SD_DEVICE_MASTER)"
                    + ")"
                    + ")";

            dbCon.save(con, sqlForUpdateDebitNote);

//            String sqlForUpdateDebitNoteDefault = "UPDATE SD_DEBIT_NOTE_DETAIL M SET BIS_ID = " + bisId + " "
//                    + "WHERE M.IMEI_NO IN ( "
//                    + "SELECT D.IMEI_NO FROM SD_DEBIT_NOTE_DETAIL D WHERE D.IMEI_NO IN (SELECT IMEI_NO FROM TEMP_IMEI WHERE IMEI_NO NOT IN ("
//                    + "SELECT IMEI_NO FROM SD_DEVICE_MASTER)"
//                    + ")"
//                    + ")";
//
//            dbCon.save(con, sqlForUpdateDebitNoteDefault);
            // 6
            String sqlForUpdateToBeReceived = "UPDATE TEMP_IMEI M SET STATUS = 'TO BE RECEIVED' "
                    + "WHERE M.IMEI_NO  IN ( SELECT D.IMEI_NO FROM SD_DEBIT_NOTE_DETAIL D WHERE D.IMEI_NO IN (SELECT IMEI_NO FROM TEMP_IMEI WHERE IMEI_NO NOT IN ("
                    + "SELECT IMEI_NO FROM SD_DEVICE_MASTER)))"
                    + "AND M.BIS_ID = '" + bisId + "' ";

            dbCon.save(con, sqlForUpdateToBeReceived);

//            String sqlForDeviceAcceptDebit = "UPDATE SD_DEVICE_MASTER SET STATUS =3, BIS_ID = '" + bisId + "' WHERE IMEI_NO IN ( "
//                    + "SELECT IMEI_NO "
//                    + "FROM TEMP_IMEI "
//                    + "WHERE IMEI_NO IN (SELECT IMEI_NO FROM SD_DEBIT_NOTE_DETAIL) "
//                    + "AND IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER ) "
//                    + "AND BIS_ID = '" + bisId + "')";
//
//            dbCon.save(con, sqlForDeviceAcceptDebit);
            // 7,8
            String imeiMasterColumn = "SELECT * FROM SD_DEBIT_NOTE_DETAIL D WHERE D.IMEI_NO IN (SELECT IMEI_NO FROM TEMP_IMEI WHERE IMEI_NO NOT IN ("
                    + "SELECT IMEI_NO FROM SD_DEVICE_MASTER))";

            ImportDebitNote debit_note = new ImportDebitNote();
            ImportDebitNoteDetails debitDetails = new ImportDebitNoteDetails();

            dbCon.prepare(con, imeiMasterColumn);
            ResultSet rs = dbCon.search(con, imeiMasterColumn);

            while (rs.next()) {

                String debitNoteNo = rs.getString("DEBIT_NOTE_NO");

                ArrayList<ImportDebitNoteDetails> imeiList = new ArrayList<>();

                String imeiMasterColumn2 = "SELECT * FROM SD_DEBIT_NOTE_DETAIL D WHERE D.IMEI_NO IN (SELECT IMEI_NO FROM TEMP_IMEI WHERE IMEI_NO NOT IN ("
                        + "SELECT IMEI_NO FROM SD_DEVICE_MASTER)) AND DEBIT_NOTE_NO = '" + debitNoteNo + "' ";
                ResultSet rs2 = dbCon.search(con, imeiMasterColumn2);

                while (rs2.next()) {

                    System.out.println("zzzDate >>>>>>>>>>>>>>> " + rs2.getString("DATE_ACCEPTED"));

                    debitDetails.setDebitNoteNo(rs2.getString("DEBIT_NOTE_NO"));
                    debitDetails.setImeiNo(rs2.getString("IMEI_NO"));
                    debitDetails.setCustomerCode(rs2.getString("CUSTOMER_CODE"));
                    debitDetails.setShopCode(rs2.getString("SHOP_CODE"));
                    debitDetails.setModleNo(rs2.getInt("MODEL_NO"));
                    debitDetails.setBisId(rs2.getInt("BIS_ID"));
                    debitDetails.setOrderNo(rs2.getString("ORDER_NO"));
                    debitDetails.setSalerPrice(rs2.getInt("SALES_PRICE"));
                    debitDetails.setDateAccepted("TO_DATE(SYSDATE, 'YYYY/MM/DD')");
                    debitDetails.setStatus(rs2.getInt("STATUS"));
                    debitDetails.setRemarks(rs2.getString("REMARKS"));
                    debitDetails.setUser(rs2.getString("USER_INSERTED"));
                    debitDetails.setSite(rs2.getString("SITE"));
                    debitDetails.setBrand("");
                    debitDetails.setProduct("");
                    debitDetails.setModleDesc("");

                    System.out.println("DebitDetails BISID -->" + debitDetails.getBisId());
                    System.out.println("DebitDetails DebitNote -->" + debitDetails.getDebitNoteNo());

                    imeiList.add(debitDetails);

                }
                System.out.println("Imei List count zzzzz" + imeiList.size());

                debit_note.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                debit_note.setBisId(rs.getInt("BIS_ID"));
                debit_note.setImportDebitList(imeiList);
                debit_note.setUser(rs.getString("USER_INSERTED"));
                debit_note.setCustomerNo("");
                debit_note.setInvoiceNo("");
                debit_note.setOrderNo("");
                debit_note.setDeleveryDate("TO_DATE(SYSDATE, 'YYYY/MM/DD')");

                debit_note.setPartNo("");
                debit_note.setStatusMsg("");
                debit_note.setOrderQty(0);
                debit_note.setStatus(0);
                debit_note.setContactNo("");
                debit_note.setDeleveryQty(0);
                debit_note.setHeadStatus(0);
                debit_note.setSiteDescription("");

                System.out.println("Imeis list" + debit_note);
                System.out.println("count " + debit_note.getImportDebitList().size());
                //System.out.println("" + debitDetails.getImeiNo());
                System.out.println("zzdebit_note BISID -->" + debit_note.getBisId());
                System.out.println("zzdebit_note DebitNote -->" + debit_note.getDebitNoteNo());

                AcceptDebitNoteController.addacceptDebitDetials(debit_note, "", "", "", "", "", "", "");

            }
//                imeiList.add(debitDetails);
//
//                debit_note.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
//                debit_note.setBisId(rs.getInt("BIS_ID"));
//                debit_note.setImportDebitList(imeiList);
//                debit_note.setUser(rs.getString("USER_INSERTED"));
//                debit_note.setCustomerNo("");
//                debit_note.setInvoiceNo("");
//                debit_note.setOrderNo("");
//                debit_note.setDeleveryDate("");
//                debit_note.setPartNo("");
//                debit_note.setStatusMsg("");
//                debit_note.setOrderQty(0);
//                debit_note.setStatus(0);
//                debit_note.setContactNo("");
//                debit_note.setDeleveryQty(0);
//                debit_note.setHeadStatus(0);
//                debit_note.setSiteDescription("");

//            System.out.println("Imeis list"+ debit_note);
//            System.out.println("count " + debit_note.getImportDebitList().size());
//            //System.out.println("" + debitDetails.getImeiNo());
//            System.out.println("zzdebit_note BISID -->"+ debit_note.getBisId());
//            System.out.println("zzdebit_note DebitNote -->"+ debit_note.getDebitNoteNo());
            // 9
            String sqlForNotReceivedImei = "UPDATE TEMP_IMEI SET STATUS ='NOT RECEIVED' WHERE IMEI_NO IN ("
                    + " SELECT (IMEI_NO) "
                    + " FROM TEMP_IMEI WHERE IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEBIT_NOTE_DETAIL ) "
                    + " AND IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER ) AND BIS_ID = '" + bisId + "' )";

            dbCon.save(con, sqlForNotReceivedImei);
            result = 1;

            logger.info("updateDebitNoteBisID ZZZZZZZZZZZZZZZZZZ.......");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in updateDebitNoteBisID Mehtod  :" + ex.toString());
            result = 9;
        } finally {
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Update Debit Note Bis ID is Successfully updated.");
            logger.info("updateDebitNoteBisID Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Update Debit Note Bis ID Update");
            logger.info("Error in Update Debit Note Bis ID................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    //End Update Debit Note Deteil Bis Id
    //WARRATY REG IMEIS UPLOAD
    public static ValidationWrapper addUploadImiesWarrentyReg( //WarrentyRegistration wr_info
            //            String user_id,
            //            String room,
            //            String department,
            //            String branch,
            //            String countryCode,
            //            String division,
            //            String organaization
            ) throws Exception {

//        logger.info("addWarrentyRegistration Mehtod Call.......");
        int result = 0;
        int warrenty_reg = 0;
        int customer_reg = 0;
        int master_cnt = 0;
        int con_code = 0;
        int warany_reg_num = 0;
        int waranty_price = 0;
        int waranty_status = 0;
        int waranty_bis_id = 0;
        String warrant_imei_no = "";
        String waranty_user_inserted = "";
        int warrant_schem_id = 0;
        int statusFlag = 2;
//        DbCon dbCon = new DbCon();
//        Connection con = dbCon.getCon();

        try {

            insertInvalidImeis();
            
            DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
            ResultSet rs_master_cnt = dbCon.search(con, "SELECT T.IMEI_NO,COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER M ,SD_TEMP_IMEI T "
                    + "WHERE M.IMEI_NO = T.IMEI_NO GROUP BY T.IMEI_NO");

            while (rs_master_cnt.next()) {
                master_cnt = rs_master_cnt.getInt("CNT");
            }
            rs_master_cnt.close();

            if (master_cnt > 0) {

//                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
//                        + "FROM SD_WARRENTY_REGISTRATION W,SD_BUSINESS_STRUCTURES S,SD_TEMP_IMEI T,SD_DEVICE_MASTER M "
//                        + "WHERE S.BIS_STRU_TYPE_ID = 3 AND S.BIS_ID = M.BIS_ID AND W.IMEI_NO = T.IMEI_NO");
//
//                while (rs_cnt.next()) {
//                    warrenty_reg = rs_cnt.getInt("CNT");
//                }
//                rs_cnt.close();
//                if (warrenty_reg < 0) {
//                    result = 2;
//                } 
//            else {
                try {

                    ResultSet rs_wrt_reg = dbCon.search(con, "SELECT NVL(MAX(WRR_REG_REF_NO),0)+1 MAX "
                            + "FROM  SD_WARRENTY_REGISTRATION");

                    while (rs_wrt_reg.next()) {
                        warany_reg_num = rs_wrt_reg.getInt("MAX");
                    }
                    rs_wrt_reg.close();

//                        ResultSet rs_schem = dbCon.search(con, "SELECT D.*, M.WRN_SCHEME_ID "
//                                + "FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_TEMP_IMEI T "
//                                + "WHERE D.MODEL_NO = M.MODEL_NO "
//                                + "AND D.IMEI_NO = T.IMEI_NO");
                    ResultSet rs_schem = dbCon.search(con, "SELECT DISTINCT D.*, M.WRN_SCHEME_ID "
                            + "FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_TEMP_IMEI T,SD_BUSINESS_STRUCTURES S,SD_MODEL_LISTS ML "
                            + "WHERE D.MODEL_NO = M.MODEL_NO "
                            + "AND D.IMEI_NO = T.IMEI_NO AND D.BIS_ID = S.BIS_ID AND S.BIS_STRU_TYPE_ID = 2 AND D.BIS_ID != 9999 AND T.REASON = 1");

                    while (rs_schem.next()) {
                        
                        
                        
                        DbCon dbCon2 = new DbCon();
                        Connection con2 = dbCon2.getCon();

                        warrant_schem_id = rs_schem.getInt("WRN_SCHEME_ID");
                        warrant_imei_no = rs_schem.getString("IMEI_NO");
                        waranty_price = rs_schem.getInt("SALES_PRICE");
                        waranty_bis_id = rs_schem.getInt("BIS_ID");
                        //waranty_status = rs_schem.getInt("STATUS");
                        waranty_user_inserted = rs_schem.getString("USER_INSERTED");
//                        System.out.println("WARREG IMEI " + warrant_imei_no);
//                            System.out.println("warrant_imei_no "+warrant_imei_no);
//                            warrant_schem_id = 
//                            warrant_schem_id =
//                            warrant_schem_id = 
                        String column = "WRR_REG_REF_NO,IMEI_NO,"
                                + "SELLING_PRICE,BIS_ID,STATUS,USER_INSERTED,"
                                + "DATE_INSERTED,WARANTY_TYPE,WRT_REG_FLAG";

//                        String imeiRegisterDate = " TO_DATE('" + wr_info.getRegisterDate() + "','YYYY/MM/DD'), "; ////REGISTER_DATE is set to registration date coming from the front end. Bz, Warrannty period is calculating from the REGISTER_DATE value 
//                        if (wr_info.getRegisterDate() == null || wr_info.getRegisterDate().length() == 0) {
//                            imeiRegisterDate = " SYSDATE, ";
//                        }
//                              System.out.println("STATUS FLAG "+statusFlag);
                        String values = "'" + warany_reg_num + "',"
                                + "'" + warrant_imei_no + "',"
                                + "'" + waranty_price + "',"
                                + "'" + waranty_bis_id + "',"
                                + "1,"
                                + "'" + waranty_user_inserted + "',"
                                + "SYSDATE,"
                                + "" + warrant_schem_id + ","
                                + "'" + statusFlag + "'";
//                        System.out.println("VAL "+values);

                        dbCon2.save(con2, "INSERT INTO SD_WARRENTY_REGISTRATION(" + column + ") VALUES(" + values + ")");

                        dbCon2.save(con2, "UPDATE SD_DEVICE_MASTER "
                                + "SET CUSTOMER_ID ='', "
                                + "STATUS =3, "
                                + "BIS_ID = 9999 "
                                + "WHERE  IMEI_NO = '" + warrant_imei_no + "'");
                        warany_reg_num++;
                        
                       
                        dbCon2.ConectionClose(con2);
                    }
                    rs_schem.close();
                    
//New Add
                    // WarrentyRegistration wr_info = new WarrentyRegistration();

//End New Add                        
//                        ResultSet rs_cus_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
//                                + "FROM SD_CUSTOMER_DETAILS "
//                                + "WHERE CUSTOMER_NIC = '" + wr_info.getCustomerNIC() + "'");
//
//                        while (rs_cus_cnt.next()) {
//                            customer_reg = rs_cus_cnt.getInt("CNT");
//                        }
//                        rs_cus_cnt.close();
//                        if (customer_reg > 0) {
//                            logger.info("This user already registered..............");
//                        } else {
//
//                            //String cus_nic = wr_info.getCustomerNIC();
//                            //String d = cus_nic.substring(0, 9);
//                            Random rand = new Random();
//                            con_code = CommonTask.getRandomInteger(0, Integer.MAX_VALUE, rand);
//                            //con_code = String.valueOf(rand.nextInt(Integer.parseInt(d)));
//
//                            
//                            dbCon.save(con, "INSERT INTO SD_CUSTOMER_DETAILS(CUSTOMER_NIC,"
//                                    + "CUSTOMER_NAME,"
//                                    + "DATE_OF_BIRTH,"
//                                    + "ADDRESS,"
//                                    + "CONTACT_NO,"
//                                    + "EMAIL,"
//                                    + "STATUS,"
//                                    + "USER_INSERTED,"
//                                    + "DATE_INSERTED,CONFIRM_CODE) VALUES('" + wr_info.getCustomerNIC() + "',"
//                                    + "'" + wr_info.getCustomerName() + "',"
//                                    + "TO_DATE('" + wr_info.getDateOfBirth() + "','YYYY/MM/DD'),"
//                                    + "'"+wr_info.getCustomerAddress()+"',"
//                                    + "'" + wr_info.getContactNo() + "',"
//                                    + "'" + wr_info.getEmail() + "',"
//                                    + "1,"
//                                    + "'" + user_id + "',"
//                                    + "SYSDATE,"
//                                    + "'" + con_code + "')");
//
//                            try {
//                                logger.info("Confirmation Code -" + con_code);
//                                EmaiSendConfirmCode.sendHTMLMail(wr_info.getEmail(), wr_info.getCustomerName(),Integer.toString(con_code));
//                            } catch (MessagingException ex) {
//                                logger.info("Error in E-mail Sending........" + ex.getMessage());
//                            }
//
//                        }
                    result = 1;
//             insertInvalidImeis();
                } catch (SQLException e) {
                    e.printStackTrace();
                    logger.info("Error in WarrentyRegistration insert  :" + e.toString());
                }

                // }
            } else {
                result = 3;
            }
            dbCon.ConectionClose(con);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in addWarrentyRegistration Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            //vw.setReturnRefNo(String.valueOf(warany_reg_num));
            vw.setReturnMsg("Warranty Registration Insert Successfully");
            logger.info("Warrernty Registration Insert Succesfully..........");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI is Already Registered");
            logger.info("This IMEI is already Registered..........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This IMEI is not in Master Record");
            logger.info("This IMEI is not in Master Record..........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Registration Insert");
            logger.info("Error in Warrernty Registration Insert................");
        }
        logger.info("Ready to Return Values..........");
//        dbCon.ConectionClose(con);
        
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    //END WARRATY REG IMEIS UPLOAD
    public static int deleteImeisUp() throws Exception {
        int response = 1;

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        PreparedStatement ps = dbCon.prepare(con, "truncate table SD_TEMP_IMEI");

        ps.executeQuery();
        dbCon.ConectionClose(con);
        return response;
    }//updateMaster

    //ADD STATUS TO INVALID IMEIS
    public static int insertInvalidImeis() throws Exception {
        int response = 1;
        int reg1 = 1;
        int reg2 = 2;
        int reg3 = 3;
        int master_cnt = 0;
        int al_reg = 0;
        int not_subdl = 0;
        String master_imei = "";
        String al_reg_imei = "";
        String not_subdl_imei = "";
        int al_reg_cnt = 0;
        int not_subdl_cnt = 0;
//        System.out.println("ZZZZ insertInvalidImeis");
        
        try {
            
        
        
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        ResultSet rs_mastr = dbCon.search(con, "SELECT IMEI_NO,count(IMEI_NO) CNT FROM SD_TEMP_IMEI WHERE IMEI_NO NOT IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER) GROUP BY IMEI_NO");
        ResultSet rs_al_reg = dbCon.search(con, "SELECT DISTINCT T.IMEI_NO,count(T.IMEI_NO) CNT FROM SD_WARRENTY_REGISTRATION W,SD_BUSINESS_STRUCTURES S,SD_TEMP_IMEI T,SD_DEVICE_MASTER M WHERE M.BIS_ID = S.BIS_ID AND W.IMEI_NO = T.IMEI_NO AND M.BIS_ID = 9999 AND T.REASON =1 GROUP BY T.IMEI_NO");
//        ResultSet rs_al_reg = dbCon.search(con, "SELECT IMEI_NO,count(IMEI_NO) CNT FROM  SD_TEMP_IMEI WHERE IMEI_NO IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE BIS_ID =9999 and IMEI_NO IN (SELECT IMEI_NO FROM SD_WARRENTY_REGISTRATION)) GROUP BY IMEI_NO");

//        ResultSet rs_not_subdl = dbCon.search(con, "SELECT DISTINCT T.IMEI_NO,count(T.IMEI_NO) CNT FROM SD_WARRENTY_REGISTRATION W,SD_BUSINESS_STRUCTURES S,SD_TEMP_IMEI T,SD_DEVICE_MASTER M WHERE S.BIS_STRU_TYPE_ID != 2 AND S.BIS_ID = M.BIS_ID AND M.IMEI_NO = T.IMEI_NO GROUP BY T.IMEI_NO");
//        master_cnt = rs_mastr.getInt("CNT");
//        al_reg_cnt = rs_al_reg.getInt("CNT");
//        not_subdl_cnt = rs_al_reg.getInt("CNT");
        //        while (rs_mastr.next()) { 
        //rs_mastr.next()
//            master_cnt = rs_mastr.getInt("CNT");
//            System.out.println("ZZZZ "+master_cnt);
//        }
//        while (rs_al_reg.next()) { 
//            al_reg_cnt = rs_al_reg.getInt("CNT");
//            System.out.println("ZZZZ2 "+master_cnt);
//        }
//        while (rs_not_subdl.next()) {  
//            
//            not_subdl_cnt = rs_not_subdl.getInt("CNT");
//            System.out.println("ZZZZ3 "+master_cnt);
//        }
//        if(master_cnt >0){
        if (!rs_mastr.next()) {
//          System.out.println("Master count call rs_mastr....");
        } else {
            do {
                 DbCon dbConn2 = new DbCon();
                Connection conn2 = dbConn2.getCon();
                
                master_imei = rs_mastr.getString("IMEI_NO");
                //System.out.println("IInside " + master_imei);
                String column = "IMEI_NO,REASON,DATE_INSERTED";

                String values = "'" + master_imei + "',"
                        + "'" + reg1 + "',"
                        + "SYSDATE";
                PreparedStatement ps_master_cnt = dbConn2.prepare(conn2, "INSERT INTO SD_INVALID_IMEI (" + column + ") VALUES(" + values + ")");
                ps_master_cnt.executeUpdate();

                PreparedStatement upate_stmnt = dbConn2.prepare(conn2, "UPDATE SD_TEMP_IMEI SET REASON = 2 WHERE IMEI_NO = '" + master_imei + "' ");
                upate_stmnt.executeUpdate();
                 dbConn2.ConectionClose(conn2);
            } while (rs_mastr.next());
        }
        rs_mastr.close();
            //rs_mastr.beforeFirst(); 

        if (!rs_al_reg.next()) {
//          System.out.println("Master count call rs_al_reg....");
        } else {
            do {
                al_reg_imei = rs_al_reg.getString("IMEI_NO");
//                System.out.println("al_reg_imei "+al_reg_imei);
                String column = "IMEI_NO,REASON,DATE_INSERTED";
                
                DbCon dbConn2 = new DbCon();
                Connection conn2 = dbConn2.getCon();

                String values = "'" + al_reg_imei + "',"
                        + "'" + reg2 + "',"
                        + "SYSDATE";
                PreparedStatement al_reg_imei_cnt = dbConn2.prepare(conn2, "INSERT INTO SD_INVALID_IMEI (" + column + ") VALUES(" + values + ")");
                al_reg_imei_cnt.executeUpdate();

                PreparedStatement upate_stmnt = dbConn2.prepare(conn2, "UPDATE SD_TEMP_IMEI SET REASON = 2 WHERE IMEI_NO = '" + al_reg_imei + "' ");
                upate_stmnt.executeUpdate();
                dbConn2.ConectionClose(conn2);
            } while (rs_al_reg.next());
        }
        rs_al_reg.close();
        ResultSet rs_not_subdl = dbCon.search(con, "SELECT * FROM (SELECT DISTINCT T.IMEI_NO,count(T.IMEI_NO) CNT, S.BIS_STRU_TYPE_ID FROM SD_WARRENTY_REGISTRATION W,SD_BUSINESS_STRUCTURES S,SD_TEMP_IMEI T,SD_DEVICE_MASTER M WHERE S.BIS_ID = M.BIS_ID AND M.IMEI_NO = T.IMEI_NO AND T.REASON = 1 GROUP BY T.IMEI_NO, S.BIS_STRU_TYPE_ID ) WHERE BIS_STRU_TYPE_ID !=2 OR BIS_STRU_TYPE_ID IS NULL");
        if (!rs_not_subdl.next()) {
//          System.out.println("Master count call rs_not_subdl....");
        } else {
            do {
                
                 DbCon dbConn2 = new DbCon();
                Connection conn2 = dbConn2.getCon();
                
                not_subdl_imei = rs_not_subdl.getString("IMEI_NO");
//                System.out.println("rs_not_subdl " + not_subdl_imei);
                String column = "IMEI_NO,REASON,DATE_INSERTED";

                String values = "'" + not_subdl_imei + "',"
                        + "'" + reg3 + "',"
                        + "SYSDATE";
                PreparedStatement not_subdl_imei_cnt = dbConn2.prepare(conn2, "INSERT INTO SD_INVALID_IMEI (" + column + ") VALUES(" + values + ")");
                not_subdl_imei_cnt.executeUpdate();

                PreparedStatement upate_stmnt = dbConn2.prepare(conn2, "UPDATE SD_TEMP_IMEI SET REASON = 2 WHERE IMEI_NO = '" + not_subdl_imei + "' ");
                upate_stmnt.executeUpdate();
                 dbConn2.ConectionClose(conn2);
            } while (rs_not_subdl.next());
        }
        rs_not_subdl.close();
    
//        con.close();
        dbCon.ConectionClose(con);
            //while(rs_mastr.next()) {

//               master_imei = rs_mastr.getString("IMEI_NO");
//                System.out.println("IInside "+master_imei);
//                 String column = "IMEI_NO,REASON";
//                                               
//                 String values = "'" + master_imei + "',"
//                                 + "'"+reg1+"'";
//                PreparedStatement ps_master_cnt = dbCon.prepare(con, "INSERT INTO SD_INVALID_IMEI (" + column + ") VALUES(" + values + ")");
//                ps_master_cnt.executeUpdate();
        //  }
//         if(al_reg_cnt >0){
//        
//           while (rs_not_subdl.next()) {
//                
//                al_reg_imei = rs_not_subdl.getString("IMEI_NO");
//                
//                 String column = "IMEI_NO,REASON";
//                                               
//                 String values = "'" + al_reg_imei + "',"
//                                 + "'"+reg2+"'";
//                PreparedStatement ps_master_cnt = dbCon.prepare(con, "INSERT INTO SD_INVALID_IMEI (" + column + ") VALUES(" + values + ")");
//                ps_master_cnt.executeUpdate();
//            }
//        }
//         if(not_subdl_cnt >0){
//        
//           while (rs_al_reg.next()) {
//                
//                not_subdl_imei = rs_al_reg.getString("IMEI_NO");
//                
//                 String column = "IMEI_NO,REASON";
//                                               
//                 String values = "'" + not_subdl_imei + "',"
//                                 + "'"+reg3+"'";
//                PreparedStatement ps_master_cnt = dbCon.prepare(con, "INSERT INTO SD_INVALID_IMEI (" + column + ") VALUES(" + values + ")");
//                ps_master_cnt.executeUpdate();
//            }
//        }
//        if(master_cnt >0){
//           
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    //END ADD STATUS TO INVALIS IMEIS
}
