 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.MdeCode;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderTransfer;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WorkOrderTransferController {

    public static MessageWrapper getWorkOrderTransfer(String work_order_no,
            String bis_name,
            int bis_id,
            String transfer_date,
            String reson,
            int delever_type,
            String curiorMail,
            int status,
            String transfer_loc,
            int trans_location_id,
            String retun_date,
            String return_reason,
            String return_delevery_details,
            String deleveryTypeDesc,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getWorkOrderTransfer Method Call....................");
        ArrayList<WorkOrderTransfer> workOrderTransList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.workOrderTransfer(order);

            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
            String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
            //String bisId = (bis_id == 0 ? "" : "AND W.BIS_ID = " + bis_id + "");
            String bisId = (bis_id == 0 ? "" : "AND (W.BIS_ID = " + bis_id + " OR  W.TRANSFER_LOC_ID = " + bis_id + ") ");
            String TransferDate = (transfer_date.equalsIgnoreCase("all") ? "NVL(W.TRANSFER_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.TRANSFER_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.TRANSFER_DATE = TO_DATE('" + transfer_date + "','YYYY/MM/DD')");
            String Reason = (reson.equalsIgnoreCase("all") ? "NVL(W.TRANSFER_REASON,'AA') = NVL(W.TRANSFER_REASON,'AA')" : "UPPER(W.TRANSFER_REASON) LIKE UPPER('%" + reson + "%')");
            String quriorMail = (curiorMail.equalsIgnoreCase("all") ? "NVL(W.COURIER_EMAIL,'AA') = NVL(W.COURIER_EMAIL,'AA')" : "UPPER(W.COURIER_EMAIL) LIKE UPPER('%" + curiorMail + "%')");
            String deleveryType = (delever_type == 0 ? "" : "AND P.DELIVERY_TYPE = " + delever_type + "");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY W.WORK_ORD_TRNS_NO DESC" : "ORDER BY " + order + "   " + type + "");
            String transLocationId = (trans_location_id == 0 ? "" : "AND W.TRANSFER_LOC_ID = " + trans_location_id + "");
            String transferLocation = (transfer_loc.equalsIgnoreCase("all") ? "NVL(B2.BIS_NAME,'AA') = NVL(B2.BIS_NAME,'AA')" : "UPPER(B2.BIS_NAME) LIKE UPPER('%" + transfer_loc + "%')");
            String returnDate = (retun_date.equalsIgnoreCase("all") ? "NVL(W.RETURN_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.RETURN_DATE,TO_DATE('20100101','YYYYMMDD'))" : "W.RETURN_DATE = TO_DATE('" + retun_date + "','YYYY/MM/DD')");
            String returnReason = (return_reason.equalsIgnoreCase("all") ? "NVL(W.RETURN_REASON,'AA') = NVL(W.RETURN_REASON,'AA')" : "UPPER(W.RETURN_REASON) LIKE UPPER('%" + return_reason + "%')");
            String returnDeleveryDetails = (return_delevery_details.equalsIgnoreCase("all") ? "NVL(W.RETURN_DELEVERY_DETAILS,'AA') = NVL(W.RETURN_DELEVERY_DETAILS,'AA')" : "UPPER(W.RETURN_DELEVERY_DETAILS) LIKE UPPER('%" + return_delevery_details + "%')");
            String DeliveryTypeDesc = (curiorMail.equalsIgnoreCase("all") ? " " : " AND UPPER(M.DESCRIPTION) LIKE UPPER('%" + deleveryTypeDesc + "%')");

            String seletColunm = "W.WORK_ORD_TRNS_NO,"
                    + "W.WORK_ORDER_NO,"
                    + "B.BIS_NAME,W.BIS_ID,"
                    + "TO_CHAR(W.TRANSFER_DATE,'YYYY/MM/DD') TRANSFER_DATE,"
                    + "W.TRANSFER_REASON,"
                    + "W.DELIVERY_TYPE,"
                    + "M.DESCRIPTION,"
                    + "W.COURIER_EMAIL,"
                    + "W.DELIVERY_DETAILS,"
                    + "W.STATUS,"
                    + "B2.BIS_NAME TRANS_LOC,"
                    + "W.TRANSFER_LOC_ID,"
                    + "W.RETURN_DELEVERY_TYPE,"
                    + "TO_CHAR(W.RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "W.RETURN_REASON,"
                    + "W.RETURN_DELEVERY_DETAILS";

            String whereClous = "W.BIS_ID = B.BIS_ID (+) "
                    + " AND W.TRANSFER_LOC_ID = B2.BIS_ID (+) "
                    + " AND W.DELIVERY_TYPE = M.CODE (+) "
                    + " AND M.DOMAIN_NAME = 'DELEVERY_TYPE'"
                    + " AND " + workOrderNo + " "
                    + " AND " + bisName + " "
                    + " " + bisId + " "
                    + " AND " + TransferDate + " "
                    + " AND " + Reason + " "
                    + " AND " + quriorMail + " "
                    + " " + deleveryType + " "
                    + " " + Status + " "
                    + " AND " + transferLocation + "  "
                    + " " + transLocationId + " "
                    + " AND " + returnDate + " "
                    + " AND " + returnReason + " "
                    + " AND " + returnDeleveryDetails + " "
                    + DeliveryTypeDesc
                    + " AND W.STATUS != 9 ";

            String tables = "SD_WORK_ORDER_TRANSFERS W,SD_BUSINESS_STRUCTURES B,SD_MDECODE M,SD_BUSINESS_STRUCTURES B2 ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderTransfer wp = new WorkOrderTransfer();
                totalrecode = rs.getInt("CNT");
                wp.setWoTransNo(rs.getInt("WORK_ORD_TRNS_NO"));
                wp.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wp.setBisId(rs.getInt("BIS_ID"));
                wp.setBisName(rs.getString("BIS_NAME"));
                wp.setTransferDate(rs.getString("TRANSFER_DATE"));
                wp.setTransferReason(rs.getString("TRANSFER_REASON"));
                wp.setCourierEmail(rs.getString("COURIER_EMAIL"));
                wp.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                wp.setDeleveryTypeDesc(rs.getString("DESCRIPTION"));
                wp.setDeleverDetils(rs.getString("DELIVERY_DETAILS"));
                wp.setTransferLocationDesc(rs.getString("TRANS_LOC"));
                wp.setTansferLocationId(rs.getInt("TRANSFER_LOC_ID"));
                wp.setRetunDeleveryType(rs.getInt("RETURN_DELEVERY_TYPE"));
                wp.setReturnDate(rs.getString("RETURN_DATE"));
                wp.setReturnReason(rs.getString("RETURN_REASON"));
                wp.setReturnDeleveryDetails(rs.getString("RETURN_DELEVERY_DETAILS"));
                wp.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    wp.setStatusMsg("Pending");
                } else if (rs.getInt("STATUS") == 2) {
                    wp.setStatusMsg("Acepted");
                } else if (rs.getInt("STATUS") == 3) {
                    wp.setStatusMsg("Return");
                } else {
                    wp.setStatusMsg("None");
                }
                workOrderTransList.add(wp);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderTransfer Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderTransList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper verifyWorkOrder(String workorderno) {

        logger.info("verifyWorkOrder Method Call....................");
        int recode_count = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE WORK_ORDER_NO = '" + workorderno + "' "
                    + "AND STATUS != 9");

            while (rs.next()) {
                recode_count = rs.getInt("CNT");
            }

        } catch (Exception ex) {
            logger.info("Error verifyWorkOrder Methd call.." + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (recode_count > 0) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("This Work Order Number Verify.");
            logger.info("This Work Order Number Verify....................");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("This Work Order Number Not in The List");
            logger.info("This Work Order Number Not in The List..............");
        }
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addWorkOrderTransfer(WorkOrderTransfer wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWorkOrderTransfer Mehtod Call.......");
        int result = 0;
        int max_woTrans = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int check_active = 0;
        try {

            ResultSet rs_che_trans = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_WORK_ORDER_TRANSFERS "
                    + "WHERE WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "' "
                    + "AND STATUS IN (1,2,4)");

            while (rs_che_trans.next()) {
                check_active = rs_che_trans.getInt("CNT");
            }

            if (check_active > 0) {
                result = 2;
            } else {

                ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(WORK_ORD_TRNS_NO),0)+1 MAX "
                        + "FROM SD_WORK_ORDER_TRANSFERS");

                while (rs_max.next()) {
                    max_woTrans = rs_max.getInt("MAX");
                }

                String insertColumn = "WORK_ORD_TRNS_NO,"
                        + "WORK_ORDER_NO,"
                        + "BIS_ID,"
                        + "TRANSFER_DATE,"
                        + "TRANSFER_REASON,"
                        + "COURIER_EMAIL,"
                        + "DELIVERY_TYPE,"
                        + "DELIVERY_DETAILS,"
                        + "STATUS,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED,TRANSFER_LOC_ID";

                String insertValus = "?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,?,?,?,SYSDATE,?";

                PreparedStatement ps2 = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_TRANSFERS(" + insertColumn + ") VALUES(" + insertValus + ")");
                ps2.setInt(1, max_woTrans);
                ps2.setString(2, wo_info.getWorkOrderNo());
                ps2.setInt(3, wo_info.getBisId());
                ps2.setString(4, wo_info.getTransferDate());
                ps2.setString(5, wo_info.getTransferReason());
                ps2.setString(6, wo_info.getCourierEmail());
                ps2.setInt(7, wo_info.getDeleveryType());
                ps2.setString(8, wo_info.getDeleverDetils());
                ps2.setInt(9, wo_info.getStatus());
                ps2.setString(10, user_id);
                ps2.setInt(11, wo_info.getTansferLocationId());
                ps2.executeUpdate();

                if (wo_info.getStatus() == 2 || wo_info.getStatus() == 4) {

                    PreparedStatement ps_up_wo = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                            + "SET BIS_ID =?,"
                            + "USER_MODIFIED=?,"
                            + "DATE_MODIFIED=SYSDATE  "
                            + "WHERE WORK_ORDER_NO = ?");

                    //ps_up_wo.setInt(1, wo_info.getTansferLocationId());
                    ps_up_wo.setInt(1, wo_info.getBisId());
                    ps_up_wo.setString(2, user_id);
                    ps_up_wo.setString(3, wo_info.getWorkOrderNo());
                    ps_up_wo.executeUpdate();

                }

                result = 1;
            }

        } catch (Exception ex) {
            logger.info("Error in addWorkOrderTransfer Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Transfer Insert Successfully");
            logger.info("WorkOrder Transfer Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This WorkOrder have a active Transfer");
            logger.info("This WorkOrder have a active Transfer..............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Transfer Insert");
            logger.info("Error in Work Order Transfer Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editWorkOrderTransfer(WorkOrderTransfer wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editWorkOrderTransfer Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String updatetrans = "UPDATE SD_WORK_ORDER_TRANSFERS "
                    + "SET WORK_ORDER_NO =?,"
                    + "BIS_ID =?,"
                    + "TRANSFER_DATE =TO_DATE(?,'YYYY/MM/DD'),"
                    + "TRANSFER_REASON =?,"
                    + "COURIER_EMAIL =?,"
                    + "DELIVERY_TYPE=?,"
                    + "DELIVERY_DETAILS =?,"
                    + "STATUS =?,"
                    + "USER_MODIFIED =?,"
                    + "DATE_MODIFIED = SYSDATE,"
                    + "TRANSFER_LOC_ID=? "
                    + "WHERE WORK_ORD_TRNS_NO =?";

            PreparedStatement ps2 = dbCon.prepare(con, updatetrans);

            ps2.setString(1, wo_info.getWorkOrderNo());
            ps2.setInt(2, wo_info.getBisId());
            ps2.setString(3, wo_info.getTransferDate());
            ps2.setString(4, wo_info.getTransferReason());
            ps2.setString(5, wo_info.getCourierEmail());
            ps2.setInt(6, wo_info.getDeleveryType());
            ps2.setString(7, wo_info.getDeleverDetils());
            ps2.setInt(8, wo_info.getStatus());
            ps2.setString(9, user_id);
            ps2.setInt(10, wo_info.getTansferLocationId());
            ps2.setInt(11, wo_info.getWoTransNo());
            ps2.executeUpdate();

            if (wo_info.getStatus() == 2) {
                PreparedStatement ps_up_wo = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                        + "SET BIS_ID =?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE  "
                        + "WHERE WORK_ORDER_NO = ?");

                //ps_up_wo.setInt(1, wo_info.getTansferLocationId());
                ps_up_wo.setInt(1, wo_info.getBisId());
                ps_up_wo.setString(2, user_id);
                ps_up_wo.setString(3, wo_info.getWorkOrderNo());
                ps_up_wo.executeUpdate();

            }

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in editWorkOrderTransfer Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Transfer Update Successfully");
            logger.info("WorkOrder Transfer Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Transfer Update");
            logger.info("Error in Work Order Transfer Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper returnWorkOrderTransfer(WorkOrderTransfer wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("returnWorkOrderTransfer Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String qry = "  SELECT  COUNT(*) AS CNT   "
                    + "     FROM   SD_WORK_ORDER_MAINTAIN   "
                    + "     WHERE  WORK_ORDER_NO = '" + wo_info.getWorkOrderNo() + "'   "
                    + "     AND    REPAIR_STATUS NOT IN (2 ,3)    ";   //////Check Work Order has Maintainance Record

            ResultSet rs_wochk = dbCon.search(con, qry);

            int chkWorkOrder = 0;
            while (rs_wochk.next()) {
                chkWorkOrder = rs_wochk.getInt("CNT");
            }

            if (chkWorkOrder == 0) {   ///If Work Order has Maintainance Record it can't be returned.

                String updatetrans = "UPDATE SD_WORK_ORDER_TRANSFERS "
                        + "SET RETURN_DELEVERY_TYPE=?,"
                        + "RETURN_DATE=SYSDATE,"
                        + "RETURN_REASON=?,"
                        + "RETURN_DELEVERY_DETAILS=?,"
                        + "STATUS =3,"
                        + "USER_MODIFIED =?,"
                        + "DATE_MODIFIED = SYSDATE, "
                        + "BIS_ID=?,    "
                        + "TRANSFER_LOC_ID=?    "
                        + "WHERE WORK_ORD_TRNS_NO =?";

                PreparedStatement ps2 = dbCon.prepare(con, updatetrans);

                ps2.setInt(1, wo_info.getRetunDeleveryType());
                ps2.setString(2, wo_info.getReturnReason());
                ps2.setString(3, wo_info.getReturnDeleveryDetails());
                ps2.setString(4, user_id);
                ps2.setInt(5, wo_info.getBisId());
                ps2.setInt(6, wo_info.getTansferLocationId());
                ps2.setInt(7, wo_info.getWoTransNo());
                ps2.executeUpdate();

                PreparedStatement ps_up_wo = dbCon.prepare(con, "UPDATE SD_WORK_ORDERS "
                        + "SET BIS_ID =?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE  "
                        + "WHERE WORK_ORDER_NO = ?");

                ps_up_wo.setInt(1, wo_info.getBisId());
                ps_up_wo.setString(2, user_id);
                ps_up_wo.setString(3, wo_info.getWorkOrderNo());
                ps_up_wo.executeUpdate();

                result = 1;

            } else {
                result = 2;
            }

        } catch (Exception ex) {
            logger.info("Error in returnWorkOrderTransfer Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Return Successfully");
            logger.info("WorkOrder Return Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Work Order Maintainance should be closed before Return.");
            logger.info("Work Order Maintainance should be closed before Return");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Return");
            logger.info("Error in Work Order Return................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper deleteWorkOrderTransfer(WorkOrderTransfer wo_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deleteWorkOrderTransfer Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String updatetrans = "UPDATE SD_WORK_ORDER_TRANSFERS "
                    + "SET STATUS =9,"
                    + "USER_MODIFIED =?,"
                    + "DATE_MODIFIED = SYSDATE "
                    + "WHERE WORK_ORD_TRNS_NO =?";

            PreparedStatement ps2 = dbCon.prepare(con, updatetrans);
            ps2.setString(1, user_id);
            ps2.setInt(2, wo_info.getWoTransNo());
            ps2.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in deleteWorkOrderTransfer Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Transfer Delete Successfully");
            logger.info("WorkOrder Transfer Delete Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Transfer Delete");
            logger.info("Error in Work Order Transfer Delete................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getWorkOrderTransferStatus() {

        logger.info("getWorkOrderTransferStatus Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'WO_TRANS'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getWorkOrderTransferStatus Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderToTransfer(String wo_no, int bis_id) {

        logger.info("getWorkOrderToTransfer  Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String workOrderNo = (wo_no.equalsIgnoreCase("all") ? "" : "AND UPPER(WORK_ORDER_NO) LIKE UPPER('%" + wo_no + "%')");
            String bisId = (bis_id == 0 ? "" : " AND BIS_ID  = " + bis_id + "");

            ResultSet rs_open_wo = dbCon.search(con, "SELECT WORK_ORDER_NO "
                    + "FROM SD_WORK_ORDERS "
                    + "WHERE STATUS = 1  "
                    + "AND WORK_ORDER_NO NOT IN (SELECT WORK_ORDER_NO "
                    + "FROM SD_WORK_ORDER_TRANSFERS "
                    + "WHERE STATUS IN (1,2,3,4))  "
                    + "AND WORK_ORDER_NO NOT IN (   SELECT WORK_ORDER_NO     "
                    + "                             FROM SD_WORK_ORDER_MAINTAIN         "
                    + "                             WHERE REPAIR_STATUS IN (1,4, 5, 6, 7, 8)) "
                    + " " + workOrderNo + "  "
                    + " " + bisId + " ");

            ArrayList<String> woOpenList = new ArrayList<>();

            while (rs_open_wo.next()) {
                woOpenList.add(rs_open_wo.getString("WORK_ORDER_NO"));

            }

            ResultSet rs_wo_main = dbCon.search(con, "SELECT WORK_ORDER_NO "
                    + "FROM SD_WORK_ORDER_MAINTAIN "
                    + "WHERE REPAIR_STATUS IN (3,9)");

            ArrayList<String> woOpenMaintainList = new ArrayList<>();

            while (rs_wo_main.next()) {
                woOpenMaintainList.add(rs_wo_main.getString("WORK_ORDER_NO"));

            }

            woOpenList.removeAll(woOpenMaintainList);

            for (String s : woOpenList) {
                WorkOrder wo = new WorkOrder();
                wo.setWorkOrderNo(s);
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderToTransfer Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

}
