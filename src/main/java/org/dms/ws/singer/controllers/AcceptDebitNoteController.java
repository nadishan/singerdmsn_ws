/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.AcceptDebitNote;
import org.dms.ws.singer.entities.AcceptInvenroryImei;
import org.dms.ws.singer.entities.AcceptInventory;
import org.dms.ws.singer.entities.ImportDebitNote;
import org.dms.ws.singer.entities.ImportDebitNoteDetails;
import org.dms.ws.singer.utils.AppParams;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class AcceptDebitNoteController {

    public static MessageWrapper getAcceptDebitNote(String debitnoteno,
            String orderno,
            String invoiceno,
            String contactno,
            String partno,
            int orderqty,
            int deleveryqty,
            String deleverydate,
            int headntatus,
            String customerno,
            int status,
            int bisid,
            String order,
            String type,
            int start,
            int limit) {

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        logger.info("getAcceptDebitNote Method Call....................");
        ArrayList<ImportDebitNote> importDebitList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.acceptDebitNote(order);

            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(D.DEBIT_NOTE_NO,'AA') = NVL(D.DEBIT_NOTE_NO,'AA')" : "UPPER(D.DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String orderNo = (orderno.equalsIgnoreCase("all") ? "NVL(D.ORDER_NO,'AA') = NVL(D.ORDER_NO,'AA')" : "UPPER(D.ORDER_NO) LIKE UPPER('%" + orderno + "%')");
            String invoiceNo = (invoiceno.equalsIgnoreCase("all") ? "NVL(D.INVOICE_NO,'AA') = NVL(D.INVOICE_NO,'AA')" : "UPPER(D.INVOICE_NO) LIKE UPPER('%" + invoiceno + "%')");
            String contactNo = (contactno.equalsIgnoreCase("all") ? "NVL(D.CONTRACT_NO,'AA') = NVL(D.CONTRACT_NO,'AA')" : "UPPER(D.CONTRACT_NO) LIKE UPPER('%" + contactno + "%')");
            String partNo = (partno.equalsIgnoreCase("all") ? "NVL(D.PART_NO,'AA') = NVL(D.PART_NO,'AA')" : "UPPER(D.PART_NO) LIKE UPPER('%" + partno + "%')");
            String orderQty = (orderqty == 0 ? "" : "AND D.ORDER_QTY = '" + orderqty + "'");
            String deleveryQty = (deleveryqty == 0 ? "" : "AND D.DELIVERED_QTY = '" + deleveryqty + "'");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(D.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(D.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "D.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String headStatus = (headntatus == 0 ? "" : "AND D.HEAD_STATUS = '" + headntatus + "' ");
            String customerNo = (customerno.equalsIgnoreCase("all") ? "NVL(D.CUSTOMER_NO,'AA') = NVL(D.CUSTOMER_NO,'AA')" : "UPPER(D.CUSTOMER_NO) LIKE UPPER('%" + customerno + "%')");
            String Status = (status == 0 ? "" : "AND D.STATUS = " + status + " ");
            String Bisid = (bisid == 0 ? "" : "AND B.BIS_ID = " + bisid + " ");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.DEBIT_NOTE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = " D.DEBIT_NOTE_NO,D.ORDER_NO,"
                    + "D.INVOICE_NO,D.CONTRACT_NO,"
                    + "D.PART_NO,D.ORDER_QTY,"
                    + "D.DELIVERED_QTY,TO_CHAR(D.DELIVERY_DATE,'YYYY/MM/DD') DELIVERYDATE,"
                    + "D.HEAD_STATUS,D.CUSTOMER_NO,"
                    + "D.SITE_DESCRIPTION,D.STATUS,B.BIS_ID";

            String whereClous = " " + debitNoteNo + " "
                    + " AND " + orderNo + " "
                    + " AND " + invoiceNo + " "
                    + " AND " + contactNo + " "
                    + " AND " + partNo + " "
                    + " " + orderQty + " "
                    + " " + deleveryQty + " "
                    + " AND " + deleveryDate + " "
                    + " " + headStatus + " "
                    + " AND " + customerNo + " "
                    + " " + Status + " "
                    + " " + Bisid + " "
                    + " AND D.STATUS != 3 "
                    + " AND D.CUSTOMER_NO = B.CUSTOMER_CODE "
                    + " AND B.STATUS=1 ";

            String table = "SD_DEBIT_NOTE_HEADER D,SD_USER_BIS_MAPPING B";

            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset Debit Note.................");

            while (rs.next()) {
                ImportDebitNote idn = new ImportDebitNote();
                totalrecode = rs.getInt("CNT");
                idn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                idn.setOrderNo(rs.getString("ORDER_NO"));
                idn.setInvoiceNo(rs.getString("INVOICE_NO"));
                idn.setContactNo(rs.getString("CONTRACT_NO"));
                idn.setPartNo(rs.getString("PART_NO"));
                idn.setOrderQty(rs.getInt("ORDER_QTY"));
                idn.setDeleveryQty(rs.getInt("DELIVERED_QTY"));
                idn.setDeleveryDate(rs.getString("DELIVERYDATE"));
                idn.setHeadStatus(rs.getInt("HEAD_STATUS"));
                idn.setCustomerNo(rs.getString("CUSTOMER_NO"));
                idn.setSiteDescription(rs.getString("SITE_DESCRIPTION"));
                idn.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    idn.setStatusMsg("Pending");
                } else if (rs.getInt("STATUS") == 2) {
                    idn.setStatusMsg("In Progress");
                } else if (rs.getInt("STATUS") == 3) {
                    idn.setStatusMsg("Accepted");
                } else {
                    idn.setStatusMsg("None");
                }
                idn.setBisId(rs.getInt("BIS_ID"));
                importDebitList.add(idn);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error getAcceptDebitNote Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);

        }
        mw.setData(importDebitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getAcceptDebitNoteDetial(String debitnoteno) {
        logger.info("getAcceptDebitNoteDetial Method Call....................");
        ArrayList<ImportDebitNote> imporDebitList = new ArrayList<>();
        ArrayList<ImportDebitNoteDetails> idnDetail = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "DEBIT_NOTE_NO,ORDER_NO,"
                    + "INVOICE_NO,CONTRACT_NO,"
                    + "PART_NO,ORDER_QTY,"
                    + "DELIVERED_QTY,"
                    + "TO_CHAR(DELIVERY_DATE,'YYYY/MM/DD') DELIVERYDATE,"
                    + "HEAD_STATUS,CUSTOMER_NO,"
                    + "SITE_DESCRIPTION";

            // String whereClous = "" + debitnoteno + "";
            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM SD_DEBIT_NOTE_HEADER "
                    + "WHERE DEBIT_NOTE_NO = '" + debitnoteno + "'");

            logger.info("Get Data to Resultset...");

            ImportDebitNote idn = new ImportDebitNote();

            while (rs.next()) {
                idn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                idn.setOrderNo(rs.getString("ORDER_NO"));
                idn.setInvoiceNo(rs.getString("INVOICE_NO"));
                idn.setContactNo(rs.getString("CONTRACT_NO"));
                idn.setPartNo(rs.getString("PART_NO"));
                idn.setOrderQty(rs.getInt("ORDER_QTY"));
                idn.setDeleveryQty(rs.getInt("DELIVERED_QTY"));
                idn.setDeleveryDate(rs.getString("DELIVERYDATE"));
                idn.setHeadStatus(rs.getInt("HEAD_STATUS"));
                idn.setCustomerNo(rs.getString("CUSTOMER_NO"));
                idn.setSiteDescription(rs.getString("SITE_DESCRIPTION"));

            }
            rs.close();

            String debitNoteDetailsSelectColumn = "D.DEBIT_NOTE_NO,D.IMEI_NO,"
                    + "D.CUSTOMER_CODE,D.SHOP_CODE,"
                    + "D.MODEL_NO,D.BIS_ID,"
                    + "D.ORDER_NO,D.SALES_PRICE,"
                    + "TO_CHAR(D.DATE_ACCEPTED,'YYYY/MM/DD') DATEACCEPTED,"
                    + "D.REMARKS,D.STATUS,"
                    + "M.MODEL_DESCRIPTION,D.SITE";

            String tables = "SD_DEBIT_NOTE_DETAIL D,SD_MODEL_LISTS M";

            String where = "D.MODEL_NO = M.MODEL_NO (+) "
                    + "AND D.STATUS != 3  "
                    + "AND DEBIT_NOTE_NO = '" + debitnoteno + "'";

            ResultSet rs_details = dbCon.search(con, "SELECT " + debitNoteDetailsSelectColumn + " "
                    + "FROM " + tables + " "
                    + "WHERE " + where + "");

            while (rs_details.next()) {
                ImportDebitNoteDetails idnd = new ImportDebitNoteDetails();
                idnd.setDebitNoteNo(rs_details.getString("DEBIT_NOTE_NO"));
                idnd.setImeiNo(rs_details.getString("IMEI_NO"));
                idnd.setCustomerCode(rs_details.getString("CUSTOMER_CODE"));
                idnd.setShopCode(rs_details.getString("SHOP_CODE"));
                idnd.setModleNo(rs_details.getInt("MODEL_NO"));
                idnd.setBisId(rs_details.getInt("BIS_ID"));
                idnd.setOrderNo(rs_details.getString("ORDER_NO"));
                idnd.setSalerPrice(rs_details.getDouble("SALES_PRICE"));
                idnd.setDateAccepted(rs_details.getString("DATEACCEPTED"));
                idnd.setRemarks(rs_details.getString("REMARKS"));
                idnd.setStatus(rs_details.getInt("STATUS"));
                idnd.setModleDesc(rs_details.getString("MODEL_DESCRIPTION"));
                idnd.setSite(rs_details.getString("SITE"));
                idnDetail.add(idnd);
            }
            rs_details.close();

            idn.setImportDebitList(idnDetail);

            imporDebitList.add(idn);

        } catch (Exception ex) {
            logger.info("Error getImportDebitNote Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = idnDetail.size();
        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }
        mw.setData(imporDebitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getAcceptDebitNoteDetialOnly(String debitnoteno, int start,
            int limit) {
        logger.info("getAcceptDebitNoteDetialOnly Method Call....................");
        ArrayList<ImportDebitNote> imporDebitList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "DEBIT_NOTE_NO,ORDER_NO,"
                    + "INVOICE_NO,CONTRACT_NO,"
                    + "PART_NO,ORDER_QTY,"
                    + "DELIVERED_QTY,"
                    + "TO_CHAR(DELIVERY_DATE,'YYYY/MM/DD') DELIVERYDATE,"
                    + "HEAD_STATUS,CUSTOMER_NO,"
                    + "SITE_DESCRIPTION";

            // String whereClous = "" + debitnoteno + "";
            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM SD_DEBIT_NOTE_HEADER "
                    + "WHERE DEBIT_NOTE_NO = '" + debitnoteno + "'");

            logger.info("Get Data to Resultset...");

            ImportDebitNote idn = new ImportDebitNote();

            while (rs.next()) {
                idn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                idn.setOrderNo(rs.getString("ORDER_NO"));
                idn.setInvoiceNo(rs.getString("INVOICE_NO"));
                idn.setContactNo(rs.getString("CONTRACT_NO"));
                idn.setPartNo(rs.getString("PART_NO"));
                idn.setOrderQty(rs.getInt("ORDER_QTY"));
                idn.setDeleveryQty(rs.getInt("DELIVERED_QTY"));
                idn.setDeleveryDate(rs.getString("DELIVERYDATE"));
                idn.setHeadStatus(rs.getInt("HEAD_STATUS"));
                idn.setCustomerNo(rs.getString("CUSTOMER_NO"));
                idn.setSiteDescription(rs.getString("SITE_DESCRIPTION"));

            }
            rs.close();

            String debitNoteDetailsSelectColumn = "D.DEBIT_NOTE_NO,D.IMEI_NO,"
                    + "D.CUSTOMER_CODE,D.SHOP_CODE,"
                    + "D.MODEL_NO,D.BIS_ID,"
                    + "D.ORDER_NO,D.SALES_PRICE,"
                    + "TO_CHAR(D.DATE_ACCEPTED,'YYYY/MM/DD') DATEACCEPTED,"
                    + "D.REMARKS,D.STATUS,"
                    + "M.MODEL_DESCRIPTION,D.SITE";

            String tables = "SD_DEBIT_NOTE_DETAIL D,SD_MODEL_LISTS M";

            String where = "D.MODEL_NO = M.MODEL_NO (+) "
                    //                    + "AND D.STATUS != 3  "
                    + "AND D.STATUS = 1  "// STATUS = 1 HAVE TO ACCEPT
                    + "AND DEBIT_NOTE_NO = '" + debitnoteno + "'";

            System.out.println("D.STATUS = 1");

            String Order = "ORDER BY D.DEBIT_NOTE_NO ASC";

            ResultSet rs_details = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + where + ")CNT,"
                    + "" + debitNoteDetailsSelectColumn + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + tables + " WHERE " + where + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            ArrayList<ImportDebitNoteDetails> idnDetail = new ArrayList<>();

            while (rs_details.next()) {
                ImportDebitNoteDetails idnd = new ImportDebitNoteDetails();
                idnd.setDebitNoteNo(rs_details.getString("DEBIT_NOTE_NO"));
                idnd.setImeiNo(rs_details.getString("IMEI_NO"));
                idnd.setCustomerCode(rs_details.getString("CUSTOMER_CODE"));
                idnd.setShopCode(rs_details.getString("SHOP_CODE"));
                idnd.setModleNo(rs_details.getInt("MODEL_NO"));
                idnd.setBisId(rs_details.getInt("BIS_ID"));
                idnd.setOrderNo(rs_details.getString("ORDER_NO"));
                idnd.setSalerPrice(rs_details.getDouble("SALES_PRICE"));
                idnd.setDateAccepted(rs_details.getString("DATEACCEPTED"));
                idnd.setRemarks(rs_details.getString("REMARKS"));
                idnd.setStatus(rs_details.getInt("STATUS"));
                idnd.setModleDesc(rs_details.getString("MODEL_DESCRIPTION"));
                idnd.setSite(rs_details.getString("SITE"));
                idnDetail.add(idnd);
            }
            rs_details.close();

            idn.setImportDebitList(idnDetail);

            imporDebitList.add(idn);

        } catch (Exception ex) {
            logger.info("Error getAcceptDebitNoteDetialOnly Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = imporDebitList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(imporDebitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addacceptDebitDetials(ImportDebitNote importdebit_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addacceptDebitDetials Method Call.............................");
        int result = 0;
        int max_accept_no = 0;
        int ime_un = 0;
        int inactive_cnt = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<String> creditList = new ArrayList<>();
        int credit_cnt = 0;
        try {

            if (importdebit_info != null) {

                ArrayList<ImportDebitNoteDetails> imeiAlrady = new ArrayList<ImportDebitNoteDetails>();
                imeiAlrady = importdebit_info.getImportDebitList();

                if (imeiAlrady.size() > 0) {

                    String selectedImeis = "";

                    // ArrayList<String> importList = new ArrayList<>();
                    for (ImportDebitNoteDetails id : imeiAlrady) {
                        // importList.add(id.getImeiNo());

                        selectedImeis += "'" + id.getImeiNo() + "'" + ",";
                    }

                    selectedImeis = selectedImeis.substring(0, (selectedImeis.length() - 1));

                    ArrayList<String> masterList = new ArrayList<>();
                    //ResultSet rs_acept_cnt = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS =1");
                    ResultSet rs_acept_cnt = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS =1 AND IMEI_NO IN (" + selectedImeis + ") ");

                    while (rs_acept_cnt.next()) {
                        String imei = rs_acept_cnt.getString("IMEI_NO");
                        masterList.add(imei);
                    }

                    for (String s : masterList) {
                        //  if (masterList.contains(s)) {

                        int temp_list1_cnt = 0;
                        int temp_list2_cnt = 0;

                        ResultSet rs_temp_List1 = dbCon.search(con, "SELECT COUNT(*) CNT "
                                + "FROM TEMP_SD_DEVICE_MASTER "
                                + "WHERE TRANSACTION_FLAG = 0 "
                                + "AND FOUNDED_FLAG=0 "
                                + "AND IMEI_NO = '" + s + "'");

                        while (rs_temp_List1.next()) {
                            temp_list1_cnt = rs_temp_List1.getInt("CNT");
                        }

                        if (temp_list1_cnt >= 0) {
                            result = deleteDuplicateImeiMaster(s);
                            updateTempImeiMasterList1(s);
                        } else {

                            ResultSet rs_temp_List2 = dbCon.search(con, "SELECT COUNT(*) CNT "
                                    + "FROM TEMP_DEVICE_MASTER_20150617 "
                                    + "WHERE TRANSACTION_FLAG = 0 "
                                    + "AND  FOUNDED_FLAG = 0 "
                                    + "AND IMEI_NO = '" + s + "'");

                            while (rs_temp_List2.next()) {

                                temp_list2_cnt = rs_temp_List2.getInt("CNT");
                            }

                            if (temp_list2_cnt >= 0) {
                                result = deleteDuplicateImeiMaster(s);
                                updateTempImeiMasterList2(s);
                            } else {

                                ResultSet rs_credit_note = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_CREDIT_NOTE_ISSUE_DTL WHERE IMEI_NO = '" + s + "'");

                                while (rs_credit_note.next()) {

                                    credit_cnt = rs_credit_note.getInt("CNT");
                                }

                                if (credit_cnt >= 1) {
                                    creditList.add(s);
                                } else {
                                    return_imei += s + ",";
                                    ime_un = 1;
                                }

                            }

                        }

                        //  }
                    }

                    ArrayList<String> inactiveList = new ArrayList<>();
                    ResultSet rs_inative_imei = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS =9 AND IMEI_NO IN (" + selectedImeis + ") ");

                    while (rs_inative_imei.next()) {
                        String inactive_imei = rs_inative_imei.getString("IMEI_NO");
                        inactiveList.add(inactive_imei);

                    }

                    if (ime_un == 1) {
                        result = 2;
                    } else {

                        for (String s : inactiveList) {
                            //   if (inactiveList.contains(s)) {

                            int temp_list1_cnt = 0;
                            int temp_list2_cnt = 0;

                            ResultSet rs_temp_List1 = dbCon.search(con, "SELECT COUNT(*) CNT FROM TEMP_SD_DEVICE_MASTER WHERE IMEI_NO = '" + s + "'");

                            while (rs_temp_List1.next()) {
                                temp_list1_cnt = rs_temp_List1.getInt("CNT");
                            }

                            if (temp_list1_cnt >= 0) {
                                result = deleteDuplicateImeiMaster(s);
                            } else {

                                ResultSet rs_temp_List2 = dbCon.search(con, "SELECT COUNT(*) CNT FROM TEMP_DEVICE_MASTER_20150617 WHERE IMEI_NO = '" + s + "'");

                                while (rs_temp_List2.next()) {
                                    temp_list2_cnt = rs_temp_List2.getInt("CNT");
                                }

                                if (temp_list2_cnt >= 0) {
                                    result = deleteDuplicateImeiMaster(s);
                                } else {

                                    ResultSet rs_credit_note = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_CREDIT_NOTE_ISSUE_DTL WHERE IMEI_NO = '" + s + "'");

                                    while (rs_credit_note.next()) {

                                        credit_cnt = rs_credit_note.getInt("CNT");
                                    }

                                    if (credit_cnt >= 1) {
                                        creditList.add(s);
                                    } else {
                                        return_imei += s + ",";
                                        inactive_cnt = 1;
                                    }

                                }

                            }

                            //  }
                        }

                        if (inactive_cnt == 1) {
                            result = 3;
                        } else {

                            ArrayList<ImportDebitNoteDetails> imDeNoList = new ArrayList<ImportDebitNoteDetails>();
                            if (importdebit_info.getImportDebitList() != null) {
                                try {
                                    ArrayList<ImportDebitNoteDetails> temimDeNoList = new ArrayList<ImportDebitNoteDetails>();

                                    ResultSet rs_modle = dbCon.search(con, "SELECT MODEL_NO,"
                                            + "MODEL_DESCRIPTION,"
                                            + "PART_PRD_CODE_DESC,"
                                            + "PART_PRD_FAMILY_DESC "
                                            + "FROM SD_MODEL_LISTS");
                                    while (rs_modle.next()) {
                                        ImportDebitNoteDetails id = new ImportDebitNoteDetails();
                                        id.setModleNo(rs_modle.getInt("MODEL_NO"));
                                        id.setBrand(rs_modle.getString("PART_PRD_CODE_DESC"));
                                        id.setProduct(rs_modle.getString("PART_PRD_FAMILY_DESC"));
                                        temimDeNoList.add(id);
                                    }

                                    imDeNoList = importdebit_info.getImportDebitList();

                                    String imeiMasterColumn = "IMEI_NO,MODEL_NO,"
                                            + "BIS_ID,DEBIT_NOTE_NO,"
                                            + "SALES_PRICE,"
                                            + "STATUS,USER_INSERTED,"
                                            + "DATE_INSERTED,ORDER_NO,BRAND,PRODUCT,BAD_ITEM_FLAG";

                                    String imeiMasterValue = "?,?,?,?,?,1,?,SYSDATE,?,?,?,?";

                                    PreparedStatement ps_master_in = dbCon.prepare(con, "INSERT INTO SD_DEVICE_MASTER(" + imeiMasterColumn + ") "
                                            + "VALUES(" + imeiMasterValue + ")");

                                    PreparedStatement ps_up_imei_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                            + "SET MODEL_NO = ?,"
                                            + "BIS_ID = ?,"
                                            + "DEBIT_NOTE_NO = ?,"
                                            + "SALES_PRICE = ?,"
                                            + "STATUS = 1,"
                                            + "USER_INSERTED = ?,"
                                            + "DATE_INSERTED = SYSDATE,"
                                            + "ORDER_NO = ?,"
                                            + "BRAND = ?,"
                                            + "PRODUCT = ?,"
                                            + "BAD_ITEM_FLAG = ? "
                                            + "WHERE IMEI_NO = ?");

                                    for (ImportDebitNoteDetails di : imDeNoList) {

                                        if (creditList.contains(di.getImeiNo())) {

                                            ps_up_imei_master.setInt(1, di.getModleNo());
                                            ps_up_imei_master.setInt(2, di.getBisId());
                                            ps_up_imei_master.setString(3, di.getDebitNoteNo());
                                            ps_up_imei_master.setDouble(4, di.getSalerPrice());
                                            ps_up_imei_master.setString(5, user_id);
                                            ps_up_imei_master.setString(6, di.getOrderNo());

                                            for (ImportDebitNoteDetails idn : temimDeNoList) {
                                                if (di.getModleNo() == idn.getModleNo()) {
                                                    ps_up_imei_master.setString(7, idn.getBrand());
                                                    ps_up_imei_master.setString(8, idn.getProduct());
                                                }
                                            }
                                            ps_up_imei_master.setInt(9, 0);
                                            ps_up_imei_master.setString(10, di.getImeiNo());
                                            ps_up_imei_master.addBatch();

                                        } else {
                                            ps_master_in.setString(1, di.getImeiNo());
                                            ps_master_in.setInt(2, di.getModleNo());
                                            ps_master_in.setInt(3, di.getBisId());
                                            ps_master_in.setString(4, di.getDebitNoteNo());
                                            ps_master_in.setDouble(5, di.getSalerPrice());
                                            ps_master_in.setString(6, user_id);
                                            ps_master_in.setString(7, di.getOrderNo());

                                            for (ImportDebitNoteDetails idn : temimDeNoList) {
                                                if (di.getModleNo() == idn.getModleNo()) {
                                                    ps_master_in.setString(8, idn.getBrand());
                                                    ps_master_in.setString(9, idn.getProduct());
                                                }
                                            }
                                            ps_master_in.setInt(10, 0);
                                            ps_master_in.addBatch();
                                        }
                                    }
                                    ps_master_in.executeBatch();
                                    ps_up_imei_master.executeBatch();

                                    PreparedStatement ps_debit_details = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL "
                                            + "SET STATUS = 3 ,"
                                            + "USER_MODIFIED=?,"
                                            + "DATE_MODIFIED=SYSDATE "
                                            + "WHERE DEBIT_NOTE_NO = ? "
                                            + "AND IMEI_NO = ?");

                                    for (ImportDebitNoteDetails di : imDeNoList) {

                                        ps_debit_details.setString(1, user_id);
                                        ps_debit_details.setString(2, di.getDebitNoteNo());
                                        ps_debit_details.setString(3, di.getImeiNo());
                                        ps_debit_details.addBatch();
                                    }
                                    ps_debit_details.executeBatch();
                                    result = 1;
                                    logger.info("DebitNote Note Details Updated......");

                                    int accept_debit_max = 0;

                                    ResultSet rs_accept_debit_max = dbCon.search(con, "SELECT NVL(MAX(ACCEPT_NOTE_DETAILS_NO),0)+1 MAX "
                                            + "FROM SD_ACCEPT_DEBIT_NOTE_DETAIL WHERE ACCEPT_NOTE_NO = " + max_accept_no + "");

                                    while (rs_accept_debit_max.next()) {
                                        accept_debit_max = rs_accept_debit_max.getInt("MAX");
                                    }
                                    rs_accept_debit_max.close();

                                    accept_debit_max = accept_debit_max - 1;

                                    String accetDebitDetailsColumn = "ACCEPT_NOTE_DETAILS_NO,"
                                            + "ACCEPT_NOTE_NO,"
                                            + "DEBIT_NOTE_NO,"
                                            + "IMEI_NO,CUSTOMER_CODE,"
                                            + "SHOP_CODE,MODEL_NO,"
                                            + "BIS_ID,ORDER_NO,"
                                            + "SALES_PRICE,DATE_ACCEPTED,"
                                            + "STATUS,REMARKS,"
                                            + "USER_INSERTED,DATE_INSERTED";

                                    String accetDebitDetailsValue = "?,?,?,?,?,?,?,?,?,?,"
                                            + "TO_DATE(?,'YYYY/MM/DD'),1,?,?,SYSDATE";

                                    PreparedStatement ps_accept_demit_in = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_DEBIT_NOTE_DETAIL(" + accetDebitDetailsColumn + ") "
                                            + "VALUES(" + accetDebitDetailsValue + ")");
                                    for (ImportDebitNoteDetails di : imDeNoList) {

                                        accept_debit_max = accept_debit_max + 1;

                                        ps_accept_demit_in.setInt(1, accept_debit_max);
                                        ps_accept_demit_in.setInt(2, max_accept_no);
                                        ps_accept_demit_in.setString(3, di.getDebitNoteNo());
                                        ps_accept_demit_in.setString(4, di.getImeiNo());
                                        ps_accept_demit_in.setString(5, di.getCustomerCode());
                                        ps_accept_demit_in.setString(6, di.getShopCode());
                                        ps_accept_demit_in.setInt(7, di.getModleNo());
                                        ps_accept_demit_in.setInt(8, di.getBisId());
                                        ps_accept_demit_in.setString(9, di.getOrderNo());
                                        ps_accept_demit_in.setDouble(10, di.getSalerPrice());
                                        ps_accept_demit_in.setString(11, di.getDateAccepted());
                                        ps_accept_demit_in.setString(12, di.getRemarks());
                                        ps_accept_demit_in.setString(13, user_id);
                                        ps_accept_demit_in.addBatch();
                                    }
                                    ps_accept_demit_in.executeBatch();

                                    result = 1;
                                    logger.info("Accepteddebit Note Details Inserted......");

                                } catch (Exception e) {
                                    logger.info("Error in Accept Debit Note Detials Update....." + e.toString());
                                    result = 9;
                                }
                            }

                            if (result == 1) {

                                ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(ACCEPT_NOTE_NO),0)+1 MAX_NUM "
                                        + "FROM "
                                        + "SD_ACCEPT_DEBIT_NOTE_HEADER");

                                while (rs_max.next()) {
                                    max_accept_no = rs_max.getInt("MAX_NUM");
                                }
                                rs_max.close();

                                String insertColumns = "ACCEPT_NOTE_NO,"
                                        + "DEBIT_NOTE_NO,"
                                        + "ORDER_NO,INVOICE_NO,"
                                        + "CONTRACT_NO,PART_NO,"
                                        + "ORDER_QTY,DELIVERED_QTY,"
                                        + "DELIVERY_DATE,HEAD_STATUS,"
                                        + "CUSTOMER_NO,SITE_DESCRIPTION,"
                                        + "STATUS,USER_INSERTED,"
                                        + "DATE_INSERTED";

                                String insertValue = "?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                                        + "?,?,?,1,?,SYSDATE";

                                PreparedStatement ps_accept_header = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_DEBIT_NOTE_HEADER(" + insertColumns + ") "
                                        + "VALUES(" + insertValue + ")");

                                ps_accept_header.setInt(1, max_accept_no);
                                ps_accept_header.setString(2, importdebit_info.getDebitNoteNo());
                                ps_accept_header.setString(3, importdebit_info.getOrderNo());
                                ps_accept_header.setString(4, importdebit_info.getInvoiceNo());
                                ps_accept_header.setString(5, importdebit_info.getContactNo());
                                ps_accept_header.setString(6, importdebit_info.getPartNo());
                                ps_accept_header.setInt(7, importdebit_info.getOrderQty());
                                ps_accept_header.setInt(8, importdebit_info.getDeleveryQty());
                                ps_accept_header.setString(9, importdebit_info.getDeleveryDate());
                                ps_accept_header.setInt(10, importdebit_info.getHeadStatus());
                                ps_accept_header.setString(11, importdebit_info.getCustomerNo());
                                ps_accept_header.setString(12, importdebit_info.getSiteDescription());
                                ps_accept_header.setString(13, user_id);
                                ps_accept_header.executeUpdate();

                                result = 1;
                                logger.info("Accept Debit Note Header Details Inserted.....");

                                String dbHeaderUpdate = "UPDATE SD_DEBIT_NOTE_HEADER "
                                        + "SET STATUS = ?,"
                                        + "USER_MODIFIED= ?,"
                                        + "DATE_MODIFIED=SYSDATE "
                                        + "WHERE DEBIT_NOTE_NO =? ";

                                PreparedStatement ps = dbCon.prepare(con, dbHeaderUpdate);
                                ps.setInt(1, importdebit_info.getStatus());
                                ps.setString(2, user_id);
                                ps.setString(3, importdebit_info.getDebitNoteNo());
                                ps.executeUpdate();
                                logger.info("Update Debit Note Header Table......");

                            }

                        }
                    }
                } else {

                }
            } else {

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error addacceptDebitDetials Method..:" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Debit Accept Successfully");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMEI No is Already Accepted");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + return_imei + " IMEI Number Inactive in Device Master");
            vw.setReturnRefNo(return_imei);
        } else if (con == null) {
            vw.setReturnFlag("1000000");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Debit Note Accept.");
        }
        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static ArrayList<ImportDebitNoteDetails> removeFromList(ArrayList<ImportDebitNoteDetails> debitDetailList, ArrayList<String> imeiList) {
        try {
            for (String e : imeiList) {
                logger.info("Debit note list :" + e);
            }
            if (!imeiList.isEmpty()) {
                for (int i = 0; i < debitDetailList.size(); i++) {
                    for (String imei : imeiList) {
                        if (debitDetailList.get(i).getImeiNo().equals(imei)) {
                            logger.info("removing : " + i);
                            debitDetailList.remove(i);
                        }
                    }
                }
            }
            for (ImportDebitNoteDetails d : debitDetailList) {
                logger.info("Debit note list :" + d.getImeiNo());
            }
        } catch (Exception e) {
            throw e;
        }
        return debitDetailList;
    }

    public static ValidationWrapper addacceptDebitDetialsAll(ImportDebitNote importdebit_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addacceptDebitDetials Method Call............1.1.................");
        int result = 0;
        int max_accept_no = 0;
        int ime_un = 0;
        int inactive_cnt = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<String> creditList = new ArrayList<>();
        ArrayList<String> removeImeiList = new ArrayList<>();
        int credit_cnt = 0;
        try {

            if (importdebit_info != null) {

                ArrayList<ImportDebitNoteDetails> imeiAlrady = new ArrayList<ImportDebitNoteDetails>();
                imeiAlrady = importdebit_info.getImportDebitList();

                if (imeiAlrady.size() > 0) {

                    String selectedImeis = "";

                    //  ArrayList<String> importList = new ArrayList<>();
                    for (ImportDebitNoteDetails id : imeiAlrady) {
                        //   importList.add(id.getImeiNo());
                        selectedImeis += "'" + id.getImeiNo() + "'" + ",";
                    }

                    selectedImeis = selectedImeis.substring(0, (selectedImeis.length() - 1));

                    ArrayList<String> masterList = new ArrayList<>();
                    //ResultSet rs_acept_cnt = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS in(1,3)");
                    ResultSet rs_acept_cnt = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS IN (1,3) AND IMEI_NO IN (" + selectedImeis + ") ");

                    while (rs_acept_cnt.next()) {
                        String imei = rs_acept_cnt.getString("IMEI_NO");
                        masterList.add(imei);

                    }

                    for (String s : masterList) {
                        //  if (masterList.contains(s)) {
                        logger.info("Imei in the master :" + s);

                        int temp_list1_cnt = 0;
                        int temp_list2_cnt = 0;

                        int temp_list3_cnt = 0;

                        ResultSet rs_temp_List3 = dbCon.search(con, "SELECT COUNT(*) CNT "
                                + "FROM SD_DEVICE_MASTER "
                                + "WHERE STATUS = 3 "
                                + "AND IMEI_NO='" + s + "'");

                        while (rs_temp_List3.next()) {
                            temp_list3_cnt = rs_temp_List3.getInt("CNT");
                            logger.info("Error occured with imei :" + s + "::::Error Code : " + AppParams.ACCEPT_DEBIT_ERROR_sold);
                        }

                        logger.info("status 3 count : " + temp_list3_cnt);

                        ResultSet rs_temp_List1 = dbCon.search(con, "SELECT COUNT(*) CNT "
                                + "FROM TEMP_SD_DEVICE_MASTER "
                                + "WHERE TRANSACTION_FLAG = 0 "
                                + "AND FOUNDED_FLAG=0 "
                                + "AND IMEI_NO = '" + s + "'");

                        while (rs_temp_List1.next()) {
                            temp_list1_cnt = rs_temp_List1.getInt("CNT");
                        }
                        logger.info("TEMP_SD_DEVICE_MASTER count :" + temp_list1_cnt);
                        if (temp_list1_cnt > 0) {
                            logger.info("Imei in the TEMP_SD_DEVICE_MASTER :" + s);
                            result = deleteDuplicateImeiMaster(s);
                            updateTempImeiMasterList1(s);
                        } else {

                            ResultSet rs_temp_List2 = dbCon.search(con, "SELECT COUNT(*) CNT "
                                    + "FROM TEMP_DEVICE_MASTER_20150617 "
                                    + "WHERE TRANSACTION_FLAG = 0 "
                                    + "AND  FOUNDED_FLAG = 0 "
                                    + "AND IMEI_NO = '" + s + "'");

                            while (rs_temp_List2.next()) {

                                temp_list2_cnt = rs_temp_List2.getInt("CNT");
                            }

                            if (temp_list2_cnt > 0) {
                                logger.info("Imei in the TEMP_DEVICE_MASTER_20150617 :" + s);
                                result = deleteDuplicateImeiMaster(s);
                                updateTempImeiMasterList2(s);
                            } else {

                                ResultSet rs_credit_note = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_CREDIT_NOTE_ISSUE_DTL WHERE IMEI_NO = '" + s + "'");

                                while (rs_credit_note.next()) {

                                    credit_cnt = rs_credit_note.getInt("CNT");
                                }

                                if (credit_cnt >= 1) {
                                    logger.info("in the credit note  :" + s);
                                    creditList.add(s);
                                } else {
                                    logger.info("Error occured with imei :" + s + "::::Error Code : " + AppParams.ACCEPT_DEBIT_ERROR);
                                    removeImeiList.add(s);

//                                        return_imei += s + ",";
//                                        ime_un = 1;
                                }

                            }

                        }

                        //  }
                    }

                    ArrayList<String> inactiveList = new ArrayList<>();
                    //ResultSet rs_inative_imei = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS =9");
                    ResultSet rs_inative_imei = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_MASTER WHERE STATUS =9 AND IMEI_NO IN (" + selectedImeis + ") ");

                    while (rs_inative_imei.next()) {
                        String inactive_imei = rs_inative_imei.getString("IMEI_NO");
                        inactiveList.add(inactive_imei);

                    }

                    if (ime_un == 1) {
                        result = 2;
                    } else {

                        for (String s : inactiveList) {
                            // if (inactiveList.contains(s)) {

                            int temp_list1_cnt = 0;
                            int temp_list2_cnt = 0;

                            ResultSet rs_temp_List1 = dbCon.search(con, "SELECT COUNT(*) CNT FROM TEMP_SD_DEVICE_MASTER WHERE IMEI_NO = '" + s + "'");

                            while (rs_temp_List1.next()) {
                                temp_list1_cnt = rs_temp_List1.getInt("CNT");
                            }

                            if (temp_list1_cnt > 0) {
                                result = deleteDuplicateImeiMaster(s);
                            } else {

                                ResultSet rs_temp_List2 = dbCon.search(con, "SELECT COUNT(*) CNT FROM TEMP_DEVICE_MASTER_20150617 WHERE IMEI_NO = '" + s + "'");

                                while (rs_temp_List2.next()) {
                                    temp_list2_cnt = rs_temp_List2.getInt("CNT");
                                }

                                if (temp_list2_cnt > 0) {
                                    result = deleteDuplicateImeiMaster(s);
                                } else {

                                    ResultSet rs_credit_note = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_CREDIT_NOTE_ISSUE_DTL WHERE IMEI_NO = '" + s + "'");

                                    while (rs_credit_note.next()) {

                                        credit_cnt = rs_credit_note.getInt("CNT");
                                    }

                                    if (credit_cnt >= 1) {
                                        creditList.add(s);
                                    } else {
                                        logger.info("Error occured with imei :" + s + "::::Error Code : " + AppParams.ACCEPT_DEBIT_ERROR);
                                        removeImeiList.add(s);
//                                            return_imei += s + ",";
//                                            inactive_cnt = 1;
                                    }

                                }

                            }

                            // }
                        }

                        if (inactive_cnt == 1) {
                            result = 3;
                        } else {

                            ArrayList<ImportDebitNoteDetails> imDeNoList = new ArrayList<ImportDebitNoteDetails>();

                            if (importdebit_info.getImportDebitList() != null) {
                                try {
                                    ArrayList<ImportDebitNoteDetails> temimDeNoList = new ArrayList<ImportDebitNoteDetails>();

                                    ResultSet rs_modle = dbCon.search(con, "SELECT MODEL_NO,"
                                            + "MODEL_DESCRIPTION,"
                                            + "PART_PRD_CODE_DESC,"
                                            + "PART_PRD_FAMILY_DESC "
                                            + "FROM SD_MODEL_LISTS");
                                    while (rs_modle.next()) {
                                        ImportDebitNoteDetails id = new ImportDebitNoteDetails();
                                        id.setModleNo(rs_modle.getInt("MODEL_NO"));
                                        id.setBrand(rs_modle.getString("PART_PRD_CODE_DESC"));
                                        id.setProduct(rs_modle.getString("PART_PRD_FAMILY_DESC"));
                                        temimDeNoList.add(id);
                                    }

                                    imDeNoList = importdebit_info.getImportDebitList();
//                                    imDeNoList = removeFromList(importdebit_info.getImportDebitList(), removeImeiList);

                                    String imeiMasterColumn = "IMEI_NO,MODEL_NO,"
                                            + "BIS_ID,DEBIT_NOTE_NO,"
                                            + "SALES_PRICE,"
                                            + "STATUS,USER_INSERTED,"
                                            + "DATE_INSERTED,ORDER_NO,BRAND,PRODUCT,BAD_ITEM_FLAG";

                                    String imeiMasterValue = "?,?,?,?,?,1,?,SYSDATE,?,?,?,?";

                                    PreparedStatement ps_master_in = dbCon.prepare(con, "INSERT INTO SD_DEVICE_MASTER(" + imeiMasterColumn + ") "
                                            + "VALUES(" + imeiMasterValue + ")");

                                    PreparedStatement ps_up_imei_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                            + "SET MODEL_NO = ?,"
                                            + "BIS_ID = ?,"
                                            + "DEBIT_NOTE_NO = ?,"
                                            + "SALES_PRICE = ?,"
                                            + "STATUS = 1,"
                                            + "USER_INSERTED = ?,"
                                            + "DATE_INSERTED = SYSDATE,"
                                            + "ORDER_NO = ?,"
                                            + "BRAND = ?,"
                                            + "PRODUCT = ?,"
                                            + "BAD_ITEM_FLAG = ? "
                                            + "WHERE IMEI_NO = ?");

                                    for (ImportDebitNoteDetails di : imDeNoList) {

                                        if (removeImeiList.contains(di.getImeiNo())) {
                                            logger.info("imei in the remove list");
                                            PreparedStatement ps_temp = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL "
                                                    + "SET STATUS = 6 "
                                                    + "WHERE "
                                                    + "DEBIT_NOTE_NO ='" + di.getDebitNoteNo() + "' AND "
                                                    + "IMEI_NO = '" + di.getImeiNo() + "'");
                                            ps_temp.execute();
                                        } else if (creditList.contains(di.getImeiNo())) {

                                            ps_up_imei_master.setInt(1, di.getModleNo());
                                            ps_up_imei_master.setInt(2, di.getBisId());
                                            ps_up_imei_master.setString(3, di.getDebitNoteNo());
                                            ps_up_imei_master.setDouble(4, di.getSalerPrice());
                                            ps_up_imei_master.setString(5, user_id);
                                            ps_up_imei_master.setString(6, di.getOrderNo());

                                            for (ImportDebitNoteDetails idn : temimDeNoList) {
                                                if (di.getModleNo() == idn.getModleNo()) {
                                                    ps_up_imei_master.setString(7, idn.getBrand());
                                                    ps_up_imei_master.setString(8, idn.getProduct());
                                                }
                                            }
                                            ps_up_imei_master.setInt(9, 0);
                                            ps_up_imei_master.setString(10, di.getImeiNo());
                                            ps_up_imei_master.addBatch();

                                        } else {
                                            logger.info("inserting : " + di.getImeiNo());
                                            ps_master_in.setString(1, di.getImeiNo());
                                            ps_master_in.setInt(2, di.getModleNo());
                                            ps_master_in.setInt(3, di.getBisId());
                                            ps_master_in.setString(4, di.getDebitNoteNo());
                                            ps_master_in.setDouble(5, di.getSalerPrice());
                                            ps_master_in.setString(6, user_id);
                                            ps_master_in.setString(7, di.getOrderNo());

                                            for (ImportDebitNoteDetails idn : temimDeNoList) {
                                                if (di.getModleNo() == idn.getModleNo()) {
                                                    ps_master_in.setString(8, idn.getBrand());
                                                    ps_master_in.setString(9, idn.getProduct());
                                                }
                                            }
                                            ps_master_in.setInt(10, 0);
                                            ps_master_in.addBatch();
                                        }
                                    }
                                    ps_master_in.executeBatch();
                                    ps_up_imei_master.executeBatch();

                                    PreparedStatement ps_debit_details = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL "
                                            + "SET STATUS = 3 ,"
                                            + "USER_MODIFIED=?,"
                                            + "DATE_MODIFIED=SYSDATE "
                                            + "WHERE DEBIT_NOTE_NO = ? "
                                            + "AND IMEI_NO = ?");

                                    for (ImportDebitNoteDetails di : imDeNoList) {

                                        if (removeImeiList.contains(di.getImeiNo())) {

                                            logger.info("imei in the remove list");
                                            PreparedStatement ps_temp = dbCon.prepare(con, "UPDATE SD_DEBIT_NOTE_DETAIL "
                                                    + "SET STATUS = 6 "
                                                    + "WHERE "
                                                    + "DEBIT_NOTE_NO ='" + di.getDebitNoteNo() + "' AND "
                                                    + "IMEI_NO = '" + di.getImeiNo() + "'");
                                            ps_temp.execute();
                                        } else {

                                            ps_debit_details.setString(1, user_id);
                                            ps_debit_details.setString(2, di.getDebitNoteNo());
                                            ps_debit_details.setString(3, di.getImeiNo());
                                            ps_debit_details.addBatch();
                                        }
                                    }
                                    ps_debit_details.executeBatch();
                                    result = 1;
                                    logger.info("DebitNote Note Details Updated......");

                                    int accept_debit_max = 0;

                                    ResultSet rs_accept_debit_max = dbCon.search(con, "SELECT NVL(MAX(ACCEPT_NOTE_DETAILS_NO),0)+1 MAX "
                                            + "FROM SD_ACCEPT_DEBIT_NOTE_DETAIL WHERE ACCEPT_NOTE_NO = " + max_accept_no + "");

                                    while (rs_accept_debit_max.next()) {
                                        accept_debit_max = rs_accept_debit_max.getInt("MAX");
                                    }
                                    rs_accept_debit_max.close();

                                    accept_debit_max = accept_debit_max - 1;

                                    String accetDebitDetailsColumn = "ACCEPT_NOTE_DETAILS_NO,"
                                            + "ACCEPT_NOTE_NO,"
                                            + "DEBIT_NOTE_NO,"
                                            + "IMEI_NO,CUSTOMER_CODE,"
                                            + "SHOP_CODE,MODEL_NO,"
                                            + "BIS_ID,ORDER_NO,"
                                            + "SALES_PRICE,DATE_ACCEPTED,"
                                            + "STATUS,REMARKS,"
                                            + "USER_INSERTED,DATE_INSERTED";

                                    String accetDebitDetailsValue = "?,?,?,?,?,?,?,?,?,?,"
                                            + "TO_DATE(?,'YYYY/MM/DD'),1,?,?,SYSDATE";

                                    PreparedStatement ps_accept_demit_in = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_DEBIT_NOTE_DETAIL(" + accetDebitDetailsColumn + ") "
                                            + "VALUES(" + accetDebitDetailsValue + ")");
                                    for (ImportDebitNoteDetails di : imDeNoList) {

                                        accept_debit_max = accept_debit_max + 1;

                                        ps_accept_demit_in.setInt(1, accept_debit_max);
                                        ps_accept_demit_in.setInt(2, max_accept_no);
                                        ps_accept_demit_in.setString(3, di.getDebitNoteNo());
                                        ps_accept_demit_in.setString(4, di.getImeiNo());
                                        ps_accept_demit_in.setString(5, di.getCustomerCode());
                                        ps_accept_demit_in.setString(6, di.getShopCode());
                                        ps_accept_demit_in.setInt(7, di.getModleNo());
                                        ps_accept_demit_in.setInt(8, di.getBisId());
                                        ps_accept_demit_in.setString(9, di.getOrderNo());
                                        ps_accept_demit_in.setDouble(10, di.getSalerPrice());
                                        ps_accept_demit_in.setString(11, di.getDateAccepted());
                                        ps_accept_demit_in.setString(12, di.getRemarks());
                                        ps_accept_demit_in.setString(13, user_id);
                                        ps_accept_demit_in.addBatch();
                                    }
                                    ps_accept_demit_in.executeBatch();

                                    result = 1;
                                    logger.info("Accepteddebit Note Details Inserted......");

                                } catch (Exception e) {
                                    logger.info("Error in Accept Debit Note Detials Update....." + e.toString());
                                    result = 9;
                                }
                            }

                            if (result == 1) {

                                ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(ACCEPT_NOTE_NO),0)+1 MAX_NUM "
                                        + "FROM "
                                        + "SD_ACCEPT_DEBIT_NOTE_HEADER");

                                while (rs_max.next()) {
                                    max_accept_no = rs_max.getInt("MAX_NUM");
                                }
                                rs_max.close();

                                String insertColumns = "ACCEPT_NOTE_NO,"
                                        + "DEBIT_NOTE_NO,"
                                        + "ORDER_NO,INVOICE_NO,"
                                        + "CONTRACT_NO,PART_NO,"
                                        + "ORDER_QTY,DELIVERED_QTY,"
                                        + "DELIVERY_DATE,HEAD_STATUS,"
                                        + "CUSTOMER_NO,SITE_DESCRIPTION,"
                                        + "STATUS,USER_INSERTED,"
                                        + "DATE_INSERTED";

                                String insertValue = "?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                                        + "?,?,?,1,?,SYSDATE";

                                PreparedStatement ps_accept_header = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_DEBIT_NOTE_HEADER(" + insertColumns + ") "
                                        + "VALUES(" + insertValue + ")");

                                ps_accept_header.setInt(1, max_accept_no);
                                ps_accept_header.setString(2, importdebit_info.getDebitNoteNo());
                                ps_accept_header.setString(3, importdebit_info.getOrderNo());
                                ps_accept_header.setString(4, importdebit_info.getInvoiceNo());
                                ps_accept_header.setString(5, importdebit_info.getContactNo());
                                ps_accept_header.setString(6, importdebit_info.getPartNo());
                                ps_accept_header.setInt(7, importdebit_info.getOrderQty());
                                ps_accept_header.setInt(8, importdebit_info.getDeleveryQty());
                                ps_accept_header.setString(9, importdebit_info.getDeleveryDate());
                                ps_accept_header.setInt(10, importdebit_info.getHeadStatus());
                                ps_accept_header.setString(11, importdebit_info.getCustomerNo());
                                ps_accept_header.setString(12, importdebit_info.getSiteDescription());
                                ps_accept_header.setString(13, user_id);
                                ps_accept_header.executeUpdate();

                                result = 1;
                                logger.info("Accept Debit Note Header Details Inserted.....");

                                int dtlAcceptedCnt = 0;

                                ResultSet rs_accept_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                                        + "FROM SD_DEBIT_NOTE_DETAIL "
                                        + "WHERE DEBIT_NOTE_NO = '" + importdebit_info.getDebitNoteNo() + "' "
                                        + "AND STATUS = 1");

                                while (rs_accept_cnt.next()) {
                                    dtlAcceptedCnt = rs_accept_cnt.getInt("CNT");
                                }

                                if (dtlAcceptedCnt > 0) {

                                } else {
                                    String dbHeaderUpdate = "UPDATE SD_DEBIT_NOTE_HEADER "
                                            + "SET STATUS = ?,"
                                            + "USER_MODIFIED= ?,"
                                            + "DATE_MODIFIED=SYSDATE "
                                            + "WHERE DEBIT_NOTE_NO =? ";

                                    PreparedStatement ps = dbCon.prepare(con, dbHeaderUpdate);
                                    ps.setInt(1, 3);
                                    ps.setString(2, user_id);
                                    ps.setString(3, importdebit_info.getDebitNoteNo());
                                    ps.executeUpdate();
                                    logger.info("Update Debit Note Header Table......");
                                }

                            }

                        }
                    }
                } else {

                }
            } else {

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error addacceptDebitDetials Method..:" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Debit Accept Successfully");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMEI No is Already Accepted");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + return_imei + " IMEI Number Inactive in Device Master");
            vw.setReturnRefNo(return_imei);
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Debit Note Accept.");
        }
        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static ValidationWrapper editAcceptDebitNote(ImportDebitNote importdebit_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editAcceptDebitNote Method Call.............................");
        int result = 1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            try {

                dbCon.save(con, "UPDATE SD_DEBIT_NOTE_HEADER "
                        + "SET STATUS = " + importdebit_info.getStatus() + ","
                        + " USER_MODIFIED = '" + user_id + "',"
                        + " DATE_MODIFIED = SYSDATE "
                        + " WHERE DEBIT_NOTE_NO = '" + importdebit_info.getDebitNoteNo() + "'");

                logger.info("Accept Debit Note Header Details Inserted.....");

            } catch (Exception e) {
                logger.info("Error Accept Debit Note Header Details Insert....." + e.toString());
                result = 9;
            }

            String accetDebitDetailsColumn = "DEBIT_NOTE_NO,"
                    + "IMEI_NO,CUSTOMER_CODE,"
                    + "SHOP_CODE,MODEL_NO,"
                    + "BIS_ID,ORDER_NO,"
                    + "SALES_PRICE,DATE_ACCEPTED,"
                    + "STATUS,REMARKS,"
                    + "USER_INSERTED,DATE_INSERTED";

            ArrayList<ImportDebitNoteDetails> imDeNoList = new ArrayList<ImportDebitNoteDetails>();
            if (importdebit_info.getImportDebitList() != null) {
                try {

                    logger.info("Delete in Debit Note No " + importdebit_info.getDebitNoteNo() + " in Debit Note Details Tabel");

                    dbCon.save(con, "DELETE FROM SD_DEBIT_NOTE_DETAIL WHERE DEBIT_NOTE_NO = '" + importdebit_info.getDebitNoteNo() + "'");

                    logger.info("Debit Note Details Tabel Deleted");

                    imDeNoList = importdebit_info.getImportDebitList();

                    String imDeNoValue = "?,?,?,"
                            + "?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,SYSDATE";

                    PreparedStatement ps_in_debit = dbCon.prepare(con, "INSERT INTO SD_DEBIT_NOTE_DETAIL(" + accetDebitDetailsColumn + ") "
                            + "VALUES(" + imDeNoValue + ")");

                    for (ImportDebitNoteDetails di : imDeNoList) {

                        ps_in_debit.setString(1, di.getDebitNoteNo());
                        ps_in_debit.setString(2, di.getImeiNo());
                        ps_in_debit.setString(3, di.getCustomerCode());
                        ps_in_debit.setString(4, di.getShopCode());
                        ps_in_debit.setInt(5, di.getModleNo());
                        ps_in_debit.setInt(6, di.getBisId());
                        ps_in_debit.setString(7, di.getOrderNo());
                        ps_in_debit.setDouble(8, di.getSalerPrice());
                        ps_in_debit.setString(9, di.getDateAccepted());
                        ps_in_debit.setInt(10, di.getStatus());
                        ps_in_debit.setString(11, di.getRemarks());
                        ps_in_debit.setString(12, user_id);
                        ps_in_debit.addBatch();

                        logger.info("Import Debit Note IMEI Inserted........");
                    }
                    ps_in_debit.executeBatch();
                } catch (Exception e) {
                    logger.info("Error in Accept Debit Note IMEI Insert....." + e.toString());
                    result = 9;
                }
            }
        } catch (Exception ex) {
            logger.info("Error editAcceptDebitNote Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Accept Debit Note Insert Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Accept Debit Note Insert Fail");
        }
        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static ValidationWrapper deleteImeiMaster(String imei_no,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deleteImeiMaster Method Call.............................");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            PreparedStatement ps_delete = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET STATUS = 9 WHERE IMEI_NO = ?");
            ps_delete.setString(1, imei_no);
            ps_delete.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error deleteImeiMaster Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("IMEI Remove Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in IMEI Remove");
        }
        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static int deleteDuplicateImeiMaster(String imei) {
        logger.info("deleteDuplicateImeiMaster Method Call........");
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int result = 0;
        try {

            PreparedStatement ps_delete_masterImei = dbCon.prepare(con, "DELETE FROM SD_DEVICE_MASTER WHERE IMEI_NO = ?");
            ps_delete_masterImei.setString(1, imei);
            ps_delete_masterImei.executeUpdate();
            result = 1;

        } catch (Exception e) {
            logger.info("Error in deleteDuplicateImeiMaster Method " + e.getMessage());
        }

        return result;

    }

    public static void updateTempImeiMasterList1(String imei) {
        logger.info("updateTempImeiMaster Method Call........");
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_delete_masterImei = dbCon.prepare(con, "UPDATE TEMP_SD_DEVICE_MASTER "
                    + "SET FOUNDED_DATE = SYSDATE,"
                    + "FOUNDED_FLAG = 1 "
                    + "WHERE IMEI_NO =  ?");
            ps_delete_masterImei.setString(1, imei);
            ps_delete_masterImei.executeUpdate();

        } catch (Exception e) {
            logger.info("Error in updateTempImeiMaster Method " + e.getMessage());
        }

    }

    public static void updateTempImeiMasterList2(String imei) {
        logger.info("updateTempImeiMaster Method Call........");
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_delete_masterImei = dbCon.prepare(con, "UPDATE TEMP_DEVICE_MASTER_20150617 "
                    + "SET FOUNDED_DATE = SYSDATE,"
                    + "FOUNDED_FLAG = 1 "
                    + "WHERE IMEI_NO =  ?");
            ps_delete_masterImei.setString(1, imei);
            ps_delete_masterImei.executeUpdate();

        } catch (Exception e) {
            logger.info("Error in updateTempImeiMaster Method " + e.getMessage());
        }

    }

}
