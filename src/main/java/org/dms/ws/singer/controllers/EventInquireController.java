/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.EventAward;
import org.dms.ws.singer.entities.EventBisType;
import org.dms.ws.singer.entities.EventBussinessList;
import org.dms.ws.singer.entities.EventMaster;
import org.dms.ws.singer.entities.EventNonSalesRule;
import org.dms.ws.singer.entities.EventParticipant;
import org.dms.ws.singer.entities.EventRulePointScheme;
import org.dms.ws.singer.entities.EventSalesRule;
import org.dms.ws.singer.entities.Part;
import org.dms.ws.singer.entities.WorkOrderDefect;
import org.dms.ws.singer.entities.WorkOrderStatusInquiry;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class EventInquireController {

    public static MessageWrapper getEventInqurieList(int event_id,
            String event_name,
            String event_desc,
            String start_date,
            String end_date,
            String threshold,
            String achievement,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getEventInqurieList method Call..........");
        ArrayList<EventMaster> eventList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            order = DBMapper.eventInquire(order);
            String eventId = ((event_id == 0) ? "" : "AND EVENT_ID = '" + event_id + "' ");
            String eventName = (event_name.equalsIgnoreCase("all") ? "NVL(EVENT_NAME,'AA') = NVL(EVENT_NAME,'AA') " : "UPPER(EVENT_NAME) LIKE UPPER('%" + event_name + "%')");
            String eventDesc = (event_desc.equalsIgnoreCase("all") ? "NVL(EVENT_DESC,'AA') = NVL(EVENT_DESC,'AA') " : "UPPER(EVENT_DESC) LIKE UPPER('%" + event_desc + "%')");
            String startDate = (start_date.equalsIgnoreCase("all") ? "NVL(START_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(START_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(START_DATE,'YYYY/MM/DD') = TO_DATE('%" + start_date + "%','YYYY/MM/DD')");
            String endDate = (end_date.equalsIgnoreCase("all") ? "NVL(END_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(END_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(END_DATE,'YYYY/MM/DD') = TO_DATE('%" + end_date + "%','YYYY/MM/DD')");
            String Thresold = (threshold.equalsIgnoreCase("all") ? "NVL(THRESHOLD,'AA') = NVL(THRESHOLD,'AA') " : "UPPER(THRESHOLD) LIKE UPPER('%" + threshold + "%')");
            String Archive = (achievement.equalsIgnoreCase("all") ? "" : "AND ACHIVE = " + achievement + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY EVENT_ID" : "ORDER BY " + order + "   " + type + "");
            String selectColumn = "EVENT_ID,EVENT_NAME,"
                    + "EVENT_DESC,"
                    + "START_DATE,"
                    + "END_DATE,"
                    + "THRESHOLD,"
                    + "STATUS,"
                    + "ACHIVE";

            String whereClous = "" + eventName + " "
                    + "" + eventId + " "
                    + "AND " + eventDesc + " "
                    + "AND " + startDate + " "
                    + "AND " + endDate + " "
                    + "AND " + Thresold + " "
                    + " " + Archive + "";

            String tables = "(SELECT EVENT_ID,"
                    + "EVENT_NAME,"
                    + "EVENT_DESC,"
                    + "START_DATE,"
                    + "END_DATE,"
                    + "THRESHOLD,"
                    + "STATUS,"
                    + "((AC/THRESHOLD)*100) ACHIVE FROM (SELECT F.EVENT_ID,"
                    + "F.EVENT_NAME,"
                    + "F.EVENT_DESC,"
                    + "TO_CHAR(F.START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(F.END_DATE,'YYYY/MM/DD') END_DATE,"
                    + "F.THRESHOLD,"
                    + "F.STATUS,"
                    + "(SELECT COUNT(*) FROM SD_WARRENTY_REGISTRATION W,SD_DEVICE_MASTER D "
                    + "WHERE W.IMEI_NO = D.IMEI_NO (+) "
                    + "AND D.MODEL_NO IN (SELECT S.MODEL_NO "
                    + "FROM SD_EVENT_MASTER E,SD_EVENT_SALE_RULES S "
                    + "WHERE  S.EVENT_ID = F.EVENT_ID )) AC "
                    + "FROM SD_EVENT_MASTER F))";

            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventMaster em = new EventMaster();
                totalrecode = rs.getInt("CNT");
                em.setEventId(rs.getInt("EVENT_ID"));;
                em.setEventName(rs.getString("EVENT_NAME"));
                em.setEventDesc(rs.getString("EVENT_DESC"));
                em.setStartDate(rs.getString("START_DATE"));
                em.setEndDate(rs.getString("END_DATE"));
                em.setThreshold(rs.getString("THRESHOLD"));
                em.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    em.setStatusMsg("Planning");
                } else if (rs.getInt("STATUS") == 3) {
                    em.setStatusMsg("Started");
                } else if (rs.getInt("STATUS") == 9) {
                    em.setStatusMsg("Closed");
                } else {
                    em.setStatusMsg("None");
                }
                em.setEventAchievement(rs.getString("ACHIVE"));

                if (rs.getInt("STATUS") == 3) {
                    eventList.add(em);
                }

            }

        } catch (Exception ex) {
            logger.info("Error getEventInqurieList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getEventMasterDetails(int event_id) {
        logger.info("getEventMasterDetails method Call..........");
        ArrayList<EventMaster> eventList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String selectColumn = "EVENT_ID,EVENT_NAME,EVENT_DESC,"
                    + "TO_CHAR(START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(END_DATE,'YYYY/MM/DD') END_DATE,"
                    + "THRESHOLD,STATUS,"
                    + "EVENT_MGR_COMMENTS,MKT_MGR_COMMENTS";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM SD_EVENT_MASTER "
                    + "WHERE EVENT_ID = " + event_id + "");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventMaster em = new EventMaster();
                totalrecode = 1;
                em.setEventId(rs.getInt("EVENT_ID"));;
                em.setEventName(rs.getString("EVENT_NAME"));
                em.setEventDesc(rs.getString("EVENT_DESC"));
                em.setStartDate(rs.getString("START_DATE"));
                em.setEndDate(rs.getString("END_DATE"));
                em.setThreshold(rs.getString("THRESHOLD"));
                em.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    em.setStatusMsg("Planning");
                } else if (rs.getInt("STATUS") == 3) {
                    em.setStatusMsg("Started");
                } else if (rs.getInt("STATUS") == 9) {
                    em.setStatusMsg("Closed");
                } else {
                    em.setStatusMsg("None");
                }
                em.setEventMrgComment(rs.getString("EVENT_MGR_COMMENTS"));
                em.setMktMrgComment(rs.getString("MKT_MGR_COMMENTS"));
                // eventList.add(em);
//------------------------------------------------------------------------------
                ArrayList<EventSalesRule> eventSalesRuleList = new ArrayList<>();

                String salesRule = "E.RULE_ID,"
                        + "E.EVENT_ID,"
                        + "E.PRODUCT_FAMILY,"
                        + "E.BRAND,"
                        + "E.MODEL_NO,"
                        + "E.PERIOD,"
                        + "E.INTERVALS,"
                        + "E.ACCEPTED_INTERVALS,"
                        + "E.SALES_TYPE,"
                        + "E.POINTS,"
                        + "E.MIN_REQ,"
                        + "E.POINT_SCHEME_FLAG,"
                        + "E.TOT_ACHIEVEMENT,"
                        + "E.STATUS,"
                        + "M.MODEL_DESCRIPTION";

                ResultSet rs_salesRule = dbCon.search(con, "SELECT " + salesRule + " FROM SD_EVENT_SALE_RULES E,SD_MODEL_LISTS M "
                        + "WHERE E.MODEL_NO = M.MODEL_NO (+)  "
                        + "AND EVENT_ID = " + event_id + "");

                logger.info("Get Data to Result Set.............");

                while (rs_salesRule.next()) {
                    EventSalesRule esr = new EventSalesRule();
                    esr.setRuleId(rs_salesRule.getInt("RULE_ID"));;
                    esr.setEventId(rs_salesRule.getInt("EVENT_ID"));
                    esr.setProductFamily(rs_salesRule.getString("PRODUCT_FAMILY"));
                    esr.setBarnd(rs_salesRule.getString("BRAND"));
                    esr.setModleNo(rs_salesRule.getInt("MODEL_NO"));
                    esr.setPeriod(rs_salesRule.getInt("PERIOD"));
                    esr.setIntervals(rs_salesRule.getInt("INTERVALS"));
                    esr.setAcceptedInteval(rs_salesRule.getInt("ACCEPTED_INTERVALS"));
                    esr.setSalesType(rs_salesRule.getInt("SALES_TYPE"));
                    esr.setPoint(rs_salesRule.getDouble("POINTS"));
                    esr.setMinReq(rs_salesRule.getDouble("MIN_REQ"));
                    esr.setPointSchemeFlag(rs_salesRule.getInt("POINT_SCHEME_FLAG"));
                    esr.setTotalAchivement(rs_salesRule.getDouble("TOT_ACHIEVEMENT"));
                    esr.setStatus(rs_salesRule.getInt("STATUS"));
                    esr.setModleDesc(rs_salesRule.getString("MODEL_DESCRIPTION"));

                    String selectPointSchema = "POINT_SCHEME_ID,"
                            + "RULE_ID,"
                            + "SALES_TYPE,"
                            + "NO_OF_UNITS,"
                            + "FIXED_POINTS,"
                            + "FLOAT_FLAG,"
                            + "ADDN_SALES_TYPE,"
                            + "ADDN_SALES_POINTS,"
                            + "ASSIGN_POINTS,"
                            + "STATUS";

                    ResultSet rs_pointSchema = dbCon.search(con, "SELECT " + selectPointSchema + " FROM SD_EVENT_RULE_POINT_SCHEMES "
                            + "WHERE RULE_ID = " + rs_salesRule.getInt("RULE_ID") + "");

                    ArrayList<EventRulePointScheme> pointSchemaList = new ArrayList<>();

                    while (rs_pointSchema.next()) {
                        EventRulePointScheme ps = new EventRulePointScheme();
                        ps.setPointSchemaId(rs_pointSchema.getInt("POINT_SCHEME_ID"));;
                        ps.setSchmeruleId(rs_pointSchema.getInt("RULE_ID"));
                        ps.setSchemeSaleType(rs_pointSchema.getInt("SALES_TYPE"));
                        ps.setSchmeNoOfUnit(rs_pointSchema.getInt("NO_OF_UNITS"));
                        ps.setScheamfixedPoint(rs_pointSchema.getDouble("FIXED_POINTS"));
                        ps.setSchmefloatFlag(rs_pointSchema.getInt("FLOAT_FLAG"));
                        ps.setSchmeAddnSalesType(rs_pointSchema.getInt("ADDN_SALES_TYPE"));
                        ps.setSchmeAddnSalesPoint(rs_pointSchema.getDouble("ADDN_SALES_POINTS"));
                        ps.setSchmeAssignPoint(rs_pointSchema.getDouble("ASSIGN_POINTS"));
                        ps.setSchmestatus(rs_pointSchema.getInt("STATUS"));
                        pointSchemaList.add(ps);
                    }

                    esr.setEventRulePointSchemaList(pointSchemaList);

                    eventSalesRuleList.add(esr);
                }

                em.setEventSalesRuleList(eventSalesRuleList);

                //--------------------------------------------------------------------------            
                ArrayList<EventNonSalesRule> nonSalesList = new ArrayList<>();

                String selectNonSales = "NS_RULE_ID,EVENT_ID,"
                        + "DESCRIPTION,TARGET,"
                        + "POINTS,SALE_TYPE,STATUS";

                ResultSet rs_nonSales = dbCon.search(con, "SELECT " + selectNonSales + " FROM SD_EVENT_NON_SALE_RULES "
                        + "WHERE EVENT_ID = " + event_id + "");

                while (rs_nonSales.next()) {
                    EventNonSalesRule ns = new EventNonSalesRule();
                    ns.setNonSalesRule(rs_nonSales.getInt("NS_RULE_ID"));;
                    ns.setEventId(rs_nonSales.getInt("EVENT_ID"));
                    ns.setDescription(rs_nonSales.getString("DESCRIPTION"));
                    ns.setTaget(rs_nonSales.getInt("TARGET"));
                    ns.setPoint(rs_nonSales.getInt("POINTS"));
                    ns.setSalesType(rs_nonSales.getInt("SALE_TYPE"));
                    ns.setStatus(rs_nonSales.getInt("STATUS"));
                    nonSalesList.add(ns);
                }

                em.setEventNonSalesRuleList(nonSalesList);
//------------------------------------------------------------------------------                

                ArrayList<EventAward> awardList = new ArrayList<>();

                String selectAward = "AWARD_ID,EVENT_ID,PLACE,STATUS";

                ResultSet rs_award = dbCon.search(con, "SELECT " + selectAward + " FROM SD_EVENT_AWARDS "
                        + "WHERE EVENT_ID = " + event_id + "");

                while (rs_award.next()) {
                    EventAward ea = new EventAward();
                    ea.setAwardId(rs_award.getInt("AWARD_ID"));;
                    ea.setEventid(rs_award.getInt("EVENT_ID"));
                    ea.setPalce(rs_award.getString("PLACE"));
                    ea.setStatus(rs_award.getInt("STATUS"));
                    awardList.add(ea);
                }

                em.setEvenAwardList(awardList);
                //--------------------------------------------------------------------------

                ArrayList<EventBisType> eventbisTypeList = new ArrayList<>();

                String selectBisType = "EVENT_PART_ID,EVENT_ID,"
                        + "BIS_TYPE_ID,USER_COUNT,STATUS";

                ResultSet rs_bisType = dbCon.search(con, "SELECT " + selectBisType + " FROM SD_EVENT_PARTICIPANTS "
                        + "WHERE EVENT_ID = " + event_id + "");

                logger.info("Get Data to Result Set.............");

                while (rs_bisType.next()) {
                    EventBisType ebt = new EventBisType();
                    ebt.setEventPartId(rs_bisType.getInt("EVENT_PART_ID"));;
                    ebt.setEventId(rs_bisType.getInt("EVENT_ID"));
                    ebt.setBisStruTypeId(rs_bisType.getInt("BIS_TYPE_ID"));
                    ebt.setBisStruTypeCount(rs_bisType.getInt("USER_COUNT"));
                    ebt.setStatus(rs_bisType.getInt("STATUS"));

                    ArrayList<EventBussinessList> eventBussList = new ArrayList<>();

                    String selectPartDtl = "EVENT_PART_ID,SEQ_NO,BIS_ID,USER_ID,STATUS";

                    ResultSet rs_prtDtl = dbCon.search(con, "SELECT " + selectPartDtl + " FROM SD_EVENT_PARTICIPANT_DTL "
                            + "WHERE EVENT_PART_ID = " + rs_bisType.getInt("EVENT_PART_ID") + "");

                    while (rs_prtDtl.next()) {
                        EventBussinessList ps = new EventBussinessList();
                        ps.setEventPartId(rs_prtDtl.getInt("EVENT_PART_ID"));;
                        ps.setSeqNo(rs_prtDtl.getInt("SEQ_NO"));
                        ps.setBisId(rs_prtDtl.getInt("BIS_ID"));
                        ps.setBisName(rs_prtDtl.getString("USER_ID"));
                        ps.setStatus(rs_prtDtl.getInt("STATUS"));
                        eventBussList.add(ps);
                    }

                    ebt.setEventBussinessList(eventBussList);
                    eventbisTypeList.add(ebt);
                }

                em.setEventBisTypeList(eventbisTypeList);
                eventList.add(em);
//--------------------------------------------------------------------------            
            }

        } catch (Exception ex) {
            logger.info("Error getEventMasterDetails......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getParticipentList(int event_id,
            String part_name,
            String location,
            String bis_type,
            String point,
            String position,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getParticipentList method Call..........");
        ArrayList<EventParticipant> paticipantList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            order = DBMapper.eventParticipant(order);

            String participantName = (part_name.equalsIgnoreCase("all") ? "NVL(BIS_NAME,'AA') = NVL(BIS_NAME,'AA') " : "UPPER(BIS_NAME) LIKE UPPER('%" + part_name + "%')");
            String Location = (location.equalsIgnoreCase("all") ? "NVL(ADDRESS,'AA') = NVL(ADDRESS,'AA') " : "UPPER(ADDRESS) LIKE UPPER('%" + location + "%')");
            String bisTypedesc = (bis_type.equalsIgnoreCase("all") ? "NVL(BIS_STRU_TYPE_DESC,'AA') = NVL(BIS_STRU_TYPE_DESC,'AA') " : "UPPER(BIS_STRU_TYPE_DESC) LIKE UPPER('%" + bis_type + "%')");
            String Point = (point.equalsIgnoreCase("all") ? "" : "AND POINT_VAL = " + point + "");
            String Position = (position.equalsIgnoreCase("all") ? "" : "AND RN = " + position + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY BIS_ID" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "BIS_ID,BIS_NAME,ADDRESS,"
                    + "BIS_STRU_TYPE_DESC,POINT_VAL,RN";

            String whereClous = "" + participantName + " "
                    + "AND " + Location + " "
                    + "AND " + bisTypedesc + " "
                    + " " + Point + " "
                    + " " + Position + " ";

            String tables = "(SELECT BIS_ID,BIS_NAME,ADDRESS,BIS_STRU_TYPE_DESC,NVL(TOTAL,0) POINT_VAL,ROW_NUMBER() OVER (ORDER BY NVL(TOTAL,0) DESC) RN "
                    + "FROM (SELECT BIS_ID,BIS_NAME,ADDRESS,BIS_STRU_TYPE_DESC,SUM(COL) TOTAL FROM (SELECT BIS_ID,BIS_NAME,ADDRESS,BIS_STRU_TYPE_DESC,(NVL(SALES_POINTS,0)+NVL(NON_SALES_POINTS,0)) COL "
                    + "FROM (SELECT F.BIS_ID,B.BIS_NAME,B.ADDRESS,T.BIS_STRU_TYPE_DESC,R.SALES_POINTS,R.NON_SALES_POINTS "
                    + "FROM SD_BUSINESS_STRUCTURES B,(SELECT BIS_ID,USER_ID FROM SD_EVENT_PARTICIPANT_DTL "
                    + "WHERE EVENT_PART_ID IN (SELECT EVENT_PART_ID "
                    + "FROM  SD_EVENT_TXN E,SD_EVENT_PARTICIPANTS P "
                    + "WHERE P.EVENT_ID = " + event_id + "))F, "
                    + "SD_BIS_STRU_TYPES T,SD_EVENT_TXN R "
                    + "WHERE F.BIS_ID = B.BIS_ID (+) "
                    + "AND B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID (+) "
                    + "AND F.USER_ID = R.USER_ID (+) AND R.STATUS = 1))GROUP BY BIS_ID,BIS_NAME,ADDRESS,BIS_STRU_TYPE_DESC))";

            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RoN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RoN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RoN ");

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RoN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RoN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RoN ");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventParticipant em = new EventParticipant();
                totalrecode = rs.getInt("CNT");
                em.setPaticipantName(rs.getString("BIS_NAME"));;
                em.setLocation(rs.getString("ADDRESS"));
                em.setPaticipantType(rs.getString("BIS_STRU_TYPE_DESC"));
                em.setAchivePoint(rs.getString("POINT_VAL"));
                em.setPosotion(rs.getString("RN"));
                paticipantList.add(em);
            }

        } catch (Exception ex) {
            logger.info("Error getParticipentList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(paticipantList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    
}
