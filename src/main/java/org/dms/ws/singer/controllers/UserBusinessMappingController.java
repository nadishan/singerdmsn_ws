/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.Customer;
import org.dms.ws.singer.entities.ErpCodes;
import org.dms.ws.singer.entities.UserBusinessMapping;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author dmd
 */
public class UserBusinessMappingController {

    public static ValidationWrapper addUserBusiness(ArrayList<UserBusinessMapping> userBusinessList,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addUserBusiness Method Call.............................");
        int result = 0;

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {
          //  int exist_cnt = 0;
            String erp_code = "";

            for (UserBusinessMapping userBusinessInfo : userBusinessList) {
                erp_code = userBusinessInfo.getCustomerNo()+",";
            }
            
            erp_code = erp_code.substring(0, (erp_code.length()-1));
            
            ResultSet rs_exist = dbCon.search(con, "SELECT BIS_ID, CUSTOMER_CODE, STATUS "
                    + "FROM SD_USER_BIS_MAPPING "
                    + "WHERE CUSTOMER_CODE IN ('" + erp_code + "')  ");

            ArrayList<ErpCodes> erpCList = new ArrayList<>();
            ArrayList<String> custList = new ArrayList<>();
            
            while (rs_exist.next()) {
                ErpCodes erpC = new ErpCodes();
                //exist_cnt = rs_exist.getInt("CNT");
                erpC.setBisId(rs_exist.getInt("BIS_ID"));
                erpC.setCusCode(rs_exist.getString("CUSTOMER_CODE"));
                erpC.setStatus(rs_exist.getInt("STATUS"));
                
                custList.add(rs_exist.getString("CUSTOMER_CODE"));
                erpCList.add(erpC);
            }

            System.out.println("CustomerList::: "+custList);
            
         //   if (exist_cnt == 1) {
           //     result = 2;
                
         //   }else{
                int max_seq = 0;
                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                        + "FROM SD_USER_BIS_MAPPING ");

                while (rs_max_dtl.next()) {
                    max_seq = rs_max_dtl.getInt("MAX");
                }
                // rs_max_dtl.close();

                String businessColumn = "SEQ_NO, BIS_ID, CUSTOMER_CODE, STATUS, USER_INSERTED, DATE_INSERTED";
                String businessValue = "?,?,?,1,?,SYSDATE";
                
              //  if (exist_cnt == 9) {
                    PreparedStatement ps_up_exist = dbCon.prepare(con, "UPDATE SD_USER_BIS_MAPPING "
                    + "SET STATUS = 1, BIS_ID = ?  "
                  //  + " WHERE BIS_ID = ?"
                    + " WHERE CUSTOMER_CODE = ?");
                    
                    for (UserBusinessMapping userBusinessInfo : userBusinessList) {
                        for(ErpCodes erp : erpCList){
                            if(erp.getCusCode().equals(userBusinessInfo.getCustomerNo())){
                                if(erp.getStatus()==9){
                                    System.out.println("Updating ERp code .............");
                                    ps_up_exist.setInt(1, userBusinessInfo.getBisId());
                                    ps_up_exist.setString(2, userBusinessInfo.getCustomerNo());
                                    ps_up_exist.executeUpdate();
                                }                                
                            }
                            
                        }                        
                    }
                    
                    
                    
             //   } else {
                    PreparedStatement ps_in_busniess = dbCon.prepare(con, "INSERT INTO SD_USER_BIS_MAPPING(" + businessColumn + ") "
                        + "VALUES(" + businessValue + ")");
                    
                     for (UserBusinessMapping userBusinessInfo : userBusinessList) {
                         if(!(custList.contains(userBusinessInfo.getCustomerNo()))){
                            max_seq = max_seq + 1;
                            ps_in_busniess.setInt(1, max_seq);
                            ps_in_busniess.setInt(2, userBusinessInfo.getBisId());
                            ps_in_busniess.setString(3, userBusinessInfo.getCustomerNo());
                            ps_in_busniess.setString(4, user_id);

                            ps_in_busniess.addBatch();
                         }
                        
                    }

                    ps_in_busniess.executeBatch();
             //   }
                

               

                logger.info("Distributors have been mapped to Bis Id ........");
                result = 1;
           // }
        } catch (Exception e) {
            logger.info("Error in Mapping Distributors and Bis Ids....." + e.toString());
            e.printStackTrace();
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("This Distributors have been mapped Successfully");
            logger.info("This Distributors have been mapped Successfully.........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("ERP Code in Already Assigne ");
            logger.info("ERP Code in Already Assigne............");
        }else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Mapping Distributors and Bis Ids");
            logger.info("Error in Mapping Distributors and Bis Ids..........");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getUserBusinessDetails(int bisId, String bisName, String customerCode, String order, String type, int start, int limit) {
        logger.info("getUserBusinessDetails Method Call.........");
        int totalrecode = 0;
        ArrayList<UserBusinessMapping> cusList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.userBusinessMapping(order);
            String BisId = ((bisId == 0) ? " " : " AND U.BIS_ID = '" + bisId + "' ");
            String BisName = (bisName.equalsIgnoreCase("all") ? " " : " AND UPPER(B.FIRST_NAME) LIKE UPPER('%" + bisName + "%')");
            String CustomerCode = (customerCode.equalsIgnoreCase("all") ? " " : " AND UPPER(U.CUSTOMER_CODE) LIKE UPPER('%" + customerCode + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY U.SEQ_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = " U.SEQ_NO, U.BIS_ID,  U.CUSTOMER_CODE, U.STATUS ";
            String tables = " SD_USER_BIS_MAPPING U ";
            String whereClause = " U.BIS_ID=U.BIS_ID AND STATUS = 1 "
                    //  + BisName
                    + CustomerCode
                    + BisId;

            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClause + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClause + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            //   System.out.println("qry: "+qry);
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                UserBusinessMapping userMap = new UserBusinessMapping();
                totalrecode = rs.getInt("CNT");
                userMap.setSeqNo(rs.getInt("SEQ_NO"));
                userMap.setBisId(rs.getInt("BIS_ID"));
                //   userMap.setBisName(rs.getString("FIRST_NAME"));
                userMap.setCustomerNo(rs.getString("CUSTOMER_CODE"));
                userMap.setStatus(rs.getInt("STATUS"));

                cusList.add(userMap);
            }

        } catch (Exception ex) {
            logger.info("Error in getCustomerDetals Method  :" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(cusList);
        logger.info("Ready to return Values.....");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper editUserBusiness(UserBusinessMapping userBusinessInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editUserBusiness Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            PreparedStatement ps_up_workorer = dbCon.prepare(con, "UPDATE SD_USER_BIS_MAPPING "
                    + "SET BIS_ID = ?, "
                    + "CUSTOMER_CODE = ?, "
                    + "STATUS = ?  "
                    + " WHERE SEQ_NO = ?");

            ps_up_workorer.setInt(1, userBusinessInfo.getBisId());
            ps_up_workorer.setString(2, userBusinessInfo.getCustomerNo());
            ps_up_workorer.setInt(3, userBusinessInfo.getStatus());
            ps_up_workorer.setInt(4, userBusinessInfo.getSeqNo());

            ps_up_workorer.executeUpdate();

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in editUserBusiness Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("User Business Mapping Update Successfully");
            logger.info("WorkOrder Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in User Business Mapping Update ");
            logger.info("Error in User Business Mapping Update ................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getBussinessStructure(int bisId, int bisType) {
        logger.info("getBussinessStructure method Call..........");
        ArrayList<BussinessStructure> bsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String BisId = "  ";
            if (bisId != 0) {
                BisId = "  AND B.BIS_ID=" + bisId + "  ";
            }

            String BisType = "";
            if (bisType != 0) {
                BisType = " AND (B.BIS_STRU_TYPE_ID=7 OR B.BIS_STRU_TYPE_ID=8) ";
            }

            ResultSet rs = dbCon.search(con, "SELECT B.BIS_ID,"
                    + "B.BIS_NAME,"
                    + "B.BIS_DESC,"
                    + "B.PARENT_BIS_ID,"
                    + "B.CATEGORY_1,"
                    + "B.CATEGORY_2,"
                    + "B.BIS_STRU_TYPE_ID,"
                    + "B.LONGITUDE,"
                    + "B.LATITUDE,"
                    + "B.ADDRESS,"
                    + "B.CONTACT_NO,"
                    + "B.SHOW_IN_MAP_FLAG,"
                    + "B.STATUS,"
                    + "(SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES U WHERE U.BIS_ID = B.BIS_ID ) CNT,"
                    + "ERP_CODE,PO_TITLE,CHQ_TITLE,ESSD_ACC_NO,ESSD_PCT,INVOICE_NO "
                    + "FROM SD_BUSINESS_STRUCTURES B "
                    + "WHERE B.STATUS=B.STATUS "
                    + BisId
                    + BisType
                    + "ORDER BY B.BIS_ID ASC");

//                        ResultSet rs = dbCon.search(con,"SELECT              B.BIS_ID BISID,B.BIS_NAME BISNAME, B.BIS_DESC BISDESC, B.PARENT_BIS_ID PARENTBISID,(SELECT COUNT(*) FROM SD_USERS U WHERE U.BIS_ID=B.BIS_ID) CNT"
//                    + "                  FROM                SD_BUSINESS_STRUCTURES B "
//                    + "                  START WITH          B.BIS_ID = B.BIS_ID "
//                    + "                  CONNECT BY PRIOR    B.PARENT_BIS_ID = B.BIS_ID "
//                    + "UNION                                                   "
//                    + "                  SELECT              B.BIS_ID BISID,B.BIS_NAME BISNAME, B.BIS_DESC BISDESC, B.PARENT_BIS_ID PARENTBISID,(SELECT COUNT(*) FROM SD_USERS U WHERE U.BIS_ID=B.BIS_ID) CNT"
//                    + "                  FROM                SD_BUSINESS_STRUCTURES B "
//                    + "                  START WITH          B.BIS_ID = B.BIS_ID "
//                    + "                  CONNECT BY PRIOR    B.BIS_ID =B.PARENT_BIS_ID");
            while (rs.next()) {
                BussinessStructure bs = new BussinessStructure();
                bs.setBisId(rs.getInt("BIS_ID"));;
                bs.setBisName(rs.getString("BIS_NAME"));
                bs.setBisDesc(rs.getString("BIS_DESC"));
                bs.setBisPrtnId(rs.getInt("PARENT_BIS_ID"));
                bs.setCategory1(rs.getInt("CATEGORY_1"));
                bs.setCategory2(rs.getInt("CATEGORY_2"));
                bs.setStructType(rs.getInt("BIS_STRU_TYPE_ID"));
                bs.setLogitude(rs.getString("LONGITUDE"));
                bs.setLatitude(rs.getString("LATITUDE"));
                bs.setAddress(rs.getString("ADDRESS"));
                bs.setTeleNumber(rs.getString("CONTACT_NO"));
                bs.setMapFlag(rs.getInt("SHOW_IN_MAP_FLAG"));
                bs.setStatus(rs.getInt("STATUS"));
                bs.setBisUserCount(rs.getInt("CNT"));
                bs.setErpCode(rs.getString("ERP_CODE"));
                bs.setPoTitle(rs.getString("PO_TITLE"));
                bs.setCheqTitle(rs.getString("CHQ_TITLE"));
                bs.setEssdAcntNo(rs.getString("ESSD_ACC_NO"));
                bs.setEddsPct(rs.getString("ESSD_PCT"));
                bs.setInvoiceNo(rs.getString("INVOICE_NO"));
                bsList.add(bs);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessStruture......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setData(bsList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static ValidationWrapper checkCustometCode(String cusCode){
        int flag = 1;
        String msg = "";
        try{
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();
            
            String qry =" SELECT M.BIS_ID, B.BIS_NAME "
                    + " FROM SD_USER_BIS_MAPPING M, SD_BUSINESS_STRUCTURES B    "
                    + " WHERE B.BIS_ID=M.BIS_ID "
                    + " AND M.STATUS = 1 "
                    + " AND   M.CUSTOMER_CODE='"+cusCode+"'   ";
            
             ResultSet rs = dbCon.search(con, qry);
             
             if(rs.next()){
                 flag=2;
                 msg = "This Customer code is already used by "+rs.getString("BIS_ID")+" - "+rs.getString("BIS_NAME");
             }
             
            dbCon.ConectionClose(con);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        ValidationWrapper vw = new ValidationWrapper();
        vw.setReturnFlag(String.valueOf(flag));
        vw.setReturnMsg(msg);
        return vw;
    }
}
