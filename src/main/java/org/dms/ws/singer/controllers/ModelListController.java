/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Category;
import org.dms.ws.singer.entities.ModelList;
import org.dms.ws.singer.entities.Part;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class ModelListController {

    public static MessageWrapper getModelDetails(int modelno,
            String modeldesc,
            String partprdcode,
            String partprddesc,
            String partfamily,
            String partfamilydesc,
            String commoditygroup1,
            String commoditygroup2,
            String wrnscheme,
            String repcat,
            double sellprice,
            double costprice,
            double dealermargin,
            double distributormargin,
            int status,
            String alt_part,
            String erp_part_code,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getModelDetails Mehtod Call.......");
        int totalrecode = 0;
        ArrayList<ModelList> modelList = new ArrayList<>();
        try {

            order = DBMapper.modle(order);

            String modelNo = (modelno == 0 ? "" : " AND m.MODEL_NO = " + modelno + " ");
            String modelDesc = (modeldesc.equalsIgnoreCase("all") ? "NVL(m.MODEL_DESCRIPTION,'AA') = NVL(m.MODEL_DESCRIPTION,'AA')" : " UPPER(m.MODEL_DESCRIPTION) LIKE UPPER('%" + modeldesc + "%')");
            String partPrdCode = (partprdcode.equalsIgnoreCase("all") ? " NVL(m.PART_PRD_CODE,'AA') = NVL(m.PART_PRD_CODE,'AA')" : " UPPER(m.PART_PRD_CODE) LIKE UPPER('%" + partprdcode + "%')");
            String partPrdDesc = (partprddesc.equalsIgnoreCase("all") ? " NVL(m.PART_PRD_CODE_DESC,'AA') = NVL(m.PART_PRD_CODE_DESC,'AA')" : " UPPER(m.PART_PRD_CODE_DESC) LIKE UPPER('%" + partprddesc + "%')");
            String partFamily = (partfamily.equalsIgnoreCase("all") ? " NVL(m.PART_PRD_FAMILY,'AA') = NVL(m.PART_PRD_FAMILY,'AA')" : " UPPER(m.PART_PRD_FAMILY) LIKE UPPER('%" + partfamily + "%')");
            String partFamilyDesc = (partfamilydesc.equalsIgnoreCase("all") ? " NVL(m.PART_PRD_FAMILY_DESC,'AA') = NVL(m.PART_PRD_FAMILY_DESC,'AA')" : " UPPER(m.PART_PRD_FAMILY_DESC) LIKE UPPER('%" + partfamilydesc + "%')");
            String commodityGroup1 = (commoditygroup1.equalsIgnoreCase("all") ? " NVL(m.COMMODITY_GROUP_1,'AA') = NVL(m.COMMODITY_GROUP_1,'AA')" : "  UPPER(m.COMMODITY_GROUP_1) LIKE UPPER('%" + commoditygroup1 + "%')");
            String commodityGroup2 = (commoditygroup2.equalsIgnoreCase("all") ? " NVL(m.COMMODITY_GROUP_2,'AA') = NVL(m.COMMODITY_GROUP_2,'AA')" : "  UPPER(m.COMMODITY_GROUP_2) LIKE UPPER('%" + commoditygroup2 + "%')");
            String wrnScheme = (wrnscheme.equalsIgnoreCase("all") ? " NVL(w.SCHEME_NAME,'AA') = NVL(w.SCHEME_NAME,'AA')" : " UPPER(w.SCHEME_NAME) LIKE UPPER('%" + wrnscheme + "%')");
            String repCat = (repcat.equalsIgnoreCase("all") ? " NVL(r.REP_CAT_DESC,'AA') = NVL(r.REP_CAT_DESC,'AA')" : "  UPPER(r.REP_CAT_DESC) LIKE UPPER('%" + repcat + "%')");
            String partSellPrice = (sellprice == 0 ? "" : " AND m.SELLING_PRICE = " + sellprice + "");
            String costPrice = (costprice == 0 ? "" : " AND m.COST_PRICE = " + costprice + "");
            String dealerMargin = (dealermargin == 0 ? "" : " AND m.DEALER_MARGIN = " + dealermargin + " ");
            String distributorMargin = (distributormargin == 0 ? "" : " AND m.DISTRIBUTOR_MARGIN = " + distributormargin + " ");
            String modelStatus = (status == 0 ? "" : "AND m.STATUS = " + status + "");
            //String altPartExist = (alt_part == 0 ? "" : "AND m.ALT_PARTS_EXIST = " + alt_part + "");
            String altPartExist = "";
            if(!alt_part.equalsIgnoreCase("all")){
                if(alt_part.equalsIgnoreCase("no")){
                    altPartExist = " AND NVL(m.ALT_PARTS_EXIST,0) = 0   ";
                }else if(alt_part.equalsIgnoreCase("yes")){
                    altPartExist = " AND m.ALT_PARTS_EXIST = 1 ";
                }
            }
            
            String erpPart = (erp_part_code.equalsIgnoreCase("all") ? "" : " AND UPPER(m.ERP_PART_NO) LIKE '%" + erp_part_code.toUpperCase() + "%' ");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY MODEL_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColnum = "m.MODEL_NO, m.MODEL_DESCRIPTION, m.PART_PRD_CODE, m.PART_PRD_CODE_DESC, m.PART_PRD_FAMILY, "
                    + "m.PART_PRD_FAMILY_DESC, m.COMMODITY_GROUP_1, m.COMMODITY_GROUP_2, m.WRN_SCHEME_ID, w.SCHEME_NAME, "
                    + "m.REP_CAT_ID, r.REP_CAT_DESC, m.SELLING_PRICE, m.COST_PRICE, m.DEALER_MARGIN, m.DISTRIBUTOR_MARGIN, m.STATUS,m.ERP_PART_NO,m.ALT_PARTS_EXIST ";

            String whereClous = "m.WRN_SCHEME_ID = w.SCHEME_ID (+) "
                    + "AND m.REP_CAT_ID = r.REP_CAT_ID (+) "
                    //    + "AND w.STATUS = 1 "
                    + "AND " + modelDesc + " "
                    + "AND " + partPrdCode + " "
                    + "AND " + partPrdDesc + " "
                    + "AND " + partFamily + " "
                    + "AND " + partFamilyDesc + " "
                    + "AND " + commodityGroup1 + " "
                    + "AND " + commodityGroup2 + " "
                    + "AND " + wrnScheme + " "
                    + "AND " + repCat + " "
                    + "" + partSellPrice + " "
                    + "" + costPrice + ""
                    + "" + modelNo + " "
                    + "" + dealerMargin + " "
                    + "" + distributorMargin + ""
                    + "" + modelStatus + " "
                    + "" + altPartExist + " "
                    + "  "+erpPart+"   ";

            String selectQry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MODEL_LISTS m, SD_WARRANTY_SCHEMES w,SD_REPAIR_CATEGORIES r WHERE  " + whereClous + ") CNT,"
                    + "" + selectColnum + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_MODEL_LISTS m, SD_WARRANTY_SCHEMES w,SD_REPAIR_CATEGORIES r "
                    + "WHERE  " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            //System.out.println("XXXXXXXX");
          //  System.out.println(selectQry);
           // System.out.println("XXXXXXXX");

            ResultSet rs = dbCon.search(con, selectQry);

            logger.info("Get Data to ResultSet............");

            while (rs.next()) {
                ModelList mdl = new ModelList();
                totalrecode = rs.getInt("CNT");
                mdl.setModel_No(rs.getInt("MODEL_NO"));
                mdl.setModel_Description(rs.getString("MODEL_DESCRIPTION"));
                mdl.setPart_Prd_Code(rs.getString("PART_PRD_CODE"));
                mdl.setArt_Prd_Code_Desc(rs.getString("PART_PRD_CODE_DESC"));
                mdl.setPart_Prd_Family(rs.getString("PART_PRD_FAMILY"));
                mdl.setPart_Prd_Family_Desc(rs.getString("PART_PRD_FAMILY_DESC"));
                mdl.setCommodity_Group_1(rs.getString("COMMODITY_GROUP_1"));
                mdl.setCommodity_Group_2(rs.getString("COMMODITY_GROUP_2"));
                mdl.setWrn_Scheme_id(rs.getInt("WRN_SCHEME_ID"));
                mdl.setWrn_Scheme(rs.getString("SCHEME_NAME"));
                mdl.setErp_part(rs.getString("ERP_PART_NO"));
                mdl.setRep_Category_id(rs.getInt("REP_CAT_ID"));
                mdl.setRep_Category(rs.getString("REP_CAT_DESC"));
                mdl.setSelling_Price(rs.getDouble("SELLING_PRICE"));
                mdl.setCost_Price(rs.getDouble("COST_PRICE"));
                mdl.setDealer_Margin(rs.getDouble("DEALER_MARGIN"));
                mdl.setDistributor_Margin(rs.getDouble("DISTRIBUTOR_MARGIN"));
                mdl.setStatus(rs.getInt("STATUS"));
                mdl.setAltPartsExist(rs.getInt("ALT_PARTS_EXIST"));
                if (rs.getInt("ALT_PARTS_EXIST") == 1) {
                    mdl.setAltPartsExistDesc("Yes");
                } else {
                    mdl.setAltPartsExistDesc("No");
                }

                modelList.add(mdl);
            }

        } catch (Exception ex) {
            logger.info("Error in getModelDetails mehtod Error... :" + ex.getMessage());
        }
        logger.info("Ready Return Values.............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(modelList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper editModel(ModelList model,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editModel Method Call.....");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            try {

                dbCon.save(con, "UPDATE SD_MODEL_LISTS "
                        + "SET      DEALER_MARGIN = '" + model.getDealer_Margin() + "',"
                        + "         DISTRIBUTOR_MARGIN = '" + model.getDistributor_Margin() + "',"
                        + "         WRN_SCHEME_ID = '" + model.getWrn_Scheme_id() + "',"
                        + "         REP_CAT_ID = '" + model.getRep_Category_id() + "',"
                        + "         USER_MODIFIED = '" + user_id + "',"
                        + "         DATE_MODIFIED = SYSDATE "
                        + "WHERE MODEL_NO = " + model.getModel_No() + " ");

                result = 1;
            } catch (Exception e) {
                logger.info("Error in Model Update Query..." + e.getMessage());
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error in editModel Method  :" + ex.getMessage());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Model Update Successfully");
            logger.info("Model Update Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Model Update");
            logger.info("Error in Model Update.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

}
