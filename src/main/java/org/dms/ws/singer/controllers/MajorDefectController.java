/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.MajorDefect;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class MajorDefectController {

    public static MessageWrapper getMajorDefects(int mjrdefectcode,
            String mjrdefectdesc,
            int mjrdefectstatus,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getMajorDefects Method Call.......");
        int totalrecode = 0;
        ArrayList<MajorDefect> mdList = new ArrayList<>();

        try {
            order = DBMapper.majorDefectCode(order);

            String mjrDefectCode = (mjrdefectcode == 0 ? "" : "AND MAJ_CODE = " + mjrdefectcode + "");
            String mjrDefectDesc = (mjrdefectdesc.equalsIgnoreCase("all") ? "NVL(MAJ_CODE_DESC,'AA') = NVL(MAJ_CODE_DESC,'AA')" : "UPPER(MAJ_CODE_DESC) LIKE UPPER('%" + mjrdefectdesc + "%')");
            String mjrDefectStatus = (mjrdefectstatus == 0 ? "" : "AND STATUS = " + mjrdefectstatus + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY MAJ_CODE DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "MAJ_CODE,MAJ_CODE_DESC,STATUS";

            String whereClous = "" + mjrDefectDesc + ""
                    + "" + mjrDefectCode + ""
                    + "" + mjrDefectStatus + "";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_DEFECT_CODES WHERE  " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM SD_DEFECT_CODES "
                    + "WHERE " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " "
                    + "AND " + (start + limit) + " ORDER BY RN");
            logger.info("Get Data to ResultSet");
            while (rs.next()) {
                MajorDefect md = new MajorDefect();
                totalrecode = rs.getInt("CNT");
                md.setMjrDefectCode(rs.getInt("MAJ_CODE"));
                md.setMjrDefectDesc(rs.getString("MAJ_CODE_DESC"));
                md.setMjrDefectStatus(rs.getInt("STATUS"));
                mdList.add(md);
            }

        } catch (Exception ex) {
            logger.info("Error getMajorDefects Method  :" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        logger.info("Redy to return Values...........");
        mw.setData(mdList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addMajorDefect(MajorDefect md,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addMajorDefect method Call........");
        int result = 0;
        int defect_cnt = 0;
        int max_defect = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEFECT_CODES "
                    + "WHERE UPPER(MAJ_CODE_DESC) = UPPER('" + md.getMjrDefectDesc() + "')");

            while (rs_cnt.next()) {
                defect_cnt = rs_cnt.getInt("CNT");
            }

            if (defect_cnt > 0) {
                result = 2;
            } else {

                ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(MAJ_CODE),0) MAX FROM SD_DEFECT_CODES");

                if (rs_max.next()) {
                    max_defect = rs_max.getInt("MAX");
                }

                String column = "MAJ_CODE,MAJ_CODE_DESC,STATUS,USER_INSERTED,DATE_INSERTED";

                String values = "?,UPPER(?),?,?,SYSDATE";

                PreparedStatement ps_in_defect = dbCon.prepare(con, "INSERT INTO SD_DEFECT_CODES(" + column + ") VALUES(" + values + ")");

                try {
                    max_defect = max_defect + 1;

                    ps_in_defect.setInt(1, max_defect);
                    ps_in_defect.setString(2, md.getMjrDefectDesc());
                    ps_in_defect.setInt(3, md.getMjrDefectStatus());
                    ps_in_defect.setString(4, user_id);
                    ps_in_defect.executeUpdate();
                    result = 1;

                } catch (Exception ex) {
                    logger.info("Error in Major DefectCode Insert.....");
                    result = 9;
                }
            }

        } catch (Exception ex) {
            logger.info("Error addMajorDefect Method  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Defect Code Insert Successfully");
            logger.info("Defect Code Insert Succesfully............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Defect Description Already Exist");
            logger.info("Defect Description Already Exist................");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Defect Code Insert");
            logger.info("Error in Defect Code Insert................");
        }
        logger.info("Ready to Returns Values......");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editMajorDefect(MajorDefect md,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editMajorDefect Method Call......");
        int result = 0;
        int defect_cnt = 0;
        int WO_defect_cnt = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            if (md.getMjrDefectStatus() == 2) {
                ResultSet rs_wo_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_WORK_ORDER_DEFECTS "
                        + "WHERE WRK_ORD_MTN_ID IN (SELECT WRK_ORD_MTN_ID "
                        + "FROM SD_WORK_ORDER_MAINTAIN "
                        + "WHERE WORK_ORDER_NO IN (SELECT WORK_ORDER_NO "
                        + "FROM SD_WORK_ORDERS "
                        + "WHERE STATUS IN (3,7,6))) "
                        + "AND MAJ_CODE = " + md.getMjrDefectCode() + "");

                while (rs_wo_cnt.next()) {
                    WO_defect_cnt = rs_wo_cnt.getInt("CNT");
                }
            }

            if (WO_defect_cnt > 0) {

                result = 3;
            } else {
                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_DEFECT_CODES "
                        + "WHERE UPPER(MAJ_CODE_DESC) = UPPER('" + md.getMjrDefectDesc() + "') "
                        + "AND MAJ_CODE != " + md.getMjrDefectCode() + " ");

                while (rs_cnt.next()) {
                    defect_cnt = rs_cnt.getInt("CNT");
                }

                if (defect_cnt == 0) {

                    PreparedStatement ps_up_defect = dbCon.prepare(con, "UPDATE SD_DEFECT_CODES "
                            + "SET MAJ_CODE_DESC = UPPER(?) ,"
                            + "STATUS = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE MAJ_CODE = ? ");

                    ps_up_defect.setString(1, md.getMjrDefectDesc());
                    ps_up_defect.setInt(2, md.getMjrDefectStatus());
                    ps_up_defect.setString(3, user_id);
                    ps_up_defect.setInt(4, md.getMjrDefectCode());
                    ps_up_defect.executeUpdate();
                    result = 1;
                } else {
                    result = 2;
                }
            }

        } catch (Exception ex) {
            logger.info("Error in editMajorDefect Method   :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Major Code Update Successfully");
            logger.info("Major Code Update Successfully...........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Defect Description Already Exist");
            logger.info("Defect Description Already Exist...........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This Defect have a active Workorder");
            logger.info("This Defect have a active Workorder...........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Major Code Update");
            logger.info("Error in Major Code Update.........");
        }
        logger.info("Redy to Return Values...........");
        dbCon.ConectionClose(con);
        return vw;
    }

}
