/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Department;

/**
 *
 * @author SDU
 */
public class DepartmentController {

    public static MessageWrapper getDepartmentList() {

        logger.info("getDepartmentList Method Call....................");
        ArrayList<Department> depatList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con,"SELECT (SELECT COUNT(*) FROM SD_BIS_STRU_TYPES) CNT,"
                    + "BIS_STRU_TYPE_ID,"
                    + "BIS_STRU_TYPE_DESC,"
                    + "ENTRY_MODE,"
                    + "STATUS "
                    + "FROM SD_BIS_STRU_TYPES");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                Department ut = new Department();
                totalrecode = rs.getInt("CNT");
                ut.setDepartmentType(rs.getInt("BIS_STRU_TYPE_ID"));
                ut.setDepatmentDesc(rs.getString("BIS_STRU_TYPE_DESC"));
                ut.setEntryCode(rs.getInt("ENTRY_MODE"));
                ut.setStatus(rs.getInt("STATUS"));

                depatList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getUserTypeList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(depatList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

}
