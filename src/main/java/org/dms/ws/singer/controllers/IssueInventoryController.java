/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.AcceptInvenroryImei;
import org.dms.ws.singer.entities.AcceptInventory;
import org.dms.ws.singer.entities.DeviceIssue;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.entities.DeviceIssueOffline;
import org.dms.ws.singer.entities.DeviceIssueOfflineList;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.InqModel;
import org.dms.ws.singer.entities.ModelList;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class IssueInventoryController {

    public static MessageWrapper getInventoryIssueList(int issue_no,
            int distributer_id,
            int dsr_id,
            String issue_date,
            int status,
            String fromdate,
            String todate,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getInventoryIssueList Method Call...........");

        ArrayList<DeviceIssue> deviceIssueList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int totalrecode = 0;
        try {

            order = DBMapper.deviceIssue(order);

            //////////////////////////////Before/////////////////////////////
//            String ItemNo = ((issue_no == 0) ? "" : "AND D.ISSUE_NO = '" + issue_no + "' ");
//            String distributerId = ((distributer_id == 0) ? "" : "AND D.DISTRIBUTER_ID = " + distributer_id + " ");
//            String dsrId = ((dsr_id == 0) ? "" : "AND D.DSR_ID = " + dsr_id + " ");
//            String IssueDate = (issue_date.equalsIgnoreCase("all") ? "NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(D.ISSUE_DATE,'YYYY/MM/DD') = TO_DATE('%" + issue_date + "%','YYYY/MM/DD')");
//            String Status = ((status == 0) ? "" : "AND D.STATUS = " + status + "");
//            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(D.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(D.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";
//            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.ISSUE_NO DESC" : "ORDER BY " + order + "   " + type + "");
            ///////////////////////////////////Changed///////////////////////////////////
            String ItemNo = ((issue_no == 0) ? "" : "AND D.ISSUE_NO = '" + issue_no + "' ");
            String distributerId = ((distributer_id == 0) ? "" : "AND D.DISTRIBUTER_ID = " + distributer_id + " ");
            String dsrId = ((dsr_id == 0) ? "" : "AND D.DSR_ID = " + dsr_id + " ");
            String IssueDate = (issue_date.equalsIgnoreCase("all") ? "NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(D.ISSUE_DATE,'YYYY/MM/DD') = TO_DATE('%" + issue_date + "%','YYYY/MM/DD')");
            String Status = ((status == 0) ? "" : "AND D.STATUS = " + status + "");
            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(D.ISSUE_DATE,'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(D.ISSUE_DATE,'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.ISSUE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "D.ISSUE_NO,D.DISTRIBUTER_ID,"
                    + "D.DSR_ID,TO_CHAR(D.ISSUE_DATE,'YYYY/MM/DD') ISSUE_DATE,"
                    + "D.STATUS,B.BIS_NAME,"
                    + "B1.BIS_ID   DISTRIBUTOR_CODE, "
                    + "  B1.BIS_NAME DISTRIBUTOR_NAME,     "
                    + "  B1.ADDRESS DISTRIBUTOR_ADDRESS,  "
                    + "  B1.CONTACT_NO DISTRIBUTOR_PHONE, "
                    + "  B.BIS_ID   SUBDEALER_CODE,  "
                    + "  B.BIS_NAME SUBDEALER_NAME,  "
                    + "  B.ADDRESS SUBDEALER_ADDRESS,  "
                    + "  B.CONTACT_NO SUBDEALER_PHONE,   "
                    + " D.DATE_INSERTED,   "
                    + " D.MANUAL_RECEIPT_NO ";

            String whereClous = "D.DSR_ID = B.BIS_ID (+) "
                    + " AND D.DISTRIBUTER_ID=B1.BIS_ID (+)  "
                    + "AND  " + IssueDate + " "
                    + " " + ItemNo + " "
                    + " " + dsrId + " "
                    + " " + distributerId + " "
                    + " " + Status + "  "
                    + " " + Range + " "
                    + " AND D.ISSUE_NO IN (SELECT DT.ISSUE_NO FROM SD_DEVICE_ISSUE_DTL DT, SD_ACCEPT_INVENTORY_DTL A WHERE DT.IMEI_NO=A.IMEI_NO) "; ////New

            String table = " SD_DEVICE_ISSUE D,SD_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES B1 ";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + table + " WHERE " + whereClous + ") CNT,"
//                    + "" + selectColumn + ",ROW_NUMBER() "
//                    + "OVER (" + Order + ") RN "
//                    + "FROM " + table + " "
//                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ");
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + table + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                DeviceIssue di = new DeviceIssue();
                totalrecode = rs.getInt("CNT");
                di.setIssueNo(rs.getInt("ISSUE_NO"));
                di.setDistributerID(rs.getInt("DISTRIBUTER_ID"));
                di.setdSRId(rs.getInt("DSR_ID"));
                di.setIssueDate(rs.getString("ISSUE_DATE"));
                di.setStatus(rs.getInt("STATUS"));
                di.setToName(rs.getString("BIS_NAME"));

                if (rs.getInt("STATUS") == 1) {

                    di.setStatusMsg("Pending");

                } else if (rs.getInt("STATUS") == 2) {

                    di.setStatusMsg("Complete");

                } else if (rs.getInt("STATUS") == 3) {

                    di.setStatusMsg("Accepted");
                } else {

                    di.setStatusMsg("None");
                }

                di.setDistributorCode(rs.getString("DISTRIBUTOR_CODE"));
                di.setDistributorName(rs.getString("DISTRIBUTOR_NAME"));
                di.setDistributorAddress(rs.getString("DISTRIBUTOR_ADDRESS"));
                di.setDistributorTelephone(rs.getString("DISTRIBUTOR_PHONE"));
                di.setSubdealerCode(rs.getString("SUBDEALER_CODE"));
                di.setSubdealerName(rs.getString("SUBDEALER_NAME"));
                di.setSubdealerAddress(rs.getString("SUBDEALER_ADDRESS"));
                di.setSubdealerTelephone(rs.getString("SUBDEALER_PHONE"));

                di.setDateInserted(rs.getString("DATE_INSERTED"));

                String manReceiptNo = "-";
                if (rs.getString("MANUAL_RECEIPT_NO") != null) {
                    manReceiptNo = rs.getString("MANUAL_RECEIPT_NO");
                }
                di.setManualReceiptNo(manReceiptNo);

                deviceIssueList.add(di);
            }

        } catch (Exception ex) {
            logger.info("Error Retriview Device Issue......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceIssueList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getInventoryIssueDetail(int issue_no, int dsr_id, int status) {
        logger.info("getInventoryIssueDetail Method Call...........");
        ArrayList<DeviceIssueIme> deviceImeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int issue_vali = 0;
        try {

            String statusW = "";
            if (status != 0) {
                statusW = " AND I.STATUS=" + status + "   ";
            } else {
                statusW = " AND I.STATUS!=1   ";
            }

            /**
             * get business structure type
             */
            String sqlForType = " SELECT BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID = '" + dsr_id + "'";

            ResultSet rsForType = dbCon.search(con, sqlForType);

            int rsTypeId = 0;

            while (rsForType.next()) {
                rsTypeId = rsForType.getInt("BIS_STRU_TYPE_ID");
            }

//            ResultSet rs_issue_vali = dbCon.search(con, "SELECT COUNT(*) CNT "
//                    + "FROM SD_DEVICE_ISSUE "
//                    + "WHERE DSR_ID = " + dsr_id + " "
//                    + "AND ISSUE_NO = " + issue_no + " "
//                    + "AND STATUS = 3");
//            while (rs_issue_vali.next()) {
//                issue_vali = rs_issue_vali.getInt("CNT");
//            }
//
//            if (issue_vali > 0) {
            ResultSet rs_ime = null;

            if (rsTypeId == 1) {
                String sqlForGetDetailsForDSR = "SELECT LI.*,  ML.SELLING_PRICE*0.9 PRICE, ML.MODEL_DESCRIPTION, ML.ERP_PART_NO, "
                        + "(SELECT BS.BIS_NAME FROM SD_BUSINESS_STRUCTURES BS WHERE BS.BIS_ID = LI.DISTRIBUTOR_CODE ) DISTRIBUTOR_NAME, "
                        + " (SELECT BS.ADDRESS FROM SD_BUSINESS_STRUCTURES BS WHERE BS.BIS_ID = LI.DISTRIBUTOR_CODE ) DISTRIBUTOR_ADDRESS, "
                        + "  (SELECT BS.CONTACT_NO FROM SD_BUSINESS_STRUCTURES BS WHERE BS.BIS_ID = LI.DISTRIBUTOR_CODE ) DISTRIBUTOR_PHONE, "
                        + "  (SELECT BS.BIS_NAME FROM SD_BUSINESS_STRUCTURES BS WHERE BS.BIS_ID = LI.SUBDEALER_CODE ) SUBDEALER_NAME, "
                        + "   (SELECT BS.ADDRESS FROM SD_BUSINESS_STRUCTURES BS WHERE BS.BIS_ID = LI.SUBDEALER_CODE ) SUBDEALER_ADDRESS, "
                        + "  (SELECT BS.CONTACT_NO FROM SD_BUSINESS_STRUCTURES BS WHERE BS.BIS_ID = LI.SUBDEALER_CODE ) SUBDEALER_PHONE "
                        + "FROM ( "
                        + "SELECT DID.SEQ_NO, DID.ISSUE_NO, DID.IMEI_NO, DID.MODEL_NO, DID.STATUS, DI.DISTRIBUTER_ID DISTRIBUTOR_CODE, DI.DSR_ID SUBDEALER_CODE "
                        + "FROM SD_DEVICE_ISSUE DI, SD_DEVICE_ISSUE_DTL DID "
                        + "WHERE DI.ISSUE_NO = DID.ISSUE_NO "
                        + "AND DI.DSR_ID = '" + dsr_id + "' "
                        + "AND DI.STATUS = '3' "
                        + "AND DID.ISSUE_NO= '" + issue_no + "' ) LI, SD_MODEL_LISTS ML "
                        + "WHERE ML.MODEL_NO = LI.MODEL_NO ";
                
                System.out.println("query for price "+sqlForGetDetailsForDSR);
                
                rs_ime = dbCon.search(con, sqlForGetDetailsForDSR);
            } else {
                
                
                System.out.println("query for non dealer.......");

                String whereClous = "D.MODEL_NO = M.MODEL_NO (+)    "
                        + " AND   I.ISSUE_NO = D.ISSUE_NO   "
                        + " AND   I.DISTRIBUTER_ID=BD.BIS_ID    "
                        + " AND   I.DSR_ID = BS.BIS_ID  "
                        + " AND D.ISSUE_NO = '" + issue_no + "' "
                        + " AND D.IMEI_NO IN ( SELECT IMEI_NO FROM SD_ACCEPT_INVENTORY_DTL) " // NEW
                        + " AND D.IMEI_NO IN (SELECT IMEI_NO FROM SD_DEVICE_MASTER M WHERE BIS_ID=" + dsr_id + ") "
                        + " " + statusW + "    ";

                String ImeColumn = "D.SEQ_NO,"
                        + "D.ISSUE_NO,"
                        + "D.IMEI_NO,"
                        + "D.MODEL_NO,"
                        + "D.STATUS,"
                        + "M.MODEL_DESCRIPTION,"
                        + "BD.BIS_ID   DISTRIBUTOR_CODE,"
                        + "BD.BIS_NAME DISTRIBUTOR_NAME,"
                        + "BD.ADDRESS DISTRIBUTOR_ADDRESS,"
                        + "BD.CONTACT_NO DISTRIBUTOR_PHONE,"
                        + "BS.BIS_ID   SUBDEALER_CODE,"
                        + "BS.BIS_NAME SUBDEALER_NAME,"
                        + "BS.ADDRESS SUBDEALER_ADDRESS,"
                        + "BS.CONTACT_NO SUBDEALER_PHONE,"
                        + "D.PRICE,  "
                        + "M.ERP_PART_NO    ";

                rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM SD_DEVICE_ISSUE I, SD_DEVICE_ISSUE_DTL D,SD_MODEL_LISTS M, SD_BUSINESS_STRUCTURES BD, SD_BUSINESS_STRUCTURES BS WHERE " + whereClous + " ORDER BY D.MODEL_NO ASC ");
            }

            logger.info("Get Data to ResultSet...........");

            while (rs_ime.next()) {
                DeviceIssueIme dim = new DeviceIssueIme();
                dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                dim.setIssueNo(rs_ime.getInt("ISSUE_NO"));
                dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                dim.setStatus(rs_ime.getInt("STATUS"));
                dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));

                dim.setDistributorCode(rs_ime.getString("DISTRIBUTOR_CODE"));
                dim.setDistributorName(rs_ime.getString("DISTRIBUTOR_NAME"));
                dim.setDistributorAddress(rs_ime.getString("DISTRIBUTOR_ADDRESS"));
                dim.setDistributorTelephone(rs_ime.getString("DISTRIBUTOR_PHONE"));
                dim.setSubdealerCode(rs_ime.getString("SUBDEALER_CODE"));
                dim.setSubdealerName(rs_ime.getString("SUBDEALER_NAME"));
                dim.setSubdealerAddress(rs_ime.getString("SUBDEALER_ADDRESS"));
                dim.setSubdealerTelephone(rs_ime.getString("SUBDEALER_PHONE"));
                dim.setSalesPrice(rs_ime.getDouble("PRICE"));
                dim.setErpModel(rs_ime.getString("ERP_PART_NO"));

                deviceImeList.add(dim);
            }
            // }

        } catch (Exception ex) {
            logger.info("Error Retriview Device Issue......:" + ex.toString());
            ex.printStackTrace();
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceImeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceImeList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getInventoryIssueDetail_To_Mobile(int issue_no, int dsr_id) {
        logger.info("getInventoryIssueDetail Method Call...........");
        ArrayList<DeviceIssueIme> deviceImeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int issue_vali = 0;
        try {

            String whereClous = "D.MODEL_NO = M.MODEL_NO (+)            "
                    + "AND     I.ISSUE_NO = D.ISSUE_NO                    "
                    + "AND   I.DISTRIBUTER_ID=BD.BIS_ID                   "
                    + "AND   BSD.BIS_STRU_TYPE_ID  = BD.BIS_STRU_TYPE_ID  "
                    + //   "AND   BSD.BIS_STRU_TYPE_ID = 3                     " +
                    "AND  I.DSR_ID = BS.BIS_ID                          "
                    + "AND  BSS.BIS_STRU_TYPE_ID  = BS.BIS_STRU_TYPE_ID   "
                    + //   "AND   BSS.BIS_STRU_TYPE_ID = 2                     " +
                    "AND D.ISSUE_NO = '" + issue_no + "'                ";

            String ImeColumn = "DISTINCT D.ISSUE_NO,"
                    + "D.SEQ_NO,"
                    + "D.IMEI_NO,"
                    + "D.MODEL_NO,"
                    + "D.STATUS,"
                    + "M.MODEL_DESCRIPTION, "
                    + "BD.BIS_ID   DISTRIBUTOR_CODE,  "
                    + "BD.BIS_NAME DISTRIBUTOR_NAME,  "
                    + "BD.ADDRESS DISTRIBUTOR_ADDRESS,  "
                    + "BD.CONTACT_NO DISTRIBUTOR_PHONE,  "
                    + "BS.BIS_ID   SUBDEALER_CODE,  "
                    + "BS.BIS_NAME SUBDEALER_NAME,  "
                    + "BS.ADDRESS SUBDEALER_ADDRESS,  "
                    + "BS.CONTACT_NO SUBDEALER_PHONE  ";

            ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM SD_MODEL_LISTS M, SD_DEVICE_ISSUE I, SD_DEVICE_ISSUE_DTL D, SD_BUSINESS_STRUCTURES BD, SD_BIS_STRU_TYPES BSD, SD_BUSINESS_STRUCTURES BS, SD_BIS_STRU_TYPES BSS WHERE " + whereClous + " ");

            logger.info("Get Data to ResultSet...........");

            while (rs_ime.next()) {
                DeviceIssueIme dim = new DeviceIssueIme();
                dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                dim.setIssueNo(rs_ime.getInt("ISSUE_NO"));
                dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                dim.setStatus(rs_ime.getInt("STATUS"));
                dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));

                dim.setDistributorCode(rs_ime.getString("DISTRIBUTOR_CODE"));
                dim.setDistributorName(rs_ime.getString("DISTRIBUTOR_NAME"));
                dim.setDistributorAddress(rs_ime.getString("DISTRIBUTOR_ADDRESS"));
                dim.setDistributorTelephone(rs_ime.getString("DISTRIBUTOR_PHONE"));

                dim.setSubdealerCode(rs_ime.getString("SUBDEALER_CODE"));
                dim.setSubdealerName(rs_ime.getString("SUBDEALER_NAME"));
                dim.setSubdealerAddress(rs_ime.getString("SUBDEALER_ADDRESS"));
                dim.setSubdealerTelephone(rs_ime.getString("SUBDEALER_PHONE"));

                deviceImeList.add(dim);
            }
            // }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Retriview Device Issue......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceImeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceImeList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getDSRInventoryIssueDetail_To_Mobile(int dsr_id) {
        logger.info("getInventoryIssueDetail Method Call...........");
        ArrayList<DeviceIssueIme> deviceImeList = new ArrayList<>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int issue_vali = 0;
        try {

            String isRetrunQry = "  SELECT B.BIS_STRU_TYPE_ID   "
                    + " FROM  SD_BUSINESS_STRUCTURES B  "
                    + " WHERE B.BIS_ID=" + dsr_id;

            ResultSet rs1 = dbCon.search(con, isRetrunQry);

            int isReturn = 1;

//            if (rs1.next()) {
//                isReturn = rs1.getInt("BIS_STRU_TYPE_ID");
//            }
            while (rs1.next()) {
                isReturn = rs1.getInt("BIS_STRU_TYPE_ID");
            }

            String priceSelect = " ";

            if (isReturn == 2) {

                System.out.println("Price select as return BIS ID - " + dsr_id);

                priceSelect = "   NVL (( SELECT DT.PRICE FROM SD_DEVICE_ISSUE_DTL DT, SD_DEVICE_ISSUE MT WHERE MT.ISSUE_NO=DT.ISSUE_NO AND MT.DSR_ID=" + dsr_id + " AND DT.STATUS=3  "
                        + "  AND MT.ISSUE_NO=D.ISSUE_NO AND DT.SEQ_NO=D.SEQ_NO   "
                        + " ), 0) AS PRICE  ";
            } else {

                System.out.println("Price select as DSR - " + dsr_id);

                priceSelect = " NVL(M.SELLING_PRICE, 0) AS PRICE  ";
            }

            String ImeColumn = " DISTINCT D.ISSUE_NO,"
                    + "D.SEQ_NO,"
                    + "D.IMEI_NO,"
                    + ""
                    + ""
                    + ""
                    + ""
                    + "D.MODEL_NO,"
                    + "D.STATUS,"
                    + "M.MODEL_DESCRIPTION, "
                    + "M.ERP_PART_NO, "
                    + "BD.BIS_ID   DISTRIBUTOR_CODE,  "
                    + "BD.BIS_NAME DISTRIBUTOR_NAME,  "
                    + "BD.ADDRESS DISTRIBUTOR_ADDRESS,  "
                    + "BD.CONTACT_NO DISTRIBUTOR_PHONE,  "
                    + "BS.BIS_ID   SUBDEALER_CODE,  "
                    + "BS.BIS_NAME SUBDEALER_NAME,  "
                    + "BS.ADDRESS SUBDEALER_ADDRESS,  "
                    + "BS.CONTACT_NO SUBDEALER_PHONE,  "
                    + "  " + priceSelect + "     ";

            //    System.out.println("qry::: "+"SELECT " + ImeColumn + " FROM SD_MODEL_LISTS M, SD_DEVICE_ISSUE I, SD_DEVICE_ISSUE_DTL D, SD_BUSINESS_STRUCTURES BD, SD_BIS_STRU_TYPES BSD, SD_BUSINESS_STRUCTURES BS, SD_BIS_STRU_TYPES BSS WHERE " + whereClous + " ");
            String tables = " SD_DEVICE_MASTER DM, "
                    + " SD_DEVICE_ISSUE_DTL D,  "
                    + " SD_DEVICE_ISSUE I,  "
                    + " SD_MODEL_LISTS M,   "
                    + " SD_BUSINESS_STRUCTURES BD,  "
                    + " SD_BIS_STRU_TYPES BSD,  "
                    + " SD_BUSINESS_STRUCTURES BS,  "
                    + " SD_BIS_STRU_TYPES BSS   ";

            String where = "    D.ISSUE_NO= I.ISSUE_NO    "
                    + " AND   D.IMEI_NO = DM.IMEI_NO    "
                    + " AND   D.MODEL_NO = M.MODEL_NO (+)   "
                    + " AND   I.DISTRIBUTER_ID=BD.BIS_ID    "
                    + " AND   BSD.BIS_STRU_TYPE_ID  = BD.BIS_STRU_TYPE_ID   "
                    + " AND   I.DSR_ID = BS.BIS_ID                         "
                    + " AND   BSS.BIS_STRU_TYPE_ID  = BS.BIS_STRU_TYPE_ID  "
                    + " AND   D.STATUS = 3 "
                    + " AND   DM.BIS_ID=" + dsr_id + " "
                    + " AND   I.DSR_ID = " + dsr_id + "    "
                    + " AND   I.ISSUE_NO  = (SELECT MAX(ISSUE_NO) FROM SD_DEVICE_ISSUE_DTL WHERE IMEI_NO = DM.IMEI_NO)    ";

            ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM " + tables + " WHERE " + where + " ORDER BY D.MODEL_NO, D.ISSUE_NO ASC  ");

            logger.info("Get Data to ResultSet........... ");

            System.out.println("sql>>>>>>>>>> SELECT " + ImeColumn + " FROM " + tables + " WHERE " + where + " ORDER BY D.MODEL_NO, D.ISSUE_NO ASC ");

            while (rs_ime.next()) {
                DeviceIssueIme dim = new DeviceIssueIme();
                dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                dim.setIssueNo(rs_ime.getInt("ISSUE_NO"));
                dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                dim.setStatus(rs_ime.getInt("STATUS"));
                dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));
                dim.setErpModel(rs_ime.getString("ERP_PART_NO"));

                dim.setDistributorCode(rs_ime.getString("DISTRIBUTOR_CODE"));
                dim.setDistributorName(rs_ime.getString("DISTRIBUTOR_NAME"));
                dim.setDistributorAddress(rs_ime.getString("DISTRIBUTOR_ADDRESS"));
                dim.setDistributorTelephone(rs_ime.getString("DISTRIBUTOR_PHONE"));

                dim.setSubdealerCode(rs_ime.getString("SUBDEALER_CODE"));
                dim.setSubdealerName(rs_ime.getString("SUBDEALER_NAME"));
                dim.setSubdealerAddress(rs_ime.getString("SUBDEALER_ADDRESS"));
                dim.setSubdealerTelephone(rs_ime.getString("SUBDEALER_PHONE"));

//                    dim.setSubdealerCode(rs_ime.getString("DISTRIBUTOR_CODE"));
//                    dim.setSubdealerName(rs_ime.getString("DISTRIBUTOR_NAME"));
//                    dim.setSubdealerAddress(rs_ime.getString("DISTRIBUTOR_ADDRESS"));
//                    dim.setSubdealerTelephone(rs_ime.getString("DISTRIBUTOR_PHONE"));
//                    
//                    dim.setDistributorCode(rs_ime.getString("SUBDEALER_CODE"));
//                    dim.setDistributorName(rs_ime.getString("SUBDEALER_NAME"));
//                    dim.setDistributorAddress(rs_ime.getString("SUBDEALER_ADDRESS"));
//                    dim.setDistributorTelephone(rs_ime.getString("SUBDEALER_PHONE"));
                double salesPrice = rs_ime.getDouble("PRICE");

                if (isReturn == 1) {
                    //double realSalesPrice = (salesPrice/86)*90;

                    double realSalesPrice = salesPrice * 0.9;
                    dim.setSalesPrice(realSalesPrice);
                } else {
                    dim.setSalesPrice(salesPrice);
                }

                deviceImeList.add(dim);
            }
            // }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Retriview Device Issue......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceImeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceImeList);

        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getAllDSRModelList(int dsr_id, int issueNo, String modelDesc) {
        logger.info("getAllModelList Method Call...........");
        ArrayList<ModelList> moodelList = new ArrayList<ModelList>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {
            String issueNoW = "";
            String modelW = "";
            if (issueNo != 0) {
                issueNoW = "    AND   I.ISSUE_NO = " + issueNo + "    ";
            }

            if (modelDesc.equalsIgnoreCase("all")) {
                modelW = " M.MODEL_DESCRIPTION = M.MODEL_DESCRIPTION ";
            } else {
                modelW = " UPPER(M.MODEL_DESCRIPTION) LIKE '%" + modelDesc.toUpperCase() + "%' ";
            }

            String ImeColumn = "  DISTINCT  D.MODEL_NO, "
                    + " M.MODEL_DESCRIPTION, "
                    + " M.ERP_PART_NO   ";

            //    System.out.println("qry::: "+"SELECT " + ImeColumn + " FROM SD_MODEL_LISTS M, SD_DEVICE_ISSUE I, SD_DEVICE_ISSUE_DTL D, SD_BUSINESS_STRUCTURES BD, SD_BIS_STRU_TYPES BSD, SD_BUSINESS_STRUCTURES BS, SD_BIS_STRU_TYPES BSS WHERE " + whereClous + " ");
            String tables = " SD_DEVICE_MASTER DM, "
                    + " SD_DEVICE_ISSUE_DTL D,  "
                    + " SD_DEVICE_ISSUE I,  "
                    + " SD_MODEL_LISTS M,   "
                    + " SD_BUSINESS_STRUCTURES BD,  "
                    + " SD_BIS_STRU_TYPES BSD,  "
                    + " SD_BUSINESS_STRUCTURES BS,  "
                    + " SD_BIS_STRU_TYPES BSS   ";

            String where = "    D.ISSUE_NO= I.ISSUE_NO    "
                    + " AND   D.IMEI_NO = DM.IMEI_NO    "
                    + " AND   D.MODEL_NO = M.MODEL_NO (+)   "
                    + " AND   I.DISTRIBUTER_ID=BD.BIS_ID    "
                    + " AND   BSD.BIS_STRU_TYPE_ID  = BD.BIS_STRU_TYPE_ID   "
                    + " AND   I.DSR_ID = BS.BIS_ID                         "
                    + " AND   BSS.BIS_STRU_TYPE_ID  = BS.BIS_STRU_TYPE_ID  "
                    + " AND   D.STATUS = 3 "
                    + " AND   " + modelW + "  "
                    + " AND   DM.BIS_ID=" + dsr_id + "  "
                    + " AND   I.DSR_ID = " + dsr_id + "    "
                    + " AND   I.ISSUE_NO  = (SELECT MAX(ISSUE_NO) FROM SD_DEVICE_ISSUE_DTL WHERE IMEI_NO = DM.IMEI_NO)  "
                    + issueNoW;

            //ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM SD_MODEL_LISTS M, SD_DEVICE_ISSUE I, SD_DEVICE_ISSUE_DTL D, SD_BUSINESS_STRUCTURES BD, SD_BIS_STRU_TYPES BSD, SD_BUSINESS_STRUCTURES BS, SD_BIS_STRU_TYPES BSS WHERE " + whereClous + " ORDER BY D.MODEL_NO, D.ISSUE_NO ASC  ");
            ResultSet rs = dbCon.search(con, "SELECT " + ImeColumn + " FROM " + tables + " WHERE " + where + " ORDER BY D.MODEL_NO ASC  ");

            logger.info("Get Data to ResultSet...........");

//                HashMap<Integer,String> models= new HashMap<Integer,String>();
            HashMap<String, String> models = new HashMap<String, String>();

            ModelList modelDef = new ModelList();
            modelDef.setModel_No(9999);
            modelDef.setModel_Description("All");
            modelDef.setErp_part("All");
            moodelList.add(modelDef);

            while (rs.next()) {
                ModelList model = new ModelList();
                model.setModel_No(rs.getInt("MODEL_NO"));
                model.setModel_Description(rs.getString("MODEL_DESCRIPTION"));
                model.setErp_part(rs.getString("ERP_PART_NO"));

                moodelList.add(model);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = moodelList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(moodelList);

        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addInventoryIssue(DeviceIssue deviceissue_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("addInventoryIssue Method Call.............................");
        int result = 0;
        int max_issue_no = 0;
        int imei_al_issue = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

//        dbCon.offAutoCommit(con);
        String invalidImeis = "";
        int hasInvalid = 0;

        String invoiceNo = "00";

        ArrayList<AcceptInvenroryImei> acceptInvImei = new ArrayList<AcceptInvenroryImei>();
        try {

            ArrayList<DeviceIssueIme> device_Ime_check = new ArrayList<DeviceIssueIme>();
            device_Ime_check = deviceissue_info.getDeviceImei();

            ArrayList<String> requestList = new ArrayList<>();
            ArrayList<String> temrequestList = new ArrayList<>();

            for (DeviceIssueIme di : device_Ime_check) {
                requestList.add(di.getImeiNo());
                temrequestList.add(di.getImeiNo());
            }

            ArrayList<String> issueList = new ArrayList<>();

//            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
//                    + "FROM SD_DEVICE_ISSUE_DTL "
//                    + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE  DISTRIBUTER_ID = " + deviceissue_info.getDistributerID() + ") "
//                    + "AND STATUS != 3");
//
//            while (rs_issue_cnt.next()) {
//                String imei = rs_issue_cnt.getString("IMEI_NO");
//                issueList.add(imei);
//            }
//            
//            for (String s : requestList) {
//                if (issueList.contains(s)) {
//                    return_imei += s + ",";
//                    imei_al_issue = 1;
//                }
//            }
            if (imei_al_issue == 1) {
                result = 3;
            } else {

//                ArrayList<String> locationList = new ArrayList<>();
//
//                ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
//                        + "FROM SD_DEVICE_MASTER "
//                        + "WHERE BIS_ID = " + deviceissue_info.getDistributerID() + " "
//                        + "AND STATUS = 1");
//
//                while (rs_ime_cnt.next()) {
//                    String imei = rs_ime_cnt.getString("IMEI_NO");
//                    locationList.add(imei);
//                }
//               
//                temrequestList.removeAll(locationList);
//                if (temrequestList.size() > 0) {
//                    for (String s : temrequestList) {
//                        return_imei += s + ",";
//                    }
//                    result = 2;
//                } else {
                ArrayList<String> availImeis = new ArrayList<>();
                String chkImeiQry = " SELECT * FROM SD_DEVICE_MASTER D "
                        + " WHERE D.BIS_ID='" + deviceissue_info.getDistributerID() + "'";
                ResultSet rchkI = dbCon.search(con, chkImeiQry);

                System.out.println("SELECT DEVICES " + chkImeiQry);

                while (rchkI.next()) {
                    availImeis.add(rchkI.getString("IMEI_NO"));
                }

                ArrayList<String> creditNoteList = new ArrayList<>();
                ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                        + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                        + "WHERE STATUS = 1");

                while (rs_crd_cnt.next()) {
                    String imei = rs_crd_cnt.getString("IMEI_NO");
                    creditNoteList.add(imei);
                }
                // rs_crd_cnt.close();

                for (String s : requestList) {

                    if (creditNoteList.contains(s)) {

                        return_imei += s + ",";
                        imei_crdit = 1;
                    }
                }

                if (imei_crdit == 1) {
                    result = 4;
                } else {
                    ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(ISSUE_NO),0) MAX FROM SD_DEVICE_ISSUE");

                    while (rs_max.next()) {
                        max_issue_no = rs_max.getInt("MAX");
                    }
                    //rs_max.close();

                    String insertColumns = ""
                            + "DISTRIBUTER_ID,"
                            + "DSR_ID,"
                            + "ISSUE_DATE,"
                            + "PRICE,"
                            + "DISTRIBUTOR_MARGIN,"
                            + "DEALER_MARGIN,"
                            + "DISCOUNT,"
                            + "STATUS,"
                            + "USER_INSERTED,"
                            + "DATE_INSERTED,"
                            + "REMARKS,"
                            + "MANUAL_RECEIPT_NO ";

                    String insertValue = "?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,?,?,?,SYSDATE,?,?";

                    String deviceDetailsColumn = "SEQ_NO,"
                            + "ISSUE_NO,"
                            + "IMEI_NO,"
                            + "MODEL_NO,"
                            + "STATUS,"
                            + "USER_INSERTED,"
                            + "DATE_INSERTED,"
                            + "PRICE,"
                            + "MARGIN";

                    String sqlForManual = "SELECT COUNT(*) CNT "
                            + "FROM SD_DEVICE_ISSUE DI "
                            + "WHERE MANUAL_RECEIPT_NO='" + deviceissue_info.getManualReceiptNo() + "' "
//                            + " AND TO_CHAR(DI.ISSUE_DATE,'MM/DD/YYYY') = TO_CHAR(SYSDATE, 'MM/DD/YYYY') ";
//                            + " AND TO_CHAR(DI.ISSUE_DATE,'MM/DD/YYYY') = '"+deviceissue_info.getIssueDate()+"' ";
                            + " AND TO_CHAR(DI.ISSUE_DATE,'MM/DD/YYYY') =  TO_CHAR(TO_DATE('"+deviceissue_info.getIssueDate()+"', 'MM/DD/YYYY'), 'MM/DD/YYYY') ";

                    ResultSet resultsForManualValidation = dbCon.search(con, sqlForManual);

                    int isAllowToInsert = 0;

                    while (resultsForManualValidation.next()) {
                        isAllowToInsert = resultsForManualValidation.getInt("CNT");
                    }

                    if (isAllowToInsert == 0) {

                        try {

                            PreparedStatement ps_in_device_hdr = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE(" + insertColumns + ") "
                                    + "VALUES(" + insertValue + ")");

                            max_issue_no = max_issue_no + 1;

//                        ps_in_device_hdr.setInt(1, max_issue_no);
                            ps_in_device_hdr.setInt(1, deviceissue_info.getDistributerID());
                            ps_in_device_hdr.setInt(2, deviceissue_info.getdSRId());
                            ps_in_device_hdr.setString(3, deviceissue_info.getIssueDate());
                            ps_in_device_hdr.setDouble(4, deviceissue_info.getPrice());
                            ps_in_device_hdr.setDouble(5, deviceissue_info.getDistributorMargin());
                            ps_in_device_hdr.setDouble(6, deviceissue_info.getDelerMargin());
                            ps_in_device_hdr.setDouble(7, deviceissue_info.getDiscount());

                            ps_in_device_hdr.setInt(8, deviceissue_info.getStatus());
                            ps_in_device_hdr.setString(9, user_id);
                            ps_in_device_hdr.setString(10, deviceissue_info.getRemarks());
                            ps_in_device_hdr.setString(11, deviceissue_info.getManualReceiptNo());

                            ps_in_device_hdr.executeUpdate();

                            ResultSet issue_no_dtl = dbCon.search(con, "select DEVICE_ISU_ID_SEQ.currval from dual");

                            while (issue_no_dtl.next()) {
                                invoiceNo = String.valueOf(issue_no_dtl.getInt("CURRVAL"));
                            }
                            System.out.println(">>>>>>>>>>>>>>>> Device Issue Details Inseted.....");
                            logger.info("Device Issue Details Inseted.....");
                            result = 1;

                        } catch (Exception e) {
                            logger.info("Error Device Issue Details Insert....." + e.toString());
                            result = 9;
                            e.printStackTrace();
                        }

                        try {

                            ArrayList<DeviceIssueIme> dIme = new ArrayList<DeviceIssueIme>();
                            dIme = deviceissue_info.getDeviceImei();
                            if (dIme.size() > 0) {

                                int max_device_dtl = 0;
                                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                        + "FROM SD_DEVICE_ISSUE_DTL "
                                        + "WHERE ISSUE_NO = " + invoiceNo + "");

                                while (rs_max_dtl.next()) {
                                    max_device_dtl = rs_max_dtl.getInt("MAX");
                                }

                                String deviceIsuueImeValue = "?,?,?,?,2,?,SYSDATE,?,?";

                                PreparedStatement ps_in_device_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE_DTL(" + deviceDetailsColumn + ") "
                                        + "VALUES(" + deviceIsuueImeValue + ")");

                                PreparedStatement ps_up_dmast = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                        + "SET BIS_ID = ? ,"
                                        + "USER_MODIFIED=?,"
                                        + "DATE_MODIFIED=SYSDATE "
                                        + "WHERE IMEI_NO = ?");

                                Set<String> imeiList = new HashSet();

                                for (DeviceIssueIme di : dIme) {

                                    if (availImeis.contains(di.getImeiNo())) {     //Check IMEIs are available to selected DSR

                                        if (!imeiList.contains(di.getImeiNo())) {

                                            max_device_dtl = max_device_dtl + 1;
                                            ps_in_device_dtl.setInt(1, max_device_dtl);
                                            ps_in_device_dtl.setInt(2, Integer.parseInt(invoiceNo));
                                            ps_in_device_dtl.setString(3, di.getImeiNo());
                                            //                                    for (DeviceIssueIme dime : TemdIme) {
                                            //
                                            //                                        if (dime.getImeiNo().equals(di.getImeiNo())) {
                                            ps_in_device_dtl.setInt(4, di.getModleNo());
                                            //                                        }
                                            //                                    }
                                            ps_in_device_dtl.setString(5, user_id);
                                            ps_in_device_dtl.setDouble(6, di.getSalesPrice());
                                            ps_in_device_dtl.setDouble(7, di.getMargin());
                                            ps_in_device_dtl.addBatch();

                                            ///////////Updating DeviceIssueMaster//////////////////
                                            ps_up_dmast.setInt(1, deviceissue_info.getdSRId());
                                            ps_up_dmast.setString(2, user_id);
                                            ps_up_dmast.setString(3, di.getImeiNo());
                                            ps_up_dmast.addBatch();

                                            AcceptInvenroryImei imei = new AcceptInvenroryImei();
                                            imei.setBisId(deviceissue_info.getdSRId());
                                            imei.setImeiNo(di.getImeiNo());
                                            imei.setIssueNo(max_issue_no);
                                            imei.setModleDescription(di.getModleDesc());
                                            imei.setModleNo(di.getModleNo());
                                            imei.setSeqNo(di.getSeqNo());
                                            imei.setStatus(2);
                                            imei.setStatusMsg("Accepted");
                                            acceptInvImei.add(imei);

                                            imeiList.add(di.getImeiNo());

                                        }
                                    } else {
                                        invalidImeis += di.getImeiNo() + " \n";

                                        hasInvalid = 1;
                                    }
                                }
                                ps_in_device_dtl.executeBatch();

                                ps_up_dmast.executeBatch();
                                logger.info("Device Issue Details IME  Data Inserted........");
                                result = 1;
                            }

                        } catch (Exception e) {
                            logger.info("Error in Device Issue Details IME  Data Inserte....." + e.toString());
                            e.printStackTrace();
                            result = 9;
                        }

                    } else {

                        result = 1;

                    }
                }
                // }
            }

        } catch (SQLException se) {
            result = 8;
            se.printStackTrace();
        } catch (Exception ex) {
            logger.info("Error addInventoryIssue Method..:" + ex.toString());
            ex.printStackTrace();
        }
        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            Date date = new Date();
            if (hasInvalid == 0) {
//                try {
//                    dbCon.doCommit(con);
//                } catch (SQLException ex) {
//                    dbCon.doRollback(con);
//                    ex.printStackTrace();
//                }

                vw.setReturnFlag("1");
                vw.setReturnMsg(invoiceNo);
            } else {
//                dbCon.doRollback(con);

                vw.setReturnFlag("100");
                vw.setReturnMsg("Following IMEIs are not belong to you! " + invalidImeis);
            }
            vw.setReturnRefNo(dateFormat.format(date));
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI " + return_imei + " is not in Your Location");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Already Issue  " + return_imei + " IMEI Number in List");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("This  " + return_imei + " IMEI No have a Credit Note");
        } else if (con == null) {
            vw.setReturnFlag("8");
            vw.setReturnMsg("Cannot connect to the database. Please contact the system admin");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Issue Inventory Insert Fail");
        }

        int chkParentFlag = 0;

        try {

//             String chkParentQry = " SELECT COUNT(*) CNT "
//                + " FROM (  "
//                + " SELECT *    "
//                + " FROM SD_BUSINESS_STRUCTURES B   "
//                + " START WITH          B.BIS_ID = "+deviceissue_info.getDistributerID()+"  "
//                + " CONNECT BY PRIOR    B.PARENT_BIS_ID = B.BIS_ID  "
//                + " )   "
//                + " WHERE BIS_ID="+deviceissue_info.getdSRId()+"    ";
//        
            String chkParentQry = "SELECT B.BIS_STRU_TYPE_ID CNT "
                    + "FROM SD_BUSINESS_STRUCTURES B "
                    + "WHERE B.BIS_ID= " + deviceissue_info.getdSRId() + "  ";

            ResultSet rs_chkParentQry = dbCon.search(con, chkParentQry);

            if (rs_chkParentQry.next()) {
                chkParentFlag = rs_chkParentQry.getInt("CNT");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            dbCon.onAutoCommit(con);
            dbCon.ConectionClose(con);

        }

        System.out.println("Is Parent.......... : " + chkParentFlag);

        if (chkParentFlag == 2 && deviceissue_info.getStatus() == 2) {
            AcceptInventory acceptinventory_info = new AcceptInventory();
            acceptinventory_info.setAcceptInvImei(acceptInvImei);
            acceptinventory_info.setDsrId(deviceissue_info.getdSRId());
            acceptinventory_info.setIssueNo(max_issue_no);
            acceptinventory_info.setStatus(deviceissue_info.getStatus());
            AcceptInventoryController.addAcceptInventory(acceptinventory_info, user_id, room, department, branch, countryCode, division, organaization);
        }
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static int closeDayEnd(int bisId, int totalTrans, int itemCnt) {

        logger.info("closeDayEnd Method Call.............................");

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        Date startDate = null;

        int result = 0;

        int currentStock = 0;

        try {

            String qryForTallyInventory = "SELECT COUNT(IMEI_NO) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE BIS_ID = '" + bisId + "' ";

            System.out.println(">>>>> qryForTallyInventory  " + qryForTallyInventory);

            ResultSet rsForStock = dbCon.search(con, qryForTallyInventory);

            while (rsForStock.next()) {
                currentStock = rsForStock.getInt("CNT");
            }

            if (currentStock == itemCnt) {

                String qryFindLAstRec = "SELECT DATE_START FROM ( SELECT DATE_START "
                        + "FROM SD_DAY_END_LOG "
                        + "WHERE BIS_ID = '" + bisId + "' "
                        + "ORDER BY DATE_START DESC ) WHERE ROWNUM = 1";

                System.out.println(">>>>> qryFindLAstRec " + qryFindLAstRec);
                ResultSet rsForLastBisId = dbCon.search(con, qryFindLAstRec);

                while (rsForLastBisId.next()) {

                    startDate = rsForLastBisId.getDate("DATE_START");

                }

                String qryForUpdateDay = "UPDATE SD_DAY_END_LOG "
                        + "SET STATUS =  'complete', "
                        + "DATE_END = SYSDATE, "
                        + "TOTAL_TRANS= '" + totalTrans + "'  "
                        + "WHERE DATE_START  IN (" + qryFindLAstRec + ")  "
                        + "AND BIS_ID='" + bisId + "'";

                System.out.println("UPDATE DAY END " + qryForUpdateDay);

                PreparedStatement prUpdateDayEnd = dbCon.prepare(con, qryForUpdateDay);

                prUpdateDayEnd.executeUpdate();

                result = 1;

            } else {
                result = 3;
            }

        } catch (Exception ex) {

            result = 2;

            ex.printStackTrace();

        }

        return result;
    }

    public static ValidationWrapper addOfflineInventoryIssue(DeviceIssueOffline deviceissue_info_pre,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("addOfflineInventoryIssue Method Call.............................");

        DeviceIssueOfflineList[] deviceissue_info_data = deviceissue_info_pre.getDeviceIssue();

        System.out.println("addOfflineInventoryIssue >>>>>>>>>>>>>>>>>>>> ");

        int result = 0;

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        dbCon.offAutoCommit(con);

        String invalidImeis = "";
        int hasInvalid = 0;

        String invoiceNo = "00";

        String return_imei = "";

        ValidationWrapper vw = new ValidationWrapper();
        int chkParentFlag = 0;

        try {

            for (DeviceIssueOfflineList deviceissue_info : deviceissue_info_data) {

                int max_issue_no = 0;
                int imei_al_issue = 0;
                int imei_crdit = 0;

//                System.out.println("device issue offline loop");
                String offlineIssuNo = deviceissue_info.getOfflineIssueNo();

                ResultSet isAvilable = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_DEVICE_ISSUE WHERE OFFLINE_ISU_NO ='" + offlineIssuNo + "' ");

                int pendingCnt = 0;

                while (isAvilable.next()) {
                    pendingCnt = isAvilable.getInt("CNT");
                }

                dbCon.offAutoCommit(con);

                if (pendingCnt == 0) {

                    System.out.println("pending count is > 0");
                    ArrayList<AcceptInvenroryImei> acceptInvImei = new ArrayList<AcceptInvenroryImei>();

                    try {

                        ArrayList<DeviceIssueIme> device_Ime_check = new ArrayList<DeviceIssueIme>();
                        device_Ime_check = deviceissue_info.getDeviceImei();

                        ArrayList<String> requestList = new ArrayList<>();
                        ArrayList<String> temrequestList = new ArrayList<>();

                        for (DeviceIssueIme di : device_Ime_check) {
                            requestList.add(di.getImeiNo());
                            temrequestList.add(di.getImeiNo());
                        }

                        ArrayList<String> issueList = new ArrayList<>();

                        if (imei_al_issue == 1) {
                            result = 3;
                            System.out.println("RESULT 3");
                        } else {

                            System.out.println("line 1003....");
                            ArrayList<String> availImeis = new ArrayList<>();
                            String chkImeiQry = " SELECT * FROM SD_DEVICE_MASTER D "
                                    + " WHERE D.BIS_ID='" + deviceissue_info.getDistributerID() + "'";
                            ResultSet rchkI = dbCon.search(con, chkImeiQry);

//                            System.out.println("SELECT DEVICES " + chkImeiQry);
                            while (rchkI.next()) {
                                availImeis.add(rchkI.getString("IMEI_NO"));
                            }

                            ArrayList<String> creditNoteList = new ArrayList<>();
                            ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                                    + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                                    + "WHERE STATUS = 1");

                            while (rs_crd_cnt.next()) {
                                String imei = rs_crd_cnt.getString("IMEI_NO");
                                creditNoteList.add(imei);
                            }

                            for (String s : requestList) {

                                if (creditNoteList.contains(s)) {

                                    return_imei += s + ",";
                                    imei_crdit = 1;
                                }
                            }

                            if (imei_crdit == 1) {
                                result = 4;
                                System.out.println("imei credit ");
                            } else {

                                System.out.println("imei credit else");
                                ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(ISSUE_NO),0) MAX FROM SD_DEVICE_ISSUE");

                                while (rs_max.next()) {
                                    max_issue_no = rs_max.getInt("MAX");
                                }
                                //rs_max.close();

                                String insertColumns = "ISSUE_NO,"
                                        + "DISTRIBUTER_ID,"
                                        + "DSR_ID,"
                                        + "ISSUE_DATE,"
                                        + "PRICE,"
                                        + "DISTRIBUTOR_MARGIN,"
                                        + "DEALER_MARGIN,"
                                        + "DISCOUNT,"
                                        + "STATUS,"
                                        + "USER_INSERTED,"
                                        + "DATE_INSERTED,"
                                        + "REMARKS,"
                                        + "MANUAL_RECEIPT_NO,"
                                        + "OFFLINE_ISU_NO ";

                                String insertValue = "?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,?,?,?,SYSDATE,?,?,?";

                                String deviceDetailsColumn = "SEQ_NO,"
                                        + "ISSUE_NO,"
                                        + "IMEI_NO,"
                                        + "MODEL_NO,"
                                        + "STATUS,"
                                        + "USER_INSERTED,"
                                        + "DATE_INSERTED,"
                                        + "PRICE,"
                                        + "MARGIN";

                                try {

                                    PreparedStatement ps_in_device_hdr = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE(" + insertColumns + ") "
                                            + "VALUES(" + insertValue + ")");

                                    max_issue_no = max_issue_no + 1;
                                    invoiceNo = String.valueOf(max_issue_no);

                                    ps_in_device_hdr.setInt(1, max_issue_no);
                                    ps_in_device_hdr.setInt(2, deviceissue_info.getDistributerID());
                                    ps_in_device_hdr.setInt(3, deviceissue_info.getdSRId());
                                    ps_in_device_hdr.setString(4, deviceissue_info.getIssueDate());
                                    ps_in_device_hdr.setDouble(5, deviceissue_info.getPrice());
                                    ps_in_device_hdr.setDouble(6, deviceissue_info.getDistributorMargin());
                                    ps_in_device_hdr.setDouble(7, deviceissue_info.getDelerMargin());
                                    ps_in_device_hdr.setDouble(8, deviceissue_info.getDiscount());

                                    ps_in_device_hdr.setInt(9, deviceissue_info.getStatus());
                                    ps_in_device_hdr.setString(10, user_id);
                                    ps_in_device_hdr.setString(11, deviceissue_info.getRemarks());
                                    ps_in_device_hdr.setString(12, deviceissue_info.getManualReceiptNo());
                                    ps_in_device_hdr.setString(13, offlineIssuNo);

                                    System.out.println("OFFLINE RECIPT NUMBER .............. " + offlineIssuNo);

                                    ps_in_device_hdr.executeUpdate();
                                    logger.info("Device Issue Details Inseted.....");
                                    result = 1;

                                } catch (Exception e) {
                                    logger.info("Error Device Issue Details Insert....." + e.toString());
                                    System.out.println("Error Device Issue Details Insert....");
                                    result = 9;
                                    e.printStackTrace();
                                }

                                try {

                                    ArrayList<DeviceIssueIme> dIme = new ArrayList<DeviceIssueIme>();
                                    dIme = deviceissue_info.getDeviceImei();

                                    System.out.println(">>>> issue note number imei " + dIme.size());

                                    if (dIme.size() > 0) {

                                        int max_device_dtl = 0;
                                        ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                                + "FROM SD_DEVICE_ISSUE_DTL "
                                                + "WHERE ISSUE_NO = " + max_issue_no + "");

                                        while (rs_max_dtl.next()) {
                                            max_device_dtl = rs_max_dtl.getInt("MAX");
                                        }

                                        System.out.println("Insert to details");

                                        String deviceIsuueImeValue = "?,?,?,?,2,?,SYSDATE,?,?";

                                        PreparedStatement ps_in_device_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE_DTL(" + deviceDetailsColumn + ") "
                                                + "VALUES(" + deviceIsuueImeValue + ")");

                                        PreparedStatement ps_up_dmast = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                                + "SET BIS_ID = ? ,"
                                                + "USER_MODIFIED=?,"
                                                + "DATE_MODIFIED=SYSDATE "
                                                + "WHERE IMEI_NO = ?");

                                        for (DeviceIssueIme di : dIme) {

                                            System.out.println("IMEI LIST" + di.getImeiNo() + " edf " + availImeis.contains(di.getImeiNo()));

                                            if (availImeis.contains(di.getImeiNo())) {     //Check IMEIs are available to selected DSR

                                                max_device_dtl = max_device_dtl + 1;
                                                ps_in_device_dtl.setInt(1, max_device_dtl);
                                                ps_in_device_dtl.setInt(2, max_issue_no);
                                                ps_in_device_dtl.setString(3, di.getImeiNo());
                                                //                                    for (DeviceIssueIme dime : TemdIme) {
                                                //
                                                //                                        if (dime.getImeiNo().equals(di.getImeiNo())) {
                                                ps_in_device_dtl.setInt(4, di.getModleNo());
                                                //                                        }
                                                //                                    }
                                                ps_in_device_dtl.setString(5, user_id);
                                                ps_in_device_dtl.setDouble(6, di.getSalesPrice());
                                                ps_in_device_dtl.setDouble(7, di.getMargin());
                                                ps_in_device_dtl.addBatch();

                                                ///////////Updating DeviceIssueMaster//////////////////
                                                ps_up_dmast.setInt(1, deviceissue_info.getdSRId());
                                                ps_up_dmast.setString(2, user_id);
                                                ps_up_dmast.setString(3, di.getImeiNo());
                                                ps_up_dmast.addBatch();

                                                AcceptInvenroryImei imei = new AcceptInvenroryImei();
                                                imei.setBisId(deviceissue_info.getdSRId());
                                                imei.setImeiNo(di.getImeiNo());
                                                imei.setIssueNo(max_issue_no);
                                                imei.setModleDescription(di.getModleDesc());
                                                imei.setModleNo(di.getModleNo());
                                                imei.setSeqNo(di.getSeqNo());
                                                imei.setStatus(2);
                                                imei.setStatusMsg("Accepted");
                                                acceptInvImei.add(imei);
                                            } else {
                                                invalidImeis += di.getImeiNo() + " \n";

                                                hasInvalid = 1;
                                            }
                                        }
                                        ps_in_device_dtl.executeBatch();

                                        ps_up_dmast.executeBatch();
                                        logger.info("Device Issue Details IME  Data Inserted........");
                                        result = 1;
                                    }

                                } catch (Exception e) {
                                    logger.info("Error in Device Issue Details IME  Data Inserte....." + e.toString());
                                    e.printStackTrace();
                                    result = 9;
                                }
                            }
                            // }
                        }

                    } catch (SQLException se) {
                        result = 8;
                        se.printStackTrace();
                    } catch (Exception ex) {
                        logger.info("Error addInventoryIssue Method..:" + ex.toString());
                        ex.printStackTrace();
                    }

                } else {
                    System.out.println("System has same offline number.....");
                    result = 50;
                }

            }

            if (result == 1) {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                Date date = new Date();
                if (hasInvalid == 0) {
                    try {
                        dbCon.doCommit(con);
                    } catch (SQLException ex) {
                        dbCon.doRollback(con);
                        ex.printStackTrace();
                    }

                    vw.setReturnFlag("1");
                    vw.setReturnMsg(invoiceNo);
                } else {

                    vw.setReturnFlag("100");
                    vw.setReturnMsg("Following IMEIs are not belong to you! " + invalidImeis);
                }
                vw.setReturnRefNo(dateFormat.format(date));
            } else if (result == 2) {
                vw.setReturnFlag("2");
                vw.setReturnMsg("This IMEI " + return_imei + " is not in Your Location");
            } else if (result == 3) {
                vw.setReturnFlag("3");
                vw.setReturnMsg("Already Issue  " + return_imei + " IMEI Number in List");
            } else if (result == 4) {
                vw.setReturnFlag("4");
                vw.setReturnMsg("This  " + return_imei + " IMEI No have a Credit Note");
            } else if (result == 50) {
                vw.setReturnFlag("50");
                vw.setReturnMsg("Transactions are already synchronized!");
            } else if (con == null) {
                vw.setReturnFlag("8");
                vw.setReturnMsg("Cannot connect to the database. Please contact the system admin");
            } else {
                vw.setReturnFlag("9");
                vw.setReturnMsg("Issue Inventory Insert Fail");
            }

//             String chkParentQry = " SELECT COUNT(*) CNT "
//                + " FROM (  "
//                + " SELECT *    "
//                + " FROM SD_BUSINESS_STRUCTURES B   "
//                + " START WITH          B.BIS_ID = "+deviceissue_info.getDistributerID()+"  "
//                + " CONNECT BY PRIOR    B.PARENT_BIS_ID = B.BIS_ID  "
//                + " )   "
//                + " WHERE BIS_ID="+deviceissue_info.getdSRId()+"    ";
//        
        } catch (Exception e) {
            dbCon.doRollback(con);
            e.printStackTrace();
        } finally {
            dbCon.onAutoCommit(con);
            dbCon.ConectionClose(con);

        }

        System.out.println("Is Parent.......... : " + chkParentFlag);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static ValidationWrapper editInventoryIssue(DeviceIssue deviceissue_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("editInventoryIssue Method Call.............................");
        int result = 0;
        int imei_al_issue = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<DeviceIssueIme> device_Ime_check = new ArrayList<DeviceIssueIme>();
            device_Ime_check = deviceissue_info.getDeviceImei();

            ArrayList<String> requestList = new ArrayList<>();
            for (DeviceIssueIme di : device_Ime_check) {
                requestList.add(di.getImeiNo());
            }

            ArrayList<String> issueList = new ArrayList<>();

            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
                    + "FROM SD_DEVICE_ISSUE_DTL "
                    + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE  DISTRIBUTER_ID = " + deviceissue_info.getDistributerID() + ") "
                    + "AND ISSUE_NO != " + deviceissue_info.getIssueNo() + " "
                    + "AND STATUS != 3");

            while (rs_issue_cnt.next()) {
                String imei = rs_issue_cnt.getString("IMEI_NO");
                issueList.add(imei);
            }
            //  rs_issue_cnt.close();

            for (String s : requestList) {
                if (issueList.contains(s)) {
                    return_imei = return_imei + "," + s;
                    imei_al_issue = 1;
                }
            }

            if (imei_al_issue == 1) {
                result = 3;
            } else {

                ArrayList<String> locationList = new ArrayList<>();

                ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
                        + "FROM SD_DEVICE_MASTER "
                        + "WHERE BIS_ID = " + deviceissue_info.getDistributerID() + " "
                        + "AND STATUS = 1");

                while (rs_ime_cnt.next()) {
                    String imei = rs_ime_cnt.getString("IMEI_NO");
                    locationList.add(imei);
                }
                // rs_ime_cnt.close();

                ArrayList<String> temrequestList = new ArrayList<>();
                temrequestList = requestList;
                temrequestList.removeAll(locationList);

                if (temrequestList.size() > 0) {
                    for (String s : temrequestList) {
                        return_imei = return_imei + "," + s;
                    }
                    result = 2;
                } else {

                    ArrayList<String> creditNoteList = new ArrayList<>();
                    ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                            + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                            + "WHERE STATUS = 1");

                    while (rs_crd_cnt.next()) {
                        String imei = rs_crd_cnt.getString("IMEI_NO");
                        creditNoteList.add(imei);
                    }
                    // rs_crd_cnt.close();

                    for (String s : requestList) {
                        if (creditNoteList.contains(s)) {
                            return_imei = return_imei + "," + s;
                            imei_crdit = 1;
                        }
                    }

                    if (imei_crdit == 1) {
                        result = 4;
                    } else {

                        try {

                            PreparedStatement ps_up_device = dbCon.prepare(con, "UPDATE SD_DEVICE_ISSUE "
                                    + "SET DISTRIBUTER_ID=?,"
                                    + "DSR_ID=?,"
                                    + "ISSUE_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                                    + "PRICE=?,"
                                    + "DISTRIBUTOR_MARGIN=?,"
                                    + "DEALER_MARGIN=?,"
                                    + "DISCOUNT=?,"
                                    + "STATUS=?,"
                                    + "USER_MODIFIED=?,"
                                    + "DATE_MODIFIED=SYSDATE "
                                    + "WHERE ISSUE_NO=?");

                            ps_up_device.setInt(1, deviceissue_info.getDistributerID());
                            ps_up_device.setInt(2, deviceissue_info.getdSRId());
                            ps_up_device.setString(3, deviceissue_info.getIssueDate());
                            ps_up_device.setDouble(4, deviceissue_info.getPrice());
                            ps_up_device.setDouble(5, deviceissue_info.getDistributorMargin());
                            ps_up_device.setDouble(6, deviceissue_info.getDelerMargin());
                            ps_up_device.setDouble(7, deviceissue_info.getDiscount());
                            ps_up_device.setInt(8, deviceissue_info.getStatus());
                            ps_up_device.setString(9, user_id);
                            ps_up_device.setInt(10, deviceissue_info.getIssueNo());
                            ps_up_device.executeUpdate();
                            result = 1;
                        } catch (Exception e) {
                            logger.info("Error in Updating.......");
                            result = 9;
                        }

                        try {
                            if (deviceissue_info.getDeviceImei() != null) {

                                deviceissue_info.getDeviceImei().get(0);

                                logger.info("Delete Device Issue Details from Issue No + " + deviceissue_info.getIssueNo() + "");

                                dbCon.save(con, "DELETE FROM SD_DEVICE_ISSUE_DTL WHERE ISSUE_NO = " + deviceissue_info.getIssueNo() + "");

                                String deviceDetailsColumn = "SEQ_NO,"
                                        + "ISSUE_NO,"
                                        + "IMEI_NO,"
                                        + "MODEL_NO,"
                                        + "STATUS,"
                                        + "USER_INSERTED,"
                                        + "DATE_INSERTED";

//                                ArrayList<DeviceIssueIme> TemdIme = new ArrayList<DeviceIssueIme>();
//
//                                ResultSet rs_master_modle = dbCon.search(con,"SELECT IMEI_NO,MODEL_NO "
//                                        + "FROM SD_DEVICE_MASTER");
//                                while (rs_master_modle.next()) {
//                                    DeviceIssueIme dii = new DeviceIssueIme();
//                                    dii.setImeiNo(rs_master_modle.getString("IMEI_NO"));
//                                    dii.setModleNo(rs_master_modle.getInt("MODEL_NO"));
//                                    TemdIme.add(dii);
//                                }
                                // rs_master_modle.close();
                                String deviceIsuueImeValue = "?,"
                                        + "?,"
                                        + "?,"
                                        + "?,"
                                        + "2,"
                                        + "?,"
                                        + "SYSDATE";

                                int max_device_dtl = 0;
                                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_DEVICE_ISSUE_DTL WHERE ISSUE_NO = " + deviceissue_info.getIssueNo() + "");

                                while (rs_max_dtl.next()) {
                                    max_device_dtl = rs_max_dtl.getInt("MAX");
                                }

                                ArrayList<DeviceIssueIme> deviceImeList = deviceissue_info.getDeviceImei();
                                if (deviceImeList.size() > 0) {

                                    PreparedStatement ps_up_device_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE_DTL(" + deviceDetailsColumn + ") "
                                            + "VALUES(" + deviceIsuueImeValue + ")");

                                    for (DeviceIssueIme us : deviceImeList) {

                                        max_device_dtl = max_device_dtl + 1;

                                        ps_up_device_dtl.setInt(1, max_device_dtl);
                                        ps_up_device_dtl.setInt(2, deviceissue_info.getIssueNo());
                                        ps_up_device_dtl.setString(3, us.getImeiNo());

//                                        for (DeviceIssueIme dime : TemdIme) {
//
//                                            if (dime.getImeiNo().equals(us.getImeiNo())) {
                                        ps_up_device_dtl.setInt(4, us.getModleNo());
//                                            }
//                                        }

                                        ps_up_device_dtl.setString(5, user_id);
                                        ps_up_device_dtl.addBatch();

                                    }
                                    ps_up_device_dtl.executeBatch();
                                    result = 1;
                                }

                            }

                        } catch (Exception e) {
                            logger.info("Error device Issue IMEI Update...:" + e.toString());
                            result = 9;
                        }
                    }
                }

            }

        } catch (Exception ex) {
            logger.info("Error editInventoryIssue Method..:" + ex.toString());
        }
        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, return_imei.length() - 1);
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Issue Successfully");
            logger.info("Device Issue Successfully.................");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI " + return_imei + " is not in Your List");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Already Issue  " + return_imei + " IMEI No in List");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("This  " + return_imei + " IMEI No have a Credit Note");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Device Issue Details Update");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper cancleInventoryIssue(DeviceIssue deviceissue_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {

        logger.info("cancleInventoryIssue Method Call.............................");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            dbCon.save(con, "UPDATE SD_DEVICE_ISSUE "
                    + "SET STATUS = 9,"
                    + "USER_MODIFIED = '" + user_id + "',"
                    + "DATE_MODIFIED = SYSDATE "
                    + "WHERE ISSUE_NO = " + deviceissue_info.getIssueNo() + "");

            PreparedStatement ps_up_device = dbCon.prepare(con, "UPDATE SD_DEVICE_ISSUE_DTL SET STATUS = 9,"
                    + "USER_MODIFIED = ?,"
                    + "DATE_MODIFIED = SYSDATE   "
                    + "WHERE ISSUE_NO = ?");

            ps_up_device.setString(1, user_id);
            ps_up_device.setInt(2, deviceissue_info.getIssueNo());
            ps_up_device.executeUpdate();

            logger.info("Device Issue Updated.....");
            result = 1;

        } catch (Exception ex) {
            logger.info("Error cancleInventoryIssue Method..:" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Issue Cancle Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Device Issue Cancle");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getUserModels(int bisId) {
        logger.info("getUserModels Method Call...........");
        ArrayList<InqModel> inqList = new ArrayList<InqModel>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String qry = "  SELECT  DISTINCT M.MODEL_NO,    "
                    + "         M.ERP_PART_NO,          "
                    + "         M.MODEL_DESCRIPTION,    "
                    + "        (SELECT COUNT(*) FROM SD_DEVICE_MASTER D1 WHERE D1.MODEL_NO=M.MODEL_NO AND D1.BIS_ID=" + bisId + ") IMEICNT,   "
                    //+ "        NVL(( SELECT SUM(M2.SELLING_PRICE) FROM SD_MODEL_LISTS M2, SD_DEVICE_MASTER D2 WHERE D2.MODEL_NO = M2.MODEL_NO AND D2.BIS_ID="+bisId+" AND M2.MODEL_NO=M.MODEL_NO ),0) SUMCOST "
                    + "         NVL(( SELECT SUM(M2.PRICE) FROM (SELECT M02.MODEL_NO, M02.PRICE FROM SD_DEVICE_ISSUE_DTL M02 WHERE ISSUE_NO = (SELECT MAX(ISSUE_NO) FROM SD_DEVICE_ISSUE M03 WHERE DSR_ID=" + bisId + ") AND ROWNUM=1) M2, SD_DEVICE_MASTER D2 WHERE D2.MODEL_NO = M2.MODEL_NO AND D2.BIS_ID=  " + bisId + "   AND M2.MODEL_NO=M.MODEL_NO ),0) SUMCOST "
                    + " FROM    SD_DEVICE_MASTER D, SD_MODEL_LISTS M    "
                    + " WHERE   D.MODEL_NO = M.MODEL_NO "
                    + " AND     D.BIS_ID=" + bisId + "    "
                    + " ORDER BY M.ERP_PART_NO  ";

            ResultSet rs = dbCon.search(con, qry);

            System.out.println("My Store Price List >>>>>> " + qry);

            while (rs.next()) {
                InqModel inq = new InqModel();
                inq.setModelNo(rs.getString("MODEL_NO"));
                inq.setErpNo(rs.getString("ERP_PART_NO"));
                inq.setModelDescription(rs.getString("MODEL_DESCRIPTION"));
                inq.setImeiCnt(rs.getString("IMEICNT"));

                int salesPrice = rs.getInt("SUMCOST");
                int realSalesPrice = (salesPrice * 90) / 100;
                inq.setSumCost(String.valueOf(realSalesPrice));

                inqList.add(inq);
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = inqList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(inqList);

        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getModelImeis(int bisId, int modelNo) {
        logger.info("getModelImeis Method Call...........");
        ArrayList<ImeiMaster> imList = new ArrayList<ImeiMaster>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String qry = "  SELECT  D.MODEL_NO, "
                    + "         D.IMEI_NO   "
                    + " FROM    SD_DEVICE_MASTER D  "
                    + " WHERE  D.BIS_ID=" + bisId + " "
                    + " AND    D.MODEL_NO=" + modelNo + "   "
                    + " ORDER BY D.IMEI_NO  ";

            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setImeiNo(rs.getString("IMEI_NO"));
                imList.add(im);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = imList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(imList);

        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getModelImeisForOffline(int bisId) {

        logger.info("getModelImeis offline Method Call...........");

        ArrayList<ImeiMaster> imList = new ArrayList<ImeiMaster>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String qry = "  SELECT  D.MODEL_NO, "
                    + "         D.IMEI_NO   "
                    + " FROM    SD_DEVICE_MASTER D  "
                    + " WHERE  D.BIS_ID=" + bisId + " "
                    + " ORDER BY D.IMEI_NO  ";

            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setImeiNo(rs.getString("IMEI_NO"));
                imList.add(im);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = imList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(imList);

        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public int checkImei(int bisId, String imei) {

        int flag = 0; // cmd = 0 ->  no problem, =1 -> not in their stock, =3 -> not in the db

//        boolean flag = false;
        try {
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            String qry = " SELECT COUNT(*) CNT  "
                    + " FROM SD_DEVICE_MASTER   "
                    + " WHERE BIS_ID=" + bisId + "    "
                    + " AND IMEI_NO='" + imei + "'   ";
//                    + " AND STATUS != 3 ";

            ResultSet rs = dbCon.search(con, qry);

            if (rs.next()) {
                if (rs.getInt("CNT") == 0) {

                    // search from the DB for invalid IMEI
                    String queryForInvalidation = " SELECT COUNT(*) CNT  "
                            + " FROM SD_DEVICE_MASTER   "
                            + " WHERE IMEI_NO='" + imei + "'   "
                            + " AND STATUS != 3 ";

                    ResultSet rs2 = dbCon.search(con, queryForInvalidation);

                    logger.info(queryForInvalidation);

                    if (rs2.next()) {
                        if (rs2.getInt("CNT") == 0) {
                            flag = 3; // not in the DB
                        } else {
                            flag = 1; // not avilable at stock
                        }
                    }

                } else {
                    flag = 0; // IMEI is avilable at their stock
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }
}
