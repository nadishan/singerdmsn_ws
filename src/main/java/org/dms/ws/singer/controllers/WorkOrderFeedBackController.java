/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.WorkOrderFeedBack;
import org.dms.ws.singer.utils.DBMapper;
import org.dms.ws.singer.utils.EmaiSendConfirmCode;
import org.dms.ws.singer.utils.EmailSendFeedBack;

/**
 *
 * @author SDU
 */
public class WorkOrderFeedBackController {

    public static MessageWrapper getWorkOrderFeedback(String workorderno) {

        logger.info("getWorkOrderFeedback Method Call....................");
        ArrayList<WorkOrderFeedBack> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.PRODUCT,"
                    + "M.MODEL_DESCRIPTION,"
                    + "F.FEEDBACK_ID,"
                    + "F.FEEDBACK,"
                    + "F.FEEDBACK_TYPE,"
                    + "(SELECT DESCRIPTION FROM SD_MDECODE WHERE DOMAIN_NAME = 'FEEDBACK_TYPE' AND  CODE =F.FEEDBACK_TYPE) FTYPE_DESC";

            String whereClous = "W.MODEL_NO = M.MODEL_NO (+) "
                    + "AND W.WORK_ORDER_NO = F.WORKORDER_NO (+)"
                    + "AND W.WORK_ORDER_NO = '" + workorderno + "'";

            String tables = "SD_WORK_ORDERS W,SD_MODEL_LISTS M,SD_WORK_ORDER_FEEDBACK F";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + "");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderFeedBack wo = new WorkOrderFeedBack();
                totalrecode = 1;
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                wo.setPrduct(rs.getString("PRODUCT"));
                wo.setModelName(rs.getString("MODEL_DESCRIPTION"));
                wo.setFeedBackId(rs.getInt("FEEDBACK_ID"));
                wo.setFeedBack(rs.getString("FEEDBACK"));
                wo.setFeedBackType(rs.getInt("FEEDBACK_TYPE"));
                wo.setFeedbackTypeDesc(rs.getString("FTYPE_DESC"));
                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderFeedback Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrderFeedbackList(int feedback_id,
            String complain_date,
            String work_order_no,
            String complain_type,
            String product,
            String modle_desc,
            String purhcse_shop,
            String date_purhchse,
            String service_center,
            String wo_open_date,
            String delevery_date,
            String warrenty,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getWorkOrderFeedbackList Method Call....................");
        ArrayList<WorkOrderFeedBack> workOrderFeedList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.workOrderFeedBack(order);

            String feedBackId = (feedback_id == 0 ? "" : "AND F.FEEDBACK_ID = '" + feedback_id + "'");
            String complainDate = (complain_date.equalsIgnoreCase("all") ? "NVL(F.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD')) = NVL(F.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD'))" : "TO_DATE(TO_CHAR(F.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + complain_date + "','YYYY/MM/DD')");
            String workOrderNo = (work_order_no.equalsIgnoreCase("all") ? "NVL(F.WORKORDER_NO,'AA') = NVL(F.WORKORDER_NO,'AA')" : "UPPER(F.WORKORDER_NO) LIKE UPPER('%" + work_order_no + "%')");
            String feedBackType = (complain_type.equalsIgnoreCase("all") ? "NVL(D.DESCRIPTION,'AA') = NVL(D.DESCRIPTION,'AA')" : "UPPER(D.DESCRIPTION) LIKE UPPER('%" + complain_type + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(W.PRODUCT,'AA') = NVL(W.PRODUCT,'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')");
            String modleDesc = (modle_desc.equalsIgnoreCase("all") ? "NVL(M.MODEL_DESCRIPTION,'AA') = NVL(M.MODEL_DESCRIPTION,'AA')" : "UPPER(M.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String registerDate = (date_purhchse.equalsIgnoreCase("all") ? "NVL(R.REGISTER_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(R.REGISTER_DATE,TO_DATE('20100101','YYYYMMDD'))" : "TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + date_purhchse + "','YYYY/MM/DD')");
            String purchesShop = (purhcse_shop.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + purhcse_shop + "%')");
            String serviceCenter = (service_center.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + service_center + "%')");
            String woOpenDate = (wo_open_date.equalsIgnoreCase("all") ? "NVL(W.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD'))" : "TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + wo_open_date + "','YYYY/MM/DD')");
            String deleveryDate = (delevery_date.equalsIgnoreCase("all") ? "NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "TO_DATE(TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + delevery_date + "','YYYY/MM/DD')");
            String Warranty = (warrenty.equalsIgnoreCase("all") ? " " : " AND UPPER(D1.DESCRIPTION) LIKE UPPER('%" + warrenty + "%')");
            String Status = (status == 0 ? "" : "AND W.STATUS = " + status + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY F.FEEDBACK_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "FEEDBACK_ID,"
                    + "F.WORKORDER_NO,"
                    + "TO_CHAR(F.DATE_INSERTED,'YYYY/MM/DD') COMPLAIN_DATE,"
                    + "D.DESCRIPTION,"
                    + "W.PRODUCT,"
                    + "M.MODEL_DESCRIPTION,"
                    + "TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE,"
                    + "B.BIS_NAME,"
                    + "B1.BIS_NAME SERVICE_CENTER,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_OPEN_DATE,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.STATUS,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "D1.DESCRIPTION WARANTY,"
                    + "W.RCC_REFERENCE,"
                    + "F.FEEDBACK,F.FEEDBACK_TYPE,"
                    + "F.REPLY_FLAG";

            String tables = "SD_WORK_ORDERS W,SD_WORK_ORDER_FEEDBACK F,"
                    + "SD_MDECODE D,SD_MODEL_LISTS M,"
                    + "SD_WARRENTY_REGISTRATION R,SD_BUSINESS_STRUCTURES B,"
                    + "SD_BUSINESS_STRUCTURES B1,SD_MDECODE D1";

            String whereClous = "F.WORKORDER_NO = W.WORK_ORDER_NO (+) "
                    + "AND F.FEEDBACK_TYPE = D.CODE (+) "
                    + "AND D.DOMAIN_NAME = 'FEEDBACK_TYPE' "
                    + "AND W.MODEL_NO = M.MODEL_NO (+) "
                    + "AND W.IMEI_NO = R.IMEI_NO (+) "
                    + "AND R.BIS_ID  = B.BIS_ID (+) "
                    + "AND W.BIS_ID  = B1.BIS_ID (+) "
                    + "AND W.WARRANTY_VRIF_TYPE = D1.CODE (+) "
                    + "AND D1.DOMAIN_NAME = 'WARANTY' "
                    + "" + feedBackId + " "
                    + "AND " + complainDate + " "
                    + "AND " + workOrderNo + " "
                    + "AND " + feedBackType + " "
                    + "AND " + Product + " "
                    + "AND " + modleDesc + " "
                    + "AND " + registerDate + " "
                    + "AND " + purchesShop + " "
                    + "AND " + serviceCenter + " "
                    + "AND " + woOpenDate + " "
                    + "AND " + deleveryDate + " "
                    + Warranty
                    + " " + Status + " ";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + tables + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrderFeedBack wo = new WorkOrderFeedBack();
                totalrecode = rs.getInt("CNT");
                wo.setFeedBackId(rs.getInt("FEEDBACK_ID"));
                wo.setWorkOrderNo(rs.getString("WORKORDER_NO"));
                wo.setComplainDate(rs.getString("COMPLAIN_DATE"));
                wo.setFeedbackTypeDesc(rs.getString("DESCRIPTION"));
                wo.setPrduct(rs.getString("PRODUCT"));
                wo.setModelName(rs.getString("MODEL_DESCRIPTION"));
                wo.setPurchaseDate(rs.getString("REGISTER_DATE"));
                wo.setPurchaseShop(rs.getString("BIS_NAME"));
                wo.setServiceCenter(rs.getString("SERVICE_CENTER"));
                wo.setWoOpenDate(rs.getString("WO_OPEN_DATE"));
                wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wo.setStatus(rs.getInt("STATUS"));
                wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                wo.setAddress(rs.getString("CUSTOMER_ADDRESS"));
                wo.setTelephoneNo(rs.getString("TELEPHONE_NO"));
                wo.setEmail(rs.getString("EMAIL"));
                wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                wo.setImeiNo(rs.getString("IMEI_NO"));
                wo.setWarrenty(rs.getString("WARANTY"));
                wo.setRccRefe(rs.getString("RCC_REFERENCE"));
                wo.setFeedBack(rs.getString("FEEDBACK"));
                wo.setFeedBackType(rs.getInt("FEEDBACK_TYPE"));
                if (rs.getInt("STATUS") == 1) {
                    wo.setStatusMsg("Open");
                } else if (rs.getInt("STATUS") == 2) {
                    wo.setStatusMsg("In Progress");
                } else if (rs.getInt("STATUS") == 3) {
                    wo.setStatusMsg("Close");
                } else {
                    wo.setStatusMsg("None");
                }
                if (rs.getInt("REPLY_FLAG") == 1) {
                    wo.setReplyStatus("Yes.");
                }else if (rs.getInt("REPLY_FLAG") == 3) {
                    wo.setReplyStatus("Serice Center Reply.");
                } else {
                    wo.setReplyStatus("No");
                }

                workOrderFeedList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderFeedList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper replyWorkOrderFeedBack(WorkOrderFeedBack wf_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("replyWorkOrderFeedBack Mehtod Call.......");
        int result = 0;
        try {

            EmailSendFeedBack.sendHTMLMail(wf_info.getEmail(), wf_info.getCustomerName(), wf_info.getWoReply());
//
            PreparedStatement ps_up_wofeed = dbCon.prepare(con, "UPDATE SD_WORK_ORDER_FEEDBACK "
                    + "SET REPLY_FLAG = 1 "
                    + "WHERE FEEDBACK_ID = ?");

            ps_up_wofeed.setInt(1, wf_info.getFeedBackId());
            ps_up_wofeed.executeUpdate();
            
            int max_feedid = 0;
            
            ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(FEEDBACK_ID),0) MAX FROM SD_WORK_ORDER_FEEDBACK");

            while (rs_max.next()) {
                max_feedid = rs_max.getInt("MAX");
            }

            max_feedid = max_feedid + 1;

            PreparedStatement ps_in_wofb = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_FEEDBACK(FEEDBACK_ID,"
                    + "WORKORDER_NO,"
                    + "FEEDBACK_TYPE,"
                    + "FEEDBACK,"
                    + "STATUS,"
                    + "USER_INSERTED,"
                    + "DATE_INSERTED,"
                    + "REPLY_FLAG) "
                    + "VALUES(?,?,?,?,1,?,SYSDATE,3)");

            ps_in_wofb.setInt(1, max_feedid);
            ps_in_wofb.setString(2, wf_info.getWorkOrderNo());
            ps_in_wofb.setInt(3, wf_info.getFeedBackType());
            ps_in_wofb.setString(4, wf_info.getWoReply());
            ps_in_wofb.setString(5, wf_info.getUser());
            
            ps_in_wofb.executeUpdate();

            result = 1;
        } catch (Exception ex) {
            logger.info("Error in replyWorkOrderFeedBack Mehtod  :" + ex.toString());
            result = 9;
        }finally{
            dbCon.ConectionClose(con);
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Email Sent Successfully");
            logger.info("Email Sent Successfully..........");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Email Sentt");
            logger.info("Error in Email Sent.............");
        }
        logger.info("Ready to Return Values..........");
        logger.info("-----------------------------------------------------------");
        return vw;
    }

}
