/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.DeviceIssue;

/**
 *
 * @author dilhan
 */
public class DsrAllowOfflineController {

    public static ValidationWrapper editDsrAllowOffline(int bisid, int statusOfline) throws Exception {
        logger.info("editDsrAllowOffline method Call........");
        int result = 0;
        int active_child = 0;
        int parent_inactive = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

//            if (busstruinfo.getStatus() == 2) {
//                ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
//                        + "FROM SD_BUSINESS_STRUCTURES "
//                        + "WHERE PARENT_BIS_ID =  " + busstruinfo.getBisId() + ""
//                        + "AND STATUS = 1");
//                while (rs_cnt.next()) {
//                    active_child = rs_cnt.getInt("CNT");
//                }
//
//            }
//
//            if (busstruinfo.getStatus() == 1) {
//                ResultSet rs_parant_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
//                        + "FROM SD_BUSINESS_STRUCTURES  "
//                        + "WHERE BIS_ID = (SELECT PARENT_BIS_ID "
//                        + "FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID = " + busstruinfo.getBisId() + ") "
//                        + "AND STATUS = 2");
//                while (rs_parant_cnt.next()) {
//                    parent_inactive = rs_parant_cnt.getInt("CNT");
//                }
//
//            }
//
//            if (active_child > 0) {
//                result = 2;
//            } else {
//                if (parent_inactive > 0) {
//                    result = 3;
//                } else {
            System.out.println("UPDATE SD_BUSINESS_STRUCTURES SET ALLOW_OFFLINE='" + statusOfline + "' WHERE BIS_ID=" + bisid);
            PreparedStatement ps_up_bs = dbCon.prepare(con, "UPDATE SD_BUSINESS_STRUCTURES SET ALLOW_OFFLINE='" + statusOfline + "' WHERE BIS_ID=" + bisid + "");
//        PreparedStatement ps_up_bs = dbCon.prepare(con, "UPDATE SD_BUSINESS_STRUCTURES "
//                + "SET ALLOW_OFFLINE= '" + statusOfline + "' WHERE BIS_ID =  "
//                + "' " + bisId + " '");

            logger.info("editDsrAllowOffline method Call........" + bisid + "statusOfline" + statusOfline);
            ps_up_bs.executeUpdate();

            result = 1;
//                }
//            }
//
        } catch (Exception ex) {
            logger.info("Error editDsrAllowOffline method..:" + ex.toString());
//            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Business Structure AllowOffline Update Successfully.");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Active Sub Business Structures in this Business Level");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Parent Level Inactive");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Business Structure AllowOffline Update.");
        }
        dbCon.ConectionClose(con);
        logger.info("Redy to return values............");
        logger.info("-----------------------------------------");
        return vw;
    }

    public static ValidationWrapper getDsrAllowOfflineStatus(int bisid, int statusOfline) throws Exception {
        logger.info("editDsrAllowOffline method Call........");
        int result = 0;
        int active_child = 0;
        int parent_inactive = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        
        int status = 0;
        
        try {
            
           // PreparedStatement pr_gt_bs = dbCon.prepare(con, "SELECT * FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID=" + bisid + "");
           // logger.info("getDsrAllowOffline method Call........" + bisid + "statusOfline" + statusOfline);
          // pr_gt_bs.executeUpdate();
            System.out.println("SELECT * FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID=" + bisid + "");
            ResultSet rs = dbCon.search(con, "SELECT * FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID=" + bisid + "");
            result = 1;
           
            while(rs.next()){
//            BussinessStructure bs = new BussinessStructure();
//            bs.setStatusOfline(rs.getInt("statusOfline"));
            status = rs.getInt("ALLOW_OFFLINE");
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error editDsrAllowOffline method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if(result == 1)
        {
            vw.setReturnFlag(String.valueOf(status));
            vw.setReturnMsg("Business Structure AllowOffline Get Successfully.");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Business Structure AllowOffline Get.");
        }
        dbCon.ConectionClose(con);
        return vw;
    }
}
