/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.map.ObjectMapper;
import org.dms.ldap.util.LdapGroup;
import org.dms.ldap.util.LdapUser;
import org.dms.ldap.util.LoginWrapper;
import org.dms.ldap.util.ResetUserResponseWrapper;
import org.dms.ldap.util.ResponseWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Group;
import org.dms.ws.singer.entities.LdapUserEnty;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.utils.AppParams;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

/**
 *
 * @author hashan
 */
public class LDAPWSController {

//    final static String host = "http://192.0.0.222";
//    final static String port = "8080";
//    final static String appPath = "/DMSSecurityPortal";
//    final static String contextPath = "/Auth";
//
//    public final static String wsURI = host + ":" + port + appPath + contextPath;
    public final static String wsURI = AppParams.LDAP_WS_URL; //LdapPath.getLdapConfiguration();

    public static ResponseWrapper getAllUsers(String status, String start, String limit, String key, String value, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        String uri = wsURI + "/UserMaintenance/GetAllUsers/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "?Status=" + status + "&Start=" + start + "&Limit=" + limit + "&Key=" + key + "&Value=" + value;

        String returnResponse = getRequestToURI(uri, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;
        try {
            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            //System.out.println(responseObject.getTotalRecords());
            ArrayList<LdapUser> users = (ArrayList<LdapUser>) responseObject.getData();

            // System.out.println(responseObject.getData());
            //UserResponseWrapper us = new LdapUser();
            // System.out.println(responseObject.getData().get(0).getFirstName());
            // us.setUserId(users);
        } catch (IOException ex) {
            logger.info("Error in LDAPWSController-getAllUsers  :" + ex.getMessage());
        }
        return responseObject;
    }

    public static String getRequestToURI(String webServiceURI,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {

        String output = null;
        try {
            ClientRequest request = new ClientRequest(webServiceURI);

            logger.info("Web Service URI : " + request.getUri() + "");

            logger.info("Request Param---------------------");

            //System.out.println("\n");
            request.header("LoggedInDN", "uid=" + user_id + ",cn=" + room + ",ou=" + department + ",ou=" + branch + ",c=" + countryCode + ",ou=" + division + ",o=" + organaization + ",ou=Organizations,dc=dmssw,dc=com");
            request.header("SystemId", system);

            output = printResponse(request.get(String.class));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public static String getRequestToURIwithoutHeder(String webServiceURI) {

        String output = null;
        try {
            ClientRequest request = new ClientRequest(webServiceURI);

            logger.info("Web Service URI : " + request.getUri() + "");

            logger.info("Request Param---------------------");

            //System.out.println("\n");
            //  request.header("LoggedInDN", "uid="+user_id+",cn="+room+",ou="+department+",ou="+branch+",c="+countryCode+",ou="+division+",o="+organaization+",ou=Organizations,dc=dmssw,dc=com");
            //  request.header("SystemId", "SingerISS");
            output = printResponse(request.get(String.class));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

//    public static String deleteRequestToURI(String webServiceURI) {
//        String output = null;
//        try {
//            ClientRequest request = new ClientRequest(webServiceURI);
//
//            System.out.println("Web Service URI : " + request.getUri() + "\n");
//
//            System.out.println("Request Param---------------------\n");
//
//            System.out.println("\n");
//
//            request.header("LoggedInDN", "uid=admin,cn=DefaultRoom,ou=DefaultDepartment,ou=HeadOffice,c=LK,ou=SingerLK,o=Singer,ou=Organizations,dc=dmssw,dc=com");
//            request.header("SystemId", "SingerISS");
//
//            output = printResponse(request.delete(String.class));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return output;
//    }
    public static String postRequestToURI(String webServiceURI,
            String acceptType,
            Object anyObj,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {
        String output = null;
        try {
            ClientRequest request = new ClientRequest(webServiceURI);
            request.accept(acceptType);
            logger.info("Web Service URI : " + request.getUri() + "");

            logger.info("Request Param---------------------");

            ObjectMapper mapper = new ObjectMapper();

            // logger.info(mapper.writeValueAsString(anyObj));
            // System.out.println("\n");
            request.body(acceptType, mapper.writeValueAsString(anyObj));

            request.header("LoggedInDN", "uid=" + user_id + ",cn=" + room + ",ou=" + department + ",ou=" + branch + ",c=" + countryCode + ",ou=" + division + ",o=" + organaization + ",ou=Organizations,dc=dmssw,dc=com");
            request.header("SystemId", system);

            output = printResponse(request.post(String.class));

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return output;
    }

    public static String postRequestToURIwithoutHeder(String webServiceURI,
            String acceptType,
            Object anyObj,
            String system) {
        String output = null;
        try {
            ClientRequest request = new ClientRequest(webServiceURI);
            request.accept(acceptType);
            logger.info("Web Service URI : " + request.getUri() + "");

            logger.info("Request Param---------------------");

            ObjectMapper mapper = new ObjectMapper();

            // logger.info(mapper.writeValueAsString(anyObj));
            // System.out.println("\n");
            request.body(acceptType, mapper.writeValueAsString(anyObj));

            request.header("LoggedInDN", "uid=admin,cn=DefaultRoom,ou=DefaultDepartment,ou=HeadOffice,c=LK,ou=SingerLK,o=Singer,ou=Organizations,dc=dmssw,dc=com");
            request.header("SystemId", system);

            output = printResponse(request.post(String.class));

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return output;
    }

    public static String putRequestToURI(String webServiceURI,
            String acceptType,
            Object anyObj,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String output = null;
        try {
            ClientRequest request = new ClientRequest(webServiceURI);
            request.accept(acceptType);
            logger.info("Web Service URI : " + request.getUri() + "");

            logger.info("Request Param---------------------");

            ObjectMapper mapper = new ObjectMapper();

            logger.info(mapper.writeValueAsString(anyObj));
            //System.out.println("\n");

            request.body(acceptType, mapper.writeValueAsString(anyObj));

            request.header("LoggedInDN", "uid=" + user_id + ",cn=" + room + ",ou=" + department + ",ou=" + branch + ",c=" + countryCode + ",ou=" + division + ",o=" + organaization + ",ou=Organizations,dc=dmssw,dc=com");
            request.header("SystemId", system);

            output = printResponse(request.put(String.class));

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return output;
    }

    public static String printResponse(ClientResponse<String> response) {
        String data = null;
        try {
            if (response.getStatus() != 201 && response.getStatus() != 200) {
                logger.info("Failed : HTTP error code : "
                        + response.getStatus());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(response.getEntity().getBytes())));

            logger.info("Response from Server-------------------------");
            String output = "";
            StringBuilder sb = new StringBuilder();
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                sb.append(output);
            }
            data = sb.toString();
//            logger.info(data);
        } catch (ClientProtocolException e) {

            logger.info(e.getMessage());

        } catch (IOException e) {

            logger.info(e.getMessage());

        } catch (Exception e) {

            logger.info(e.getMessage());
        }

        return data;
    }

    public static void addUser(User u,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String uri = wsURI + "/UserMaintenance/AddUser/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";

        putRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);

    }

    public static ResponseWrapper modifyUserWithGroups(Object u,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String uri = wsURI + "/UserMaintenance/ModifyUserWithGroups/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = postRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper addUserWithGroups(Object u,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String uri = wsURI + "/UserMaintenance/AddUserWithGroups/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = putRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-addUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper getAllUserGroups(String uid,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String uri = wsURI + "/UserMaintenance/GetAllUserGroups/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "/" + uid;
        String returnResponse = getRequestToURI(uri, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;
        try {
            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);

            ArrayList<LdapUser> users = (ArrayList<LdapUser>) responseObject.getData();

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return responseObject;

    }

    public static ResponseWrapper modifyUser(Object u,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String uri = wsURI + "/UserMaintenance/ModifyUser/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = postRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper generateResetPassowrd(String userId,
            String email,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        User u = new User();
        u.setUserId(userId);
        u.setEmail(email);

        String uri = wsURI + "/UserMaintenance/ResetPassword/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = postRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper login(String uid,
            String password, String system) {

        User u = new User();
        u.setUserId(uid);
        u.setPassword(password);

        String uri = wsURI + "/AuthService/AuthLogin";
        String returnResponse = postRequestToURIwithoutHeder(uri, "application/json", u, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }
    
    public static LoginWrapper loginAndroid(String uid,
            String password, String system) {

        User u = new User();
        u.setUserId(uid);
        u.setPassword(password);

        String uri = wsURI + "/AuthService/AuthLogin";
        String returnResponse = postRequestToURIwithoutHeder(uri, "application/json", u, system);
        ObjectMapper oMapper = new ObjectMapper();
        LoginWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, LoginWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper changeUserPassowrd(Object u,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {

        String uri = wsURI + "/UserMaintenance/ChangePassword/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = postRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper getAllGroupsInSystem(String start,
            String limit,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {

        String uri = wsURI + "/SystemMaintenance/GetAllGroupsInSystem/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "?Start=" + start + "&Limit=" + limit;
        String returnResponse = getRequestToURI(uri, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;
        try {
            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return responseObject;

    }

    public static ResponseWrapper logout(String uid,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        User u = new User();
        u.setUserId(uid);

        String uri = wsURI + "/AuthService/AuthLogout/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = postRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyUserWithGroups :" + ex.getMessage());
        }

        return responseObject;

    }

    public static ResponseWrapper getUserGroups(String uid,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {

        String uri = wsURI + "/UserMaintenance/GetAllUserGroups/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "?uid=" + uid + "&Start=all&Limit=all";
        String returnResponse = getRequestToURI(uri, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;
        try {
            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<LdapUser> users = (ArrayList<LdapUser>) responseObject.getData();

        } catch (IOException ex) {
            logger.info("Error in LDAPWSController-getAllUsers  :" + ex.getMessage());
        }
        return responseObject;

    }

    public static ResponseWrapper modifyMultipleUsers(ArrayList<User> u,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        String uri = wsURI + "/UserMaintenance/ModifyMultipleUsers/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "";
        String returnResponse = postRequestToURI(uri, "application/json", u, user_id, room, department, branch, countryCode, division, organaization, system);
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;

        try {

            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            ArrayList<String> successMessages = (ArrayList<String>) responseObject.getSuccessMessages();

        } catch (Exception ex) {
            logger.info("Error in LDAPWSController-modifyMultipleUsers :" + ex.getMessage());
        }

        return responseObject;

    }

//    public static void main(String[] args) {
//        //addUserWithGroups();
//        //addUser();
//        //modifyUser();
//        //modifyUserWithGroups();
//        //changeUserState();
//        //changeUserStatePost();
//        //generateResetPassowrd();
//        //changeUserPassowrd("y5h5%Nj2Tm","26538mouse");
//        getAllUsers("1");
//        //getAllUserGroups("hasha41");
//
//    }
    public static ResponseWrapper getAllUsers123(String status, String start, String limit, HashMap<String, String> searchParams, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        String uri = wsURI + "/UserMaintenance/GetAllUsers/" + organaization + "/" + division + "/" + countryCode + "/" + branch + "/" + department + "/" + room + "/" + status + "/" + start + "/" + limit + "/all/all";

        
        System.out.println("-------"+uri+"---------------");
        
        String returnResponse = postRequestToURI(uri, "application/json", searchParams, user_id, room, department, branch, countryCode, division, organaization, system);
        
        ObjectMapper oMapper = new ObjectMapper();
        ResponseWrapper responseObject = null;
        try {
            
            responseObject = oMapper.readValue(returnResponse, ResponseWrapper.class);
            //System.out.println(responseObject.getTotalRecords());
            ArrayList<LdapUser> users = (ArrayList<LdapUser>) responseObject.getData();

            // System.out.println(responseObject.getData());
            //UserResponseWrapper us = new LdapUser();
            // System.out.println(responseObject.getData().get(0).getFirstName());
            // us.setUserId(users);
        } catch (IOException ex) {
            logger.info("Error in LDAPWSController-getAllUsers  :" + ex.getMessage());
        }
        return responseObject;
    }
}
