/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BussinessType;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class BussinessTypeController {
    
    
    
     public static MessageWrapper getBussinessType(int  type_id,
            String type_desc,
            int entry_mode,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getBussinessType Method Call....................");
        ArrayList<BussinessType> bussinessTypeList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.bussinessType(order);

            String typeId = (type_id == 0 ? "" : "AND BIS_STRU_TYPE_ID = "+type_id+"");
            String typeDesc = (type_desc.equalsIgnoreCase("all") ? "NVL(BIS_STRU_TYPE_DESC,'AA') = NVL(BIS_STRU_TYPE_DESC,'AA')" : "UPPER(BIS_STRU_TYPE_DESC) LIKE UPPER('%"+type_desc+"%')");
            String entryMode = (type_id == 0 ? "" : "AND ENTRY_MODE = "+entry_mode+"");
            String Status = (type_id == 0 ? "" : "AND STATUS = "+status+"");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY BIS_STRU_TYPE_ID" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "BIS_STRU_TYPE_ID,"
                    + "BIS_STRU_TYPE_DESC,"
                    + "ENTRY_MODE,"
                    + "STATUS";

            String whereClous = ""+typeDesc+" "
                    + ""+typeId+" "
                    + ""+entryMode+" "
                    + ""+Status+"";

            
            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_BIS_STRU_TYPES WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_BIS_STRU_TYPES WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                BussinessType bt = new BussinessType();
                totalrecode = rs.getInt("CNT");
                bt.setBisTypeId(rs.getInt("BIS_STRU_TYPE_ID"));
                bt.setBisTypeDesc(rs.getString("BIS_STRU_TYPE_DESC"));
                bt.setEntyMode(rs.getInt("ENTRY_MODE"));
                bt.setStatus(rs.getInt("STATUS"));
                bussinessTypeList.add(bt);
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessType Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(bussinessTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
    
}
