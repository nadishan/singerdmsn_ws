/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Part;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.entities.WorkOrderDefect;
import org.dms.ws.singer.entities.WorkOrderEstimate;
import org.dms.ws.singer.entities.WorkOrderInquire;
import org.dms.ws.singer.entities.WorkOrderMaintain;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import org.dms.ws.singer.entities.WorkOrderStatusInquiry;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WorkOrderInquireController {

    public static MessageWrapper getWorkOrderInquire(String workorder_no,
            String imei,
            String nic,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getWorkOrderInquire Method Call....................");
        ArrayList<WorkOrderInquire> workOrderInquireList = new ArrayList<>();
        int totalrecode = 0;

        try {

            String workOrderNo = (workorder_no.equalsIgnoreCase("all") ? "NVL(W.WORK_ORDER_NO,'AA') = NVL(W.WORK_ORDER_NO,'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workorder_no + "%')");
            String ImieNo = (imei.equalsIgnoreCase("all") ? "NVL(W.IMEI_NO,'AA') = NVL(W.IMEI_NO,'AA')" : "UPPER(W.IMEI_NO) LIKE UPPER('%" + imei + "%')");
            String CusNIC = (nic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + nic + "%')");
            
            
            
            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.STATUS,"
                    + "W.PRODUCT,"
                    + "M.MODEL_DESCRIPTION,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') DATE_INSERTED,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "E.EST_REF_NO,"
                    + "E.STATUS ES_STATUS,(E.COST_FOR_SPARE_PARTS+e.COST_FOR_LABOUR) TOTAL_COST";
                    // Changes - Dilip
//                    + "W.ACTION_TAKEN,"
//                    + "E.EST_REF_NO as INVOICE_NO,"
//                    + "W.REMARKS";

            String whereClous = "W.WORK_ORDER_NO = E.WORK_ORDER_NO (+) "
                    + "AND W.MODEL_NO = M.MODEL_NO (+) "
                    + "AND " + workOrderNo + " "
                    + "AND "+ImieNo+" "
                    + "AND "+CusNIC+"";

            String table = "SD_WORK_ORDERS W,SD_WORK_ORDER_ESTIMATE E, SD_MODEL_LISTS M";

            
            
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE  " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( ORDER BY W.WORK_ORDER_NO DESC ) RN "
                    + " FROM " + table + "  WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                logger.info("INSDIE DATA >>>>> ");
                WorkOrderInquire wr = new WorkOrderInquire();
                totalrecode = rs.getInt("CNT");
                wr.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                if (rs.getInt("STATUS") == 1) {
                    wr.setWoStatus("Pending");
                } else if (rs.getInt("STATUS") == 2) {
                    wr.setWoStatus("In Progress");
                } else if (rs.getInt("STATUS") == 3) {
                    wr.setWoStatus("Complete");
                } else {
                    wr.setWoStatus("None");
                }
                wr.setProduct(rs.getString("PRODUCT"));
                wr.setModle(rs.getString("MODEL_DESCRIPTION"));
                wr.setOpenDate(rs.getString("DATE_INSERTED"));
                wr.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                wr.setEstimateNo(rs.getString("EST_REF_NO"));
                if (rs.getInt("ES_STATUS") == 1) {
                    wr.setEstimateStatus("Pending");
                } else if (rs.getInt("ES_STATUS") == 2) {
                    wr.setEstimateStatus("Normal");
                } else if (rs.getInt("ES_STATUS") == 3) {
                    wr.setEstimateStatus("Closed");
                } else {
                    wr.setEstimateStatus("None");
                }
                wr.setEstimateCost(rs.getString("TOTAL_COST"));

//                //Changes - Dilip
//                
//                wr.setInvoiceNo(rs.getString("INVOICE_NO"));
//                wr.setActionTaken(rs.getString("ACTION_TAKEN"));
//                wr.setRemarks(rs.getString("REMARKS"));
//                logger.info("New Paras>>>>> act /"+wr.getActionTaken()+ "/in/"+ wr.getInvoiceNo()+"/re/"+wr.getRemarks() );
                

                workOrderInquireList.add(wr);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderInquire Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderInquireList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

public static MessageWrapper getWorkOrderStatusInquiryListLoad(String wono, String custonic, String imei, String rccref, String telephoneno,String customername,String shopname) {
//public static MessageWrapper getWorkOrderStatusInquiryList(String wono, String custonic, String imei, String rccref,String ) {
        logger.info("getWorkOrderStatusInquiryList method Call..........");
        ArrayList<WorkOrderStatusInquiry> workOrderStatusInquiryList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        String workOrderNo = null;
        try {

            String woNo = (wono.equalsIgnoreCase("all") ? "NVL(WORK_ORDER_NO,'AA') = NVL(WORK_ORDER_NO,'AA')" : "UPPER(WORK_ORDER_NO) LIKE UPPER('%" + wono + "%')");
            String cusNic = (custonic.equalsIgnoreCase("all") ? "NVL(CUSTOMER_NIC,'AA') = NVL(CUSTOMER_NIC,'AA')" : "UPPER(CUSTOMER_NIC) LIKE UPPER('%" + custonic + "%')");
            String imieiNo = (imei.equalsIgnoreCase("all") ? "NVL(IMEI_NO,'AA') = NVL(IMEI_NO,'AA')" : "UPPER(IMEI_NO) LIKE UPPER('%" + imei + "%')");
            String rccRef = (rccref.equalsIgnoreCase("all") ? "NVL(RCC_REFERENCE,'AA') = NVL(RCC_REFERENCE,'AA')" : "UPPER(RCC_REFERENCE) LIKE UPPER('%" + rccref + "%')");
            //new
            String telephoneNo = (telephoneno.equalsIgnoreCase("all") ? "NVL(TELEPHONE_NO,'AA') = NVL(TELEPHONE_NO,'AA')" : "UPPER(TELEPHONE_NO) LIKE UPPER('%" + telephoneno + "%')");
            String customerName = (customername.equalsIgnoreCase("all") ? "NVL(CUSTOMER_NAME,'AA') = NVL(CUSTOMER_NAME,'AA')" : "UPPER(CUSTOMER_NAME) LIKE UPPER('%" + customername + "%')");
            String shopName = (shopname.equalsIgnoreCase("all") ? "NVL(WM.TECHNICIAN,'AA') = NVL(WM.TECHNICIAN,'AA')" : "UPPER(WM.TECHNICIAN) LIKE UPPER('%" + shopname + "%')");
            
            String workOrderNo1 = (wono.equalsIgnoreCase("all") ? "NVL(WORK_ORDER_NO,'AA') = NVL(WORK_ORDER_NO,'AA')" : "UPPER(WORK_ORDER_NO) LIKE UPPER('%" + wono + "%')");
            String workOrderNo2 = (wono.equalsIgnoreCase("all") ? "NVL(WO.WORK_ORDER_NO,'AA') = NVL(WO.WORK_ORDER_NO,'AA')" : "UPPER(WO.WORK_ORDER_NO) LIKE UPPER('%" + wono + "%')");
            
            
            
            String gwtWONumberwhere = "" + woNo + " "
                    + "AND " + cusNic + " "
                    + "AND " + imieiNo + " "
                    + "AND " + rccRef + " "
                    + "AND " + telephoneNo + " "
                    + "AND " + customerName + " "
                    + "AND " + shopName + " ";
                    

//            String getWorkOrderNoQry = "SELECT  WORK_ORDER_NO   "
//                    + "FROM SD_WORK_ORDERS       "
//                    + "WHERE    " + gwtWONumberwhere + "   ORDER BY WORK_ORDER_NO ASC";
//            
//            System.out.println("getWorkOrderNoQry "+getWorkOrderNoQry);
//                   
//            ResultSet rs_getWO = dbCon.search(con, getWorkOrderNoQry);
//
//            if (rs_getWO.next()) {
//                workOrderNo = rs_getWO.getString("WORK_ORDER_NO");
//            }

//            if (workOrderNo != null) {

            
                String selectColumn = "WO.WORK_ORDER_NO, "
                        + "        WO.CUSTOMER_NAME, "
                        + "        WO.CUSTOMER_ADDRESS, "
                        + "        WO.CUSTOMER_NIC, "
                        + "        WO.TELEPHONE_NO,"
                        + "        WO.RCC_REFERENCE, "
                        + "        WO.EMAIL, "
                        + "        WO.IMEI_NO, "
                        + "        WO.PRODUCT, "
                        + "        WO.BRAND, "
                        + "        WO.DEFECTS,"
                        + "        WO.ACCESSORIES_RETAINED,"
                        + "        WO.RCC_REFERENCE,"
                    //    + "        WO.REMARKS,"
                        + "        WO.NON_WARRANTY_VRIF_TYPE,"
                        + "        WO.NON_WARRANTY_REMARKS,"
                        + "        WO.CUSTOMER_COMPLAIN,"
                    //    + "        WM.INVOICE_NO,"
                        + "        WM.REMARKS AS REMARKS_TWO,"
                        + "        WM.EST_CLOSING_DATE,"
                    //    + "        WO.ACTION_TAKEN, "
                        + "        TO_CHAR(WO.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE, "
                        + "        MD.DESCRIPTION AS WARRANTY_TYPE,"
                        + "        ML.MODEL_DESCRIPTION, "
                        + "        WM.TECHNICIAN, "
                        + "        MD1.DESCRIPTION AS DELIVERY_TYPE,"
                        + "          ( SELECT  B.BIS_NAME "
                        + "           FROM   SD_WORK_ORDER_TRANSFERS T,SD_BUSINESS_STRUCTURES B"
                        + "           WHERE WORK_ORD_TRNS_NO IN (SELECT MAX(WORK_ORD_TRNS_NO)"
                        + "           FROM SD_WORK_ORDER_TRANSFERS )"
                        + "          AND T.BIS_ID = B.BIS_ID (+)) AS TRANSFER_LOCATION,"
                        + "          TO_CHAR(WO.DELIVERY_DATE, 'YYYY/MM/DD') AS DELIVERY_DATE,"
                        + "          TO_CHAR(WM.EST_CLOSING_DATE, 'YYYY/MM/DD') AS EST_CLOSING_DATE,"
                        + "          WO.NON_WARRANTY_VRIF_TYPE,"
                        + "          WO.NON_WARRANTY_REMARKS,"
                        + "          TO_CHAR(WO.DATE_INSERTED,'YYYY/MM/DD') WO_DATE"
                        // --> Changes - Dilip
                        + "     WO.ACTION_TAKEN,"
                        + "     E.EST_REF_NO as INVOICE_NO,"
                        + "     WO.REMARKS";
                        // <--    
                            
                String fromClous = "SD_WORK_ORDERS WO, SD_MDECODE MD, SD_WORK_ORDER_MAINTAIN WM,SD_MODEL_LISTS ML,SD_MDECODE MD1, SD_WORK_ORDER_ESTIMATE E";

                String whereClous = "NVL(WO.WORK_ORDER_NO,'AA')= NVL(WO.WORK_ORDER_NO,'AA') " // " WO.WORK_ORDER_NO='" + workOrderNo + "' "
                        + " AND     MD.SEQUENCE_NO=WO.WARRANTY_VRIF_TYPE "
                        + " AND     MD.DOMAIN_NAME='WARANTY' "
                        + " AND     WO.WORK_ORDER_NO=WM.WORK_ORDER_NO (+) "
                        + " AND     WO.MODEL_NO  = ML.MODEL_NO (+) "
                        + " AND     MD1.SEQUENCE_NO=WO.DELIVERY_TYPE "
                        + " AND     MD1.DOMAIN_NAME='DELEVERY_TYPE' "
                        + " AND     NVL(WM.TECHNICIAN,'AA') = NVL(WM.TECHNICIAN,'AA') "
                        + " AND "+workOrderNo2
                        + " AND "+cusNic
                        + " AND "+imieiNo
                        + " AND "+telephoneNo
                        + " AND "+rccRef
                        + " AND "+customerName
                        + " AND "+shopName;
                        
 //                
                       
 //                       + "AND     WO.CUSTOMER_NAME= '"  "';

                String qry = " SELECT   " + selectColumn + "   "
                        + " FROM     " + fromClous + "      "
                        + " WHERE    " + whereClous + "     ";
                System.out.println("qry "+qry);
                ResultSet rs = dbCon.search(con, qry);

                logger.info("Get Data to Result Set.............");

                while (rs.next()) {
                    WorkOrderStatusInquiry workOrderInq = new WorkOrderStatusInquiry();

                    workOrderInq.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                    workOrderInq.setCustomerName(rs.getString("CUSTOMER_NAME"));
                    workOrderInq.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                    workOrderInq.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                    workOrderInq.setCustomerTelephoneNo(rs.getString("TELEPHONE_NO"));
                    workOrderInq.setRccReference(rs.getString("RCC_REFERENCE"));
                    workOrderInq.setEmail(rs.getString("EMAIL"));
                    workOrderInq.setImeiNo(rs.getString("IMEI_NO"));
                    workOrderInq.setProduct(rs.getString("PRODUCT"));
                    workOrderInq.setBrand(rs.getString("BRAND"));
                    workOrderInq.setDateOfSale(rs.getString("DATE_OF_SALE"));
                    workOrderInq.setWarrantyType(rs.getString("WARRANTY_TYPE"));
                    workOrderInq.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    workOrderInq.setTechnician(rs.getString("TECHNICIAN"));
                    workOrderInq.setDeliveryType(rs.getString("DELIVERY_TYPE"));
                    workOrderInq.setTransferLocation(rs.getString("TRANSFER_LOCATION"));
                    workOrderInq.setDeliveryDate(rs.getString("DELIVERY_DATE"));
                    workOrderInq.setEstClosingDate(rs.getString("EST_CLOSING_DATE"));
                    workOrderInq.setNonWarrentyVrifType(rs.getString("NON_WARRANTY_VRIF_TYPE"));
                    workOrderInq.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                    workOrderInq.setCreateDate(rs.getString("WO_DATE"));
                    workOrderInq.setActionTaken(rs.getString("ACTION_TAKEN"));
                    workOrderInq.setDefects(rs.getString("DEFECTS"));
                    //
                    workOrderInq.setAccesoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                    workOrderInq.setRccReference(rs.getString("RCC_REFERENCE"));
                    workOrderInq.setRemark(rs.getString("REMARKS"));
                    workOrderInq.setNonWarrentyVrifType(rs.getString("NON_WARRANTY_VRIF_TYPE"));
                    workOrderInq.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                    workOrderInq.setInvoiceNo(rs.getString("INVOICE_NO"));
                    workOrderInq.setRemark2(rs.getString("REMARKS_TWO"));

                    // Not being used
                  
                   // logger.info("New Paras>>>>> act /"+workOrderInq.getActionTaken()+ "/in/"+ workOrderInq.getInvoiceNo()+"/re/"+workOrderInq.getRemark()+"/de/"+ workOrderInq.getDefects()+"/Cus /"+workOrderInq.getCustomerComplain());

                    ArrayList<Part> partList = WorkOrderInquireController.getWorkOrderSparePartsDetails(dbCon, con, rs.getString("WORK_ORDER_NO"));
                    workOrderInq.setPartList(partList);

                    double totalCost = 0;
                    for (Part part : partList) {
                        totalCost += part.getPartSellPrice();
                    }

                    ArrayList<WorkOrderDefect> workOrderDefects = WorkOrderInquireController.getWorkOrderDefectDetails(dbCon, con, rs.getString("WORK_ORDER_NO"));
                    workOrderInq.setWoDefect(workOrderDefects);

                    ArrayList<WorkOrderMaintain> utlList = WorkOrderInquireController.getWorkOrderMaintainaceDetails(rs.getString("WORK_ORDER_NO"));

                    for (WorkOrderMaintain workOrderMains : utlList) {
                        totalCost += workOrderMains.getRepairAmount();
                        workOrderInq.setRepairAmount(workOrderMains.getRepairAmount());
                        workOrderInq.setRepairLevelId(workOrderMains.getRepairLevelId());
                        workOrderInq.setRepairLevelName(workOrderMains.getLevelDesc());
                        workOrderInq.setRepairStatus(workOrderMains.getRepairStatus());
                        workOrderInq.setRepairStatusMsg(workOrderMains.getStatusMsg());
                    }

                    String woEstimateColumn = " E.EST_REF_NO, E.WORK_ORDER_NO, "
                            + " E.IMEI_NO, E.BIS_ID, E.COST_FOR_SPARE_PARTS, "
                            + " E.COST_FOR_LABOUR, E.STATUS, M.DESCRIPTION,  E.CUSTOMER_REF, E.PAYMENT_OPTION ";

                    String woEstimateValue = "WORK_ORDER_NO = '" + rs.getString("WORK_ORDER_NO") + "'";

                    ResultSet rs_estimate = dbCon.search(con, "SELECT " + woEstimateColumn + " FROM SD_WORK_ORDER_ESTIMATE E, SD_MDECODE M  WHERE M.DOMAIN_NAME = 'WO_ESTIMATE' AND E.STATUS = M.CODE AND " + woEstimateValue + "");

                    ArrayList<WorkOrderEstimate> woEstimateList = new ArrayList<>();

                    while (rs_estimate.next()) {
                        WorkOrderEstimate woel = new WorkOrderEstimate();
                        woel.setEsRefNo(rs_estimate.getString("EST_REF_NO"));
                        woel.setWorkOrderNo(rs_estimate.getString("WORK_ORDER_NO"));
                        woel.setImeiNo(rs_estimate.getString("IMEI_NO"));
                        woel.setBisId(rs_estimate.getInt("BIS_ID"));
                        woel.setCostSparePart(rs_estimate.getDouble("COST_FOR_SPARE_PARTS"));
                        woel.setCostLabor(rs_estimate.getDouble("COST_FOR_LABOUR"));
                        woel.setStatus(rs_estimate.getInt("STATUS"));
                        woel.setStatusMsg(rs_estimate.getString("DESCRIPTION"));
                        woel.setCustomerRef(rs_estimate.getString("CUSTOMER_REF"));
                        woel.setPaymentOption(rs_estimate.getInt("PAYMENT_OPTION"));
                        woEstimateList.add(woel);
                    }

                    workOrderInq.setWoEstimate(woEstimateList);

                    workOrderInq.setTotalCost(totalCost);

                    workOrderStatusInquiryList.add(workOrderInq);
                }

//            }else{
//                logger.info("No WorkOrder for relevent Details........");
//            }
        } catch (Exception e) {
            logger.info("Error getParticipentList......:" + e.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        if(con == null){
             mw.setTotalRecords(1000000);
        }else{
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderStatusInquiryList);
        logger.info("Ready to Return Values............");
        }
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

        public static MessageWrapper getWorkOrderStatusInquiryList(String wono, String custonic, String imei, String rccref) {

        logger.info("getWorkOrderStatusInquiryList method Call NEW..........");
        logger.info("NIC1 >>>>"+ custonic);
        ArrayList<WorkOrderStatusInquiry> workOrderStatusInquiryList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        String workOrderNo = null;
        
         if(con != null){
             
             try {
            
           
            
                    String woNo = (wono.equalsIgnoreCase("all") ? "NVL(WORK_ORDER_NO,'AA') = NVL(WORK_ORDER_NO,'AA')" : "UPPER(WORK_ORDER_NO) LIKE UPPER('%" + wono + "%')");
                    String cusNic = (custonic.equalsIgnoreCase("all") ? "NVL(CUSTOMER_NIC,'AA') = NVL(CUSTOMER_NIC,'AA')" : "UPPER(CUSTOMER_NIC) LIKE UPPER('%" + custonic + "%')");
                    String imieiNo = (imei.equalsIgnoreCase("all") ? "NVL(IMEI_NO,'AA') = NVL(IMEI_NO,'AA')" : "UPPER(IMEI_NO) LIKE UPPER('%" + imei + "%')");
                    String rccRef = (rccref.equalsIgnoreCase("all") ? "NVL(RCC_REFERENCE,'AA') = NVL(RCC_REFERENCE,'AA')" : "UPPER(RCC_REFERENCE) LIKE UPPER('%" + rccref + "%')");

                    logger.info("NIC2 >>>>"+ cusNic+"/"+woNo+"/"+imieiNo+"/"+rccRef);

                    String gwtWONumberwhere = "" + woNo + " "
                            + "AND " + cusNic + " "
                            + "AND " + imieiNo + " "
                            + "AND " + rccRef + "";

                    String getWorkOrderNoQry = "SELECT  WORK_ORDER_NO  "
                            + "FROM SD_WORK_ORDERS       "
                            + "WHERE    " + gwtWONumberwhere + "   ORDER BY WORK_ORDER_NO ASC";



                    ResultSet rs_getWO = dbCon.search(con, getWorkOrderNoQry);

                    if (rs_getWO.next()) {
                        workOrderNo = rs_getWO.getString("WORK_ORDER_NO");
                    }

                    logger.info("WO_NUM >>>>"+ workOrderNo);

                    if (workOrderNo != null) {

                        String selectColumn = "WO.WORK_ORDER_NO, "
                                + "        WO.CUSTOMER_NAME, "
                                + "        WO.CUSTOMER_ADDRESS, "
                                + "        WO.CUSTOMER_NIC, "
                                + "        WO.TELEPHONE_NO,"
                                + "        WO.RCC_REFERENCE, "
                                + "        WO.EMAIL, "
                                + "        WO.IMEI_NO, "
                                + "        WO.PRODUCT, "
                                + "        WO.BRAND, "
                                + "        TO_CHAR(WO.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE, "
                                + "        MD.DESCRIPTION AS WARRANTY_TYPE,"
                                + "        ML.MODEL_DESCRIPTION, "
                                + "        WM.TECHNICIAN, "
                                + "          ( SELECT  MD1.DESCRIPTION "
                                + "           FROM   SD_MDECODE MD1, SD_WORK_ORDERS WO    "
                                + "           WHERE  MD1.SEQUENCE_NO=WO.DELIVERY_TYPE "
                                + "          AND    WO.WORK_ORDER_NO='" + workOrderNo + "'    "
                                + "           AND    MD1.DOMAIN_NAME='DELEVERY_TYPE' ) AS DELIVERY_TYPE,"
                                + "            (SELECT B.BIS_NAME FROM SD_BUSINESS_STRUCTURES B WHERE B.BIS_ID = WO.SERVICE_BIS_ID) AS DIST_SHOP_NAME,     "
                                // + "        BS.BIS_NAME AS TRANSFER_LOCATION, "
                                + "         ( SELECT B.BIS_NAME "
                                + "          FROM SD_WORK_ORDER_TRANSFERS T,SD_BUSINESS_STRUCTURES B "
                                + "          WHERE WORK_ORD_TRNS_NO IN (SELECT MAX(WORK_ORD_TRNS_NO) "
                                + "                                     FROM SD_WORK_ORDER_TRANSFERS "
                                + "                                     WHERE WORK_ORDER_NO = '"+workOrderNo+"') "
                                + "          AND T.BIS_ID = B.BIS_ID (+)) AS TRANSFER_LOCATION,   "
                                //+ "        TO_CHAR(WT.RETURN_DATE, 'YYYY/MM/DD') AS DELIVERY_DATE, "
                                + "            ( SELECT TO_CHAR(WO.DELIVERY_DATE, 'YYYY/MM/DD')   "
                                + "           FROM  SD_WORK_ORDERS WO    "
                                + "           WHERE  WO.WORK_ORDER_NO='" + workOrderNo + "') AS DELIVERY_DATE,     "
                                + "           TO_CHAR(WM.EST_CLOSING_DATE, 'YYYY/MM/DD') AS EST_CLOSING_DATE,          "
                             //   + "           NMD.DESCRIPTION AS NON_WARRANTY_VRIF_TYPE,   "
                                + "         NVL((SELECT A1.DESCRIPTION FROM SD_MDECODE A1 WHERE A1.DOMAIN_NAME='NON_WARANTY_TYPE' AND A1.SEQUENCE_NO=WO.NON_WARRANTY_VRIF_TYPE), '-') AS NON_WARRANTY_VRIF_TYPE, "
                                + "           WO.NON_WARRANTY_REMARKS,     "
                                + "           TO_CHAR(WO.DATE_INSERTED,'YYYY/MM/DD') WO_DATE, "
                                 // --> Changes - Dilip
                                + "        WO.CUSTOMER_COMPLAIN,"
                                + "         WM.ACTION_TAKEN,"
                                + "         WM.INVOICE_NO,"
                                + "         WO.REMARKS,"
                                + "         WM.REMARKS AS MREMARKS,"
                                + "         SDC.MAJ_CODE_DESC as DEFECTS,"
                                + "         WO.COURIER_NO,"
                                + "         WO.GATE_PASS,"
                                + "         (SELECT B.BIS_NAME FROM SD_BUSINESS_STRUCTURES B WHERE B.BIS_ID = WO.TRANSFER_LOCATION)  AS TR_LOC,"
                                + "         WO.ACCESSORIES_RETAINED";

                        String fromClous = "SD_WORK_ORDERS WO, SD_MDECODE MD, SD_MDECODE NMD, SD_WORK_ORDER_MAINTAIN WM,SD_MODEL_LISTS ML,  SD_WORK_ORDER_ESTIMATE ERN, SD_DEFECT_CODES SDC ";

        //                String whereClous = " WO.WORK_ORDER_NO='" + workOrderNo + "' "
        //                        + "AND     MD.SEQUENCE_NO=WO.WARRANTY_VRIF_TYPE (+) "
        //                        + "AND     NMD.SEQUENCE_NO=WO.NON_WARRANTY_VRIF_TYPE (+) "
        //                        + "AND     MD.DOMAIN_NAME='WARANTY' (+) "
        //                        + "AND     NMD.DOMAIN_NAME='NON_WARANTY_TYPE' (+)"
        //                        + "AND     WO.WORK_ORDER_NO=WM.WORK_ORDER_NO (+) "
        //                        + "AND     WO.MODEL_NO  = ML.MODEL_NO (+) "
        //                        + "AND     WO.DEFECTS  = SDC.MAJ_CODE (+) ";
        //                
                        String whereClous = " WO.WORK_ORDER_NO='" + workOrderNo + "' "
                                + "AND     MD.SEQUENCE_NO=WO.WARRANTY_VRIF_TYPE "
                                + "AND     MD.DOMAIN_NAME='WARANTY' "
                                + "AND     WO.WORK_ORDER_NO=WM.WORK_ORDER_NO (+) "
                                + "AND     WO.MODEL_NO  = ML.MODEL_NO (+) "
                                + "AND     WO.DEFECTS  = SDC.MAJ_CODE (+) ";

                        String qry = " SELECT   " + selectColumn + "   "
                                + " FROM     " + fromClous + "      "
                                + " WHERE    " + whereClous + "     ";
                        // System.out.println(qry);
                        ResultSet rs = dbCon.search(con, qry);

                        logger.info("Get Data to Result Set.............");

                        if (rs.next()) {
                            WorkOrderStatusInquiry workOrderInq = new WorkOrderStatusInquiry();

                            workOrderInq.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                            workOrderInq.setCustomerName(rs.getString("CUSTOMER_NAME"));
                            workOrderInq.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                            workOrderInq.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                            workOrderInq.setCustomerTelephoneNo(rs.getString("TELEPHONE_NO"));
                            workOrderInq.setRccReference(rs.getString("RCC_REFERENCE"));
                            workOrderInq.setEmail(rs.getString("EMAIL"));
                            workOrderInq.setImeiNo(rs.getString("IMEI_NO"));
                            workOrderInq.setProduct(rs.getString("PRODUCT"));
                            workOrderInq.setBrand(rs.getString("BRAND"));
                            workOrderInq.setDateOfSale(rs.getString("DATE_OF_SALE"));
                            workOrderInq.setWarrantyType(rs.getString("WARRANTY_TYPE"));
                            workOrderInq.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                            workOrderInq.setTechnician(rs.getString("TECHNICIAN"));
                            workOrderInq.setDeliveryType(rs.getString("DELIVERY_TYPE"));
                            workOrderInq.setTransferLocation(rs.getString("TR_LOC"));
                           //workOrderInq.setTransferLocation(rs.getString("TRANSFER_LOCATION"));
                            workOrderInq.setDeliveryDate(rs.getString("DELIVERY_DATE"));
                            workOrderInq.setEstClosingDate(rs.getString("EST_CLOSING_DATE"));
                            workOrderInq.setNonWarrentyVrifType(rs.getString("NON_WARRANTY_VRIF_TYPE"));
                            workOrderInq.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                            workOrderInq.setCreateDate(rs.getString("WO_DATE"));
                            workOrderInq.setInvoiceNo(rs.getString("INVOICE_NO"));
                            workOrderInq.setActionTaken(rs.getString("ACTION_TAKEN"));
                            workOrderInq.setRemark(rs.getString("REMARKS"));
                            workOrderInq.setAccesoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                            workOrderInq.setDefects(rs.getString("DEFECTS"));
                            workOrderInq.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                            workOrderInq.setGatePass(rs.getString("GATE_PASS"));
                            workOrderInq.setCourierNo(rs.getString("COURIER_NO"));
                            workOrderInq.setDistShop(rs.getString("DIST_SHOP_NAME"));
                            workOrderInq.setRemark2(rs.getString("MREMARKS"));



                            ArrayList<Part> partList = WorkOrderInquireController.getWorkOrderSparePartsDetails(dbCon, con, workOrderNo);
                            workOrderInq.setPartList(partList);

                             logger.info("New Paras>>>>> act /"+workOrderInq.getActionTaken()+ "/in/"+ workOrderInq.getInvoiceNo()+"/re/"+workOrderInq.getRemark()+"/de/"+ workOrderInq.getDefects()+"/Tech/"+workOrderInq.getTechnician()+"/ NON_WARR_TYP"+workOrderInq.getNonWarrentyVrifType()+" / TrLoc / "+workOrderInq.getTransferLocation() );

                            double totalCost = 0;
                            for (Part part : partList) {
                                totalCost += part.getPartSellPrice();
                            }

                            ArrayList<WorkOrderDefect> workOrderDefects = WorkOrderInquireController.getWorkOrderDefectDetails(dbCon, con, workOrderNo);
                            workOrderInq.setWoDefect(workOrderDefects);

                            ArrayList<WorkOrderMaintain> utlList = WorkOrderInquireController.getWorkOrderMaintainaceDetails(rs.getString("WORK_ORDER_NO"));

                            for (WorkOrderMaintain workOrderMains : utlList) {
                                totalCost += workOrderMains.getRepairAmount();
                                workOrderInq.setRepairAmount(workOrderMains.getRepairAmount());
                                workOrderInq.setRepairLevelId(workOrderMains.getRepairLevelId());
                                workOrderInq.setRepairLevelName(workOrderMains.getLevelDesc());
                                workOrderInq.setRepairStatus(workOrderMains.getRepairStatus());
                                workOrderInq.setRepairStatusMsg(workOrderMains.getStatusMsg());
                            }

                            String woEstimateColumn = " E.EST_REF_NO, E.WORK_ORDER_NO, "
                                    + " E.IMEI_NO, E.BIS_ID, E.COST_FOR_SPARE_PARTS, "
                                    + " E.COST_FOR_LABOUR, E.STATUS, M.DESCRIPTION,  E.CUSTOMER_REF, E.PAYMENT_OPTION ";

                            String woEstimateValue = "WORK_ORDER_NO = '" + workOrderNo + "'";

                            ResultSet rs_estimate = dbCon.search(con, "SELECT " + woEstimateColumn + " FROM SD_WORK_ORDER_ESTIMATE E, SD_MDECODE M  WHERE M.DOMAIN_NAME = 'ESTIMATE_STATUS' AND E.STATUS = M.CODE AND " + woEstimateValue + "");

                            ArrayList<WorkOrderEstimate> woEstimateList = new ArrayList<>();

                            while (rs_estimate.next()) {
                                WorkOrderEstimate woel = new WorkOrderEstimate();
                                woel.setEsRefNo(rs_estimate.getString("EST_REF_NO"));
                                woel.setWorkOrderNo(rs_estimate.getString("WORK_ORDER_NO"));
                                woel.setImeiNo(rs_estimate.getString("IMEI_NO"));
                                woel.setBisId(rs_estimate.getInt("BIS_ID"));
                                woel.setCostSparePart(rs_estimate.getDouble("COST_FOR_SPARE_PARTS"));
                                woel.setCostLabor(rs_estimate.getDouble("COST_FOR_LABOUR"));
                                woel.setStatus(rs_estimate.getInt("STATUS"));
                                woel.setStatusMsg(rs_estimate.getString("DESCRIPTION"));
                                woel.setCustomerRef(rs_estimate.getString("CUSTOMER_REF"));
                                woel.setPaymentOption(rs_estimate.getInt("PAYMENT_OPTION"));
                                woEstimateList.add(woel);
                            }

                            workOrderInq.setWoEstimate(woEstimateList);

                            workOrderInq.setTotalCost(totalCost);

                            workOrderStatusInquiryList.add(workOrderInq);
                        }

                    }else{
                        logger.info("No WorkOrder for relevent Details........");
                    }
                } catch (Exception e) {
                    logger.info("Error getParticipentList......:" + e.toString());
                }
                
           }else {
                totalrecode = 1000000;
            }
        
        

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderStatusInquiryList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

// End of Load Service
    public static ArrayList<Part> getWorkOrderSparePartsDetails(DbCon dbCon, Connection con, String workOrderNo) {
        ArrayList<Part> partList = new ArrayList<>();

        try {
            
            double partsCost = 0;

            String selectColumns = "SP.SPARE_PART_NO, "
                    + "       SP.PART_DESCRIPTION, "
                    + "       SP.ALTERNATE_PART_EXISTS, "
                    + "       SP.PART_PRD_CODE, "
                    + "       SP.PART_PRD_CODE_DESC, "
                    + "       SP.PART_PRD_FAMILY, "
                    + "       SP.PART_PRD_FAMILY_DESC, "
                    + "       SP.COMMODITY_GROUP_1, "
                    + "       SP.COMMODITY_GROUP_2, "
                    + "       SP.SELLING_PRICE, "
                    + "       SP.STATUS, "
                    + "       SP.COST_PRICE,"
                    + "       SP.ERP_SPARE_PART_NO,"
                    + "       WMD.SELLING_PRICE WM_PRICE ";

            String fromClause = " SD_WORK_ORDER_MAINTAIN_DTL WMD, SD_SPARE_PARTS SP ";

            String whereClause = " WMD.SPARE_PART_NO=SP.SPARE_PART_NO "
                    + " AND    WMD.WORK_ORDER_NO= '" + workOrderNo + "' ";

            String qry = " SELECT " + selectColumns + "  "
                    + " FROM " + fromClause + "     "
                    + " WHERE " + whereClause + "   ";

            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                Part part = new Part();

                part.setPartNo(rs.getInt("SPARE_PART_NO"));
                part.setPartDesc(rs.getString("PART_DESCRIPTION"));
                part.setPartExist(rs.getInt("ALTERNATE_PART_EXISTS"));
                part.setPartPrdCode(rs.getString("PART_PRD_CODE"));
                part.setPartPrdDesc(rs.getString("PART_PRD_CODE_DESC"));
                part.setPartPrdFamily(rs.getString("PART_PRD_FAMILY"));
                part.setPartPrdFamilyDesc(rs.getString("PART_PRD_FAMILY_DESC"));
                part.setPartGroup1(rs.getString("COMMODITY_GROUP_1"));
                part.setPartGroup2(rs.getString("COMMODITY_GROUP_2"));
                partsCost =+ rs.getDouble("SELLING_PRICE");
                partsCost =+ rs.getDouble("WM_PRICE");
                part.setPartSellPrice(partsCost);
                part.setPartCost(rs.getDouble("COST_PRICE"));
                part.setErpPartNo(rs.getString("ERP_SPARE_PART_NO"));
                partList.add(part);
            }

        } catch (Exception e) {

        }

        return partList;
    }

    public static MessageWrapper getWorkOrderList() {

        logger.info("getWorkOrderList Method Call....................");
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "W.WORK_ORDER_NO ";

            String whereClous = "";

            String table = "SD_WORK_ORDERS W ";

            String qry = "SELECT " + seletColunm + " FROM " + table + " ";

            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                WorkOrder wo = new WorkOrder();
                wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));

                workOrderList.add(wo);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(workOrderList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ArrayList<WorkOrderDefect> getWorkOrderDefectDetails(DbCon dbCon, Connection con, String workOrderNo) {

        ArrayList<WorkOrderDefect> defectList = new ArrayList<>();

        try {

            String selectColumns = " WD.WRK_ORD_DEFT_ID, WD.WRK_ORD_MTN_ID, WD.MAJ_CODE, DC.MAJ_CODE_DESC, WD.REMARKS, WD.STATUS, WD.MIN_CODE, MC.MIN_CODE_DESC ";

            String fromClause = " SD_WORK_ORDER_MAINTAIN WM, SD_WORK_ORDER_DEFECTS WD, SD_DEFECT_CODES DC, SD_DEFECT_MINOR_CODES MC ";

            String whereClause = " WM.WRK_ORD_MTN_ID = WD.WRK_ORD_MTN_ID "
                    + "AND     WD.MAJ_CODE=DC.MAJ_CODE "
                        + "AND     WD.MIN_CODE=MC.MIN_CODE "
                    + "AND     WM.WORK_ORDER_NO='" + workOrderNo + "' ";

            String qry = " SELECT " + selectColumns + " "
                    + " FROM " + fromClause + " "
                    + " WHERE " + whereClause + "   ";

            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                WorkOrderDefect defect = new WorkOrderDefect();

                defect.setWoDefectId(rs.getInt("WRK_ORD_DEFT_ID"));
                defect.setWoMaintainId(rs.getInt("WRK_ORD_MTN_ID"));
                defect.setMajorCode(rs.getInt("MAJ_CODE"));
                defect.setMinorCode(rs.getInt("MIN_CODE"));
                defect.setRemarks(rs.getString("REMARKS"));
                defect.setStatus(rs.getInt("STATUS"));
                defect.setMajDesc(rs.getString("MAJ_CODE_DESC"));
                defect.setMinDesc(rs.getString("MIN_CODE_DESC"));

                defectList.add(defect);
            }

        } catch (Exception e) {
            e.getMessage();
        }

        return defectList;
    }

    public static ArrayList<WorkOrderMaintain> getWorkOrderMaintainaceDetails(String workOrderNo) {

        ArrayList<WorkOrderMaintain> woDetailsList = new ArrayList<>();

        int totalrecode = 0;
        String wo_num = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String woDetailColumn = " W.REPAIR_LEVEL_ID, L.LEVEL_NAME, W.REPAIR_LEVEL_AMOUNT, W.REPAIR_STATUS, M.DESCRIPTION REPAIR_DESC  ";
            String woDetailswhere = "W.WORK_ORDER_NO = '" + workOrderNo + "' ";

            ResultSet rs_details = dbCon.search(con, "SELECT " + woDetailColumn + " FROM SD_WORK_ORDER_MAINTAIN W, SD_MDECODE M, SD_REPAIR_LEVELS L WHERE M.DOMAIN_NAME = 'RP_STATUS' AND W.REPAIR_STATUS = M.CODE AND W.REPAIR_LEVEL_ID = L.LEVEL_ID AND " + woDetailswhere + "");

            while (rs_details.next()) {
                
                WorkOrderMaintain wodl = new WorkOrderMaintain();
                wodl.setRepairLevelId(rs_details.getInt("REPAIR_LEVEL_ID"));
                wodl.setLevelDesc(rs_details.getString("LEVEL_NAME"));
                wodl.setRepairAmount(rs_details.getInt("REPAIR_LEVEL_AMOUNT"));
                wodl.setRepairStatus(rs_details.getInt("REPAIR_STATUS"));
                wodl.setStatusMsg(rs_details.getString("REPAIR_DESC"));
                woDetailsList.add(wodl);
            }

        } catch (Exception ex) {
            logger.info("Error getWorkOrderDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();

        return woDetailsList;
    }

}
