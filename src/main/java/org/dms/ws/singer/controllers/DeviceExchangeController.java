/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.DeviceExchange;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.MdeCode;
import org.dms.ws.singer.entities.WorkOrder;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class DeviceExchangeController {

    public static MessageWrapper getDeviceExchangeDetails(int bisId, 
            String referenc_no,
            String work_no,
            String imei_no,
            String custome_name,
            String customer_nic,
            String product,
            String brand,
            String modledesc,
            String date_of_sale,
            String reason_desc,
            String exchage_desc,
            String warranty_desc,
            String order,
            String erpCode,
            String type,
            int start,
            int limit) {

        logger.info("getDeviceExchangeDetails Method Call....................");
        ArrayList<DeviceExchange> devicExchangeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.deviceExchange(order);

            String exchangeReferenceNo = (referenc_no.equalsIgnoreCase("all") ? "NVL(E.REFERENCE_NO,'AA') = NVL(E.REFERENCE_NO,'AA')" : "UPPER(E.REFERENCE_NO) LIKE UPPER('%" + referenc_no + "%')");
            String workOrderNo = (work_no.equalsIgnoreCase("all") ? "NVL(E.WORK_ORDER_NO,'AA') = NVL(E.WORK_ORDER_NO,'AA')" : "UPPER(E.WORK_ORDER_NO) LIKE UPPER('%" + work_no + "%')");
            String imeiNo = (imei_no.equalsIgnoreCase("all") ? "NVL(E.IMEI_NO,'AA') = NVL(E.IMEI_NO,'AA')" : "UPPER(E.IMEI_NO) LIKE UPPER('%" + imei_no + "%')");
            // String Reason = (reason == 0 ? "" : "AND E.REASON = " + reason + "");
            // String ExcnhageType = (type_exch == 0 ? "" : "AND E.TYPE_OF_EXCHANGE = " + type_exch + " ");
             String bisIdQ = (bisId == 0 ? " " : " AND E.BIS_ID = " + bisId + " ");
            // String Status = (status == 0 ? "" : "AND E.STATUS = " + status + "");
            String CustomerName = (custome_name.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NAME,'AA') = NVL(W.CUSTOMER_NAME,'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + custome_name + "%')");
            String CustomerNic = (customer_nic.equalsIgnoreCase("all") ? "NVL(W.CUSTOMER_NIC,'AA') = NVL(W.CUSTOMER_NIC,'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customer_nic + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(D.PRODUCT,'AA') = NVL(D.PRODUCT,'AA')" : "UPPER(D.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(D.BRAND,'AA') = NVL(D.BRAND,'AA')" : "UPPER(D.BRAND) LIKE UPPER('%" + brand + "%')");
            String modleDesc = (modledesc.equalsIgnoreCase("all") ? "NVL(M.MODEL_DESCRIPTION,'AA') = NVL(M.MODEL_DESCRIPTION,'AA')" : "UPPER(M.MODEL_DESCRIPTION) LIKE UPPER('%" + modledesc + "%')");
            String dateOfSale = (date_of_sale.equalsIgnoreCase("all") ? "NVL(W.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD')) = NVL(W.DATE_INSERTED,TO_DATE('20100101','YYYYMMDD'))" : "TO_DATE(TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + date_of_sale + "','YYYY/MM/DD')");
            String reasonDesc = (reason_desc.equalsIgnoreCase("all") ? "NVL(MD.DESCRIPTION,'AA') = NVL(MD.DESCRIPTION,'AA')" : "UPPER(MD.DESCRIPTION) LIKE UPPER('%" + reason_desc + "%')");
            String exchangeDesc = (exchage_desc.equalsIgnoreCase("all") ? "NVL(MD1.DESCRIPTION,'AA') = NVL(MD1.DESCRIPTION,'AA')" : "UPPER(MD1.DESCRIPTION) LIKE UPPER('%" + exchage_desc + "%')");
            //  String warrantyDesc = (warranty_desc.equalsIgnoreCase("all") ? "NVL(MD2.DESCRIPTION,'AA') = NVL(MD2.DESCRIPTION,'AA')" : "UPPER(MD2.DESCRIPTION) LIKE UPPER('%" + warranty_desc + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY E.REFERENCE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "E.REFERENCE_NO,E.WORK_ORDER_NO,"
                    + "E.IMEI_NO,W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_NIC,TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') DATE_INSERTED,"
                    + "W.STATUS W_STATUS,D.PRODUCT,"
                    + "D.BRAND,M.MODEL_DESCRIPTION,"
                    + "MD.DESCRIPTION REASON_DESC,"
                    + "MD1.DESCRIPTION EXCHANGE_DESC,"
                    //   + "MD2.DESCRIPTION WARRANTY_DESC,"
                    + "E.REASON,E.TYPE_OF_EXCHANGE,"
                    + "E.BIS_ID,E.STATUS,E.REMARKS,"
                    + "E.WRT_PERIOD_FLAG,"
                    + "E.WRT_IMEI_FLAG,E.WRT_DAMAGE_FLAG,"
                    + "E.UN_REPAIR_FLAG,E.WATER_DAMAGE,"
                    + "E.REPRODUCIBLE_FLAG, "
                    + "M.ERP_PART_NO, "
                    + "TO_CHAR(E.DATE_INSERTED,'YYYY/MM/DD') AS EXCHANGE_DATE    ";

            String whereClous = "E.IMEI_NO = W.IMEI_NO (+) "
                    + " AND E.IMEI_NO = D.IMEI_NO (+) "
                    + " AND D.MODEL_NO = M.MODEL_NO (+)  "
                    + " AND E.REASON = MD.CODE (+) "
                    + " AND MD.DOMAIN_NAME = 'DE_REASON'  "
                    + " AND E.TYPE_OF_EXCHANGE = MD1.CODE (+) "
                    + " AND MD1.DOMAIN_NAME = 'EXC_TYPE' "
                    //                    + " AND W.WARANTY_TYPE = MD2.CODE (+) "
                    //                    + " AND MD2.DOMAIN_NAME = 'WARANTY'  "
                    + " AND " + exchangeReferenceNo + " "
                    + " AND " + workOrderNo + " "
                    + " AND " + imeiNo + " "
                    + " AND " + CustomerName + " "
                    + " AND " + CustomerNic + " "
                    + " AND " + Product + " "
                    + " AND " + Brand + " "
                    + " AND " + modleDesc + " "
                    + " AND " + dateOfSale + " "
                    + " AND " + reasonDesc + " "
                    + " AND " + exchangeDesc + " "
                    //   + " AND " + warrantyDesc + " "
                    +" " + bisIdQ + " "
                    + " AND E.STATUS != 9";

            String table = "SD_DEVICE_EXCHANGE E,"
                    + "SD_WARRENTY_REGISTRATION W,"
                    + "SD_DEVICE_MASTER D,"
                    + "SD_MODEL_LISTS M,"
                    + "SD_MDECODE MD,"
                    + "SD_MDECODE MD1 ";
            //    + "SD_MDECODE MD2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + table + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + table + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> warrantImie = WorkPaymentController.checkImeiWarranty();

            while (rs.next()) {

                String warrantyStatus = "Out of Warranty";

                for (ImeiMaster i : warrantImie) {
                    if (i.getImeiNo().equals(rs.getString("IMEI_NO"))) {
                        warrantyStatus = "Under Warranty";
                    }
                }

                DeviceExchange de = new DeviceExchange();
                totalrecode = rs.getInt("CNT");
                de.setExchangeReferenceNo(rs.getString("REFERENCE_NO"));
                de.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                de.setImeiNo(rs.getString("IMEI_NO"));
                de.setCustomerName(rs.getString("CUSTOMER_NAME"));
                de.setCustomerNIC(rs.getString("CUSTOMER_NIC"));
                de.setWarrantyRegDate(rs.getString("DATE_INSERTED"));
                de.setProduct(rs.getString("PRODUCT"));
                de.setBrand(rs.getString("BRAND"));
                de.setModelDesc(rs.getString("MODEL_DESCRIPTION"));
                de.setReasonDesc(rs.getString("REASON_DESC"));
                de.setExchangeDesc(rs.getString("EXCHANGE_DESC"));

                //de.setWarrantyDesc(rs.getString("WARRANTY_DESC"));
                de.setWarrantyDesc(warrantyStatus);

                de.setModelDesc(rs.getString("MODEL_DESCRIPTION"));
                de.setErpCode(rs.getString("ERP_PART_NO"));
                de.setReason(rs.getInt("REASON"));
                de.setTypeOfExchange(rs.getInt("TYPE_OF_EXCHANGE"));
                de.setBisId(rs.getInt("BIS_ID"));
                de.setStatus(rs.getInt("STATUS"));
                de.setRemarks(rs.getString("REMARKS"));
                de.setWrtPeriodFlag(rs.getInt("WRT_PERIOD_FLAG"));
                de.setWrtImeiFlag(rs.getInt("WRT_IMEI_FLAG"));
                de.setWrtDamageFlag(rs.getInt("WRT_DAMAGE_FLAG"));
                de.setUnAuthRepairFlag(rs.getInt("UN_REPAIR_FLAG"));
                de.setWaterDamage(rs.getInt("WATER_DAMAGE"));
                de.setReprodicbleFlag(rs.getInt("REPRODUCIBLE_FLAG"));
                de.setExchangeDate(rs.getString("EXCHANGE_DATE"));
                devicExchangeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getDeviceExchangeDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(devicExchangeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addDeviceExchange(DeviceExchange de_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addDeviceExchange Mehtod Call.......");
        int result = 0;
        String last_deviceExch_id = "DE000000";
        String nxtNewDeviceExchangeId = null;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        
        try{
            String qryParentBisIdSelect = "SELECT PARENT_BIS_ID FROM SD_BUSINESS_STRUCTURES WHERE PARENT_BIS_ID ='"+de_info.getBisId()+"";
            ResultSet rsParentBisId = dbCon.search(con, qryParentBisIdSelect);
            
            while (rsParentBisId.next()) {
                int parentBisID =  rsParentBisId.getInt("PARENT_BIS_ID");
                de_info.setBisId(parentBisID);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        
        
        try {

            String qry = "  SELECT  COUNT(*) AS CNT   "
                    + "     FROM   SD_WORK_ORDER_MAINTAIN   "
                    + "     WHERE  WORK_ORDER_NO = '" + de_info.getWorkOrderNo() + "'   "
                    + "     AND    (REPAIR_STATUS = 2  OR  REPAIR_STATUS =3)    ";   //////Check Work Order has Maintainance Record

            ResultSet rs_wochk = dbCon.search(con, qry);

            int chkWorkOrder = 0;
            while (rs_wochk.next()) {
                chkWorkOrder = rs_wochk.getInt("CNT");
            }

            if (chkWorkOrder > 0) {   ///If Work Order has Maintainance Record it can't be Excahnged.

                ResultSet rs_decnt = dbCon.search(con, "SELECT REFERENCE_NO "
                        + "FROM SD_DEVICE_EXCHANGE "
                        + "WHERE ROWNUM = 1 "
                        + "ORDER BY REFERENCE_NO DESC ");

                if (rs_decnt.next()) {
                    last_deviceExch_id = rs_decnt.getString(1);
                    logger.info("Last Work Order id   " + last_deviceExch_id);
                }

                last_deviceExch_id = last_deviceExch_id.split("DE")[1];
                int nxt_de_num = Integer.parseInt(last_deviceExch_id) + 1;
                String nxtDeviceExchangeId = String.valueOf(nxt_de_num);
                int nxtDeviceExchange_Length = nxtDeviceExchangeId.length();

                nxtNewDeviceExchangeId = "DE";
                if (nxtDeviceExchange_Length < 6) {
                    for (int i = 0; i < (6 - nxtDeviceExchange_Length); i++) {
                        nxtNewDeviceExchangeId += "0";
                    }
                }
                nxtNewDeviceExchangeId += nxtDeviceExchangeId;

                logger.info("Inserting................Work Order No = " + nxtNewDeviceExchangeId);

                String column = "REFERENCE_NO,WORK_ORDER_NO,"
                        + "IMEI_NO,REASON,"
                        + "TYPE_OF_EXCHANGE,BIS_ID,"
                        + "STATUS,USER_INSERTED,DATE_INSERTED,"
                        + "WRT_PERIOD_FLAG,WRT_IMEI_FLAG,"
                        + "WRT_DAMAGE_FLAG,UN_REPAIR_FLAG,"
                        + "WATER_DAMAGE,REPRODUCIBLE_FLAG";

                String values = "?,?,?,?,?,?,1,?,SYSDATE,?,?,?,?,?,?";

                try {

                    PreparedStatement ps_in_deEx = dbCon.prepare(con, "INSERT INTO SD_DEVICE_EXCHANGE(" + column + ") VALUES(" + values + ")");
                    ps_in_deEx.setString(1, nxtNewDeviceExchangeId);
                    ps_in_deEx.setString(2, de_info.getWorkOrderNo());
                    ps_in_deEx.setString(3, de_info.getImeiNo());
                    ps_in_deEx.setInt(4, de_info.getReason());
                    ps_in_deEx.setInt(5, de_info.getTypeOfExchange());
                    ps_in_deEx.setInt(6, de_info.getBisId());
                    ps_in_deEx.setString(7, user_id);
                    ps_in_deEx.setInt(8, de_info.getWrtPeriodFlag());
                    ps_in_deEx.setInt(9, de_info.getWrtImeiFlag());
                    ps_in_deEx.setInt(10, de_info.getWrtDamageFlag());
                    ps_in_deEx.setInt(11, de_info.getUnAuthRepairFlag());
                    ps_in_deEx.setInt(12, de_info.getWaterDamage());
                    ps_in_deEx.setInt(13, de_info.getReprodicbleFlag());
                    ps_in_deEx.executeUpdate();
                    result = 1;
                } catch (Exception e) {
                    logger.info("Error in addDeviceExchange Insert......" + e.getMessage());
                    result = 9;
                }

            } else {
                result = 2;
            }

        } catch (Exception ex) {
            logger.info("Error in addDeviceExchange Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(nxtNewDeviceExchangeId);
            vw.setReturnMsg("Device Exchange Insert Successfully");
            logger.info("Device Exchange Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Work Order Maintainance Records should be closed before Exchanging.");
            logger.info("Device Exchange Insert Succesfully............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Device Exchange Insert");
            logger.info("Error in Device Exchange Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editDeviceExchange(DeviceExchange de_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editDeviceExchange Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            PreparedStatement ps_up_device = dbCon.prepare(con, "UPDATE SD_DEVICE_EXCHANGE "
                    + " SET WORK_ORDER_NO =?,"
                    + " REASON =?,"
                    + " TYPE_OF_EXCHANGE =?,"
                    + " USER_MODIFIED=?,"
                    + " DATE_MODIFIED=SYSDATE "
                    + " WHERE REFERENCE_NO =?");

            ps_up_device.setString(1, de_info.getWorkOrderNo());
            ps_up_device.setInt(2, de_info.getReason());
            ps_up_device.setInt(3, de_info.getTypeOfExchange());
            ps_up_device.setString(4, user_id);
            ps_up_device.setString(5, de_info.getExchangeReferenceNo());
            ps_up_device.executeUpdate();

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in editDeviceExchange Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Exchange Update Successfully");
            logger.info("Device Exchange Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Device Exchange Update");
            logger.info("Error in Device Exchange Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper deleteDeviceExchange(DeviceExchange de_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deleteDeviceExchange Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_delete = dbCon.prepare(con, "UPDATE SD_DEVICE_EXCHANGE "
                    + " SET STATUS =9,"
                    + " REMARKS = ?,"
                    + " USER_MODIFIED=?,"
                    + " DATE_MODIFIED=SYSDATE "
                    + " WHERE REFERENCE_NO =?");

            ps_delete.setString(1, de_info.getRemarks());
            ps_delete.setString(2, user_id);
            ps_delete.setString(3, de_info.getExchangeReferenceNo());
            ps_delete.executeUpdate();

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in deleteDeviceExchange Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Exchange Delete Successfully");
            logger.info("Device Exchange Delete Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Device Exchange Delete");
            logger.info("Error in Device Exchange Delete................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getDeviceExchangeReason() {

        logger.info("getDeviceExchangeReason Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'DE_REASON'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getDeviceExchangeReason Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getDeviceExchangeType() {

        logger.info("getDeviceExchangeType Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'EXC_TYPE'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }

        } catch (Exception ex) {
            logger.info("Error in getDeviceExchangeType Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getWorkOrdersToExchange(int bisId,
            String workOrderNo, 
            String customerName, 
            String customerAddress,
            String workTelephoneNo, 
            String email,
            String customerNic,
            int imeiNo,
            String product,
            String brand,
            String modleDesc,
            String defectDesc,
            String rccReference,
            String dateOfSale,
            String proofOfPurches,
            String binName,
            String warrantyStatusMsg,
            int delayDate
    ) {
        
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<WorkOrder> workOrderList = new ArrayList<>();
        
        String workOrderNoQry = (workOrderNo.equalsIgnoreCase("all")) ? "NVL(W.WORK_ORDER_NO, 'AA') = NVL(W.WORK_ORDER_NO, 'AA')" : "UPPER(W.WORK_ORDER_NO) LIKE UPPER('%" + workOrderNo + "%')";
        String customerNameQry = (customerName.equalsIgnoreCase("all")) ? "NVL(W.CUSTOMER_NAME, 'AA') = NVL(W.CUSTOMER_NAME, 'AA')" : "UPPER(W.CUSTOMER_NAME) LIKE UPPER('%" + customerName + "%')";
        String customerAddressQry = (customerAddress.equalsIgnoreCase("all")) ? "NVL(W.CUSTOMER_ADDRESS, 'AA') = NVL(W.CUSTOMER_ADDRESS, 'AA')" : "UPPER(W.CUSTOMER_ADDRESS) LIKE UPPER('%" + customerAddress + "%')";
        String workTelephoneNoQry = (workTelephoneNo.equalsIgnoreCase("all")) ? "NVL(W.TELEPHONE_NO, 'AA') = NVL(W.TELEPHONE_NO, 'AA')" : "UPPER(W.TELEPHONE_NO) LIKE UPPER('%" + workTelephoneNo + "%')";
        String emailQry = (email.equalsIgnoreCase("all")) ? "NVL(W.EMAIL, 'AA') = NVL(W.EMAIL, 'AA')" : "UPPER(W.EMAIL) LIKE UPPER('%" + email + "%')";
        String customerNicQry = (customerNic.equalsIgnoreCase("all")) ? "NVL(W.CUSTOMER_NIC, 'AA') = NVL(W.CUSTOMER_NIC, 'AA')" : "UPPER(W.CUSTOMER_NIC) LIKE UPPER('%" + customerNic + "%')";
        String imeiNoQry = ((imeiNo == 0)) ? "NVL(W.IMEI_NO, 0) = NVL(W.IMEI_NO, 0)" : "W.IMEI_NO LIKE UPPER('%" + imeiNo + "%')";
        String productcQry = (product.equalsIgnoreCase("all")) ? "NVL(W.PRODUCT, 'AA') = NVL(W.PRODUCT, 'AA')" : "UPPER(W.PRODUCT) LIKE UPPER('%" + product + "%')";
        String brandQry = (brand.equalsIgnoreCase("all")) ? "NVL(W.BRAND, 'AA') = NVL(W.BRAND, 'AA')" : "UPPER(W.BRAND) LIKE UPPER('%" + brand + "%')";
        String modleDescQry = (modleDesc.equalsIgnoreCase("all")) ? "NVL(M.DESCRIPTION, 'AA') = NVL(M.DESCRIPTION, 'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + modleDesc + "%')";
        String defectDescDescQry = (defectDesc.equalsIgnoreCase("all")) ? "NVL(D.MAJ_CODE_DESC, 'AA') = NVL(D.MAJ_CODE_DESC, 'AA')" : "UPPER(D.MAJ_CODE_DESC) LIKE UPPER('%" + defectDesc + "%')";
        String rccReferenceQry = (rccReference.equalsIgnoreCase("all")) ? "NVL(W.RCC_REFERENCE, 'AA') = NVL(W.RCC_REFERENCE, 'AA')" : "UPPER(W.RCC_REFERENCE) LIKE UPPER('%" + rccReference + "%')";
        String dateOfSaleQry = (dateOfSale.equalsIgnoreCase("all")) ? "" : "AND W.DATE_OF_SALE BETWEEN TO_DATE('"+dateOfSale+"','YYYY/MM/DD') AND TO_DATE('"+dateOfSale+"','YYYY/MM/DD')";
        String proofOfPurchesQry = (proofOfPurches.equalsIgnoreCase("all")) ? "NVL(W.PROOF_OF_PURCHASE, 'AA') = NVL(W.PROOF_OF_PURCHASE, 'AA')" : "UPPER(W.PROOF_OF_PURCHASE) LIKE UPPER('%" + proofOfPurches + "%')";
        String binNameQry = (binName.equalsIgnoreCase("all")) ? "NVL(B.BIS_NAME, 'AA') = NVL(B.BIS_NAME, 'AA')" : "UPPER(B.BIS_NAME) LIKE UPPER('%" + binName + "%')";
        String warrantyStatusMsgQry = (warrantyStatusMsg.equalsIgnoreCase("all")) ? "NVL(M.DESCRIPTION, 'AA') = NVL(M.DESCRIPTION, 'AA')" : "UPPER(M.DESCRIPTION) LIKE UPPER('%" + warrantyStatusMsg + "%')";
        String delayDateQry = (delayDate == 0) ? "" : "AND TRUNC(SYSDATE - W.DELIVERY_DATE) = "+delayDate+"";

        try {

            String seletColunm = "W.WORK_ORDER_NO,"
                    + "W.CUSTOMER_NAME,"
                    + "W.CUSTOMER_ADDRESS,"
                    + "W.TELEPHONE_NO,"
                    + "W.EMAIL,"
                    + "W.CUSTOMER_NIC,"
                    + "W.IMEI_NO,"
                    + "W.PRODUCT,"
                    + "W.BRAND,"
                    + "W.MODEL_NO,"
                    + "W.ACCESSORIES_RETAINED,"
                    + "W.DEFECTS,"
                    + "W.RCC_REFERENCE,"
                    + "TO_CHAR(W.DATE_OF_SALE,'YYYY/MM/DD') DATE_OF_SALE,"
                    + "W.PROOF_OF_PURCHASE,"
                    + "W.BIS_ID,"
                    + "W.SCHEME_ID,"
                    + "W.WORK_ORD_STATUS,"
                    + "W.WARRANTY_VRIF_TYPE,"
                    + "W.NON_WARRANTY_VRIF_TYPE,"
                    + "W.TECHNICIAN,"
                    + "W.ACTION_TAKEN,"
                    + "W.LEVEL_ID,"
                    + "W.REMARKS,"
                    + "W.REPAIR_STATUS,"
                    + "W.DELIVERY_TYPE,"
                    + "W.TRANSFER_LOCATION,"
                    + "W.COURIER_NO,"
                    + "TO_CHAR(W.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "W.GATE_PASS,"
                    + "W.STATUS,"
                    + "TRUNC(SYSDATE - W.DELIVERY_DATE) DELAY_DATE,"
                    + "W.NON_WARRANTY_REMARKS,"
                    + "W.CUSTOMER_COMPLAIN,"
                    + "TO_CHAR(W.DATE_INSERTED,'YYYY/MM/DD') WO_DATE,"
                    + "L.LEVEL_NAME,"
                    + "M.DESCRIPTION,"
                    + "S.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME,"
                    + "D.MAJ_CODE_DESC,"
                    + "W.SERVICE_BIS_ID,"
                    + "B1.BIS_NAME SERVICE_NAME,"
                    + "TO_CHAR(NVL(W.DATE_MODIFIED,W.DATE_INSERTED),'YYYY/MM/DD') REPAIR_DATE,"
                    + "S2.DESCRIPTION REPAIR_MSG";

            String whereClous = "W.LEVEL_ID  = L.LEVEL_ID (+)  "
                    + "AND W.WARRANTY_VRIF_TYPE = M.CODE (+) "
                    + "AND M.DOMAIN_NAME = 'WARANTY'  "
                    + "AND W.MODEL_NO = S.MODEL_NO (+) "
                    + "AND W.BIS_ID = B.BIS_ID (+) "
                    + "AND W.DEFECTS = D.MAJ_CODE (+) "
                    + "AND W.SERVICE_BIS_ID = B1.BIS_ID (+) "
                    + "AND W.REPAIR_STATUS = S2.CODE (+) "
                    + "AND S2.DOMAIN_NAME = 'RP_STATUS' "
                    + "AND W.STATUS != 9    "
                    + "    AND    W.BIS_ID=" + bisId + "    "
                    + "     AND W.WORK_ORDER_NO       "
                    + "     NOT IN ( SELECT WORK_ORDER_NO       "
                    + "     FROM    SD_DEVICE_EXCHANGE )        "
                    + "     AND W.WORK_ORDER_NO     "
                    + "     IN ( SELECT WORK_ORDER_NO       "
                    + "              FROM    SD_WORK_ORDERS     "
                    + "              WHERE   STATUS = 3   )"
                    + "AND "+workOrderNoQry+" "
                    + "AND "+customerNameQry+""
                    + "AND "+workTelephoneNoQry+""
                    + "AND "+customerAddressQry+""
                    + "AND "+emailQry+""
                    + "AND "+customerNicQry+""
                    + "AND "+imeiNoQry+""
                    + "AND "+productcQry+""
                    + "AND "+brandQry+""
                    + "AND "+modleDescQry+""
                    + "AND "+defectDescDescQry+""
                    + "AND "+rccReferenceQry+""
                    + ""+dateOfSaleQry+""
                    + "AND "+proofOfPurchesQry+""
                    + "AND "+binNameQry+""
                    + "AND "+warrantyStatusMsgQry+""
                    + ""+delayDateQry+"";

            String table = "SD_WORK_ORDERS W,SD_REPAIR_LEVELS L,"
                    + "SD_MDECODE M,SD_MODEL_LISTS S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_DEFECT_CODES D,SD_BUSINESS_STRUCTURES B1,SD_MDECODE S2";
           
            String qry = "  SELECT " + seletColunm + "    "
                    + "     FROM   " + table + "   "
                    + "     WHERE  " + whereClous + "       ";

            System.out.println("Queryyy"+ qry);
            ResultSet rs = dbCon.search(con, qry);
            logger.info("Get Data to Resultset...");

            ArrayList<ImeiMaster> checkImeiWarrantyList = DeviceExchangeController.checkImeiUnderWarranty();

            while (rs.next()) {

                for (ImeiMaster i : checkImeiWarrantyList) {
                    if (i.getImeiNo().equalsIgnoreCase(rs.getString("IMEI_NO")) || rs.getInt("WARRANTY_VRIF_TYPE") == 2) {

                        WorkOrder wo = new WorkOrder();
                        wo.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                        wo.setCustomerName(rs.getString("CUSTOMER_NAME"));
                        wo.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
                        wo.setWorkTelephoneNo(rs.getString("TELEPHONE_NO"));
                        wo.setEmail(rs.getString("EMAIL"));
                        wo.setCustomerNic(rs.getString("CUSTOMER_NIC"));
                        wo.setImeiNo(rs.getString("IMEI_NO"));
                        wo.setProduct(rs.getString("PRODUCT"));
                        wo.setBrand(rs.getString("BRAND"));
                        wo.setModleNo(rs.getInt("MODEL_NO"));
                        wo.setAssessoriesRetained(rs.getString("ACCESSORIES_RETAINED"));
                        wo.setDefectNo(rs.getString("DEFECTS"));
                        wo.setRccReference(rs.getString("RCC_REFERENCE"));
                        wo.setDateOfSale(rs.getString("DATE_OF_SALE"));
                        wo.setProofOfPurches(rs.getString("PROOF_OF_PURCHASE"));
                        wo.setBisId(rs.getInt("BIS_ID"));
                        wo.setSchemeId(rs.getInt("SCHEME_ID"));
                        wo.setWorkOrderStatus(rs.getInt("WORK_ORD_STATUS"));
                        wo.setWarrentyVrifType(rs.getInt("WARRANTY_VRIF_TYPE"));
                        wo.setNonWarrentyVrifType(rs.getInt("NON_WARRANTY_VRIF_TYPE"));
                        wo.setTecnician(rs.getString("TECHNICIAN"));
                        wo.setActionTaken(rs.getString("ACTION_TAKEN"));
                        wo.setLevelId(rs.getInt("LEVEL_ID"));
                        wo.setRemarks(rs.getString("REMARKS"));
                        wo.setRepairStatus(rs.getInt("REPAIR_STATUS"));
                        wo.setDeleveryType(rs.getInt("DELIVERY_TYPE"));
                        wo.setTransferLocation(rs.getInt("TRANSFER_LOCATION"));
                        wo.setCourierNo(rs.getString("COURIER_NO"));
                        wo.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                        wo.setGatePass(rs.getString("GATE_PASS"));
                        wo.setStatus(rs.getInt("STATUS"));
                        wo.setDelayDate(rs.getString("DELAY_DATE"));
                        if (rs.getInt("STATUS") == 1) {
                            wo.setStatusMsg("Open");
                        } else if (rs.getInt("STATUS") == 2) {
                            wo.setStatusMsg("in Progress");
                        } else if (rs.getInt("STATUS") == 3) {
                            wo.setStatusMsg("Close");
                        } else {
                            wo.setStatusMsg("None");
                        }
                        wo.setNonWarrantyRemarks(rs.getString("NON_WARRANTY_REMARKS"));
                        wo.setCustomerComplain(rs.getString("CUSTOMER_COMPLAIN"));
                        wo.setCreateDate(rs.getString("WO_DATE"));
                        wo.setWarrantyStatusMsg(rs.getString("DESCRIPTION"));
                        wo.setLevelName(rs.getString("LEVEL_NAME"));
                        wo.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                        wo.setBinName(rs.getString("BIS_NAME"));
                        wo.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                        wo.setServiceBisId(rs.getInt("SERVICE_BIS_ID"));
                        wo.setServiceBisDesc(rs.getString("SERVICE_NAME"));
                        wo.setRegisterDate(rs.getString("DATE_OF_SALE"));
                        wo.setRepairDate(rs.getString("REPAIR_DATE"));
                        wo.setRepairStatusMsg(rs.getString("REPAIR_MSG"));
                        wo.setEstimateStatus(rs.getInt("WARRANTY_VRIF_TYPE"));  //This is used Edit screen of WorkOrder
                        wo.setExpireStatus(rs.getInt("WARRANTY_VRIF_TYPE"));

                        workOrderList.add(wo);

                        break;
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(workOrderList.size());
        mw.setData(workOrderList);
        return mw;
    }

    public static ArrayList<ImeiMaster> checkImeiOutofWarranty() {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String qry = "  SELECT D.IMEI_NO  "
                    + "     FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A, SD_WARRENTY_REGISTRATION R     "
                    + "     WHERE D.MODEL_NO = M.MODEL_NO (+)           "
                    + "     AND   D.IMEI_NO = R.IMEI_no (+)             "
                    + "     AND M.WRN_SCHEME_ID = W.SCHEME_ID (+)       "
                    + "     AND D.IMEI_NO = A.IMEI (+)                  "
                    + "     AND  TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') < SYSDATE       ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            while (rs_master_cnt.next()) {
                ImeiMaster imie = new ImeiMaster();

                imie.setImeiNo(rs_master_cnt.getString("IMEI_NO"));
                imeiMasterList.add(imie);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }

        dbCon.ConectionClose(con);
        return imeiMasterList;

    }
    
    
     public static ArrayList<ImeiMaster> checkImeiUnderWarranty() {
        logger.info("getImeiMasterDetailswithWarranty Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            String qry = "  SELECT D.IMEI_NO  "
                    + "     FROM SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_WARRANTY_SCHEMES W,SD_WARRANTY_ADJUSTMENT A, SD_WARRENTY_REGISTRATION R     "
                    + "     WHERE D.MODEL_NO = M.MODEL_NO (+)           "
                    + "     AND   D.IMEI_NO = R.IMEI_no (+)             "
                    + "     AND M.WRN_SCHEME_ID = W.SCHEME_ID (+)       "
                    + "     AND D.IMEI_NO = A.IMEI (+)                  "
                    + "     AND  TO_DATE(TO_CHAR(((TO_DATE(TO_CHAR(R.REGISTER_DATE,'YYYY/MM/DD'),'YYYY/MM/DD'))+(NVL(W.PERIOD,0) + NVL(A.EXTENDED_DAYS,0))),'YYYY/MM/DD'),'YYYY/MM/DD') >= SYSDATE       ";

            ResultSet rs_master_cnt = dbCon.search(con, qry);

            while (rs_master_cnt.next()) {
                ImeiMaster imie = new ImeiMaster();

                imie.setImeiNo(rs_master_cnt.getString("IMEI_NO"));
                imeiMasterList.add(imie);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetailswithWarranty  " + ex.getMessage());
        }

        dbCon.ConectionClose(con);
        return imeiMasterList;

    }

     public static int approveRejectExchange(String refNo, int status){
         int flag = 1;
         try{
             DbCon dbCon = new DbCon();
             Connection con = dbCon.getCon();
             String qry = " UPDATE SD_DEVICE_EXCHANGE "
                     + "    SET STATUS = "+status+" "
                     + "    WHERE REFERENCE_NO='"+refNo+"' ";
             
             PreparedStatement ps_up_device = dbCon.prepare(con,qry);
             ps_up_device.executeUpdate();
             
             
         }catch(Exception e){
             flag = 9;
             e.printStackTrace();
         }
         return flag;
     }

}
