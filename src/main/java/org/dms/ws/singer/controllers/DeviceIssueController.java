/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.ejb.Singleton;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.DSRReportEntity;
import org.dms.ws.singer.entities.DeviceIssue;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class DeviceIssueController {

    public static MessageWrapper getDeviceIssueList(int issue_no,
            int distributer_id,
            int dsr_id,
            String issue_date,
            double price,
            double distributer_margin,
            double deler_margin,
            double discount,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getDeviceIssueList Method Call...........");
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<DeviceIssue> deviceIssueList = new ArrayList<>();
        int totalrecode = 0;
        try {
            order = DBMapper.deviceIssue(order);

            String ItemNo = ((issue_no == 0) ? "" : "AND D.ISSUE_NO = " + issue_no + " ");
            String DistributerId = ((distributer_id == 0) ? "" : "AND D.DISTRIBUTER_ID = " + distributer_id + " ");
            String DSRId = ((dsr_id == 0) ? "" : "AND D.DSR_ID = " + dsr_id + " ");
            String IssueDate = (issue_date.equalsIgnoreCase("all") ? "NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(D.ISSUE_DATE,'YYYY/MM/DD') = TO_DATE('%" + issue_date + "%','YYYY/MM/DD')");
            String Price = (price == 0) ? "" : "AND D.PRICE = '" + price + "' ";
            String DistributorMargin = (distributer_margin == 0) ? "" : "AND D.DISTRIBUTOR_MARGIN = " + distributer_margin + " ";
            String DelerMargin = (deler_margin == 0) ? "" : "AND D.DEALER_MARGIN = " + deler_margin + " ";
            String Discount = (discount == 0) ? "" : "AND D.DISCOUNT = " + discount + " ";
            String Status = (status == 0) ? "" : "AND D.STATUS = " + status + " ";
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.ISSUE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "D.ISSUE_NO,D.DISTRIBUTER_ID,"
                    + "D.DSR_ID,TO_CHAR(D.ISSUE_DATE,'YYYY/MM/DD') ISSUE_DATE,"
                    + "D.PRICE,D.DISTRIBUTOR_MARGIN,"
                    + "D.DEALER_MARGIN,D.DISCOUNT,D.STATUS,"
                    + "B1.BIS_NAME FROM_BIS,"
                    + "B2.BIS_NAME TO_BIS,D.REMARKS,"
                    + "(SELECT  COUNT(*) FROM SD_DEVICE_ISSUE_DTL WHERE ISSUE_NO = D.ISSUE_NO) CNTDTL";

            String whereClous = "D.DISTRIBUTER_ID = B1.BIS_ID "
                    + "AND D.DSR_ID = B2.BIS_ID "
                    + "AND D.STATUS != 9 "
                    + "AND " + IssueDate + " "
                    + "" + ItemNo + " "
                    + "" + DistributerId + " "
                    + "" + DSRId + " "
                    + "" + Price + " "
                    + "" + DistributorMargin + " "
                    + "" + DelerMargin + " "
                    + "" + Discount + " "
                    + "" + Status + "";

            String table = "SD_DEVICE_ISSUE D,SD_BUSINESS_STRUCTURES B1,SD_BUSINESS_STRUCTURES B2";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + table + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                DeviceIssue di = new DeviceIssue();
                totalrecode = rs.getInt("CNT");
                di.setIssueNo(rs.getInt("ISSUE_NO"));
                di.setDistributerID(rs.getInt("DISTRIBUTER_ID"));
                di.setdSRId(rs.getInt("DSR_ID"));
                di.setIssueDate(rs.getString("ISSUE_DATE"));
                di.setPrice(rs.getDouble("PRICE"));
                di.setDistributorMargin(rs.getDouble("DISTRIBUTOR_MARGIN"));
                di.setDelerMargin(rs.getDouble("DEALER_MARGIN"));
                di.setDiscount(rs.getDouble("DISCOUNT"));
                di.setStatus(rs.getInt("STATUS"));
                di.setFromName(rs.getString("FROM_BIS"));
                di.setToName(rs.getString("TO_BIS"));
                di.setRemarks(rs.getString("REMARKS"));

                if (rs.getInt("STATUS") == 1) {
                    di.setStatusMsg("Pending");
                } else if (rs.getInt("STATUS") == 2) {
                    di.setStatusMsg("Complete");
                } else if (rs.getInt("STATUS") == 3) {
                    di.setStatusMsg("Accepted");
                } else {
                    di.setStatusMsg("None");
                }

                di.setDtlCnt(rs.getInt("CNTDTL"));

                deviceIssueList.add(di);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error Retriview Device Issue......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }
        mw.setData(deviceIssueList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addDeviceIssue(DeviceIssue deviceissue_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addDeviceIssue Method Call.............................");
        int result = 0;
        int max_issue_no = 0;
        int imei_al_issue = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<DeviceIssueIme> device_Ime_check = new ArrayList<DeviceIssueIme>();
            device_Ime_check = deviceissue_info.getDeviceImei();

            ArrayList<String> requestList = new ArrayList<>();
            ArrayList<String> temrequestList = new ArrayList<>();
            for (DeviceIssueIme di : device_Ime_check) {
                requestList.add(di.getImeiNo());
                temrequestList.add(di.getImeiNo());
            }

            ArrayList<String> issueList = new ArrayList<>();

            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
                    + "FROM SD_DEVICE_ISSUE_DTL "
                    + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE  DISTRIBUTER_ID = " + deviceissue_info.getDistributerID() + ") "
                    + "AND STATUS != 3");

            while (rs_issue_cnt.next()) {
                String imei = rs_issue_cnt.getString("IMEI_NO");
                issueList.add(imei);
            }

            for (String s : requestList) {
                if (issueList.contains(s)) {
                    return_imei += s + ",";
                    imei_al_issue = 1;
                }
            }

            if (imei_al_issue == 1) {
                result = 3;
            } else {

                ArrayList<String> locationList = new ArrayList<>();

                ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
                        + "FROM SD_DEVICE_MASTER "
                        + "WHERE BIS_ID = " + deviceissue_info.getDistributerID() + " "
                        + "AND STATUS = 1");

                while (rs_ime_cnt.next()) {
                    String imei = rs_ime_cnt.getString("IMEI_NO");
                    locationList.add(imei);
                }
                // rs_ime_cnt.close();

                temrequestList.removeAll(locationList);

                if (temrequestList.size() > 0) {
                    for (String s : temrequestList) {
                        return_imei += s + ",";
                    }
                    result = 2;
                } else {

                    ArrayList<String> creditNoteList = new ArrayList<>();
                    ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                            + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                            + "WHERE STATUS = 1");

                    while (rs_crd_cnt.next()) {
                        String imei = rs_crd_cnt.getString("IMEI_NO");
                        creditNoteList.add(imei);
                    }

                    for (String s : requestList) {

                        if (creditNoteList.contains(s)) {

                            return_imei += s + ",";
                            imei_crdit = 1;
                        }
                    }

                    if (imei_crdit == 1) {
                        result = 4;
                    } else {
                        ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(ISSUE_NO),0) MAX FROM SD_DEVICE_ISSUE");

                        while (rs_max.next()) {
                            max_issue_no = rs_max.getInt("MAX");
                        }

                        String insertColumns = ""
                                + "DISTRIBUTER_ID,"
                                + "DSR_ID,"
                                + "ISSUE_DATE,"
                                + "PRICE,"
                                + "DISTRIBUTOR_MARGIN,"
                                + "DEALER_MARGIN,"
                                + "DISCOUNT,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,REMARKS";

                        String insertValue = "?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,?,?,?,SYSDATE,?";

                        String deviceDetailsColumn = "SEQ_NO,"
                                + "ISSUE_NO,"
                                + "IMEI_NO,"
                                + "MODEL_NO,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,"
                                + "PRICE,"
                                + "MARGIN";

                        try {

                            PreparedStatement ps_in_device_hdr = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE(" + insertColumns + ") "
                                    + "VALUES(" + insertValue + ")");

                            max_issue_no = max_issue_no + 1;

//                            ps_in_device_hdr.setInt(1, max_issue_no);
                            ps_in_device_hdr.setInt(1, deviceissue_info.getDistributerID());
                            ps_in_device_hdr.setInt(2, deviceissue_info.getdSRId());
                            ps_in_device_hdr.setString(3, deviceissue_info.getIssueDate());
                            ps_in_device_hdr.setDouble(4, deviceissue_info.getPrice());
                            ps_in_device_hdr.setDouble(5, deviceissue_info.getDistributorMargin());
                            ps_in_device_hdr.setDouble(6, deviceissue_info.getDelerMargin());
                            ps_in_device_hdr.setDouble(7, deviceissue_info.getDiscount());

                            //ps_in_device_hdr.setInt(9, deviceissue_info.getStatus());
                            if (deviceissue_info.getType().equals("2")) {
                                ps_in_device_hdr.setInt(8, 3);
                            } else {
                                ps_in_device_hdr.setInt(8, deviceissue_info.getStatus());
                            }

                            ps_in_device_hdr.setString(9, user_id);
                            ps_in_device_hdr.setString(10, deviceissue_info.getRemarks());

                            ps_in_device_hdr.executeUpdate();
                            logger.info("Device Issue Details Inseted.....");
                            result = 1;

                        } catch (Exception e) {
                            logger.info("Error Device Issue Details Insert....." + e.toString());
                            result = 9;
                            e.printStackTrace();
                        }

                        try {

                            ArrayList<DeviceIssueIme> dIme = new ArrayList<DeviceIssueIme>();
                            dIme = deviceissue_info.getDeviceImei();
                            if (dIme.size() > 0) {

                                int max_device_dtl = 0;
                                int issue_no = 0;

                                ResultSet issue_no_dtl = dbCon.search(con, "select DEVICE_ISU_ID_SEQ.currval from dual");

                                while (issue_no_dtl.next()) {
                                    issue_no = issue_no_dtl.getInt("CURRVAL");
                                }

                                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                        + "FROM SD_DEVICE_ISSUE_DTL "
                                        + "WHERE ISSUE_NO = " + issue_no + "");
                                
                                while (rs_max_dtl.next()) {                                    
                                    max_device_dtl = rs_max_dtl.getInt("MAX");
                                }

                                // rs_max_dtl.close();
                                String deviceIsuueImeValue = "?,?,?,?,2,?,SYSDATE,?,?";

                                // ArrayList<DeviceIssueIme> TemdIme = new ArrayList<DeviceIssueIme>();
//                                ResultSet rs_master_modle = dbCon.search(con,"SELECT IMEI_NO,MODEL_NO "
//                                        + "FROM SD_DEVICE_MASTER");
//                                while (rs_master_modle.next()) {
//                                    DeviceIssueIme dii = new DeviceIssueIme();
//                                    dii.setImeiNo(rs_master_modle.getString("IMEI_NO"));
//                                    dii.setModleNo(rs_master_modle.getInt("MODEL_NO"));
//                                    TemdIme.add(dii);
//                                }
                                // rs_master_modle.close();
                                PreparedStatement ps_in_device_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE_DTL(" + deviceDetailsColumn + ") "
                                        + "VALUES(" + deviceIsuueImeValue + ")");

                                PreparedStatement ps_up_device_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                        + "SET NET_PRICE = ? WHERE IMEI_NO = ?");

                                for (DeviceIssueIme di : dIme) {

                                    max_device_dtl = max_device_dtl + 1;
//                                    max_device_dtl = max_device_dtl;
                                    ps_in_device_dtl.setInt(1, max_device_dtl);
                                    ps_in_device_dtl.setInt(2, issue_no);
                                    ps_in_device_dtl.setString(3, di.getImeiNo());
                                    ps_in_device_dtl.setInt(4, di.getModleNo());
                                    ps_in_device_dtl.setString(5, user_id);
                                    ps_in_device_dtl.setDouble(6, di.getSalesPrice());
                                    ps_in_device_dtl.setDouble(7, di.getMargin());
                                    ps_in_device_dtl.addBatch();

                                    ps_up_device_master.setDouble(1, di.getSalesPrice());
                                    ps_up_device_master.setString(2, di.getImeiNo());
                                    ps_up_device_master.addBatch();
                                }
                                ps_in_device_dtl.executeBatch();
                                ps_up_device_master.executeBatch();

                                logger.info("Device Issue Details IME  Data Inserted........");
                                result = 1;
                            }

                        } catch (Exception e) {
                            logger.info("Error in Device Issue Details IME  Data Inserte....." + e.toString());
                            e.printStackTrace();
                            result = 9;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            logger.info("Error addDeviceIssue Method..:" + ex.toString());
            ex.printStackTrace();
        }
        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnRefNo(String.valueOf(max_issue_no));
            vw.setReturnMsg("Device Issue Details Insert Successfully");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            //vw.setReturnMsg("This IMEI " + return_imei + " is not in Your Location");
            vw.setReturnMsg(return_imei + " Belongs to another Location.");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            //vw.setReturnMsg("Already Issue  " + return_imei + " IMEI Number in List");
            vw.setReturnMsg(return_imei + " Already Issued.");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            //vw.setReturnMsg("This  " + return_imei + " IMEI No have a Credit Note");
            vw.setReturnMsg(return_imei + " Belongs to another Location.");
        } else if (con == null) {
            vw.setReturnFlag("1000000");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Device Issue Details Insert Fail");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addDeviceIssue123(DeviceIssue deviceissue_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addDeviceIssue Method Call.............................");
        int result = 0;
        int max_issue_no = 0;
        int imei_al_issue = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<DeviceIssueIme> device_Ime_check = new ArrayList<DeviceIssueIme>();
            device_Ime_check = deviceissue_info.getDeviceImei();

            ArrayList<String> requestList = new ArrayList<>();
            ArrayList<String> temrequestList = new ArrayList<>();
            for (DeviceIssueIme di : device_Ime_check) {
                requestList.add(di.getImeiNo());
                temrequestList.add(di.getImeiNo());
            }

            ArrayList<String> locationList = new ArrayList<>();

            ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE BIS_ID = " + deviceissue_info.getDistributerID() + " "
                    + "AND STATUS = 1");

            while (rs_ime_cnt.next()) {
                String imei = rs_ime_cnt.getString("IMEI_NO");
                locationList.add(imei);
            }

            temrequestList.removeAll(locationList);

            if (temrequestList.size() > 0) {
                for (String s : temrequestList) {
                    return_imei += s + ",";
                }
                result = 2;
            } else {

                ArrayList<String> creditNoteList = new ArrayList<>();
                ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                        + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                        + "WHERE STATUS = 1");

                while (rs_crd_cnt.next()) {
                    String imei = rs_crd_cnt.getString("IMEI_NO");
                    creditNoteList.add(imei);
                }

                for (String s : requestList) {

                    if (creditNoteList.contains(s)) {

                        return_imei += s + ",";
                        imei_crdit = 1;
                    }
                }

                if (imei_crdit == 1) {
                    result = 4;
                } else {

                    ArrayList<String> issueList = new ArrayList<>();

                    ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
                            + "FROM SD_DEVICE_ISSUE_DTL "
                            + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE  DISTRIBUTER_ID = " + deviceissue_info.getDistributerID() + ") "
                            + " AND STATUS != 3");

                    while (rs_issue_cnt.next()) {
                        String imei = rs_issue_cnt.getString("IMEI_NO");
                        issueList.add(imei);
                    }

                    for (String s : requestList) {
                        if (issueList.contains(s)) {
                            return_imei += s + ",";
                            imei_al_issue = 1;
                        }
                    }

                    if (imei_al_issue == 1) {
                        result = 3;
                    } else {

                        ResultSet rs_max = dbCon.search(con, "SELECT NVL(MAX(ISSUE_NO),0) MAX FROM SD_DEVICE_ISSUE");

                        while (rs_max.next()) {
                            max_issue_no = rs_max.getInt("MAX");
                        }

                        String insertColumns = "ISSUE_NO,"
                                + "DISTRIBUTER_ID,"
                                + "DSR_ID,"
                                + "ISSUE_DATE,"
                                + "PRICE,"
                                + "DISTRIBUTOR_MARGIN,"
                                + "DEALER_MARGIN,"
                                + "DISCOUNT,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,REMARKS";

                        String insertValue = "?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,?,?,?,SYSDATE,?";

                        String deviceDetailsColumn = "SEQ_NO,"
                                + "ISSUE_NO,"
                                + "IMEI_NO,"
                                + "MODEL_NO,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED,"
                                + "PRICE,"
                                + "MARGIN";

                        try {

                            PreparedStatement ps_in_device_hdr = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE(" + insertColumns + ") "
                                    + "VALUES(" + insertValue + ")");

                            max_issue_no = max_issue_no + 1;

                            ps_in_device_hdr.setInt(1, max_issue_no);
                            ps_in_device_hdr.setInt(2, deviceissue_info.getDistributerID());
                            ps_in_device_hdr.setInt(3, deviceissue_info.getdSRId());
                            ps_in_device_hdr.setString(4, deviceissue_info.getIssueDate());
                            ps_in_device_hdr.setDouble(5, deviceissue_info.getPrice());
                            ps_in_device_hdr.setDouble(6, deviceissue_info.getDistributorMargin());
                            ps_in_device_hdr.setDouble(7, deviceissue_info.getDelerMargin());
                            ps_in_device_hdr.setDouble(8, deviceissue_info.getDiscount());
                            ps_in_device_hdr.setInt(9, deviceissue_info.getStatus());
                            ps_in_device_hdr.setString(10, user_id);
                            ps_in_device_hdr.setString(11, deviceissue_info.getRemarks());

                            ps_in_device_hdr.executeUpdate();
                            logger.info("Device Issue Details Inseted.....");
                            result = 1;

                        } catch (Exception e) {
                            logger.info("Error Device Issue Details Insert....." + e.toString());
                            result = 9;
                            e.printStackTrace();
                        }

                        try {

                            ArrayList<DeviceIssueIme> dIme = new ArrayList<DeviceIssueIme>();
                            dIme = deviceissue_info.getDeviceImei();
                            if (dIme.size() > 0) {

                                int max_device_dtl = 0;
                                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                        + "FROM SD_DEVICE_ISSUE_DTL "
                                        + "WHERE ISSUE_NO = " + max_issue_no + "");

                                while (rs_max_dtl.next()) {
                                    max_device_dtl = rs_max_dtl.getInt("MAX");
                                }

                                String deviceIsuueImeValue = "?,?,?,?,2,?,SYSDATE,?,?";

                                PreparedStatement ps_in_device_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE_DTL(" + deviceDetailsColumn + ") "
                                        + "VALUES(" + deviceIsuueImeValue + ")");

                                PreparedStatement ps_up_device_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                        + "SET NET_PRICE = ? WHERE IMEI_NO = ?");

                                for (DeviceIssueIme di : dIme) {

                                    max_device_dtl = max_device_dtl + 1;
                                    ps_in_device_dtl.setInt(1, max_device_dtl);
                                    ps_in_device_dtl.setInt(2, max_issue_no);
                                    ps_in_device_dtl.setString(3, di.getImeiNo());
                                    ps_in_device_dtl.setInt(4, di.getModleNo());
                                    ps_in_device_dtl.setString(5, user_id);
                                    ps_in_device_dtl.setDouble(6, di.getSalesPrice());
                                    ps_in_device_dtl.setDouble(7, di.getMargin());
                                    ps_in_device_dtl.addBatch();

                                    ps_up_device_master.setDouble(1, di.getSalesPrice());
                                    ps_up_device_master.setString(2, di.getImeiNo());
                                    ps_up_device_master.addBatch();
                                }
                                ps_in_device_dtl.executeBatch();
                                ps_up_device_master.executeBatch();

                                logger.info("Device Issue Details IME  Data Inserted........");
                                result = 1;
                            }

                        } catch (Exception e) {
                            logger.info("Error in Device Issue Details IME  Data Inserte....." + e.toString());
                            e.printStackTrace();
                            result = 9;
                        }

                    }
                }

            }

        } catch (Exception ex) {

            logger.info("Error addDeviceIssue Method..:" + ex.toString());
            ex.printStackTrace();
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Issue Details Insert Successfully");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg(return_imei + " Belongs to another Location.");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg(return_imei + " Already Issued.");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg(return_imei + " Belongs to another Location.");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Device Issue Details Insert Fail");
        }

        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getDeviceIssueDetail(int issue_no) {
        logger.info("getDeviceIssueDetail Method Call...........");
        ArrayList<DeviceIssueIme> deviceImeList = new ArrayList<>();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String whereClous = "   D.MODEL_NO = M.MODEL_NO (+) "
                    + " AND   A.IMEI_NO = D.IMEI_NO "
                    + " AND   D.IMEI_NO = DM.IMEI_NO    "
                    + " AND D.ISSUE_NO = '" + issue_no + "'";

            String ImeColumn = "D.SEQ_NO,"
                    + "D.ISSUE_NO,"
                    + "D.IMEI_NO,"
                    + "D.MODEL_NO,"
                    + "D.STATUS,"
                    + "M.MODEL_DESCRIPTION,"
                    + "M.ERP_PART_NO,"
                    + "DM.SALES_PRICE AS PRICE, "
                    + "D.MARGIN,"
                    + " A.BRAND   ";

            String table = " SD_DEVICE_ISSUE_DTL D,SD_MODEL_LISTS M, SD_DEVICE_MASTER A, SD_DEVICE_MASTER DM ";

            ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM " + table + " WHERE " + whereClous + " ");

            logger.info("Get Data to ResultSet...........");

            while (rs_ime.next()) {
                DeviceIssueIme dim = new DeviceIssueIme();
                dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                dim.setIssueNo(rs_ime.getInt("ISSUE_NO"));
                dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                dim.setStatus(rs_ime.getInt("STATUS"));
                dim.setModleDesc(rs_ime.getString("ERP_PART_NO"));
                dim.setSalesPrice(rs_ime.getDouble("PRICE"));
                dim.setMargin(rs_ime.getDouble("MARGIN"));
                dim.setBrand(rs_ime.getString("BRAND"));
                deviceImeList.add(dim);
            }
            // rs_ime.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Retriview Device Issue Detals......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(1);
        mw.setData(deviceImeList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper editDeviceIssue(DeviceIssue deviceissue_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editDeviceIssue Method Call............");
        int result = 0;
        int imei_al_issue = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<DeviceIssueIme> device_Ime_check = new ArrayList<DeviceIssueIme>();
            device_Ime_check = deviceissue_info.getDeviceImei();

            ArrayList<String> requestList = new ArrayList<>();
            for (DeviceIssueIme di : device_Ime_check) {
                requestList.add(di.getImeiNo());
            }

            ArrayList<String> issueList = new ArrayList<>();

            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
                    + "FROM SD_DEVICE_ISSUE_DTL "
                    + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE  DISTRIBUTER_ID = " + deviceissue_info.getDistributerID() + ") "
                    + "AND ISSUE_NO != " + deviceissue_info.getIssueNo() + " "
                    + "AND STATUS != 3");

            while (rs_issue_cnt.next()) {
                String imei = rs_issue_cnt.getString("IMEI_NO");
                issueList.add(imei);
            }
            //rs_issue_cnt.close();

            for (String s : requestList) {
                if (issueList.contains(s)) {
                    return_imei = return_imei + "," + s;
                    imei_al_issue = 1;
                }
            }
            if (imei_al_issue == 1) {
                result = 3;
            } else {

                ArrayList<String> locationList = new ArrayList<>();

                ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
                        + "FROM SD_DEVICE_MASTER "
                        + "WHERE BIS_ID = " + deviceissue_info.getDistributerID() + " "
                        + "AND STATUS = 1");

                while (rs_ime_cnt.next()) {
                    String imei = rs_ime_cnt.getString("IMEI_NO");
                    locationList.add(imei);
                }
                //rs_ime_cnt.close();

                ArrayList<String> temrequestList = new ArrayList<>();
                temrequestList = requestList;
                temrequestList.removeAll(locationList);

                if (temrequestList.size() > 0) {
                    for (String s : temrequestList) {
                        return_imei = s + ",";
                    }
                    result = 2;
                } else {

                    ArrayList<String> creditNoteList = new ArrayList<>();
                    ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                            + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                            + "WHERE STATUS = 1");

                    while (rs_crd_cnt.next()) {
                        String imei = rs_crd_cnt.getString("IMEI_NO");
                        creditNoteList.add(imei);
                    }
                    //rs_crd_cnt.close();

                    for (String s : requestList) {
                        if (creditNoteList.contains(s)) {
                            return_imei = return_imei + "," + s;
                            imei_crdit = 1;
                        }
                    }

                    if (imei_crdit == 1) {
                        result = 4;
                    } else {

                        try {

                            PreparedStatement ps_up_device = dbCon.prepare(con, "UPDATE SD_DEVICE_ISSUE "
                                    + "SET DISTRIBUTER_ID=?,"
                                    + "DSR_ID=?,"
                                    + "ISSUE_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                                    + "PRICE=?,"
                                    + "DISTRIBUTOR_MARGIN=?,"
                                    + "DEALER_MARGIN=?,"
                                    + "DISCOUNT=?,"
                                    + "STATUS=?,"
                                    + "USER_MODIFIED=?,"
                                    + "DATE_MODIFIED=SYSDATE,"
                                    + "REMARKS=? "
                                    + "WHERE ISSUE_NO=?");

                            ps_up_device.setInt(1, deviceissue_info.getDistributerID());
                            ps_up_device.setInt(2, deviceissue_info.getdSRId());
                            ps_up_device.setString(3, deviceissue_info.getIssueDate());
                            ps_up_device.setDouble(4, deviceissue_info.getPrice());
                            ps_up_device.setDouble(5, deviceissue_info.getDistributorMargin());
                            ps_up_device.setDouble(6, deviceissue_info.getDelerMargin());
                            ps_up_device.setDouble(7, deviceissue_info.getDiscount());
                            ps_up_device.setInt(8, deviceissue_info.getStatus());
                            ps_up_device.setString(9, user_id);
                            ps_up_device.setString(10, deviceissue_info.getRemarks());
                            ps_up_device.setInt(11, deviceissue_info.getIssueNo());

                            ps_up_device.executeUpdate();
                            result = 1;

                        } catch (Exception e) {
                            logger.info("Error in Updating......." + e.getMessage());
                            result = 9;
                        }

                        try {
                            if (deviceissue_info.getDeviceImei().size() > 0) {

                                // deviceissue_info.getDeviceImei().get(0);
                                logger.info("Delete Device Issue Details from Issue No + " + deviceissue_info.getIssueNo() + "");

                                dbCon.save(con, "DELETE FROM SD_DEVICE_ISSUE_DTL WHERE ISSUE_NO = " + deviceissue_info.getIssueNo() + "");

                                String deviceDetailsColumn = "SEQ_NO,"
                                        + "ISSUE_NO,"
                                        + "IMEI_NO,"
                                        + "MODEL_NO,"
                                        + "STATUS,"
                                        + "USER_INSERTED,"
                                        + "DATE_INSERTED,"
                                        + "PRICE,"
                                        + "MARGIN";

//                                ArrayList<DeviceIssueIme> TemdIme = new ArrayList<DeviceIssueIme>();
//
//                                ResultSet rs_master_modle = dbCon.search(con,"SELECT IMEI_NO,MODEL_NO "
//                                        + "FROM SD_DEVICE_MASTER");
//
//                                while (rs_master_modle.next()) {
//                                    DeviceIssueIme dii = new DeviceIssueIme();
//                                    dii.setImeiNo(rs_master_modle.getString("IMEI_NO"));
//                                    dii.setModleNo(rs_master_modle.getInt("MODEL_NO"));
//                                    TemdIme.add(dii);
//                                }
//                                rs_master_modle.close();
                                String deviceIsuueImeValue = "?,?,?,?,2,?,SYSDATE,?,?";

                                int max_device_dtl = 0;
                                ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_DEVICE_ISSUE_DTL WHERE ISSUE_NO = " + deviceissue_info.getIssueNo() + "");

                                while (rs_max_dtl.next()) {
                                    max_device_dtl = rs_max_dtl.getInt("MAX");
                                }
                                rs_max_dtl.close();

                                ArrayList<DeviceIssueIme> deviceImeList = deviceissue_info.getDeviceImei();
                                if (deviceImeList.size() > 0) {

                                    PreparedStatement ps_up_device_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_ISSUE_DTL(" + deviceDetailsColumn + ") "
                                            + "VALUES(" + deviceIsuueImeValue + ")");

                                    PreparedStatement ps_up_device_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                            + "SET NET_PRICE = ? WHERE IMEI_NO = ?");

                                    for (DeviceIssueIme us : deviceImeList) {

                                        max_device_dtl = max_device_dtl + 1;
                                        ps_up_device_dtl.setInt(1, max_device_dtl);
                                        ps_up_device_dtl.setInt(2, deviceissue_info.getIssueNo());
                                        ps_up_device_dtl.setString(3, us.getImeiNo());
                                        ps_up_device_dtl.setInt(4, us.getModleNo());
                                        ps_up_device_dtl.setString(5, user_id);
                                        ps_up_device_dtl.setDouble(6, us.getSalesPrice());
                                        ps_up_device_dtl.setDouble(7, us.getMargin());
                                        ps_up_device_dtl.addBatch();

                                        ps_up_device_master.setDouble(1, us.getSalesPrice());
                                        ps_up_device_master.setString(2, us.getImeiNo());
                                        ps_up_device_master.addBatch();

                                    }
                                    ps_up_device_dtl.executeBatch();
                                    ps_up_device_master.executeBatch();

                                    result = 1;
                                }

                            }

                        } catch (Exception e) {
                            logger.info("Error device Issue IMEI Update...:" + e.toString());
                            result = 9;
                        }
                    }
                }

            }

        } catch (Exception ex) {
            logger.info("Error editDeviceIssue Method.. :" + ex.toString());
            result = 9;
        }
        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, return_imei.length() - 1);
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Issue Update Successfully");
            logger.info("Device Issue Successfully.................");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            //vw.setReturnMsg("This IMEI " + return_imei + " is not in Your Location");
            vw.setReturnMsg(return_imei + " Belongs to another Location.");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            //vw.setReturnMsg("Already Issue  " + return_imei + " IMEI Number in List");
            vw.setReturnMsg(return_imei + "  Already Issued.");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            //vw.setReturnMsg("This  " + return_imei + " IMEI Number have a Credit Note");
            vw.setReturnMsg(return_imei + " Belongs to another Location.");
        } else if (con == null) {
            vw.setReturnFlag("1000000");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Device Issue Update");
            logger.info("Getting Error Device Issue.................");
        }
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return vw;
    }

    public static ArrayList<DSRReportEntity> getModelWiseDSRDeviceIssue(int issueNo) {
        ArrayList<DSRReportEntity> dataList = new ArrayList<DSRReportEntity>();
        try {
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();

            String qry = "  SELECT COUNT(*) MODEL_CNT, D.MODEL_NO, L.ERP_PART_NO, D.STATUS, M.SALES_PRICE AS ITEM_VALUE, (M.SALES_PRICE*COUNT(*)) AS TOT_VAL, B.BIS_NAME DSR, B1.BIS_NAME DISTR  "
                    + "     FROM SD_DEVICE_ISSUE M, SD_DEVICE_ISSUE_DTL D, SD_MODEL_LISTS L, SD_BUSINESS_STRUCTURES B, SD_BUSINESS_STRUCTURES B1, SD_DEVICE_MASTER M     "
                    + "     WHERE M.ISSUE_NO = D.ISSUE_NO   "
                    + "     AND D.MODEL_NO = L.MODEL_NO     "
                    + "     AND M.DSR_ID = B.BIS_ID             "
                    + "     AND M.DISTRIBUTER_ID = B1.BIS_ID    "
                    + "     AND M.IMEI_NO = D.IMEI_NO       "
                    + "     AND D.ISSUE_NO=" + issueNo + "                "
                    + "     GROUP BY D.MODEL_NO, L.ERP_PART_NO, D.STATUS, M.SALES_PRICE,B.BIS_NAME, B1.BIS_NAME  ";

            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                DSRReportEntity data = new DSRReportEntity();
                data.setDistributorName(rs.getString("DISTR"));
                data.setDsrName(rs.getString("DSR"));
                data.setModelCnt(rs.getString("MODEL_CNT"));
                data.setModelNo(rs.getString("MODEL_NO"));
                data.setModelDescription(rs.getString("ERP_PART_NO"));
                data.setStatus(rs.getString("STATUS"));
                data.setPrice(rs.getString("ITEM_VALUE"));
                data.setTotValue(rs.getString("TOT_VAL"));

                dataList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }
}
