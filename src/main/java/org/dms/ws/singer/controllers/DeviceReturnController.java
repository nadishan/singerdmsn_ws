/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.entities.DeviceReturn;
import org.dms.ws.singer.entities.DeviceReturnDetail;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class DeviceReturnController {

    public static MessageWrapper getDeviceReturnList(int returnno,
            int distributor_id,
            int dsrbis_id,
            String distributor_name,
            String dsr_name,
            String returndate,
            double price,
            double distributormargin,
            double dealermargin,
            double discount,
            int status,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getDeviceReturnDetails Method Call....................");
        ArrayList<DeviceReturn> devicReturnList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            order = DBMapper.devicereturn(order);

            String returnNo = (returnno == 0 ? "" : "AND R.RETURN_NO = '" + returnno + "'");
            //String distrId = (distributor_id == 0 ? "" : "AND R.DISTRIBUTER_ID = " + distributor_id + " ");
            //String dsrBisId = (dsrbis_id == 0 ? "" : "AND R.DSR_ID = " + dsrbis_id + " ");

            String distrId = (distributor_id == 0 ? "" : " AND (R.DISTRIBUTER_ID = " + distributor_id + " OR R.DSR_ID = " + distributor_id + ")  ");
            String dsrBisId = (dsrbis_id == 0 ? "" : " AND (R.DSR_ID = " + dsrbis_id + " OR R.DISTRIBUTER_ID = " + dsrbis_id + ") ");

            String distributorId = (distributor_name.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('" + distributor_name + "')");
            String dsrId = (dsr_name.equalsIgnoreCase("all") ? "NVL(B2.BIS_NAME,'AA') = NVL(B2.BIS_NAME,'AA')" : "UPPER(B2.BIS_NAME) LIKE UPPER('" + dsr_name + "')");
            String returnDate = (returndate.equalsIgnoreCase("all") ? "NVL(R.RETURN_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(R.RETURN_DATE,TO_DATE('20100101','YYYYMMDD'))" : "R.RETURN_DATE = TO_DATE('" + returndate + "','YYYY/MM/DD')");
            String Price = (price == 0 ? "" : "AND R.PRICE = '" + price + "' ");
            String distributorMargin = (distributormargin == 0 ? "" : "AND R.DISTRIBUTOR_MARGIN = '" + distributormargin + "'");
            String dealerMargin = (dealermargin == 0 ? "" : "AND R.DEALER_MARGIN = '" + dealermargin + "'");
            String Discount = (discount == 0 ? "" : "AND R.DISCOUNT = '" + discount + "'");
            String Status = (status == 0 ? "" : "AND R.STATUS = '" + status + "'");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY R.RETURN_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "R.RETURN_NO,R.DISTRIBUTER_ID,"
                    + "B1.BIS_NAME DISTRIBUTER_NAME,"
                    + "R.DSR_ID,B2.BIS_NAME DSR_NAME,"
                    + "TO_CHAR(R.RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "R.PRICE,R.DISTRIBUTOR_MARGIN,"
                    + "R.DEALER_MARGIN,R.DISCOUNT,R.STATUS";

            String whereClous = "R.DISTRIBUTER_ID = B1.BIS_ID (+) "
                    + "AND R.DSR_ID = B2.BIS_ID (+)  "
                    + "" + distrId + " "
                    + "" + dsrBisId + " "
                    + "AND " + returnDate + " "
                    + " " + returnNo + " "
                    + " AND " + distributorId + " "
                    + " AND " + dsrId + " "
                    + " " + Price + " "
                    + " " + distributorMargin + " "
                    + " " + dealerMargin + " "
                    + " " + Discount + " "
                    + " " + Status + " ";

            String tables = "SD_DEVICE_RETURN R,SD_BUSINESS_STRUCTURES B1,SD_BUSINESS_STRUCTURES B2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + tables + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//            
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                DeviceReturn dr = new DeviceReturn();

                totalrecode = rs.getInt("CNT");
                dr.setReturnNo(rs.getInt("RETURN_NO"));
                dr.setDistributorId(rs.getInt("DISTRIBUTER_ID"));
                dr.setDistributorName(rs.getString("DISTRIBUTER_NAME"));
                dr.setDsrId(rs.getInt("DSR_ID"));
                dr.setDsrName(rs.getString("DSR_NAME"));
                dr.setReturnDate(rs.getString("RETURN_DATE"));
                dr.setPrice(rs.getDouble("PRICE"));
                dr.setDistributorMargin(rs.getDouble("DISTRIBUTOR_MARGIN"));
                dr.setDealerMargin(rs.getDouble("DEALER_MARGIN"));
                dr.setDiscount(rs.getDouble("DISCOUNT"));
                dr.setStatus(rs.getInt("STATUS"));
                devicReturnList.add(dr);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getDeviceReturnDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(devicReturnList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    //Get head office device return list
    public static MessageWrapper getDeviceReturnListHeadOffice(int returnno,
            int distributor_id,
            int dsrbis_id,
            String distributor_name,
            String dsr_name,
            String returndate,
            double price,
            double distributormargin,
            double dealermargin,
            double discount,
            int status,
            String reason,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getDeviceReturnDetails Method Call....................");
        ArrayList<DeviceReturn> devicReturnList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {

            order = DBMapper.devicereturn(order);

            String returnNo = (returnno == 0 ? "" : "AND R.RETURN_NO = '" + returnno + "'");
            //String distrId = (distributor_id == 0 ? "" : "AND R.DISTRIBUTER_ID = " + distributor_id + " ");
            //String dsrBisId = (dsrbis_id == 0 ? "" : "AND R.DSR_ID = " + dsrbis_id + " ");

            String distrId = (distributor_id == 0 ? "" : " AND (R.DISTRIBUTER_ID = " + distributor_id + " OR R.DSR_ID = " + distributor_id + ")  ");
            String dsrBisId = (dsrbis_id == 0 ? "" : " AND (R.DSR_ID = " + dsrbis_id + " OR R.DISTRIBUTER_ID = " + dsrbis_id + ") ");

            String distributorId = (distributor_name.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA')" : "UPPER(B1.BIS_NAME) LIKE UPPER('" + distributor_name + "')");
            String dsrId = (dsr_name.equalsIgnoreCase("all") ? "NVL(B2.BIS_NAME,'AA') = NVL(B2.BIS_NAME,'AA')" : "UPPER(B2.BIS_NAME) LIKE UPPER('" + dsr_name + "')");
            String returnDate = (returndate.equalsIgnoreCase("all") ? "NVL(R.RETURN_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(R.RETURN_DATE,TO_DATE('20100101','YYYYMMDD'))" : "R.RETURN_DATE = TO_DATE('" + returndate + "','YYYY/MM/DD')");
            String Price = (price == 0 ? "" : "AND R.PRICE = '" + price + "' ");
            String distributorMargin = (distributormargin == 0 ? "" : "AND R.DISTRIBUTOR_MARGIN = '" + distributormargin + "'");
            String dealerMargin = (dealermargin == 0 ? "" : "AND R.DEALER_MARGIN = '" + dealermargin + "'");
            String Discount = (discount == 0 ? "" : "AND R.DISCOUNT = '" + discount + "'");
            String Status = (status == 0 ? "" : "AND R.STATUS = '" + status + "'");
            String Reason = (reason.equalsIgnoreCase("all") ? "NVL(R.REASON,'AA') = NVL(R.REASON,'AA')" : "UPPER(R.REASON) LIKE UPPER('" + reason + "')");
            //String Reason = (reason== 0 ? "" : "AND R.REASON = '" + reason + "'");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY R.RETURN_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "R.RETURN_NO,R.DISTRIBUTER_ID,R.REASON,"
                    + "B1.BIS_NAME DISTRIBUTER_NAME,"
                    + "R.DSR_ID,B2.BIS_NAME DSR_NAME,"
                    + "TO_CHAR(R.RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "R.PRICE,R.DISTRIBUTOR_MARGIN,"
                    + "R.DEALER_MARGIN,R.DISCOUNT,R.STATUS";

            String whereClous = "R.DISTRIBUTER_ID = B1.BIS_ID (+) "
                    + "AND R.DSR_ID = B2.BIS_ID (+)  "
                    + "AND R.DISTRIBUTER_ID = 219"
                    + "" + distrId + " "
                    + "" + dsrBisId + " "
                    + "AND " + returnDate + " "
                    + " " + returnNo + " "
                    + " AND " + distributorId + " "
                    + "AND " + Reason + " "
                    + " AND " + dsrId + " "
                    + " " + Price + " "
                    + " " + distributorMargin + " "
                    + " " + dealerMargin + " "
                    + " " + Discount + " "
                    + " " + Status + " ";
            // + " " + Reason + " ";

            String tables = "SD_DEVICE_RETURN R,SD_BUSINESS_STRUCTURES B1,SD_BUSINESS_STRUCTURES B2";

//            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM " + tables + " WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//            
            System.out.println("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                DeviceReturn dr = new DeviceReturn();

                totalrecode = rs.getInt("CNT");
                dr.setReturnNo(rs.getInt("RETURN_NO"));
                dr.setDistributorId(rs.getInt("DISTRIBUTER_ID"));
                dr.setDistributorName(rs.getString("DISTRIBUTER_NAME"));
                dr.setDsrId(rs.getInt("DSR_ID"));
                dr.setDsrName(rs.getString("DSR_NAME"));
                dr.setReturnDate(rs.getString("RETURN_DATE"));
                dr.setPrice(rs.getDouble("PRICE"));
                dr.setDistributorMargin(rs.getDouble("DISTRIBUTOR_MARGIN"));
                dr.setDealerMargin(rs.getDouble("DEALER_MARGIN"));
                dr.setDiscount(rs.getDouble("DISCOUNT"));
                dr.setStatus(rs.getInt("STATUS"));
                dr.setReason(rs.getString("REASON"));
                devicReturnList.add(dr);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getDeviceReturnDetails Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(devicReturnList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    //End Get head office device return list 
    public static ValidationWrapper addDeviceReturn(DeviceReturn devicReturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addDeviceReturn Method Call.............................");
        int result = 0;
        int ime_un = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
            drList = devicReturn_info.getDeviceReturnList();
            ArrayList<String> temrequestList = new ArrayList<>();

            ArrayList<String> requestList = new ArrayList<>();
            for (DeviceReturnDetail dr : drList) {
                requestList.add(dr.getImeiNo());
                temrequestList.add(dr.getImeiNo());
            }

            ArrayList<String> returnList = new ArrayList<>();

            ResultSet rs_acept_cnt = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_RETURN_DTL WHERE STATUS = 1");

            while (rs_acept_cnt.next()) {
                String imei = rs_acept_cnt.getString("IMEI_NO");
                returnList.add(imei);
            }

            for (String s : requestList) {
                if (returnList.contains(s)) {
                    return_imei += s + ",";
                    ime_un = 1;
                }
            }

            if (ime_un == 1) {
                result = 2;
            } else {

                ArrayList<String> creditNoteList = new ArrayList<>();
                ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                        + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                        + "WHERE STATUS = 1");

                while (rs_crd_cnt.next()) {
                    String imei = rs_crd_cnt.getString("IMEI_NO");
                    creditNoteList.add(imei);
                }

                for (String s : requestList) {

                    if (creditNoteList.contains(s)) {

                        return_imei += s + ",";
                        imei_crdit = 1;
                    }
                }

                if (imei_crdit == 1) {
                    result = 3;

                } else {

                    ArrayList<String> locationList = new ArrayList<>();

                    ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
                            + "FROM SD_DEVICE_MASTER "
                            + "WHERE BIS_ID = " + devicReturn_info.getDsrId() + " "
                            + "AND STATUS = 1");

                    while (rs_ime_cnt.next()) {
                        String imei = rs_ime_cnt.getString("IMEI_NO");
                        locationList.add(imei);
                    }
                    // rs_ime_cnt.close();

                    temrequestList.removeAll(locationList);

                    if (temrequestList.size() > 0) {
                        for (String s : temrequestList) {
                            return_imei += s + ",";
                        }
                        result = 4;
                    } else {

                        int max_ret_heder = 0;
                        ResultSet rs_max_rh = dbCon.search(con, "SELECT NVL(MAX(RETURN_NO),0) MAX FROM SD_DEVICE_RETURN");

                        if (rs_max_rh.next()) {
                            max_ret_heder = rs_max_rh.getInt("MAX");
                        }

                        max_ret_heder = max_ret_heder + 1;

                        String insertColumns = "RETURN_NO,DISTRIBUTER_ID,DSR_ID,RETURN_DATE,PRICE,STATUS,USER_INSERTED,DATE_INSERTED";

                        String insertValue = "?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,SYSDATE";

                        try {

                            PreparedStatement ps_in_rtheader = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN(" + insertColumns + ") "
                                    + "VALUES(" + insertValue + ")");

                            ps_in_rtheader.setInt(1, max_ret_heder);
                            ps_in_rtheader.setInt(2, devicReturn_info.getDistributorId());
                            ps_in_rtheader.setInt(3, devicReturn_info.getDsrId());
                            ps_in_rtheader.setString(4, devicReturn_info.getReturnDate());
                            ps_in_rtheader.setDouble(5, devicReturn_info.getPrice());
                            ps_in_rtheader.setInt(6, devicReturn_info.getStatus());
                            ps_in_rtheader.setString(7, user_id);
                            ps_in_rtheader.executeUpdate();
                            result = 1;
                            logger.info("DeviceReturn Details Inseted.....");
                        } catch (Exception e) {
                            logger.info("Error DeviceReturn Details Insert....." + e.toString());
                            result = 9;
                        }

                        try {

                            String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                            String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";

                            int max_ret_dtl = 0;
                            ResultSet rs_max_rd = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = " + max_ret_heder + "");

                            if (rs_max_rd.next()) {
                                max_ret_dtl = rs_max_rd.getInt("MAX");
                            }

                            PreparedStatement ps_in_return_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN_DTL(" + deviceReturnColumn + ") "
                                    + "VALUES(" + deviceReturnValue + ")");

                            for (DeviceReturnDetail di : drList) {

                                max_ret_dtl = max_ret_dtl + 1;

                                int oldBisId = 0;

                                String qryChkBisId = "SELECT BIS_ID "
                                        + "FROM SD_DEVICE_MASTER "
                                        + "WHERE IMEI_NO = '" + di.getImeiNo() + "' ";

                                ResultSet rsForBisId = dbCon.search(con, qryChkBisId);

                                while (rsForBisId.next()) {
                                    oldBisId = rsForBisId.getInt("BIS_ID");
                                }

                                if (oldBisId == devicReturn_info.getDsrId()) {

                                    ps_in_return_dtl.setInt(1, max_ret_dtl);
                                    ps_in_return_dtl.setInt(2, max_ret_heder);
                                    ps_in_return_dtl.setString(3, di.getImeiNo());
                                    ps_in_return_dtl.setInt(4, di.getModleNo());
                                    if (devicReturn_info.getStatus() == 2) {
                                        ps_in_return_dtl.setInt(5, 3);
                                    } else {
                                        ps_in_return_dtl.setInt(5, 1);
                                    }
                                    ps_in_return_dtl.setString(6, user_id);
                                    ps_in_return_dtl.setDouble(7, di.getSalesPrice());
                                    ps_in_return_dtl.addBatch();

                                }

                            }
                            ps_in_return_dtl.executeBatch();
                            result = 1;
                            logger.info("Device Return Details IME  Data Inserted........");

                            if (devicReturn_info.getStatus() == 2) {
                                PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
                                        + "BIS_ID = ?,"
                                        + "USER_MODIFIED = ?,"
                                        + "DATE_MODIFIED = SYSDATE "
                                        + "WHERE IMEI_NO = ?");

                                for (DeviceReturnDetail dr : drList) {

                                    /////////////////////////////////////////////////////
                                    ////////////////////////////////////////////////////
                                    //check the bi id of the imei
                                    int oldBisId = 0;

                                    String qryChkBisId = "SELECT BIS_ID "
                                            + "FROM SD_DEVICE_MASTER "
                                            + "WHERE IMEI_NO = '" + dr.getImeiNo() + "' ";

                                    ResultSet rsForBisId = dbCon.search(con, qryChkBisId);

                                    while (rsForBisId.next()) {
                                        oldBisId = rsForBisId.getInt("BIS_ID");
                                    }

                                    if (oldBisId == devicReturn_info.getDsrId()) {

                                        ps_up_master.setInt(1, devicReturn_info.getDistributorId());
                                        ps_up_master.setString(2, user_id);
                                        ps_up_master.setString(3, dr.getImeiNo());
                                        ps_up_master.addBatch();

                                    }
                                }
                                ps_up_master.executeBatch();
                                logger.info("Device Master Details IMEI Update........");
                            }

                        } catch (Exception e) {
                            logger.info("Error Device Return Details IME  Data Inserte....." + e.toString());
                            result = 9;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            logger.info("Error addDeviceReturn Method..:" + ex.toString());
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Return Insert Successfully");
            logger.info("Device Return Insert Successfully........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Credit Note List");
            logger.info("This " + return_imei + " IMIE Number in Credit Note List.........");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("This " + return_imei + " IMIE Number Not in Your Location");
            logger.info("This " + return_imei + " IMIE Number Not in Location.........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Device Return Insert Fail");
            logger.info("Device Return Insert Fail..........");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    //Return To Head Office
    public static ValidationWrapper addDeviceReturnHeadOffice(DeviceReturn devicReturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addDeviceReturnHeadOffice Method Call.............................");
        int result = 0;
        int ime_un = 0;
        int imei_crdit = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
            drList = devicReturn_info.getDeviceReturnList();
            ArrayList<String> temrequestList = new ArrayList<>();

            ArrayList<String> requestList = new ArrayList<>();
            for (DeviceReturnDetail dr : drList) {
                requestList.add(dr.getImeiNo());
                temrequestList.add(dr.getImeiNo());
            }

            ArrayList<String> returnList = new ArrayList<>();

            ResultSet rs_acept_cnt = dbCon.search(con, "SELECT IMEI_NO FROM SD_DEVICE_RETURN_DTL WHERE STATUS = 1");

            while (rs_acept_cnt.next()) {
                String imei = rs_acept_cnt.getString("IMEI_NO");
                returnList.add(imei);
            }

            for (String s : requestList) {
                if (returnList.contains(s)) {
                    return_imei += s + ",";
                    ime_un = 1;
                }
            }

            if (ime_un == 1) {
                result = 2;
            } else {

                ArrayList<String> creditNoteList = new ArrayList<>();
                ResultSet rs_crd_cnt = dbCon.search(con, "SELECT IMEI_NO "
                        + "FROM SD_CREDIT_NOTE_ISSUE_DTL  "
                        + "WHERE STATUS = 1");

                while (rs_crd_cnt.next()) {
                    String imei = rs_crd_cnt.getString("IMEI_NO");
                    creditNoteList.add(imei);
                }

                for (String s : requestList) {

                    if (creditNoteList.contains(s)) {

                        return_imei += s + ",";
                        imei_crdit = 1;
                    }
                }

                if (imei_crdit == 1) {
                    result = 3;

                } else {

                    ArrayList<String> locationList = new ArrayList<>();

                    ResultSet rs_ime_cnt = dbCon.search(con, "SELECT IMEI_NO,BIS_ID "
                            + "FROM SD_DEVICE_MASTER "
                            + "WHERE BIS_ID = " + devicReturn_info.getDsrId() + " "
                            + "AND STATUS = 1");

                    while (rs_ime_cnt.next()) {
                        String imei = rs_ime_cnt.getString("IMEI_NO");
                        locationList.add(imei);
                    }
                    // rs_ime_cnt.close();

                    temrequestList.removeAll(locationList);

                    if (temrequestList.size() > 0) {
                        for (String s : temrequestList) {
                            return_imei += s + ",";
                        }
                        result = 4;
                    } else {

                        int max_ret_heder = 0;
                        ResultSet rs_max_rh = dbCon.search(con, "SELECT NVL(MAX(RETURN_NO),0) MAX FROM SD_DEVICE_RETURN");

                        if (rs_max_rh.next()) {
                            max_ret_heder = rs_max_rh.getInt("MAX");
                        }

                        max_ret_heder = max_ret_heder + 1;

                        String insertColumns = "RETURN_NO,DISTRIBUTER_ID,DSR_ID,RETURN_DATE,PRICE,STATUS,USER_INSERTED,DATE_INSERTED,REASON";

                        String insertValue = "?,219,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,SYSDATE,?";

                        try {

                            PreparedStatement ps_in_rtheader = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN(" + insertColumns + ") "
                                    + "VALUES(" + insertValue + ")");

                            ps_in_rtheader.setInt(1, max_ret_heder);
                            // ps_in_rtheader.setInt(2, devicReturn_info.getDistributorId());
                            ps_in_rtheader.setInt(2, devicReturn_info.getDsrId());
                            ps_in_rtheader.setString(3, devicReturn_info.getReturnDate());
                            ps_in_rtheader.setDouble(4, devicReturn_info.getPrice());
                            ps_in_rtheader.setInt(5, devicReturn_info.getStatus());
                            ps_in_rtheader.setString(6, user_id);
                            ps_in_rtheader.setString(7, devicReturn_info.getReason());
                            ps_in_rtheader.executeUpdate();
                            result = 1;
                            logger.info("DeviceReturn Details Inseted.....");
                        } catch (Exception e) {
                            logger.info("Error addDeviceReturnHeadOffice Details Insert....." + e.toString());
                            result = 9;
                        }

                        try {

                            String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                            String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";

                            int max_ret_dtl = 0;
                            ResultSet rs_max_rd = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = " + max_ret_heder + "");

                            if (rs_max_rd.next()) {
                                max_ret_dtl = rs_max_rd.getInt("MAX");
                            }

                            PreparedStatement ps_in_return_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN_DTL(" + deviceReturnColumn + ") "
                                    + "VALUES(" + deviceReturnValue + ")");

                            for (DeviceReturnDetail di : drList) {

                                max_ret_dtl = max_ret_dtl + 1;

                                ps_in_return_dtl.setInt(1, max_ret_dtl);
                                ps_in_return_dtl.setInt(2, max_ret_heder);
                                ps_in_return_dtl.setString(3, di.getImeiNo());
                                ps_in_return_dtl.setInt(4, di.getModleNo());
                                if (devicReturn_info.getStatus() == 2) {
                                    ps_in_return_dtl.setInt(5, 3);
                                } else {
                                    ps_in_return_dtl.setInt(5, 1);
                                }
                                ps_in_return_dtl.setString(6, user_id);
                                ps_in_return_dtl.setDouble(7, di.getSalesPrice());
                                ps_in_return_dtl.addBatch();

                            }
                            ps_in_return_dtl.executeBatch();
                            result = 1;
                            logger.info("Device Return Details IME  Data Inserted........");

                            if (devicReturn_info.getStatus() == 2) {
                                PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
                                        + "BIS_ID = ?,"
                                        + "USER_MODIFIED = ?,"
                                        + "DATE_MODIFIED = SYSDATE "
                                        + "WHERE IMEI_NO = ?");

                                for (DeviceReturnDetail dr : drList) {
                                    ps_up_master.setInt(1, devicReturn_info.getDistributorId());
                                    ps_up_master.setString(2, user_id);
                                    ps_up_master.setString(3, dr.getImeiNo());
                                    ps_up_master.addBatch();
                                }
                                ps_up_master.executeBatch();
                                logger.info("Device Master Details IMEI Update........");
                            }

                        } catch (Exception e) {
                            logger.info("Error Device Return Details IME  Data Inserte....." + e.toString());
                            result = 9;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            logger.info("Error addDeviceReturn Method..:" + ex.toString());
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Return Insert Successfully");
            logger.info("Device Return Insert Successfully........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Credit Note List");
            logger.info("This " + return_imei + " IMIE Number in Credit Note List.........");
        } else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("This " + return_imei + " IMIE Number Not in Your Location");
            logger.info("This " + return_imei + " IMIE Number Not in Location.........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Device Return Insert Fail");
            logger.info("Device Return Insert Fail..........");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }
    //End Return To Head Office

    //==
    //editDeviceReturnDtlHo 
    public static int editDeviceReturnDtlHoRej(int returnNo) {
        logger.info("editDeviceReturnDtlHoRej Method Call............");
        int result = 1;

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {
            String rejectimeis = "UPDATE SD_DEVICE_RETURN  "
                    + "SET STATUS = 3 "
                    + "WHERE RETURN_NO= '" + returnNo + "' AND STATUS =1";

            System.out.println("rejectimeis  " + rejectimeis);

            dbCon.save(con, rejectimeis);

            String rejectimeisdtl = "UPDATE SD_DEVICE_RETURN_DTL  "
                    + "SET STATUS = 3 "
                    + "WHERE RETURN_NO= '" + returnNo + "' AND STATUS =1";

            System.out.println("rejectimeisdtl " + rejectimeisdtl);
            dbCon.save(con, rejectimeisdtl);

        } catch (Exception ex1) {
            // Logger.getLogger(DeviceReturnController.class.getName()).log(Level.SEVERE, null, ex1);
        }

        dbCon.ConectionClose(con);

        return result;

        //return vw;
    }
    //===

    public static MessageWrapper getDeviceReturnDetail(int retun_no) {
        logger.info("getDeviceReturnDetail Method Call...........");
        ArrayList<DeviceReturn> deviceRetunInfo = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "RETURN_NO,"
                    + "DSR_ID,"
                    + "DISTRIBUTER_ID,"
                    + "TO_CHAR(RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "STATUS";

            String whereClous = "RETURN_NO = " + retun_no + "";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM SD_DEVICE_RETURN WHERE " + whereClous + "");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                DeviceReturn di = new DeviceReturn();
                di.setReturnNo(rs.getInt("RETURN_NO"));
                di.setDsrId(rs.getInt("DSR_ID"));
                di.setDistributorId(rs.getInt("DISTRIBUTER_ID"));
                di.setReturnDate(rs.getString("RETURN_DATE"));
                di.setStatus(rs.getInt("STATUS"));

                String ImeColumn = "D.SEQ_NO,"
                        + "D.RETURN_NO,"
                        + "D.IMEI_NO,"
                        + "D.MODEL_NO,"
                        + "D.STATUS,D.PRICE,M.MODEL_DESCRIPTION, NVL(M.SELLING_PRICE,0) AS SELLING_PRICE  ";

                ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " "
                        + "FROM SD_DEVICE_RETURN_DTL D,SD_MODEL_LISTS M, SD_DEVICE_MASTER A "
                        + " WHERE A.MODEL_NO = M.MODEL_NO (+)  "
                        + " AND    D.IMEI_NO=A.IMEI_NO  "
                        + "AND " + whereClous + "");

                ArrayList<DeviceReturnDetail> deviceRetunrDetailList = new ArrayList<>();

                while (rs_ime.next()) {
                    DeviceReturnDetail dim = new DeviceReturnDetail();
                    dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                    dim.setReturnNo(rs_ime.getInt("RETURN_NO"));
                    dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                    dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                    dim.setStatus(rs_ime.getInt("STATUS"));
                    dim.setSalesPrice(rs_ime.getDouble("PRICE"));
                    //  dim.setSalesPrice(rs_ime.getDouble("SELLING_PRICE"));
                    dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));
                    deviceRetunrDetailList.add(dim);
                }

                di.setDeviceReturnList(deviceRetunrDetailList);

                deviceRetunInfo.add(di);
            }

        } catch (Exception ex) {
            logger.info("Error Retriview Device Return......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceRetunInfo.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceRetunInfo);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper checkDayEnd(int bisId) {

        logger.info("checkDayEnd Method Call...........");

        MessageWrapper mw = new MessageWrapper();
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        String status = "";
        int result = 0;
        try {

            String checkColumn = "SELECT * FROM (SELECT STATUS "
                    + "FROM SD_DAY_END_LOG D "
                    + "WHERE BIS_ID = '" + bisId + "' "
                    + "ORDER BY DATE_START DESC) WHERE ROWNUM = 1 ";

            System.out.println("Day end check query " + checkColumn);

            ResultSet rs = dbCon.search(con, checkColumn);

            if (rs.next()) {
                status = rs.getString("STATUS");
            }

            if (status.equals("complete")) {
                result = 1;
            } else if (status.equals("pending")) {
                result = 2;
            } else {
                result = 0;
            }

            mw.setTotalRecords(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mw;
    }

    //Device Return Detail Head Office
    public static MessageWrapper getDeviceReturnDetailHeadOffice(int retun_no) {
        logger.info("getDeviceReturnDetail Method Call...........");
        ArrayList<DeviceReturn> deviceRetunInfo = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "RETURN_NO,"
                    + "DSR_ID,"
                    + "DISTRIBUTER_ID,"
                    + "TO_CHAR(RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "STATUS,"
                    + "REASON";

            String whereClous = "RETURN_NO = " + retun_no + "";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM SD_DEVICE_RETURN WHERE " + whereClous + "");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                DeviceReturn di = new DeviceReturn();
                di.setReturnNo(rs.getInt("RETURN_NO"));
                di.setDsrId(rs.getInt("DSR_ID"));
                di.setDistributorId(rs.getInt("DISTRIBUTER_ID"));
                di.setReturnDate(rs.getString("RETURN_DATE"));
                di.setStatus(rs.getInt("STATUS"));
                di.setReason(rs.getString("REASON"));

                String ImeColumn = "D.SEQ_NO,"
                        + "D.RETURN_NO,"
                        + "D.IMEI_NO,"
                        + "M.MODEL_NO,"
                        + "D.STATUS,D.PRICE,M.MODEL_DESCRIPTION, NVL(M.SELLING_PRICE,0) AS SELLING_PRICE  ";

                System.out.println("HO RETURN DTL QRY...... " + "SELECT " + ImeColumn + " "
                        + "FROM SD_DEVICE_RETURN_DTL D,SD_MODEL_LISTS M, SD_DEVICE_MASTER A "
                        + " WHERE M.MODEL_NO = A.MODEL_NO  "
                        + " AND A.IMEI_NO = D.IMEI_NO "
                        + " AND    D.IMEI_NO=A.IMEI_NO  "
                        + "AND " + whereClous + "");

                ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " "
                        + "FROM SD_DEVICE_RETURN_DTL D,SD_MODEL_LISTS M, SD_DEVICE_MASTER A "
                        + " WHERE M.MODEL_NO = A.MODEL_NO  "
                        + " AND A.IMEI_NO = D.IMEI_NO "
                        + " AND    D.IMEI_NO=A.IMEI_NO  "
                        + "AND " + whereClous + "");

                ArrayList<DeviceReturnDetail> deviceRetunrDetailList = new ArrayList<>();

                while (rs_ime.next()) {
                    DeviceReturnDetail dim = new DeviceReturnDetail();
                    dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                    dim.setReturnNo(rs_ime.getInt("RETURN_NO"));
                    dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                    dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                    dim.setStatus(rs_ime.getInt("STATUS"));
                    dim.setSalesPrice(rs_ime.getDouble("PRICE"));
                    //  dim.setSalesPrice(rs_ime.getDouble("SELLING_PRICE"));
                    dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));
                    deviceRetunrDetailList.add(dim);
                }

                di.setDeviceReturnList(deviceRetunrDetailList);

                deviceRetunInfo.add(di);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Retriview Device Return......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceRetunInfo.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceRetunInfo);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    //Device Return Detail Head lits Office
    public static MessageWrapper getDeviceReturnDetailHeadOfficeList(int retun_no, String modleDesc, String imeiNo, int salesPrice) {
        logger.info("getDeviceReturnDetail Method Call...........");
        ArrayList<DeviceReturnDetail> deviceRetunInfo = new ArrayList<>();

        ArrayList<DeviceReturnDetail> deviceRetunrDetailList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        
        modleDesc = modleDesc.toUpperCase();
        
        
        
        String modelDescription  = (modleDesc.equalsIgnoreCase("all")) ?  "" : " AND M.MODEL_DESCRIPTION LIKE '%"+modleDesc+"%'";
        String imeiNumber  = (imeiNo.equalsIgnoreCase("all")) ?  "" : " AND D.IMEI_NO LIKE '%"+imeiNo+"%'";
        String salesPriceVal  = (salesPrice == 0) ?  "" : " AND D.PRICE LIKE '%"+salesPrice+"%'";
        
        
        try {

            String selectColumn = "RETURN_NO,"
                    + "DSR_ID,"
                    + "DISTRIBUTER_ID,"
                    + "TO_CHAR(RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "STATUS,"
                    + "REASON";

            String whereClous = "RETURN_NO = " + retun_no + " "
                    +modelDescription
                    +imeiNumber
                    +salesPriceVal;

            String ImeColumn = "D.SEQ_NO,"
                    + "D.RETURN_NO,"
                    + "D.IMEI_NO,"
                    + "M.MODEL_NO,"
                    + "D.STATUS,D.PRICE,M.MODEL_DESCRIPTION, NVL(M.SELLING_PRICE,0) AS SELLING_PRICE  ";

            System.out.println("HO RETURN DTL QRY...... " + "SELECT " + ImeColumn + " "
                    + "FROM SD_DEVICE_RETURN_DTL D,SD_MODEL_LISTS M, SD_DEVICE_MASTER A "
                    + " WHERE M.MODEL_NO = A.MODEL_NO  "
                    + " AND A.IMEI_NO = D.IMEI_NO "
                    + " AND    D.IMEI_NO=A.IMEI_NO  "
                    + "AND " + whereClous + "");

            ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " "
                    + "FROM SD_DEVICE_RETURN_DTL D,SD_MODEL_LISTS M, SD_DEVICE_MASTER A "
                    + " WHERE M.MODEL_NO = A.MODEL_NO  "
                    + " AND A.IMEI_NO = D.IMEI_NO "
                    + " AND    D.IMEI_NO=A.IMEI_NO  "
                    + "AND " + whereClous + "");

            while (rs_ime.next()) {

                DeviceReturnDetail dim = new DeviceReturnDetail();
                dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                dim.setReturnNo(rs_ime.getInt("RETURN_NO"));
                dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                dim.setStatus(rs_ime.getInt("STATUS"));
                dim.setSalesPrice(rs_ime.getDouble("PRICE"));
                //  dim.setSalesPrice(rs_ime.getDouble("SELLING_PRICE"));
                dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));
                deviceRetunrDetailList.add(dim);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Retriview Device Return......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceRetunInfo.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceRetunrDetailList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }
    //End Device Return Detail Head Office

    public static ValidationWrapper editDeviceReturn(DeviceReturn deviceReturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editDeviceReturn Method Call............");
        int result = 1;
        int ime_un = 0;
        int distributor_id = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
            drList = deviceReturn_info.getDeviceReturnList();

            ArrayList<String> requesList = new ArrayList<>();
            for (DeviceReturnDetail dr : drList) {
                requesList.add(dr.getImeiNo());
            }

            ArrayList<String> returnList = new ArrayList<>();
            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
                    + "FROM SD_DEVICE_RETURN_DTL "
                    + "WHERE RETURN_NO != " + deviceReturn_info.getReturnNo() + "");

            while (rs_issue_cnt.next()) {
                String imei = rs_issue_cnt.getString("IMEI_NO");
                returnList.add(imei);
            }

            for (String s : requesList) {
                if (returnList.contains(s)) {
                    return_imei += s + ",";
                    ime_un = 1;
                }
            }

            if (ime_un == 1) {
                result = 2;
            } else {

                PreparedStatement ps_up_dr = dbCon.prepare(con, "UPDATE SD_DEVICE_RETURN "
                        + "SET DISTRIBUTER_ID=?,"
                        + "DSR_ID = ?,"
                        + "RETURN_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                        + "STATUS=?,"
                        + "PRICE=?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE RETURN_NO=?");

                ps_up_dr.setInt(1, deviceReturn_info.getDistributorId());
                distributor_id = deviceReturn_info.getDistributorId();
                ps_up_dr.setInt(2, deviceReturn_info.getDsrId());
                ps_up_dr.setString(3, deviceReturn_info.getReturnDate());
                ps_up_dr.setInt(4, deviceReturn_info.getStatus());
                ps_up_dr.setDouble(5, deviceReturn_info.getPrice());
                ps_up_dr.setString(6, user_id);
                ps_up_dr.setInt(7, deviceReturn_info.getReturnNo());
                ps_up_dr.executeUpdate();

                result = 1;

                ArrayList<DeviceReturnDetail> deviceReturnList = deviceReturn_info.getDeviceReturnList();
                deviceReturnList = deviceReturn_info.getDeviceReturnList();

                PreparedStatement ps_dle_dtl = dbCon.prepare(con, "DELETE FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = ?");
                ps_dle_dtl.setInt(1, deviceReturn_info.getReturnNo());
                ps_dle_dtl.executeUpdate();

                if (deviceReturnList.size() > 0) {

                    String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                    String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";

                    int max_dtl = 0;

                    ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                            + "FROM SD_DEVICE_RETURN_DTL "
                            + "WHERE RETURN_NO = " + deviceReturn_info.getReturnNo() + "");

                    while (rs_max_dtl.next()) {
                        max_dtl = rs_max_dtl.getInt("MAX");
                    }

                    PreparedStatement ps_in_retun_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN_DTL(" + deviceReturnColumn + ") "
                            + "VALUES(" + deviceReturnValue + ")");

                    for (DeviceReturnDetail dr : deviceReturnList) {
                        max_dtl = max_dtl + 1;
                        ps_in_retun_dtl.setInt(1, max_dtl);
                        ps_in_retun_dtl.setInt(2, deviceReturn_info.getReturnNo());
                        ps_in_retun_dtl.setString(3, dr.getImeiNo());
                        ps_in_retun_dtl.setInt(4, dr.getModleNo());
                        if (deviceReturn_info.getStatus() == 2) {
                            ps_in_retun_dtl.setInt(5, 3);
                        } else {
                            ps_in_retun_dtl.setInt(5, 1);
                        }
                        ps_in_retun_dtl.setString(6, user_id);
                        ps_in_retun_dtl.setDouble(7, dr.getSalesPrice());
                        ps_in_retun_dtl.addBatch();
                    }
                    ps_in_retun_dtl.executeBatch();
                    result = 1;
                    logger.info("Device Return Details IME  Data Inserted........");
                }

                if (deviceReturn_info.getStatus() == 2) {
                    PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
                            + "BIS_ID = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE IMEI_NO = ?");
                    for (DeviceReturnDetail dr : deviceReturnList) {
                        ps_up_master.setInt(1, deviceReturn_info.getDistributorId());
                        ps_up_master.setString(2, user_id);
                        ps_up_master.setString(3, dr.getImeiNo());
                        ps_up_master.addBatch();
                    }
                    ps_up_master.executeBatch();
                }

            }

        } catch (Exception ex) {
            logger.info("Error editDeviceReturn Method.. :" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Return Edit Successfully ");
            logger.info("Device Return Edit Successfully ...........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Device Return Edit");
            logger.info("Getting Error in Device Return Edit ...............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    //Edit device return headoffice
    public static ValidationWrapper editDeviceReturnHeadOffice(DeviceReturn deviceReturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editDeviceReturnHeadOffice Method Call............");
        int result = 1;
        int ime_un = 0;
        int distributor_id = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
            drList = deviceReturn_info.getDeviceReturnList();

            ArrayList<String> requesList = new ArrayList<>();
            for (DeviceReturnDetail dr : drList) {
                requesList.add(dr.getImeiNo());
            }

            ArrayList<String> returnList = new ArrayList<>();
            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
                    + "FROM SD_DEVICE_RETURN_DTL "
                    + "WHERE RETURN_NO != " + deviceReturn_info.getReturnNo() + "");

            while (rs_issue_cnt.next()) {
                String imei = rs_issue_cnt.getString("IMEI_NO");
                returnList.add(imei);
            }

            for (String s : requesList) {
                if (returnList.contains(s)) {
                    return_imei += s + ",";
                    ime_un = 1;
                }
            }

            if (ime_un == 1) {
                result = 2;
            } else {

                PreparedStatement ps_up_dr = dbCon.prepare(con, "UPDATE SD_DEVICE_RETURN "
                        //+ "SET DISTRIBUTER_ID=?,"
                        + "SET DSR_ID = ?,"
                        + "RETURN_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                        + "STATUS=?,"
                        + "PRICE=?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE RETURN_NO=?");

                //ps_up_dr.setInt(1, deviceReturn_info.getDistributorId());
                distributor_id = deviceReturn_info.getDistributorId();
                ps_up_dr.setInt(1, deviceReturn_info.getDsrId());
                ps_up_dr.setString(2, deviceReturn_info.getReturnDate());
                ps_up_dr.setInt(3, deviceReturn_info.getStatus());
                ps_up_dr.setDouble(4, deviceReturn_info.getPrice());
                ps_up_dr.setString(5, user_id);
                ps_up_dr.setInt(6, deviceReturn_info.getReturnNo());
                ps_up_dr.executeUpdate();

                result = 1;

                ArrayList<DeviceReturnDetail> deviceReturnList = deviceReturn_info.getDeviceReturnList();
                deviceReturnList = deviceReturn_info.getDeviceReturnList();

                PreparedStatement ps_dle_dtl = dbCon.prepare(con, "DELETE FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = ?");
                ps_dle_dtl.setInt(1, deviceReturn_info.getReturnNo());
                ps_dle_dtl.executeUpdate();

                if (deviceReturnList.size() > 0) {

                    String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                    String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";

                    int max_dtl = 0;

                    ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                            + "FROM SD_DEVICE_RETURN_DTL "
                            + "WHERE RETURN_NO = " + deviceReturn_info.getReturnNo() + "");

                    while (rs_max_dtl.next()) {
                        max_dtl = rs_max_dtl.getInt("MAX");
                    }

                    PreparedStatement ps_in_retun_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN_DTL(" + deviceReturnColumn + ") "
                            + "VALUES(" + deviceReturnValue + ")");

                    for (DeviceReturnDetail dr : deviceReturnList) {
                        max_dtl = max_dtl + 1;
                        ps_in_retun_dtl.setInt(1, max_dtl);
                        ps_in_retun_dtl.setInt(2, deviceReturn_info.getReturnNo());
                        ps_in_retun_dtl.setString(3, dr.getImeiNo());
                        ps_in_retun_dtl.setInt(4, dr.getModleNo());
                        if (deviceReturn_info.getStatus() == 2) {
                            ps_in_retun_dtl.setInt(5, 3);
                        } else {
                            ps_in_retun_dtl.setInt(5, 1);
                        }
                        ps_in_retun_dtl.setString(6, user_id);
                        ps_in_retun_dtl.setDouble(7, dr.getSalesPrice());
                        ps_in_retun_dtl.addBatch();
                    }
                    ps_in_retun_dtl.executeBatch();
                    result = 1;
                    logger.info("Device Return Details IME  Data Inserted........");
                }

                if (deviceReturn_info.getStatus() == 2) {
                    PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
                            + "BIS_ID = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE IMEI_NO = ?");
                    for (DeviceReturnDetail dr : deviceReturnList) {
                        ps_up_master.setInt(1, deviceReturn_info.getDistributorId());
                        ps_up_master.setString(2, user_id);
                        ps_up_master.setString(3, dr.getImeiNo());
                        ps_up_master.addBatch();
                    }
                    ps_up_master.executeBatch();
                }

            }

        } catch (Exception ex) {
            logger.info("Error editDeviceReturn Method.. :" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Return Edit Successfully ");
            logger.info("Device Return Edit Successfully ...........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Device Return Edit");
            logger.info("Getting Error in Device Return Edit ...............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    //End Edit device return headoffice
    public static ValidationWrapper editDeviceReturnDtl(DeviceReturn deviceReturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editDeviceReturn Method Call............");
        int result = 1;
        int ime_un = 0;
        int distributor_id = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
            drList = deviceReturn_info.getDeviceReturnList();

            ArrayList<String> requesList = new ArrayList<>();
            for (DeviceReturnDetail dr : drList) {
                requesList.add(dr.getImeiNo());
            }

//            ArrayList<String> returnList = new ArrayList<>();
//            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
//                    + "FROM SD_DEVICE_RETURN_DTL "
//                    + "WHERE RETURN_NO != " + deviceReturn_info.getReturnNo() + "");
//
//            while (rs_issue_cnt.next()) {
//                String imei = rs_issue_cnt.getString("IMEI_NO");
//                returnList.add(imei);
//            }
//
//            for (String s : requesList) {
//                if (returnList.contains(s)) {
//                    return_imei += s + ",";
//                    ime_un = 1;
//                }
//            }
            if (ime_un == 1) {
                result = 2;
            } else {

                PreparedStatement ps_up_dr = dbCon.prepare(con, "UPDATE SD_DEVICE_RETURN "
                        + "SET DISTRIBUTER_ID=?,"
                        + "DSR_ID = ?,"
                        + "RETURN_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                        + "STATUS=?,"
                        + "PRICE=?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE RETURN_NO=?");

                ps_up_dr.setInt(1, deviceReturn_info.getDistributorId());
                distributor_id = deviceReturn_info.getDistributorId();
                ps_up_dr.setInt(2, deviceReturn_info.getDsrId());
                ps_up_dr.setString(3, deviceReturn_info.getReturnDate());
                ps_up_dr.setInt(4, deviceReturn_info.getStatus());
                ps_up_dr.setDouble(5, deviceReturn_info.getPrice());
                ps_up_dr.setString(6, user_id);
                ps_up_dr.setInt(7, deviceReturn_info.getReturnNo());
                ps_up_dr.executeUpdate();

                result = 1;

                ArrayList<DeviceReturnDetail> deviceReturnList = deviceReturn_info.getDeviceReturnList();
                deviceReturnList = deviceReturn_info.getDeviceReturnList();

                PreparedStatement ps_dle_dtl = dbCon.prepare(con, "DELETE FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = ?");
                ps_dle_dtl.setInt(1, deviceReturn_info.getReturnNo());
                ps_dle_dtl.executeUpdate();

                if (deviceReturnList.size() > 0) {

                    String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                    String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";

                    int max_dtl = 0;

                    ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                            + "FROM SD_DEVICE_RETURN_DTL "
                            + "WHERE RETURN_NO = " + deviceReturn_info.getReturnNo() + "");

                    while (rs_max_dtl.next()) {
                        max_dtl = rs_max_dtl.getInt("MAX");
                    }

                    PreparedStatement ps_in_retun_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN_DTL(" + deviceReturnColumn + ") "
                            + "VALUES(" + deviceReturnValue + ")");

                    for (DeviceReturnDetail dr : deviceReturnList) {
                        max_dtl = max_dtl + 1;
                        ps_in_retun_dtl.setInt(1, max_dtl);
                        ps_in_retun_dtl.setInt(2, deviceReturn_info.getReturnNo());
                        ps_in_retun_dtl.setString(3, dr.getImeiNo());
                        ps_in_retun_dtl.setInt(4, dr.getModleNo());
                        if (deviceReturn_info.getStatus() == 2) {
                            ps_in_retun_dtl.setInt(5, 3);
                        } else {
                            ps_in_retun_dtl.setInt(5, 1);
                        }
                        ps_in_retun_dtl.setString(6, user_id);
                        ps_in_retun_dtl.setDouble(7, dr.getSalesPrice());
                        ps_in_retun_dtl.addBatch();

                    }
                    ps_in_retun_dtl.executeBatch();

                    System.out.println("Execute SD_DEVICE_RETURN_DTL ..............");

                    result = 1;
                    logger.info("Device Return Details IME  Data Inserted........");
                }

                if (deviceReturn_info.getStatus() == 2) {
                    PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
                            + "BIS_ID = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE IMEI_NO = ?");
                    for (DeviceReturnDetail dr : deviceReturnList) {
                        ps_up_master.setInt(1, deviceReturn_info.getDistributorId());
                        ps_up_master.setString(2, user_id);
                        ps_up_master.setString(3, dr.getImeiNo());
                        ps_up_master.addBatch();

                    }
                    ps_up_master.executeBatch();

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error editDeviceReturn Method.. :" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Return Edit Successfully ");
            logger.info("Device Return Edit Successfully ...........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Device Return Edit");
            logger.info("Getting Error in Device Return Edit ...............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editDeviceReturnDtlHo(DeviceReturn deviceReturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editDeviceReturn Method Call............");
        int result = 1;
        int ime_un = 0;
        int distributor_id = 0;
        String return_imei = "";
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
            drList = deviceReturn_info.getDeviceReturnList();

            ArrayList<String> requesList = new ArrayList<>();
            for (DeviceReturnDetail dr : drList) {
                requesList.add(dr.getImeiNo());
            }

            if (ime_un == 1) {
                result = 2;
            } else {

                PreparedStatement ps_up_dr = dbCon.prepare(con, "UPDATE SD_DEVICE_RETURN "
                        + "SET DISTRIBUTER_ID=?,"
                        + "DSR_ID = ?,"
                        + "RETURN_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                        + "STATUS=?,"
                        + "PRICE=?,"
                        + "USER_MODIFIED=?,"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE RETURN_NO=?");

                ps_up_dr.setInt(1, deviceReturn_info.getDistributorId());
                distributor_id = deviceReturn_info.getDistributorId();
                ps_up_dr.setInt(2, deviceReturn_info.getDsrId());
                ps_up_dr.setString(3, deviceReturn_info.getReturnDate());
                ps_up_dr.setInt(4, deviceReturn_info.getStatus());
                ps_up_dr.setDouble(5, deviceReturn_info.getPrice());
                ps_up_dr.setString(6, user_id);
                ps_up_dr.setInt(7, deviceReturn_info.getReturnNo());
                ps_up_dr.executeUpdate();

                result = 1;

//                ArrayList<DeviceReturnDetail> deviceReturnList = deviceReturn_info.getDeviceReturnList();
                ArrayList<DeviceReturnDetail> deviceReturnList = deviceReturn_info.getDeviceReturnList();
//                deviceReturnList = deviceReturn_info.getDeviceReturnList();

                if (deviceReturnList.size() > 0) {

                    System.out.println("Device return list count...");

                    String getAllReturnImei = "SELECT IMEI_NO "
                            + "FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = '" + deviceReturn_info.getReturnNo() + "' ";

                    ResultSet rs = dbCon.search(con, getAllReturnImei);

                    ArrayList<String> arrImei = new ArrayList<>();
                    ArrayList<String> acceptedImei = new ArrayList<>();

                    while (rs.next()) {
                        arrImei.add(rs.getString("IMEI_NO"));
                    }

                    ArrayList<DeviceReturnDetail> drt = deviceReturn_info.getDeviceReturnList();

                    for (DeviceReturnDetail devDetail : drt) {
                        acceptedImei.add(devDetail.getImeiNo());
                    }

                    String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                    String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";

                    int max_dtl = 0;

                    ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                            + "FROM SD_DEVICE_RETURN_DTL "
                            + "WHERE RETURN_NO = " + deviceReturn_info.getReturnNo() + "");

                    while (rs_max_dtl.next()) {
                        max_dtl = rs_max_dtl.getInt("MAX");
                    }
                    String qry = "UPDATE SD_DEVICE_RETURN_DTL SET STATUS= ?, DATE_MODIFIED = SYSDATE, USER_MODIFIED = ?"
                            + " WHERE RETURN_NO = ? AND IMEI_NO = ? ";

                    PreparedStatement ps_in_retun_dtl = dbCon.prepare(con, qry);

                    for (DeviceReturnDetail dr : deviceReturnList) {

                        for (String imeil : arrImei) {

                            System.out.println("IMEI NO >> " + imeil);

                            if (acceptedImei.contains(imeil)) {
                                System.out.println("IMEI " + imeil + " true ");
                                ps_in_retun_dtl.setInt(1, 2);
                            } else {
                                System.out.println("IMEI " + imeil + " false ");
                                ps_in_retun_dtl.setInt(1, 3);
                            }

                            ps_in_retun_dtl.setString(2, user_id);
                            ps_in_retun_dtl.setInt(3, deviceReturn_info.getReturnNo());
                            ps_in_retun_dtl.setString(4, imeil);

                            ps_in_retun_dtl.executeUpdate();
                        }

                    }

                    System.out.println("Execute SD_DEVICE_RETURN_DTL ..............");

                    result = 1;
                    logger.info("Device Return Details IME  Data Inserted........");
                }

                if (deviceReturn_info.getStatus() == 2) {
                    PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
                            + "BIS_ID = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE IMEI_NO = ?");
                    for (DeviceReturnDetail dr : deviceReturnList) {

                        ps_up_master.setInt(1, deviceReturn_info.getDistributorId());
                        ps_up_master.setString(2, user_id);
                        ps_up_master.setString(3, dr.getImeiNo());
                        ps_up_master.addBatch();

                    }
                    ps_up_master.executeBatch();

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error editDeviceReturn Method.. :" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Device Return Edit Successfully ");
            logger.info("Device Return Edit Successfully ...........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Device Return Edit");
            logger.info("Getting Error in Device Return Edit ...............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    //editDeviceReturnDtlHo 
//    public static ValidationWrapper editDeviceReturnDtlHo(DeviceReturn deviceReturn_info,
//            String user_id,
//            String room,
//            String department,
//            String branch,
//            String countryCode,
//            String division,
//            String organaization) {
//        logger.info("editDeviceReturn Method Call............");
//        int result = 1;
//        int ime_un = 0;
//        int distributor_id = 0;
//        String return_imei = "";
//        DbCon dbCon = new DbCon();
//        Connection con = dbCon.getCon();
//        try {
//            ArrayList<DeviceReturnDetail> drList = new ArrayList<DeviceReturnDetail>();
//            drList = deviceReturn_info.getDeviceReturnList();
//            
//            //drList.contains(con)
//            ArrayList<String> returnList = new ArrayList<>();
//            ResultSet rs_ho_accept  = dbCon.search(con, "SELECT IMEI_NO "
//                    + "FROM SD_DEVICE_RETURN "
//                    + "WHERE RETURN_NO = " + deviceReturn_info.getReturnNo() + "");
//
//            while (rs_ho_accept.next()) {
//                String imei = rs_ho_accept.getString("IMEI_NO");
//                returnList.add(imei);
//            }
//
//         
//            //End accept imei list
//            ArrayList<String> requesList = new ArrayList<>();
//            for (DeviceReturnDetail dr : drList) {
//                requesList.add(dr.getImeiNo());
//            }
//            
//            for (String i : returnList) {
//             //   if (returnList.contains(i)) {
//                //if (returnList.contains(i) == drList.contains(i)) {
////                    
////                    //TEST
////                    
////                    //END test
//                    
//                //}
//            }  
////            ArrayList<String> returnList = new ArrayList<>();
////            ResultSet rs_issue_cnt = dbCon.search(con, "SELECT IMEI_NO "
////                    + "FROM SD_DEVICE_RETURN_DTL "
////                    + "WHERE RETURN_NO != " + deviceReturn_info.getReturnNo() + "");
////
////            while (rs_issue_cnt.next()) {
////                String imei = rs_issue_cnt.getString("IMEI_NO");
////                returnList.add(imei);
////            }
////
////            for (String s : requesList) {
////                if (returnList.contains(s)) {
////                    return_imei += s + ",";
////                    ime_un = 1;
////                }
////            }
//
//          
//            
//            if (ime_un == 1) {
//                result = 2;
//            } else {
//
//                PreparedStatement ps_up_dr = dbCon.prepare(con, "UPDATE SD_DEVICE_RETURN "
//                        + "SET DISTRIBUTER_ID=?,"
//                        + "DSR_ID = ?,"
//                        + "RETURN_DATE=TO_DATE(?,'YYYY/MM/DD'),"
//                        + "STATUS=?,"
//                        + "PRICE=?,"
//                        + "USER_MODIFIED=?,"
//                        + "DATE_MODIFIED=SYSDATE "
//                        + "WHERE RETURN_NO=?");
//
//                ps_up_dr.setInt(1, deviceReturn_info.getDistributorId());
//                distributor_id = deviceReturn_info.getDistributorId();
//                ps_up_dr.setInt(2, deviceReturn_info.getDsrId());
//                ps_up_dr.setString(3, deviceReturn_info.getReturnDate());
//                ps_up_dr.setInt(4, deviceReturn_info.getStatus());
//                ps_up_dr.setDouble(5, deviceReturn_info.getPrice());
//                ps_up_dr.setString(6, user_id);
//                ps_up_dr.setInt(7, deviceReturn_info.getReturnNo());
//                ps_up_dr.executeUpdate();
//                
//                
//                result = 1;
//
//                ArrayList<DeviceReturnDetail> deviceReturnList = deviceReturn_info.getDeviceReturnList();
//                deviceReturnList = deviceReturn_info.getDeviceReturnList();
//
//                PreparedStatement ps_dle_dtl = dbCon.prepare(con, "DELETE FROM SD_DEVICE_RETURN_DTL WHERE RETURN_NO = ?");
//                ps_dle_dtl.setInt(1, deviceReturn_info.getReturnNo());
//                ps_dle_dtl.executeUpdate();
//
//                
//                if (deviceReturnList.size() > 0) {
//
//                    String deviceReturnColumn = "SEQ_NO,RETURN_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";
//
//                    String deviceReturnValue = "?,?,?,?,?,?,SYSDATE,?";
//
//                    int max_dtl = 0;
//
//                    ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
//                            + "FROM SD_DEVICE_RETURN_DTL "
//                            + "WHERE RETURN_NO = " + deviceReturn_info.getReturnNo() + "");
//
//                    while (rs_max_dtl.next()) {
//                        max_dtl = rs_max_dtl.getInt("MAX");
//                    }
//
//                    PreparedStatement ps_in_retun_dtl = dbCon.prepare(con, "INSERT INTO SD_DEVICE_RETURN_DTL(" + deviceReturnColumn + ") "
//                            + "VALUES(" + deviceReturnValue + ")");
//
//                    for (DeviceReturnDetail dr : deviceReturnList) {
//                        max_dtl = max_dtl + 1;
//                        ps_in_retun_dtl.setInt(1, max_dtl);
//                        ps_in_retun_dtl.setInt(2, deviceReturn_info.getReturnNo());
//                        ps_in_retun_dtl.setString(3, dr.getImeiNo());
//                        ps_in_retun_dtl.setInt(4, dr.getModleNo());
//                        if(deviceReturn_info.getStatus() == 2){
//                            ps_in_retun_dtl.setInt(5, 3);
//                        }else{
//                            ps_in_retun_dtl.setInt(5, 1);
//                        }
//                        ps_in_retun_dtl.setString(6, user_id);
//                        ps_in_retun_dtl.setDouble(7, dr.getSalesPrice());
//                        ps_in_retun_dtl.addBatch();
//                        
//                        
//                    }
//                    ps_in_retun_dtl.executeBatch();
//                    
//                    System.out.println("Execute SD_DEVICE_RETURN_DTL ..............");
//                    
//                    result = 1;
//                    logger.info("Device Return Details IME  Data Inserted........");
//                }
//
//                
//                if (deviceReturn_info.getStatus() == 2) {
//                    PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET "
//                            + "BIS_ID = ?,"
//                            + "USER_MODIFIED = ?,"
//                            + "DATE_MODIFIED = SYSDATE "
//                            + "WHERE IMEI_NO = ?");
//                    for (DeviceReturnDetail dr : deviceReturnList) {
//                        ps_up_master.setInt(1, deviceReturn_info.getDistributorId());
//                        ps_up_master.setString(2, user_id);
//                        ps_up_master.setString(3, dr.getImeiNo());
//                        ps_up_master.addBatch();
//                        
//                        
//                    }
//                    ps_up_master.executeBatch();
//                    
//                    
//                }
//
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.info("Error editDeviceReturn Method.. :" + ex.toString());
//            result = 9;
//        }
//
//        if (!return_imei.equals("")) {
//            return_imei = return_imei.substring(0, (return_imei.length() - 1));
//        }
//
//        ValidationWrapper vw = new ValidationWrapper();
//        if (result == 1) {
//            vw.setReturnFlag("1");
//            vw.setReturnMsg("Device Return Edit Successfully ");
//            logger.info("Device Return Edit Successfully ...........");
//        } else if (result == 2) {
//            vw.setReturnFlag("2");
//            vw.setReturnMsg("This " + return_imei + " IMIE Number in Device Return List");
//            logger.info("This " + return_imei + " IMIE Number in Device Return List........");
//        } else {
//            vw.setReturnFlag("9");
//            vw.setReturnMsg("Getting Error in Device Return Edit");
//            logger.info("Getting Error in Device Return Edit ...............");
//        }
//
//        dbCon.ConectionClose(con);
//        logger.info("Successfully Return Value.............");
//        logger.info("-----------------------------------------------------");
//        return vw;
//    }
//    
    //End editDeviceReturnDtlHo
}
