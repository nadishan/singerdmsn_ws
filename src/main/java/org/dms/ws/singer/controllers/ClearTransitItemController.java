/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ClearTransitItem;
import org.dms.ws.singer.entities.DeviceIssue;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.entities.ModelList;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class ClearTransitItemController {

    public static MessageWrapper getClearTransitList(int issue_no,
            String form_user,
            int disributor,
            String to_user,
            int dsr,
            String issue_date,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getClearTransitList Method Call...........");
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<DeviceIssue> deviceIssueList = new ArrayList<>();
        int totalrecode = 0;
        try {
            order = DBMapper.clearTransit(order);

            String issueNo = ((issue_no == 0) ? "" : "AND D.ISSUE_NO = '" + issue_no + "' ");
            String fromUser = (form_user.equalsIgnoreCase("all") ? "NVL(B1.BIS_NAME,'AA') = NVL(B1.BIS_NAME,'AA') " : "UPPER(B1.BIS_NAME) LIKE UPPER('%" + form_user + "%')");
            String distributorId = ((disributor == 0) ? "" : "AND D.DISTRIBUTER_ID = '" + disributor + "' ");
            String toUser = (to_user.equalsIgnoreCase("all") ? "NVL(B2.BIS_NAME,'AA') = NVL(B2.BIS_NAME,'AA') " : "UPPER(B2.BIS_NAME) LIKE UPPER('%" + to_user + "%')");
            String dsrId= ((dsr == 0) ? "" : "AND D.DSR_ID = '" + dsr + "' ");
            String issueDate = (issue_date.equalsIgnoreCase("all") ? "NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(D.ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(D.ISSUE_DATE,'YYYY/MM/DD') = TO_DATE('%" + issue_date + "%','YYYY/MM/DD')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.ISSUE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "D.ISSUE_NO,B1.BIS_NAME FROM_USER,"
                    + "D.DISTRIBUTER_ID,B2.BIS_NAME TO_USER,"
                    + "D.DSR_ID,"
                    + "TO_CHAR(D.ISSUE_DATE,'YYYY/MM/DD') ISSUE_DATE,"
                    + "D.STATUS";

            String whereClous = "D.DISTRIBUTER_ID = B1.BIS_ID (+) "
                    + "AND D.DSR_ID = B2.BIS_ID (+) "
                    + "AND D.STATUS IN (2) "
                    + "" + issueNo + " "
                    + "AND " + fromUser + " "
                    + " "+distributorId+""
                    + "AND " + toUser + "  "
                    + ""+dsrId+""
                    + "AND " + issueDate + "";

            String tables = "SD_DEVICE_ISSUE D,SD_BUSINESS_STRUCTURES B1,SD_BUSINESS_STRUCTURES B2";

 
            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                DeviceIssue di = new DeviceIssue();
                totalrecode = rs.getInt("CNT");
                di.setIssueNo(rs.getInt("ISSUE_NO"));
                di.setFromName(rs.getString("FROM_USER"));
                di.setDistributerID(rs.getInt("DISTRIBUTER_ID"));
                di.setToName(rs.getString("TO_USER"));
                di.setdSRId(rs.getInt("DSR_ID"));
                di.setIssueDate(rs.getString("ISSUE_DATE"));
                di.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    di.setStatusMsg("Pending");
                } else if (rs.getInt("STATUS") == 2) {
                    di.setStatusMsg("Complete");
                } else if (rs.getInt("STATUS") == 3) {
                    di.setStatusMsg("Accepted");
                } else {
                    di.setStatusMsg("None");
                }

                deviceIssueList.add(di);
            }

        } catch (Exception ex) {
            logger.info("Error in getClearTransitList method......:" + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceIssueList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getClearTransitDetails(int issue_no) {
        logger.info("getClearTransitDetails Mehtod Call.......");
        int totalrecode = 0;
        ArrayList<DeviceIssueIme> clearTransitList = new ArrayList<DeviceIssueIme>();
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "SEQ_NO,ISSUE_NO,IMEI_NO";

            String whereClous = "STATUS = 2 AND ISSUE_NO = " + issue_no + "";

            ResultSet rs = dbCon.search(con,"SELECT " + selectColumn + " "
                    + "FROM SD_DEVICE_ISSUE_DTL "
                    + "WHERE " + whereClous + "");

            logger.info("Get Data to ResultSet............");

            while (rs.next()) {
                DeviceIssueIme cti = new DeviceIssueIme();
                totalrecode = 1;
                cti.setSeqNo(rs.getInt("SEQ_NO"));
                cti.setIssueNo(rs.getInt("ISSUE_NO"));
                cti.setImeiNo(rs.getString("IMEI_NO"));
                clearTransitList.add(cti);
            }

        } catch (Exception ex) {
            logger.info("Error in getClearTransitDetails mehtod Error... :" + ex.getMessage());
        }
        logger.info("Ready Return Values.............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(clearTransitList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addClearTransit(DeviceIssue transit_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addClearTransit Method Call.....");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_deviceissue = dbCon.prepare(con,"UPDATE SD_DEVICE_ISSUE "
                    + "SET STATUS = ?,"
                    + "USER_MODIFIED = ?,"
                    + "DATE_MODIFIED =SYSDATE "
                    + "WHERE ISSUE_NO = ?");

            ps_up_deviceissue.setInt(1, transit_info.getStatus());
            ps_up_deviceissue.setString(2, user_id);
            ps_up_deviceissue.setInt(3, transit_info.getIssueNo());
            ps_up_deviceissue.executeUpdate();

            ArrayList<DeviceIssueIme> deviceList = new ArrayList<>();
            deviceList = transit_info.getDeviceImei();

            if (deviceList.size() > 0) {

                try {

                    String tansColumn = "SEQ_NO,ISSUE_NO,IMEI_NO,REMARKS,USER_INSERTED,DATE_INSERTED";

                    String tansValues = "?,?,?,?,?,SYSDATE";
                    int trans_seq = 0;
                    ResultSet rs_maxtrans = dbCon.search(con,"SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_CLEAR_TANS_DETAIL");

                    while (rs_maxtrans.next()) {
                        trans_seq = rs_maxtrans.getInt("MAX");
                    }

                    PreparedStatement ps_in_dtl = dbCon.prepare(con,"INSERT INTO SD_CLEAR_TANS_DETAIL(" + tansColumn + ") VALUES(" + tansValues + ")");

                    PreparedStatement ps_up_dtl = dbCon.prepare(con,"UPDATE SD_DEVICE_ISSUE_DTL "
                            + "SET STATUS = ?,"
                            + "USER_MODIFIED = ?,"
                            + "DATE_MODIFIED = SYSDATE "
                            + "WHERE ISSUE_NO = ? "
                            + "AND IMEI_NO = ?");

                    for (DeviceIssueIme di : deviceList) {

                        trans_seq = trans_seq + 1;
                        ps_in_dtl.setInt(1, trans_seq);
                        ps_in_dtl.setInt(2, transit_info.getIssueNo());
                        ps_in_dtl.setString(3, di.getImeiNo());
                        ps_in_dtl.setString(4, transit_info.getRemarks());
                        ps_in_dtl.setString(5, user_id);
                        ps_in_dtl.addBatch();

                        ps_up_dtl.setInt(1, 5);
                        ps_up_dtl.setString(2, user_id);
                        ps_up_dtl.setInt(3, transit_info.getIssueNo());
                        ps_up_dtl.setString(4, di.getImeiNo());
                        ps_up_dtl.addBatch();
                    }

                    ps_in_dtl.executeBatch();
                    ps_up_dtl.executeBatch();
                    result = 1;

                } catch (Exception e) {
                    logger.info("Error in Transit Update Query..." + e.getMessage());
                    result = 9;
                }

            }

        } catch (Exception ex) {
            logger.info("Error in editTransit Method  :" + ex.getMessage());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Transit Insert Successfully");
            logger.info("Transit Insert Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Transit Insert");
            logger.info("Error in Transit Insert.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

}
