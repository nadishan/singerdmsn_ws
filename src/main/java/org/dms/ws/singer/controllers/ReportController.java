/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.services.ReportService;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ReadExcel;
import org.dms.ws.singer.utils.AppParams;

/**
 *
 * @author Dasun Chathuranga
 */
public class ReportController {

    public ResponseWrapper getReportDetailsFromDB(ResponseWrapper response) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<ReadExcel> reportDetails = new ArrayList<ReadExcel>();
        String qry = "SELECT * FROM EXCELREPORTS ";

        try {
            ResultSet rs = dbCon.search(con, qry);

            while (rs.next()) {
                ReadExcel rpt = new ReadExcel();
                rpt.setReportID(rs.getString("ID"));
                rpt.setReportName(rs.getString("REPORTNAME"));
                rpt.setReportQuery(rs.getString("REPORT_QUERY"));
                rpt.setStatus(rs.getInt("STATUS"));
                rpt.setCond1(rs.getString("CONDITION_1"));
                rpt.setCond2(rs.getString("CONDITION_2"));
                rpt.setCond3(rs.getString("CONDITION_3"));
                rpt.setCond4(rs.getString("CONDITION_4"));
                rpt.setCond5(rs.getString("CONDITION_5"));
                rpt.setCond6(rs.getString("CONDITION_6"));

                rpt.setCond1a(rs.getString("CONDITION_1A"));
                rpt.setCond2a(rs.getString("CONDITION_2A"));
                rpt.setCond3a(rs.getString("CONDITION_3A"));
                rpt.setCond4a(rs.getString("CONDITION_4A"));
                rpt.setCond5a(rs.getString("CONDITION_5A"));
                rpt.setCond6a(rs.getString("CONDITION_6A"));
                reportDetails.add(rpt);
            }
            response.setFlag("100");
            response.setData(reportDetails);

        } catch (Exception ex) {
            ArrayList<String> err = new ArrayList<>();
            err.add(ex.getMessage());
            response.setFlag("999");
            response.setExceptionMessages(err);
        }

        return response;
    }

    public static int insertReportDetails(ReadExcel excelReports) {
        int result = 1;

        // logger.info("Cond1 value=>>>>>>>>>>>>>>>>>>>>>"+excelReports.getCond1());
        try {
            DbCon dbconn = new DbCon();
            Connection con = dbconn.getCon();

            int repID = 0;
            ResultSet rset;
            try (Statement stmt = (Statement) con.createStatement()) {
                String countQry = "SELECT COUNT(*) FROM EXCELREPORTS";
                rset = stmt.executeQuery(countQry);

                if (rset.next()) {
                    repID = rset.getInt(1) + 1;
                }

            }
            rset.close();

            String selectColumns = "ID,"
                    + "REPORTNAME,"
                    + "REPORT_QUERY,"
                    + "STATUS,"
                    + "CONDITION_1,"
                    + "CONDITION_2,"
                    + "CONDITION_3,"
                    + "CONDITION_4,"
                    + "CONDITION_5,"
                    + "CONDITION_6,"
                    + "CONDITION_1A,"
                    + "CONDITION_2A,"
                    + "CONDITION_3A,"
                    + "CONDITION_4A,"
                    + "CONDITION_5A,"
                    + "CONDITION_6A";

            // String values = repID + ",?,?,?,?,?,?,?,?,?";
            String values = repID + ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";

            PreparedStatement ps = dbconn.prepare(con, "INSERT INTO EXCELREPORTS(" + selectColumns + ") "
                    + "VALUES(" + values + ")");

            ps.setString(1, excelReports.getReportName());
            ps.setString(2, excelReports.getReportQuery());
            ps.setInt(3, excelReports.getStatus());

            // System.out.println("0000000000000000000000");
            if (excelReports.getCond1() != null) {
                ps.setString(4, excelReports.getCond1());
                logger.info("Cond1 value=>>" + excelReports.getCond1());
                //   System.out.println("cond1="+ excelReports.getCond1());
            } else {
                ps.setString(4, "0");
                logger.info("Cond1 value=>> 0");
                //   System.out.println("Else =>"+excelReports.getCond1());
            }

            if (excelReports.getCond2() != null) {
                ps.setString(5, excelReports.getCond2());
            } else {
                ps.setString(5, "0");
            }

            if (excelReports.getCond3() != null) {
                ps.setString(6, excelReports.getCond3());
            } else {
                ps.setString(6, "0");
            }

            if (excelReports.getCond4() != null) {
                ps.setString(7, excelReports.getCond4());
            } else {
                ps.setString(7, "0");
            }

            if (excelReports.getCond5() != null) {
                ps.setString(8, excelReports.getCond5());
            } else {
                ps.setString(8, "0");
            }

            if (excelReports.getCond6() != null) {
                ps.setString(9, excelReports.getCond6());
            } else {
                ps.setString(9, "0");
            }

            // Alias innsertion 
            if (excelReports.getCond1a() != null) {
                ps.setString(10, excelReports.getCond1a());
            } else {
                ps.setString(10, "0");
            }

            if (excelReports.getCond2a() != null) {
                ps.setString(11, excelReports.getCond2a());
            } else {
                ps.setString(11, "0");
            }

            if (excelReports.getCond3a() != null) {
                ps.setString(12, excelReports.getCond3a());
            } else {
                ps.setString(12, "0");
            }

            if (excelReports.getCond4a() != null) {
                ps.setString(13, excelReports.getCond4a());
            } else {
                ps.setString(13, "0");
            }

            if (excelReports.getCond5a() != null) {
                ps.setString(14, excelReports.getCond5a());
            } else {
                ps.setString(14, "0");
            }

            if (excelReports.getCond6a() != null) {
                ps.setString(15, excelReports.getCond6a());
            } else {
                ps.setString(15, "0");
            }

            ps.executeUpdate();

            dbconn.ConectionClose(con);

        } catch (Exception ex) {
            ex.printStackTrace();
            result = 9;
        }
        return result;
    }

    public static int updateReportDetails(ReadExcel excelReports) {
        int result = 1;
        try {
            DbCon dbconn = new DbCon();
            Connection con = dbconn.getCon();

            System.out.println("inside update");

            String selectColumns = "REPORTNAME=?,"
                    + "REPORT_QUERY=?,"
                    + "STATUS=?,"
                    + "CONDITION_1=?,"
                    + "CONDITION_2=?,"
                    + "CONDITION_3=?,"
                    + "CONDITION_4=?,"
                    + "CONDITION_5=?,"
                    + "CONDITION_6=?,"
                    + "CONDITION_1a=?,"
                    + "CONDITION_2a=?,"
                    + "CONDITION_3a=?,"
                    + "CONDITION_4a=?,"
                    + "CONDITION_5a=?,"
                    + "CONDITION_6a=?";

            String psstr = "UPDATE EXCELREPORTS SET " + selectColumns + "  WHERE ID = ? ";
            PreparedStatement ps = dbconn.prepare(con, "UPDATE EXCELREPORTS SET " + selectColumns + "  WHERE ID = ? ");

            // System.out.println("QUERY>>>>>>"+ps);
            ps.setString(1, excelReports.getReportName());
            ps.setString(2, excelReports.getReportQuery());
            ps.setInt(3, excelReports.getStatus());

            if (excelReports.getCond1() != null) {
                ps.setString(4, excelReports.getCond1());
                //System.out.println("Test =>"+excelReports.getCond1());
            } else {
                ps.setString(4, "0");
                // System.out.println("Else => 0");
            }
            if (excelReports.getCond2() != null) {
                ps.setString(5, excelReports.getCond2());
            } else {
                ps.setString(5, "0");
            }

            if (excelReports.getCond3() != null) {
                ps.setString(6, excelReports.getCond3());
            } else {
                ps.setString(6, "0");
            }

            if (excelReports.getCond4() != null) {
                ps.setString(7, excelReports.getCond4());
            } else {
                ps.setString(7, "0");
            }

            if (excelReports.getCond5() != null) {
                ps.setString(8, excelReports.getCond5());
            } else {
                ps.setString(8, "0");
            }

            if (excelReports.getCond6() != null) {
                ps.setString(9, excelReports.getCond6());
            } else {
                ps.setString(9, "0");
            }

            if (excelReports.getCond1a() != null) {
                ps.setString(10, excelReports.getCond1a());
            } else {
                ps.setString(10, "0");
            }

            if (excelReports.getCond2a() != null) {
                ps.setString(11, excelReports.getCond2a());
            } else {
                ps.setString(11, "0");
            }

            if (excelReports.getCond3a() != null) {
                ps.setString(12, excelReports.getCond3a());
            } else {
                ps.setString(12, "0");
            }

            if (excelReports.getCond4a() != null) {
                ps.setString(13, excelReports.getCond4a());
            } else {
                ps.setString(13, "0");
            }

            if (excelReports.getCond5a() != null) {
                ps.setString(14, excelReports.getCond5a());
            } else {
                ps.setString(14, "0");
            }

            if (excelReports.getCond6a() != null) {
                ps.setString(15, excelReports.getCond6a());
            } else {
                ps.setString(15, "0");
            }

            ps.setString(16, excelReports.getReportID());

            //System.out.println("PS=>>>"+psstr);
            ps.executeUpdate();

            dbconn.ConectionClose(con);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = 9;
        }
        return result;
    }

    public byte[] getData(String query, String condition1, String condition2, String condition3, String condition4, String condition5, String condition6) {

        ArrayList<Object> objArr = new ArrayList<Object>();

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        String repPath = ReportService.excelPath();

        byte[] bytesEncoded = null;

        String filename = repPath + "/report.xls";
        new File(filename).delete();
        HSSFWorkbook workbook = new HSSFWorkbook();

        //  System.out.println(condition3.isEmpty()+ " - AAAAAAAAAAAA - "+condition3.trim().length());
        System.out.println("cons =>" + condition1 + "/" + condition2 + "/" + condition3 + "/" + condition4 + "/" + condition5 + "/" + condition6 + "/");

        try {

            /**
             * Conditions *
             */
            String emptyText = "=''";
            if (!query.toUpperCase().contains("WHERE")) {

                System.out.println("NOT WHERE CLAUSE");
                System.out.println("condition1>>> " + condition1 + " condition2>>>" + condition2 + "cond 3 >>" + condition3 + " condition3 >> " + condition3.equals(emptyText) + " condition6 " + condition6);

                if (!condition1.equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText) && !condition4.trim().equals(emptyText) && !condition5.trim().equals(emptyText) && !condition6.trim().equals(emptyText)) {
                    query = query.concat(" WHERE " + condition1 + " AND " + condition2 + " AND " + condition3 + " AND " + condition4 + " AND " + condition5 + " AND " + condition6);
                    System.out.println("all con not empty");
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText) && !condition4.trim().equals(emptyText) && !condition5.trim().equals(emptyText)) {
                    query = query.concat(" WHERE " + condition1 + " AND " + condition2 + " AND " + condition3 + " AND " + condition4 + " AND " + condition5);
                    System.out.println("1 empty");
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText) && !condition4.trim().equals(emptyText)) {
                    query = query.concat(" WHERE " + condition1 + " AND " + condition2 + " AND " + condition3 + " AND " + condition4);
                    System.out.println("2 empty");
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText)) {
                    query = query.concat(" WHERE " + condition1 + " AND " + condition2 + " AND " + condition3);
                    System.out.println("3 empty");
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText)) {
                    query = query.concat(" WHERE " + condition1 + " AND " + condition2);
                    System.out.println("4 empty");
                } else if (!condition1.trim().equals(emptyText)) {
                    query = query.concat(" WHERE " + condition1);
                    System.out.println("5 empty");
                } else {
                    System.out.println(">>>> No where condition");
                }

            } else {

                System.out.println("WITH WHERE CLAUSE");

                if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText) && !condition4.trim().equals(emptyText) && !condition5.trim().equals(emptyText) && !condition6.trim().equals(emptyText)) {
                    query = query.concat(" " + condition1 + " AND " + condition2 + " AND " + condition3 + " AND " + condition4 + " AND " + condition5 + " AND " + condition6);
                    // System.out.println("all con not empty");
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText) && !condition4.trim().equals(emptyText) && !condition5.trim().equals(emptyText)) {
                    query = query.concat(" " + condition1 + " AND " + condition2 + " AND " + condition3 + " AND " + condition4 + " AND " + condition5);
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText) && !condition4.trim().equals(emptyText)) {
                    query = query.concat(" " + condition1 + " AND " + condition2 + " AND " + condition3 + " AND " + condition4);
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText) && !condition3.trim().equals(emptyText)) {
                    query = query.concat(" " + condition1 + " AND " + condition2 + " AND " + condition3);
                } else if (!condition1.trim().equals(emptyText) && !condition2.trim().equals(emptyText)) {
                    query = query.concat(" " + condition1 + " AND " + condition2);
                    // System.out.println("con2");
                } else if (!condition1.trim().equals(emptyText)) {
                    query = query.concat(" " + condition1);
                    //  System.out.println("con1");
                }

            }

            System.out.println("Query getdata =>>>>>>>>>> " + query);

            String countquery = "SELECT COUNT(*) AS COUNT FROM (" + query + ")";

            ResultSet rsCount = dbCon.search(con, countquery);
            int resultCount = 0;

            while (rsCount.next()) {
                resultCount = rsCount.getInt("COUNT");
            }

            int rsC;
            int sheetRowRound;

            System.out.println("Sheet COUNT >>> " + resultCount / 65500);

            HSSFSheet sheet;

            if (resultCount > 65500) {

                sheetRowRound = resultCount / 65500;

                int sheetCount = 1;
                for (int m = 0; m < sheetRowRound; m++) {

                    int lowLimit = m * 65500;
                    int upLimit = (m + 1) * 65500;

                    
                    if(lowLimit !=0){
                        lowLimit++;
                    }
                    

                    String newQuery = "SELECT * FROM "
                            + "(SELECT S.*, ROWNUM as RN FROM ( "
                            + query
                            + " ) S "
                            + ") M WHERE M.RN BETWEEN "+lowLimit+" AND "+upLimit;


                    ResultSet rs = dbCon.search(con, newQuery);

                    String sheetName = "Sheet".concat("" + sheetCount);
                    sheet = workbook.createSheet(sheetName);

                    rsC = 1;
                    while (rs.next()) {

                        HSSFRow rowhead = sheet.createRow((int) 0);
                        HSSFRow row = sheet.createRow((int) rsC);
                        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                            rowhead.createCell(i).setCellValue(rs.getMetaData().getColumnName(i));
                            row.createCell(i).setCellValue(rs.getString(i));
                        }

                        rsC++;

                    }
                    sheetCount++;

                }

            } else {

                String sheetName = "Sheet1";
                sheet = workbook.createSheet(sheetName);
                ResultSet rs = dbCon.search(con, query);
                rsC = 1;

                while (rs.next()) {

                    HSSFRow rowhead = sheet.createRow((int) 0);
                    HSSFRow row = sheet.createRow((int) rsC);
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

                        rowhead.createCell(i).setCellValue(rs.getMetaData().getColumnName(i));
                        row.createCell(i).setCellValue(rs.getString(i));
                    }

                    rsC++;

                }

            }

//            while (rs.next()) {
//
//                if (rsC == 65500 || rsC == 1) {
//                    String sheetName = "Sheet".concat("" + sheetCount);
//                    sheet = workbook.createSheet(sheetName);
//                    sheetRowRound = 0;
//                    rsC = 1;
//                    sheetCount++;
//                }
//
//                HSSFRow rowhead = sheet.createRow((int) 0);
//                HSSFRow row = sheet.createRow((int) rsC);
//                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//                    rowhead.createCell(i).setCellValue(rs.getMetaData().getColumnName(i));
//                    row.createCell(i).setCellValue(rs.getString(i));
//                }
//
//                rsC++;
//                sheetRowRound++;
//
//            }
            FileOutputStream fileOut = new FileOutputStream(filename);

            workbook.write(fileOut);
            fileOut.close();

            dbCon.ConectionClose(con);
        } catch (Exception er) {
            er.printStackTrace();

        }
        return bytesEncoded;
    }
}
