/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ShopVisitItemList;
import org.dms.ws.singer.entities.ShopVistCheck;

/**
 *
 * @author SDU
 */
public class ShopVisitController {

    public static MessageWrapper getShopVisitList(String latitute,
            String logtitute,
            int distance) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getShopVisitList Method Call....................");
        ArrayList<ShopVistCheck> shopVisitList = new ArrayList<>();
        int totalrecode = 0;
        try {
                    
            ResultSet rs ;
//            if (logtitute.equalsIgnoreCase("all") || latitute.equalsIgnoreCase("all")) {
//
//              rs = dbCon.search(con, "SELECT D.BIS_ID,"
//                        + "D.USER_ID,"
//                        + "D.FIRST_NAME,"
//                        + "D.LAST_NAME,"
//                        + "B.BIS_NAME,"
//                        + "B.LONGITUDE,"
//                        + "B.LATITUDE "
//                        + "FROM SD_USER_BUSINESS_STRUCTURES D,SD_BUSINESS_STRUCTURES B "
//                        + "WHERE  BIS_STRU_TYPE_ID =2 "
//                        + "AND D.BIS_ID = B.BIS_ID (+) "
//                        + "ORDER BY D.BIS_ID ASC");
//                
//            } else {

                rs = dbCon.search(con, "SELECT D.BIS_ID,"
                        + " D.USER_ID,"
                        + " D.FIRST_NAME,"
                        + " D.LAST_NAME,"
                        + " B.BIS_NAME,"
                        + " B.LONGITUDE,"
                        + " B.LATITUDE  "
                        + " FROM SD_USER_BUSINESS_STRUCTURES D,SD_BUSINESS_STRUCTURES B "
                        + " WHERE D.BIS_ID IN (SELECT BIS_ID "
                        + " FROM (SELECT BIS_ID,"
                        + " ((( 3959 * acos(cos( RADIANS(" + latitute + ")) * cos(RADIANS(LATITUDE))* cos( RADIANS(LONGITUDE )- RADIANS(" + logtitute + ") ) + sin( RADIANS(" + latitute + ") ) * sin( RADIANS( LATITUDE ) ) ) )*8)/5) AS DISTALNCE "
                        + " FROM SD_BUSINESS_STRUCTURES WHERE BIS_STRU_TYPE_ID =2) "
                        + " WHERE DISTALNCE/1000 <= " + distance + ") "
                        + " AND D.BIS_ID = B.BIS_ID (+)"
                        + " ORDER BY D.BIS_ID ASC");
         //   }
            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                ShopVistCheck wr = new ShopVistCheck();
                wr.setBisId(rs.getInt("BIS_ID"));
                wr.setUserId(rs.getString("USER_ID"));
                wr.setFirstName(rs.getString("FIRST_NAME"));
                wr.setLastName(rs.getString("LAST_NAME"));
                wr.setBisName(rs.getString("BIS_NAME"));
                wr.setLongtitude(rs.getString("LONGITUDE"));
                wr.setLatitude(rs.getString("LATITUDE"));
                shopVisitList.add(wr);
            }

        } catch (Exception ex) {
            logger.info("Error getWarrentyRegistration Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = shopVisitList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(shopVisitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addShopVisitCheck(ShopVistCheck svc_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("addShopVisitCheck Mehtod Call.......");
        int result = 0;
        int shop_visit_max = 0;
        try {

            ResultSet rs_max_shop = dbCon.search(con, "SELECT NVL(MAX(SHOP_VISIT_ID),0) MAX FROM SD_SHOP_VISIT_CHECK");

            while (rs_max_shop.next()) {
                shop_visit_max = rs_max_shop.getInt("MAX");
            }

            shop_visit_max = shop_visit_max + 1;

            String column = "SHOP_VISIT_ID,DSR,"
                    + "LONGITUDE,LATITUDE,"
                    + "VISIT_DATE,VISIT_TIME,"
                    + "SUB_DEALER,STATUS,"
                    + "USER_INSERTED,DATE_INSERTED";

            String values = "?,?,?,?,SYSDATE,SYSDATE,?,1,?,SYSDATE";

            PreparedStatement ps_in_sv = dbCon.prepare(con, "INSERT INTO SD_SHOP_VISIT_CHECK(" + column + ") VALUES(" + values + ")");
            ps_in_sv.setInt(1, shop_visit_max);
            ps_in_sv.setInt(2, svc_info.getdSR());
            ps_in_sv.setString(3, svc_info.getLongtitude());
            ps_in_sv.setString(4, svc_info.getLatitude());
            ps_in_sv.setInt(5, svc_info.getSubDealer());
            ps_in_sv.setString(6, user_id);
            ps_in_sv.executeUpdate();

            ResultSet rs_modle_count = dbCon.search(con, "SELECT MODEL_NO,"
                    + "COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE BIS_ID = " + svc_info.getdSR() + " "
                    + "GROUP BY MODEL_NO");

            int max_item_seq = 0;

            ResultSet rs_max_item = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_SHOP_VISIT_ITEM_LIST");
            while (rs_max_item.next()) {
                max_item_seq = rs_max_item.getInt("MAX");
            }

            PreparedStatement ps_in_shop_item = dbCon.prepare(con, "INSERT INTO SD_SHOP_VISIT_ITEM_LIST(SEQ_NO,"
                    + "SHOP_VISIT_ID,MODLE_NO,"
                    + "NO_OF_ITEM,STATUS,"
                    + "USER_INSERTED,DATE_INSERTED) "
                    + "VALUES(?,?,?,?,1,?,SYSDATE)");

            while (rs_modle_count.next()) {
                max_item_seq = max_item_seq + 1;
                ps_in_shop_item.setInt(1, max_item_seq);
                ps_in_shop_item.setInt(2, shop_visit_max);
                ps_in_shop_item.setInt(3, rs_modle_count.getInt("MODEL_NO"));
                ps_in_shop_item.setInt(4, rs_modle_count.getInt("CNT"));
                ps_in_shop_item.setString(5, user_id);
                ps_in_shop_item.addBatch();
            }
            ps_in_shop_item.executeBatch();

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in addShopVisitCheck Mehtod  :" + ex.toString());
            result = 9;
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Shop Visit Insert Successfully");
            logger.info("Shop Visit Insert Succesfully..........");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Shop Visit Insert");
            logger.info("Error in Shop Visit Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getDayMovement(int bis_id,
            String date, int start, int limit) {

        logger.info("getDayMovement Method Call....................");
        ArrayList<ShopVistCheck> shopVisitList = new ArrayList<>();
        int totalrecode = 1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "S.SHOP_VISIT_ID,"
                    + "S.DSR,"
                    + "S.LONGITUDE,"
                    + "S.LATITUDE,"
                    + "TO_CHAR(S.VISIT_TIME,'YYYY/MM/DD HH24:MI:SS') VISIT_TIME,"
                    + "B.BIS_NAME,"
                    + "B.CONTACT_NO,"
                    + "B1.BIS_NAME SUB_NAME,"
                    + "B2.BIS_NAME PRT_NAME";

            String whereClous = "S.DSR  = B.BIS_ID (+)  "
                    + "AND S.SUB_DEALER = B1.BIS_ID (+) "
                    + "AND B.PARENT_BIS_ID = B2.BIS_ID (+) "
                    + "AND DSR = " + bis_id + " "
                    + "AND TO_DATE(TO_CHAR(VISIT_DATE,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + date + "','YYYY/MM/DD')";

            String table = "SD_SHOP_VISIT_CHECK S,SD_BUSINESS_STRUCTURES B,"
                    + "SD_BUSINESS_STRUCTURES B1,SD_BUSINESS_STRUCTURES B2";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE  " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (ORDER BY S.SHOP_VISIT_ID DESC) RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            ArrayList<ShopVistCheck> tempShopVisitList = new ArrayList<>();

            while (rs.next()) {
                ShopVistCheck sc = new ShopVistCheck();
                sc.setShopVistId(rs.getInt("SHOP_VISIT_ID"));
                sc.setdSR(rs.getInt("DSR"));
                sc.setBisId(rs.getInt("DSR"));
                sc.setLongtitude(rs.getString("LONGITUDE"));
                sc.setLatitude(rs.getString("LATITUDE"));
                sc.setVisitDate(rs.getString("VISIT_TIME"));
                sc.setBisName(rs.getString("BIS_NAME"));
                sc.setContactNo(rs.getString("CONTACT_NO"));
                sc.setSubDelerName(rs.getString("SUB_NAME"));
                sc.setParentName(rs.getString("PRT_NAME"));
                tempShopVisitList.add(sc);
            }

            //ArrayList<ShopVisitItemList> shopVisitItemList = new ArrayList<>();
            for (ShopVistCheck svc : tempShopVisitList) {
                ArrayList<ShopVisitItemList> shopVisitItemList = new ArrayList<>();
                ShopVistCheck svclist = new ShopVistCheck();
                ResultSet rs_modle_count = dbCon.search(con, "SELECT S.SEQ_NO,"
                        + "S.SHOP_VISIT_ID,"
                        + "S.MODLE_NO,"
                        + "S.NO_OF_ITEM,"
                        + "M.MODEL_DESCRIPTION  "
                        + "FROM SD_SHOP_VISIT_ITEM_LIST S,SD_MODEL_LISTS M "
                        + "WHERE S.MODLE_NO = M.MODEL_NO (+) "
                        + "AND S.SHOP_VISIT_ID = " + svc.getShopVistId() + "");
                while (rs_modle_count.next()) {
                    ShopVisitItemList si = new ShopVisitItemList();
                    si.setSeqNo(rs_modle_count.getInt("SEQ_NO"));
                    si.setShopVisitId(rs_modle_count.getInt("SHOP_VISIT_ID"));
                    si.setModleNo(rs_modle_count.getInt("MODLE_NO"));
                    si.setModleDesc(rs_modle_count.getString("MODEL_DESCRIPTION"));
                    si.setItemCount(rs_modle_count.getInt("NO_OF_ITEM"));
                    shopVisitItemList.add(si);
                }
                svclist.setShopVisitItemList(shopVisitItemList);
                svclist.setShopVistId(svc.getShopVistId());
                svclist.setdSR(svc.getdSR());
                svclist.setBisId(svc.getBisId());
                svclist.setLongtitude(svc.getLongtitude());
                svclist.setLatitude(svc.getLatitude());
                svclist.setVisitDate(svc.getVisitDate());
                svclist.setBisName(svc.getBisName());
                svclist.setContactNo(svc.getContactNo());
                svclist.setSubDelerName(svc.getSubDelerName());
                svclist.setParentName(svc.getParentName());
                shopVisitList.add(svclist);

            }
        } catch (Exception ex) {
            logger.info("Error getDayMovement Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(shopVisitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

}
