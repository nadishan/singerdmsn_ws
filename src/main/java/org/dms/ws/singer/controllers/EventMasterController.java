/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.EventAward;
import org.dms.ws.singer.entities.EventBisType;
import org.dms.ws.singer.entities.EventBussinessList;
import org.dms.ws.singer.entities.EventMaster;
import org.dms.ws.singer.entities.EventNonSalesRule;
import org.dms.ws.singer.entities.EventRulePointScheme;
import org.dms.ws.singer.entities.EventSalesRule;
import org.dms.ws.singer.entities.MdeCode;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class EventMasterController {

    public static MessageWrapper getEventMasterList(int event_id,
            String event_name,
            String event_desc,
            String start_date,
            String end_date,
            String threshold,
            String status,
            int statusval,
            String statusString,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getEventMasterList method Call..........");
        ArrayList<EventMaster> eventList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            int stu = 0;
            order = DBMapper.eventMaster(order);
            status = status.toUpperCase();
            String statusHint = status.substring(0, 1);
            if (statusHint.equals("P")) {
                stu = 1;
            } else if (statusHint.equals("S")) {
                stu = 3;
            } else if (statusHint.equals("C")) {
                stu = 9;
            } else {
                stu = statusval;
            }

            String eventId = ((event_id == 0) ? "" : "AND EVENT_ID = '" + event_id + "' ");
            String eventName = (event_name.equalsIgnoreCase("all") ? "NVL(EVENT_NAME,'AA') = NVL(EVENT_NAME,'AA') " : "UPPER(EVENT_NAME) LIKE UPPER('%" + event_name + "%')");
            String eventDesc = (event_desc.equalsIgnoreCase("all") ? "NVL(EVENT_DESC,'AA') = NVL(EVENT_DESC,'AA') " : "UPPER(EVENT_DESC) LIKE UPPER('%" + event_desc + "%')");
            String startDate = (start_date.equalsIgnoreCase("all") ? "NVL(START_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(START_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(START_DATE,'YYYY/MM/DD') = TO_DATE('%" + start_date + "%','YYYY/MM/DD')");
            String endDate = (end_date.equalsIgnoreCase("all") ? "NVL(END_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(END_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(END_DATE,'YYYY/MM/DD') = TO_DATE('%" + end_date + "%','YYYY/MM/DD')");
            String Thresold = (threshold.equalsIgnoreCase("all") ? "NVL(THRESHOLD,'AA') = NVL(THRESHOLD,'AA') " : "UPPER(THRESHOLD) LIKE UPPER('%" + threshold + "%')");
            String Status = (stu == 0 ? "" : "AND STATUS = " + stu + "");
            String Status_String = (statusString.equalsIgnoreCase("all") ? "" : "AND STATUS IN ("+statusString+")");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY EVENT_ID" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "EVENT_ID,EVENT_NAME,"
                    + "EVENT_DESC,"
                    + "TO_CHAR(START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(END_DATE,'YYYY/MM/DD') END_DATE,"
                    + "THRESHOLD,STATUS,"
                    + "EVENT_MGR_COMMENTS,MKT_MGR_COMMENTS";

            String whereClous = "" + eventName + " "
                    + "" + eventId + " "
                    + "AND " + eventDesc + " "
                    + "AND " + startDate + " "
                    + "AND " + endDate + " "
                    + "AND " + Thresold + " "
                    + " " + Status + " "
                    + " "+ Status_String+"";

            String tables = "SD_EVENT_MASTER";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventMaster em = new EventMaster();
                totalrecode = rs.getInt("CNT");
                em.setEventId(rs.getInt("EVENT_ID"));;
                em.setEventName(rs.getString("EVENT_NAME"));
                em.setEventDesc(rs.getString("EVENT_DESC"));
                em.setStartDate(rs.getString("START_DATE"));
                em.setEndDate(rs.getString("END_DATE"));
                em.setThreshold(rs.getString("THRESHOLD"));
                em.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    em.setStatusMsg("Planning");
                } else if (rs.getInt("STATUS") == 3) {
                    em.setStatusMsg("Started");
                } else if (rs.getInt("STATUS") == 9) {
                    em.setStatusMsg("Closed");
                } else {
                    em.setStatusMsg("None");
                }
                em.setEventMrgComment(rs.getString("EVENT_MGR_COMMENTS"));
                em.setMktMrgComment(rs.getString("MKT_MGR_COMMENTS"));
                eventList.add(em);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error getEventMasterList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getBussinessStructTypeList() {
        logger.info("getBussinessStructTypeList method Call..........");
        ArrayList<EventBisType> bisTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String selectColumn = "T.BIS_STRU_TYPE_ID,"
                    + "T.BIS_STRU_TYPE_DESC,"
                    + "(SELECT COUNT(*) FROM SD_USER_BUSINESS_STRUCTURES WHERE BIS_ID IN (SELECT BIS_ID FROM SD_BUSINESS_STRUCTURES WHERE BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID)) CNT";

            String tables = "SD_BIS_STRU_TYPES T";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                    + "FROM " + tables + " "
                    + "ORDER BY T.BIS_STRU_TYPE_ID");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventBisType bt = new EventBisType();
                totalrecode = 1;
                bt.setBisStruTypeId(rs.getInt("BIS_STRU_TYPE_ID"));;
                bt.setBisStruTypeName(rs.getString("BIS_STRU_TYPE_DESC"));
                bt.setBisStruTypeCount(rs.getInt("CNT"));
                bisTypeList.add(bt);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error getBussinessStructTypeList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(bisTypeList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getBussinessList(String bisTypeList,
            String bis_name,
            String bistype_name,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getBussinessList method Call..........");
        ArrayList<EventBussinessList> eventBisList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            if (bisTypeList.length() > 0) {

                order = DBMapper.eventBussinesss(order);

                String val = bisTypeList;

                String bisName = (bis_name.equalsIgnoreCase("all") ? "NVL(B.BIS_NAME,'AA') = NVL(B.BIS_NAME,'AA') " : "UPPER(B.BIS_NAME) LIKE UPPER('%" + bis_name + "%')");
                String biTypeName = (bistype_name.equalsIgnoreCase("all") ? "NVL(T.BIS_STRU_TYPE_DESC,'AA') = NVL(T.BIS_STRU_TYPE_DESC,'AA') " : "UPPER(T.BIS_STRU_TYPE_DESC) LIKE UPPER('%" + bistype_name + "%')");
                String Order = (order.equalsIgnoreCase("all") ? "ORDER BY U.BIS_ID" : "ORDER BY " + order + "   " + type + "");

                String selectColumn = "U.BIS_ID,U.USER_ID,U.FIRST_NAME,U.LAST_NAME,"
                        + "B.BIS_NAME,B.BIS_STRU_TYPE_ID,T.BIS_STRU_TYPE_DESC";

                String tables = "SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B,SD_BIS_STRU_TYPES T";

                String whereClous = "U.BIS_ID IN (SELECT BIS_ID FROM SD_BUSINESS_STRUCTURES WHERE BIS_STRU_TYPE_ID IN (" + val + "))  "
                        + "AND U.BIS_ID = B.BIS_ID (+) "
                        + "AND B.BIS_STRU_TYPE_ID = T.BIS_STRU_TYPE_ID (+)"
                        + "AND " + bisName + " "
                        + "AND " + biTypeName + " ";

                ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                        + "" + selectColumn + ",ROW_NUMBER() "
                        + "OVER (" + Order + ") RN "
                        + "FROM " + tables + " "
                        + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

                logger.info("Get Data to Result Set.............");

                while (rs.next()) {
                    EventBussinessList eb = new EventBussinessList();
                    totalrecode = rs.getInt("CNT");
                    eb.setBisId(rs.getInt("BIS_ID"));;
                    eb.setUserId(rs.getString("USER_ID"));
                    eb.setBisName(rs.getString("BIS_NAME"));
                    eb.setBisTypeId(rs.getInt("BIS_STRU_TYPE_ID"));
                    eb.setBisTypeDesc(rs.getString("BIS_STRU_TYPE_DESC"));
                    eventBisList.add(eb);
                }
                rs.close();
            }

        } catch (Exception ex) {
            logger.info("Error getBussinessStructTypeList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventBisList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addEventMaster(EventMaster eventMasterInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addEventMaster Method Call............");
        int result = 0;
        int event_max = 0;
        int event_name = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_event_name = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_EVENT_MASTER "
                    + "WHERE EVENT_NAME = '" + eventMasterInfo.getEventName() + "'");
            while (rs_event_name.next()) {
                event_name = rs_event_name.getInt("CNT");
            }
            rs_event_name.close();

            if (event_name > 0) {
                result = 2;

            } else {
                ResultSet rs_max_event = dbCon.search(con, "SELECT NVL(MAX(EVENT_ID),0) MAX FROM SD_EVENT_MASTER");
                while (rs_max_event.next()) {
                    event_max = rs_max_event.getInt("MAX");
                }
                rs_max_event.close();

                event_max = event_max + 1;

                PreparedStatement ps_in_event_master = dbCon.prepare(con, "INSERT INTO SD_EVENT_MASTER(EVENT_ID,EVENT_NAME,"
                        + "EVENT_DESC,START_DATE,"
                        + "END_DATE,THRESHOLD,"
                        + "STATUS,EVENT_MGR_COMMENTS,"
                        + "MKT_MGR_COMMENTS,USER_INSERTED,"
                        + "DATE_INSERTED) VALUES(?,?,?,TO_DATE(?,'YYYY/MM/DD'),TO_DATE(?,'YYYY/MM/DD'),?,1,?,?,?,SYSDATE)");

                ps_in_event_master.setInt(1, event_max);
                ps_in_event_master.setString(2, eventMasterInfo.getEventName());
                ps_in_event_master.setString(3, eventMasterInfo.getEventDesc());
                ps_in_event_master.setString(4, eventMasterInfo.getStartDate());
                ps_in_event_master.setString(5, eventMasterInfo.getEndDate());
                ps_in_event_master.setString(6, eventMasterInfo.getThreshold());
                ps_in_event_master.setString(7, eventMasterInfo.getEventMrgComment());
                ps_in_event_master.setString(8, eventMasterInfo.getMktMrgComment());
                ps_in_event_master.setString(9, user_id);
                ps_in_event_master.executeUpdate();

                ArrayList<EventBisType> eventBisType = eventMasterInfo.getEventBisTypeList();
                if (eventBisType.size() > 0) {
                    int max_part_id = 0;
                    ResultSet rs_max_bisType = dbCon.search(con, "SELECT NVL(MAX(EVENT_PART_ID),0) MAX "
                            + "FROM SD_EVENT_PARTICIPANTS");
                    while (rs_max_bisType.next()) {
                        max_part_id = rs_max_bisType.getInt("MAX");
                    }
                    rs_max_bisType.close();

                    PreparedStatement ps_in_bisType = dbCon.prepare(con, "INSERT INTO SD_EVENT_PARTICIPANTS(EVENT_PART_ID,EVENT_ID,BIS_TYPE_ID,USER_COUNT,STATUS,USER_INSERTED,DATE_INSERTED) "
                            + "VALUES(?,?,?,?,1,?,SYSDATE)");
                    for (EventBisType eb : eventBisType) {
                        max_part_id = max_part_id + 1;
                        ps_in_bisType.setInt(1, max_part_id);
                        ps_in_bisType.setInt(2, event_max);
                        ps_in_bisType.setInt(3, eb.getBisStruTypeId());
                        ps_in_bisType.setInt(4, eb.getBisStruTypeCount());
                        ps_in_bisType.setString(5, user_id);
                        ps_in_bisType.addBatch();

                        ArrayList<EventBussinessList> eventPaticDtl = eb.getEventBussinessList();
                        if (eventPaticDtl.size() > 0) {

                            int max_patci_seq = 0;
                            ResultSet rs_event_prt_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                    + "FROM SD_EVENT_PARTICIPANT_DTL "
                                    + "WHERE EVENT_PART_ID = " + max_part_id + "");
                            while (rs_event_prt_dtl.next()) {
                                max_patci_seq = rs_event_prt_dtl.getInt("MAX");
                            }
                            rs_event_prt_dtl.close();

                            PreparedStatement ps_in_part_dtl = dbCon.prepare(con, "INSERT INTO SD_EVENT_PARTICIPANT_DTL(EVENT_PART_ID,SEQ_NO,BIS_ID,USER_ID,STATUS,USER_INSERTED,DATE_INSERTED) "
                                    + "VALUES(?,?,?,?,1,?,SYSDATE)");
                            for (EventBussinessList ebl : eventPaticDtl) {
                                max_patci_seq = max_patci_seq + 1;
                                ps_in_part_dtl.setInt(1, max_part_id);
                                ps_in_part_dtl.setInt(2, max_patci_seq);
                                ps_in_part_dtl.setInt(3, ebl.getBisId());
                                ps_in_part_dtl.setString(4, ebl.getUserId());
                                ps_in_part_dtl.setString(5, user_id);
                                ps_in_part_dtl.addBatch();
                            }
                            ps_in_part_dtl.executeBatch();
                        }

                    }
                    ps_in_bisType.executeBatch();
                }

                ArrayList<EventSalesRule> eventSaleRule = eventMasterInfo.getEventSalesRuleList();
                if (eventSaleRule.size() > 0) {
                    int max_sale_rule = 0;
                    ResultSet rs_max_esr = dbCon.search(con, "SELECT NVL(MAX(RULE_ID),0) MAX "
                            + "FROM SD_EVENT_SALE_RULES");
                    while (rs_max_esr.next()) {
                        max_sale_rule = rs_max_esr.getInt("MAX");
                    }
                    rs_max_esr.close();

                    PreparedStatement ps_in_sales_rule = dbCon.prepare(con, "INSERT INTO SD_EVENT_SALE_RULES(RULE_ID,EVENT_ID,"
                            + "PRODUCT_FAMILY,BRAND,"
                            + "MODEL_NO,PERIOD,"
                            + "INTERVALS,ACCEPTED_INTERVALS,"
                            + "SALES_TYPE,POINTS,"
                            + "MIN_REQ,POINT_SCHEME_FLAG,"
                            + "TOT_ACHIEVEMENT,STATUS,"
                            + "USER_INSERTED,DATE_INSERTED) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,SYSDATE)");
                    for (EventSalesRule esr : eventSaleRule) {

                        max_sale_rule = max_sale_rule + 1;
                        ps_in_sales_rule.setInt(1, max_sale_rule);
                        ps_in_sales_rule.setInt(2, event_max);
                        ps_in_sales_rule.setString(3, esr.getProductFamily());
                        ps_in_sales_rule.setString(4, esr.getBarnd());
                        ps_in_sales_rule.setInt(5, esr.getModleNo());
                        ps_in_sales_rule.setInt(6, esr.getPeriod());
                        ps_in_sales_rule.setInt(7, esr.getIntervals());
                        ps_in_sales_rule.setInt(8, esr.getAcceptedInteval());
                        ps_in_sales_rule.setInt(9, esr.getSalesType());
                        ps_in_sales_rule.setDouble(10, esr.getPoint());
                        ps_in_sales_rule.setDouble(11, esr.getMinReq());
                        ps_in_sales_rule.setInt(12, esr.getPointSchemeFlag());
                        ps_in_sales_rule.setDouble(13, esr.getTotalAchivement());
                        ps_in_sales_rule.setString(14, user_id);
                        ps_in_sales_rule.executeUpdate();

                        int max_point_schema = 0;
                        ResultSet rs_point_schema = dbCon.search(con, "SELECT NVL(MAX(POINT_SCHEME_ID),0) MAX "
                                + "FROM SD_EVENT_RULE_POINT_SCHEMES");
                        while (rs_point_schema.next()) {
                            max_point_schema = rs_point_schema.getInt("MAX");
                        }
                        rs_point_schema.close();

                        PreparedStatement ps_in_point_schema = dbCon.prepare(con, "INSERT INTO SD_EVENT_RULE_POINT_SCHEMES(POINT_SCHEME_ID,RULE_ID,"
                                + "SALES_TYPE,NO_OF_UNITS,"
                                + "FIXED_POINTS,FLOAT_FLAG,"
                                + "ADDN_SALES_TYPE,ADDN_SALES_POINTS,"
                                + "ASSIGN_POINTS,STATUS,"
                                + "USER_INSERTED,DATE_INSERTED) "
                                + "VALUES (?,?,?,?,?,?,?,?,?,1,?,SYSDATE)");

                        ArrayList<EventRulePointScheme> rulePointSchme = new ArrayList<>();
                        rulePointSchme = esr.getEventRulePointSchemaList();

                        if (rulePointSchme.size() > 0) {
                            for (EventRulePointScheme ps : rulePointSchme) {

                                max_point_schema = max_point_schema + 1;

                                ps_in_point_schema.setInt(1, max_point_schema);
                                ps_in_point_schema.setInt(2, max_sale_rule);
                                ps_in_point_schema.setInt(3, ps.getSchemeSaleType());
                                ps_in_point_schema.setInt(4, ps.getSchmeNoOfUnit());
                                ps_in_point_schema.setDouble(5, ps.getScheamfixedPoint());
                                ps_in_point_schema.setInt(6, ps.getSchmefloatFlag());
                                ps_in_point_schema.setInt(7, ps.getSchmeAddnSalesType());
                                ps_in_point_schema.setDouble(8, ps.getSchmeAddnSalesPoint());
                                ps_in_point_schema.setDouble(9, ps.getSchmeAssignPoint());
                                ps_in_point_schema.setString(10, user_id);
                                ps_in_point_schema.executeUpdate();

                            }
                        }

                    }

                }
                ArrayList<EventNonSalesRule> eventNonSaleRules = eventMasterInfo.getEventNonSalesRuleList();
                if (eventNonSaleRules.size() > 0) {
                    int max_non_sale = 0;
                    ResultSet rs_non_sales = dbCon.search(con, "SELECT NVL(MAX(NS_RULE_ID),0) MAX "
                            + "FROM SD_EVENT_NON_SALE_RULES");
                    while (rs_non_sales.next()) {
                        max_non_sale = rs_non_sales.getInt("MAX");
                    }
                    rs_non_sales.close();

                    PreparedStatement ps_in_non_sales = dbCon.prepare(con, "INSERT INTO SD_EVENT_NON_SALE_RULES(NS_RULE_ID,EVENT_ID,"
                            + "DESCRIPTION,TARGET,"
                            + "POINTS,SALE_TYPE,"
                            + "STATUS,USER_INSERTED,"
                            + "DATE_INSERTED) "
                            + "VALUES (?,?,?,?,?,?,1,?,SYSDATE)");

                    for (EventNonSalesRule ens : eventNonSaleRules) {
                        max_non_sale = max_non_sale + 1;
                        ps_in_non_sales.setInt(1, max_non_sale);
                        ps_in_non_sales.setInt(2, event_max);
                        ps_in_non_sales.setString(3, ens.getDescription());
                        ps_in_non_sales.setInt(4, ens.getTaget());
                        ps_in_non_sales.setInt(5, ens.getPoint());
                        ps_in_non_sales.setInt(6, ens.getSalesType());
                        ps_in_non_sales.setString(7, user_id);
                        ps_in_non_sales.addBatch();
                    }
                    ps_in_non_sales.executeBatch();
                }

                ArrayList<EventAward> eventAwards = eventMasterInfo.getEvenAwardList();
                if (eventAwards.size() > 0) {

                    int max_event_award = 0;
                    ResultSet rs_event_award = dbCon.search(con, "SELECT NVL(MAX(AWARD_ID),0) MAX "
                            + "FROM SD_EVENT_AWARDS");
                    while (rs_event_award.next()) {
                        max_event_award = rs_event_award.getInt("MAX");
                    }
                    rs_event_award.close();

                    PreparedStatement ps_in_event_award = dbCon.prepare(con, "INSERT INTO SD_EVENT_AWARDS(AWARD_ID,EVENT_ID,"
                            + "PLACE,AWARD,STATUS,"
                            + "USER_INSERTED,DATE_INSERTED) "
                            + "VALUES (?,?,?,?,1,?,SYSDATE)");

                    for (EventAward ea : eventAwards) {
                        max_event_award = max_event_award + 1;
                        ps_in_event_award.setInt(1, max_event_award);
                        ps_in_event_award.setInt(2, event_max);
                        ps_in_event_award.setString(3, ea.getPalce());
                        ps_in_event_award.setString(4, ea.getAward());
                        ps_in_event_award.setString(5, user_id);
                        ps_in_event_award.addBatch();
                    }
                    ps_in_event_award.executeBatch();
                }

                result = 1;
            }

        } catch (Exception ex) {
            logger.info("Error in addEventMaster Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Event Create Successfully");
            logger.info("Event Create Successfully............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Event Name Already Exist");
            logger.info("Event Name Already Exist............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Create Event");
            logger.info("Error in Create Event..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editEventMaster(EventMaster eventMasterInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editEventMaster Method Call............");
        int result = 0;
        //  int event_max = 0;
        int event_name = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_event_name = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_EVENT_MASTER "
                    + "WHERE EVENT_NAME = '" + eventMasterInfo.getEventName() + "' "
                    + "AND EVENT_ID != " + eventMasterInfo.getEventId() + "");

            while (rs_event_name.next()) {
                event_name = rs_event_name.getInt("CNT");
            }

            if (event_name > 0) {
                result = 2;

            } else {

                PreparedStatement ps_up_event_master = dbCon.prepare(con, "UPDATE SD_EVENT_MASTER "
                        + "SET EVENT_NAME = ?,"
                        + "EVENT_DESC =?,"
                        + "START_DATE = TO_DATE(?,'YYYY/MM/DD'),"
                        + "END_DATE = TO_DATE(?,'YYYY/MM/DD'),"
                        + "THRESHOLD = ?,"
                        + "STATUS = ?,"
                        + "EVENT_MGR_COMMENTS = ?,"
                        + "MKT_MGR_COMMENTS = ?,"
                        + "USER_MODIFIED = ?,"
                        + "DATE_MODIFIED = SYSDATE "
                        + "WHERE EVENT_ID = ?");

                ps_up_event_master.setString(1, eventMasterInfo.getEventName());
                ps_up_event_master.setString(2, eventMasterInfo.getEventDesc());
                ps_up_event_master.setString(3, eventMasterInfo.getStartDate());
                ps_up_event_master.setString(4, eventMasterInfo.getEndDate());
                ps_up_event_master.setString(5, eventMasterInfo.getThreshold());
                ps_up_event_master.setInt(6, eventMasterInfo.getStatus());
                ps_up_event_master.setString(7, eventMasterInfo.getEventMrgComment());
                ps_up_event_master.setString(8, eventMasterInfo.getMktMrgComment());
                ps_up_event_master.setString(9, user_id);
                ps_up_event_master.setInt(10, eventMasterInfo.getEventId());
                ps_up_event_master.executeUpdate();

                ArrayList<EventBisType> eventBisType = eventMasterInfo.getEventBisTypeList();
                if (eventBisType.size() > 0) {

                    dbCon.save(con, "DELETE FROM SD_EVENT_PARTICIPANT_DTL "
                            + "WHERE EVENT_PART_ID IN (SELECT EVENT_PART_ID "
                            + "FROM SD_EVENT_PARTICIPANTS "
                            + "WHERE EVENT_ID  =" + eventMasterInfo.getEventId() + ")");

                    dbCon.save(con, "DELETE FROM SD_EVENT_PARTICIPANTS "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");

                    int max_part_id = 0;
                    ResultSet rs_max_bisType = dbCon.search(con, "SELECT NVL(MAX(EVENT_PART_ID),0) MAX "
                            + "FROM SD_EVENT_PARTICIPANTS");
                    while (rs_max_bisType.next()) {
                        max_part_id = rs_max_bisType.getInt("MAX");
                    }

                    PreparedStatement ps_in_bisType = dbCon.prepare(con, "INSERT INTO SD_EVENT_PARTICIPANTS(EVENT_PART_ID,EVENT_ID,BIS_TYPE_ID,USER_COUNT,STATUS,USER_INSERTED,DATE_INSERTED) "
                            + "VALUES(?,?,?,?,1,?,SYSDATE)");
                    for (EventBisType eb : eventBisType) {
                        max_part_id = max_part_id + 1;
                        ps_in_bisType.setInt(1, max_part_id);
                        ps_in_bisType.setInt(2, eventMasterInfo.getEventId());
                        ps_in_bisType.setInt(3, eb.getBisStruTypeId());
                        ps_in_bisType.setInt(4, eb.getBisStruTypeCount());
                        ps_in_bisType.setString(5, user_id);
                        ps_in_bisType.addBatch();
                        ArrayList<EventBussinessList> eventPaticDtl = eb.getEventBussinessList();
                        if (eventPaticDtl.size() > 0) {

                            int max_patci_seq = 0;
                            ResultSet rs_event_prt_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                    + "FROM SD_EVENT_PARTICIPANT_DTL "
                                    + "WHERE EVENT_PART_ID = " + max_part_id + "");
                            while (rs_event_prt_dtl.next()) {
                                max_patci_seq = rs_event_prt_dtl.getInt("MAX");
                            }

                            PreparedStatement ps_in_part_dtl = dbCon.prepare(con, "INSERT INTO SD_EVENT_PARTICIPANT_DTL(EVENT_PART_ID,SEQ_NO,BIS_ID,USER_ID,STATUS,USER_INSERTED,DATE_INSERTED) "
                                    + "VALUES(?,?,?,?,1,?,SYSDATE)");
                            for (EventBussinessList ebl : eventPaticDtl) {
                                max_patci_seq = max_patci_seq + 1;
                                ps_in_part_dtl.setInt(1, max_part_id);
                                ps_in_part_dtl.setInt(2, max_patci_seq);
                                ps_in_part_dtl.setInt(3, ebl.getBisId());
                                ps_in_part_dtl.setString(4, ebl.getUserId());
                                ps_in_part_dtl.setString(5, user_id);
                                ps_in_part_dtl.addBatch();
                            }
                            ps_in_part_dtl.executeBatch();
                        }

                    }
                    ps_in_bisType.executeBatch();
                } else {
                    dbCon.save(con, "DELETE FROM SD_EVENT_PARTICIPANT_DTL "
                            + "WHERE EVENT_PART_ID IN (SELECT EVENT_PART_ID "
                            + "FROM SD_EVENT_PARTICIPANTS "
                            + "WHERE EVENT_ID  =" + eventMasterInfo.getEventId() + ")");

                    dbCon.save(con, "DELETE FROM SD_EVENT_PARTICIPANTS "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");
                }

                ArrayList<EventSalesRule> eventSaleRule = eventMasterInfo.getEventSalesRuleList();

                if (eventSaleRule.size() > 0) {

                    dbCon.save(con, "DELETE FROM SD_EVENT_RULE_POINT_SCHEMES "
                            + "WHERE RULE_ID IN (SELECT RULE_ID "
                            + "FROM SD_EVENT_SALE_RULES "
                            + "WHERE EVENT_ID =" + eventMasterInfo.getEventId() + ")");

                    dbCon.save(con, "DELETE FROM SD_EVENT_SALE_RULES "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");

                    int max_sale_rule = 0;
                    ResultSet rs_max_esr = dbCon.search(con, "SELECT NVL(MAX(RULE_ID),0) MAX "
                            + "FROM SD_EVENT_SALE_RULES");
                    while (rs_max_esr.next()) {
                        max_sale_rule = rs_max_esr.getInt("MAX");
                    }

                    PreparedStatement ps_in_sales_rule = dbCon.prepare(con, "INSERT INTO SD_EVENT_SALE_RULES(RULE_ID,EVENT_ID,"
                            + "PRODUCT_FAMILY,BRAND,"
                            + "MODEL_NO,PERIOD,"
                            + "INTERVALS,ACCEPTED_INTERVALS,"
                            + "SALES_TYPE,POINTS,"
                            + "MIN_REQ,POINT_SCHEME_FLAG,"
                            + "TOT_ACHIEVEMENT,STATUS,"
                            + "USER_INSERTED,DATE_INSERTED) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,SYSDATE)");
                    for (EventSalesRule esr : eventSaleRule) {
                        max_sale_rule = max_sale_rule + 1;
                        ps_in_sales_rule.setInt(1, max_sale_rule);
                        ps_in_sales_rule.setInt(2, eventMasterInfo.getEventId());
                        ps_in_sales_rule.setString(3, esr.getProductFamily());
                        ps_in_sales_rule.setString(4, esr.getBarnd());
                        ps_in_sales_rule.setInt(5, esr.getModleNo());
                        ps_in_sales_rule.setInt(6, esr.getPeriod());
                        ps_in_sales_rule.setInt(7, esr.getIntervals());
                        ps_in_sales_rule.setInt(8, esr.getAcceptedInteval());
                        ps_in_sales_rule.setInt(9, esr.getSalesType());
                        ps_in_sales_rule.setDouble(10, esr.getPoint());
                        ps_in_sales_rule.setDouble(11, esr.getMinReq());
                        ps_in_sales_rule.setInt(12, esr.getPointSchemeFlag());
                        ps_in_sales_rule.setDouble(13, esr.getTotalAchivement());
                        ps_in_sales_rule.setString(14, user_id);
                        ps_in_sales_rule.executeUpdate();
                        int max_point_schema = 0;
                        ResultSet rs_point_schema = dbCon.search(con, "SELECT NVL(MAX(POINT_SCHEME_ID),0) MAX "
                                + "FROM SD_EVENT_RULE_POINT_SCHEMES");
                        while (rs_point_schema.next()) {
                            max_point_schema = rs_point_schema.getInt("MAX");
                        }

                        PreparedStatement ps_in_point_schema = dbCon.prepare(con, "INSERT INTO SD_EVENT_RULE_POINT_SCHEMES(POINT_SCHEME_ID,RULE_ID,"
                                + "SALES_TYPE,NO_OF_UNITS,"
                                + "FIXED_POINTS,FLOAT_FLAG,"
                                + "ADDN_SALES_TYPE,ADDN_SALES_POINTS,"
                                + "ASSIGN_POINTS,STATUS,"
                                + "USER_INSERTED,DATE_INSERTED) "
                                + "VALUES (?,?,?,?,?,?,?,?,?,1,?,SYSDATE)");

                        ArrayList<EventRulePointScheme> rulePointSchme = new ArrayList<>();
                        rulePointSchme = esr.getEventRulePointSchemaList();
                        for (EventRulePointScheme ps : rulePointSchme) {
                            max_point_schema = max_point_schema + 1;
                            ps_in_point_schema.setInt(1, max_point_schema);
                            ps_in_point_schema.setInt(2, max_sale_rule);
                            ps_in_point_schema.setInt(3, ps.getSchemeSaleType());
                            ps_in_point_schema.setInt(4, ps.getSchmeNoOfUnit());
                            ps_in_point_schema.setDouble(5, ps.getScheamfixedPoint());
                            ps_in_point_schema.setInt(6, ps.getSchmefloatFlag());
                            ps_in_point_schema.setInt(7, ps.getSchmeAddnSalesType());
                            ps_in_point_schema.setDouble(8, ps.getSchmeAddnSalesPoint());
                            ps_in_point_schema.setDouble(9, ps.getSchmeAssignPoint());
                            ps_in_point_schema.setString(10, user_id);
                            ps_in_point_schema.executeUpdate();
                        }
                    }

                } else {

                    dbCon.save(con, "DELETE FROM SD_EVENT_RULE_POINT_SCHEMES "
                            + "WHERE RULE_ID IN (SELECT RULE_ID "
                            + "FROM SD_EVENT_SALE_RULES "
                            + "WHERE EVENT_ID =" + eventMasterInfo.getEventId() + ")");

                    dbCon.save(con, "DELETE FROM SD_EVENT_SALE_RULES "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");
                }

                ArrayList<EventNonSalesRule> eventNonSaleRules = eventMasterInfo.getEventNonSalesRuleList();
                if (eventNonSaleRules.size() > 0) {

                    dbCon.save(con, "DELETE FROM SD_EVENT_NON_SALE_RULES "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");

                    int max_non_sale = 0;
                    ResultSet rs_non_sales = dbCon.search(con, "SELECT NVL(MAX(NS_RULE_ID),0) MAX "
                            + "FROM SD_EVENT_NON_SALE_RULES");
                    while (rs_non_sales.next()) {
                        max_non_sale = rs_non_sales.getInt("MAX");
                    }
                    rs_non_sales.close();

                    PreparedStatement ps_in_non_sales = dbCon.prepare(con, "INSERT INTO SD_EVENT_NON_SALE_RULES(NS_RULE_ID,EVENT_ID,"
                            + "DESCRIPTION,TARGET,"
                            + "POINTS,SALE_TYPE,"
                            + "STATUS,USER_INSERTED,"
                            + "DATE_INSERTED) "
                            + "VALUES (?,?,?,?,?,?,1,?,SYSDATE)");

                    for (EventNonSalesRule ens : eventNonSaleRules) {
                        max_non_sale = max_non_sale + 1;
                        ps_in_non_sales.setInt(1, max_non_sale);
                        ps_in_non_sales.setInt(2, eventMasterInfo.getEventId());
                        ps_in_non_sales.setString(3, ens.getDescription());
                        ps_in_non_sales.setInt(4, ens.getTaget());
                        ps_in_non_sales.setInt(5, ens.getPoint());
                        ps_in_non_sales.setInt(6, ens.getSalesType());
                        ps_in_non_sales.setString(7, user_id);
                        ps_in_non_sales.addBatch();
                    }
                    ps_in_non_sales.executeBatch();

                } else {

                    dbCon.save(con, "DELETE FROM SD_EVENT_NON_SALE_RULES "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");
                }

                ArrayList<EventAward> eventAwards = eventMasterInfo.getEvenAwardList();
                if (eventAwards.size() > 0) {

                    dbCon.save(con, "DELETE FROM SD_EVENT_AWARDS "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");

                    int max_event_award = 0;
                    ResultSet rs_event_award = dbCon.search(con, "SELECT NVL(MAX(AWARD_ID),0) MAX "
                            + "FROM SD_EVENT_AWARDS");
                    while (rs_event_award.next()) {
                        max_event_award = rs_event_award.getInt("MAX");
                    }
                    rs_event_award.close();

                    PreparedStatement ps_in_event_award = dbCon.prepare(con, "INSERT INTO SD_EVENT_AWARDS(AWARD_ID,EVENT_ID,"
                            + "PLACE,AWARD,STATUS,"
                            + "USER_INSERTED,DATE_INSERTED) "
                            + "VALUES (?,?,?,?,1,?,SYSDATE)");

                    for (EventAward ea : eventAwards) {
                        max_event_award = max_event_award + 1;
                        ps_in_event_award.setInt(1, max_event_award);
                        ps_in_event_award.setInt(2, eventMasterInfo.getEventId());
                        ps_in_event_award.setString(3, ea.getPalce());
                        ps_in_event_award.setString(4, ea.getAward());
                        ps_in_event_award.setString(5, user_id);
                        ps_in_event_award.addBatch();
                    }
                    ps_in_event_award.executeBatch();

                } else {
                    dbCon.save(con, "DELETE FROM SD_EVENT_AWARDS "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");
                }

                result = 1;
            }

        } catch (Exception ex) {
            logger.info("Error in editEventMaster Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Event Update Successfully");
            logger.info("Event Update Successfully............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Event Name Already Exist");
            logger.info("Event Name Already Exist............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Update Event");
            logger.info("Error in Update Event..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

//    public static MessageWrapper getEventMasterDetails(int event_id) {
//        logger.info("getEventMasterDetails method Call..........");
//        ArrayList<EventMaster> eventList = new ArrayList<>();
//        int totalrecode = 0;
//        DbCon dbCon = new DbCon();
//        Connection con = dbCon.getCon();
//        try {
//            String selectColumn = "EVENT_ID,EVENT_NAME,EVENT_DESC,"
//                    + "TO_CHAR(START_DATE,'YYYY/MM/DD') START_DATE,"
//                    + "TO_CHAR(END_DATE,'YYYY/MM/DD') END_DATE,"
//                    + "THRESHOLD,STATUS,"
//                    + "EVENT_MGR_COMMENTS,MKT_MGR_COMMENTS";
//
//            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM SD_EVENT_MASTER "
//                    + "WHERE EVENT_ID = " + event_id + "");
//
//            logger.info("Get Data to Result Set.............");
//
//            while (rs.next()) {
//                EventMaster em = new EventMaster();
//                totalrecode = 1;
//                em.setEventId(rs.getInt("EVENT_ID"));;
//                em.setEventName(rs.getString("EVENT_NAME"));
//                em.setEventDesc(rs.getString("EVENT_DESC"));
//                em.setStartDate(rs.getString("START_DATE"));
//                em.setEndDate(rs.getString("END_DATE"));
//                em.setThreshold(rs.getString("THRESHOLD"));
//                em.setStatus(rs.getInt("STATUS"));
//                if (rs.getInt("STATUS") == 1) {
//                    em.setStatusMsg("Planning");
//                } else if (rs.getInt("STATUS") == 3) {
//                    em.setStatusMsg("Started");
//                } else if (rs.getInt("STATUS") == 9) {
//                    em.setStatusMsg("Closed");
//                } else {
//                    em.setStatusMsg("None");
//                }
//                em.setEventMrgComment(rs.getString("EVENT_MGR_COMMENTS"));
//                em.setMktMrgComment(rs.getString("MKT_MGR_COMMENTS"));
//                // eventList.add(em);
//
////------------------------------------------------------------------------------                
////------------------------------------------------------------------------------
//                ArrayList<EventSalesRule> eventSalesRuleList = new ArrayList<>();
//
//                String salesRule = "E.RULE_ID,"
//                        + "E.EVENT_ID,"
//                        + "E.PRODUCT_FAMILY,"
//                        + "E.BRAND,"
//                        + "E.MODEL_NO,"
//                        + "E.PERIOD,"
//                        + "E.INTERVALS,"
//                        + "E.ACCEPTED_INTERVALS,"
//                        + "E.SALES_TYPE,"
//                        + "E.POINTS,"
//                        + "E.MIN_REQ,"
//                        + "E.POINT_SCHEME_FLAG,"
//                        + "E.TOT_ACHIEVEMENT,"
//                        + "E.STATUS,"
//                        + "M.MODEL_DESCRIPTION";
//
//                ResultSet rs_salesRule = dbCon.search(con, "SELECT " + salesRule + " FROM SD_EVENT_SALE_RULES E,SD_MODEL_LISTS M "
//                        + "WHERE E.MODEL_NO = M.MODEL_NO (+)  "
//                        + "AND EVENT_ID = " + event_id + "");
//
//                logger.info("Get Data to Result Set.............");
//
//                while (rs_salesRule.next()) {
//                    EventSalesRule esr = new EventSalesRule();
//                    esr.setRuleId(rs_salesRule.getInt("RULE_ID"));;
//                    esr.setEventId(rs_salesRule.getInt("EVENT_ID"));
//                    esr.setProductFamily(rs_salesRule.getString("PRODUCT_FAMILY"));
//                    esr.setBarnd(rs_salesRule.getString("BRAND"));
//                    esr.setModleNo(rs_salesRule.getInt("MODEL_NO"));
//                    esr.setPeriod(rs_salesRule.getInt("PERIOD"));
//                    esr.setIntervals(rs_salesRule.getInt("INTERVALS"));
//                    esr.setAcceptedInteval(rs_salesRule.getInt("ACCEPTED_INTERVALS"));
//                    esr.setSalesType(rs_salesRule.getInt("SALES_TYPE"));
//                    esr.setPoint(rs_salesRule.getDouble("POINTS"));
//                    esr.setMinReq(rs_salesRule.getDouble("MIN_REQ"));
//                    esr.setPointSchemeFlag(rs_salesRule.getInt("POINT_SCHEME_FLAG"));
//                    esr.setTotalAchivement(rs_salesRule.getDouble("TOT_ACHIEVEMENT"));
//                    esr.setStatus(rs_salesRule.getInt("STATUS"));
//                    esr.setModleDesc(rs_salesRule.getString("MODEL_DESCRIPTION"));
//
//                    // ArrayList<EventRulePointScheme> pointSchemaList = new ArrayList<>();
//                    String selectPointSchema = "POINT_SCHEME_ID,"
//                            + "RULE_ID,"
//                            + "SALES_TYPE,"
//                            + "NO_OF_UNITS,"
//                            + "FIXED_POINTS,"
//                            + "FLOAT_FLAG,"
//                            + "ADDN_SALES_TYPE,"
//                            + "ADDN_SALES_POINTS,"
//                            + "ASSIGN_POINTS,"
//                            + "STATUS";
//
//                    ResultSet rs_pointSchema = dbCon.search(con, "SELECT " + selectPointSchema + " FROM SD_EVENT_RULE_POINT_SCHEMES "
//                            + "WHERE RULE_ID = " + rs_salesRule.getInt("RULE_ID") + "");
//
//                    ArrayList<EventRulePointScheme> pointSchemaList = new ArrayList<>();
//
//                    while (rs_pointSchema.next()) {
//                        EventRulePointScheme ps = new EventRulePointScheme();
//                        ps.setPointSchemaId(rs_pointSchema.getInt("POINT_SCHEME_ID"));;
//                        ps.setSalesType(rs_pointSchema.getInt("SALES_TYPE"));
//                        ps.setNoOfUnit(rs_pointSchema.getInt("NO_OF_UNITS"));
//                        ps.setFixedPoint(rs_pointSchema.getDouble("FIXED_POINTS"));
//                        ps.setFloatFlag(rs_pointSchema.getInt("FLOAT_FLAG"));
//                        ps.setAddnSalesType(rs_pointSchema.getInt("ADDN_SALES_TYPE"));
//                        ps.setAddnSalesPoint(rs_pointSchema.getDouble("ADDN_SALES_POINTS"));
//                        ps.setAssignPoint(rs_pointSchema.getDouble("ASSIGN_POINTS"));
//                        ps.setStatus(rs_pointSchema.getInt("STATUS"));
//                        pointSchemaList.add(ps);
//                    }
//                    rs_pointSchema.close();
//
//                    esr.setEventRulePointSchemaList(pointSchemaList);
//                    eventSalesRuleList.add(esr);
//                }
//                rs_salesRule.close();
//
//                em.setEventSalesRuleList(eventSalesRuleList);
//
//                //--------------------------------------------------------------------------            
//                ArrayList<EventNonSalesRule> nonSalesList = new ArrayList<>();
//
//                String selectNonSales = "NS_RULE_ID,EVENT_ID,"
//                        + "DESCRIPTION,TARGET,"
//                        + "POINTS,SALE_TYPE,STATUS";
//
//                ResultSet rs_nonSales = dbCon.search(con, "SELECT " + selectNonSales + " FROM SD_EVENT_NON_SALE_RULES "
//                        + "WHERE EVENT_ID = " + event_id + "");
//
//                while (rs_nonSales.next()) {
//                    EventNonSalesRule ns = new EventNonSalesRule();
//                    ns.setNonSalesRule(rs_nonSales.getInt("NS_RULE_ID"));;
//                    ns.setEventId(rs_nonSales.getInt("EVENT_ID"));
//                    ns.setDescription(rs_nonSales.getString("DESCRIPTION"));
//                    ns.setTaget(rs_nonSales.getInt("TARGET"));
//                    ns.setPoint(rs_nonSales.getInt("POINTS"));
//                    ns.setSalesType(rs_nonSales.getInt("SALE_TYPE"));
//                    ns.setStatus(rs_nonSales.getInt("STATUS"));
//                    nonSalesList.add(ns);
//                }
//                rs_nonSales.close();
//
//                em.setEventNonSalesRuleList(nonSalesList);
////------------------------------------------------------------------------------                
//
//                ArrayList<EventAward> awardList = new ArrayList<>();
//
//                String selectAward = "AWARD_ID,EVENT_ID,AWARD,PLACE,STATUS";
//
//                ResultSet rs_award = dbCon.search(con, "SELECT " + selectAward + " FROM SD_EVENT_AWARDS "
//                        + "WHERE EVENT_ID = " + event_id + "");
//
//                while (rs_award.next()) {
//                    EventAward ea = new EventAward();
//                    ea.setAwardId(rs_award.getInt("AWARD_ID"));;
//                    ea.setEventid(rs_award.getInt("EVENT_ID"));
//                    ea.setAward(rs_award.getString("AWARD"));
//                    ea.setPalce(rs_award.getString("PLACE"));
//                    ea.setStatus(rs_award.getInt("STATUS"));
//                    awardList.add(ea);
//                }
//                rs_award.close();
//
//                em.setEvenAwardList(awardList);
//                //--------------------------------------------------------------------------
//
//                ArrayList<EventBisType> eventbisTypeList = new ArrayList<>();
//
//                ArrayList<EventBisType> temEventbisTypeList = new ArrayList<>();
//                String selectBisType = "E.EVENT_PART_ID,E.EVENT_ID,"
//                        + "E.BIS_TYPE_ID,E.USER_COUNT,E.STATUS,B.BIS_STRU_TYPE_DESC";
//
//                ResultSet rs_bisType = dbCon.search(con, "SELECT " + selectBisType + " FROM SD_EVENT_PARTICIPANTS E,SD_BIS_STRU_TYPES B "
//                        + "WHERE E.BIS_TYPE_ID = B.BIS_STRU_TYPE_ID (+) "
//                        + "AND E.EVENT_ID = " + event_id + "");
//
//                logger.info("Get Data to Result Set.............");
//                while (rs_bisType.next()) {
//                    EventBisType ebt = new EventBisType();
//                    ebt.setEventPartId(rs_bisType.getInt("EVENT_PART_ID"));;
//                    ebt.setEventId(rs_bisType.getInt("EVENT_ID"));
//                    ebt.setBisStruTypeId(rs_bisType.getInt("BIS_TYPE_ID"));
//                    ebt.setBisStruTypeCount(rs_bisType.getInt("USER_COUNT"));
//                    ebt.setStatus(rs_bisType.getInt("STATUS"));
//                    ebt.setBisStruTypeName(rs_bisType.getString("BIS_STRU_TYPE_DESC"));
//                    temEventbisTypeList.add(ebt);
//                }
//
//                ArrayList<EventBussinessList> eventBussList = new ArrayList<>();
//
//                String selectPartDtl = "EVENT_PART_ID,SEQ_NO,BIS_ID,USER_ID,STATUS";
//
//                for (EventBisType teb : temEventbisTypeList) {
//
//                    EventBisType ebt = new EventBisType();
//
//                    ResultSet rs_prtDtl = dbCon.search(con, "SELECT " + selectPartDtl + " FROM SD_EVENT_PARTICIPANT_DTL "
//                            + "WHERE EVENT_PART_ID = " + teb.getEventPartId() + "");
//                    while (rs_prtDtl.next()) {
//                        EventBussinessList ps = new EventBussinessList();
//                        ps.setEventPartId(rs_prtDtl.getInt("EVENT_PART_ID"));;
//                        ps.setSeqNo(rs_prtDtl.getInt("SEQ_NO"));
//                        ps.setBisId(rs_prtDtl.getInt("BIS_ID"));
//                        ps.setBisTypeId(teb.getBisStruTypeId());
//                        ps.setBisTypeDesc(teb.getBisStruTypeName());
//                        ps.setUserId(rs_prtDtl.getString("USER_ID"));
//                        ps.setStatus(rs_prtDtl.getInt("STATUS"));
//                        eventBussList.add(ps);
//                    }
//
//                    ebt.setEventBussinessList(eventBussList);
//                    ebt.setEventPartId(teb.getEventPartId());;
//                    ebt.setEventId(teb.getEventId());
//                    ebt.setBisStruTypeId(teb.getBisStruTypeId());
//                    ebt.setBisStruTypeCount(teb.getBisStruTypeCount());
//                    ebt.setStatus(teb.getStatus());
//                    ebt.setBisStruTypeName(teb.getBisStruTypeName());
//                    eventbisTypeList.add(ebt);
//                }
//
//                em.setEventBisTypeList(eventbisTypeList);
//                eventList.add(em);
////--------------------------------------------------------------------------            
//            }
//            rs.close();
//
//        } catch (Exception ex) {
//            logger.info("Error getEventMasterDetails......:" + ex.toString());
//        }
//        MessageWrapper mw = new MessageWrapper();
//        mw.setTotalRecords(totalrecode);
//        mw.setData(eventList);
//        logger.info("Ready to Return Values............");
//        dbCon.ConectionClose(con);
//        logger.info("---------------------------------------------");
//        return mw;
//    }
    public static MessageWrapper getEventMasterDetails(int event_id) {
        logger.info("getEventMasterDetails method Call..........");
        ArrayList<EventMaster> eventList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String selectColumn = "EVENT_ID,EVENT_NAME,EVENT_DESC,"
                    + "TO_CHAR(START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(END_DATE,'YYYY/MM/DD') END_DATE,"
                    + "THRESHOLD,STATUS,"
                    + "EVENT_MGR_COMMENTS,MKT_MGR_COMMENTS";
            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM SD_EVENT_MASTER "
                    + "WHERE EVENT_ID = " + event_id + "");

            logger.info("Get Data to Result Set.............");

            ArrayList<EventMaster> temeventList = new ArrayList<>();

            while (rs.next()) {
                EventMaster ems = new EventMaster();
                ems.setEventId(rs.getInt("EVENT_ID"));;
                ems.setEventName(rs.getString("EVENT_NAME"));
                ems.setEventDesc(rs.getString("EVENT_DESC"));
                ems.setStartDate(rs.getString("START_DATE"));
                ems.setEndDate(rs.getString("END_DATE"));
                ems.setThreshold(rs.getString("THRESHOLD"));
                ems.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    ems.setStatusMsg("Planning");
                } else if (rs.getInt("STATUS") == 3) {
                    ems.setStatusMsg("Started");
                } else if (rs.getInt("STATUS") == 9) {
                    ems.setStatusMsg("Closed");
                } else {
                    ems.setStatusMsg("None");
                }
                ems.setEventMrgComment(rs.getString("EVENT_MGR_COMMENTS"));
                ems.setMktMrgComment(rs.getString("MKT_MGR_COMMENTS"));
                temeventList.add(ems);
            }
            rs.close();
//------------------------------------------------------------------------------                
            for (EventMaster emaster : temeventList) {

                EventMaster em = new EventMaster();
                em.setEventId(emaster.getEventId());;
                em.setEventName(emaster.getEventName());
                em.setEventDesc(emaster.getEventDesc());
                em.setStartDate(emaster.getStartDate());
                em.setEndDate(emaster.getEndDate());
                em.setThreshold(emaster.getThreshold());
                em.setStatus(emaster.getStatus());
                em.setStatusMsg(emaster.getStatusMsg());
                em.setEventMrgComment(emaster.getEventMrgComment());
                em.setMktMrgComment(emaster.getMktMrgComment());

                ArrayList<EventSalesRule> temeventSalesRuleList = new ArrayList<>();

                String salesRule = "E.RULE_ID,"
                        + "E.EVENT_ID,"
                        + "E.PRODUCT_FAMILY,"
                        + "E.BRAND,"
                        + "E.MODEL_NO,"
                        + "E.PERIOD,"
                        + "E.INTERVALS,"
                        + "E.ACCEPTED_INTERVALS,"
                        + "E.SALES_TYPE,"
                        + "E.POINTS,"
                        + "E.MIN_REQ,"
                        + "E.POINT_SCHEME_FLAG,"
                        + "E.TOT_ACHIEVEMENT,"
                        + "E.STATUS,"
                        + "M.MODEL_DESCRIPTION";

                ResultSet rs_salesRule = dbCon.search(con, "SELECT " + salesRule + " FROM SD_EVENT_SALE_RULES E,SD_MODEL_LISTS M "
                        + "WHERE E.MODEL_NO = M.MODEL_NO (+)  "
                        + "AND EVENT_ID = " + event_id + "");

                logger.info("Get Data to Result Set.............");

                while (rs_salesRule.next()) {
                    EventSalesRule esr = new EventSalesRule();
                    esr.setRuleId(rs_salesRule.getInt("RULE_ID"));;
                    esr.setEventId(rs_salesRule.getInt("EVENT_ID"));
                    esr.setProductFamily(rs_salesRule.getString("PRODUCT_FAMILY"));
                    esr.setBarnd(rs_salesRule.getString("BRAND"));
                    esr.setModleNo(rs_salesRule.getInt("MODEL_NO"));
                    esr.setPeriod(rs_salesRule.getInt("PERIOD"));
                    esr.setIntervals(rs_salesRule.getInt("INTERVALS"));
                    esr.setAcceptedInteval(rs_salesRule.getInt("ACCEPTED_INTERVALS"));
                    esr.setSalesType(rs_salesRule.getInt("SALES_TYPE"));
                    esr.setPoint(rs_salesRule.getDouble("POINTS"));
                    esr.setMinReq(rs_salesRule.getDouble("MIN_REQ"));
                    esr.setPointSchemeFlag(rs_salesRule.getInt("POINT_SCHEME_FLAG"));
                    esr.setTotalAchivement(rs_salesRule.getDouble("TOT_ACHIEVEMENT"));
                    esr.setStatus(rs_salesRule.getInt("STATUS"));
                    esr.setModleDesc(rs_salesRule.getString("MODEL_DESCRIPTION"));
                    temeventSalesRuleList.add(esr);
                }
                rs_salesRule.close();

                String selectPointSchema = "POINT_SCHEME_ID,"
                        + "RULE_ID,"
                        + "SALES_TYPE,"
                        + "NO_OF_UNITS,"
                        + "FIXED_POINTS,"
                        + "FLOAT_FLAG,"
                        + "ADDN_SALES_TYPE,"
                        + "ADDN_SALES_POINTS,"
                        + "ASSIGN_POINTS,"
                        + "STATUS";

                ResultSet rs_pointSchema = dbCon.search(con, "SELECT " + selectPointSchema + " FROM SD_EVENT_RULE_POINT_SCHEMES");
                ArrayList<EventRulePointScheme> tempointSchemaList = new ArrayList<>();

                while (rs_pointSchema.next()) {
                    EventRulePointScheme ps = new EventRulePointScheme();
                    ps.setPointSchemaId(rs_pointSchema.getInt("POINT_SCHEME_ID"));
                    ps.setSchmeruleId(rs_pointSchema.getInt("RULE_ID"));
                    ps.setSchemeSaleType(rs_pointSchema.getInt("SALES_TYPE"));
                    ps.setSchmeNoOfUnit(rs_pointSchema.getInt("NO_OF_UNITS"));
                    ps.setScheamfixedPoint(rs_pointSchema.getDouble("FIXED_POINTS"));
                    ps.setSchmefloatFlag(rs_pointSchema.getInt("FLOAT_FLAG"));
                    ps.setSchmeAddnSalesType(rs_pointSchema.getInt("ADDN_SALES_TYPE"));
                    ps.setSchmeAddnSalesPoint(rs_pointSchema.getDouble("ADDN_SALES_POINTS"));
                    ps.setSchmeAssignPoint(rs_pointSchema.getDouble("ASSIGN_POINTS"));
                    ps.setSchmestatus(rs_pointSchema.getInt("STATUS"));
                    tempointSchemaList.add(ps);
                }

                ArrayList<EventSalesRule> eventSalesRuleList = new ArrayList<>();
                double schemePoint = 0;
                int floatFlag = 0;
                double floatPoint = 0;
                int schemeSaleType = 0;
                int noOfUnit = 0;
                int addnSalesType = 0;
                double addnSalesPoint = 0;
                double assignPoint = 0;

                for (EventSalesRule saleRule : temeventSalesRuleList) {
                    ArrayList<EventRulePointScheme> pointSchemaList = new ArrayList<>();

                    for (EventRulePointScheme saleRuleSch : tempointSchemaList) {
                        if (saleRuleSch.getSchmeruleId() == saleRule.getRuleId()) {
                            EventRulePointScheme erps = new EventRulePointScheme();
                            erps.setPointSchemaId(saleRuleSch.getPointSchemaId());;
                            erps.setSchemeSaleType(saleRuleSch.getSchemeSaleType());
                            schemeSaleType = saleRuleSch.getSchemeSaleType();
                            erps.setSchmeNoOfUnit(saleRuleSch.getSchmeNoOfUnit());
                            noOfUnit = saleRuleSch.getSchmeNoOfUnit();
                            erps.setScheamfixedPoint(saleRuleSch.getScheamfixedPoint());
                            schemePoint = saleRuleSch.getScheamfixedPoint();
                            erps.setSchmefloatFlag(saleRuleSch.getSchmefloatFlag());
                            floatFlag = saleRuleSch.getSchmefloatFlag();
                            erps.setSchmeAddnSalesType(saleRuleSch.getSchmeAddnSalesType());
                            addnSalesType = saleRuleSch.getSchmeAddnSalesType();
                            erps.setSchmeAddnSalesPoint(saleRuleSch.getSchmeAddnSalesPoint());
                            floatPoint = saleRuleSch.getSchmeAddnSalesPoint();
                            erps.setSchmeAssignPoint(saleRuleSch.getSchmeAssignPoint());
                            assignPoint = saleRuleSch.getSchmeAssignPoint();
                            erps.setSchmestatus(saleRuleSch.getSchmestatus());
                            pointSchemaList.add(erps);
                        }
                    }
                    EventSalesRule esr = new EventSalesRule();
                    esr.setRuleId(saleRule.getRuleId());;
                    esr.setEventId(saleRule.getEventId());
                    esr.setProductFamily(saleRule.getProductFamily());
                    esr.setBarnd(saleRule.getBarnd());
                    esr.setModleNo(saleRule.getModleNo());
                    esr.setPeriod(saleRule.getPeriod());
                    esr.setIntervals(saleRule.getIntervals());
                    esr.setAcceptedInteval(saleRule.getAcceptedInteval());
                    esr.setSalesType(saleRule.getSalesType());
                    esr.setPoint(saleRule.getPoint());
                    esr.setMinReq(saleRule.getMinReq());
                    esr.setPointSchemeFlag(saleRule.getPointSchemeFlag());
                    esr.setTotalAchivement(saleRule.getTotalAchivement());
                    esr.setStatus(saleRule.getStatus());
                    esr.setModleDesc(saleRule.getModleDesc());
                    esr.setEventRulePointSchemaList(pointSchemaList);
                    esr.setScheamfixedPoint(schemePoint);
                    esr.setSchmefloatFlag(floatFlag);
                    if (floatFlag == 1) {
                        esr.setSchmefloagFlagString("Yes");
                    } else {
                        esr.setSchmefloagFlagString("No");
                    }
                    esr.setSchmefloatPoint(floatPoint);
                    esr.setSchemeSaleType(schemeSaleType);
                    esr.setSchmeNoOfUnit(noOfUnit);
                    esr.setSchmeAddnSalesType(addnSalesType);
                    esr.setSchmeAddnSalesPoint(addnSalesPoint);
                    esr.setSchmeAssignPoint(assignPoint);
                    eventSalesRuleList.add(esr);
                }

                em.setEventSalesRuleList(eventSalesRuleList);

                //--------------------------------------------------------------------------            
                ArrayList<EventNonSalesRule> nonSalesList = new ArrayList<>();

                String selectNonSales = "NS_RULE_ID,EVENT_ID,"
                        + "DESCRIPTION,TARGET,"
                        + "POINTS,SALE_TYPE,STATUS";

                ResultSet rs_nonSales = dbCon.search(con, "SELECT " + selectNonSales + " FROM SD_EVENT_NON_SALE_RULES "
                        + "WHERE EVENT_ID = " + event_id + "");

                while (rs_nonSales.next()) {
                    EventNonSalesRule ns = new EventNonSalesRule();
                    ns.setNonSalesRule(rs_nonSales.getInt("NS_RULE_ID"));;
                    ns.setEventId(rs_nonSales.getInt("EVENT_ID"));
                    ns.setDescription(rs_nonSales.getString("DESCRIPTION"));
                    ns.setTaget(rs_nonSales.getInt("TARGET"));
                    ns.setPoint(rs_nonSales.getInt("POINTS"));
                    ns.setSalesType(rs_nonSales.getInt("SALE_TYPE"));
                    ns.setStatus(rs_nonSales.getInt("STATUS"));
                    nonSalesList.add(ns);
                }
                rs_nonSales.close();
                em.setEventNonSalesRuleList(nonSalesList);
//------------------------------------------------------------------------------                
                ArrayList<EventAward> awardList = new ArrayList<>();

                String selectAward = "AWARD_ID,EVENT_ID,AWARD,PLACE,STATUS";

                ResultSet rs_award = dbCon.search(con, "SELECT " + selectAward + " FROM SD_EVENT_AWARDS "
                        + "WHERE EVENT_ID = " + event_id + "");

                while (rs_award.next()) {
                    EventAward ea = new EventAward();
                    ea.setAwardId(rs_award.getInt("AWARD_ID"));;
                    ea.setEventid(rs_award.getInt("EVENT_ID"));
                    ea.setAward(rs_award.getString("AWARD"));
                    ea.setPalce(rs_award.getString("PLACE"));
                    ea.setStatus(rs_award.getInt("STATUS"));
                    awardList.add(ea);
                }
                rs_award.close();
                em.setEvenAwardList(awardList);
                //--------------------------------------------------------------------------
                ArrayList<EventBisType> temEventbisTypeList = new ArrayList<>();
                String selectBisType = "E.EVENT_PART_ID,E.EVENT_ID,"
                        + "E.BIS_TYPE_ID,E.USER_COUNT,E.STATUS,B.BIS_STRU_TYPE_DESC";
                ResultSet rs_bisType = dbCon.search(con, "SELECT " + selectBisType + " FROM SD_EVENT_PARTICIPANTS E,SD_BIS_STRU_TYPES B "
                        + "WHERE E.BIS_TYPE_ID = B.BIS_STRU_TYPE_ID (+) "
                        + "AND E.EVENT_ID = " + event_id + "");

                logger.info("Get Data to Result Set.............");
                while (rs_bisType.next()) {
                    EventBisType ebt = new EventBisType();
                    ebt.setEventPartId(rs_bisType.getInt("EVENT_PART_ID"));
                    ebt.setEventId(rs_bisType.getInt("EVENT_ID"));
                    ebt.setBisStruTypeId(rs_bisType.getInt("BIS_TYPE_ID"));
                    ebt.setBisStruTypeCount(rs_bisType.getInt("USER_COUNT"));
                    ebt.setStatus(rs_bisType.getInt("STATUS"));
                    ebt.setBisStruTypeName(rs_bisType.getString("BIS_STRU_TYPE_DESC"));
                    temEventbisTypeList.add(ebt);
                }
                ArrayList<EventBussinessList> temeventBussList = new ArrayList<>();

                String selectPartDtl = "EVENT_PART_ID,SEQ_NO,BIS_ID,USER_ID,STATUS";

                ResultSet rs_prtDtl = dbCon.search(con, "SELECT " + selectPartDtl + " FROM SD_EVENT_PARTICIPANT_DTL");
                while (rs_prtDtl.next()) {
                    EventBussinessList ps = new EventBussinessList();
                    ps.setEventPartId(rs_prtDtl.getInt("EVENT_PART_ID"));;
                    ps.setSeqNo(rs_prtDtl.getInt("SEQ_NO"));
                    ps.setBisId(rs_prtDtl.getInt("BIS_ID"));
                    ps.setUserId(rs_prtDtl.getString("USER_ID"));
                    ps.setStatus(rs_prtDtl.getInt("STATUS"));
                    temeventBussList.add(ps);
                }

                ArrayList<EventBisType> eventbisTypeList = new ArrayList<>();

                for (EventBisType tebt : temEventbisTypeList) {
                    EventBisType ebt = new EventBisType();
                    ArrayList<EventBussinessList> eventBussList = new ArrayList<>();

                    for (EventBussinessList eb : temeventBussList) {
                        if (eb.getEventPartId() == tebt.getEventPartId()) {
                            EventBussinessList ps = new EventBussinessList();
                            ps.setEventPartId(eb.getEventPartId());;
                            ps.setSeqNo(eb.getSeqNo());
                            ps.setBisId(eb.getBisId());
                            ps.setBisTypeId(tebt.getBisStruTypeId());
                            ps.setBisTypeDesc(tebt.getBisStruTypeName());
                            ps.setUserId(eb.getUserId());
                            ps.setStatus(eb.getStatus());
                            eventBussList.add(ps);
                        }

                    }

                    ebt.setEventBussinessList(eventBussList);
                    ebt.setEventPartId(tebt.getEventPartId());;
                    ebt.setEventId(tebt.getEventId());
                    ebt.setBisStruTypeId(tebt.getBisStruTypeId());
                    ebt.setBisStruTypeCount(tebt.getBisStruTypeCount());
                    ebt.setStatus(tebt.getStatus());
                    ebt.setBisStruTypeName(tebt.getBisStruTypeName());
                    eventbisTypeList.add(ebt);

                }
                em.setEventBisTypeList(eventbisTypeList);
                eventList.add(em);
            }
//------------------------------------------------------------------------------            
        } catch (Exception ex) {
            logger.info("Error getEventMasterDetails......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = eventList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getSalesType() {

        logger.info("getSalesType Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'EVENT_SALE_TYPE'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error in getSalesType Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getEventPeriod() {

        logger.info("getEventPeriod Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "CODE,DESCRIPTION";

            String whereClous = "DOMAIN_NAME = 'EVENT_PERIOD'";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MDECODE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (ORDER BY CODE ASC) RN "
                    + " FROM SD_MDECODE WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                totalrecode = rs.getInt("CNT");
                de.setCode(rs.getInt("CODE"));
                de.setDescription(rs.getString("DESCRIPTION"));
                mdeCodeList.add(de);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error in getEventPeriod Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getProductFamily() {

        logger.info("getProductFamily Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "DISTINCT PART_PRD_FAMILY_DESC";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " "
                    + " FROM SD_MODEL_LISTS");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                de.setDescription(rs.getString("PART_PRD_FAMILY_DESC"));
                mdeCodeList.add(de);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error in getProductFamily Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = mdeCodeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getBrandList() {

        logger.info("getBrandList Method Call....................");
        ArrayList<MdeCode> mdeCodeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "DISTINCT PART_PRD_CODE_DESC";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " "
                    + " FROM SD_MODEL_LISTS");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                MdeCode de = new MdeCode();
                de.setDescription(rs.getString("PART_PRD_CODE_DESC"));
                mdeCodeList.add(de);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error in getBrandList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = mdeCodeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(mdeCodeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper startEvent(EventMaster eventMasterInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("startEvent Method Call............");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        try {
            int event_buss = 0;
            int paticipant_cnt = 0;
            int sale_rule_cnt = 0;
            ResultSet rs_buss_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_EVENT_PARTICIPANTS "
                    + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");
            while (rs_buss_cnt.next()) {
                event_buss = rs_buss_cnt.getInt("CNT");
            }

            if (event_buss > 0) {

                ResultSet rs_paticipant_cnt = dbCon.search(con, "SELECT COUNT(*) CNT "
                        + "FROM SD_EVENT_PARTICIPANT_DTL "
                        + "WHERE EVENT_PART_ID IN (SELECT EVENT_PART_ID "
                        + "FROM SD_EVENT_PARTICIPANTS "
                        + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + ")");

                while (rs_paticipant_cnt.next()) {
                    paticipant_cnt = rs_paticipant_cnt.getInt("CNT");
                }

                if (paticipant_cnt > 0) {

                    ResultSet rs_event_sales = dbCon.search(con, "SELECT COUNT(*) CNT "
                            + "FROM SD_EVENT_SALE_RULES "
                            + "WHERE EVENT_ID = " + eventMasterInfo.getEventId() + "");
                    while (rs_event_sales.next()) {
                        sale_rule_cnt = rs_event_sales.getInt("CNT");
                    }

                    if (sale_rule_cnt > 0) {
                        PreparedStatement ps_up_event_master = dbCon.prepare(con, "UPDATE SD_EVENT_MASTER "
                                + "SET STATUS =  3 "
                                + "WHERE EVENT_ID = ?");

                        ps_up_event_master.setInt(1, eventMasterInfo.getEventId());
                        ps_up_event_master.executeUpdate();
                        result = 1;
                    } else {
                        result  = 4;
                    }

                } else {
                    result = 3;
                }

            } else {
                result = 2;
            }

        } catch (Exception ex) {
            logger.info("Error in startEvent Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Event Start Successfully");
            logger.info("Event Start Successfully............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Participant Type List Empty in this Event");
            logger.info("Participant Type List Empty in this Event..........");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Participant Details List Empty in this Event");
            logger.info("Participant Details List Empty in this Event..........");
        }else if (result == 4) {
            vw.setReturnFlag("4");
            vw.setReturnMsg("Sales Rule List Empty in this Event");
            logger.info("Sales Rule List Empty in this Event..........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Event Start");
            logger.info("Error in Event Start..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

}
