/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.DeviceIssue;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.entities.ReplasementReturn;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class ReturnReplacementController {

    public static MessageWrapper getReturnReplacement(int replretutnno,
            String exgrefno,
            int distributerid,
            String imeino,
            String modle_desc,
            String returnnate,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getReturnReplacement Method Call...........");

        ArrayList<ReplasementReturn> replaceReturnList = new ArrayList<>();
        int totalrecode = 0;
        try {

            order = DBMapper.returnReplacement(order);

            String replRetutnNo = ((replretutnno == 0) ? "" : "AND R.REPL_RETURN_NO = '" + replretutnno + "' ");
            String exgRefNo = (exgrefno.equalsIgnoreCase("all") ? "NVL(R.EXG_REF_NO,'AA') = NVL(R.EXG_REF_NO,'AA') " : "UPPER(R.EXG_REF_NO) LIKE UPPER('%" + exgrefno + "%')");
            String distributerId = ((distributerid == 0) ? "" : "AND R.DISTRIBUTER_ID = '" + distributerid + "' ");
            String imeiNo = (imeino.equalsIgnoreCase("all") ? "NVL(R.IMEI_NO,'AA') = NVL(R.IMEI_NO,'AA') " : "UPPER(R.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String modelDesc = (modle_desc.equalsIgnoreCase("all") ? "NVL(L.MODEL_DESCRIPTION,'AA') = NVL(L.MODEL_DESCRIPTION,'AA') " : "UPPER(L.MODEL_DESCRIPTION) LIKE UPPER('%" + modle_desc + "%')");
            String returnDate = (returnnate.equalsIgnoreCase("all") ? "NVL(R.RETURN_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(R.RETURN_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_DATE(TO_CHAR(R.RETURN_DATE,'YYYY/MM/DD'),'YYYY/MM/DD') = TO_DATE('" + returnnate + "','YYYY/MM/DD')");
            String Status = (status == 0) ? " AND ( R.STATUS =1 OR R.STATUS =3 ) " : "AND R.STATUS = '" + status + "' ";
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY R.REPL_RETURN_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "R.REPL_RETURN_NO,"
                    + "R.EXG_REF_NO,"
                    + "R.DISTRIBUTER_ID,"
                    + "R.IMEI_NO,"
                    + "TO_CHAR(R.RETURN_DATE,'YYYY/MM/DD') RETURN_DATE,"
                    + "R.STATUS,"
                    + "L.MODEL_DESCRIPTION";

            String whereClous = "R.IMEI_NO = M.IMEI_NO (+) "
                    + " AND M.MODEL_NO = L.MODEL_NO (+)"
                    + " AND " + exgRefNo + " "
                    + "" + replRetutnNo + " "
                    + "" + distributerId + " "
                    + "AND " + imeiNo + " "
                    + "AND " + modelDesc + " "
                    + "AND " + returnDate + " "
                    + "" + Status + " "
                    + "AND R.STATUS != 9";

            String tables = "SD_REPLACEMEN_RETURN R,SD_DEVICE_MASTER M,SD_MODEL_LISTS L";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + "+1 AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                ReplasementReturn rr = new ReplasementReturn();
                totalrecode = rs.getInt("CNT");
                rr.setReplRetutnNo(rs.getInt("REPL_RETURN_NO"));
                rr.setExchangeReferenceNo(rs.getString("EXG_REF_NO"));
                rr.setDistributerId(rs.getInt("DISTRIBUTER_ID"));
                rr.setImeiNo(rs.getString("IMEI_NO"));
                rr.setReturnDate(rs.getString("RETURN_DATE"));
                rr.setStatus(rs.getInt("STATUS"));
                rr.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                if(rs.getInt("STATUS") == 1){
                   rr.setStatusMsg("Pending");
                }else if (rs.getInt("STATUS") == 3){
                    rr.setStatusMsg("Approve");
                }else if (rs.getInt("STATUS") == 9){
                    rr.setStatusMsg("Cancle");
                }else{
                    rr.setStatusMsg("None");
                }
                replaceReturnList.add(rr);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error in getReturnReplacement  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(replaceReturnList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper addReplasementReturn(ReplasementReturn replacereturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addReplasementReturn Method Call.............................");
        int result = 0;
        int replace_ref_cnt = 0;
        int next_retr_no = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_REPLACEMEN_RETURN "
                    + "WHERE EXG_REF_NO= '" + replacereturn_info.getExchangeReferenceNo() + "' "
                    + "AND STATUS = 1");

            while (rs_cnt.next()) {
                replace_ref_cnt = rs_cnt.getInt("CNT");
            }

            ResultSet rs_max_ref = dbCon.search(con, "SELECT NVL(MAX(REPL_RETURN_NO),0) MAX FROM SD_REPLACEMEN_RETURN");

            while (rs_max_ref.next()) {
                next_retr_no = rs_max_ref.getInt("MAX");
            }
            next_retr_no = next_retr_no + 1;
            if (replace_ref_cnt > 0) {
                result = 2;
            } else {

                String insertColumns = "REPL_RETURN_NO,"
                        + "EXG_REF_NO,"
                        + "DISTRIBUTER_ID,"
                        + "IMEI_NO,"
                        + "RETURN_DATE,"
                        + "STATUS,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED";

                String insertValue = "?,?,?,?,SYSDATE,1,?,SYSDATE";

                PreparedStatement ps_in_return = dbCon.prepare(con, "INSERT INTO SD_REPLACEMEN_RETURN(" + insertColumns + ") VALUES(" + insertValue + ")");

                PreparedStatement ps_up_return = dbCon.prepare(con, "UPDATE SD_DEVICE_EXCHANGE SET STATUS = 3 WHERE REFERENCE_NO = ?");

                try {
                    ps_in_return.setInt(1, next_retr_no);
                    ps_in_return.setString(2, replacereturn_info.getExchangeReferenceNo());
                    ps_in_return.setInt(3, replacereturn_info.getDistributerId());
                    ps_in_return.setString(4, replacereturn_info.getImeiNo());
                    ps_in_return.setString(5, user_id);
                    ps_in_return.executeUpdate();
                    logger.info("Replacement Details Inseted.....");

                    ps_up_return.setString(1, replacereturn_info.getExchangeReferenceNo());
                    ps_up_return.executeUpdate();

                    result = 1;

                } catch (Exception e) {
                    logger.info("Error Replacement Details Insert....." + e.toString());
                    result = 9;
                }

            }

        } catch (Exception ex) {
            logger.info("Error addReplasementReturn Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Replacement Insert Successfully");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Exchange Reference No Already Exist");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Replacement Insert Fail");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editReplasementReturn(ReplasementReturn replacereturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editReplasementReturn Method Call............");
        int result = 0;
        int replace_ref_cnt = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs_cnt = dbCon.search(con, "SELECT COUNT(*) CNT FROM SD_REPLACEMEN_RETURN "
                    + " WHERE EXG_REF_NO= '" + replacereturn_info.getExchangeReferenceNo() + "' "
                    + " AND REPL_RETURN_NO != " + replacereturn_info.getReplRetutnNo() + " "
                    + " AND STATUS = 1");

            while (rs_cnt.next()) {
                replace_ref_cnt = rs_cnt.getInt("CNT");
            }

            if (replace_ref_cnt > 0) {

                result = 2;
            } else {

                try {
                    PreparedStatement ps_up_rern = dbCon.prepare(con, "UPDATE SD_REPLACEMEN_RETURN "
                            + "SET EXG_REF_NO=?,"
                            + "IMEI_NO=?,"
                            + "RETURN_DATE=TO_DATE(?,'YYYY/MM/DD'),"
                            + "USER_MODIFIED=?,"
                            + "DATE_MODIFIED=SYSDATE "
                            + "WHERE REPL_RETURN_NO=?");

                    ps_up_rern.setString(1, replacereturn_info.getExchangeReferenceNo());
                    ps_up_rern.setString(2, replacereturn_info.getImeiNo());
                    ps_up_rern.setString(3, replacereturn_info.getReturnDate());
                    ps_up_rern.setString(4, user_id);
                    ps_up_rern.setInt(5, replacereturn_info.getReplRetutnNo());
                    ps_up_rern.executeUpdate();

                    result = 1;

                } catch (Exception e) {
                    logger.info("Error in Updating.......");
                    result = 9;
                }

            }

        } catch (Exception ex) {
            logger.info("Error editReplasementReturn Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Update Successfully in Replacement");
            logger.info("Update Successsfully in Replacement.............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Exchange Reference No Already Exist");
            logger.info("Exchange Reference No Already Exist............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Update Replacement");
            logger.info("Getting Error Update Replacement............");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper approveReplasementReturn(ReplasementReturn replacereturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("approveReplasementReturn Method Call............");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_rern = dbCon.prepare(con, "UPDATE SD_REPLACEMEN_RETURN "
                    + "SET STATUS=?,"
                    + "USER_MODIFIED=?,"
                    + "DATE_MODIFIED=SYSDATE "
                    + "WHERE REPL_RETURN_NO=?");

            ps_up_rern.setInt(1, 3);
            ps_up_rern.setString(2, user_id);
            ps_up_rern.setInt(3, replacereturn_info.getReplRetutnNo());
            ps_up_rern.executeUpdate();
            
            
            PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                    + "SET BIS_ID = ?,"
                    + "USER_MODIFIED = ?,"
                    + "DATE_MODIFIED = SYSDATE,"
                    + "BAD_ITEM_FLAG = ?  "
                    + "WHERE IMEI_NO = ?");

            ps_up_master.setInt(1, replacereturn_info.getDistributerId());
            ps_up_master.setString(2, user_id);
            ps_up_master.setInt(3, 1);
            ps_up_master.setString(4, replacereturn_info.getImeiNo());
            ps_up_master.executeUpdate();

            result = 1;

        } catch (Exception ex) {
            logger.info("Error approveReplasementReturn Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Approve Successfully in Replacement");
            logger.info("Approve Successsfully in Replacement.............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Approve Replacement");
            logger.info("Getting Error Approve Replacement............");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper deleteReplasementReturn(ReplasementReturn replacereturn_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("deleteReplasementReturn Method Call............");
        int result = 0;
        try {

            try {
                dbCon.save(con, "UPDATE SD_REPLACEMEN_RETURN SET STATUS=9,"
                        + "USER_MODIFIED='" + user_id + "',"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE REPL_RETURN_NO=" + replacereturn_info.getReplRetutnNo() + "");

                result = 1;

            } catch (Exception e) {
                logger.info("Error in change status.......");
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error in deleteReplasementReturn Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Delete Successfully in Replacement");
            logger.info("Delete Successsfully in Replacement............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Delete Replacement");
            logger.info("Getting Error Delete Replacement..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

}
