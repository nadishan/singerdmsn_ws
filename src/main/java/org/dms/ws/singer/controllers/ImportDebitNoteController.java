/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.ImportDebitNote;
import org.dms.ws.singer.entities.ImportDebitNoteDetails;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class ImportDebitNoteController {

    public static MessageWrapper getImportDebitNote(int bisId,
            String debitnoteno,
            String orderno,
            String invoiceno,
            String contactno,
            String partno,
            int orderqty,
            int deleveryqty,
            String deleverydate,
            int headntatus,
            String customerno,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getImportDebitNote Method Call....................");
        ArrayList<ImportDebitNote> imporDebitList = new ArrayList<>();
        int totalrecode = 0;
      DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.importdebitNote(order);

            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(DH.DEBIT_NOTE_NO,'AA') = NVL(DH.DEBIT_NOTE_NO,'AA')" : "UPPER(DH.DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String orderNo = (orderno.equalsIgnoreCase("all") ? "NVL(DH.ORDER_NO,'AA') = NVL(DH.ORDER_NO,'AA')" : "UPPER(DH.ORDER_NO) LIKE UPPER('%" + orderno + "%')");
            String invoiceNo = (invoiceno.equalsIgnoreCase("all") ? "NVL(DH.INVOICE_NO,'AA') = NVL(DH.INVOICE_NO,'AA')" : "UPPER(DH.INVOICE_NO) LIKE UPPER('%" + invoiceno + "%')");
            String contactNo = (contactno.equalsIgnoreCase("all") ? "NVL(DH.CONTRACT_NO,'AA') = NVL(DH.CONTRACT_NO,'AA')" : "UPPER(DH.CONTRACT_NO) LIKE UPPER('%" + contactno + "%')");
            String partNo = (partno.equalsIgnoreCase("all") ? "NVL(DH.PART_NO,'AA') = NVL(DH.PART_NO,'AA')" : "UPPER(DH.PART_NO) LIKE UPPER('%" + partno + "%')");
            String orderQty = (orderqty == 0 ? "" : "AND DH.ORDER_QTY = " + orderqty + "");
            String deleveryQty = (deleveryqty == 0 ? "" : "AND DH.DELIVERED_QTY = " + deleveryqty + "");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(DH.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(DH.DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "DH.DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String headStatus = (headntatus == 0 ? "" : "AND DH.HEAD_STATUS = " + headntatus + " ");
            String customerNo = (customerno.equalsIgnoreCase("all") ? "NVL(DH.CUSTOMER_NO,'AA') = NVL(DH.CUSTOMER_NO,'AA')" : "UPPER(DH.CUSTOMER_NO) LIKE UPPER('%" + customerno + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY DH.DEBIT_NOTE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "DISTINCT DH.DEBIT_NOTE_NO,DH.ORDER_NO,"
                    + "DH.INVOICE_NO,DH.CONTRACT_NO,"
                    + "DH.PART_NO,DH.ORDER_QTY,"
                    + "DH.DELIVERED_QTY,TO_CHAR(DH.DELIVERY_DATE,'YYYY/MM/DD') DELIVERY_DATE,"
                    + "DH.HEAD_STATUS,DH.CUSTOMER_NO,"
                    + "DH.SITE_DESCRIPTION,DH.STATUS";

            String whereClous = " " + debitNoteNo + " "
                    + " AND " + orderNo + " "
                    + " AND " + invoiceNo + " "
                    + " AND " + contactNo + " "
                    + " AND " + partNo + " "
                    + " " + orderQty + " "
                    + " " + deleveryQty + " "
                    + " AND " + deleveryDate + " "
                    + " " + headStatus + " "
                    + " AND " + customerNo + " "
                    + " AND     DH.DEBIT_NOTE_NO IN (SELECT DISTINCT DEBIT_NOTE_NO FROM SD_DEBIT_NOTE_DETAIL WHERE BIS_ID IN (SELECT BIS_ID FROM SD_BUSINESS_STRUCTURES START WITH  BIS_ID = "+bisId+" CONNECT BY PRIOR  BIS_ID = PARENT_BIS_ID))";

//            
//            String qry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_DEBIT_NOTE_HEADER DH, SD_DEBIT_NOTE_DETAIL DD WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
//                    + " FROM SD_DEBIT_NOTE_HEADER DH, SD_DEBIT_NOTE_DETAIL DD WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";
//            
            
            String qry = "  SELECT *    "
                    + " FROM (  "
                    + " SELECT  "+seletColunm
                    + " , (SELECT COUNT(*) CNT  "
                    + " FROM (  "
                    + " SELECT  "+seletColunm
                    +"   FROM    SD_DEBIT_NOTE_HEADER DH     "
                    + " WHERE   "+whereClous
                    +"   )) AS CNT, "
                    + " ROW_NUMBER()    "
                    + " OVER (ORDER BY DH.DEBIT_NOTE_NO DESC) RN "
                    + " FROM    SD_DEBIT_NOTE_HEADER DH     "
                    + " WHERE   "+whereClous
                    + " )   "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ";
            
          //  System.out.println("Qry: "+qry);
            ResultSet rs = dbCon.search(con,qry);

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                ImportDebitNote idn = new ImportDebitNote();
                totalrecode = rs.getInt("CNT");
                idn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                idn.setOrderNo(rs.getString("ORDER_NO"));
                idn.setInvoiceNo(rs.getString("INVOICE_NO"));
                idn.setContactNo(rs.getString("CONTRACT_NO"));
                idn.setPartNo(rs.getString("PART_NO"));
                idn.setOrderQty(rs.getInt("ORDER_QTY"));
                idn.setDeleveryQty(rs.getInt("DELIVERED_QTY"));
                idn.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                idn.setHeadStatus(rs.getInt("HEAD_STATUS"));
                idn.setCustomerNo(rs.getString("CUSTOMER_NO"));
                idn.setSiteDescription(rs.getString("SITE_DESCRIPTION"));
                idn.setStatus(rs.getInt("STATUS"));
                imporDebitList.add(idn);
            }

        } catch (Exception ex) {
            logger.info("Error getImportDebitNote Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imporDebitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getImportDebitNoteDetial(String debitnoteno,
            String imeino,
            String customercode,
            String shopcode,
            int modleno,
            int bisid,
            String orderno,
            double salerprice,
            String dateaccepted,
            int status,
            String remark,
            String site,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getImportDebitNoteDetial Method Call....................");
        ArrayList<ImportDebitNoteDetails> imporDebitList = new ArrayList<>();
        int totalrecode = 0;
      DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.importdebitNoteDetails(order);

            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(D.DEBIT_NOTE_NO,'AA') = NVL(D.DEBIT_NOTE_NO,'AA')" : "UPPER(D.DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String imeiNo = (imeino.equalsIgnoreCase("all") ? "NVL(D.IMEI_NO,'AA') = NVL(D.IMEI_NO,'AA')" : "UPPER(D.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String customerCode = (customercode.equalsIgnoreCase("all") ? "NVL(D.CUSTOMER_CODE,'AA') = NVL(D.CUSTOMER_CODE,'AA')" : "UPPER(D.CUSTOMER_CODE) LIKE UPPER('%" + customercode + "%')");
            String shopCode = (shopcode.equalsIgnoreCase("all") ? "NVL(D.SHOP_CODE,'AA') = NVL(D.SHOP_CODE,'AA')" : "UPPER(D.SHOP_CODE) LIKE UPPER('%" + shopcode + "%')");
            String modleNo = (modleno == 0 ? "" : "AND D.MODEL_NO = '" + modleno + "'");
            String bisId = (bisid == 0 ? "" : "AND D.BIS_ID = '" + bisid + "' ");
            String orderNo = (orderno.equalsIgnoreCase("all") ? "NVL(D.ORDER_NO,'AA') = NVL(D.ORDER_NO,'AA')" : "UPPER(D.ORDER_NO) LIKE UPPER('%" + orderno + "%')");
            String salerPrice = (salerprice == 0 ? "" : "AND D.SALES_PRICE = '" + salerprice + "' ");
            String dateAccepted = (dateaccepted.equalsIgnoreCase("all") ? "NVL(D.DATE_ACCEPTED,TO_DATE('20100101','YYYYMMDD')) = NVL(D.DATE_ACCEPTED,TO_DATE('20100101','YYYYMMDD'))" : "D.DATE_ACCEPTED = TO_DATE('" + dateaccepted + "','YYYY/MM/DD')");
            String statuS = (status == 0 ? "" : "AND D.STATUS = '" + status + "' ");
            String remarks = (site.equalsIgnoreCase("all") ? "NVL(D.REMARKS,'AA') = NVL(D.REMARKS,'AA')" : "UPPER(D.REMARKS) LIKE UPPER('%" + site + "%')");
     //       String Site = (site.equalsIgnoreCase("all") ? "" : "AND UPPER((SELECT SITE_DESCRIPTION FROM SD_DEBIT_NOTE_HEADER WHERE DEBIT_NOTE_NO = B.DEBIT_NOTE_NO)) LIKE UPPER('%" + site + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.DEBIT_NOTE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "D.DEBIT_NOTE_NO,D.IMEI_NO,"
                    + "D.CUSTOMER_CODE,D.SHOP_CODE,"
                    + "D.MODEL_NO,D.BIS_ID,"
                    + "D.ORDER_NO,D.SALES_PRICE,"
                    + "TO_CHAR(D.DATE_ACCEPTED,'YYYY/MM/DD') DATE_ACCEPTED,D.STATUS,"
                    + "D.REMARKS,"
                    + "M.MODEL_DESCRIPTION,"
                    + "D.SITE";

            String whereClous = "D.MODEL_NO = M.MODEL_NO "
                    + " AND " + debitNoteNo + " "
                    + " AND " + imeiNo + " "
                    + " AND " + customerCode + " "
                    + " AND " + shopCode + " "
                    + " " + modleNo + "  "
                    + " " + bisId + " "
                    + " AND " + orderNo + " "
                    + " " + salerPrice + " "
                    + " AND " + dateAccepted + " "
                    + " " + statuS + " "
                    + " AND " + remarks + "  ";
            
            String table = "SD_DEBIT_NOTE_DETAIL D,SD_MODEL_LISTS M";
            

            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM "+table+" WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM "+table+" WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                ImportDebitNoteDetails idn = new ImportDebitNoteDetails();
                totalrecode = rs.getInt("CNT");
                idn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                idn.setImeiNo(rs.getString("IMEI_NO"));
                idn.setCustomerCode(rs.getString("CUSTOMER_CODE"));
                idn.setShopCode(rs.getString("SHOP_CODE"));
                idn.setModleNo(rs.getInt("MODEL_NO"));
                idn.setBisId(rs.getInt("BIS_ID"));
                idn.setOrderNo(rs.getString("ORDER_NO"));
                idn.setSalerPrice(rs.getDouble("SALES_PRICE"));
                idn.setDateAccepted(rs.getString("DATE_ACCEPTED"));
                idn.setStatus(rs.getInt("STATUS"));
                idn.setRemarks(rs.getString("REMARKS"));
                idn.setSite(rs.getString("REMARKS"));
                idn.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                idn.setSite(rs.getString("SITE"));
                imporDebitList.add(idn);
            }

        } catch (Exception ex) {
            logger.info("Error getImportDebitNote Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imporDebitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addImportDebitNote(ImportDebitNote importdebit_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addImportDebitNote Method Call.............................");
        int result = 1;
              DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String insertColumns = "DEBIT_NOTE_NO,"
                    + "ORDER_NO,INVOICE_NO,"
                    + "CONTRACT_NO,PART_NO,"
                    + "ORDER_QTY,DELIVERED_QTY,"
                    + "DELIVERY_DATE,HEAD_STATUS,"
                    + "CUSTOMER_NO,SITE_DESCRIPTION,"
                    + "STATUS,USER_INSERTED,"
                    + "DATE_INSERTED";

            String insertValue = "'" + importdebit_info.getDebitNoteNo() + "',"
                    + " '" + importdebit_info.getOrderNo() + "',"
                    + " '" + importdebit_info.getInvoiceNo() + "',"
                    + " '" + importdebit_info.getContactNo() + "',"
                    + " '" + importdebit_info.getPartNo() + "',"
                    + " " + importdebit_info.getOrderQty() + ","
                    + " " + importdebit_info.getDeleveryQty() + ","
                    + " TO_DATE('" + importdebit_info.getDeleveryDate() + "','YYYY/MM/DD'),"
                    + "" + importdebit_info.getHeadStatus() + ","
                    + "'" + importdebit_info.getCustomerNo() + "',"
                    + "'" + importdebit_info.getSiteDescription() + "',"
                    + "" + importdebit_info.getStatus() + ","
                    + "'" + user_id + "',"
                    + "SYSDATE";

            try {

                dbCon.save(con,"INSERT INTO SD_DEBIT_NOTE_HEADER(" + insertColumns + ") "
                        + "VALUES(" + insertValue + ")");
                logger.info("Accept Debit Note Header Details Inserted.....");
            } catch (Exception e) {
                logger.info("Error Accept Debit Note Header Details Insert....." + e.toString());
                result = 9;
            }

            String accetDebitDetailsColumn = "DEBIT_NOTE_NO,"
                    + "IMEI_NO,CUSTOMER_CODE,"
                    + "SHOP_CODE,MODEL_NO,"
                    + "BIS_ID,ORDER_NO,"
                    + "SALES_PRICE,DATE_ACCEPTED,"
                    + "STATUS,REMARKS,"
                    + "USER_INSERTED,DATE_INSERTED";

            ArrayList<ImportDebitNoteDetails> imDeNoList = new ArrayList<ImportDebitNoteDetails>();
            if (importdebit_info.getImportDebitList() != null) {
                try {

                    imDeNoList = importdebit_info.getImportDebitList();
                    
                    String imDeNoValue = "?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),"
                                + "?,?,?,SYSDATE";

                    PreparedStatement ps_in_debit = dbCon.prepare(con, "INSERT INTO SD_DEBIT_NOTE_DETAIL(" + accetDebitDetailsColumn + ") "
                                + "VALUES(" + imDeNoValue + ")");
                    
                    
                    for (ImportDebitNoteDetails di : imDeNoList) {

                        ps_in_debit.setString(1, di.getDebitNoteNo());
                        ps_in_debit.setString(2, di.getImeiNo());
                        ps_in_debit.setString(3, di.getCustomerCode());
                        ps_in_debit.setString(4, di.getShopCode());
                        ps_in_debit.setInt(5, di.getModleNo());
                        ps_in_debit.setInt(6, di.getBisId());
                        ps_in_debit.setString(7, di.getOrderNo());
                        ps_in_debit.setDouble(8, di.getSalerPrice());
                        ps_in_debit.setString(9, di.getDateAccepted());
                        ps_in_debit.setInt(10, di.getStatus());
                        ps_in_debit.setString(11, di.getRemarks());
                        ps_in_debit.setString(12, user_id);
                        ps_in_debit.addBatch();
                        

                        logger.info("Import Debit Note IMEI Inserted........");
                    }
                    ps_in_debit.executeBatch();
                    
                } catch (Exception e) {
                    logger.info("Error in Accept Debit Note IMEI Insert....." + e.toString());
                    result = 9;
                }
            }
        } catch (Exception ex) {
            logger.info("Error addImportDebitNote Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result
                == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Accept Debit Note Insert Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Accept Debit Note Insert Fail");
        }
        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static MessageWrapper getAcceptInventory(String debitnoteno,
            String deleverydate,
            int status,
            String fromdate,
            String todate,
            String order,
            String type,
            int start,
            int limit) {

        logger.info("getAcceptInventory Method Call....................");
        ArrayList<ImportDebitNote> imporDebitList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.importdebitNote(order);

            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(DEBIT_NOTE_NO,'AA') = NVL(DEBIT_NOTE_NO,'AA')" : "UPPER(DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String deleveryDate = (deleverydate.equalsIgnoreCase("all") ? "NVL(DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(DELIVERY_DATE,TO_DATE('20100101','YYYYMMDD'))" : "DELIVERY_DATE = TO_DATE('" + deleverydate + "','YYYY/MM/DD')");
            String Status = (status == 0 ? "" : "AND STATUS = '" + status + "' ");
            String Range = fromdate.equalsIgnoreCase("all") ? "" : "AND DATE_INSERTED BETWEEN (TO_DATE('" + fromdate + "','YYYY/MM/DD')) AND (TO_DATE('" + todate + "','YYYY/MM/DD')) ";
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY DEBIT_NOTE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "DEBIT_NOTE_NO,ORDER_NO,"
                    + "INVOICE_NO,CONTRACT_NO,"
                    + "PART_NO,ORDER_QTY,"
                    + "DELIVERED_QTY,DELIVERY_DATE,"
                    + "HEAD_STATUS,CUSTOMER_NO,"
                    + "SITE_DESCRIPTION,STATUS";

            String whereClous = " " + debitNoteNo + " "
                    + " AND " + deleveryDate + " "
                    + " " + Status + " "
                    + " " + Range + " ";

            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_DEBIT_NOTE_HEADER WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM SD_DEBIT_NOTE_HEADER WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                ImportDebitNote idn = new ImportDebitNote();
                totalrecode = rs.getInt("CNT");
                idn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                idn.setOrderNo(rs.getString("ORDER_NO"));
                idn.setInvoiceNo(rs.getString("INVOICE_NO"));
                idn.setContactNo(rs.getString("CONTRACT_NO"));
                idn.setPartNo(rs.getString("PART_NO"));
                idn.setOrderQty(rs.getInt("ORDER_QTY"));
                idn.setDeleveryQty(rs.getInt("DELIVERED_QTY"));
                idn.setDeleveryDate(rs.getString("DELIVERY_DATE"));
                idn.setHeadStatus(rs.getInt("HEAD_STATUS"));
                idn.setCustomerNo(rs.getString("CUSTOMER_NO"));
                idn.setSiteDescription(rs.getString("SITE_DESCRIPTION"));
                idn.setStatus(rs.getInt("STATUS"));
                imporDebitList.add(idn);
            }

        } catch (Exception ex) {
            logger.info("Error getAcceptInventory Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imporDebitList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper editImportDebitNote(ImportDebitNote importdebit_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editmportDebitNote Method Call.............................");
        int result = 1;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            try {

                dbCon.save(con,"UPDATE SD_DEBIT_NOTE_HEADER "
                        + "SET STATUS = " + importdebit_info.getStatus() + ","
                        + " USER_MODIFIED = '" + user_id + "',"
                        + " DATE_MODIFIED = SYSDATE "
                        + " WHERE DEBIT_NOTE_NO = '" + importdebit_info.getDebitNoteNo() + "'");

                logger.info("Accept Debit Note Header Details Inserted.....");

            } catch (Exception e) {
                logger.info("Error Accept Debit Note Header Details Insert....." + e.toString());
                result = 9;
            }

            String accetDebitDetailsColumn = "DEBIT_NOTE_NO,"
                    + "IMEI_NO,CUSTOMER_CODE,"
                    + "SHOP_CODE,MODEL_NO,"
                    + "BIS_ID,ORDER_NO,"
                    + "SALES_PRICE,DATE_ACCEPTED,"
                    + "STATUS,REMARKS,"
                    + "USER_INSERTED,DATE_INSERTED";

            ArrayList<ImportDebitNoteDetails> imDeNoList = new ArrayList<ImportDebitNoteDetails>();
            if (importdebit_info.getImportDebitList() != null) {
                try {

                    logger.info("Delete in Debit Note No "+importdebit_info.getDebitNoteNo()+" in Debit Note Details Tabel");
                    
                    dbCon.save(con,"DELETE FROM SD_DEBIT_NOTE_DETAIL WHERE DEBIT_NOTE_NO = '"+importdebit_info.getDebitNoteNo()+"'");

                    logger.info("Debit Note Details Tabel Deleted");
                    
                    imDeNoList = importdebit_info.getImportDebitList();

                    String imDeNoValue = "?,?,?,?,?,?,?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,?,SYSDATE";
                    
                    
                    PreparedStatement ps_in_debit = dbCon.prepare(con, "INSERT INTO SD_DEBIT_NOTE_DETAIL(" + accetDebitDetailsColumn + ") "
                                + "VALUES(" + imDeNoValue + ")");
                    
                    for (ImportDebitNoteDetails di : imDeNoList) {

                       ps_in_debit.setString(1, di.getDebitNoteNo());
                       ps_in_debit.setString(2, di.getImeiNo());
                       ps_in_debit.setString(3, di.getCustomerCode() );
                       ps_in_debit.setString(4, di.getShopCode());
                       ps_in_debit.setInt(5, di.getModleNo());
                       ps_in_debit.setInt(6, di.getBisId());
                       ps_in_debit.setString(7, di.getOrderNo());
                       ps_in_debit.setDouble(8, di.getSalerPrice());
                       ps_in_debit.setString(9, di.getDateAccepted());
                       ps_in_debit.setInt(10, di.getStatus());
                       ps_in_debit.setString(11, di.getRemarks());
                       ps_in_debit.setString(12, user_id);
                       ps_in_debit.addBatch();

                        logger.info("Import Debit Note IMEI Inserted........");
                    }
                    ps_in_debit.executeBatch();
                } catch (Exception e) {
                    logger.info("Error in Accept Debit Note IMEI Insert....." + e.toString());
                    result = 9;
                }
            }
        } catch (Exception ex) {
            logger.info("Error addImportDebitNote Method..:" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result== 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Accept Debit Note Insert Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Accept Debit Note Insert Fail");
        }
        dbCon.ConectionClose(con);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

}
