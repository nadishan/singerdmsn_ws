/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.AcceptInvenroryImei;
import org.dms.ws.singer.entities.AcceptInventory;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class AcceptInventoryController {
    
    

    

public static MessageWrapper getAcceptInventory(int issue_no,
            int status,
            String issuedate,
            int distibutor_id,
            int dsr_id,
            String fromdate,
            String todate,
            String order,
            String type,
            int view,
            int start,
            int limit) {

        logger.info("getAcceptInventory Method Call....................");
        ArrayList<AcceptInventory> aceptinventoryList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        
        try {

            int bisType = 1;
            
            ResultSet rs_accept = dbCon.search(con, "SELECT B.BIS_STRU_TYPE_ID CNT "
                    + "FROM SD_BUSINESS_STRUCTURES B "
                    + "WHERE B.BIS_ID= " + dsr_id + "  ");
            while (rs_accept.next()) {
                bisType = rs_accept.getInt("CNT");
            }
            
            order = DBMapper.deviceIssue(order);

            String issueNo = (issue_no == 0 ? "" : "AND ISSUE_NO = " + issue_no + "");
            
            String Status = "";
            
            if(view==0){
                if(bisType == 1){ //DSR
                    Status = " AND ISSUE_NO IN (SELECT DT.ISSUE_NO FROM SD_DEVICE_ISSUE_DTL DT WHERE DT.STATUS=2)  AND STATUS = " + status + " ";
                }else if(bisType == 2){  //SubDealer
                    Status = " AND ISSUE_NO IN (SELECT DT.ISSUE_NO FROM SD_DEVICE_ISSUE_DTL DT WHERE DT.STATUS=3)  AND STATUS = " + status + " ";
                }
            }else{
                if(bisType == 1){ //DSR
                    Status = " AND ISSUE_NO IN (SELECT DT.ISSUE_NO FROM SD_DEVICE_ISSUE_DTL DT WHERE DT.STATUS=3)  AND STATUS = " + status + " ";
                }else if(bisType == 2){  //SubDealer
                    Status = " AND ISSUE_NO IN (SELECT DT.ISSUE_NO FROM SD_DEVICE_ISSUE_DTL DT WHERE DT.STATUS=3)  AND STATUS = " + status + " ";
                }
            }
            
            
            String issueDate = (issuedate.equalsIgnoreCase("all") ? "NVL(ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(ISSUE_DATE,TO_DATE('20100101','YYYYMMDD'))" : "ISSUE_DATE = TO_DATE('" + issuedate + "','YYYY/MM/DD')");
            String distributorId = (distibutor_id == 0 ? "" : "AND DISTRIBUTER_ID = " + distibutor_id + " ");
            String dsrId = (dsr_id == 0 ? "" : "AND DSR_ID = " + dsr_id + " ");
            //String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND TO_DATE(TO_CHAR(NVL(DATE_MODIFIED,DATE_INSERTED),'YYYY/MM/DD'),'YYYY/MM/DD') >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND TO_DATE(TO_CHAR(NVL(DATE_MODIFIED,DATE_INSERTED),'YYYY/MM/DD'),'YYYY/MM/DD') <= TO_DATE('" + todate + "','YYYY/MM/DD') ";
            String Range = todate.equalsIgnoreCase("all") ? "" : fromdate.equalsIgnoreCase("all") ? "" : "AND NVL(ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) >= TO_DATE('" + fromdate + "','YYYY/MM/DD') AND NVL(ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) <= TO_DATE('" + todate + "','YYYY/MM/DD') ";

            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY ISSUE_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = " ISSUE_NO,TO_CHAR(ISSUE_DATE,'YYYY/MM/DD')"
                    + " ISSUE_DATE,STATUS,DSR_ID,MANUAL_RECEIPT_NO ";
            if(distibutor_id != 0 && dsr_id!=0){
                   seletColunm = " ISSUE_NO,TO_CHAR(ISSUE_DATE,'YYYY/MM/DD') ISSUE_DATE,STATUS,DSR_ID, (SELECT BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID="+distibutor_id+") FROMFLAG, (SELECT BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES WHERE BIS_ID="+dsr_id+") TOFLAG, MANUAL_RECEIPT_NO ";
            }
             
            String whereClous = " " + issueDate + " "
                    + " " + issueNo + " "
                    + " " + dsrId + " "
                    + " " + distributorId + " "
                    + " " + Status + " "
                    + " " + Range + " ";
                  //  + " AND ISSUE_NO IN (SELECT DT.ISSUE_NO FROM SD_DEVICE_ISSUE_DTL DT, SD_ACCEPT_INVENTORY_DTL A WHERE DT.IMEI_NO=A.IMEI_NO) "; ////New

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_DEVICE_ISSUE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM SD_DEVICE_ISSUE WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                AcceptInventory idn = new AcceptInventory();
                totalrecode = rs.getInt("CNT");
                idn.setIssueNo(rs.getInt("ISSUE_NO"));
                idn.setIssueDate(rs.getString("ISSUE_DATE"));
                idn.setStatus(rs.getInt("STATUS"));
                idn.setDsrId(rs.getInt("DSR_ID"));
                
                String manReceiptNo = "-";
                if(rs.getString("MANUAL_RECEIPT_NO") != null){
                    manReceiptNo = rs.getString("MANUAL_RECEIPT_NO");
                }
                idn.setManualReceiptNo(manReceiptNo);
                
                if (rs.getInt("STATUS") == 2) {
                    idn.setStatusMsg("Pending");
                } else if (rs.getInt("STATUS") == 3) {
                    idn.setStatusMsg("Accepted");
                } else {
                    idn.setStatusMsg("None");
                }
                
               if(distibutor_id != 0 && dsr_id!=0){
                    if(rs.getInt("FROMFLAG")==2 && rs.getInt("TOFLAG")==1){
                        idn.setIsPrint(true);
                    }else{
                        idn.setIsPrint(false);
                    }
               }else{
                        idn.setIsPrint(false);
                }
               
                aceptinventoryList.add(idn);
            }
            // rs.close();

        } catch (Exception ex) {
            logger.info("Error getAcceptDebitNote Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(aceptinventoryList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getAcceptInventoryImei(int issue_no, int status, int drs_id, int view,
            int start,
            int limit) {

        logger.info("getAcceptInventoryImei Method Call....................");
        ArrayList<AcceptInvenroryImei> aceptinventoryList = new ArrayList<>();
        int totalrecode = 0;
        int statusFlag = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ResultSet rs_accept = dbCon.search(con, "SELECT B.BIS_STRU_TYPE_ID CNT "
                    + "FROM SD_BUSINESS_STRUCTURES B "
                    + "WHERE B.BIS_ID= " + drs_id + "  ");
            while (rs_accept.next()) {
                statusFlag = rs_accept.getInt("CNT");
            }
            //  rs_accept.close();

            String issueNo = (issue_no == 0 ? "" : " AND D.ISSUE_NO = " + issue_no + "");
            String Status = (status == 0 ? "" : " AND D.STATUS = " + status + "");
            
            if(view==0){
                if(statusFlag==1){ //DSR
                    Status = (status == 0 ? "" : " AND (D.STATUS = 2) AND U.STATUS = " + status + " ");
                }
//            
                if(statusFlag==2){ //Sub Dealer
                    Status = (status == 0 ? "" : " AND D.STATUS = 3 ");
                }
            }else{
                
                if(statusFlag==1){ //DSR
                    Status = (status == 0 ? "" : " AND (D.STATUS = 3) AND U.STATUS = " + status + " ");
                }
//            
                if(statusFlag==2){ //Sub Dealer
                    Status = (status == 0 ? "" : " AND D.STATUS = 3 ");
                }
                
            }


            String devIssueStatus = " AND U.STATUS="+status+" ";
            
            String seletColunm = "D.SEQ_NO,D.ISSUE_NO,D.IMEI_NO,M.MODEL_DESCRIPTION,D.STATUS,D.MODEL_NO, "
                    + " (SELECT B1.BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES B1 WHERE B1.BIS_ID=U.DISTRIBUTER_ID) AS FROMFLAG, "
                    + " (SELECT B1.BIS_STRU_TYPE_ID FROM SD_BUSINESS_STRUCTURES B1 WHERE B1.BIS_ID=U.DSR_ID) AS TOFLAG,  "
                    + "M.ERP_PART_NO, "
                    + "BD.BIS_ID   DISTRIBUTOR_CODE,  "
                   + "BD.BIS_NAME DISTRIBUTOR_NAME,  "    
                   + "BD.ADDRESS DISTRIBUTOR_ADDRESS,  "
                   + "BD.CONTACT_NO DISTRIBUTOR_PHONE,  "
                   + "BS.BIS_ID   SUBDEALER_CODE,  "
                   + "BS.BIS_NAME SUBDEALER_NAME,  "
                   + "BS.ADDRESS SUBDEALER_ADDRESS,  "
                   + "BS.CONTACT_NO SUBDEALER_PHONE,  "
                   + "NVL(D.PRICE, 0) AS PRICE     ";

            String whereClous = " D.MODEL_NO = M.MODEL_NO (+) "
                    + " AND  D.ISSUE_NO=U.ISSUE_NO  "
                    + " AND   U.DISTRIBUTER_ID=BD.BIS_ID "
                    + "  AND   U.DSR_ID = BS.BIS_ID "
                    + "" + issueNo + " "
                    + "" + Status + "   ";
//
//            System.out.println( "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MODEL_LISTS M,SD_DEVICE_ISSUE_DTL D, SD_DEVICE_ISSUE U WHERE " + whereClous + ")CNT,"
//                    + "" + seletColunm + ",ROW_NUMBER() OVER ( ORDER BY SEQ_NO ASC ) RN "
//                    + " FROM  SD_MODEL_LISTS M,SD_DEVICE_ISSUE_DTL D, SD_DEVICE_ISSUE U WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");
//           
            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_MODEL_LISTS M,SD_DEVICE_ISSUE_DTL D, SD_DEVICE_ISSUE U, SD_BUSINESS_STRUCTURES BD, SD_BUSINESS_STRUCTURES BS WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( ORDER BY SEQ_NO ASC ) RN "
                    + " FROM SD_MODEL_LISTS M,SD_DEVICE_ISSUE_DTL D, SD_DEVICE_ISSUE U, SD_BUSINESS_STRUCTURES BD, SD_BUSINESS_STRUCTURES BS WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                AcceptInvenroryImei idn = new AcceptInvenroryImei();
                totalrecode = rs.getInt("CNT");
                idn.setSeqNo(rs.getInt("SEQ_NO"));
                idn.setIssueNo(rs.getInt("ISSUE_NO"));
                idn.setImeiNo(rs.getString("IMEI_NO"));
                idn.setModleDescription(rs.getString("MODEL_DESCRIPTION"));
                idn.setModleNo(rs.getInt("MODEL_NO"));
                idn.setStatus(rs.getInt("STATUS"));
                
                idn.setErpModel(rs.getString("ERP_PART_NO"));
                    
                idn.setDistributorCode(rs.getString("DISTRIBUTOR_CODE"));
                idn.setDistributorName(rs.getString("DISTRIBUTOR_NAME"));
                idn.setDistributorAddress(rs.getString("DISTRIBUTOR_ADDRESS"));
                idn.setDistributorTelephone(rs.getString("DISTRIBUTOR_PHONE"));

                idn.setSubdealerCode(rs.getString("SUBDEALER_CODE"));
                idn.setSubdealerName(rs.getString("SUBDEALER_NAME"));
                idn.setSubdealerAddress(rs.getString("SUBDEALER_ADDRESS"));
                idn.setSubdealerTelephone(rs.getString("SUBDEALER_PHONE"));

                double salesPrice = rs.getDouble("PRICE");
                //double realSalesPrice = (salesPrice*90)/100;
                idn.setSalesPrice(salesPrice);
                
                
                
                 if(rs.getInt("FROMFLAG")==2 && rs.getInt("TOFLAG")==1){
                        idn.setIsPrint(true);
                    }else{
                        idn.setIsPrint(false);
                    }
                
                aceptinventoryList.add(idn);
            }
            //  rs.close();
            

        } catch (Exception ex) {
            logger.info("Error getAcceptDebitNote Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(aceptinventoryList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addAcceptInventory(AcceptInventory acceptinventory_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addAcceptInventory Method Call.............................");
        int result = 0;
        String locaation_imei = "";
        int returnNo =0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            if (acceptinventory_info != null) {

                ArrayList<AcceptInvenroryImei> imeiAlrady = new ArrayList<AcceptInvenroryImei>();
                imeiAlrady = acceptinventory_info.getAcceptInvImei();

                ArrayList<String> RequestList = new ArrayList<>();

                if (imeiAlrady.size() > 0) {

                    ResultSet rs_location_cnt = dbCon.search(con, "SELECT IMEI_NO "
                            + "FROM SD_DEVICE_ISSUE_DTL  "
                            + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE DSR_ID =" + acceptinventory_info.getDsrId() + ")");

                    ArrayList<String> imiList = new ArrayList<>();

                    while (rs_location_cnt.next()) {
                        String imei = rs_location_cnt.getString("IMEI_NO");
                        imiList.add(imei);

                    }
                    // rs_location_cnt.close();

                    for (AcceptInvenroryImei im : imeiAlrady) {
                        RequestList.add(im.getImeiNo());

                    }
                    RequestList.removeAll(imiList);
                    //System.out.println(RequestList.size());
                    //System.out.println(RequestList);
                }

                if (RequestList.size() > 0) {
                    result = 3;
                    for (String s : RequestList) {
                        locaation_imei += s + ",";
                    }

                } else {

                    String acceptInventortColumn = "ISSUE_NO,DSR_ID,STATUS,USER_INSERTED,DATE_INSERTED";

                    String acceptInventoryValue = "?,?,?,?,SYSDATE";

                    PreparedStatement ps_in_accept = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_INVENTORY(" + acceptInventortColumn + ") "
                            + "VALUES(" + acceptInventoryValue + ")");
                    ps_in_accept.setInt(1, acceptinventory_info.getIssueNo());
                    ps_in_accept.setInt(2, acceptinventory_info.getDsrId());
                    ps_in_accept.setInt(3, acceptinventory_info.getStatus());
                    ps_in_accept.setString(4, user_id);
                    ps_in_accept.executeUpdate();

                    PreparedStatement ps_up_device = dbCon.prepare(con, "UPDATE SD_DEVICE_ISSUE "
                            + "SET STATUS = ? ,"
                            + "USER_MODIFIED=? ,"
                            + "DATE_MODIFIED=SYSDATE "
                            + "WHERE ISSUE_NO = ? ");

                    ps_up_device.setInt(1, acceptinventory_info.getStatus());
                    ps_up_device.setString(2, user_id);
                    ps_up_device.setInt(3, acceptinventory_info.getIssueNo());
                    ps_up_device.executeUpdate();

                    logger.info("Update Device Issue Header Table......");

                    ArrayList<AcceptInvenroryImei> imDeNoList = new ArrayList<AcceptInvenroryImei>();

                    if (acceptinventory_info.getAcceptInvImei() != null) {
                        try {
                            int max_accept_dtl = 0;
                            ResultSet rs_max_accept = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_ACCEPT_INVENTORY_DTL");
                            while (rs_max_accept.next()) {
                                max_accept_dtl = rs_max_accept.getInt("MAX");
                            }
                            rs_max_accept.close();

                            String accetDebitDetailsColumn = "SEQ_NO,ISSUE_NO,"
                                    + "IMEI_NO,MODEL_NO,"
                                    + "STATUS,"
                                    + "USER_INSERTED,"
                                    + "DATE_INSERTED";

                            String imDeNoValue = "?,?,?,?,3,?,SYSDATE";

                            PreparedStatement ps_in_accept_dtl = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_INVENTORY_DTL(" + accetDebitDetailsColumn + ") "
                                    + "VALUES(" + imDeNoValue + ")");

                            for (AcceptInvenroryImei di : acceptinventory_info.getAcceptInvImei()) {
                                max_accept_dtl = max_accept_dtl + 1;

                                ps_in_accept_dtl.setInt(1, max_accept_dtl);
                                ps_in_accept_dtl.setInt(2, di.getIssueNo());
                                ps_in_accept_dtl.setString(3, di.getImeiNo());
                                ps_in_accept_dtl.setInt(4, di.getModleNo());
                                ps_in_accept_dtl.setString(5, user_id);
                                ps_in_accept_dtl.addBatch();

                            }
                            ps_in_accept_dtl.executeBatch();

                            imDeNoList = acceptinventory_info.getAcceptInvImei();

                            PreparedStatement ps_up_device_issue_dtl = dbCon.prepare(con, "UPDATE SD_DEVICE_ISSUE_DTL "
                                    + "SET STATUS = 3 ,"
                                    + "USER_MODIFIED=?,"
                                    + "DATE_MODIFIED=SYSDATE "
                                    + "WHERE IMEI_NO = ?");

                            for (AcceptInvenroryImei di : imDeNoList) {
                                ps_up_device_issue_dtl.setString(1, user_id);
                                ps_up_device_issue_dtl.setString(2, di.getImeiNo());
                                ps_up_device_issue_dtl.addBatch();
                            }
                            ps_up_device_issue_dtl.executeBatch();

                            imDeNoList = acceptinventory_info.getAcceptInvImei();

                            PreparedStatement ps_up_device_mast = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                    + "SET BIS_ID = ? ,"
                                    + "USER_MODIFIED=?,"
                                    + "DATE_MODIFIED=SYSDATE "
                                    + "WHERE IMEI_NO = ?");

                            for (AcceptInvenroryImei di : imDeNoList) {
                                ps_up_device_mast.setInt(1, di.getBisId());
                                ps_up_device_mast.setString(2, user_id);
                                ps_up_device_mast.setString(3, di.getImeiNo());
                                ps_up_device_mast.addBatch();

                            }
                            ps_up_device_mast.executeBatch();

                            result = 1;
                            
                            returnNo = returnDealerToDSR(dbCon, con, acceptinventory_info, user_id);
                            
                            logger.info("Device Issue Details Updated......");

                        } catch (Exception e) {
                            logger.info("Error in Accept inventory Detials Update....." + e.toString());
                            result = 9;
                        }
                    }
//
                }

            }
        } catch (Exception ex) {
            logger.info("Error addAcceptInventory Method..:" + ex.toString());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
           DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	   Date date = new Date();
	  // System.out.println(dateFormat.format(date));
            vw.setReturnFlag("1");
            //vw.setReturnMsg("Accept Inventory Insert Successfully");
            vw.setReturnMsg(String.valueOf(returnNo));
            vw.setReturnRefNo(dateFormat.format(date));
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + locaation_imei + " IMEI No is Not in Accept List ");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Accept Inventory Insert");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }
    
    private static int returnDealerToDSR(DbCon dbCon, Connection con, AcceptInventory accept_info, String user){
        int nxt_returnNo = 1;
        try{
            
            if(accept_info.getAcceptInvImei().size()>0){
                
                String qry = "  SELECT DI.ISSUE_NO, UD.BIS_ID AS DSR_BISID,UD.BIS_STRU_TYPE_ID DSR, UL.BIS_ID AS DEALER_BISID, UL.BIS_STRU_TYPE_ID SUB, DI.DISTRIBUTOR_MARGIN, DI.DEALER_MARGIN, DI.PRICE, DI.DISCOUNT  "
                    + " FROM   SD_DEVICE_ISSUE DI,  "
                    + "        (SELECT  BD.BIS_ID, BD.BIS_STRU_TYPE_ID  "
                    + "         FROM    SD_BUSINESS_STRUCTURES BD   "
                    + "         WHERE   BD.BIS_STRU_TYPE_ID=2) UD,   "
                    + "         (SELECT  BD.BIS_ID, BD.BIS_STRU_TYPE_ID "
                    + "         FROM    SD_BUSINESS_STRUCTURES BD   "
                    + "         WHERE   BD.BIS_STRU_TYPE_ID=1) UL"
                    + " WHERE DI.DISTRIBUTER_ID = UD.BIS_ID  "
                    + " AND   DI.DSR_ID = UL.BIS_ID  "
                    + " AND   DI.ISSUE_NO="+accept_info.getIssueNo()+"   "
                    //+ " AND   DI.DSR_ID="+accept_info.getDsrId()+"     ";
                    + " AND UD.BIS_ID="+accept_info.getAcceptInvImei().get(0).getDistributorCode()+"     "
                    + " AND   UL.BIS_ID="+accept_info.getAcceptInvImei().get(0).getSubdealerCode()+"     ";
                ResultSet rs = dbCon.search(con, qry);
                int dsrId = 0;
                double distributorMargin = 0;
                double dealerMargin = 0;
                double discount = 0;
                int price = 0;
                boolean returnIssue = false;
                if (rs.next()) {
                    returnIssue = true;
                    dsrId = rs.getInt("DSR_BISID");
                    distributorMargin = rs.getDouble("DISTRIBUTOR_MARGIN");
                    dealerMargin = rs.getDouble("DEALER_MARGIN");
                    discount = rs.getDouble("DISCOUNT");
                   // price = rs.getDouble("PRICE");
                }
                
                if(returnIssue==true){
                    
                    for(AcceptInvenroryImei imei : accept_info.getAcceptInvImei()){
                        price += imei.getSalesPrice();
                    }
                    
                    String insertDevReturnQry = "   INSERT INTO SD_DEVICE_RETURN(RETURN_NO, DISTRIBUTER_ID, DSR_ID, RETURN_DATE, PRICE,DISTRIBUTOR_MARGIN, DEALER_MARGIN, DISCOUNT, STATUS, USER_INSERTED, DATE_INSERTED, MANUAL_RECEIPT_NO) "
                        + " VALUES (?,?,?,SYSDATE,?,?,?,?,2,?,SYSDATE,?)  ";

                    ResultSet rs1 = dbCon.search(con, "SELECT MAX(RETURN_NO) NXT_RETNO FROM SD_DEVICE_RETURN");

                    if(rs1.next()){
                        nxt_returnNo = rs1.getInt("NXT_RETNO")+1;
                    }

                    System.out.println("Header Return No:: "+nxt_returnNo);
                    System.out.println("Distrib:DSR = "+dsrId+" : "+accept_info.getDsrId());

                     PreparedStatement psH = dbCon.prepare(con, insertDevReturnQry);
                     psH.setInt(1, nxt_returnNo);
//                     psH.setInt(2, dsrId);
//                     psH.setInt(3, accept_info.getDsrId());
                     psH.setInt(2, accept_info.getDsrId());
                     psH.setInt(3, dsrId);
                     psH.setDouble(4, price);
                     psH.setDouble(5, distributorMargin);
                     psH.setDouble(6, dealerMargin);
                     psH.setDouble(7, discount);
                     psH.setString(8, user);
                     psH.setString(9, accept_info.getManualReceiptNo());
                     psH.executeUpdate();

                     String insertReturnDetails = " INSERT INTO SD_DEVICE_RETURN_DTL(SEQ_NO, RETURN_NO, IMEI_NO, MODEL_NO, STATUS, USER_INSERTED, DATE_INSERTED, PRICE) "
                             + "    VALUES(?,?,?,?,3,?,SYSDATE,?)";

        //            ResultSet rs2 = dbCon.search(con, "SELECT MAX(SEQ_NO) NXT_SEQNO FROM SD_DEVICE_RETURN_DTL");
        //            int nxt_returnSeqNo = 1;
        //            if(rs2.next()){
        //                nxt_returnSeqNo = rs.getInt("NXT_SEQNO");
        //            }

                     PreparedStatement psD = dbCon.prepare(con, insertReturnDetails);

                     int cnt = 1;
                     for(AcceptInvenroryImei imei : accept_info.getAcceptInvImei()){
                         System.out.println(nxt_returnNo+" - Detail Sequence::: "+cnt);
                         psD.setInt(1, cnt);
                         psD.setInt(2, nxt_returnNo);
                         psD.setString(3, imei.getImeiNo());
                         psD.setInt(4, imei.getModleNo());
                         psD.setString(5, user);
                         psD.setDouble(6, imei.getSalesPrice());

                         psD.addBatch();
                         cnt++;
                     }
                     psD.executeBatch();
                
                }
                
                
            }
            
             
        }catch(Exception e){
            System.out.println("DSR to Dealer Return Error...........");
            e.printStackTrace();
        }
        return nxt_returnNo;
    }

    public static ValidationWrapper editAcceptInventory(AcceptInventory acceptinventory_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editAcceptInventory Method Call.............................");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int max_accept = 0;
        try {
            if (acceptinventory_info != null) {

                dbCon.save(con, "UPDATE SD_ACCEPT_INVENTORY "
                        + "SET STATUS = " + acceptinventory_info.getStatus() + ","
                        + "USER_MODIFIED = '" + user_id + "',"
                        + "DATE_MODIFIED = SYSDATE "
                        + "WHERE ISSUE_NO = " + acceptinventory_info.getIssueNo() + " ");

                dbCon.save(con, "UPDATE SD_DEVICE_ISSUE "
                        + "SET STATUS = " + acceptinventory_info.getStatus() + " ,"
                        + "USER_MODIFIED='" + user_id + "' ,"
                        + "DATE_MODIFIED=SYSDATE "
                        + "WHERE ISSUE_NO = " + acceptinventory_info.getIssueNo() + " ");

                logger.info("Update Device Issue Header Table......");

                ArrayList<AcceptInvenroryImei> imDeNoList = new ArrayList<AcceptInvenroryImei>();

                if (acceptinventory_info.getAcceptInvImei() != null) {
                    try {

                        String accetDebitDetailsColumn = "SEQ_NO,ISSUE_NO,"
                                + "IMEI_NO,"
                                + "MODEL_NO,"
                                + "STATUS,"
                                + "USER_INSERTED,"
                                + "DATE_INSERTED";

                        ResultSet rs_max_acp = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_ACCEPT_INVENTORY_DTL");
                        while (rs_max_acp.next()) {
                            max_accept = rs_max_acp.getInt("MAX");
                        }

                        String imDeNoValue = "?,?,?,?,3,?,SYSDATE";

                        PreparedStatement ps_in_accept = dbCon.prepare(con, "INSERT INTO SD_ACCEPT_INVENTORY_DTL(" + accetDebitDetailsColumn + ") "
                                + "VALUES(" + imDeNoValue + ")");

                        for (AcceptInvenroryImei di : acceptinventory_info.getAcceptInvImei()) {

                            max_accept = max_accept + 1;

                            ps_in_accept.setInt(1, max_accept);
                            ps_in_accept.setInt(2, di.getIssueNo());
                            ps_in_accept.setString(3, di.getImeiNo());
                            ps_in_accept.setInt(4, di.getModleNo());
                            ps_in_accept.setString(5, user_id);
                            ps_in_accept.addBatch();;

                        }
                        ps_in_accept.executeBatch();

                        imDeNoList = acceptinventory_info.getAcceptInvImei();
                        PreparedStatement ps_up_device = dbCon.prepare(con, "UPDATE SD_DEVICE_ISSUE_DTL "
                                + "SET STATUS = 3 ,"
                                + "USER_MODIFIED=?,"
                                + "DATE_MODIFIED=SYSDATE "
                                + "WHERE ISSUE_NO = ? "
                                + "AND IMEI_NO = ?");

                        for (AcceptInvenroryImei di : imDeNoList) {
                            ps_up_device.setString(1, user_id);
                            ps_up_device.setInt(2, di.getIssueNo());
                            ps_up_device.setString(3, di.getImeiNo());
                            ps_up_device.addBatch();

                        }
                        ps_up_device.executeBatch();

                        imDeNoList = acceptinventory_info.getAcceptInvImei();
                        PreparedStatement ps_up_dmast = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER "
                                + "SET BIS_ID = ? ,"
                                + "USER_MODIFIED=?,"
                                + "DATE_MODIFIED=SYSDATE "
                                + "WHERE IMEI_NO = ?");

                        for (AcceptInvenroryImei di : imDeNoList) {
                            ps_up_dmast.setInt(1, di.getBisId());
                            ps_up_dmast.setString(2, user_id);
                            ps_up_dmast.setString(3, di.getImeiNo());
                            ps_up_dmast.addBatch();

                        }
                        ps_up_dmast.executeBatch();

                        result = 1;
                        logger.info("Device Issue Details Updated......");

                    } catch (Exception e) {
                        logger.info("Error in Accept inventory Detials Update....." + e.toString());
                        result = 9;
                    }
                }

            }
        } catch (Exception ex) {
            logger.info("Error addAcceptInventory Method..:" + ex.toString());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Accept Inventory Update Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Accept Update Inventory");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }
}
