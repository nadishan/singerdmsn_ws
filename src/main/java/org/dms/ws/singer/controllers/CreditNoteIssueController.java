/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.CreditNoteIssue;
import org.dms.ws.singer.entities.CreditNoteIssueDetail;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class CreditNoteIssueController {

    public static MessageWrapper getCreditNoteIssueDetails(int credit_no,
            String debit_no,
            int distributor_id,
            String issue_date,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getCreditNoteIssueDetails Method Call....................");
        ArrayList<CreditNoteIssue> creditIssueList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.creditnoteIssue(order);

            String CreditNo = (credit_no == 0 ? "" : "AND CR_NOTE_ISS_NO = " + credit_no + "");
            String DebitNo = (debit_no.equalsIgnoreCase("all") ? "NVL(DEBIT_NOTE_NO,'AA') = NVL(DEBIT_NOTE_NO,'AA')" : "UPPER(DEBIT_NOTE_NO) LIKE UPPER('%" + debit_no + "%')");
            String DistributorId = (distributor_id == 0 ? "" : "AND DISTRIBUTER_ID = " + distributor_id + "");
            String IssueDate = (issue_date.equalsIgnoreCase("all") ? "NVL(ISSUE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(ISSUE_DATE,TO_DATE('20100101','YYYYMMDD'))" : "ISSUE_DATE = TO_DATE('" + issue_date + "','YYYY/MM/DD')");
            String Status = (status == 0 ? "" : "AND STATUS = '" + status + "'");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY CR_NOTE_ISS_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "CR_NOTE_ISS_NO,"
                    + "DEBIT_NOTE_NO,"
                    + "DISTRIBUTER_ID,"
                    + "TO_CHAR(ISSUE_DATE,'YYYY/MM/DD') ISSUE_DATE,"
                    + "STATUS";

            String whereClous = " " + DebitNo + " "
                    + " " + CreditNo + " "
                    + " " + DistributorId + " "
                    + " AND " + IssueDate + " "
                    + " " + Status + " ";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_CREDIT_NOTE_ISSUE WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER (" + Order + ") RN "
                    + " FROM SD_CREDIT_NOTE_ISSUE WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                CreditNoteIssue cn = new CreditNoteIssue();
                totalrecode = rs.getInt("CNT");
                cn.setCreditNoteIssueNo(rs.getInt("CR_NOTE_ISS_NO"));
                cn.setDebitNoteNo(rs.getString("DEBIT_NOTE_NO"));
                cn.setDistributerId(rs.getInt("DISTRIBUTER_ID"));
                cn.setIssueDate(rs.getString("ISSUE_DATE"));
                cn.setStatus(rs.getInt("STATUS"));
                creditIssueList.add(cn);
            }

        } catch (Exception ex) {
            logger.info("Error getCreditNoteIssueDetails Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(creditIssueList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addCreditNoteIssue(CreditNoteIssue creditNote_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addCreditNoteIssue Method Call.............................");
        int result = 0;
        int imei_mast_cnt = 0;
        int max_issue = 0;
        String return_imei = "";
        int imet_al_credit = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<CreditNoteIssueDetail> crDetailList = new ArrayList<CreditNoteIssueDetail>();
            crDetailList = creditNote_info.getCerditNoteList();

            ArrayList<String> requestList = new ArrayList<>();
            if (crDetailList.size() > 0) {

                for (CreditNoteIssueDetail cd : crDetailList) {
                    requestList.add(cd.getImeiNo());
                }

                ArrayList<String> creditList = new ArrayList<>();

                ResultSet rs_credit = dbCon.search(con, "SELECT IMEI_NO  "
                        + "FROM SD_CREDIT_NOTE_ISSUE_DTL");

                while (rs_credit.next()) {
                    creditList.add(rs_credit.getString("IMEI_NO"));
                }

                for (String s : requestList) {
                    if (creditList.contains(s)) {
                        return_imei += s + ",";
                        imet_al_credit = 1;
                    }
                }

            }

            if (imet_al_credit > 0) {
                result = 3;
            } else {

                ArrayList<String> masterList = new ArrayList<>();

                ResultSet rs_master = dbCon.search(con, "SELECT IMEI_NO "
                        + "FROM SD_DEVICE_MASTER WHERE BIS_ID = " + creditNote_info.getDistributerId() + " ");

                while (rs_master.next()) {
                    masterList.add(rs_master.getString("IMEI_NO"));
                }

                ArrayList<String> temrequestList = new ArrayList<>();
                temrequestList = requestList;
                temrequestList.removeAll(masterList);

                if (temrequestList.size() > 0) {
                    for (String s : temrequestList) {
                        return_imei += s + ",";
                    }
                    imei_mast_cnt = 1;

                }

                if (imei_mast_cnt > 0) {
                    result = 2;
                } else {

                    ResultSet rs_max_issu = dbCon.search(con, "SELECT NVL(MAX(CR_NOTE_ISS_NO),0) MAX FROM SD_CREDIT_NOTE_ISSUE");

                    while (rs_max_issu.next()) {
                        max_issue = rs_max_issu.getInt("MAX");
                    }

                    String insertColumns = "CR_NOTE_ISS_NO,DISTRIBUTER_ID,ISSUE_DATE,STATUS,USER_INSERTED,DATE_INSERTED";

                    String insertValue = "?,?,TO_DATE(?,'YYYY/MM/DD'),?,?,SYSDATE";

                    PreparedStatement ps_in_iss = dbCon.prepare(con, "INSERT INTO SD_CREDIT_NOTE_ISSUE(" + insertColumns + ") "
                            + "VALUES(" + insertValue + ")");

                    ps_in_iss.setInt(1, max_issue + 1);
                    ps_in_iss.setInt(2, creditNote_info.getDistributerId());
                    ps_in_iss.setString(3, creditNote_info.getIssueDate());
                    ps_in_iss.setInt(4, creditNote_info.getStatus());
                    ps_in_iss.setString(5, user_id);
                    ps_in_iss.executeUpdate();
                    result = 1;
                    logger.info("Credit Note Issue  Details Inseted.....");

                    int max_issue_dtl = 0;
                    ResultSet rs_max_issudtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX FROM SD_CREDIT_NOTE_ISSUE_DTL WHERE CR_NOTE_ISS_NO = " + (max_issue + 1) + "");

                    while (rs_max_issudtl.next()) {
                        max_issue_dtl = rs_max_issudtl.getInt("MAX");
                    }

                    String ceditNoteDetailColumn = "SEQ_NO,CR_NOTE_ISS_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                    String creditNoteValue = "?,?,?,?,1,?,SYSDATE,?";

                    PreparedStatement ps_in_issdtl = dbCon.prepare(con, "INSERT INTO SD_CREDIT_NOTE_ISSUE_DTL(" + ceditNoteDetailColumn + ") "
                            + "VALUES(" + creditNoteValue + ")");

                    PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET BAD_ITEM_FLAG = ? WHERE IMEI_NO = ?");

                    for (CreditNoteIssueDetail cd : crDetailList) {
                        max_issue_dtl = max_issue_dtl + 1;
                        ps_in_issdtl.setInt(1, max_issue_dtl);
                        ps_in_issdtl.setInt(2, (max_issue + 1));
                        ps_in_issdtl.setString(3, cd.getImeiNo());

                        if (creditNote_info.getStatus() == 2) {
                            ps_up_master.setInt(1, 1);
                            ps_up_master.setString(2, cd.getImeiNo());
                            ps_up_master.addBatch();
                        }
                        ps_in_issdtl.setInt(4, cd.getModleNo());
                        ps_in_issdtl.setString(5, user_id);
                        ps_in_issdtl.setDouble(6, cd.getSalesPrice());
                        ps_in_issdtl.addBatch();
                    }
                    ps_in_issdtl.executeBatch();
                    ps_up_master.executeBatch();

                    result = 1;
                    logger.info("Credit Note Issue Details IME  Data Inserted........");

                }
            }

        } catch (Exception ex) {
            logger.info("Error addCreditNoteIssue Method..:" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Credit Note Issue Insert Successfully");
            logger.info("Credit Note Issue Insert Successfully.............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMEI Number Not in Your Location");
            logger.info("This " + return_imei + " IMEI Number Not in Your Location.............");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + return_imei + " IMEI Number have a Credit Note");
            logger.info("This " + return_imei + " IMEI Number have a Credit Note.............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Credit Note Issue Insert Fail");
            logger.info("Credit Note Issue Insert Fail.............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    public static MessageWrapper getCreditNoteIssueDetail(int credit_note_no) {
        logger.info("getCreditNoteIssueDetail Method Call...........");
        ArrayList<CreditNoteIssueDetail> crditNoteDetailList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String whereClous = "C.MODEL_NO = M.MODEL_NO (+) "
                    + "AND CR_NOTE_ISS_NO = " + credit_note_no + "";

            String ImeColumn = "C.SEQ_NO,"
                    + "C.CR_NOTE_ISS_NO,"
                    + "C.IMEI_NO,"
                    + "C.MODEL_NO,"
                    + "C.STATUS,"
                    + "M.MODEL_DESCRIPTION,C.PRICE";

            String table = "SD_CREDIT_NOTE_ISSUE_DTL C,SD_MODEL_LISTS M";

            ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM " + table + " WHERE " + whereClous + "");

            while (rs_ime.next()) {
                CreditNoteIssueDetail cni = new CreditNoteIssueDetail();
                cni.setSeqNo(rs_ime.getInt("SEQ_NO"));
                cni.setCrNoteissueNo(rs_ime.getInt("CR_NOTE_ISS_NO"));
                cni.setImeiNo(rs_ime.getString("IMEI_NO"));
                cni.setModleNo(rs_ime.getInt("MODEL_NO"));
                cni.setStatus(rs_ime.getInt("STATUS"));
                cni.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));
                cni.setSalesPrice(rs_ime.getDouble("PRICE"));
                crditNoteDetailList.add(cni);
            }

        } catch (Exception ex) {
            logger.info("Error Retriview Device Return......:" + ex.toString());
        }
        totalrecode = crditNoteDetailList.size();
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(crditNoteDetailList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper editCreditNoteIssue(CreditNoteIssue creditNote_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editCreditNoteIssue Method Call............");
        int result = 0;
        int imei_mast_cnt = 0;
        //int max_issue = 0;
        String return_imei = "";
        int imet_al_credit = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ArrayList<CreditNoteIssueDetail> crDetailList = new ArrayList<CreditNoteIssueDetail>();
            crDetailList = creditNote_info.getCerditNoteList();

            ArrayList<String> requestList = new ArrayList<>();
            if (crDetailList.size() > 0) {

                for (CreditNoteIssueDetail cd : crDetailList) {
                    requestList.add(cd.getImeiNo());
                }

                ArrayList<String> creditList = new ArrayList<>();

                ResultSet rs_credit = dbCon.search(con, "SELECT IMEI_NO  "
                        + "FROM SD_CREDIT_NOTE_ISSUE_DTL WHERE CR_NOTE_ISS_NO != " + creditNote_info.getCreditNoteIssueNo() + "");

                while (rs_credit.next()) {
                    creditList.add(rs_credit.getString("IMEI_NO"));
                }

                for (String s : requestList) {
                    if (creditList.contains(s)) {
                        return_imei += s + ",";
                        imet_al_credit = 1;
                    }
                }

            }

            if (imet_al_credit > 0) {
                result = 3;
            } else {

                ArrayList<String> masterList = new ArrayList<>();

                ResultSet rs_master = dbCon.search(con, "SELECT IMEI_NO "
                        + "FROM SD_DEVICE_MASTER WHERE BIS_ID = " + creditNote_info.getDistributerId() + "");

                while (rs_master.next()) {
                    masterList.add(rs_master.getString("IMEI_NO"));
                }

                ArrayList<String> temrequestList = new ArrayList<>();
                temrequestList = requestList;
                temrequestList.removeAll(masterList);

                if (temrequestList.size() > 0) {
                    for (String s : temrequestList) {
                        return_imei += s + ",";
                    }
                    imei_mast_cnt = 1;

                }

                if (imei_mast_cnt > 0) {
                    result = 2;
                } else {

                    dbCon.save(con, "DELETE SD_CREDIT_NOTE_ISSUE_DTL "
                            + "WHERE CR_NOTE_ISS_NO = " + creditNote_info.getCreditNoteIssueNo() + "");

                    dbCon.save(con, "UPDATE SD_CREDIT_NOTE_ISSUE SET ISSUE_DATE=TO_DATE('" + creditNote_info.getIssueDate() + "','YYYY/MM/DD'),"
                            + "STATUS=" + creditNote_info.getStatus() + ","
                            + "USER_MODIFIED='" + user_id + "',"
                            + "DATE_MODIFIED=SYSDATE "
                            + "WHERE CR_NOTE_ISS_NO=" + creditNote_info.getCreditNoteIssueNo() + "");

                    result = 1;

                    if (crDetailList.size() > 0) {

                        String ceditNoteDetailColumn = "SEQ_NO,CR_NOTE_ISS_NO,IMEI_NO,MODEL_NO,STATUS,USER_INSERTED,DATE_INSERTED,PRICE";

                        String creditNoteValue = "?,?,?,?,1,?,SYSDATE,?";

                        PreparedStatement ps_in_credit_dtl = dbCon.prepare(con, "INSERT INTO SD_CREDIT_NOTE_ISSUE_DTL(" + ceditNoteDetailColumn + ") "
                                + "VALUES(" + creditNoteValue + ")");

                        PreparedStatement ps_up_master = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET BAD_ITEM_FLAG = ? WHERE IMEI_NO = ?");

                        int max_dtl = 0;

                        ResultSet rs_max_dtl = dbCon.search(con, "SELECT NVL(MAX(SEQ_NO),0) MAX "
                                + "FROM SD_CREDIT_NOTE_ISSUE_DTL "
                                + "WHERE CR_NOTE_ISS_NO = " + creditNote_info.getCreditNoteIssueNo() + "");

                        while (rs_max_dtl.next()) {
                            max_dtl = rs_max_dtl.getInt("MAX");
                        }

                        for (CreditNoteIssueDetail cd : crDetailList) {
                            max_dtl = max_dtl + 1;
                            ps_in_credit_dtl.setInt(1, max_dtl);
                            ps_in_credit_dtl.setInt(2, creditNote_info.getCreditNoteIssueNo());
                            ps_in_credit_dtl.setString(3, cd.getImeiNo());
                            
                            if (creditNote_info.getStatus() == 2) {
                                ps_up_master.setInt(1, 1);
                                ps_up_master.setString(2, cd.getImeiNo());
                                ps_up_master.addBatch();
                            }
                            ps_in_credit_dtl.setInt(4, cd.getModleNo());
                            ps_in_credit_dtl.setString(5, user_id);
                            ps_in_credit_dtl.setDouble(6, cd.getSalesPrice());
                            ps_in_credit_dtl.addBatch();
                        }
                        ps_in_credit_dtl.executeBatch();
                        ps_up_master.executeBatch();
                        result = 1;

                        logger.info("Credit Note Issue Details IME  Data Inserted........");
                    }
                }
            }

        } catch (Exception ex) {
            logger.info("Error editCreditNoteIssue Method.. :" + ex.toString());
            result = 9;
        }

        if (!return_imei.equals("")) {
            return_imei = return_imei.substring(0, (return_imei.length() - 1));
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Credit Note Issue Edit Successfully ");
            logger.info("Credit Note Issue Edit Successfully ...........");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This " + return_imei + " IMEI Number Not in Master Record");
            logger.info("This " + return_imei + " IMEI Number Not in Master Record.............");
        } else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("This " + return_imei + " IMEI Number have a Credit Note");
            logger.info("This " + return_imei + " IMEI Number have a Credit Note.............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error in Credit Note Issue Edit");
            logger.info("Getting Error in Credit Note Issue Edit ...............");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static int checkImeiReplacementReturn(String imei) {
        int okToCreditNote = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            String qry = " SELECT   COUNT(*) AS CNT             "
                    + " FROM        SD_IMEI_REPLACEMENT         "
                    + " WHERE       OLD_IMEI='" + imei + "'     ";

            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                okToCreditNote = rs.getInt("CNT");
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return okToCreditNote;
    }
}
