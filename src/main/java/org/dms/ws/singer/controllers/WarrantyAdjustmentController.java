/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.WarrantyAdjustment;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WarrantyAdjustmentController {

    public static MessageWrapper getWarrantyAdjustmentDetails(String imei,
            int extendedDays,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getWarrantyAdjustmentDetails Mehtod Call.......");
        int totalrecode = 0;
        ArrayList<WarrantyAdjustment> warrantyAdjustList = new ArrayList<WarrantyAdjustment>();
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.warrentyAdjusment(order);

            String IMEI = (imei.equalsIgnoreCase("all") ? " NVL(IMEI,'AA') = NVL(IMEI,'AA')" : " UPPER(IMEI) LIKE UPPER('%" + imei + "%')");
            String ExtendedDays = (extendedDays == 0 ? "" : " AND EXTENDED_DAYS = " + extendedDays + " ");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY IMEI DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = " ID, IMEI, EXTENDED_DAYS,STATUS ";

            String whereClous = " " + IMEI + " "
                    + " " + ExtendedDays + "  "
                    + "AND STATUS != 9";


            String selectQry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_WARRANTY_ADJUSTMENT WHERE   " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER ("+Order+") RN "
                    + "FROM SD_WARRANTY_ADJUSTMENT  "
                    + "WHERE   " + whereClous + ") "
                    + "WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN";

            //System.out.println(selectQry);
            ResultSet rs = dbCon.search(con,selectQry);

            logger.info("Get Data to ResultSet............");

            while (rs.next()) {
                WarrantyAdjustment warAdj = new WarrantyAdjustment();
                totalrecode = rs.getInt("CNT");
                warAdj.setWarrantyId(rs.getInt("ID"));
                warAdj.setiMEI(rs.getString("IMEI"));
                warAdj.setExtendedDays(rs.getInt("EXTENDED_DAYS"));
                warAdj.setStatus(rs.getInt("STATUS"));

                warrantyAdjustList.add(warAdj);
            }

        } catch (Exception ex) {
            logger.info("Error in getWarrantyAdjustmentDetails mehtod Error... :" + ex.getMessage());
        }
        logger.info("Ready Return Values.............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(warrantyAdjustList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addWarrantyAdjustment(WarrantyAdjustment warrantyAdjst,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addWarrantyAdjustment Mehtod Call.......");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            //Check IMEI is already exist............
            String selectQry = "SELECT COUNT(*) FROM SD_WARRANTY_ADJUSTMENT WHERE IMEI='" + warrantyAdjst.getiMEI() + "'";
            ResultSet rs = dbCon.search(con,selectQry);
            boolean imeiExist = false;
            if (rs.next()) {
                if (rs.getInt(1) > 0) {
                    imeiExist = true;
                }
            }

            String column = "ID,IMEI,EXTENDED_DAYS,STATUS,USER_INSERTED,DATE_INSERTED ";

            String values = "(SELECT NVL(MAX(ID),0)+1 FROM SD_WARRANTY_ADJUSTMENT),"
                    + "'" + warrantyAdjst.getiMEI() + "',"
                    + "" + warrantyAdjst.getExtendedDays() + ","
                    + " 1,   "
                    + "'" + user_id + "',"
                    + "SYSDATE";
            //System.out.println("INSERT INTO SD_DEFECT_MINOR_CODES(" + column + ") VALUES(" + values + ")");
            try {
                if (imeiExist == false) {
                    dbCon.save(con,"INSERT INTO SD_WARRANTY_ADJUSTMENT(" + column + ") VALUES(" + values + ")");
                    result = 1;
                } else {
                    result = 2;
                }

            } catch (Exception e) {
                logger.info("Error in Warranty Adjustment Insert......");
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error in addWarrantyAdjustment Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Adjustment Insert Successfully");
            logger.info("Warranty Adjustment Insert Succesfully............");

        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("The Warranty of IMEI you entered is already extended. Please Search IMEI from the list and Edit the Warranty Period");
            logger.info("IMEI is already existing............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Adjustment Insert");
            logger.info("Error in Warranty Adjustment Insert................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editWarrantyAdjustment(WarrantyAdjustment warrantyAdjst,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editWarrantyAdjustment Method Call.....");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {


                dbCon.save(con,"UPDATE   SD_WARRANTY_ADJUSTMENT  "
                        + "SET      EXTENDED_DAYS = " + warrantyAdjst.getExtendedDays() + ",   "
                        + "         USER_MODIFIED = '" + user_id + "',"
                        + "         DATE_MODIFIED = SYSDATE "
                        + "WHERE    ID = '" + warrantyAdjst.getWarrantyId() + "' ");

                result = 1;


        } catch (Exception ex) {
            logger.info("Error in editWarrantyAdjustment Method  :" + ex.getMessage());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Adjustment Update Successfully");
            logger.info("Warranty Adjusment Update Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Adjustment Update");
            logger.info("Error in Warranty Adjusment Update.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    
    
    
     public static ValidationWrapper deleteWarrantyAdjustment(WarrantyAdjustment warrantyAdjst,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deleteWarrantyAdjustment Method Call.....");
        int result = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            try {
                PreparedStatement ps_up_wa = dbCon.prepare(con,"UPDATE   SD_WARRANTY_ADJUSTMENT  "
                        + "SET      STATUS = 9,   "
                        + "         USER_MODIFIED = ?,"
                        + "         DATE_MODIFIED = SYSDATE "
                        + "WHERE    ID = ? ");
                ps_up_wa.setString(1, user_id);
                ps_up_wa.setInt(2, warrantyAdjst.getWarrantyId());
                ps_up_wa.executeUpdate();
                
                result = 1;
            } catch (Exception e) {
                logger.info("Error in Warranty Update Query..." + e.getMessage());
                result = 9;
            }

        } catch (Exception ex) {
            logger.info("Error in deleteWarrantyAdjustment Method  :" + ex.getMessage());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Warranty Adjustment Delete Successfully");
            logger.info("Waranty Adjusment Delete Successfully.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Warranty Adjustment Delete");
            logger.info("Error in Waranty Adjusment Delete.....");
        }
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }
}
