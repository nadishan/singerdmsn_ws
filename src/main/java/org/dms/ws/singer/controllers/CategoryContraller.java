/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Category;
import org.dms.ws.singer.utils.DBMapper;

/**
 *  
 * @author SDU
 */
public class CategoryContraller {

    public static MessageWrapper getCategories(int cateid,
            String catename,
            String catedesc,
            int catestatus,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getCategories Method Call........");

        ArrayList<Category> cateList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            order = DBMapper.categoty(order);

            String CateId = (cateid == 0 ? "" : "AND BCAT_ID=" + cateid + "");
            String CateName = (catename.equalsIgnoreCase("all") ? "NVL(BCAT_NAME,'AA') = NVL(BCAT_NAME,'AA')" : "UPPER(BCAT_NAME) LIKE UPPER('%" + catename + "%')");
            String CateDesc = (catedesc.equalsIgnoreCase("all") ? "NVL(BCAT_DESC,'AA') = NVL(BCAT_DESC,'AA')" : "UPPER(BCAT_DESC) LIKE UPPER('%" + catedesc + "%')");
            String CateStatus = (catestatus == 0 ? "" : "AND STATUS=" + catestatus + "");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY BCAT_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColunm = "BCAT_ID,BCAT_NAME,BCAT_DESC,STATUS,REMARKS";

            String whereClous = "" + CateName + " "
                    + "" + CateId + " "
                    + "AND " + CateDesc + " "
                    + "" + CateStatus + "";

            String selectQry = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_BUSINESS_CATEGORIES WHERE " + whereClous + ") CNT,"
                    + " " + selectColunm + ",ROW_NUMBER() OVER (" + Order + ") RN FROM SD_BUSINESS_CATEGORIES WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " "
                    + "AND " + (start + limit) + "   ORDER BY RN";

//            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*) FROM SD_BUSINESS_CATEGORIES WHERE " + whereClous + ") CNT,"
//                    + " " + selectColunm + ",ROW_NUMBER() OVER (ORDER BY BCAT_ID) RN FROM SD_BUSINESS_CATEGORIES WHERE " + whereClous + ") "
//                    + " WHERE RN BETWEEN " + start + " "
//                    + "AND " + (start + limit) + " ORDER BY RN");
            ResultSet rs = dbCon.search(con,selectQry);

            logger.info("Get Data to ResultSet.............");
            while (rs.next()) {
                Category cg = new Category();
                totalrecode = rs.getInt("CNT");
                cg.setCateId(rs.getInt("BCAT_ID"));
                cg.setCateName(rs.getString("BCAT_NAME"));
                cg.setCateDesc(rs.getString("BCAT_DESC"));
                cg.setCateStatus(rs.getInt("STATUS"));
                cg.setRemarks(rs.getString("REMARKS"));
                cateList.add(cg);
            }

        } catch (Exception ex) {
            logger.info("Error getCategories method  :" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(cateList);
        logger.info("Ready to Return Values......");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper addCaterory(Category cateInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("addCaterory Method Call.................");
        int result = 0;
        int cat_count = 0;
        int max_cate = 0;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet cat_cnt = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_BUSINESS_CATEGORIES WHERE UPPER(BCAT_NAME)=UPPER('" + cateInfo.getCateName() + "')");

            while (cat_cnt.next()) {
                cat_count = cat_cnt.getInt("CNT");
            }

            ResultSet rs_max = dbCon.search(con,"SELECT MAX(NVL(BCAT_ID,0)) MAX FROM SD_BUSINESS_CATEGORIES");

            if (rs_max.next()) {
                max_cate = rs_max.getInt("MAX");
            }

            String columns = "BCAT_ID,BCAT_NAME,BCAT_DESC,STATUS,USER_INSERTED,DATE_INSERTED";

            String values = "?,UPPER(?),?,?,?,SYSDATE ";

            if (cat_count == 0) {
                try {

                    PreparedStatement ps_in_cate = dbCon.prepare(con,"INSERT INTO SD_BUSINESS_CATEGORIES(" + columns + ") "
                            + "VALUES(" + values + ")");

                    max_cate = max_cate + 1;

                    ps_in_cate.setInt(1, max_cate);
                    ps_in_cate.setString(2, cateInfo.getCateName());
                    ps_in_cate.setString(3, cateInfo.getCateDesc());
                    ps_in_cate.setInt(4, cateInfo.getCateStatus());
                    ps_in_cate.setString(5, user_id);
                    ps_in_cate.executeUpdate();
                    result = 1;
                } catch (Exception e) {
                    logger.info("Error in Inserting  " + e.getMessage());
                    result = 9;
                }
            } else {
                result = 2;
                logger.info("Category Name Allredy Exists");
            }

        } catch (Exception ex) {
            logger.info("Error addCaterory Method   :" + ex.toString());
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Category insert Successfully");
            logger.info("Category insert Successfully.......");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Category Name Already Exist");
            logger.info("Category Name Already Exist......");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Category Insert");
            logger.info("Error in Category Insert......");
        }
        logger.info("Ready to Return Values......................");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper editCategory(Category cateInfo,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("editCategory Method Call.....");
        int result = 0;
        int cat_count = 0;
        int cat_ald_cnt = 0;
        int cat_use_cnt = 0;
        String bis_id = null;
                DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

             
                ResultSet rs_cat1_use = dbCon.search(con,"SELECT BIS_ID,COUNT(*) CNT "
                        + "FROM SD_BUSINESS_STRUCTURES "
                        + "WHERE CATEGORY_1 = " + cateInfo.getCateId() + " GROUP BY BIS_ID");

                while (rs_cat1_use.next()) {
                    bis_id = rs_cat1_use.getString("BIS_ID");
                    cat_use_cnt = rs_cat1_use.getInt("CNT");
                }

                if (cat_use_cnt == 0) {
                    ResultSet rs_cat2_use = dbCon.search(con,"SELECT BIS_ID,COUNT(*) CNT "
                            + "FROM SD_BUSINESS_STRUCTURES "
                            + "WHERE CATEGORY_2 = " + cateInfo.getCateId() + " GROUP BY BIS_ID");

                    while (rs_cat2_use.next()) {
                        bis_id = rs_cat2_use.getString("BIS_ID");
                        cat_use_cnt = rs_cat2_use.getInt("CNT");
                    }
                }

            

            if(cat_use_cnt > 0){
                result = 3;
                
            }else{
                
                 ResultSet rs_cat_ald_cnt = dbCon.search(con,"SELECT COUNT(*) CNT FROM SD_BUSINESS_CATEGORIES "
                    + "WHERE UPPER(BCAT_NAME) = UPPER('" + cateInfo.getCateName() + "') "
                    + "AND BCAT_ID != " + cateInfo.getCateId() + "");

            while (rs_cat_ald_cnt.next()) {
                cat_ald_cnt = rs_cat_ald_cnt.getInt("CNT");
            }

            if (cat_ald_cnt > 0) {
                result = 2;
            } else {

                try {

                    PreparedStatement ps_up_cate = dbCon.prepare(con,"UPDATE SD_BUSINESS_CATEGORIES "
                            + "SET      BCAT_NAME = UPPER(?),"
                            + "         BCAT_DESC = ?,"
                            + "         STATUS = ?,"
                            + "         USER_MODIFIED = ?,"
                            + "         DATE_MODIFIED = SYSDATE, "
                            + "         REMARKS = ?  "
                            + "WHERE BCAT_ID = ?");

                    ps_up_cate.setString(1, cateInfo.getCateName());
                    ps_up_cate.setString(2, cateInfo.getCateDesc());
                    ps_up_cate.setInt(3, cateInfo.getCateStatus());
                    ps_up_cate.setString(4, user_id);
                    ps_up_cate.setString(5, cateInfo.getRemarks());
                    ps_up_cate.setInt(6, cateInfo.getCateId());
                    ps_up_cate.executeUpdate();

                    result = 1;

                } catch (Exception e) {
                    logger.info("Error in Category Update...");
                    result = 9;
                }

            }
                
            }
            
        } catch (Exception ex) {
            logger.info("Error editCategory Method  :" + ex.toString());
        }

        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Category Update Successfully");
            logger.info("Category Update Successfully.....");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("Category Name Already Exist");
            logger.info("Category Name Already Exist.....");
        }else if (result == 3) {
            vw.setReturnFlag("3");
            vw.setReturnMsg("Status Update Failed.This Category Status Cannot Be Updated.");
            logger.info("Status Update Failed.This Category Status Cannot Be Updated.....");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error Category Update");
            logger.info("Error Category Update.....");
        }
        logger.info("Ready to Return Values......................");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }
}
