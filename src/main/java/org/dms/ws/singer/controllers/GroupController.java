/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.Group;

/**
 *
 * @author SDU
 */
public class GroupController {

    public static ResponseWrapper getGroups(String start, 
            String limit,
            String user_id, 
            String room, 
            String department, 
            String branch, 
            String countryCode, 
            String division, 
            String organaization,String system) {

        logger.info("get Groups Method Call...........");
        ResponseWrapper rw = null;
        String g = null;
        ArrayList<Group> gp = new ArrayList<>();
        try {
            logger.info("Call LDAP Get Groups.............");
            rw = LDAPWSController.getAllGroupsInSystem(start, limit,user_id,room,department,branch,countryCode,division,organaization,system);

        } catch (Exception ex) {
            logger.info("Error in get Groups Method :  " + ex.getMessage());
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return rw;
//        
//       
//        
//        ArrayList<Group> groupList = new ArrayList<>();
//        int totalrecode = 0;
//        try{
//            String GroupID = ((groupid==0)? "" : "AND GROUP_ID = '"+groupid+"' "); 
//            String GroupName = (groupname.equalsIgnoreCase("all") ? "NVL(GROUP_NAME,'AA') = NVL(GROUP_NAME,'AA') " : "UPPER(GROUP_NAME) LIKE UPPER('%"+groupname+"%')");
//            String Status = (status==0) ? "" : "AND STATUS = '"+status+"' ";
//            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(REMARKS,'AA') = NVL(REMARKS,'AA') " : "UPPER(REMARKS) LIKE UPPER('%"+remarks+"%')");
//            
//            
//            String selectColumn = "GROUP_ID,GROUP_NAME,STATUS,REMARKS";
//            
//            String whereClous = " "+GroupName+" "
//                    + ""+GroupID+" "
//                    + "AND "+Remarks+" "
//                    + ""+Status+"";
//                     
//            ResultSet rs = dbCon.search(con,"SELECT * FROM (SELECT (SELECT COUNT(*)FROM SD_GROUP_FUNCTIONS WHERE "+whereClous+") CNT,"
//                    + ""+selectColumn+",ROW_NUMBER() "
//                    + "OVER (ORDER BY GROUP_ID) RN "
//                    + "FROM SD_GROUP_FUNCTIONS "
//                    + "WHERE "+whereClous+" ) WHERE RN BETWEEN "+start+"+1 AND "+(start+limit)+" ORDER BY RN ");
//            
//            logger.info("Get Data to ResultSet...........");
//            while(rs.next()){
//                Group gp = new Group();
//                totalrecode = rs.getInt("CNT");
//                gp.setGroupId(rs.getString("GROUP_ID"));
//                gp.setGroupName(rs.getString("GROUP_NAME"));
//                //gp.setS(rs.getInt("STATUS"));
//                //gp.setRemarks(rs.getString("REMARKS"));
//                groupList.add(gp);
//            }
//            
//        }catch(Exception ex){
//            logger.info("Error Retriview Groups......:"+ex.toString());        
//        }
//        MessageWrapper mw = new MessageWrapper();
//        mw.setTotalRecords(totalrecode);
//        mw.setData(groupList);
//       logger.info("Ready to Return Values.................");
//       dbCon.ConectionClose(con);
//       logger.info("---------------------------------------------------");
//       return mw;
    }

}
