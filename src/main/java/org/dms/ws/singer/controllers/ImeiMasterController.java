/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.DeviceIssueIme;
import org.dms.ws.singer.entities.ImeiMaster;
import org.dms.ws.singer.entities.QuickImeiMaster;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class ImeiMasterController {

    public static MessageWrapper getImeiMasterDetailsVerify(String imeino,
            int modleno,
            int bisid,
            String debitnoteno,
            String purchasedate,
            String brand,
            String product,
            String location,
            double saleprice,
            int status,
            String modeldesc,
            String order,
            String type,
            String erpPartCode,
            int start,
            int limit) {
        logger.info("getImeiMasterDetails Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.imeiMaster(order);

            String imeiNo = (imeino.equalsIgnoreCase("all") ? "NVL(M.IMEI_NO,'AA') = NVL(M.IMEI_NO,'AA') " : "UPPER(M.IMEI_NO) = UPPER('" + imeino + "')");
            String modleNo = ((modleno == 0) ? "" : "AND M.MODEL_NO = '" + modleno + "' ");
            String bisId = ((bisid == 0) ? "" : "AND M.BIS_ID = '" + bisid + "' ");
            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(M.DEBIT_NOTE_NO,'AA') = NVL(M.DEBIT_NOTE_NO,'AA') " : "UPPER(M.DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String purchaseDate = (purchasedate.equalsIgnoreCase("all") ? "NVL(M.PURCHASE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(M.PURCHASE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "M.PURCHASE_DATE = TO_DATE('%" + purchasedate + "%','YYYY/MM/DD')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(M.BRAND,'AA') = NVL(M.BRAND,'AA') " : "UPPER(M.BRAND) LIKE UPPER('%" + brand + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(M.PRODUCT,'AA') = NVL(M.PRODUCT,'AA') " : "UPPER(M.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Location = (location.equalsIgnoreCase("all") ? "NVL(M.LOCATION,'AA') = NVL(M.LOCATION,'AA') " : "UPPER(M.LOCATION) LIKE UPPER('%" + location + "%')");
            String salesPrice = ((saleprice == 0) ? "" : "AND M.SALES_PRICE = '" + saleprice + "' ");
            String Status = (status == 0) ? "" : "AND M.STATUS = '" + status + "' ";
            String modelDesc = (modeldesc.equalsIgnoreCase("all") ? "NVL(L.MODEL_DESCRIPTION,'AA') = NVL(L.MODEL_DESCRIPTION,'AA') " : "UPPER(L.MODEL_DESCRIPTION) LIKE UPPER('%" + modeldesc + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY M.IMEI_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                    + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                    + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,L.ERP_PART_NO,"
                    + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,M.NET_PRICE";

            String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                    + "AND M.MODEL_NO = L.MODEL_NO (+)"
                    + "AND " + imeiNo + " "
                    + " " + modleNo + " "
                    + " " + bisId + " "
                    + "AND " + debitNoteNo + " "
                    + "AND " + purchaseDate + " "
                    + "AND " + Brand + " "
                    + "AND " + Product + " "
                    + "AND " + Location + " "
                    + " " + salesPrice + " "
                    + "" + Status + " "
                    + "AND " + modelDesc + "";

            String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ");

            String query = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ";

            System.out.println("XXXXXXXXXXXXXXX");
            System.out.println("QUERY >>>>>" + query);
            //   System.out.println(rs);
            System.out.println("XXXXXXXXXXXXXXX");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();
                totalrecode = rs.getInt("CNT");
                im.setImeiNo(rs.getString("IMEI_NO"));
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setBisId(rs.getInt("BIS_ID"));
                im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                im.setBrand(rs.getString("BRAND"));
                im.setProduct(rs.getString("PRODUCT"));
                im.setLocation(rs.getString("LOCATION"));
                im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                im.setStatus(rs.getInt("STATUS"));
                im.setBisName(rs.getString("BIS_NAME"));
                im.setErpCode(rs.getString("ERP_PART_NO"));
                im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                im.setNetPrice(rs.getDouble("NET_PRICE"));
                imeiMasterList.add(im);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetails  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiMasterDetails(String imeino,
            int modleno,
            int bisid,
            String debitnoteno,
            String purchasedate,
            String brand,
            String product,
            String location,
            double saleprice,
            int status,
            String modeldesc,
            String order,
            String type,
            String erpPartCode,
            int start,
            int limit) {
        logger.info("getImeiMasterDetails Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.imeiMaster(order);

            String imeiNo = (imeino.equalsIgnoreCase("all") ? "NVL(M.IMEI_NO,'AA') = NVL(M.IMEI_NO,'AA') " : "UPPER(M.IMEI_NO) LIKE UPPER('%" + imeino + "%')");
            String modleNo = ((modleno == 0) ? "" : "AND M.MODEL_NO = '" + modleno + "' ");
            String bisId = ((bisid == 0) ? "" : "AND M.BIS_ID = '" + bisid + "' ");
            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(M.DEBIT_NOTE_NO,'AA') = NVL(M.DEBIT_NOTE_NO,'AA') " : "UPPER(M.DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String purchaseDate = (purchasedate.equalsIgnoreCase("all") ? "NVL(M.PURCHASE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(M.PURCHASE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "M.PURCHASE_DATE = TO_DATE('%" + purchasedate + "%','YYYY/MM/DD')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(M.BRAND,'AA') = NVL(M.BRAND,'AA') " : "UPPER(M.BRAND) LIKE UPPER('%" + brand + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(M.PRODUCT,'AA') = NVL(M.PRODUCT,'AA') " : "UPPER(M.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Location = (location.equalsIgnoreCase("all") ? "NVL(M.LOCATION,'AA') = NVL(M.LOCATION,'AA') " : "UPPER(M.LOCATION) LIKE UPPER('%" + location + "%')");
            String salesPrice = ((saleprice == 0) ? "" : "AND M.SALES_PRICE = '" + saleprice + "' ");
            String Status = (status == 0) ? "" : "AND M.STATUS = '" + status + "' ";
            String modelDesc = (modeldesc.equalsIgnoreCase("all") ? "NVL(L.MODEL_DESCRIPTION,'AA') = NVL(L.MODEL_DESCRIPTION,'AA') " : "UPPER(L.MODEL_DESCRIPTION) LIKE UPPER('%" + modeldesc + "%')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY M.IMEI_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                    + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                    + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,L.ERP_PART_NO,"
                    + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,M.NET_PRICE";

            String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                    + "AND M.MODEL_NO = L.MODEL_NO (+)"
                    + "AND " + imeiNo + " "
                    + " " + modleNo + " "
                    + " " + bisId + " "
                    + "AND " + debitNoteNo + " "
                    + "AND " + purchaseDate + " "
                    + "AND " + Brand + " "
                    + "AND " + Product + " "
                    + "AND " + Location + " "
                    + " " + salesPrice + " "
                    + "" + Status + " "
                    + "AND " + modelDesc + "";

            String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ");

            String query = "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + table + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ";

            System.out.println("XXXXXXXXXXXXXXX");
            System.out.println("QUERY >>>>>" + query);
            //   System.out.println(rs);
            System.out.println("XXXXXXXXXXXXXXX");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();
                totalrecode = rs.getInt("CNT");
                im.setImeiNo(rs.getString("IMEI_NO"));
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setBisId(rs.getInt("BIS_ID"));
                im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                im.setBrand(rs.getString("BRAND"));
                im.setProduct(rs.getString("PRODUCT"));
                im.setLocation(rs.getString("LOCATION"));
                im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                im.setStatus(rs.getInt("STATUS"));
                im.setBisName(rs.getString("BIS_NAME"));
                im.setErpCode(rs.getString("ERP_PART_NO"));
                im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                im.setNetPrice(rs.getDouble("NET_PRICE"));
                imeiMasterList.add(im);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetails  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper deactivateImeiMaster(ImeiMaster imeimaster_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("deactivateImeiMaster Method Call............");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_ime = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET STATUS=9,"
                    + "USER_MODIFIED=?,"
                    + "DATE_MODIFIED=SYSDATE "
                    + "WHERE IMEI_NO=?");

            ps_up_ime.setString(1, user_id);
            ps_up_ime.setString(2, imeimaster_info.getImeiNo());
            ps_up_ime.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in deactivateImeiMaster Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Deactivate Successfully in IMEI Master");
            logger.info("Deactivate Successsfully in ImeiMaster............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Deactivate IMEI Master");
            logger.info("Getting Error Deactivate ImeiMaster..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper activateImeiMaster(ImeiMaster imeimaster_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        logger.info("activateImeiMaster Method Call............");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            PreparedStatement ps_up_ime = dbCon.prepare(con, "UPDATE SD_DEVICE_MASTER SET STATUS=1,"
                    + "USER_MODIFIED=?,"
                    + "DATE_MODIFIED=SYSDATE "
                    + "WHERE IMEI_NO=?");

            ps_up_ime.setString(1, user_id);
            ps_up_ime.setString(2, imeimaster_info.getImeiNo());
            ps_up_ime.executeUpdate();
            result = 1;

        } catch (Exception ex) {
            logger.info("Error in deactivateImeiMaster Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Activate Successfully in IMEI Master");
            logger.info("Activate Successsfully in ImeiMaster............");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Activate IMEI Master");
            logger.info("Getting Error Activate ImeiMaster..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getQuickImeiMasterDetails(String imeino,
            int modleno,
            int bisid,
            String debitnoteno,
            String purchasedate,
            String brand,
            String product,
            String location,
            double saleprice,
            int status,
            String order,
            String type,
            int start,
            int limit) {
        logger.info("getImeiMasterDetails Method Call...........");

        ArrayList<QuickImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            order = DBMapper.imeiMaster(order);

            String imeiNo = (imeino.equalsIgnoreCase("all") ? "NVL(D.IMEI_NO,'AA') = NVL(D.IMEI_NO,'AA') " : "D.IMEI_NO  ='" + imeino + "'");
            String modleNo = ((modleno == 0) ? "" : "AND D.MODEL_NO = '" + modleno + "' ");
            String bisId = ((bisid == 0) ? "" : "AND D.BIS_ID = '" + bisid + "' ");
            String debitNoteNo = (debitnoteno.equalsIgnoreCase("all") ? "NVL(D.DEBIT_NOTE_NO,'AA') = NVL(D.DEBIT_NOTE_NO,'AA') " : "UPPER(D.DEBIT_NOTE_NO) LIKE UPPER('%" + debitnoteno + "%')");
            String purchaseDate = (purchasedate.equalsIgnoreCase("all") ? "NVL(D.PURCHASE_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(D.PURCHASE_DATE,TO_DATE('20100101','YYYYMMDD')) " : "D.PURCHASE_DATE = TO_DATE('%" + purchasedate + "%','YYYY/MM/DD')");
            String Brand = (brand.equalsIgnoreCase("all") ? "NVL(D.BRAND,'AA') = NVL(D.BRAND,'AA') " : "UPPER(D.BRAND) LIKE UPPER('%" + brand + "%')");
            String Product = (product.equalsIgnoreCase("all") ? "NVL(D.PRODUCT,'AA') = NVL(D.PRODUCT,'AA') " : "UPPER(D.PRODUCT) LIKE UPPER('%" + product + "%')");
            String Location = (location.equalsIgnoreCase("all") ? "NVL(D.LOCATION,'AA') = NVL(D.LOCATION,'AA') " : "UPPER(D.LOCATION) LIKE UPPER('%" + location + "%')");
            String salesPrice = ((saleprice == 0) ? "" : "AND D.SALES_PRICE = '" + saleprice + "' ");
            String Status = (status == 0) ? "" : "AND D.STATUS = '" + status + "' ";
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY D.IMEI_NO DESC" : "ORDER BY " + order + "   " + type + "");

            String selectColumn = "D.IMEI_NO,"
                    + "D.MODEL_NO,"
                    + "D.BIS_ID,"
                    + "D.DEBIT_NOTE_NO,"
                    + "D.PURCHASE_DATE,"
                    + "D.BRAND,"
                    + "D.PRODUCT,"
                    + "D.LOCATION,"
                    + "D.SALES_PRICE,"
                    + "D.STATUS,"
                    + "M.MODEL_DESCRIPTION,"
                    + "B.BIS_NAME";

            String whereClous = "D.MODEL_NO = M.MODEL_NO (+)  "
                    + "AND D.BIS_ID = B.BIS_ID (+)"
                    + " AND " + imeiNo + " "
                    + " " + modleNo + " "
                    + " " + bisId + " "
                    + " AND " + debitNoteNo + " "
                    + " AND " + purchaseDate + " "
                    + " AND " + Brand + " "
                    + " AND " + Product + " "
                    + " AND " + Location + " "
                    + " " + salesPrice + " "
                    + "" + Status + "";

            String tables = "SD_DEVICE_MASTER D,SD_MODEL_LISTS M,SD_BUSINESS_STRUCTURES B";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*)FROM " + tables + " WHERE " + whereClous + ") CNT,"
                    + "" + selectColumn + ",ROW_NUMBER() "
                    + "OVER (" + Order + ") RN "
                    + "FROM " + tables + " "
                    + "WHERE " + whereClous + " ) WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                QuickImeiMaster im = new QuickImeiMaster();
                totalrecode = rs.getInt("CNT");
                im.setImeiNo(rs.getString("IMEI_NO"));
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setProduct(rs.getString("PRODUCT"));
                im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                im.setLocation(rs.getString("LOCATION"));
                im.setBisId(rs.getInt("BIS_ID"));
                im.setBisName(rs.getString("BIS_NAME"));
                imeiMasterList.add(im);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiMasterDetails  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper validateImeiInLocation(String imei, int bis_id) {
        logger.info("validateImeiInLocation Method Call............");
        int result = 0;
        int imei_cnt = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ResultSet rs = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER "
                    + "WHERE IMEI_NO = '" + imei + "' "
                    + "AND BIS_ID = " + bis_id + "");
            while (rs.next()) {
                imei_cnt = rs.getInt("CNT");
            }
            if (imei_cnt > 0) {
                result = 1;
            } else {
                result = 2;
            }

        } catch (Exception ex) {
            logger.info("Error in validateImeiInLocation Method.. :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("This IMEI in Your Location");
            logger.info("This IMEI in Your Location............");
        } else if (result == 2) {
            vw.setReturnFlag("2");
            vw.setReturnMsg("This IMEI Can't Allow");
            logger.info("This IMEI Can't Allow..........");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Getting Error Deactivate IMEI Master");
            logger.info("Getting Error Deactivate ImeiMaster..........");
        }
        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getImeiDetailsForIssue(String imeino, String bisId) {
        logger.info("getImeiDetails Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        MessageWrapper mw = new MessageWrapper();
        
        String imeiNumber = imeino;

        try {

            String imeiNo = (imeino.equalsIgnoreCase("all") ? "" : " AND M.IMEI_NO = '" + imeino + "'");

            String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                    + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                    + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                    + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,"
                    + "TO_CHAR(W.REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE";

            String whereClous = "";

            if (bisId.equals("all")) {

                whereClous = " M.BIS_ID = B.BIS_ID (+) "
                        + "AND M.MODEL_NO = L.MODEL_NO (+) "
                        + "AND M.IMEI_NO = W.IMEI_NO (+) "
                        + "" + imeiNo + "";
                
                System.out.println("All clause");
            } else {

                whereClous = " M.BIS_ID = B.BIS_ID (+) "
                        + "AND M.MODEL_NO = L.MODEL_NO (+) "
                        + "AND M.BIS_ID='" + bisId + "'"
                        + "AND M.IMEI_NO = W.IMEI_NO (+) "
                        + "" + imeiNo + "";
                
                   System.out.println("BIS id clause");
            }

            System.out.println(">>>>>>>>>> " + bisId);

            String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L,SD_WARRENTY_REGISTRATION W";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();

                im.setImeiNo(rs.getString("IMEI_NO"));
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setBisId(rs.getInt("BIS_ID"));
                im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                im.setBrand(rs.getString("BRAND"));
                im.setProduct(rs.getString("PRODUCT"));
                im.setLocation(rs.getString("LOCATION"));
                im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                im.setStatus(rs.getInt("STATUS"));
                im.setBisName(rs.getString("BIS_NAME"));
                im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                im.setPurchaseDate(rs.getString("REGISTER_DATE"));
                imeiMasterList.add(im);
            }
             totalrecode = imeiMasterList.size();
            
            String qryForCheckReturn = "SELECT STATUS "
                    + "FROM SD_DEVICE_RETURN_DTL "
                    + "WHERE IMEI_NO='"+imeiNumber+"'";
            
            System.out.println("QRY CHK RETURN "+qryForCheckReturn);
            
            ResultSet rsReturn = dbCon.search(con, qryForCheckReturn);
            
            int statusMsg = 0;
            
            while (rsReturn.next()) {                
                statusMsg = rsReturn.getInt("STATUS");
            }
            
            
            
            if(statusMsg == 1){
                
                System.out.println("pending return");
                
                totalrecode = 50;
            }

        } catch (Exception ex) {
            

            System.out.println("EEEEEEEEEEEEEEEe " + con);

               ex.printStackTrace();
            logger.info("Error in getImeiDetails  " + ex.getMessage());
        }

       

        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }

        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiDetails(String imeino, String bisId) {
        logger.info("getImeiDetails Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        MessageWrapper mw = new MessageWrapper();

        try {

            String imeiNo = (imeino.equalsIgnoreCase("all") ? "" : " AND M.IMEI_NO = '" + imeino + "'");

            String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                    + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                    + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                    + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,"
                    + "TO_CHAR(W.REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE";

            String whereClous = "";

            if (bisId.equals("all")) {

                whereClous = " M.BIS_ID = B.BIS_ID (+) "
                        + "AND M.MODEL_NO = L.MODEL_NO (+) "
                        + "AND M.IMEI_NO = W.IMEI_NO (+) "
                        + "" + imeiNo + "";
                
                System.out.println("All clause");
            } else {

                whereClous = " M.BIS_ID = B.BIS_ID (+) "
                        + "AND M.MODEL_NO = L.MODEL_NO (+) "
                        + "AND M.BIS_ID='" + bisId + "'"
                        + "AND M.IMEI_NO = W.IMEI_NO (+) "
                        + "" + imeiNo + "";
                
                   System.out.println("BIS id clause");
            }

            System.out.println(">>>>>>>>>> " + bisId);

            String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L,SD_WARRENTY_REGISTRATION W";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                    + "FROM " + table + " "
                    + "WHERE " + whereClous + " ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();

                im.setImeiNo(rs.getString("IMEI_NO"));
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setBisId(rs.getInt("BIS_ID"));
                im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                im.setBrand(rs.getString("BRAND"));
                im.setProduct(rs.getString("PRODUCT"));
                im.setLocation(rs.getString("LOCATION"));
                im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                im.setStatus(rs.getInt("STATUS"));
                im.setBisName(rs.getString("BIS_NAME"));
                im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                im.setPurchaseDate(rs.getString("REGISTER_DATE"));
                imeiMasterList.add(im);
            }

        } catch (Exception ex) {

            System.out.println("EEEEEEEEEEEEEEEe " + con);

            //   ex.printStackTrace();
            logger.info("Error in getImeiDetails  " + ex.getMessage());
        }

        totalrecode = imeiMasterList.size();

        if (con == null) {
            mw.setTotalRecords(1000000);
        } else {
            mw.setTotalRecords(totalrecode);
        }

        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiValidationIssueInventory(String imei, int bis_id) {
        logger.info("getImeiValidationIssueInventory Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            int location_cnt = 0;

            ResultSet rs_master = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_MASTER  "
                    + " WHERE BIS_ID = " + bis_id + " "
                    + " AND IMEI_NO = '" + imei + "'");

            while (rs_master.next()) {
                location_cnt = rs_master.getInt("CNT");
            }
            //rs_master.close();
            logger.info("Check IMEI Location....");

            if (location_cnt > 0) {
                String imeiNo = " AND M.IMEI_NO = '" + imei + "'";

                String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                        + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                        + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                        + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,"
                        + "TO_CHAR(W.REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE";

                String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                        + "AND M.MODEL_NO = L.MODEL_NO (+) "
                        + "AND M.IMEI_NO = W.IMEI_NO (+) "
                        + "" + imeiNo + "";

                String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L,SD_WARRENTY_REGISTRATION W";

                ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                        + "FROM " + table + " "
                        + "WHERE " + whereClous + " ");

                logger.info("Get Data to ResultSet...........");
                while (rs.next()) {
                    ImeiMaster im = new ImeiMaster();

                    im.setImeiNo(rs.getString("IMEI_NO"));
                    im.setModleNo(rs.getInt("MODEL_NO"));
                    im.setBisId(rs.getInt("BIS_ID"));
                    im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                    im.setBrand(rs.getString("BRAND"));
                    im.setProduct(rs.getString("PRODUCT"));
                    im.setLocation(rs.getString("LOCATION"));
                    im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                    im.setStatus(rs.getInt("STATUS"));
                    im.setBisName(rs.getString("BIS_NAME"));
                    im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    im.setPurchaseDate(rs.getString("REGISTER_DATE"));
                    imeiMasterList.add(im);
                }
            } else {

                totalrecode = 0;

            }

        } catch (Exception ex) {
            logger.info("Error in getImeiValidationIssueInventory  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = imeiMasterList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiValidationAcceptInventory_org(String imei, int bis_id) {
        logger.info("getImeiValidationAcceptInventory Method Call...........");

        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            int location_cnt = 0;

            String cntImei = " ";
            if (imei.length() == 4) {
                cntImei = " AND SUBSTR(IMEI_NO, (LENGTH(IMEI_NO)-3), LENGTH(IMEI_NO)) = '" + imei + "' ";
            } else if (imei.length() == 6) {
                cntImei = " AND SUBSTR(IMEI_NO, (LENGTH(IMEI_NO)-5), LENGTH(IMEI_NO)) = '" + imei + "' ";
            } else {
                cntImei = " AND IMEI_NO = '" + imei + "' ";
            }

            ResultSet rs_master = dbCon.search(con, "SELECT COUNT(*) CNT "
                    + "FROM SD_DEVICE_ISSUE_DTL "
                    + "WHERE ISSUE_NO IN (SELECT ISSUE_NO FROM SD_DEVICE_ISSUE WHERE DSR_ID = " + bis_id + ") "
                    + " " + cntImei + " ");

            while (rs_master.next()) {
                location_cnt = rs_master.getInt("CNT");
            }
            rs_master.close();
            logger.info("Check IMEI Location....");

            if (location_cnt > 0) {
                // String imeiNo = " AND M.IMEI_NO = '" + imei + "'";
                String imeiNo = " ";

                if (imei.length() == 4) {
                    imeiNo = "  AND SUBSTR(M.IMEI_NO, (LENGTH(M.IMEI_NO)-3), LENGTH(M.IMEI_NO)) = '" + imei + "'    ";
                } else if (imei.length() == 6) {
                    imeiNo = "  AND SUBSTR(M.IMEI_NO, (LENGTH(M.IMEI_NO)-5), LENGTH(M.IMEI_NO)) = '" + imei + "'    ";
                } else {
                    imeiNo = " AND M.IMEI_NO = '" + imei + "'";
                }

                String selectColumn = "M.IMEI_NO,M.MODEL_NO,M.BIS_ID,"
                        + "M.DEBIT_NOTE_NO,M.PURCHASE_DATE,"
                        + "M.BRAND,M.PRODUCT,M.LOCATION,M.SALES_PRICE,"
                        + "M.STATUS,B.BIS_NAME,L.MODEL_DESCRIPTION,"
                        + "TO_CHAR(W.REGISTER_DATE,'YYYY/MM/DD') REGISTER_DATE";

                String whereClous = " M.BIS_ID = B.BIS_ID (+) "
                        + "AND M.MODEL_NO = L.MODEL_NO (+) "
                        + "AND M.IMEI_NO = W.IMEI_NO (+) "
                        + " " + imeiNo + " ";

                String table = "SD_DEVICE_MASTER M,SD_BUSINESS_STRUCTURES B,SD_MODEL_LISTS L,SD_WARRENTY_REGISTRATION W";

                ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                        + "FROM " + table + " "
                        + "WHERE " + whereClous + " ");

                logger.info("Get Data to ResultSet...........");
                while (rs.next()) {
                    ImeiMaster im = new ImeiMaster();

                    im.setImeiNo(rs.getString("IMEI_NO"));
                    im.setModleNo(rs.getInt("MODEL_NO"));
                    im.setBisId(rs.getInt("BIS_ID"));
                    im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                    im.setBrand(rs.getString("BRAND"));
                    im.setProduct(rs.getString("PRODUCT"));
                    im.setLocation(rs.getString("LOCATION"));
                    im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                    im.setStatus(rs.getInt("STATUS"));
                    im.setBisName(rs.getString("BIS_NAME"));
                    im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                    im.setPurchaseDate(rs.getString("REGISTER_DATE"));
                    imeiMasterList.add(im);
                }
            } else {

                totalrecode = 0;

            }

        } catch (Exception ex) {
            logger.info("Error in getImeiValidationAcceptInventory  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = imeiMasterList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiDetailsAccept(String imeino) {
        logger.info("getImeiDetailsAccept Method Call...........");
        ArrayList<ImeiMaster> imeiMasterList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "D.IMEI_NO,"
                    + "D.SALES_PRICE,"
                    + "D.MODEL_NO,"
                    + "M.MODEL_DESCRIPTION,"
                    + "D.DEBIT_NOTE_NO,"
                    + "M.PART_PRD_FAMILY_DESC";

            String whereClous = "D.MODEL_NO = M.MODEL_NO (+) "
                    + "AND D.IMEI_NO = '" + imeino + "'";

            String table = "SD_DEBIT_NOTE_DETAIL D,SD_MODEL_LISTS M";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " FROM " + table + " WHERE " + whereClous + " ");

            logger.info("Get Data to ResultSet...........");
            while (rs.next()) {
                ImeiMaster im = new ImeiMaster();
                im.setImeiNo(rs.getString("IMEI_NO"));
                im.setSalesPrice(rs.getDouble("SALES_PRICE"));
                im.setDebitnoteNo(rs.getString("DEBIT_NOTE_NO"));
                im.setModleNo(rs.getInt("MODEL_NO"));
                im.setModleDesc(rs.getString("MODEL_DESCRIPTION"));
                im.setProduct(rs.getString("PART_PRD_FAMILY_DESC"));
                imeiMasterList.add(im);
            }

        } catch (Exception ex) {
            logger.info("Error in getImeiDetailsAccept  " + ex.getMessage());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = imeiMasterList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(imeiMasterList);
        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;

    }

    public static MessageWrapper getImeiValidationAcceptInventory(String imei, int bis_id, int status) {
        logger.info("getInventoryIssueDetail Method Call...........");
        ArrayList<DeviceIssueIme> deviceImeList = new ArrayList<>();

        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int issue_vali = 0;
        try {

            String statusW = "";
            if (status == 0) {
                statusW = " AND   I.STATUS=3 ";
            } else {
                statusW = " AND   I.STATUS=" + status + " ";
            }

            String ImeColumn = " DISTINCT D.ISSUE_NO,"
                    + "D.SEQ_NO,"
                    + "D.IMEI_NO,"
                    + "D.MODEL_NO,"
                    + "D.STATUS,"
                    + "M.MODEL_DESCRIPTION, "
                    + "M.ERP_PART_NO, "
                    + "BD.BIS_ID   DISTRIBUTOR_CODE,  "
                    + "BD.BIS_NAME DISTRIBUTOR_NAME,  "
                    + "BD.ADDRESS DISTRIBUTOR_ADDRESS,  "
                    + "BD.CONTACT_NO DISTRIBUTOR_PHONE,  "
                    + "BS.BIS_ID   SUBDEALER_CODE,  "
                    + "BS.BIS_NAME SUBDEALER_NAME,  "
                    + "BS.ADDRESS SUBDEALER_ADDRESS,  "
                    + "BS.CONTACT_NO SUBDEALER_PHONE,  "
                    + "NVL(M.SELLING_PRICE, 0) AS PRICE     ";

            //    System.out.println("qry::: "+"SELECT " + ImeColumn + " FROM SD_MODEL_LISTS M, SD_DEVICE_ISSUE I, SD_DEVICE_ISSUE_DTL D, SD_BUSINESS_STRUCTURES BD, SD_BIS_STRU_TYPES BSD, SD_BUSINESS_STRUCTURES BS, SD_BIS_STRU_TYPES BSS WHERE " + whereClous + " ");
            String tables = " SD_DEVICE_MASTER DM, "
                    + " SD_DEVICE_ISSUE_DTL D,  "
                    + " SD_DEVICE_ISSUE I,  "
                    + " SD_MODEL_LISTS M,   "
                    + " SD_BUSINESS_STRUCTURES BD,  "
                    + " SD_BIS_STRU_TYPES BSD,  "
                    + " SD_BUSINESS_STRUCTURES BS,  "
                    + " SD_BIS_STRU_TYPES BSS   ";

            String where = "    D.ISSUE_NO= I.ISSUE_NO    "
                    + " AND   D.IMEI_NO = DM.IMEI_NO    "
                    + " AND   D.MODEL_NO = M.MODEL_NO (+)   "
                    + " AND   I.DISTRIBUTER_ID=BD.BIS_ID    "
                    + " AND   BSD.BIS_STRU_TYPE_ID  = BD.BIS_STRU_TYPE_ID   "
                    + " AND   I.DSR_ID = BS.BIS_ID                         "
                    + " AND   BSS.BIS_STRU_TYPE_ID  = BS.BIS_STRU_TYPE_ID  "
                    + " AND   D.IMEI_NO LIKE '%" + imei + "%'   "
                    + "  " + statusW + " "
                    // + " AND   I.STATUS=3  "
                    + " AND   I.DSR_ID = " + bis_id + "      "
                    + " AND   DM.BIS_ID=" + bis_id + "  "
                    + " AND   I.ISSUE_NO  = (SELECT MAX(ISSUE_NO) FROM SD_DEVICE_ISSUE_DTL WHERE IMEI_NO = DM.IMEI_NO)  ";

            ResultSet rs_ime = dbCon.search(con, "SELECT " + ImeColumn + " FROM " + tables + " WHERE " + where + " ORDER BY D.MODEL_NO, D.ISSUE_NO ASC  ");

            logger.info("Get Data to ResultSet...........");

            while (rs_ime.next()) {
                DeviceIssueIme dim = new DeviceIssueIme();
                dim.setSeqNo(rs_ime.getInt("SEQ_NO"));
                dim.setIssueNo(rs_ime.getInt("ISSUE_NO"));
                dim.setImeiNo(rs_ime.getString("IMEI_NO"));
                dim.setModleNo(rs_ime.getInt("MODEL_NO"));
                dim.setStatus(rs_ime.getInt("STATUS"));
                dim.setModleDesc(rs_ime.getString("MODEL_DESCRIPTION"));
                dim.setErpModel(rs_ime.getString("ERP_PART_NO"));

                dim.setDistributorCode(rs_ime.getString("DISTRIBUTOR_CODE"));
                dim.setDistributorName(rs_ime.getString("DISTRIBUTOR_NAME"));
                dim.setDistributorAddress(rs_ime.getString("DISTRIBUTOR_ADDRESS"));
                dim.setDistributorTelephone(rs_ime.getString("DISTRIBUTOR_PHONE"));

                dim.setSubdealerCode(rs_ime.getString("SUBDEALER_CODE"));
                dim.setSubdealerName(rs_ime.getString("SUBDEALER_NAME"));
                dim.setSubdealerAddress(rs_ime.getString("SUBDEALER_ADDRESS"));
                dim.setSubdealerTelephone(rs_ime.getString("SUBDEALER_PHONE"));

                double salesPrice = rs_ime.getDouble("PRICE");
                //double realSalesPrice = (salesPrice/86)*90;
                double realSalesPrice = salesPrice * 0.9;
                dim.setSalesPrice(realSalesPrice);

                deviceImeList.add(dim);
            }
            // }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Retriview Device Issue......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = deviceImeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(deviceImeList);

        logger.info("Ready to Return Values.................");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------------");
        return mw;
    }

}
