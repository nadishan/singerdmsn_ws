/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BussinessStructure;
import org.dms.ws.singer.entities.UserTypeList;

/**
 *
 * @author SDU
 */
public class UserTypeListController {

    public static MessageWrapper getUserTypeList(String user_id, int bis_type) {

        logger.info("getUserTypeList Method Call....................");
        ArrayList<UserTypeList> userTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT U.BIS_ID, "
                    + "U.USER_ID, "
                    + "U.FIRST_NAME, "
                    + "U.LAST_NAME,"
                    + "B.BIS_NAME "
                    + " FROM SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B "
                    + " WHERE U.BIS_ID IN (SELECT BIS_ID FROM SD_BUSINESS_STRUCTURES B, "
                    + " SD_BIS_STRU_TYPES T "
                    + " WHERE PARENT_BIS_ID=(SELECT BIS_ID "
                    + " FROM SD_USER_BUSINESS_STRUCTURES "
                    + " WHERE USER_ID = '" + user_id + "') "
                    + " AND B.BIS_STRU_TYPE_ID =T.BIS_STRU_TYPE_ID "
                    + " AND T.BIS_STRU_TYPE_ID = " + bis_type + ") "
                    + " AND U.BIS_ID = B.BIS_ID (+) ");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                UserTypeList ut = new UserTypeList();
                ut.setBisId(rs.getInt("BIS_ID"));
                ut.setBisName(rs.getString("BIS_NAME"));
                ut.setUserId(rs.getString("USER_ID"));
                ut.setFirstName(rs.getString("FIRST_NAME"));
                ut.setLastName(rs.getString("LAST_NAME"));
                userTypeList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getUserTypeList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getUserTypeListLowUpper(String user_id, String bisName) {

        logger.info("getUserTypeListLowUpper Method Call....................");
        ArrayList<UserTypeList> userTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        int bis_type = 0;
        String bisNameW = "";
        try {
            ResultSet rs_bis_type = dbCon.search(con, "SELECT U.BIS_ID,B.BIS_STRU_TYPE_ID "
                    + "FROM SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B "
                    + "WHERE U.USER_ID = '"+user_id+"' "
                    + "AND U.BIS_ID = B.BIS_ID");

            while (rs_bis_type.next()) {
                bis_type = rs_bis_type.getInt("BIS_STRU_TYPE_ID");

            }
            if (bis_type == 1) {
                
                if(bisName.equalsIgnoreCase("all")){
                    bisNameW = " B.BIS_NAME = B.BIS_NAME ";
                }else{
                    bisNameW = " UPPER(B.BIS_NAME) LIKE '%"+bisName.toUpperCase()+"%' ";
                }
                                
                ResultSet rs = dbCon.search(con, "SELECT B.BIS_ID"
                        + ",B.BIS_NAME"
                        + ",B.BIS_DESC"
                        + ",B.BIS_STRU_TYPE_ID "
                        + ",B.ADDRESS       "
                        + ",B.CONTACT_NO    "
                        + ",B.PARENT_BIS_ID "
                        + ",B1.BIS_NAME AS PARENT_BIS_NAME  "
                        + "FROM SD_BUSINESS_STRUCTURES B, SD_BUSINESS_STRUCTURES B1  "
                        + "WHERE B.PARENT_BIS_ID IN (SELECT PARENT_BIS_ID "
                                                   + "FROM  SD_BUSINESS_STRUCTURES "
                                                  + "WHERE BIS_ID IN (SELECT BIS_ID "
                                                  + "FROM SD_USER_BUSINESS_STRUCTURES "
                                                  + "WHERE USER_ID = '"+user_id+"')) "
                        + " AND B.PARENT_BIS_ID=B1.BIS_ID    "
                        + " AND B.BIS_STRU_TYPE_ID = 2 "
                        + " AND B.STATUS = 1 "
                        + " AND "+bisNameW+"    "
                        + " ORDER BY B.BIS_NAME ASC ");

                logger.info("Get Data to Resultset...");
                while (rs.next()) {
                    UserTypeList ut = new UserTypeList();
                    ut.setBisId(rs.getInt("BIS_ID"));
                    ut.setBisName(rs.getString("BIS_NAME"));
                    ut.setUserId(rs.getString("BIS_NAME"));
                    ut.setFirstName(rs.getString("BIS_DESC"));
                    ut.setLastName(rs.getString("BIS_STRU_TYPE_ID"));
                    ut.setParent_bisId(rs.getInt("PARENT_BIS_ID"));
                    ut.setParent_bisName(rs.getString("PARENT_BIS_NAME"));
                    ut.setAddress(rs.getString("ADDRESS"));
                    ut.setTeleNumber(rs.getString("CONTACT_NO"));
                    
                    userTypeList.add(ut);
                }
            } else if (bis_type == 2) {
                
                 if(bisName.equalsIgnoreCase("all")){
                    bisNameW = " B.BIS_NAME = B.BIS_NAME ";
                }else{
                    bisNameW = " UPPER(B.BIS_NAME) LIKE '%"+bisName.toUpperCase()+"%' ";
                }
                
                ResultSet rs = dbCon.search(con, "SELECT B.BIS_ID,  "
                        + "B.BIS_NAME,"
                        + "B.BIS_DESC,"
                        + "B.BIS_STRU_TYPE_ID, "
                        + "B.ADDRESS,       "
                        + "B.CONTACT_NO, "
                        + " B.PARENT_BIS_ID,    "
                        + " B1.BIS_NAME AS PARENT_BIS_NAME  "
                        + "FROM SD_BUSINESS_STRUCTURES B, SD_BUSINESS_STRUCTURES B1   "
                        + "WHERE B.PARENT_BIS_ID IN (SELECT PARENT_BIS_ID "
                        + "FROM SD_BUSINESS_STRUCTURES "
                        + "WHERE BIS_ID=(SELECT BIS_ID "
                        + "FROM SD_USER_BUSINESS_STRUCTURES "
                        + "WHERE USER_ID = '"+user_id+"')) "
                        + "AND    B.PARENT_BIS_ID=B1.BIS_ID "
                        + "AND  B.BIS_STRU_TYPE_ID = 1  "
                        + "AND B.STATUS = 1 "
                        + " AND "+bisNameW+" "
                        + " ORDER BY B.BIS_NAME ASC ");

                logger.info("Get Data to Resultset...");
                while (rs.next()) {
                    UserTypeList ut = new UserTypeList();
                    ut.setBisId(rs.getInt("BIS_ID"));
                    ut.setBisName(rs.getString("BIS_NAME"));
                    ut.setUserId(rs.getString("BIS_NAME"));
                    ut.setFirstName(rs.getString("BIS_DESC"));
                    ut.setLastName(rs.getString("BIS_STRU_TYPE_ID"));
                    ut.setAddress(rs.getString("ADDRESS"));
                    ut.setTeleNumber(rs.getString("CONTACT_NO"));
                    ut.setParent_bisId(rs.getInt("PARENT_BIS_ID"));
                    ut.setParent_bisName(rs.getString("PARENT_BIS_NAME"));
                    userTypeList.add(ut);
                }
            }
        } catch (Exception ex) {
            logger.info("Error getUserTypeListLowUpper Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getBussinessTypeList(int bis_type) {

        logger.info("getBussinessTypeList Method Call....................");
        ArrayList<BussinessStructure> bisTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT (SELECT COUNT(*) FROM SD_BUSINESS_STRUCTURES WHERE BIS_STRU_TYPE_ID =" + bis_type + ") CNT,"
                    + "BIS_ID,"
                    + "BIS_NAME,"
                    + "BIS_DESC  "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE BIS_STRU_TYPE_ID =" + bis_type + " "
                    + "AND STATUS = 1");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                BussinessStructure ut = new BussinessStructure();
                totalrecode = rs.getInt("CNT");
                ut.setBisId(rs.getInt("BIS_ID"));
                ut.setBisName(rs.getString("BIS_NAME"));
                ut.setBisDesc(rs.getString("BIS_DESC"));
                bisTypeList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getBussinessTypeList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(bisTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getLowLevelBussinessList(int bis_type, int bis_id) {

        logger.info("getBussinessTypeList Method Call....................");
        ArrayList<BussinessStructure> bisTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String whereClous = "BIS_STRU_TYPE_ID =" + bis_type + " AND PARENT_BIS_ID = " + bis_id + "";

            ResultSet rs = dbCon.search(con, "SELECT (SELECT COUNT(*) FROM SD_BUSINESS_STRUCTURES WHERE " + whereClous + ") CNT,"
                    + "BIS_ID,"
                    + "BIS_NAME,"
                    + "BIS_DESC,"
                    + "LONGITUDE,"
                    + "LATITUDE,ADDRESS,CONTACT_NO  "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE " + whereClous + "");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                BussinessStructure ut = new BussinessStructure();
                totalrecode = rs.getInt("CNT");
                ut.setBisId(rs.getInt("BIS_ID"));
                ut.setBisName(rs.getString("BIS_NAME"));
                ut.setBisDesc(rs.getString("BIS_DESC"));
                ut.setLogitude(rs.getString("LONGITUDE"));
                ut.setLatitude(rs.getString("LATITUDE"));
                ut.setAddress(rs.getString("ADDRESS"));
                ut.setTeleNumber(rs.getString("CONTACT_NO"));
                bisTypeList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getBussinessTypeList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(bisTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getBussinessAvilableTypeList(int bis_id, int bis_type) {

        logger.info("getBussinessAvilableTypeList Method Call....................");
        ArrayList<UserTypeList> userTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT BIS_ID,"
                    + "USER_ID,"
                    + "FIRST_NAME,"
                    + "LAST_NAME "
                    + "FROM SD_USER_BUSINESS_STRUCTURES "
                    + "WHERE BIS_ID IN (SELECT BIS_ID "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE BIS_STRU_TYPE_ID =" + bis_type + " "
                    + "AND PARENT_BIS_ID = " + bis_id + ")");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                UserTypeList ut = new UserTypeList();
                ut.setBisId(rs.getInt("BIS_ID"));
                ut.setUserId(rs.getString("USER_ID"));
                ut.setFirstName(rs.getString("FIRST_NAME"));
                ut.setLastName(rs.getString("LAST_NAME"));
                userTypeList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getBussinessAvilableTypeList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getUpperLevelList(int bis_id, int bis_type) {
        logger.info("getUpperLevelList Method Call....................");
        ArrayList<UserTypeList> userTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT BIS_ID,"
                    + "BIS_NAME,"
                    + "BIS_DESC,"
                    + "LONGITUDE,"
                    + "LATITUDE,"
                    + "ADDRESS,"
                    + "CONTACT_NO "
                    + "FROM SD_BUSINESS_STRUCTURES  "
                    + "WHERE BIS_STRU_TYPE_ID = " + bis_type + " "
                    + "AND STATUS = 1 "
                    + "AND BIS_ID IN (SELECT PARENT_BIS_ID "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE  BIS_ID = " + bis_id + ") ");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                UserTypeList ut = new UserTypeList();
                ut.setBisId(rs.getInt("BIS_ID"));
                ut.setBisName(rs.getString("BIS_NAME"));
                ut.setBisDesc(rs.getString("BIS_DESC"));
                ut.setLongitude(rs.getString("LONGITUDE"));
                ut.setLatitude(rs.getString("LATITUDE"));
                ut.setAddress(rs.getString("ADDRESS"));
                ut.setTeleNumber(rs.getString("CONTACT_NO"));
                userTypeList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getUpperLevelList Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = userTypeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(userTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getBusinessDownList(int bisId, int bisTypeId) {

        logger.info("getBusinessDownList Method Call....................");
        ArrayList<UserTypeList> userList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "  U.USER_ID, U.BIS_ID,U.FIRST_NAME,U.LAST_NAME,B.BIS_NAME,"
                    + "B.LONGITUDE,B.LATITUDE,B.BIS_DESC,B.ADDRESS,B.CONTACT_NO ";

            String whereClous = "U.BIS_ID IN (SELECT BIS_ID FROM (SELECT *  FROM SD_BUSINESS_STRUCTURES "
                    + "START WITH BIS_ID = " + bisId + " "
                    + "CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID) "
                    + "WHERE BIS_STRU_TYPE_ID = " + bisTypeId + " "
                    + "AND STATUS != 2) "
                    + "AND U.BIS_ID = B.BIS_ID (+)";

            String tables = "SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B";

            String qry = "SELECT DISTINCT USER_ID, CNT, BIS_ID,FIRST_NAME,LAST_NAME,BIS_NAME,LONGITUDE,LATITUDE,BIS_DESC,ADDRESS,CONTACT_NO FROM (SELECT (SELECT COUNT(*) FROM " + tables + "  WHERE  " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + ")";
            
         //   System.out.println("AAAAAAA: "+qry);
            
            ResultSet rs = dbCon.search(con, qry);

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                UserTypeList us = new UserTypeList();
                totalrecode = rs.getInt("CNT");
                us.setBisId(rs.getInt("BIS_ID"));
                us.setUserId(rs.getString("USER_ID"));
                us.setFirstName(rs.getString("FIRST_NAME"));
                us.setLastName(rs.getString("LAST_NAME"));
                us.setBisName(rs.getString("USER_ID")+" - "+rs.getString("BIS_NAME"));
                us.setLongitude(rs.getString("LONGITUDE"));
                us.setLatitude(rs.getString("LATITUDE"));
                us.setBisDesc(rs.getString("BIS_DESC"));
                us.setAddress(rs.getString("ADDRESS"));
                us.setTeleNumber(rs.getString("CONTACT_NO"));
                userList.add(us);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getBusinessDownList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
    
    
     public static MessageWrapper getBusinessDownListWithoutBussCategory(int bisId) {

        logger.info("getBusinessDownListWithoutBussCategory Method Call....................");
        ArrayList<UserTypeList> userList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "U.BIS_ID,U.USER_ID,U.FIRST_NAME,U.LAST_NAME,B.BIS_NAME,"
                    + "B.LONGITUDE,B.LATITUDE,B.BIS_DESC,B.ADDRESS,B.CONTACT_NO";

            String whereClous = "U.BIS_ID = "+bisId+"  "
                    + "AND U.BIS_ID = B.BIS_ID (+)";

            String tables = "SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + "  WHERE  " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                UserTypeList us = new UserTypeList();
                totalrecode = rs.getInt("CNT");
                us.setBisId(rs.getInt("BIS_ID"));
                us.setUserId(rs.getString("USER_ID"));
                us.setFirstName(rs.getString("FIRST_NAME"));
                us.setLastName(rs.getString("LAST_NAME"));
                us.setBisName(rs.getString("BIS_NAME"));
                us.setLongitude(rs.getString("LONGITUDE"));
                us.setLatitude(rs.getString("LATITUDE"));
                us.setBisDesc(rs.getString("BIS_DESC"));
                us.setAddress(rs.getString("ADDRESS"));
                us.setTeleNumber(rs.getString("CONTACT_NO"));
                userList.add(us);
            }

        } catch (Exception ex) {
            logger.info("Error getBusinessDownListWithoutBussCategory Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
    
    

    public static MessageWrapper getBusinessDownTypeList(int bisId, int bisTypeId) {

        logger.info("getBusinessDownTypeList Method Call....................");
        ArrayList<UserTypeList> userList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "BIS_ID,BIS_NAME,LONGITUDE,LATITUDE";

            String whereClous = "BIS_STRU_TYPE_ID = " + bisTypeId + " "
                    + "AND STATUS != 2";

            String tables = "(SELECT *  "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "START WITH BIS_ID = " + bisId + " "
                    + "CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID)";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + "  WHERE  " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                UserTypeList us = new UserTypeList();
                totalrecode = rs.getInt("CNT");
                us.setBisId(rs.getInt("BIS_ID"));
                us.setBisName(rs.getString("BIS_NAME"));
                us.setLongitude(rs.getString("LONGITUDE"));
                us.setLatitude(rs.getString("LATITUDE"));
                userList.add(us);
            }

        } catch (Exception ex) {
            logger.info("Error getBusinessDownTypeList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper getUpperBusinessLevel(int bis_id, int bis_type) {
        logger.info("getUpperBusinessLevel Method Call....................");
        ArrayList<UserTypeList> userTypeList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, "SELECT BIS_ID,BIS_NAME "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE BIS_ID IN (SELECT PARENT_BIS_ID "
                    + "FROM SD_BUSINESS_STRUCTURES "
                    + "WHERE  BIS_ID = " + bis_id + ") "
                    + "AND BIS_STRU_TYPE_ID = " + bis_type + "");

            logger.info("Get Data to Resultset...");
            while (rs.next()) {
                UserTypeList ut = new UserTypeList();
                ut.setBisId(rs.getInt("BIS_ID"));
                ut.setBisName(rs.getString("BIS_NAME"));
                userTypeList.add(ut);
            }
        } catch (Exception ex) {
            logger.info("Error getUpperBusinessLevel Methd call.." + ex.toString());
        }

        MessageWrapper mw = new MessageWrapper();
        totalrecode = userTypeList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(userTypeList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    /////////////////////Get Users for bisId//////////////////////
     public static MessageWrapper getBisIdUserList(int bisId) {

        logger.info("getBusinessDownList Method Call....................");
        ArrayList<UserTypeList> userList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String seletColunm = "U.BIS_ID,U.USER_ID,U.FIRST_NAME,U.LAST_NAME,B.BIS_NAME,"
                    + "B.LONGITUDE,B.LATITUDE,B.BIS_DESC,B.ADDRESS,B.CONTACT_NO";

            String whereClous = "U.BIS_ID IN (SELECT BIS_ID FROM (SELECT *  FROM SD_BUSINESS_STRUCTURES "
                    + "START WITH BIS_ID = " + bisId + " "
                    + "CONNECT BY PRIOR BIS_ID = PARENT_BIS_ID) "
                    + "WHERE STATUS != 2) "
                    + "AND U.BIS_ID = B.BIS_ID (+)";

            String tables = "SD_USER_BUSINESS_STRUCTURES U,SD_BUSINESS_STRUCTURES B";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + "  WHERE  " + whereClous + ")CNT,"
                    + "" + seletColunm + " "
                    + " FROM " + tables + " WHERE " + whereClous + ")");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                UserTypeList us = new UserTypeList();
                totalrecode = rs.getInt("CNT");
                us.setBisId(rs.getInt("BIS_ID"));
                us.setUserId(rs.getString("USER_ID"));
                us.setFirstName(rs.getString("FIRST_NAME"));
                us.setLastName(rs.getString("LAST_NAME"));
                us.setBisName(rs.getString("BIS_NAME"));
                us.setLongitude(rs.getString("LONGITUDE"));
                us.setLatitude(rs.getString("LATITUDE"));
                us.setBisDesc(rs.getString("BIS_DESC"));
                us.setAddress(rs.getString("ADDRESS"));
                us.setTeleNumber(rs.getString("CONTACT_NO"));
                userList.add(us);
            }

        } catch (Exception ex) {
            logger.info("Error getBusinessDownList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(userList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }
}
