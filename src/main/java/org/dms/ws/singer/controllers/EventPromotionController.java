/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.EventMaster;

/**
 *
 * @author SDU
 */
public class EventPromotionController {

    public static MessageWrapper getEventPromotionList(String user_id) {
        logger.info("getEventPromotionList method Call..........");
        ArrayList<EventMaster> eventList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "EVENT_ID,EVENT_NAME,"
                    + "EVENT_DESC,"
                    + "TO_CHAR(START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(END_DATE,'YYYY/MM/DD') END_DATE,"
                    + "STATUS";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                    + "FROM SD_EVENT_MASTER "
                    + "WHERE EVENT_ID IN (SELECT EVENT_ID  "
                    + "FROM SD_EVENT_PARTICIPANTS "
                    + "WHERE EVENT_PART_ID IN (SELECT EVENT_PART_ID "
                    + "FROM SD_EVENT_PARTICIPANT_DTL "
                    + "WHERE USER_ID = '" + user_id + "')) "
                    + "AND STATUS != 9");

            logger.info("Get Data to Result Set.............");

            while (rs.next()) {
                EventMaster em = new EventMaster();
                em.setEventId(rs.getInt("EVENT_ID"));;
                em.setEventName(rs.getString("EVENT_NAME"));
                em.setEventDesc(rs.getString("EVENT_DESC"));
                em.setStartDate(rs.getString("START_DATE"));
                em.setEndDate(rs.getString("END_DATE"));
                em.setStatus(rs.getInt("STATUS"));
                if (rs.getInt("STATUS") == 1) {
                    em.setStatusMsg("Planning");
                } else if (rs.getInt("STATUS") == 3) {
                    em.setStatusMsg("Started");
                } else if (rs.getInt("STATUS") == 9) {
                    em.setStatusMsg("Closed");
                } else {
                    em.setStatusMsg("None");
                }
                em.setRetunType("1");
                eventList.add(em);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error getEventPromotionList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = eventList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

    public static MessageWrapper getPromotionPointList(String user_id) {
        logger.info("getPromotionPointList method Call..........");
        ArrayList<EventMaster> eventList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            String selectColumn = "T.EVENT_ID,EVENT_NAME,"
                    + "M.EVENT_DESC,M.THRESHOLD,"
                    + "TO_CHAR(M.START_DATE,'YYYY/MM/DD') START_DATE,"
                    + "TO_CHAR(M.END_DATE,'YYYY/MM/DD') END_DATE,"
                    + "SUM(T.SALES_POINTS) SALES_POINTS,"
                    + "SUM(T.NON_SALES_POINTS) NON_SALES_POINTS";

            ResultSet rs = dbCon.search(con, "SELECT " + selectColumn + " "
                    + "FROM SD_EVENT_TXN T,SD_EVENT_MASTER M "
                    + "WHERE T.EVENT_ID = M.EVENT_ID (+) "
                    + "AND T.USER_ID = '" + user_id + "' "
                    + "GROUP BY T.EVENT_ID,M.EVENT_NAME,M.EVENT_DESC,M.THRESHOLD,M.START_DATE,M.END_DATE");

            logger.info("Get Data to Result Set.............");

            int sales_point = 0;
            int non_sales_point = 0;

            while (rs.next()) {
                sales_point = 0;
                non_sales_point = 0;
                EventMaster em = new EventMaster();
                em.setEventId(rs.getInt("EVENT_ID"));;
                em.setEventName(rs.getString("EVENT_NAME"));
                em.setEventDesc(rs.getString("EVENT_DESC"));
                em.setStartDate(rs.getString("START_DATE"));
                em.setEndDate(rs.getString("END_DATE"));
                em.setThreshold(rs.getString("THRESHOLD"));
                sales_point = rs.getInt("SALES_POINTS");
                non_sales_point = rs.getInt("NON_SALES_POINTS");
                em.setEventPoint(sales_point + non_sales_point);
                em.setRetunType("2");
                eventList.add(em);
            }
            rs.close();

        } catch (Exception ex) {
            logger.info("Error getPromotionPointList......:" + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        totalrecode = eventList.size();
        mw.setTotalRecords(totalrecode);
        mw.setData(eventList);
        logger.info("Ready to Return Values............");
        dbCon.ConectionClose(con);
        logger.info("---------------------------------------------");
        return mw;
    }

}
