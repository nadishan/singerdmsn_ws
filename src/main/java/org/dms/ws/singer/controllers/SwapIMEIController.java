/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.dms.ws.singer.entities.SwapIMEI;

/**
 *
 * @author Dasun Chathuranga
 */
public class SwapIMEIController {
    
    public SwapIMEI getCurrentIMEiLocation(String imeiNo){
        SwapIMEI loc = new SwapIMEI();
        
        try{
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();
        
            String qry = " SELECT D.BIS_ID, B.BIS_NAME "
                    + " FROM SD_DEBIT_NOTE_DETAIL D, SD_BUSINESS_STRUCTURES B   "
                    + " WHERE D.BIS_ID = B.BIS_ID       "
                    + " AND D.IMEI_NO = '"+imeiNo+"'   ";
            
            ResultSet rs = dbCon.search(con, qry);
            
            if(rs.next()){
                loc.setBisId(rs.getInt("BIS_ID"));
                loc.setBisName(rs.getString("BIS_NAME"));
            }
            
            dbCon.ConectionClose(con);
            
            loc.setStatusMsg("Successfull");
        }catch(Exception e){
            loc.setStatusMsg("Connection Error. Please Contact System Administrator.");
            e.printStackTrace();
        }
        
        return  loc;
    }
    
    public int changeIMEI(int type, String oldIMEI, String newIMEI, int bisId){
        int flag = 1;
        try{
            DbCon dbCon = new DbCon();
            Connection con = dbCon.getCon();
            
            if(type==1){
                String qry0 = " SELECT COUNT(*) CNT "
                        + " FROM  SD_DEBIT_NOTE_DETAIL  "
                        + " WHERE IMEI_NO = '"+newIMEI+"'   ";
                
                ResultSet rs = dbCon.search(con, qry0);
                int cnt = 0;
                if(rs.next()){
                    cnt = rs.getInt("CNT");
                }
                
                if(cnt==0){
                    String qry1 = " UPDATE SD_DEBIT_NOTE_DETAIL "
                        + " SET IMEI_NO = '"+newIMEI+"' "
                        + " WHERE IMEI_NO = '"+oldIMEI+"'   ";
                    
                    PreparedStatement ps = dbCon.prepare(con,qry1);
                    ps.executeUpdate();
                    flag = 1;
                }else{
                    flag = 2; //Imei No is already existing
                }
                
            }else{
                String qry0 = " SELECT COUNT(*) CNT "
                        + " FROM  SD_DEBIT_NOTE_DETAIL  "
                        + " WHERE IMEI_NO = '"+oldIMEI+"'   ";
                
                ResultSet rs = dbCon.search(con, qry0);
                int cnt = 0;
                if(rs.next()){
                    cnt = rs.getInt("CNT");
                }
                if(cnt==1){
                    String qry1 = " UPDATE SD_DEBIT_NOTE_DETAIL "
                       + " SET BIS_ID = '"+bisId+"' "
                       + " WHERE IMEI_NO = '"+oldIMEI+"'   ";
                    
                    PreparedStatement ps = dbCon.prepare(con,qry1);
                    ps.executeUpdate();
                    flag = 1;
                }else{
                    flag = 3; // No Imei No to change
                }
            }
            
            dbCon.ConectionClose(con);
        }catch(Exception e){
            flag =9;
            e.printStackTrace();
        }        
        return flag;     
    }
}
