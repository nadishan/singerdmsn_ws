/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.ws.rs.HeaderParam;
import org.dms.ldap.util.ResponseWrapper;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.BSUserMapper;
import org.dms.ws.singer.entities.User;
import org.dms.ws.singer.entities.UserBusinessStructure;

/**
 *
 * @author SDU
 */
public class UserBusinessStructuresController {

    public static MessageWrapper getUserBusinessStructures(int bis_is) {
        logger.info("getUserBusinessStructures Method Call......");

        ArrayList<User> ubsList = new ArrayList<>();
        int totalrecode = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {

            ResultSet rs = dbCon.search(con, " SELECT BIS_ID,"
                    + "USER_ID,"
                    + "FIRST_NAME,"
                    + "LAST_NAME,"
                    + "STATUS "
                    + " FROM SD_USER_BUSINESS_STRUCTURES "
                    + "WHERE BIS_ID=" + bis_is + "");

            logger.info("Get Data to ResultSet.........");
            while (rs.next()) {
                User ubs = new User();
                ubs.setExtraParams(rs.getString("BIS_ID"));
                ubs.setUserId(rs.getString("USER_ID"));
                ubs.setFirstName(rs.getString("FIRST_NAME"));
                ubs.setLastName(rs.getString("LAST_NAME"));
                if (rs.getInt("STATUS") == 1) {
                    ubs.setStatus("Avtive");
                } else {
                    ubs.setStatus("Inactive");
                }

                ubsList.add(ubs);
            }

        } catch (Exception ex) {
            logger.info("Error getUserBusinessStructures...:" + ex.toString());
        }
        logger.info("Redy Return Values............");
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(ubsList);
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------");
        return mw;

    }

    public static ValidationWrapper addUserBusinessStructure(UserBusinessStructure ubs_info) {
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            System.out.println("Bis ID" + ubs_info.getBisId());

            dbCon.save(con, "DELETE SD_USER_BUSINESS_STRUCTURES WHERE BIS_ID=" + ubs_info.getBisId() + "");

            String insetColumn = "BIS_ID,USER_ID,FIRST_NAME,LAST_NAME,USER_INSERTED,DATE_INSERTED";

            logger.info("Bis ID " + ubs_info.getBisId() + " Data Delete in table.. ");

            ArrayList<UserBusinessStructure> ubs = new ArrayList<UserBusinessStructure>();

            ubs = ubs_info.getUbsList();
            
            String Value = "?,?,?,?,?,SYSDATE";
            
            
            PreparedStatement ps_in_bussines = dbCon.prepare(con, "INSERT INTO SD_USER_BUSINESS_STRUCTURES(" + insetColumn + ") "
                        + "VALUES(" + Value + ")");

          
            for (UserBusinessStructure di : ubs) {

                ps_in_bussines.setInt(1, di.getBisId());
                ps_in_bussines.setString(2, di.getUserId());
                ps_in_bussines.setString(3, di.getFirstName());
                ps_in_bussines.setString(4, di.getLastName());
                ps_in_bussines.setString(5, di.getUser());
                ps_in_bussines.addBatch();
                


            }
            ps_in_bussines.executeBatch();
            logger.info("User Business Structure  Data Inserted........");
            result = 1;
        } catch (Exception e) {
            logger.info("Error User Business Structure Data Insert....." + e.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("User Business Structure Insert Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("User Business Structure Insert Fail");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;
    }

    //////////////////////////////////////////Add Users to LDAP and Front DB/////////////////////////////////////
    public static ValidationWrapper editBusinessStructureUser_LDAP_DB(BSUserMapper bsum_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {
        logger.info("editBusinessStructureUser_LDAP_DB Method Call......");
        int result = 1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            ArrayList<User> assUserList = bsum_info.getAssignedUsers();
            ArrayList<User> unassUserList = bsum_info.getUnassignedUsers();
          //  System.out.println(assUserList.size());
            //  System.out.println(unassUserList.size());

            if (assUserList.size() > 0) {
                logger.info("1 Bis ID    " + assUserList.get(0).getExtraParams());

                String insetColumn = "BIS_ID,USER_ID,FIRST_NAME,LAST_NAME,STATUS,USER_INSERTED,DATE_INSERTED";

                logger.info("Bis ID " + assUserList.get(0).getExtraParams() + " Data Deleted in table.. ");
                
                String Value = "?,?,?,?,1,?,SYSDATE";
                PreparedStatement ps_in_business = dbCon.prepare(con, "INSERT INTO SD_USER_BUSINESS_STRUCTURES(" + insetColumn + ") "
                        + "VALUES(" + Value + ")");
                for (User di : assUserList) {

                    dbCon.save(con, "DELETE SD_USER_BUSINESS_STRUCTURES WHERE BIS_ID=" + di.getExtraParams() + " AND USER_ID = '" + di.getUserId() + "'");

                    ps_in_business.setInt(1, Integer.parseInt(di.getExtraParams()));
                    ps_in_business.setString(2, di.getUserId());
                    ps_in_business.setString(3, di.getFirstName());
                    ps_in_business.setString(4, di.getLastName());
                    ps_in_business.setString(5, user_id);
                    ps_in_business.addBatch();

//                    dbCon.Batchsave(con, "INSERT INTO SD_USER_BUSINESS_STRUCTURES(" + insetColumn + ") "
//                            + "VALUES(" + Value + ")");
                }
                ps_in_business.executeBatch();
                //dbCon.ExecuteBatch(con);
                logger.info("User Business Structure  Data Inserted........");
                result = 1;

            }
            if (unassUserList.size() > 0) {

                logger.info("2 Bis ID    " + unassUserList.get(0).getExtraParams());

                for (User luList : unassUserList) {
                    dbCon.save(con, "DELETE SD_USER_BUSINESS_STRUCTURES WHERE BIS_ID=" + luList.getExtraParams() + " AND USER_ID = '" + luList.getUserId() + "'");
                }
                result = 1;
            }

            if (assUserList.size() > 0) {
                ArrayList<User> ldapUserList = new ArrayList<>();
                for (User luList : assUserList) {
                    User u = new User();
                    u.setUserId(luList.getUserId());
                    u.setFirstName(luList.getFirstName());
                    u.setLastName(luList.getLastName());
                    u.setExtraParams(luList.getExtraParams());
                    //   System.out.println("Assgn ---"+luList.getFirstName()+"====="+luList.getExtraParams());
                    ldapUserList.add(u);
                }

                ResponseWrapper urw = null;
                try {
                    logger.info("Call LDAP modifyMultipleUsers Method......!");
                    urw = LDAPWSController.modifyMultipleUsers(ldapUserList, user_id, room, department, branch, countryCode, division, organaization, system);

                } catch (Exception ex) {
                    logger.info("Error LDAP modifyMultipleUsers Method...........");
                    result = 9;
                }
                result = 1;
            }
            if (unassUserList.size() > 0) {

               // assUserList.addAll(unassUserList);
                ArrayList<User> ldapUserList = new ArrayList<>();
                for (User luList : unassUserList) {
                    User u = new User();
                    u.setUserId(luList.getUserId());
                    u.setFirstName(luList.getFirstName());
                    u.setLastName(luList.getLastName());
                    u.setExtraParams("0");
                    //  System.out.println("UnAssgn ---"+luList.getFirstName()+"=====");
                    ldapUserList.add(u);
                }

                ResponseWrapper urw = null;
                try {
                    logger.info("Call LDAP modifyMultipleUsers Method......!");
                    urw = LDAPWSController.modifyMultipleUsers(ldapUserList, user_id, room, department, branch, countryCode, division, organaization, system);

                } catch (Exception ex) {
                    logger.info("Error LDAP modifyMultipleUsers Method...........");
                    result = 9;
                }
                result = 1;
            }

        } catch (Exception e) {
            logger.info("Error User Business Structure Data Insert....." + e.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("User Business Structure Update Successfully");
        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("User Business Structure Update Fail");
        }

        dbCon.ConectionClose(con);
        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return vw;

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
}
