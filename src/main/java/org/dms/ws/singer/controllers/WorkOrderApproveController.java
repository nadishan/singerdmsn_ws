/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dms.ws.singer.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.dms.ws.services.MessageWrapper;
import org.dms.ws.services.ValidationWrapper;
import static org.dms.ws.singer.controllers.DbCon.logger;
import org.dms.ws.singer.entities.PaymentApprove;
import org.dms.ws.singer.entities.WorkOrderDefect;
import org.dms.ws.singer.entities.WorkOrderEstimate;
import org.dms.ws.singer.entities.WorkOrderMaintain;
import org.dms.ws.singer.entities.WorkOrderMaintainDetail;
import org.dms.ws.singer.entities.WorkOrderPayment;
import org.dms.ws.singer.entities.WorkOrderPaymentDetail;
import org.dms.ws.singer.utils.DBMapper;

/**
 *
 * @author SDU
 */
public class WorkOrderApproveController {

    public static MessageWrapper getApprovePaymentList(String service_name,
            double payment,
            int payment_id,
            String ref_date,
            int status,
            double tax,
            String essd_no,
            String po_number,
            String po_date,
            double settle_amount,
            String remarks,
            String cheque_no,
            String cheque_details,
            String cheque_date,
            String order,
            String type,
            int start,
            int limit) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getApprovePaymentList Method Call....................");

        ArrayList<PaymentApprove> paymentApproveList = new ArrayList<>();
        int totalrecode = 0;

        try {

            order = DBMapper.paymentApprove(order);

            String ServiceName = (service_name.equalsIgnoreCase("all") ? "NVL(S.BIS_NAME,'AA') = NVL(S.BIS_NAME,'AA')" : "UPPER(S.BIS_NAME) LIKE UPPER('%" + service_name + "%')");
            String amount = (payment == 0 ? "" : "AND P.AMOUNT = " + payment + "");
            String paymentId = (payment_id == 0 ? "" : "AND P.PAYMENT_ID = " + payment_id + "");
            // String refDate = (ref_date.equalsIgnoreCase("all") ? "NVL(P.PAYMENT_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(P.PAYMENT_DATE,TO_DATE('20100101','YYYYMMDD')) " : "P.PAYMENT_DATE = TO_DATE('" + ref_date + "','YYYY/MM/DD')");
            String refDate = (ref_date.equalsIgnoreCase("all") ? "NVL(P.PAYMENT_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(P.PAYMENT_DATE,TO_DATE('20100101','YYYYMMDD')) " : "TO_CHAR(P.PAYMENT_DATE , 'YYYY/MM/DD')  = '" + ref_date + "'");
            String Status = (status == 0 ? "" : "AND P.STATUS = " + status + "");
            String Tax = (tax == 0 ? "" : "AND P.TAX = " + tax + "");
            String essdAcountNo = (essd_no.equalsIgnoreCase("all") ? "NVL(P.ESSD_ACCOUNT_NO,'AA') = NVL(P.ESSD_ACCOUNT_NO,'AA')" : "UPPER(P.ESSD_ACCOUNT_NO) LIKE UPPER('%" + essd_no + "%')");
            String poNumber = (po_number.equalsIgnoreCase("all") ? "NVL(P.PO_NUMBER,'AA') = NVL(P.PO_NUMBER,'AA')" : "UPPER(P.PO_NUMBER) LIKE UPPER('%" + po_number + "%')");
            String poDate = (po_date.equalsIgnoreCase("all") ? "NVL(P.PO_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(P.PO_DATE,TO_DATE('20100101','YYYYMMDD')) " : "P.PO_DATE = TO_DATE('" + po_date + "','YYYY/MM/DD')");
            String setleAmount = (settle_amount == 0 ? "" : "AND P.SETTLE_AMOUNT = " + settle_amount + "");
            String Remarks = (remarks.equalsIgnoreCase("all") ? "NVL(P.REMARKS,'AA') = NVL(P.REMARKS,'AA')" : "UPPER(P.REMARKS) LIKE UPPER('%" + remarks + "%')");
            String chequeNo = (cheque_no.equalsIgnoreCase("all") ? "NVL(P.CHEQUE_NO,'AA') = NVL(P.CHEQUE_NO,'AA')" : "UPPER(P.CHEQUE_NO) LIKE UPPER('%" + cheque_no + "%')");
            String chequeDetails = (cheque_details.equalsIgnoreCase("all") ? "NVL(P.CHEQUE_DETIALS,'AA') = NVL(P.CHEQUE_DETIALS,'AA')" : "UPPER(P.CHEQUE_DETIALS) LIKE UPPER('%" + cheque_details + "%')");
            String chequeDate = (cheque_date.equalsIgnoreCase("all") ? "NVL(P.CHEQUE_COLLECTED_DATE,TO_DATE('20100101','YYYYMMDD')) = NVL(P.CHEQUE_COLLECTED_DATE,TO_DATE('20100101','YYYYMMDD')) " : "P.CHEQUE_COLLECTED_DATE = TO_DATE('" + cheque_date + "','YYYY/MM/DD')");
            String Order = (order.equalsIgnoreCase("all") ? "ORDER BY P.PAYMENT_ID DESC" : "ORDER BY " + order + "   " + type + "");

            String seletColunm = "S.BIS_NAME,"
                    + "P.AMOUNT," //change this line
                    + "P.PAYMENT_ID,"
                    + "TO_CHAR(P.PAYMENT_DATE,'YYYY/MM/DD') PAYMENT_DATE,"
                    + "P.STATUS,P.TAX,"
                    + "P.ESSD_ACCOUNT_NO,P.PO_NUMBER,"
                    + "TO_CHAR(P.PO_DATE,'YYYY/MM/DD') PO_DATE,"
                    + "P.SETTLE_AMOUNT,"
                    + "P.REMARKS,"
                    + "P.CHEQUE_NO,"
                    + "P.DEDUCT_AMOUNT,"
                    //  + "(P.AMOUNT + NVL(P.NBT_VALUE,0) + NVL(P.VAT_VALUE,0)- NVL(P.DEDUCT_AMOUNT,0)) AS AMOUNT,"
                    + "P.CHEQUE_DETIALS,"
                    + "TO_CHAR(P.CHEQUE_COLLECTED_DATE,'YYYY/MM/DD') CHEQUE_COLLECTED_DATE"; //add this line

            String whereClous = "P.USER_INSERTED = B.USER_ID (+) "
                    + " AND B.BIS_ID = S.BIS_ID (+) "
                    + " AND " + ServiceName + " "
                    + " " + amount + " "
                    + " " + paymentId + " "
                    + " AND " + refDate + " "
                    + " " + Status + " "
                    + " " + Tax + " "
                    + " AND " + essdAcountNo + " "
                    + " AND " + poNumber + " "
                    + " AND " + poDate + " "
                    + " " + setleAmount + " "
                    + " AND " + Remarks + " "
                    + " AND " + chequeNo + " "
                    + " AND " + chequeDetails + " "
                    + " AND " + chequeDate + "";

            String tables = "SD_WORK_ORDER_PAYMENTS P,SD_USER_BUSINESS_STRUCTURES B,SD_BUSINESS_STRUCTURES S";

            ResultSet rs = dbCon.search(con, "SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            logger.info("Get Data to Resultset.....");

            logger.info("SELECT * FROM (SELECT (SELECT COUNT(*) FROM " + tables + " WHERE " + whereClous + ")CNT,"
                    + "" + seletColunm + ",ROW_NUMBER() OVER ( " + Order + " ) RN "
                    + " FROM " + tables + " WHERE " + whereClous + ") "
                    + " WHERE RN BETWEEN " + start + " AND " + (start + limit) + " ORDER BY RN");

            while (rs.next()) {
                PaymentApprove pa = new PaymentApprove();
                totalrecode = rs.getInt("CNT");
                pa.setSetviceName(rs.getString("BIS_NAME"));
                pa.setPaymentPrice(rs.getDouble("AMOUNT"));
                pa.setPaymentId(rs.getInt("PAYMENT_ID"));
                pa.setRefDate(rs.getString("PAYMENT_DATE"));
                pa.setStatus(rs.getInt("STATUS"));
                pa.setTax(rs.getDouble("TAX"));
                pa.setEssdAccountNo(rs.getString("ESSD_ACCOUNT_NO"));
                pa.setPoNumber(rs.getString("PO_NUMBER"));
                pa.setPoDate(rs.getString("PO_DATE"));
                pa.setSettleAmount(rs.getDouble("SETTLE_AMOUNT"));
                pa.setRemarks(rs.getString("REMARKS"));
                pa.setChequeNo(rs.getString("CHEQUE_NO"));
                pa.setChequeDetails(rs.getString("CHEQUE_DETIALS"));
                pa.setChequeDate(rs.getString("CHEQUE_COLLECTED_DATE"));
                pa.setDeductionAmount(rs.getInt("DEDUCT_AMOUNT"));
                paymentApproveList.add(pa);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getApprovePaymentList Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(paymentApproveList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static MessageWrapper addWorkOrderGridUpdate(String workOrderNo, int paymentId, int deductionAmount, double nbtValue, double vatValue, double currentDeduction, int status) {

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("getApprovePaymentList Method Call....................");
        ArrayList<PaymentApprove> paymentApproveList = new ArrayList<>();

        int result = 0;
        double tot = 0;
        try {

            String sqlForGetCurrentLineDedcut = "SELECT NVL(D.DEDUCTION_AMOUNT,0) DEDUCTION_AMOUNT ,"
                    + " D.PAYMENT_ID,"
                    + " P.AMOUNT"
                    + " FROM SD_WORK_ORDER_PAYMENTS_DTL D, SD_WORK_ORDER_PAYMENTS P "
                    + "WHERE D.WORK_ORDER_NO='" + workOrderNo + "' AND P.PAYMENT_ID = D.PAYMENT_ID";

            System.out.println("QRYY" + sqlForGetCurrentLineDedcut);
            ResultSet res = dbCon.search(con, sqlForGetCurrentLineDedcut);

            double curDeduct = 0;
            int payId = 0;
            double vat = 0;
            double nbt = 0;
            int vatApplicable = 0;
            int nbtApplicable = 0;
            
            while (res.next()) {
                curDeduct = res.getDouble("DEDUCTION_AMOUNT");
                payId = res.getInt("PAYMENT_ID");
            }

            System.out.println("WWWWW" + curDeduct + "dddddddddddd" + payId);

            String qryForGetBeforeValue = "SELECT SUM (pd.amount) TOT_AMOUNT, "
                    + "SUM (pd.deduction_amount) TOT_DED_AMOUNT,"
                    + "(SELECT p.deduct_amount "
                    + "FROM sd_work_order_payments p"
                    + " WHERE p.payment_id = '" + paymentId + "') DED_AMOUNT_P,"
                    + "(SELECT p.VAT_APPLICABLE"
                    + " FROM sd_work_order_payments p"
                    + " WHERE p.payment_id = '" + paymentId + "') VAT_APPLICABLE,"
                    + "(SELECT p.NBT_APPLICABLE FROM sd_work_order_payments p"
                    + " WHERE p.payment_id = '" + paymentId + "') NBT_APPLICABLE "
                    + "FROM sd_work_order_payments_dtl pd WHERE pd.payment_id = '" + paymentId + "'"
                    + " GROUP BY pd.payment_id";

            System.out.println("QRYY2222222 " + qryForGetBeforeValue);
            ResultSet resBefore = dbCon.search(con, qryForGetBeforeValue);

            while (resBefore.next()) {
                
                tot = resBefore.getDouble("TOT_AMOUNT") - resBefore.getDouble("TOT_DED_AMOUNT") - resBefore.getDouble("DED_AMOUNT_P");
                System.out.println("totWithoutTax " + tot);

                nbtApplicable = resBefore.getInt("NBT_APPLICABLE");
                vatApplicable = resBefore.getInt("VAT_APPLICABLE");
            }

            String qry = "UPDATE SD_WORK_ORDER_PAYMENTS_DTL "
                    + " SET DEDUCTION_AMOUNT= " + deductionAmount + " "
                    + " WHERE PAYMENT_ID= " + paymentId + "  "
                    + " AND WORK_ORDER_NO= '" + workOrderNo + "'  ";

            dbCon.save(con, qry);

            if (curDeduct > deductionAmount) {
                System.out.println("Dedut....11111");
                double diff = Math.abs(curDeduct - deductionAmount);
                tot = tot + diff;

            } else {
                System.out.println("Dedut....222222");
                double diff = Math.abs(curDeduct - deductionAmount);
                tot = tot - diff;

                if (tot <= 0) {
                    tot = 0;
                }

            }

            double taxTotal = 0;
            
            double cDeduct = currentDeduction;
            
            if(0 > cDeduct){
                cDeduct = 0;
            }

            nbt = cDeduct * nbtValue;
            
            vat = (cDeduct + nbt) * vatValue;

            if (nbtApplicable == 1) {
                cDeduct = cDeduct * (1+ nbtValue);
//                taxTotal += nbt;
            }

            if (vatApplicable == 1) {
               cDeduct = cDeduct * (1+ vatValue);
            }

            

            String nbtQry = " NBT_VALUE = CASE WHEN NBT_VALUE = '0' AND NBT_APPLICABLE = '0' THEN '0' ELSE '" + nbt + "' END,"; // changes has made
            String vatQry = " VAT_VALUE = CASE WHEN VAT_VALUE = '0' AND VAT_APPLICABLE = '0' THEN '0' ELSE '" + vat + "' END"; //changes has made

            System.out.println("NNNNNNNNNNNNnnnn " + nbt + " VVVVVV " + vat);

            if (status == 1) {
                tot = tot + taxTotal;
                
                String sqlForUpdatePaymentDetails = "UPDATE SD_WORK_ORDER_PAYMENTS SET AMOUNT='" + cDeduct + "', " + nbtQry + " " + vatQry + " WHERE PAYMENT_ID='" + payId + "'";
                System.out.println("Qry For details update" + sqlForUpdatePaymentDetails);
                dbCon.save(con, sqlForUpdatePaymentDetails);
               
                String sqlForUpdatePaymentDetailsUpdate = "UPDATE SD_WORK_ORDER_PAYMENTS_DTL WPD SET WPD.STATUS = 1 WHERE WPD.WORK_ORDER_NO = '"+workOrderNo+"'";
                dbCon.save(con, sqlForUpdatePaymentDetailsUpdate);
                
            }else{
                
                String sqlForUpdatePaymentDetails = "UPDATE SD_WORK_ORDER_PAYMENTS_DTL WPD SET WPD.STATUS = 0 WHERE WPD.WORK_ORDER_NO = '"+workOrderNo+"'";
                dbCon.save(con, sqlForUpdatePaymentDetails);
                
                
                String sqlForUpdatePaymentDetailsWO = "UPDATE SD_WORK_ORDER_PAYMENTS SET LINE_DEDUCT_SUM = '"+currentDeduction+"' WHERE PAYMENT_ID='" + payId + "'";
                System.out.println("Qry For details update" + sqlForUpdatePaymentDetailsWO);
                dbCon.save(con, sqlForUpdatePaymentDetails);
                
            }

            System.out.println("Saved..............");

            result = 1;

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error in editApprovePayment Mehtod  :" + ex.toString());
            result = 9;
        }

        ArrayList<Double> arrOut = new ArrayList<Double>();
        arrOut.add(tot);
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(result);
//        mw.setTotalRecords(totalrecode);
        mw.setData(arrOut);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
// return workOrderNo;
    }

    public static MessageWrapper getApprovePaymentDetails(int paymentId, String workOrderNo, String warrentyVrifType, String imeiNo) {

        logger.info("getApprovePaymentDetails Method Call....................");

        ArrayList<WorkOrderPaymentDetail> paymentDeialsList = new ArrayList<>();
        int totalrecode = 1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            System.out.println("SDSDSDSFDFESFEE" + workOrderNo);
            String ImeiQry = (imeiNo.equalsIgnoreCase("all") ? " P.IMEI_NO=P.IMEI_NO " : " UPPER(P.IMEI_NO) LIKE UPPER('%" + imeiNo + "%') ");
            String woQry = (workOrderNo.equalsIgnoreCase("all") ? " P.WORK_ORDER_NO=P.WORK_ORDER_NO " : " UPPER(P.WORK_ORDER_NO) LIKE UPPER('%" + workOrderNo + "%') ");
            String warrantyQry = (warrentyVrifType.equalsIgnoreCase("all") ? " P.WARRANTY_VERIFY_TYPE=P.WARRANTY_VERIFY_TYPE " : " UPPER(M.DESCRIPTION) LIKE UPPER('%" + warrentyVrifType + "%') ");

            String seletColunm = "P.PAYMENT_ID,"
                    + "P.WORK_ORDER_NO,"
                    + "P.WARRANTY_VERIFY_TYPE,"
                    + "P.IMEI_NO,"
                    + "D.MAJ_CODE_DESC,"
                    + "P.REFERENCE_NO,"
                    + "P.STATUS,"
                    + "P.AMOUNT,"
                    + "M.DESCRIPTION,"
                    + "P.DEDUCTION_AMOUNT,"
                    + "P.DELAY_DAYS,"
                    + "P.AMOUNT,"
                    + "PD.NBT_APPLICABLE,"
                    + "PD.VAT_APPLICABLE,"
                    + "PD.DEDUCT_AMOUNT AS INITDEDUCT, "
                    + "NVL(PD.LINE_DEDUCT_SUM,100000000) LINE_DEDUCT_SUM";

            String whereClous = "W.DEFECTS = D.MAJ_CODE (+) "
                    + " AND P.WARRANTY_VERIFY_TYPE = M.CODE (+) "
                    + " AND PD.PAYMENT_ID = P.PAYMENT_ID"
                    + " AND M.DOMAIN_NAME = 'WARANTY' "
                    + " AND W.WORK_ORDER_NO = P.WORK_ORDER_NO "
                    + " AND P.PAYMENT_ID = " + paymentId + " "
                    + " AND " + ImeiQry + " "
                    + " AND " + woQry + " "
                    + " AND " + warrantyQry + " "
                    + " AND P.STATUS != 9";

            String tables = "SD_WORK_ORDER_PAYMENTS_DTL P,SD_WORK_ORDER_PAYMENTS PD, SD_DEFECT_CODES D,SD_MDECODE M, SD_WORK_ORDERS W ";

            System.out.println("WRRRRRR SELECT " + seletColunm + " FROM " + tables + " WHERE " + whereClous + " ");

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM " + tables + " WHERE " + whereClous + "");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {

                String refNumber = rs.getString("WORK_ORDER_NO");

                WorkOrderPaymentDetail pa = new WorkOrderPaymentDetail();
                pa.setPaymentId(rs.getInt("PAYMENT_ID"));
                pa.setWorkOrderNo(rs.getString("WORK_ORDER_NO"));
                pa.setWarrantyVerifType(rs.getInt("WARRANTY_VERIFY_TYPE"));
                pa.setImeiNo(rs.getString("IMEI_NO"));
                pa.setDefectDesc(rs.getString("MAJ_CODE_DESC"));
                pa.setReferenceNo(rs.getString("REFERENCE_NO"));
                pa.setStatus(rs.getInt("STATUS"));
                pa.setWarrentyTypeDesc(rs.getString("DESCRIPTION"));
                pa.setDelayDays(rs.getInt("DELAY_DAYS"));
                pa.setDeductionAmount(rs.getDouble("DEDUCTION_AMOUNT"));
                pa.setAmount(rs.getDouble("AMOUNT"));
                pa.setNbtApplicable(rs.getInt("NBT_APPLICABLE"));
                pa.setVatApplicable(rs.getInt("VAT_APPLICABLE"));
                pa.setInitDeduct(rs.getDouble("INITDEDUCT"));
                pa.setLineDeductSum(rs.getDouble("LINE_DEDUCT_SUM"));
//                ###############################################
                //get delivery date to calulate delay days
                String queryForDelayDays = "SELECT "
                        + "TO_DATE(swo.DELIVERY_DATE , 'DD/MM/YYYY') - TO_DATE( swo.DATE_INSERTED , 'DD/MM/YYYY') as delayDays"
                        + " FROM SD_WORK_ORDERS swo "
                        + " WHERE swo.WORK_ORDER_NO = '" + refNumber + "' ";

                logger.info(queryForDelayDays);

                ResultSet resultForDelayDays = dbCon.search(con, queryForDelayDays);

                while (resultForDelayDays.next()) {
                    logger.info(resultForDelayDays.getInt("delayDays"));
                    pa.setDelayDays(resultForDelayDays.getInt("delayDays"));
                }

                paymentDeialsList.add(pa);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error getApprovePaymentDetails Methd call.." + ex.toString());
        }
        totalrecode = paymentDeialsList.size();
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(paymentDeialsList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

    public static ValidationWrapper editApprovePayment(WorkOrderPaymentDetail wop_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("editApprovePayment Mehtod Call.......");
        int result = 0;
        try {

            dbCon.save(con, "UPDATE SD_WORK_ORDER_PAYMENTS_DTL "
                    + " SET STATUS = " + wop_info.getStatus() + ","
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED=SYSDATE "
                    + " WHERE WORK_ORDER_NO = '" + wop_info.getWorkOrderNo() + "'");

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in editApprovePayment Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Work Order Payment Update Successfully");
            logger.info("WorkOrder Payment Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Work Order Payment Update");
            logger.info("Error in WorkOrder Payment Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addApprovePayment(PaymentApprove wop_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            int delayDays,
            int deductionAmount,
            String organaization) {

        logger.info("addApprovePayment Mehtod Call.......");
        int result = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            
             dbCon.offAutoCommit(con);

            dbCon.save(con, "DELETE SD_WORK_ORDER_PAYMENTS_DTL WHERE PAYMENT_ID = " + wop_info.getPaymentId() + "");

            ArrayList<WorkOrderPaymentDetail> wolist = new ArrayList<WorkOrderPaymentDetail>();
            wolist = wop_info.getWoPaymentDetials();

            if (wolist != null && wolist.size() > 0) {

                int max_pay_details = 0;

                String paymenrDetialColumn = "SEQ_NO,"
                        + "PAYMENT_ID,"
                        + "WORK_ORDER_NO,"
                        + "IMEI_NO,"
                        + "WARRANTY_VERIFY_TYPE,"
                        + "DEFECT_TYPE,"
                        + "AMOUNT,"
                        + "PAYMENT_DATE,"
                        + "STATUS,"
                        + "USER_INSERTED,"
                        + "DATE_INSERTED, "
                        + "DELAY_DAYS, "
                        + "DEDUCTION_AMOUNT";

                String woPaymentValue = "?,"
                        + "?,"
                        + "?,"
                        + "?,"
                        + "?,"
                        + "?,"
                        + "?,"
                        + "SYSDATE,"
                        + "?,"
                        + "?,"
                        + "SYSDATE, "
                        + "?, "
                        + "?";

                ResultSet rs_cnt = dbCon.search(con, "SELECT (NVL(MAX(SEQ_NO),0)) MAX "
                        + "FROM SD_WORK_ORDER_PAYMENTS_DTL "
                        + "WHERE PAYMENT_ID = " + wop_info.getPaymentId() + "");

                while (rs_cnt.next()) {
                    max_pay_details = rs_cnt.getInt("MAX");
                }

                PreparedStatement ps_dt = dbCon.prepare(con, "INSERT INTO SD_WORK_ORDER_PAYMENTS_DTL(" + paymenrDetialColumn + ") "
                        + "VALUES(" + woPaymentValue + ")");

                for (WorkOrderPaymentDetail di : wolist) {

                    max_pay_details = max_pay_details + 1;

                    ps_dt.setInt(1, max_pay_details);
                    ps_dt.setInt(2, wop_info.getPaymentId());
                    ps_dt.setString(3, di.getWorkOrderNo());
                    ps_dt.setString(4, di.getImeiNo());
                    ps_dt.setInt(5, di.getWarrantyVerifType());
                    ps_dt.setInt(6, di.getDefectType());
                    ps_dt.setDouble(7, di.getAmount());
                    ps_dt.setDouble(8, di.getStatus());
                    ps_dt.setString(9, user_id);
                    ps_dt.setInt(10, di.getDelayDays());
                    ps_dt.setDouble(11, di.getDeductionAmount());
                    ps_dt.addBatch();

                }
                ps_dt.executeBatch();

                /////////////////////////////Update Work Orders ///////////////////////////////////
                if (wop_info.getStatus() == 2) {
                    String updateQry = " UPDATE SD_WORK_ORDERS   " //////////////Update Work Order table as Repair Payment Approved
                            + "         SET    STATUS = 6       "
                            + "         WHERE  WORK_ORDER_NO = ? ";

                    PreparedStatement prep = dbCon.prepare(con, updateQry);

                    for (WorkOrderPaymentDetail wo : wolist) {
                        prep.setString(1, wo.getWorkOrderNo());
                        prep.addBatch();
                    }

                    prep.executeBatch();

                    result = 1;
                }

                if (wop_info.getStatus() == 3) {
                    String updateQry = " UPDATE SD_WORK_ORDERS   " //////////////Update Work Order table as Repair Payment Rejected
                            + "         SET    STATUS = 7       "
                            + "         WHERE  WORK_ORDER_NO = ? ";

                    PreparedStatement prep = dbCon.prepare(con, updateQry);

                    for (WorkOrderPaymentDetail wo : wolist) {
                        prep.setString(1, wo.getWorkOrderNo());
                        prep.addBatch();
                    }

                    prep.executeBatch();

                    result = 1;
                }

                
            }

            double nbt = wop_info.getNbtValue() * wop_info.getTotalAmountWithDeduct();
            double vat = wop_info.getVatValue() * (wop_info.getTotalAmountWithDeduct() + nbt);
            
            
            System.out.println("nbt" + nbt);
            System.out.println("vattttttttt" + vat);

            String sqlqry = "UPDATE SD_WORK_ORDER_PAYMENTS "
                    + " SET STATUS = " + wop_info.getStatus() + ","
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED = SYSDATE,"
                    + " AMOUNT = " + wop_info.getPaymentPrice() + ", "
                    + " NBT_VALUE = CASE WHEN NBT_VALUE = '0' AND NBT_APPLICABLE = '0' THEN '0' ELSE '" + nbt + "' END," // changes has made
                    + " VAT_VALUE = CASE WHEN VAT_VALUE = '0' AND VAT_APPLICABLE = '0' THEN '0' ELSE '" + vat + "' END," //changes has made
                    + " APPROVE_BIS_ID = " + wop_info.getApproveBisId() + ","
                    + " REMARKS = '" + wop_info.getRemarks() + "',"
                    + " LINE_DEDUCT_SUM = '" + wop_info.getLineDeductSum() + "' "
                    + " WHERE PAYMENT_ID = '" + wop_info.getPaymentId() + "'";

            System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR" + sqlqry);

            dbCon.save(con, sqlqry);

            result = 1;
            
              dbCon.doCommit(con);

        } catch (Exception ex) {
            dbCon.doRollback(con);
            logger.info("Error in addApprovePayment Mehtod  :" + ex.toString());
            ex.printStackTrace();
            result = 9;
            
        }finally{
               dbCon.onAutoCommit(con);
        }
        
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Approve Payment Update Successfully");
            logger.info("Approve Payment Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Approve Payment Update");
            logger.info("Error in Approve Payment Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addSettleAmount(PaymentApprove wop_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("addSettleAmount Mehtod Call.......");
        int result = 0;
        try {

            dbCon.save(con, "UPDATE SD_WORK_ORDER_PAYMENTS "
                    + " SET PO_NUMBER = '" + wop_info.getPoNumber() + "',"
                    + " PO_DATE = TO_DATE('" + wop_info.getPoDate() + "','YYYY/MM/DD'),"
                    + " SETTLE_AMOUNT = " + wop_info.getSettleAmount() + ","
                    + " REMARKS = '" + wop_info.getRemarks() + "',"
                    + " STATUS = " + wop_info.getStatus() + ","
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED = SYSDATE "
                    + " WHERE PAYMENT_ID = '" + wop_info.getPaymentId() + "'");

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in addSettleAmount Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Settle Amount Update Successfully");
            logger.info("Settle Amount Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Settle Amount Update");
            logger.info("Error in Settle Amount Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static ValidationWrapper addChequeDetails(PaymentApprove wop_info,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization) {
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        logger.info("addChequeDetails Mehtod Call.......");
        int result = 0;
        try {

            dbCon.save(con, "UPDATE SD_WORK_ORDER_PAYMENTS "
                    + " SET CHEQUE_NO = '" + wop_info.getChequeNo() + "',"
                    + " CHEQUE_DETIALS = '" + wop_info.getChequeDetails() + "',"
                    + " CHEQUE_COLLECTED_DATE = TO_DATE('" + wop_info.getChequeDate() + "','YYYY/MM/DD'),"
                    + " STATUS = " + wop_info.getStatus() + ","
                    + " USER_MODIFIED = '" + user_id + "',"
                    + " DATE_MODIFIED = SYSDATE "
                    + " WHERE PAYMENT_ID = '" + wop_info.getPaymentId() + "'");

            result = 1;

        } catch (Exception ex) {
            logger.info("Error in addChequeDetails Mehtod  :" + ex.toString());
            result = 9;
        }
        ValidationWrapper vw = new ValidationWrapper();
        if (result == 1) {
            vw.setReturnFlag("1");
            vw.setReturnMsg("Cheque Details Update Succesfully");
            logger.info("Cheque Details Update Succesfully............");

        } else {
            vw.setReturnFlag("9");
            vw.setReturnMsg("Error in Cheque Details Update");
            logger.info("Error in Cheque Details Update................");
        }
        logger.info("Ready to Return Values..........");
        dbCon.ConectionClose(con);
        logger.info("-----------------------------------------------------------");
        return vw;
    }

    public static MessageWrapper getApprovePaymentHeader(int payment_id) {

        logger.info("getApprovePaymentHeader Method Call....................");
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        ArrayList<PaymentApprove> paymentDeialsList = new ArrayList<>();
        int totalrecode = 1;

        try {

            String seletColunm = "PAYMENT_ID,"
                    + "AMOUNT,"
                    + "TAX,"
                    + "ESSD_ACCOUNT_NO,"
                    + "TO_CHAR(PAYMENT_DATE,'YYYY/MM/DD') PAYMENT_DATE,"
                    + "STATUS,"
                    + "PO_NUMBER,"
                    + "PO_DATE,"
                    + "SETTLE_AMOUNT,"
                    + "REMARKS,"
                    + "CHEQUE_NO,"
                    + "CHEQUE_DETIALS,"
                    + "TO_CHAR(CHEQUE_COLLECTED_DATE,'YYYY/MM/DD') CHEQUE_COLLECTED_DATE";

            String whereClous = "PAYMENT_ID = " + payment_id + "";

            String tables = "SD_WORK_ORDER_PAYMENTS";

            ResultSet rs = dbCon.search(con, "SELECT " + seletColunm + " FROM " + tables + " WHERE " + whereClous + "");

            logger.info("Get Data to Resultset...");

            while (rs.next()) {
                PaymentApprove pa = new PaymentApprove();
                pa.setPaymentId(rs.getInt("PAYMENT_ID"));
                pa.setPaymentPrice(rs.getDouble("AMOUNT"));
                pa.setTax(rs.getDouble("TAX"));
                pa.setEssdAccountNo(rs.getString("ESSD_ACCOUNT_NO"));
                pa.setPaymentDate(rs.getString("PAYMENT_DATE"));
                pa.setStatus(rs.getInt("STATUS"));
                pa.setPoNumber(rs.getString("PO_NUMBER"));
                pa.setPoDate(rs.getString("PO_DATE"));
                pa.setSettleAmount(rs.getDouble("SETTLE_AMOUNT"));
                pa.setRemarks(rs.getString("REMARKS"));
                pa.setChequeNo(rs.getString("CHEQUE_NO"));
                pa.setChequeDetails(rs.getString("CHEQUE_DETIALS"));
                pa.setChequeDate(rs.getString("CHEQUE_COLLECTED_DATE"));
                paymentDeialsList.add(pa);
            }

        } catch (Exception ex) {
            logger.info("Error getApprovePaymentHeader Methd call.." + ex.toString());
        }
        MessageWrapper mw = new MessageWrapper();
        mw.setTotalRecords(totalrecode);
        mw.setData(paymentDeialsList);
        logger.info("Ready to Return Values...............");
        dbCon.ConectionClose(con);
        logger.info("----------------------------------------------------");
        return mw;
    }

//    public static MessageWrapper getApprovePaymentList(String all, int i, int i0, String all0, int i1, int i2, String all1, String all2, String all3, int i3, String all4, String all5, String all6, String all7, String all8, String all9, int i4, int i5) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
