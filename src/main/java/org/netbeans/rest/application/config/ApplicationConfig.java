/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.rest.application.config;

import java.io.IOException;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.ws.rs.core.Application;

/**
 *
 * @author SDU
 */
@javax.ws.rs.ApplicationPath("Services")
//@WebFilter(urlPatterns = "/Services")
public class ApplicationConfig extends Application { //implements Filter{

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(org.dms.ws.services.AcceptDebitNotes.class);
        resources.add(org.dms.ws.services.AcceptInventories.class);
        resources.add(org.dms.ws.services.BussinessCategories.class);
        resources.add(org.dms.ws.services.BussinessStructures.class);
        resources.add(org.dms.ws.services.BussinessTrackings.class);
        resources.add(org.dms.ws.services.BussinessTypes.class);
        resources.add(org.dms.ws.services.Categories.class);
        resources.add(org.dms.ws.services.ClearTransitItems.class);
        resources.add(org.dms.ws.services.CreditNoteIssues.class);
        resources.add(org.dms.ws.services.Customers.class);
        resources.add(org.dms.ws.services.DbProcedures.class);
        resources.add(org.dms.ws.services.Departments.class);
        resources.add(org.dms.ws.services.DeviceExchanges.class);
        resources.add(org.dms.ws.services.DeviceIssues.class);
        resources.add(org.dms.ws.services.DeviceReturns.class);
        resources.add(org.dms.ws.services.DsrAllowOffline.class);
        resources.add(org.dms.ws.services.EventAssignMarks.class);
        resources.add(org.dms.ws.services.EventInquires.class);
        resources.add(org.dms.ws.services.EventMasters.class);
        resources.add(org.dms.ws.services.EventPromotions.class);
        resources.add(org.dms.ws.services.Functions.class);
        resources.add(org.dms.ws.services.Groups.class);
        resources.add(org.dms.ws.services.ImeiAdjusments.class);
        resources.add(org.dms.ws.services.ImeiMasters.class);
        resources.add(org.dms.ws.services.ImportDebitNotes.class);
        resources.add(org.dms.ws.services.InventoryUpload.class);
        resources.add(org.dms.ws.services.IssueInventories.class);
        resources.add(org.dms.ws.services.Login.class);
        resources.add(org.dms.ws.services.MajorDefects.class);
        resources.add(org.dms.ws.services.MinorDefects.class);
        resources.add(org.dms.ws.services.Models.class);
        resources.add(org.dms.ws.services.Parts.class);
        resources.add(org.dms.ws.services.PaymentApproves.class);
        resources.add(org.dms.ws.services.Promotions.class);
        resources.add(org.dms.ws.services.RepairCategories.class);
        resources.add(org.dms.ws.services.RepairLevels.class);
        resources.add(org.dms.ws.services.ReplacementReturns.class);
        resources.add(org.dms.ws.services.ReportService.class);
        resources.add(org.dms.ws.services.ShopVistChecks.class);
        resources.add(org.dms.ws.services.SmsService.class);
        resources.add(org.dms.ws.services.SwapIMEIService.class);
        resources.add(org.dms.ws.services.TransferRepairs.class);
        resources.add(org.dms.ws.services.UserBusinessMappings.class);
        resources.add(org.dms.ws.services.UserBussinessStructures.class);
        resources.add(org.dms.ws.services.UserTypeLists.class);
        resources.add(org.dms.ws.services.Users.class);
        resources.add(org.dms.ws.services.WarrantyAdjustments.class);
        resources.add(org.dms.ws.services.WarrantySchemes.class);
        resources.add(org.dms.ws.services.WarrentyRegistrations.class);
        resources.add(org.dms.ws.services.WarrentyReplacements.class);
        resources.add(org.dms.ws.services.WorkOrderCloses.class);
        resources.add(org.dms.ws.services.WorkOrderFeedBacks.class);
        resources.add(org.dms.ws.services.WorkOrderInquries.class);
        resources.add(org.dms.ws.services.WorkOrderMaintainances.class);
        resources.add(org.dms.ws.services.WorkOrderPayments.class);
        resources.add(org.dms.ws.services.WorkOrderTransfers.class);
        resources.add(org.dms.ws.services.WorkOrders.class);
    }
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        //chain.doFilter(request, response);
////throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void destroy() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    
}
